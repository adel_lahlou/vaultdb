#include <cstdio>
#include <exception>
#include <string>
#include <utilities/OutsideController.h>
#include "vaultdb_procedures/include/hb_procedures.h"
#include <iostream>

#ifndef SERVER_BOUND
//#define SERVER_BOUND
#endif
#ifndef PROCEDURES_DEBUG
//#define PROCEDURES_DEBUG
#endif

static int localPort;

static void cleanup();
static int derivePort(int localInstanceID);

#ifdef SERVER_BOUND
static std::map<int, std::string> hostNameMap = {{0, "vaultdb01:4000"}, {1, "vaultdb02:4111"}, {2, "vaultdb03:4222"}, {3, "vaultdb03:4333"}};
#else
static std::map<int, std::string> hostNameMap = {{0, "localhost:4000"}, {1, "localhost:4111"}, {2, "localhost:4222"}, {3, "localhost:4333"}};
#endif

static void bootHonestBroker(int numHosts, int queryID) {
    localPort = derivePort(0);
    fprintf(stdout, "[INFO] HonestBroker booted. %d known machines. Local port %d\n", numHosts, localPort);

    OutsideController::StartServer(0, std::string(hostNameMap[0]));
#ifdef SERVER_BOUND
    for (MachineID i = 1, last = (MachineID) numHosts; i <= last; i++) {
#ifdef PROCEDURES_DEBUG
        fprintf(stdout, "[INFO] Registering host %d at %s:%d\n", i, hostNameMap[i], derivePort(i));
#endif
        OutsideController::RegisterMachine(i, hostNameMap[i], derivePort(i));
    }
#else
    for (MachineID i = 1, last = (MachineID) numHosts; i <= last; i++) {
#ifdef PROCEDURES_DEBUG
        fprintf(stdout, "[INFO] Registering host %d at %s:%d\n", i, hostNameMap[i], derivePort(i));
#endif
        OutsideController::RegisterMachine(i, hostNameMap[i]);
    }
#endif
    while (queryID > 0) {
        switch (queryID) {
            case 1:
                if (procedures::basic_postgres_to_merge_sort(numHosts) != GenericReturnStatus::Ok) {
                    fprintf(stderr, "[FAILURE] Query 1, basic_postgres_to_merge_sort, Failed.\n");
                };
                break;
            default:
                fprintf(stderr, "[FAILURE] Unsupported query: %d\n", queryID);
                break;
        }
        fprintf(stdout, "[INFO] Enter queryID to run a query or 0 to quit: ");
        while (scanf("%d", &queryID) != 1) {
            fprintf(stdout, "[INFO] Enter queryID to run a query or 0 to quit: ");
            while(getchar() != '\n');
        };
    }
}

static void bootHost(int numHosts, int id) {
    localPort = derivePort(id);
    fprintf(stdout, "[INFO] Host %d booted. %d known machines. Local port %d\n", id, numHosts, localPort);

    OutsideController::StartServer(id, std::string(hostNameMap[id]));
#ifdef SERVER_BOUND
#ifdef PROCEDURES_DEBUG
    fprintf(stdout, "[INFO] Registering HonestBroker %s:%d\n", hostNameMap[0], derivePort(0));
#endif
    OutsideController::RegisterHonestBroker(0, hostNameMap[0], derivePort(0));
    for (MachineID i = 1, last = (MachineID) numHosts; i <= last; i++) {
        if (i == id) continue;
#ifdef PROCEDURES_DEBUG
        fprintf(stdout, "[INFO] Registering host %d at %s:%d\n", i, hostNameMap[i], derivePort(i));
#endif
        OutsideController::RegisterMachine(i, hostNameMap[i], derivePort(i));
    }
#else
#ifdef PROCEDURES_DEBUG
    fprintf(stdout, "[INFO] Registering HonestBroker %s:%d\n", hostNameMap[0], derivePort(0));
#endif
    OutsideController::RegisterHonestBroker(0, hostNameMap[0]);
    for (MachineID i = 1, last = (MachineID) numHosts; i <= last; i++) {
        if (i == id) continue;
#ifdef PROCEDURES_DEBUG
        fprintf(stdout, "[INFO] Registering host %d at %s:%d\n", i, hostNameMap[i], derivePort(i));
#endif
        OutsideController::RegisterMachine(i, hostNameMap[i]);
    }
#endif
    getchar();
}

int main(int argc, char ** argv) {
    int instanceID = -1;
    int queryID= -1;
    int numMachines = -1;
    if (argc == 3) {
        try {
            numMachines = std::stoi( argv[1]);
            instanceID = std::stoi( argv[2]);
        } catch (std::exception const &e) {
            fprintf(stderr, "[FAILURE] An argument is not integer: %s, %s\n", argv[1], argv[2]);
            return 1;
        }
    } else if (argc == 4) {
        try {
            numMachines = std::stoi( argv[1]);
            instanceID = std::stoi( argv[2]);
            queryID = std::stoi( argv[3]);
        } catch (std::exception const &e) {
            fprintf(stderr, "[FAILURE] An argument is not integer: %s, %s, %s\n", argv[1], argv[2], argv[3]);
            return 1;
        }
    }
    if (instanceID < 0 || (instanceID == 0 && queryID < 0)) {
        fprintf(stderr, "[INFO] Usage: ./vaultdb_run #machines (0 #queryID | #hostID)\n");
        return 1;
    }

    if (instanceID == 0) {
        bootHonestBroker(numMachines, queryID);
    } else {
        bootHost(numMachines, instanceID);
    }


    cleanup();
    return 0;
}


static void cleanup() {
    fprintf(stdout, "[INFO] Shutting down...");
    OutsideController::StopServer();
    fprintf(stdout, " done!\n");
}

static int derivePort(int localInstanceID) {
    return 4000 + 100 * localInstanceID + 10 * localInstanceID + localInstanceID;
};