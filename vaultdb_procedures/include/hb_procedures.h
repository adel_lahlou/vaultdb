#ifndef VAULTDB_PROJ_HB_PROCEDURES_H
#define VAULTDB_PROJ_HB_PROCEDURES_H

#include "in_sgx/obdata/ObliviousTuple.h"
#include "shared/Definitions.h"

namespace procedures {
    extern size_vdb icd9_len;
    extern size_vdb k_value;
    extern type::RecordSchema diag;

    void holdUntilQueryIsDone();
    GenericReturnStatus basic_postgres_to_merge_sort(int numHosts);
    bool oblivious_tuples_equal(const db::obdata::ObliviousTuple& t1, const db::obdata::ObliviousTuple& t2);
    db::obdata::obfield::ObliviousField makeObliviousTimestampNoZoneFieldFromString(const char * data);
}

#endif //VAULTDB_PROJ_HB_PROCEDURES_H
