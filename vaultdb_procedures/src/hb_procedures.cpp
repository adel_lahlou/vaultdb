#include "../include/hb_procedures.h"
#include "../../vaultdb_tests/utility_tests/utilityTestFixture.h"
#include <cstring>
#include <in_sgx/plan/kaoperators/KAnonymousSeqScan.h>
#include <in_sgx/plan/kaoperators/ClusterKAnonymousBitonicMergeSort.h>
#include <utilities/OutsideController.h>

#ifndef PROCEDURES_DEBUG
//#define PROCEDURES_DEBUG
#endif

namespace procedures {

    size_vdb icd9_len = 6;
    size_vdb k_value = 2;

    type::RecordSchema diag { 5, {
            {type::FieldDataType::Int, INT_FIELD_SIZE},
            {type::FieldDataType::Fixchar, icd9_len},
            {type::FieldDataType::TimestampNoZone, TIMENOZONE_FIELD_SIZE}}};

    GenericReturnStatus basic_postgres_to_merge_sort(int numHosts) {
        fprintf(stdout, "[INFO] Query 1, basic_postgres_to_merge_sort, called\n");
        if (numHosts != 2) {
            fprintf(stderr, "[FAILURE] Number of hosts expected: 2; actual: %d\n", numHosts);
            return GenericReturnStatus::FatalError;
        }

        const char *postgresQueries[2];
        const char * q1 = "select patient_id, icd9, timestamp_ from diagnoses where patient_id = 1";
        const char * q2 = "select patient_id, icd9, timestamp_ from diagnoses where patient_id = 2";
        postgresQueries[0] = q1;
        postgresQueries[1] = q2;
        type::RecordSchema postgresOutputSchema = diag;

        TableID postgresTableID = postgresOutputSchema.getTableID();
        TableID queryID = 10;

        //------------------------------ ACTUAL RUN -----------------------------

        for (MachineID machineID = 1; machineID <= 2; machineID++) {

#ifdef PROCEDURES_DEBUG
            fprintf(stdout, "[INFO] Before dispatching PostgreSQL queries for machine %d\n", machineID);
#endif
            if (GenericReturnStatus ::Ok !=
                    OutsideController::SendPostgreSQLQuery(machineID, postgresTableID, diag, postgresQueries[machineID - 1], STEP_QUERY)) {
                return GenericReturnStatus ::FatalError;
            };
#ifdef PROCEDURES_DEBUG
            fprintf(stdout, "[INFO] PostgreSQL queries dispatched for %d\n", machineID);
#endif
        }

        std::vector<std::unique_ptr<unsigned char[]>> secureQueries;
        ObliviousTupleTable tempTable (postgresOutputSchema, {});
        for (MachineID machineID = 1; machineID <= 2; machineID++) {
            //----------------------------- SECOND QUERY ----------------------------
            plan::kaoperators::KAnonymousSeqScan *scan1 =
                    new plan::kaoperators::KAnonymousSeqScan(queryID, k_value, &tempTable);
            plan::kaoperators::ClusterKAnonymousBitonicMergeSort *sort1 =
                    new plan::kaoperators::ClusterKAnonymousBitonicMergeSort(machineID, queryID, 0, k_value, scan1,
                                                                             {0, 1, 2},
                                                                             {SortOrder::ASCEND, SortOrder::ASCEND,
                                                                              SortOrder::ASCEND}, nullptr,
                                                                             (MachineID) 1);
            size_vdb encodingSize;
            secureQueries.emplace_back(std::unique_ptr<unsigned char[]> (
                    plan::kaoperators::KAnonymousOperator::EncodeQueryToNewArray(sort1, encodingSize)));
#ifdef PROCEDURES_DEBUG
            fprintf(stdout, "[INFO] Before dispatching secure queries for machine %d\n", machineID);
#endif
            std::thread t1(OutsideController::SendQuery, machineID, queryID, secureQueries[machineID - 1].get(), encodingSize, FINAL_QUERY);
            t1.detach();
#ifdef PROCEDURES_DEBUG
            fprintf(stdout, "[INFO] Secure queries dispatched for %d\n", machineID);
#endif
        }


        //--------------------------- RESULT CHECKING ---------------------------
        holdUntilQueryIsDone();

        ObliviousTupleList expectedTuples{
//    1,'008.45',2006-01-01 00:00:00
//    1,'414.01',2006-09-07 00:00:00
//    2,'008.45',2006-02-01 00:00:00
//    2,'414.01',2006-09-07 00:00:00
                ObliviousTuple(false, {
                        db::obdata::obfield::ObliviousField::makeObliviousIntField(1),
                        db::obdata::obfield::ObliviousField::makeObliviousFixcharField("008.45", icd9_len),
                        makeObliviousTimestampNoZoneFieldFromString("2006-01-01 00:00:00"),
                }),
                ObliviousTuple(false, {
                        db::obdata::obfield::ObliviousField::makeObliviousIntField(1),
                        db::obdata::obfield::ObliviousField::makeObliviousFixcharField("414.01", icd9_len),
                        makeObliviousTimestampNoZoneFieldFromString("2006-09-07 00:00:00"),
                }),
                ObliviousTuple(false, {
                        db::obdata::obfield::ObliviousField::makeObliviousIntField(2),
                        db::obdata::obfield::ObliviousField::makeObliviousFixcharField("008.45", icd9_len),
                        makeObliviousTimestampNoZoneFieldFromString("2006-02-01 00:00:00"),
                }),
                ObliviousTuple(false, {
                        db::obdata::obfield::ObliviousField::makeObliviousIntField(2),
                        db::obdata::obfield::ObliviousField::makeObliviousFixcharField("414.01", icd9_len),
                        makeObliviousTimestampNoZoneFieldFromString("2006-09-07 00:00:00"),
                }),
        };

        db::obdata::ObliviousTupleTable *resultTable = &OutsideController::GetResultObliviousTupleTable();
#ifdef PROCEDURES_DEBUG
        printf("Table Output: \n");
#endif
        if (!(diag == resultTable->getSchema())) {
#ifdef PROCEDURES_DEBUG
            fprintf(stderr, "[FAILURE] Schemas are not equal.\n");
#endif
            return GenericReturnStatus::FatalError;
        };

        for (pos_vdb i = 0, size = (size_vdb) resultTable->size(); i < size; i++) {
#ifdef PROCEDURES_DEBUG
            fprintf(stdout, "[INFO] Expected / Actual: %s / %s\n", expectedTuples[i].toString().c_str(),
                    (*resultTable)[i].toString().c_str());
#endif
            if (!oblivious_tuples_equal(expectedTuples[i], (*resultTable)[i])) {
                fprintf(stderr, "[FAILURE] Tuples are not equal.\n");
                return GenericReturnStatus::FatalError;
            };
        }
        fprintf(stdout, "[INFO] Query 1, basic_postgres_to_merge_sort, succeeded.\n");
        return GenericReturnStatus::Ok;
    }

    void holdUntilQueryIsDone() {
        struct timespec tim, tim2;
        tim.tv_sec = 0;
        tim.tv_nsec = 200000000L;
        while (!OutsideController::IsDone()) {
            nanosleep(&tim, &tim2);
        };
    };

    bool oblivious_tuples_equal(const ObliviousTuple &t1, const ObliviousTuple &t2) {
        if (t1.size() != t2.size())
            return false;
        for (int i = 0; i < t1.size(); ++i) {
            if (!(t1[i] == t2[i]))
                return false;
        }
        return true;
    }

    db::obdata::obfield::ObliviousField makeObliviousTimestampNoZoneFieldFromString(const char * data) {
        if (strlen(data) < 19) {
            throw std::length_error("Input char array too short");
        }
        tm tm1;
        sscanf(data,"%04d-%02d-%2d %02d:%02d:%02d",&tm1.tm_year,&tm1.tm_mon,&tm1.tm_mday,
               &tm1.tm_hour,&tm1.tm_min,&tm1.tm_sec);
        tm1.tm_year -= 1900;
        tm1.tm_mon -= 1;
        tm1.tm_isdst = 0;
        return db::obdata::obfield::ObliviousField::makeObliviousTimestampNoZoneField(timegm(&tm1));
    };
}