-- Servers
CREATE SERVER vdb01_healthlnk_fdw FOREIGN DATA WRAPPER postgres_fdw OPTIONS (host 'vaultdb01', dbname 'healthlnk', port '5432');
CREATE SERVER vdb02_healthlnk_fdw FOREIGN DATA WRAPPER postgres_fdw OPTIONS (host 'vaultdb02', dbname 'healthlnk', port '5432');
CREATE SERVER vdb03_healthlnk_fdw FOREIGN DATA WRAPPER postgres_fdw OPTIONS (host 'vaultdb03', dbname 'healthlnk', port '5432');
CREATE SERVER vdb04_healthlnk_fdw FOREIGN DATA WRAPPER postgres_fdw OPTIONS (host 'vaultdb04', dbname 'healthlnk', port '5432');

-- User mapping
CREATE USER MAPPING FOR test_user SERVER vdb01_healthlnk_fdw OPTIONS (user 'test_user', password 's!mpl3pw4t&r');
CREATE USER MAPPING FOR test_user SERVER vdb02_healthlnk_fdw OPTIONS (user 'test_user', password 's!mpl3pw4t&r');
CREATE USER MAPPING FOR test_user SERVER vdb03_healthlnk_fdw OPTIONS (user 'test_user', password 's!mpl3pw4t&r');
CREATE USER MAPPING FOR test_user SERVER vdb04_healthlnk_fdw OPTIONS (user 'test_user', password 's!mpl3pw4t&r');

-- Host 1
CREATE SCHEMA vdb01;

-- alternative 1
IMPORT FOREIGN SCHEMA public FROM SERVER vdb01_healthlnk_fdw INTO vdb01;

-- alternative 2
CREATE FOREIGN TABLE vdb01.diagnoses (patient_id integer OPTIONS (column_name 'patient_id'), visit_no integer OPTIONS (column_name 'visit_no')) SERVER vdb01_healthlnk_fdw OPTIONS (schema_name 'public', table_name 'diagnoses');
CREATE FOREIGN TABLE vdb01.vitals (patient_id integer OPTIONS (column_name 'patient_id'), visit_no integer OPTIONS (column_name 'visit_no')) SERVER vdb01_healthlnk_fdw OPTIONS (schema_name 'public', table_name 'vitals');
CREATE FOREIGN TABLE vdb01.medications (patient_id integer OPTIONS (column_name 'patient_id')) SERVER vdb01_healthlnk_fdw OPTIONS (schema_name 'public', table_name 'medications');
CREATE FOREIGN TABLE vdb01.demographics (patient_id integer OPTIONS (column_name 'patient_id')) SERVER vdb01_healthlnk_fdw OPTIONS (schema_name 'public', table_name 'demographics');
CREATE FOREIGN TABLE vdb01.cdiff_cohort_2006 (patient_id integer OPTIONS (column_name 'patient_id')) SERVER vdb01_healthlnk_fdw OPTIONS (schema_name 'public', table_name 'cdiff_cohort_2006');

-- Host 2
CREATE SCHEMA vdb02;

-- alternative 1
IMPORT FOREIGN SCHEMA public FROM SERVER vdb02_healthlnk_fdw INTO vdb02;

-- alternative 2
CREATE FOREIGN TABLE vdb02.diagnoses (patient_id integer OPTIONS (column_name 'patient_id'), visit_no integer OPTIONS (column_name 'visit_no')) SERVER vdb02_healthlnk_fdw OPTIONS (schema_name 'public', table_name 'diagnoses');
CREATE FOREIGN TABLE vdb02.vitals (patient_id integer OPTIONS (column_name 'patient_id'), visit_no integer OPTIONS (column_name 'visit_no')) SERVER vdb02_healthlnk_fdw OPTIONS (schema_name 'public', table_name 'vitals');
CREATE FOREIGN TABLE vdb02.medications (patient_id integer OPTIONS (column_name 'patient_id')) SERVER vdb02_healthlnk_fdw OPTIONS (schema_name 'public', table_name 'medications');
CREATE FOREIGN TABLE vdb02.demographics (patient_id integer OPTIONS (column_name 'patient_id')) SERVER vdb02_healthlnk_fdw OPTIONS (schema_name 'public', table_name 'demographics');
CREATE FOREIGN TABLE vdb02.cdiff_cohort_2006 (patient_id integer OPTIONS (column_name 'patient_id')) SERVER vdb02_healthlnk_fdw OPTIONS (schema_name 'public', table_name 'cdiff_cohort_2006');

-- Host 3
CREATE SCHEMA vdb03;

-- alternative 1
IMPORT FOREIGN SCHEMA public FROM SERVER vdb03_healthlnk_fdw INTO vdb03;

-- alternative 2
CREATE FOREIGN TABLE vdb03.diagnoses (patient_id integer OPTIONS (column_name 'patient_id'), visit_no integer OPTIONS (column_name 'visit_no')) SERVER vdb03_healthlnk_fdw OPTIONS (schema_name 'public', table_name 'diagnoses');
CREATE FOREIGN TABLE vdb03.vitals (patient_id integer OPTIONS (column_name 'patient_id'), visit_no integer OPTIONS (column_name 'visit_no')) SERVER vdb03_healthlnk_fdw OPTIONS (schema_name 'public', table_name 'vitals');
CREATE FOREIGN TABLE vdb03.medications (patient_id integer OPTIONS (column_name 'patient_id')) SERVER vdb03_healthlnk_fdw OPTIONS (schema_name 'public', table_name 'medications');
CREATE FOREIGN TABLE vdb03.demographics (patient_id integer OPTIONS (column_name 'patient_id')) SERVER vdb03_healthlnk_fdw OPTIONS (schema_name 'public', table_name 'demographics');
CREATE FOREIGN TABLE vdb03.cdiff_cohort_2006 (patient_id integer OPTIONS (column_name 'patient_id')) SERVER vdb03_healthlnk_fdw OPTIONS (schema_name 'public', table_name 'cdiff_cohort_2006');

-- Host 4
CREATE SCHEMA vdb04;

-- alternative 1
IMPORT FOREIGN SCHEMA public FROM SERVER vdb04_healthlnk_fdw INTO vdb04;

-- alternative 2
CREATE FOREIGN TABLE vdb04.diagnoses (patient_id integer OPTIONS (column_name 'patient_id'), visit_no integer OPTIONS (column_name 'visit_no')) SERVER vdb04_healthlnk_fdw OPTIONS (schema_name 'public', table_name 'diagnoses');
CREATE FOREIGN TABLE vdb04.vitals (patient_id integer OPTIONS (column_name 'patient_id'), visit_no integer OPTIONS (column_name 'visit_no')) SERVER vdb04_healthlnk_fdw OPTIONS (schema_name 'public', table_name 'vitals');
CREATE FOREIGN TABLE vdb04.medications (patient_id integer OPTIONS (column_name 'patient_id')) SERVER vdb04_healthlnk_fdw OPTIONS (schema_name 'public', table_name 'medications');
CREATE FOREIGN TABLE vdb04.demographics (patient_id integer OPTIONS (column_name 'patient_id')) SERVER vdb04_healthlnk_fdw OPTIONS (schema_name 'public', table_name 'demographics');
CREATE FOREIGN TABLE vdb04.cdiff_cohort_2006 (patient_id integer OPTIONS (column_name 'patient_id')) SERVER vdb04_healthlnk_fdw OPTIONS (schema_name 'public', table_name 'cdiff_cohort_2006');

-- HB
CREATE FOREIGN TABLE vdb01.patient_id_buffer_1 (patient_id integer OPTIONS (column_name 'patient_id')) SERVER vdb01_healthlnk_fdw OPTIONS (schema_name 'vdb01', table_name 'patient_id_buffer_1');
CREATE FOREIGN TABLE vdb01.patient_id_buffer_2 (patient_id integer OPTIONS (column_name 'patient_id')) SERVER vdb01_healthlnk_fdw OPTIONS (schema_name 'vdb01', table_name 'patient_id_buffer_2');
CREATE FOREIGN TABLE vdb01.patient_id_buffer_3 (patient_id integer OPTIONS (column_name 'patient_id')) SERVER vdb01_healthlnk_fdw OPTIONS (schema_name 'vdb01', table_name 'patient_id_buffer_3');
CREATE FOREIGN TABLE vdb01.patient_id_buffer_4 (patient_id integer OPTIONS (column_name 'patient_id')) SERVER vdb01_healthlnk_fdw OPTIONS (schema_name 'vdb01', table_name 'patient_id_buffer_4');

-- Grant
GRANT USAGE ON SCHEMA vdb01 TO test_user;
GRANT USAGE ON SCHEMA vdb02 TO test_user;
GRANT USAGE ON SCHEMA vdb03 TO test_user;
GRANT USAGE ON SCHEMA vdb04 TO test_user;

GRANT SELECT ON ALL TABLES IN SCHEMA public TO test_user;

GRANT SELECT ON ALL TABLES IN SCHEMA vdb01 TO test_user;
GRANT SELECT ON ALL TABLES IN SCHEMA vdb02 TO test_user;
GRANT SELECT ON ALL TABLES IN SCHEMA vdb03 TO test_user;
GRANT SELECT ON ALL TABLES IN SCHEMA vdb04 TO test_user;