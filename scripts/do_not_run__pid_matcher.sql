-- run this on the full set, save the result and ship
WITH a AS (
	SELECT patient_id FROM diagnoses
	union
	SELECT patient_id FROM vitals
	union
	SELECT patient_id FROM medications
	UNION
	SELECT patient_id FROM demographics
)
SELECT patient_id, cast(row_number() OVER (ORDER BY patient_id) AS integer) AS pid INTO pid_matcher
FROM a;


-- run this on each host
CREATE TABLE pid_matcher (
	patient_id integer,
	pid integer
);
 \copy pid_matcher FROM '/home/zsh745/pid_matcher' BINARY;