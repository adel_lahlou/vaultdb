\set year 2006

\echo Creating cohort table cdiff_cohort_:year

SELECT DISTINCT patient_id INTO cdiff_cohort_:year FROM diagnoses WHERE icd9='008.45' AND year = :year;