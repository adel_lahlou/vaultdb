\set year 2006

\echo Populating cross-host patient_id tables for :year

DELETE FROM vdb01.patient_id_buffer_1;
DELETE FROM vdb01.patient_id_buffer_2;
DELETE FROM vdb01.patient_id_buffer_3;
DELETE FROM vdb01.patient_id_buffer_4;

INSERT INTO vdb01.patient_id_buffer_1
WITH a AS (
	SELECT DISTINCT patient_id FROM diagnoses d WHERE year = :year
	UNION ALL
	SELECT DISTINCT patient_id FROM vdb02.diagnoses d WHERE year = :year
	UNION ALL
	SELECT DISTINCT patient_id FROM vdb03.diagnoses d WHERE year = :year
	UNION ALL
	SELECT DISTINCT patient_id FROM vdb04.diagnoses d WHERE year = :year
)
SELECT patient_id
FROM a
GROUP BY patient_id
HAVING count(*) > 1;

INSERT INTO vdb01.patient_id_buffer_2
WITH a AS (
	SELECT DISTINCT patient_id FROM vitals WHERE year = :year
	UNION ALL
	SELECT DISTINCT patient_id FROM vdb02.vitals WHERE year = :year
	UNION ALL
	SELECT DISTINCT patient_id FROM vdb03.vitals WHERE year = :year
	UNION ALL
	SELECT DISTINCT patient_id FROM vdb04.vitals WHERE year = :year
)
SELECT patient_id
FROM a
GROUP BY patient_id
HAVING count(*) > 1;

INSERT INTO vdb01.patient_id_buffer_3
WITH a AS (
	SELECT DISTINCT patient_id FROM medications WHERE year = :year
	UNION ALL
	SELECT DISTINCT patient_id FROM vdb02.medications WHERE year = :year
	UNION ALL
	SELECT DISTINCT patient_id FROM vdb03.medications WHERE year = :year
	UNION ALL
	SELECT DISTINCT patient_id FROM vdb04.medications WHERE year = :year
)
SELECT patient_id
FROM a
GROUP BY patient_id
HAVING count(*) > 1;

INSERT INTO vdb01.patient_id_buffer_4 SELECT * FROM (
	SELECT * FROM vdb01.patient_id_buffer_1
	UNION
	SELECT * FROM vdb01.patient_id_buffer_2
	UNION
	SELECT * FROM vdb01.patient_id_buffer_3
	UNION
	SELECT * FROM vdb01.patient_id_buffer_4
) a;