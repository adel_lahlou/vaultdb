CREATE SCHEMA IF NOT EXISTS vdb01;
ALTER SCHEMA vdb01 OWNER TO test_user;

CREATE TABLE IF NOT EXISTS vdb01.patient_id_buffer_1 (patient_id integer);
CREATE TABLE IF NOT EXISTS vdb01.patient_id_buffer_2 (patient_id integer);
CREATE TABLE IF NOT EXISTS vdb01.patient_id_buffer_3 (patient_id integer);
CREATE TABLE IF NOT EXISTS vdb01.patient_id_buffer_4 (patient_id integer);

ALTER TABLE vdb01.patient_id_buffer_1 OWNER TO test_user;
ALTER TABLE vdb01.patient_id_buffer_2 OWNER TO test_user;
ALTER TABLE vdb01.patient_id_buffer_3 OWNER TO test_user;
ALTER TABLE vdb01.patient_id_buffer_4 OWNER TO test_user;

create table vaultdb_performance ( time_mark timestamp, query_number integer, k integer, tag varchar, value real, comment varchar);
