#include <mutex>
#include "utilities/PostgreSQLHandler.h"

#define ERROR_CLEANUP_CONN(x, e){\
    cleanup();\
    char err[256];\
    sprintf(err, x, e);\
    fprintf(stderr, "%s\n", err);\
}

using namespace utilities;

PostgreSQLHandler::PostgreSQLHandler(const char *db_user, const char *db_pass, const char *dbname)
: _user(db_user),
  _pass(db_pass),
  _db(dbname),
  _res(nullptr),
  _conn(nullptr),
  _inTransaction(false),
  _isCleanupIssued(false)
{}


PostgreSQLHandler::~PostgreSQLHandler () {
	cleanup();
};


int PostgreSQLHandler::connect(){
    if (_user == nullptr || _pass == nullptr || _db == nullptr) {
      fprintf(stderr, "[FAILURE] Missing PostgreSQL connection info");
      return RETURN_FAILURE;
    }

    char conn_str[256];
    if (sprintf(conn_str, "user = %s password = %s dbname = %s", _user, _pass, _db) < 0) {
      fprintf(stderr, "[FAILURE] Invalid connection string %s\n", conn_str);
      return RETURN_FAILURE;
    }

    _conn = PQconnectdb(conn_str);
    if (PQstatus(_conn) != CONNECTION_OK) {
      ERROR_CLEANUP_CONN("[FAILURE] Connection to database failed: %s", PQerrorMessage(_conn));
      return RETURN_FAILURE;
    }
    return RETURN_SUCCESS;
}


void PostgreSQLHandler::disconnect() {
  endTransaction();
  if (_conn!= nullptr) PQfinish(_conn);
  _conn= nullptr;
};


void PostgreSQLHandler::clearResults(){
    if (_res != nullptr)
    PQclear(_res);
    _res = nullptr;
}

void PostgreSQLHandler::cleanup() {
    if (_isCleanupIssued) {
        return;
    }
    _isCleanupIssued = true;
    clearResults();
    disconnect();
    _isCleanupIssued = false;
};


int PostgreSQLHandler::startTransaction(){
    if (_inTransaction) {
        fprintf(stdout, "[INFO] Unable to start transaction; another transaction is in progress...\n");
        return RETURN_FAILURE;
    }
    _inTransaction = true;
    PQclear(_res);
    _res = PQexec(_conn, "BEGIN");
    if (PQresultStatus(_res) != PGRES_COMMAND_OK)
    {
        ERROR_CLEANUP_CONN("[FAILURE] BEGIN command failed: %s", PQerrorMessage(_conn));
        _inTransaction = false;
        return RETURN_FAILURE;
    }
    return RETURN_SUCCESS;
}


void PostgreSQLHandler::endTransaction(){
    if (!_inTransaction) {
        return;
    }

    PQclear(_res);
    _res = PQexec(_conn, "END");
    if (PQresultStatus(_res) != PGRES_COMMAND_OK)
    {
        ERROR_CLEANUP_CONN("[FAILURE] END command failed: %s", PQerrorMessage(_conn));
    }
    PQclear(_res);
    _res = nullptr;
    _inTransaction = false;
}


int PostgreSQLHandler::query(const char *command) {
    if (_conn== nullptr && connect() == RETURN_FAILURE) {
        return RETURN_FAILURE;
    }

    if (startTransaction() == RETURN_FAILURE) {
        return RETURN_FAILURE;
    }

    clearResults();
    _res = PQexecParams(_conn, command, 0, nullptr, nullptr, nullptr, nullptr, 1);
    if (PQresultStatus(_res) != PGRES_TUPLES_OK) {
        ERROR_CLEANUP_CONN("[FAILURE] PostgresHandler unable to fetch data; result error message: %s", PQresultErrorMessage(_res));
        return RETURN_FAILURE;
    }
    return RETURN_SUCCESS;
};


int PostgreSQLHandler::singleQueryWithObliviousTupleTable(db::obdata::ObliviousTupleTable & out, type::RecordSchema outputSchema, const char * query) {

    std::unique_lock<std::mutex> guard(_queryMutex);
    if (this->query(query) != RETURN_SUCCESS) {
        ERROR_CLEANUP_CONN("[FAILURE] PostgreSQLHandler unable to fetch data; result error message: %s", PQresultErrorMessage(_res));
        return RETURN_FAILURE;
    }

    int oid, i , j, nTuples = PQntuples(_res), nFields = PQnfields(_res);

    std::vector<size_vdb> maxLengths;
    db::obdata::ObliviousTupleList tuples;
    for (i = 0; i < nTuples; i++) {
        db::obdata::ObliviousFieldHolder fields;
        for (j = 0; j < nFields; j++) {
            switch (oid = PQftype(_res, j)) {
                case 23:
                    fields.push_back(db::obdata::obfield::ObliviousField::makeObliviousIntField(
                            swap_endian<int>(*(int*)PQgetvalue(_res, i, j))));
                    break;
                case 1114:
                    fields.push_back(db::obdata::obfield::ObliviousField::makeObliviousTimestampNoZoneField(
                            swap_endian<time_t>(*(time_t*)PQgetvalue(_res, i, j)) / 1000000 + 60*60*24*10957));
                    break;
                case 1042:
                    fields.push_back(db::obdata::obfield::ObliviousField::makeObliviousFixcharField(
                            PQgetvalue(_res, i, j), outputSchema[j].size));
                    break;
                case 1043:
                    fields.push_back(db::obdata::obfield::ObliviousField::makeObliviousFixcharField(
                            PQgetvalue(_res, i, j), outputSchema[j].size));
                    break;
                default: {
                    ERROR_CLEANUP_CONN("[FAILURE] PostgresHandler unsupported type: %d", oid);
                    return RETURN_FAILURE;
                }
            }
        }
        tuples.emplace_back(fields);
    }
    endTransaction();
    cleanup();
    out = db::obdata::ObliviousTupleTable(outputSchema, tuples);
    return RETURN_SUCCESS;
};

int PostgreSQLHandler::singleQueryWithNoResult(const char * query) {
    std::unique_lock<std::mutex> guard(_queryMutex);
    if (_conn== nullptr && connect() == RETURN_FAILURE) {
        return RETURN_FAILURE;
    }

    if (startTransaction() == RETURN_FAILURE) {
        return RETURN_FAILURE;
    }

    clearResults();
    _res = PQexecParams(_conn, query, 0, nullptr, nullptr, nullptr, nullptr, 0);
    if (PQresultStatus(_res) != PGRES_COMMAND_OK) {
        ERROR_CLEANUP_CONN("[FAILURE] PostgresHandler unable to non-return query; result error message: %s", PQresultErrorMessage(_res));
        return RETURN_FAILURE;
    }
    endTransaction();
    cleanup();
    return RETURN_SUCCESS;
};