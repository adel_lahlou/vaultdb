#include "utilities/ConfigManager.h"

std::shared_ptr<ConfigManager> ConfigManager::_instance;
bool ConfigManager::_isInitialized = false;

ConfigManager * ConfigManager::instance()
{
    if (!_isInitialized) {
        _instance = std::shared_ptr<ConfigManager>(new ConfigManager());
        _isInitialized = true;
    }
    return _instance.get();
};

void ConfigManager::addEntry(int key, string value) {
    char * c = new char[value.size()+1];
    const char * cc = value.c_str();
    memcpy(c, cc, value.size()+1);
    _data.emplace(key, std::shared_ptr<char>(c, [](char * c){delete[] c;}));
}

ConfigManager::ConfigManager()
: _reader(CONFIG_PATH)
{
    if (_reader.ParseError() >= 0){
        addEntry(0, _reader.Get("pems", "cipher_list", "UNKNOWN"));
        addEntry(1, _reader.Get("pems", "cert_home", "UNKNOWN") + _reader.Get("pems", "cert_file", "UNKNOWN"));
        addEntry(2, _reader.Get("pems", "cert_home", "UNKNOWN") + _reader.Get("pems", "key_file", "UNKNOWN"));
        addEntry(3, _reader.Get("pems", "cert_home", "UNKNOWN") + _reader.Get("pems", "ca_file", "UNKNOWN"));
        addEntry(4, _reader.Get("pems", "key_passw", "UNKNOWN"));

        addEntry(5, _reader.Get("postgres", "username", "UNKNOWN"));
        addEntry(6, _reader.Get("postgres", "userpass", "UNKNOWN"));
        addEntry(7, _reader.Get("postgres", "database", "UNKNOWN"));
    }
};

std::shared_ptr<char> ConfigManager::getCipherList()
{
    return _data[0];
};

std::shared_ptr<char> ConfigManager::getCertFilePath()
{
    return _data[1];
};

std::shared_ptr<char> ConfigManager::getKeyFilePath()
{
    return _data[2];
};

std::shared_ptr<char> ConfigManager::getCAFilePath()
{
    return _data[3];
};

std::shared_ptr<char> ConfigManager::getKeyPW()
{
    return _data[4];
};

std::shared_ptr<char> ConfigManager::getPgUser()
{
    return _data[5];
};

std::shared_ptr<char> ConfigManager::getPgUserPass()
{
    return _data[6];
};

std::shared_ptr<char> ConfigManager::getPgDB()
{
    return _data[7];
};