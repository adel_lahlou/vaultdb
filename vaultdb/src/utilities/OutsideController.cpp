#include "utilities/OutsideController.h"
#include "utilities/NetworkTransmissionHandler.h"

#include <cstring>
#include <gateway/GatewayBetweenTwoWorlds.h>
#include <in_sgx/serialization/ObliviousSerializer.h>
#include <utilities/ConfigManager.h>
#ifndef LOCAL_DEBUG_MODE
#include <SGXLib.h>
#endif

#ifndef CONTROLLER_DEBUG
//#define CONTROLLER_DEBUG
#endif

void pass_on_oblivious_table(unsigned int dst_machine_id, unsigned int opT_id, unsigned char * table, unsigned int size, int is_final_query) {
    OutsideController::SendObliviousTupleTable(dst_machine_id, opT_id, table, size, is_final_query);
}

void deposit_table_to_outside_storage(int machine_id, int op_id, int partition_id, unsigned char * data, int len) {
    OutsideController::DepositTable(machine_id, op_id, partition_id, data, len);
}

void retrieve_oblivious_table(int * ret, int machine_id, int op_id, int partition_id, unsigned char * data, int len) {
    OutsideController::RetrieveTableIntoEnclave(ret, machine_id, op_id, partition_id, data, len);
}

OutsideController::OutsideController(MachineID localID, std::string addressAndPort)
: _localMachineID(localID),
  _honestBrokerID(0),
  _postgres(ConfigManager::instance()->getPgUser().get(),ConfigManager::instance()->getPgUserPass().get(),ConfigManager::instance()->getPgDB().get())
{
    if (_localMachineID != 0) {
        launchEnclaves();
    }
#ifdef HONEST_BROKER_INSTANCE
    _isTupleTableInitialized = false;
#endif
    _network = new NetworkTransmissionHandler(this, localID, addressAndPort);
    registerMachine(_localMachineID, addressAndPort);
}

OutsideController::~OutsideController() {
    delete _network;
    if (_localMachineID != 0) {
        destroyEnclaves();
    }
};

OutsideController * OutsideController::_instance = nullptr;

void OutsideController::RegisterMachine(MachineID machineID, std::string hostNamePort) {
    _instance->registerMachine(machineID, hostNamePort);
};

void OutsideController::RegisterHonestBroker(MachineID machineID, std::string hostNamePort) {
    _instance->registerHonestBroker(machineID, hostNamePort);
};

void OutsideController::SendObliviousTupleTable(MachineID dst_machine_id, TableID outputTableID, unsigned char *table, size_vdb size, int isFinalQuery) {
    _instance->sendObliviousTupleTable(dst_machine_id, outputTableID, table, size, isFinalQuery);
};

void OutsideController::DepositTable(MachineID machineID, TableID tableID, pos_vdb partitionID, unsigned char *table, size_vdb size) {
    _instance->depositTable(machineID, tableID, partitionID, table, size);
};

void OutsideController::RetrieveTableIntoEnclave(int *ret, int machine_id, int op_id, int partition_id, unsigned char *data,
                                                 int len) {
    _instance->retrieveTableIntoEnclave(ret, machine_id, op_id, partition_id, data, len);
};

//-------------------------- SERVICE UTILITIES --------------------------

void OutsideController::registerMachine(MachineID machineID, std::string hostNamePort) {
#ifdef CONTROLLER_DEBUG
    fprintf(stdout, "[INFO] Registering host %d at %s\n", machineID, hostNamePort.c_str());
#endif
    _network->registerMachine(machineID, hostNamePort);
    std::unique_lock<std::mutex> guard(_registryLock);
    _machineRegistry.insert(machineID);
    for (auto v : _enclaveRegistry) {
        DeliverRequestToRegisterRemoteMachine(v, machineID);
    }
};

void OutsideController::registerHonestBroker(MachineID machineID, std::string hostNamePort) {
#ifdef CONTROLLER_DEBUG
    fprintf(stdout, "[INFO] Registering honest broker at %d at %s\n", machineID, hostNamePort.c_str());
#endif
    _network->registerHonestBroker(machineID, hostNamePort);
    _honestBrokerID = machineID;
    std::unique_lock<std::mutex> guard(_registryLock);
    _machineRegistry.insert(machineID);
    for (auto v : _enclaveRegistry) {
        DeliverRequestToRegisterHonestBroker(v, machineID);
    }
};

void OutsideController::launchEnclaves() {
#ifdef CONTROLLER_DEBUG
    fprintf(stdout, "[INFO] Launching enclave.\n");
#endif
    // FIXME launch each enclave and add the 'sgx_enclave_id_t' to _enclaveRegistry; we currently plan for 1 enclave each machine.
    _enclaveRegistry.push_back(0);
    InitializeInSGXController(_localMachineID, 0);
};

void OutsideController::destroyEnclaves() {
    // FIXME destroy each enclave in _enclaveRegistry
};

int OutsideController::receiveSecureNewQuery(unsigned char * inputBuffer, size_vdb size, int isFinalQuery) {
#ifdef CONTROLLER_DEBUG
    fprintf(stdout, "[INFO] Received new secure query; isFinal? %d\n", isFinalQuery);
#endif
    return DeliverQueryIntoEnclave(_enclaveRegistry[0], inputBuffer, size, isFinalQuery);
};

int OutsideController::receiveObliviousTupleTable(MachineID src_machine_id, unsigned char * inputBuffer, size_vdb size) {
#ifdef CONTROLLER_DEBUG
    fprintf(stdout, "[INFO] Received ObliviousTupleTable from %d; proceeding to ingest.\n", src_machine_id);
#endif
    return DeliveryObliviousTupleTableIntoEnclave(_enclaveRegistry[0], src_machine_id, size, inputBuffer);
};


void OutsideController::StartServer(MachineID localID, std::string hostNameAndPort) {
    if (_instance == nullptr) {
        _instance = new OutsideController(localID, hostNameAndPort);
    } else {
        fprintf(stderr, "[FAILURE] Server already running; please call StopServer() before issuing new request.");
    }
};

void OutsideController::StopServer() {
    delete _instance;
    _instance = nullptr;
};

bool OutsideController::IsDone() {
    return _instance->_waitForCount.empty();
};

void OutsideController::sendObliviousTupleTable(MachineID dst_machine_id, TableID outputTableID, unsigned char *table,
                                                size_vdb size, int isFinalQuery) {
    if (dst_machine_id == _localMachineID) {
#ifdef CONTROLLER_DEBUG
        fprintf(stdout, "[INFO] Re-ingesting locally-directed ObliviousTuplesTable %d\n", outputTableID);
#endif
        DeliveryObliviousTupleTableIntoEnclave(_enclaveRegistry[0], _localMachineID, size, table);
    } else if (dst_machine_id == _honestBrokerID && isFinalQuery == STEP_QUERY) {
#ifdef CONTROLLER_DEBUG
        fprintf(stdout, "[INFO] Step query %d finished, acknowledge HB.\n", outputTableID);
#endif
        _network->acknowledgeHonestBrokerOfRecentFinish(outputTableID);
    } else {
#ifdef CONTROLLER_DEBUG
        fprintf(stdout, "[INFO] Sending ObliviousTupleTable %d to %d.\n", outputTableID, dst_machine_id);
#endif
        _network->sendObliviousTupleTable(dst_machine_id, table, size);
#ifdef CONTROLLER_DEBUG
        fprintf(stdout, "[INFO] ObliviousTupleTable sent to host %d.\n", dst_machine_id);
#endif
    }
};

void OutsideController::depositTable(MachineID machineID, TableID tableID, pos_vdb partitionID, unsigned char *table, size_vdb size) {
#ifdef CONTROLLER_DEBUG
    fprintf(stdout, "[INFO] Deposit table [%d %d %d] to external storage.\n", machineID, tableID, partitionID);
#endif
    std::unique_lock<std::mutex> guard(_registryLock);
    if (_tableRequestRegistry[machineID][tableID].find(partitionID) == _tableRequestRegistry[machineID][tableID].end()) {
        _tableRepository[machineID][tableID][partitionID] = { size, std::shared_ptr<unsigned char>(table, [](unsigned char * p){delete[] p;})};
    }
    memcpy(_tableRepository[machineID][tableID][partitionID].data.get(), table, size);
//    else {
//        std::map<std::shared_ptr<unsigned char>, size_vdb> dispatches;
//        for (auto p : _tableRepository[machineID][tableID][partitionID]) {
//            dispatches.emplace(p);
//            _tableRequestRegistry[machineID][tableID][partitionID]--;
//        }
//        if (_tableRequestRegistry[machineID][tableID][partitionID];
//        delete guard;
//        for (auto p : dispatches) {
//            DeliveryObliviousTupleTableIntoEnclave(_enclaveRegistry[0], _localMachineID, p.second, p.first.get());
//        }
//    }
};

void OutsideController::retrieveTableIntoEnclave(int *ret, int machine_id, int op_id, int partition_id, unsigned char *data, int len) {
    if (_tableRepository[machine_id][op_id].find(partition_id) != _tableRepository[machine_id][op_id].end()) {
        memcpy(data, _tableRepository[machine_id][op_id][partition_id].data.get(), len);
        *ret = 0;
    } else {
        *ret = -1;
    }

}

//void OutsideController::requestTableToEnterEnclave(TableID tableID, size_vdb count) {
//#ifdef CONTROLLER_DEBUG
//    fprintf(stdout, "[INFO] Requesting table %d to enter enclave.\n", tableID);
//#endif
//    auto guard = new std::unique_lock<std::mutex>(_registryLock);
//    if (_tableRepository.find(tableID) != _tableRepository.end()) {
//        std::map<std::shared_ptr<unsigned char>, size_vdb> dispatches;
//        for (auto p : _tableRepository[tableID]) {
//            dispatches.emplace(p);
//        }
//        _tableRepository.erase(tableID);
//        delete guard;
//        for (auto p : dispatches) {
//            DeliveryObliviousTupleTableIntoEnclave(_enclaveRegistry[0], _localMachineID, p.second, p.first.get());
//        }
//    } else {
//        _tableRequestRegistry.emplace(tableID, count);
//        delete guard;
//    }
//};

void OutsideController::queryLocalPostgreAndSendIntoEnclave(type::RecordSchema & schema, const char * query, int isFinalQuery) {
#ifdef CONTROLLER_DEBUG
    fprintf(stdout, "[INFO] Received PostgreSQL query \"%s\".\n", query);
#endif
    db::obdata::ObliviousTupleTable out;
    if ( _postgres.singleQueryWithObliviousTupleTable(out, schema, query) == RETURN_SUCCESS) {
        size_vdb size = out.serializationSize();
        unsigned char data[size];
        out.serializeTo(data);
        if (isFinalQuery == FINAL_QUERY) {
#ifdef CONTROLLER_DEBUG
            fprintf(stdout, "[INFO] Delivering PostgreSQL output to HonestBroker.\n");
#endif
            _network->sendObliviousTupleTable(_honestBrokerID, data, size);
        } else {
#ifdef CONTROLLER_DEBUG
            fprintf(stdout, "[INFO] Delivering PostgreSQL output into enclave.\n");
#endif
            DeliveryObliviousTupleTableIntoEnclave(_enclaveRegistry[0], _localMachineID, size, data);
#ifdef CONTROLLER_DEBUG
            fprintf(stdout, "[INFO] Acknowledging HB of completion PostgreSQL query.\n");
#endif
            _network->acknowledgeHonestBrokerOfRecentFinish(schema.getTableID());
        }
    } else {
        fprintf(stderr, "[FAILURE] Cannot query postgres to create table\n");
    };
#ifdef CONTROLLER_DEBUG
    fprintf(stdout, "[INFO] PostgreSQL query concluded.\n");
#endif
};

//---------------------- HONEST BROKER UTILITIES -----------------------
#ifdef HONEST_BROKER_INSTANCE
GenericReturnStatus OutsideController::SendQuery(MachineID dst_machine_id, TableID queryID, unsigned char *query,
                                                 size_vdb size,
                                                 int isFinalQuery) {
#ifdef CONTROLLER_DEBUG
    fprintf(stdout, "[INFO] Honest Broker sending secure query %d to host %d. isFinal? %d\n", queryID, dst_machine_id, isFinalQuery);
#endif
    auto guard = new std::unique_lock<std::mutex>(_instance->_registryLock);
    if (_instance->_waitForCount.find(queryID) == _instance->_waitForCount.end()) {
        _instance->_waitForCount[queryID] = (size_vdb)_instance->_machineRegistry.size() - 1;
        _instance->_isTupleTableInitialized = false;
    }
    delete guard;
    if (_instance->_network->sendQuery(dst_machine_id, query, size, isFinalQuery == FINAL_QUERY ? TransmissionType::OneFinalQuery : TransmissionType::OneStepQuery)
        != RETURN_SUCCESS) {
        fprintf(stderr, "[FAILURE] Network failed and query %d was NOT sent\n", queryID);
        guard = new std::unique_lock<std::mutex>(_instance->_registryLock);
        _instance->_waitForCount.erase(queryID);
        delete guard;
        return GenericReturnStatus ::FatalError;
    }
#ifdef CONTROLLER_DEBUG
    fprintf(stdout, "[INFO] Secure query %d delivered to %d.\n", queryID, dst_machine_id);
#endif
    return GenericReturnStatus ::Ok;
};

GenericReturnStatus OutsideController::SendPostgreSQLQuery(MachineID dst_machine_id, TableID queryOutputTableID,
                                                           type::RecordSchema &psqlSchema, const char *sqlQuery,
                                                           int isFinalQuery) {
#ifdef CONTROLLER_DEBUG
    fprintf(stdout, "[INFO] Honest Broker sending PostgreSQL query to host %d; output TableID: %d.\n", dst_machine_id, queryOutputTableID);
#endif
    auto guard = new std::unique_lock<std::mutex>(_instance->_registryLock);
    if (_instance->_waitForCount.find(queryOutputTableID) == _instance->_waitForCount.end()) {
        _instance->_waitForCount[queryOutputTableID] = (size_vdb)_instance->_machineRegistry.size() - 1;
    }
    delete guard;

    size_vdb psqlSchemaSize = psqlSchema.serializationSize();
    size_vdb querySize = (size_vdb)strlen(sqlQuery) + 1;
    size_vdb totalSize = psqlSchemaSize + querySize;
    unsigned char psqlQuery[totalSize];
    psqlSchema.serializeTo(psqlQuery);
    memcpy(psqlQuery + psqlSchemaSize, sqlQuery, querySize);

    if (_instance->_network->sendQuery(dst_machine_id, psqlQuery, totalSize, isFinalQuery == FINAL_QUERY ? TransmissionType::OneFinalPostgreSQLQuery : TransmissionType::OneStepPostgreSQLQuery)
        != RETURN_SUCCESS) {
        fprintf(stderr, "[FAILURE] Network failed and query %d was NOT sent\n", queryOutputTableID);
        guard = new std::unique_lock<std::mutex>(_instance->_registryLock);
        _instance->_waitForCount.erase(queryOutputTableID);
        delete guard;
        return GenericReturnStatus ::FatalError;
    }
#ifdef CONTROLLER_DEBUG
    fprintf(stdout, "[INFO] PostgreSQL query %d delivered to %d.\n", queryOutputTableID, dst_machine_id);
#endif
    return GenericReturnStatus ::Ok;
};

int OutsideController::receiveOutputResult(MachineID src_machine_id, db::obdata::ObliviousTupleTable table) {
#ifdef CONTROLLER_DEBUG
    fprintf(stdout, "[INFO] Honest broker receiving final result submission from host %d.\n", src_machine_id);
#endif
    TableID tableID = table.getTableID();
    std::unique_lock<std::mutex> merge_guard(_mergeLock);
    if (!_isTupleTableInitialized) {
        _tupleTableResult = table;
        _isTupleTableInitialized = true;
    } else {
        _tupleTableResult.append(table);
    }
    _waitForCount[tableID]--;
    if (_waitForCount[tableID] == 0) {
        _waitForCount.erase(tableID);
#ifdef CONTROLLER_DEBUG
        fprintf(stdout, "[INFO] All hosts submitted their results.\n");
#endif
    } else {
#ifdef CONTROLLER_DEBUG
        fprintf(stdout, "[INFO] waiting for %d more hosts to submit their results for query %d.\n", _waitForCount[tableID], tableID);
#endif
    }
    return 0;
};

void OutsideController::receiveCompletionAck(MachineID src_machineid, TableID tableID) {
#ifdef CONTROLLER_DEBUG
    fprintf(stdout, "[INFO] Host %d acknowledges query completion for %d.\n", src_machineid, tableID);
#endif
    std::unique_lock<std::mutex> merge_guard(_mergeLock);
    _waitForCount[tableID]--;
    if (_waitForCount[tableID] == 0) {
        _waitForCount.erase(tableID);
#ifdef CONTROLLER_DEBUG
        fprintf(stdout, "[INFO] All hosts acknowledges completion of query %d.\n", tableID);
#endif
    }
};

db::obdata::ObliviousTupleTable & OutsideController::GetResultObliviousTupleTable() {
    return _instance->_tupleTableResult;
};
#endif

GenericReturnStatus OutsideController::HBDataEntry(time_t queryTime, int queryNumber, size_vdb k, std::string tag,
                                                   double value_w_double_type, std::string comment) {
    return _instance->honestBrokerDataEntry(queryTime, queryNumber, k, tag, value_w_double_type, comment);
}

GenericReturnStatus
OutsideController::honestBrokerDataEntry(time_t queryTime, int queryNumber, size_vdb k, std::string tag, double value_w_double_type,
                                         std::string comment) {
    // FIXME get the time stamp done right
    std::string log = std::string("INSERT INTO vaultdb_performance VALUES ( to_timestamp(") +
            std::to_string((unsigned long)queryTime + 7200) + std::string(")::timestamp, ") +
            std::to_string(queryNumber) + std::string(", ") +
            std::to_string(k) + std::string(", '") +
            tag + std::string("', ") +
            std::to_string(value_w_double_type) + std::string(", '") +
            comment + std::string("')");
    if (_postgres.singleQueryWithNoResult(log.c_str()) != RETURN_SUCCESS) {
        fprintf(stderr, "[FAILURE] Cannot add log entry. Query: '%s'\n", log.c_str());
        return GenericReturnStatus::FatalError;
    };
#ifdef CONTROLLER_DEBUG
    fprintf(stdout, "[INFO] Honest broker logged performance result.\n");
#endif
    return GenericReturnStatus::Ok;
};
