#include "utilities/TuplePrinter.h"
#include <string>

std::string TuplePrinter::convert(const type::RecordSchema * schema, db::obdata::ObliviousTuple & tuple) {
    std::string ret;
    for (pos_vdb i = 0, size = schema->size(); i < size; i++) {
        if (i > 0) {
            ret += std::string(",");
        }
        switch ((*schema)[i].type) {
            case type::FieldDataType::Int:
            case type::FieldDataType::Fixchar:
                ret += tuple[i].toString();
                break;
            case type::FieldDataType::TimestampNoZone: {
                auto vecBuffer = tuple[i].convertToUnsignedCharVector();
                unsigned char valueBuffer[sizeof(time_t)];
                std::copy(vecBuffer.begin(), vecBuffer.end(), valueBuffer);
                tm * t = gmtime((time_t*)valueBuffer);
                t->tm_isdst = 0;
                char buffer[20];
                sprintf(buffer,"%04d-%02d-%02d %02d:%02d:%02d", t->tm_year + 1900,t->tm_mon + 1,t->tm_mday,
                        t->tm_hour,t->tm_min,t->tm_sec);
                ret += std::string(buffer);
                break;
            }
            default:
                ret += std::string("(Unknown type code: ") + std::to_string((int) (*schema)[i].type) + std::string(")");
        }
    }
    return ret;
};