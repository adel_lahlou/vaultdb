#include "utilities/NetworkTransmissionHandler.h"

// compilation:
// gcc -Wall -o server server.c -L/usr/lib -lssl -lcrypto

#include <in_sgx/serialization/ObliviousSerializer.h>
#include <utilities/ConfigManager.h>
#include "shared/Definitions.h"
//TODO: fix this import. 
#include "../../rpclib/include/vdbrpclib.h"

#ifndef TRANSMISSION_DEBUG
//#define TRANSMISSION_DEBUG
#endif

//------------------------- PRIVATE CONSTRUCTOR -------------------------

NetworkTransmissionHandler::NetworkTransmissionHandler(OutsideController * localConstroller, MachineID localID, std::string hostNamePort)
        : _localMachineID(localID),
          _honestBrokerID(localConstroller->_honestBrokerID),
          _localConstroller(localConstroller)
{
    _runningServerProcess = std::unique_ptr<std::thread>(new std::thread(RunServer, this, hostNamePort));
    registerMachine(_localMachineID, hostNamePort);
}

NetworkTransmissionHandler::~NetworkTransmissionHandler() {
};


//-------------------------- SERVICE UTILITIES --------------------------

//TODO: this should be a flag somewhere!!
void NetworkTransmissionHandler::registerMachine(MachineID machineID, std::string hostNamePort) {
    std::unique_lock<std::mutex> guard(_registryLock);
    _registry.emplace(machineID, hostNamePort);
};

void NetworkTransmissionHandler::registerHonestBroker(MachineID machineID, std::string hostNamePort) {
    _honestBrokerID = machineID;
    std::unique_lock<std::mutex> guard(_registryLock);
    _registry.emplace(machineID, hostNamePort);
};

//--------------------------- SERVER UTILITIES --------------------------

void NetworkTransmissionHandler::handlePostgresIntoEnclaveQuery(unsigned char *inputBuffer, size_vdb size, int isFinalQuery) {

    type::RecordSchema schema = type::RecordSchema::deserializeFrom(inputBuffer);
    size_vdb schemaSize = schema.serializationSize();
#ifdef TRANSMISSION_DEBUG
    fprintf(stdout, "[INFO] received request for querying postgres and ingest the result\n");
#endif
    _localConstroller->queryLocalPostgreAndSendIntoEnclave(schema, (char *) inputBuffer + schemaSize, isFinalQuery);
}

void NetworkTransmissionHandler::serverProvideService(MachineID srcMachineID, uint8_t type, unsigned char * buffer, unsigned int size) {
        // FIXME implement ObliviousPagedTable etc.
        switch ((TransmissionType)type) {
            case TransmissionType::OneStepQuery:
                handleNewQuery(buffer, (size_vdb)size, STEP_QUERY);
                break;
            case TransmissionType::OneFinalQuery:
                handleNewQuery(buffer, (size_vdb)size, FINAL_QUERY);
                break;
            case TransmissionType::WholeObliviousTupleTable:
                handleWholeObliviousTupleTable(srcMachineID, buffer, (size_vdb)size);
                break;
            case TransmissionType::OneStepPostgreSQLQuery:
                handlePostgresIntoEnclaveQuery(buffer, (size_vdb)size, STEP_QUERY);
                break;
            case TransmissionType::OneFinalPostgreSQLQuery:
                handlePostgresIntoEnclaveQuery(buffer, (size_vdb)size, FINAL_QUERY);
                break;
            case TransmissionType::ResultObliviousTupleTable:
                handleResultObliviousTupleTable(srcMachineID, buffer, (size_vdb)size);
                break;
            case TransmissionType::AcknowledgeQueryComplete:
                handleCompletionAck(srcMachineID, *(TableID*)buffer);
                break;
            default:
                fprintf(stderr, "[FAILURE] Server SSL_read cannot handle transmission type; type: %d\n", (int) type);
                break;
    }
}

void NetworkTransmissionHandler::stopServer() {
};


//-------------------------- CLIENT UTILITIES --------------------------

int NetworkTransmissionHandler::startClientTransmission(TransmissionType type, std::string hostNamePort,
                                                        unsigned char *data, size_vdb size) {
    std::vector<uint8_t> buffer;
    buffer.insert(buffer.end(), data, data+size);
    return client_send(hostNamePort, _localMachineID, (uint8_t)type, buffer);
};

int NetworkTransmissionHandler::sendObliviousTupleTable(MachineID dst_machine_id, unsigned char *table, size_vdb size) {

    auto * guard = new std::unique_lock<std::mutex>(_registryLock);
    std::string & hostNamePort = _registry.at(dst_machine_id);
    delete guard;

    if (dst_machine_id == _honestBrokerID) {
        return startClientTransmission(TransmissionType::ResultObliviousTupleTable, hostNamePort, table, size);
    } else {
        return startClientTransmission(TransmissionType::WholeObliviousTupleTable, hostNamePort, table, size);
    }
};

void NetworkTransmissionHandler::acknowledgeHonestBrokerOfRecentFinish(TableID opID) {
    auto * guard = new std::unique_lock<std::mutex>(_registryLock);
    std::string & hostNamePort = _registry.at(_honestBrokerID);
    delete guard;
    unsigned char data[sizeof(TableID)];
    *(TableID*) data = opID;
    startClientTransmission(TransmissionType::AcknowledgeQueryComplete, hostNamePort, data, sizeof(TableID));
};

void NetworkTransmissionHandler::handleNewQuery(unsigned char *inputBuffer, size_vdb size, int isFinalQuery) {
#ifdef TRANSMISSION_DEBUG
    fprintf(stdout, "[INFO] received request for a new secure query\n");
#endif
    _localConstroller->receiveSecureNewQuery(inputBuffer, size, isFinalQuery);
};

void NetworkTransmissionHandler::handleWholeObliviousTupleTable(MachineID src_machine_id, unsigned char * inputBuffer, size_vdb size) {
#ifdef TRANSMISSION_DEBUG
    fprintf(stdout, "[INFO] Machine %d received request for ingesting an ObliviousTupleTable, table %d, from %d\n", _localMachineID, *(TableID*)inputBuffer, src_machine_id);
#endif
    _localConstroller->receiveObliviousTupleTable(src_machine_id, inputBuffer, size);
};

//---------------------- HONEST BROKER UTILITIES -----------------------
#ifdef HONEST_BROKER_INSTANCE
int
NetworkTransmissionHandler::sendQuery(MachineID dst_machine_id, unsigned char *query, size_vdb size,
                                      TransmissionType queryType) {
    auto * guard = new std::unique_lock<std::mutex>(_registryLock);
    std::string & hostNamePort = _registry.at(dst_machine_id);
    delete guard;
    return startClientTransmission(queryType, hostNamePort, query, size);

};

void NetworkTransmissionHandler::handleResultObliviousTupleTable(MachineID src_machine_id, unsigned char * inputBuffer, size_vdb size) {
#ifdef TRANSMISSION_DEBUG
    fprintf(stdout, "[INFO] received request for collecting an ObliviousTupleTable final output\n");
#endif
    _localConstroller->receiveOutputResult(
            src_machine_id,
            db::obdata::ObliviousSerializer::deserializeObliviousTupleTable(inputBuffer,size));

};

void NetworkTransmissionHandler::handleCompletionAck(MachineID src_machine_id, TableID tableID) {
    _localConstroller->receiveCompletionAck(src_machine_id, tableID);
};
#endif
