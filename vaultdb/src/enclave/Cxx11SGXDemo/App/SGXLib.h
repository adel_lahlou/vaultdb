#ifndef _SGXLIB_H_
#define _SGXLIB_H_
#include <stdint.h>

int sgx_start_enclave(const char * enclave_path);

void sgx_create_insgxcontroller(uint64_t eid, int local_id);

void sgx_tuple_table_delivery(uint64_t eid, unsigned int machine_id, 
	unsigned char * data, int len);

void sgx_query_delivery(uint64_t eid, unsigned char * data, int len, int isFinalQuery);

void sgx_register_remote_machine(uint64_t eid, int machine_id); 

void sgx_register_honest_broker(uint64_t eid, int machine_id); 

void sgx_app_destroy_enclave(uint64_t eid);

void ocall_transmit_oblivious_table(unsigned char * data, int len, int dst_machine_id);

void ocall_deposit_table_temporarily(int machine_id, int table_id, int partition_id, unsigned char * data, int len);

void ocall_fetch_table_from_local(int* ret, int machine_id, int op_id, int partition_id, unsigned char * data, int len);

#endif

