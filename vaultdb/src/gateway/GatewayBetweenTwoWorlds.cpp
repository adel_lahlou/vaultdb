#include <in_sgx/utilities/InSGXController.h>
#include <cstring>
#include <utilities/OutsideController.h>
#include "gateway/GatewayBetweenTwoWorlds.h"
#include <SGXLib.h>
#include <gflags/gflags.h>


static uint64_t _eid;
DEFINE_string(enclave_path, "enclave.signed.so", "path for enclave.signed.so file");


// From inside of an enclave
int InitializeInSGXController(MachineID localID, uint64_t enclaveID) {
    //this will call initialize_enclave, and keep track of the enclave ID
    _eid = sgx_start_enclave(FLAGS_enclave_path.c_str());
    sgx_create_insgxcontroller(_eid, localID);
};

//
int LastMileObliviousTupleTableDeliveryIntoEnclave(MachineID src_machine_id, size_vdb size, unsigned char * data) {
    sgx_tuple_table_delivery(_eid, src_machine_id, data, size); 
};

int LastMileQueryDeliveryIntoEnclave(unsigned char * data, size_vdb size, int isFinalQuery) {
    sgx_query_delivery(_eid, data, size, isFinalQuery); 
};

int RegisterRemoteMachineInSGX(MachineID machineID) {
    sgx_register_remote_machine(_eid, machineID);
};

int RegisterHonestBrokerInSGX(MachineID machineID) {
    sgx_register_honest_broker(_eid, machineID);
};

/*
int PassOnObliviousTupleTableFromEnclave(MachineID localMachineID, MachineID dst_machine_id, TableID opID, size_vdb size, unsigned char *data, int isFinalQuery) {
    // FIXME receives an OCALL from inside
    _outsideControllers[localMachineID]->sendObliviousTupleTable(dst_machine_id, opID, data, size, isFinalQuery);
};

*/

// From outside of an enclave
int DeliverInSGXControllerConstructionRequest(MachineID localID, uint64_t enclaveID) {
    InitializeInSGXController(localID, enclaveID);
};

int DeliverQueryIntoEnclave(uint64_t enclaveID, unsigned char * data, size_vdb size, int isFinalQuery) {
    LastMileQueryDeliveryIntoEnclave( data, size, isFinalQuery);
};

int DeliveryObliviousTupleTableIntoEnclave(uint64_t enclaveID, MachineID src_machine_id, size_vdb size, unsigned char * data) {
    LastMileObliviousTupleTableDeliveryIntoEnclave(src_machine_id, size, data);
};

int DeliverRequestToRegisterRemoteMachine(uint64_t enclaveID, MachineID machineID) {
    RegisterRemoteMachineInSGX(machineID);
};

int DeliverRequestToRegisterHonestBroker(uint64_t enclaveID, MachineID machineID) {
    RegisterHonestBrokerInSGX(machineID);
};

