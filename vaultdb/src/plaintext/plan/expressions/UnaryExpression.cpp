#include "plaintext/plan/expressions/UnaryExpression.h"

using namespace plan::expressions;
using namespace db::data::field;


bool LogicalExpression::isAggregateFunction() const
{
    return false;
}


And::And()
{}

And::And(Expression lhs, Expression  rhs)
        : _lhs(lhs), _rhs(rhs)
{}

And::~And()
{}

void And::setTuple(TupleBox tp)
{
    _lhs.setTuple(tp);
    _rhs.setTuple(tp);
}

Field And::evaluate() const
{
    return Field::makeBoolField(_lhs.evaluate().isTruthy() && _rhs.evaluate().isTruthy());
}



Or::Or()
{}

Or::Or(Expression lhs, Expression  rhs)
        : _lhs(lhs), _rhs(rhs)
{}

Or::~Or()
{}

void Or::setTuple(TupleBox tp)
{
    _lhs.setTuple(tp);
    _rhs.setTuple(tp);
}

Field Or::evaluate() const
{
    return Field::makeBoolField(_lhs.evaluate().isTruthy() || _rhs.evaluate().isTruthy());
}



Not::Not()
{}

Not::Not(Expression exp)
        : _exp(exp)
{}

Not::~Not()
{}

void Not::setTuple(TupleBox tp)
{
    _exp.setTuple(tp);
}

Field Not::evaluate() const
{
    return Field::makeBoolField(!_exp.evaluate().isTruthy());
}
