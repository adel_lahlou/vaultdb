#include "plaintext/plan/expressions/Arithmetic.h"

using namespace plan::expressions;
using namespace db::data::field;

bool ArithmeticExpression::isAggregateFunction() const
{
    return false;
}


Add::Add()
{}

Add::Add(Expression lhs, Expression rhs)
        : _lhs(lhs), _rhs(rhs)
{}

Add::~Add()
{}

void Add::setTuple(TupleBox tp)
{
    _lhs.setTuple(tp);
    _rhs.setTuple(tp);
}

Field Add::evaluate() const
{
    return _lhs.evaluate() + _rhs.evaluate();
}



Sub::Sub()
{}

Sub::Sub(Expression lhs, Expression  rhs)
        : _lhs(lhs), _rhs(rhs)
{}

Sub::~Sub()
{}

void Sub::setTuple(TupleBox tp)
{
    _lhs.setTuple(tp);
    _rhs.setTuple(tp);
}

Field Sub::evaluate() const
{
    return _lhs.evaluate() - _rhs.evaluate();
}



Mul::Mul()
{}

Mul::Mul(Expression lhs, Expression  rhs)
        : _lhs(lhs), _rhs(rhs)
{}

Mul::~Mul()
{}

void Mul::setTuple(TupleBox tp)
{
    _lhs.setTuple(tp);
    _rhs.setTuple(tp);
}

Field Mul::evaluate() const
{
    return _lhs.evaluate() * _rhs.evaluate();
}



Div::Div()
{}

Div::Div(Expression lhs, Expression  rhs)
        : _lhs(lhs), _rhs(rhs)
{}

Div::~Div()
{}

void Div::setTuple(TupleBox tp)
{
    _lhs.setTuple(tp);
    _rhs.setTuple(tp);
}

Field Div::evaluate() const
{
    return _lhs.evaluate() / _rhs.evaluate();
}