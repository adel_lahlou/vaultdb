#include "plaintext/plan/expressions/Values.h"

using namespace plan::expressions;
using namespace db::data::field;

// TODO: Nothing stops a column from having a nullptr for tp
bool ValueExpression::isAggregateFunction() const
{
    return false;
}


Literal::Literal()
        : value(Field::makeIntField(0))
{}

Literal::Literal(Field v)
        : value(v)
{}

void Literal::setTuple(TupleBox tp)
{}

Field Literal::evaluate() const
{
    return value;
}




Column::Column()
        : col(-1), curTp(nullptr)
{}

Column::Column(size_vdb colNum)
        : col(colNum), curTp(nullptr)
{}

void Column::setTuple(TupleBox tp)
{
    curTp = tp;
}


Field Column::evaluate() const
{
    return (*curTp)[col];
}
