#include "plaintext/plan/expressions/AggregateExpressionList.h"

using namespace plan::expressions;
using namespace db::data;

AggregateExpressionList::AggregateExpressionList()
{}

AggregateExpressionList::AggregateExpressionList(const std::vector<AggregateFunction>& aggs)
        : expressions(aggs)
{}

AggregateExpressionList::AggregateExpressionList(std::initializer_list<AggregateFunction> aggs)
        : expressions(aggs)
{}

AggregateExpressionList::~AggregateExpressionList()
{}


bool AggregateExpressionList::isAllTruthy() const
{
    for(auto& e : expressions) {
        if (!e.evaluate().isTruthy())
            return false;
    }

    return true;
}

bool AggregateExpressionList::isAnyTruthy() const
{
    for(auto& e : expressions) {
        if (e.evaluate().isTruthy())
            return true;
    }

    return false;
}

int AggregateExpressionList::size() const
{
    return expressions.size();
}

void AggregateExpressionList::setTuple(TupleBox t)
{
    for(auto &e : expressions){
        e.setTuple(t);
    }
}

void AggregateExpressionList::update()
{
    for(auto &e : expressions){
        e.update();
    }
}

void AggregateExpressionList::reset()
{
    for(auto &e : expressions){
        e.reset();
    }
}

Tuple AggregateExpressionList::evaluate() const
{
    Tuple t;
    for(auto& e : expressions) {
        t.push_back(e.evaluate());
    }

    return t;
}

void AggregateExpressionList::push_back(AggregateFunction e)
{
    expressions.push_back(e);
}


Expression& AggregateExpressionList::operator[](size_vdb pos)
{
    return expressions[pos];
}


const Expression& AggregateExpressionList::operator[](size_vdb pos) const
{
    return expressions[pos];
}
