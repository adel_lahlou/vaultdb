#include "plaintext/plan/expressions/Aggregates.h"

using namespace plan::expressions;
using namespace db::data::field;

AggregateFunction::AggregateFunction()
{}

// TODO: think of a safer/verbose way to do this
AggregateFunction::AggregateFunction(const Expression &other)
        : Expression(other)
{}

bool AggregateFunction::isAggregateFunction() const
{
    return true;
}

void AggregateFunction::update()
{
    std::static_pointer_cast<AggregateFunction>(rep)->update();
}

void AggregateFunction::reset()
{
    std::static_pointer_cast<AggregateFunction>(rep)->reset();
}

Sum::Sum(size_vdb colNum)
        : column(colNum)
{}

Sum::Sum(Column col)
        : column(col)
{}

Sum::~Sum()
{}

void Sum::setTuple(TupleBox tp)
{
    column.setTuple(tp);
}

void Sum::update()
{
    sum = sum + column.evaluate();
}

void Sum::reset()
{
    sum = Field::makeIntField(0);
}

db::data::field::Field Sum::evaluate() const
{
    return sum;
}




Count::Count(size_vdb colNum)
        : column(colNum)
{}

Count::Count(Column col)
        : column(col)
{}

Count::~Count()
{}

void Count::setTuple(TupleBox tp)
{
    column.setTuple(tp);
}

void Count::update()
{
    count++;
}

void Count::reset()
{
    count = 0;
}

db::data::field::Field Count::evaluate() const
{
    return Field::makeIntField(count);
}
