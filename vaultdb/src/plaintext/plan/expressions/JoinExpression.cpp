#include "plaintext/plan/expressions/JoinExpression.h"

using namespace plan::expressions;
using namespace db::data::field;

JoinExpression::JoinExpression(Expression lhs, Expression rhs)
        : _lhs(lhs), _rhs(rhs)
{
    _pred = Expression::makeEq(_lhs, _rhs);
}

JoinExpression::~JoinExpression()
{}


void JoinExpression::setTuple(TupleBox tp)
{}

void JoinExpression::setLeftTuple(TupleBox ltp)
{
    _lhs.setTuple(ltp);
}

void JoinExpression::setRightTuple(TupleBox rtp)
{
    _rhs.setTuple(rtp);
}


db::data::field::Field JoinExpression::evaluate() const
{
    return _pred.evaluate();
}