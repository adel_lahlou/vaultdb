#include "plaintext/plan/expressions/Comparison.h"

using namespace plan::expressions;
using namespace db::data::field;

bool ComparisonExpression::isAggregateFunction() const
{
    return false;
}


Eq::Eq()
{}

Eq::Eq(Expression lhs, Expression  rhs)
        : _lhs(lhs), _rhs(rhs)
{}

Eq::~Eq()
{}

void Eq::setTuple(TupleBox tp)
{
    _lhs.setTuple(tp);
    _rhs.setTuple(tp);
}

Field Eq::evaluate() const
{
    return Field::makeBoolField(_lhs.evaluate() == _rhs.evaluate());
}



NEq::NEq()
{}

NEq::NEq(Expression lhs, Expression  rhs)
        : _lhs(lhs), _rhs(rhs)
{}

NEq::~NEq()
{}

void NEq::setTuple(TupleBox tp)
{
    _lhs.setTuple(tp);
    _rhs.setTuple(tp);
}

Field NEq::evaluate() const
{
    return Field::makeBoolField(_lhs.evaluate() != _rhs.evaluate());
}



GT::GT()
{}

GT::GT(Expression lhs, Expression  rhs)
        : _lhs(lhs), _rhs(rhs)
{}

GT::~GT()
{}

void GT::setTuple(TupleBox tp)
{
    _lhs.setTuple(tp);
    _rhs.setTuple(tp);
}

Field GT::evaluate() const
{
    return Field::makeBoolField(_lhs.evaluate() > _rhs.evaluate());
}



LT::LT()
{}

LT::LT(Expression lhs, Expression  rhs)
        : _lhs(lhs), _rhs(rhs)
{}

LT::~LT()
{}

void LT::setTuple(TupleBox tp)
{
    _lhs.setTuple(tp);
    _rhs.setTuple(tp);
}

Field LT::evaluate() const
{
    return Field::makeBoolField(_lhs.evaluate() < _rhs.evaluate());
}



GTE::GTE()
{}

GTE::GTE(Expression lhs, Expression  rhs)
        : _lhs(lhs), _rhs(rhs)
{}

GTE::~GTE()
{}

void GTE::setTuple(TupleBox tp)
{
    _lhs.setTuple(tp);
    _rhs.setTuple(tp);
}

Field GTE::evaluate() const
{
    return Field::makeBoolField(_lhs.evaluate() >= _rhs.evaluate());
}



LTE::LTE()
{}

LTE::LTE(Expression lhs, Expression  rhs)
        : _lhs(lhs), _rhs(rhs)
{}

LTE::~LTE()
{}

void LTE::setTuple(TupleBox tp)
{
    _lhs.setTuple(tp);
    _rhs.setTuple(tp);
}

Field LTE::evaluate() const
{
    return Field::makeBoolField(_lhs.evaluate() <= _rhs.evaluate());
}