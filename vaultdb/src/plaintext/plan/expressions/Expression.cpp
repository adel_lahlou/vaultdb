#include "plaintext/plan/expressions/Expression.h"
#include "plaintext/plan/expressions/Values.h"
#include "plaintext/plan/expressions/Arithmetic.h"
#include "plaintext/plan/expressions/Comparison.h"
#include "plaintext/plan/expressions/UnaryExpression.h"
#include "plaintext/plan/expressions/JoinExpression.h"
#include "plaintext/plan/expressions/Aggregates.h"

using namespace plan::expressions;
using namespace db::data::field;
using namespace db::data;

// TODO: Add null concept of field

Expression::Expression()
        : rep(nullptr)
{}

Expression::Expression(BaseConstructor)
        :rep(nullptr)
{}

Expression& Expression::operator=(const Expression &rhs)
{
    Expression temp(rhs);
    this->swap(temp);
    return *this;
}

Expression::Expression(const Expression& e)
        : rep(e.rep)
{}

Expression::~Expression()
{}

void Expression::swap(Expression& e) throw()
{
    std::swap(this->rep, e.rep);
}

void Expression::setTuple(TupleBox tp)
{
    rep->setTuple(tp);
}

void Expression::redefine(ExpressionPtr p)
{
    rep = p;
}

bool Expression::isAggregateFunction() const
{
    return rep->isAggregateFunction();
}

db::data::field::Field Expression::evaluate() const
{
    return rep->evaluate();
}

type::SchemaColumn Expression::outputDatatype() const
{
    return rep->evaluate().dataType();
}

Expression Expression::makeLiteral(Field f)
{
    Expression e;
    ExpressionPtr ep(new Literal(f));
    e.redefine(ep);
    return e;
}

Expression Expression::makeColumn(size_vdb colNum)
{
    Expression e;
    ExpressionPtr ep(new Column(colNum));
    e.redefine(ep);
    return e;
}

Expression Expression::makeAdd(Expression lhs, Expression rhs)
{
    Expression e;
    ExpressionPtr ep(new Add(lhs, rhs));
    e.redefine(ep);
    return e;
}


Expression Expression::makeSub(Expression lhs, Expression rhs)
{
    Expression e;
    ExpressionPtr ep(new Sub(lhs, rhs));
    e.redefine(ep);
    return e;
}


Expression Expression::makeMul(Expression lhs, Expression rhs)
{
    Expression e;
    ExpressionPtr ep(new Mul(lhs, rhs));
    e.redefine(ep);
    return e;
}


Expression Expression::makeDiv(Expression lhs, Expression rhs)
{
    Expression e;
    ExpressionPtr ep(new Div(lhs, rhs));
    e.redefine(ep);
    return e;
}


Expression Expression::makeEq(Expression lhs, Expression rhs)
{
    Expression e;
    ExpressionPtr ep(new Eq(lhs, rhs));
    e.redefine(ep);
    return e;
}


Expression Expression::makeNEq(Expression lhs, Expression rhs)
{
    Expression e;
    ExpressionPtr ep(new NEq(lhs, rhs));
    e.redefine(ep);
    return e;
}


Expression Expression::makeLT(Expression lhs, Expression rhs)
{
    Expression e;
    ExpressionPtr ep(new LT(lhs, rhs));
    e.redefine(ep);
    return e;
}


Expression Expression::makeGT(Expression lhs, Expression rhs)
{
    Expression e;
    ExpressionPtr ep(new GT(lhs, rhs));
    e.redefine(ep);
    return e;
}


Expression Expression::makeLTE(Expression lhs, Expression rhs)
{
    Expression e;
    ExpressionPtr ep(new LTE(lhs, rhs));
    e.redefine(ep);
    return e;
}


Expression Expression::makeGTE(Expression lhs, Expression rhs)
{
    Expression e;
    ExpressionPtr ep(new GTE(lhs, rhs));
    e.redefine(ep);
    return e;
}


Expression Expression::makeAnd(Expression lhs, Expression rhs)
{
    Expression e;
    ExpressionPtr ep(new And(lhs, rhs));
    e.redefine(ep);
    return e;
}


Expression Expression::makeOr(Expression lhs, Expression rhs)
{
    Expression e;
    ExpressionPtr ep(new Or(lhs, rhs));
    e.redefine(ep);
    return e;
}


Expression Expression::makeNot(Expression exp)
{
    Expression e;
    ExpressionPtr ep(new Not(exp));
    e.redefine(ep);
    return e;
}


Expression Expression::makeJoin(Expression lhs, Expression rhs)
{
    Expression e;
    ExpressionPtr ep(new JoinExpression(lhs, rhs));
    e.redefine(ep);
    return e;
}


Expression Expression::makeSum(size_vdb colNum)
{
    Expression e;
    ExpressionPtr ep(new Sum(colNum));
    e.redefine(ep);
    return e;
}


Expression Expression::makeCount(size_vdb colNum)
{
    Expression e;
    ExpressionPtr ep(new Count(colNum));
    e.redefine(ep);
    return e;
}

namespace std
{
    template<>
    void swap(plan::expressions::Expression& e1, plan::expressions::Expression& e2)
    {
        e1.swap(e2);
    }
}
