#include "plaintext/plan/expressions/ExpressionList.h"

using namespace plan::expressions;
using namespace db::data;

ExpressionList::ExpressionList()
{}

ExpressionList::ExpressionList(std::vector<Expression> es)
        : expressions(es)
{}

ExpressionList::ExpressionList(std::initializer_list<Expression> es)
        : expressions(es)
{}

ExpressionList::~ExpressionList()
{}


bool ExpressionList::isAllTruthy() const
{
    for(auto& e : expressions) {
        if (!e.evaluate().isTruthy())
            return false;
    }

    return true;
}

bool ExpressionList::isAnyTruthy() const
{
    for(auto& e : expressions) {
        if (e.evaluate().isTruthy())
            return true;
    }

    return false;
}

void ExpressionList::setTuple(TupleBox t)
{
    for(auto &e : expressions){
        e.setTuple(t);
    }
}

int ExpressionList::size() const
{
    return expressions.size();
}

Tuple ExpressionList::evaluate() const
{
    Tuple t;
    for(auto& e : expressions) {
        t.push_back(e.evaluate());
    }

    return t;
}

type::SchemaColumnHolder ExpressionList::outputDatatype() const
{
    type::SchemaColumnHolder dts;
    for(auto& e : expressions) {
        dts.push_back(e.outputDatatype());
    }

    return dts;
}

void ExpressionList::push_back(Expression e)
{
    expressions.push_back(e);
}


Expression& ExpressionList::operator[](size_vdb pos)
{
    return expressions[pos];
}


const Expression& ExpressionList::operator[](size_vdb pos) const
{
    return expressions[pos];
}
