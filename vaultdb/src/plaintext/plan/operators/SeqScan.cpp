#include "plaintext/plan/operators/SeqScan.h"

using namespace plan::operators;
using namespace db::data;

SeqScan::SeqScan(
        TableID opId,
        type::RecordSchema &outSchema,
        OperatorPtr parent,
        db::data::TupleTable inputTable
)
        : Operator(opId, outSchema, false, parent, OperatorPtrList{}),
          _inputTable(inputTable)
{}

SeqScan::~SeqScan() {}


OperatorStatus SeqScan::next()
{
    if(_scanPos >= _inputTable.size()) {
        _status = OperatorStatus::NoMoreTuples;
    } else {
        _currentTuple = _inputTable[_scanPos++];
        _status = OperatorStatus::Ok;
    }

    return _status;
}

void SeqScan::reset()
{
    _scanPos = 0;
    Operator::reset();
}