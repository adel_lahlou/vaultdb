#include "plaintext/plan/operators/NestedJoin.h"

using namespace plan::operators;
using namespace plan::expressions;
using namespace db::data;
using namespace type;


NestedJoin::NestedJoin(
        TableID opId,
        OperatorPtr leftChild,
        OperatorPtr rightChild,
        JoinExpression joinPred
)   : Operator(
        opId,
        RecordSchema::join(leftChild->getSchema(), rightChild->getSchema()),
        false,
        nullptr,
        OperatorPtrList{leftChild, rightChild}
),
      _joinPred(joinPred),
      _leftJoinTuple(new Tuple),
      _rightJoinTuple(new Tuple)
{
    _joinPred.setLeftTuple(_leftJoinTuple);
    _joinPred.setRightTuple(_rightJoinTuple);
    getLeftChild()->next();
}

NestedJoin::~NestedJoin()
{
    delete _leftJoinTuple;
    delete _rightJoinTuple;
}

OperatorPtr NestedJoin::getLeftChild() const
{
    return _children[0];
}

OperatorPtr NestedJoin::getRightChild() const
{
    return _children[1];
}

OperatorStatus NestedJoin::next()
{
    while(getLeftChild()->getCurrentTuple(*_leftJoinTuple) == OperatorStatus::Ok){
        while(getRightChild()->next() == OperatorStatus::Ok) {
            getRightChild()->getCurrentTuple(*_rightJoinTuple);

            if(_joinPred.evaluate().isTruthy()) {
                _currentTuple = Tuple::join(*_leftJoinTuple, *_rightJoinTuple);
                _status = OperatorStatus::Ok;
                return OperatorStatus::Ok;
            }
        }

        getRightChild()->reset();
        getLeftChild()->next();
    }

    _status = OperatorStatus::NoMoreTuples;
    return OperatorStatus::NoMoreTuples;
}

void NestedJoin::reset()
{
    Operator::reset();
    getLeftChild()->next();
}