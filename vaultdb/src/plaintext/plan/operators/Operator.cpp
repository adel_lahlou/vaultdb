#include "plaintext/plan/operators/Operator.h"

using namespace plan::operators;
using namespace type;


Operator::Operator() {}

Operator::Operator(const Operator &o) {}

Operator::Operator(
    TableID opId,
    type::RecordSchema outSchema,
    bool blocking,
    Operator* parent,
    OperatorPtrList children
)       : _operatorId(opId), _outSchema(outSchema), _blocking(blocking), _parent(parent)
{
    _status = OperatorStatus::NotInitialized;

    for (auto c : children){
        c->setParent(this);
    }

    _children = children;
}

Operator::~Operator()
{
    for(auto p : _children)
        delete p;
}


const OperatorStatus Operator::getStatus() const
{
    return _status;
}

const Operator* Operator::getChild(size_vdb child) const
{
    return _children[child];
}

const std::vector<Operator*>& Operator::getChildren() const
{
    return _children;
}

const RecordSchema& Operator::getChildSchema(int child) const
{
    return getChild(child)->getSchema();
}

const RecordSchema& Operator::getSchema() const
{
    return _outSchema;
}

const Operator* Operator::getParent() const
{
    return _parent;
}

bool Operator::isBlocking() const
{
    return _blocking;
}


OperatorStatus Operator::getCurrentTuple(db::data::Tuple &out) const
{
    out = _currentTuple;
    return _status;
}

void Operator::setParent(Operator* p)
{
    _parent = p;
}

void Operator::reset()
{
    for(auto c : _children) {
        c->reset();
    }

    _status = OperatorStatus::NotInitialized;
}

void Operator::encodeOperator(unsigned char *&writeHead)
{
    throw(std::domain_error("Operator does not encode."));
};