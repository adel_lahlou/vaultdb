#include "plaintext/plan/operators/SortedGroup.h"
#include "plaintext/plan/operators/QuickSort.h"
#include "plaintext/data/DataFactory.h"

using namespace plan::operators;
using namespace plan::expressions;
using namespace db::data;


SortedGroup::SortedGroup(
        TableID opId,
        OperatorPtr child,
        ExpressionList exps,
        std::vector<size_vdb> groupBys
)
        : Operator(
            opId,
            child->getSchema(),
            false,
            nullptr,
            OperatorPtrList{
                    new QuickSort(
                        opId+1,
                        child,
                        groupBys,
                        SortDirection::Ascending
                    )}),
          _exps(exps)
{
    Tuple dummy = DataFactory::createDefaultTuple(_outSchema);
    _exps.setTuple(&dummy);
    // TODO: replace with correct tableId
    _outSchema = type::RecordSchema(_outSchema.getTableID(), _exps.outputDatatype());

    _lastAggTuple = new Tuple;
    _curAggTuple = new Tuple;
    _exps.setTuple(_curAggTuple);

    for(auto col : groupBys) {
        Expression lhs = Expression::makeColumn(col);
        Expression rhs = Expression::makeColumn(col);

        lhs.setTuple(_lastAggTuple);
        rhs.setTuple(_curAggTuple);
        Expression eq = Expression::makeEq(lhs, rhs);
        _checkInGroup.push_back(eq);
    }

    for(int i = 0; i < _exps.size(); ++i) {
        if(_exps[i].isAggregateFunction()){
            _aggs.push_back(AggregateFunction(_exps[i]));
        }
    }

    reset();
}

SortedGroup::~SortedGroup() {
    delete _lastAggTuple;
    delete _curAggTuple;
}


OperatorStatus SortedGroup::next()
{
    size_t aggregated = 0;

    if (_firstPull) {
        _firstPull = false;

        if(_children[0]->next() == OperatorStatus::Ok) {
            _children[0]->getCurrentTuple(*_curAggTuple);
            _aggs.update();
            aggregated++;
        } else {
            _status = OperatorStatus::NoMoreTuples;
            return OperatorStatus::NoMoreTuples;
        }
    }

    while( _children[0]->next() == OperatorStatus::Ok) {
        aggregated++;
        *_lastAggTuple = *_curAggTuple;
        _children[0]->getCurrentTuple(*_curAggTuple);

        if(!_checkInGroup.isAllTruthy()) {
            Tuple tmp = *_curAggTuple;
            *_curAggTuple = *_lastAggTuple;
            _currentTuple = _exps.evaluate();
            *_curAggTuple = tmp;
            _aggs.reset();
            _aggs.update();
            _status = OperatorStatus::Ok;
            return OperatorStatus::Ok;
        }

        _aggs.update();
    }

    if(aggregated == 0) {
        _status = OperatorStatus::NoMoreTuples;
        return OperatorStatus::NoMoreTuples;
    }

    _currentTuple = _exps.evaluate();
    _status = OperatorStatus::Ok;
    return OperatorStatus::Ok;
}

void SortedGroup::reset() {
    Operator::reset();
    _aggs.reset();
    _firstPull = true;
}