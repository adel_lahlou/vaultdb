#include "plaintext/plan/operators/Filter.h"

using namespace plan::operators;
using namespace db::data;


Filter::Filter(
        TableID opId,
        type::RecordSchema &outSchema,
        OperatorPtr parent,
        OperatorPtr child,
        plan::expressions::Expression filterPred
)
        : Operator(opId, outSchema, false, parent, OperatorPtrList{child}),
          _filterPred(filterPred)
{
    _filterTuple = new Tuple;
    _filterPred.setTuple(_filterTuple);
}

Filter::~Filter() {
    delete _filterTuple;
}


OperatorStatus Filter::next()
{
    Tuple input;

    while(_children[0]->next() == OperatorStatus::Ok) {
        _children[0]->getCurrentTuple(input);
        *_filterTuple = input;

        if(_filterPred.evaluate().isTruthy()){
            _currentTuple = input;
            _status = OperatorStatus::Ok;
            return _status;
        }
    }
    _status = OperatorStatus::NoMoreTuples;
    return _status;
}

void Filter::reset() {
    Operator::reset();
}