#include "plaintext/plan/operators/QuickSort.h"

using namespace plan::operators;
using namespace db::data;


QuickSort::QuickSort(
        TableID opId,
        OperatorPtr child,
        std::vector<size_vdb> sortIndices,
        SortDirection direction
)       : Operator(opId, child->getSchema(), true, nullptr, OperatorPtrList{child}),
          _sortIndices(sortIndices),
          _direction(direction)
{}

QuickSort::~QuickSort()
{}

OperatorStatus QuickSort::next()
{
    if(_sorted){
        if(_hasTuplesLeft()){
            _currentTuple = _output[_scanPos];
            _scanPos += _direction;
            _status = OperatorStatus::Ok;
        } else {
            _status = OperatorStatus::NoMoreTuples;
        }

        return _status;
    } else {
        _readInChild();
        _quicksort(0, _output.size() - 1);
        reset();
        _status = OperatorStatus::Ok;
        _sorted = true;
        return next();
    }
}

bool QuickSort::_hasTuplesLeft() const
{
    return _scanPos < _output.size();
}

void QuickSort::_readInChild()
{
    Tuple output;

    while(_children[0]->next() == OperatorStatus::Ok){
        _children[0]->getCurrentTuple(output);
        _output.push_back(output);
    }
}

void QuickSort::_quicksort(int low, int high)
{
    if(low >= high) {
        return;
    }

    size_vdb pi = _partition(low, high);
    _quicksort(low, pi - 1);
    _quicksort(pi + 1, high);
}

int QuickSort::_partition(int low, int high)
{
    Tuple pivot = _output[low];

    int left = low + 1;
    int right = high;

    while(right >= left) {
        while(left <= right && Tuple::compare(_output[left], pivot, _sortIndices) <= 0 ){
            left++;
        }

        while(right >= left && Tuple::compare(_output[right], pivot, _sortIndices) >= 0 ){
            right--;
        }

        if(right >= left){
            std::swap(_output[left], _output[right]);
        }
    }

    std::swap(_output[low], _output[right]);
    return right;
}

// TODO: reset does not make you do the work over again, also operator status is reall ok
void QuickSort::reset()
{
    _status = OperatorStatus::NotInitialized;
    _scanPos = (_direction == Ascending) ? 0 : _output.size() - 1;
}