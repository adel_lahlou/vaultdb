#include "plaintext/plan/operators/Union.h"

using namespace db::data::field;
using namespace db::data;
using namespace plan::operators;


Union::Union(TableID opId, OperatorPtrList children)
        : Operator(
            opId,
            children[0]->getSchema(),
            false,
            nullptr,
            children
        )
{}

// TODO: does filter, project need to delete their tuple boxes?
Union::~Union() {}


OperatorStatus Union::next()
{
    for(;_curChild < _children.size(); ++ _curChild) {
        if(getCurrentChild()->next() == OperatorStatus::Ok){
            getCurrentChild()->getCurrentTuple(_currentTuple);
            _status = OperatorStatus::Ok;
            return OperatorStatus::Ok;
        }
    }

    _status = OperatorStatus::NoMoreTuples;
    return OperatorStatus::NoMoreTuples;
}

void Union::reset()
{
    Operator::reset();
    _curChild = 0;
}


OperatorPtr Union::getCurrentChild() const
{
    return _children[_curChild];
}