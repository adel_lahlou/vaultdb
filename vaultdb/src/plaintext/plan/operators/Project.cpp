#include "plaintext/plan/operators/Project.h"
#include "plaintext/data/DataFactory.h"

using namespace plan::expressions;
using namespace plan::operators;
using namespace db::data;

Project::Project(
        TableID opId,
        OperatorPtr child,
        ExpressionList exps
)
        : Operator(opId, child->getSchema(), false, nullptr, OperatorPtrList{child}),
          _exps(exps)
{
    Tuple dummy = DataFactory::createDefaultTuple(_outSchema);
    _exps.setTuple(&dummy);
    _outSchema = type::RecordSchema(_outSchema.getTableID(), _exps.outputDatatype());

    _projectTuple = new Tuple;
    _exps.setTuple(_projectTuple);
}

Project::~Project()
{
    if(_projectTuple != nullptr)
        delete _projectTuple;
}


OperatorStatus Project::next()
{
    if(_children[0]->next() == OperatorStatus::NoMoreTuples) {
        _status = OperatorStatus::NoMoreTuples;
        return OperatorStatus::NoMoreTuples;
    } else {
        _children[0]->getCurrentTuple(*_projectTuple);
        _currentTuple = _exps.evaluate();
        _status = OperatorStatus::Ok;
        return OperatorStatus::Ok;
    }
}

void Project::reset()
{
    Operator::reset();
}
