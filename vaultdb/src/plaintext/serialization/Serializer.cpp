#include "plaintext/serialization/Serializer.h"
#include "plaintext/data/TupleTable.h"
#include "plaintext/data/field/IntField.h"
#include "plaintext/data/field/VarcharField.h"

using namespace db::data;
using namespace db::data::field;
using namespace type;

Serializer::Serializer()
{}

char* Serializer::serializeTupleTable(const TupleTable &tt, size_vdb & serializedSize)
{
    size_vdb tt_size = tt.serializationSize();
    char * tt_data = new char[tt_size];
    tt.serializeTo(tt_data, tt_size);
    serializedSize = tt_size;
    return tt_data;
}

TupleTable Serializer::deserializeTupleTable(const char * data, size_vdb size)
{
    size_vdb offset = 0;

    // parse schema
    RecordSchema s = RecordSchema::deserializeFrom((unsigned char*)data);
    offset += s.serializationSize();

    // parse table id from tuple side
    offset += sizeof(size_vdb);

    // parse number of tuples
    size_vdb numTuples = *(size_vdb*) (data + offset);
    offset += sizeof(size_vdb);

    // parse each tuple
    TupleList tuples;
    for(int i = 0; i < numTuples; ++i) {
        Tuple out = deserializeTuple(s, (data + offset), size);
        offset += out.serializationSize();

        tuples.push_back(out);
    }

    return TupleTable(s, tuples);
}

Tuple Serializer::deserializeTuple(const RecordSchema &s, const char *data, size_vdb size)
{
    FieldHolder fields;
    size_vdb numFields = s.size();
    size_vdb offset = 0;

    bool dummyFlag = *data == 1;
    offset++;

    for(int f = 0; f < numFields; ++f) {
        switch (s[f].type) {
            case FieldDataType::Int:
                fields.push_back(IntField::deserializeFrom((data + offset), size));
                offset += fields[f].serializationSize();
                break;
            case FieldDataType::Varchar:
                fields.push_back(VarcharField::deserializeFrom((data + offset), size));
                offset += fields[f].serializationSize();
                break;
            default:
                throw;
        };
    }

    return Tuple(dummyFlag, fields);
}