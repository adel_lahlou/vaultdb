#include <plaintext/data/field/Field.h>
#include <plaintext/data/field/IntField.h>
#include <plaintext/data/field/BoolField.h>
#include <plaintext/data/field/VarcharField.h>

using namespace db::data::field;

// tors
Field& Field::operator = (const Field &f)
{
    Field temp(f);
    this->swap(temp); // TODO: inefficent for strings
    return *this;
}


Field::Field(BaseConstructor)
        : rep (nullptr)
{}


Field::Field()
        : rep(nullptr)
{}


Field::Field(const Field &f)
        : rep(f.rep)
{}


Field::~Field()
{}


// factory functions
Field Field::makeIntField(int v)
{
    Field f;
    FieldPtr fp(new IntField(v));
    f.redefine(fp);
    return f;
}


Field Field::makeBoolField(bool v)
{
    Field f;
    FieldPtr fp(new BoolField(v));
    f.redefine(fp);
    return f;
}


Field Field::makeVarcharField(std::string& v)
{
    Field f;
    FieldPtr fp(new VarcharField(v));
    f.redefine(fp);
    return f;
}


Field Field::makeVarcharField(std::string&& v)
{
    Field f;
    FieldPtr fp(new VarcharField(v));
    f.redefine(fp);
    return f;
}


Field Field::makeVarcharField(const char * s)
{
    return Field::makeVarcharField(std::string(s));
}

Field Field::makeVarcharField(const char *s, size_vdb n)
{
    return Field::makeVarcharField(std::string(s, n));
}

// meta functions

bool Field::isTruthy() const
{
    return rep->isTruthy();
}


size_vdb Field::size() const
{
    return rep->size();
}


type::SchemaColumn Field::dataType() const noexcept
{
    return rep->dataType();
}

std::string Field::toString() const
{
    return rep->toString();
}


int Field::serializeTo(char *buf, size_vdb limit) const
{
    return rep->serializeTo(buf, limit);
}


size_vdb Field::serializationSize() const
{
    return rep->serializationSize();
}


void Field::swap(Field& f) throw()
{
    std::swap(this->rep, f.rep);
}

const void * Field::getValuePointer() const
{
    return rep->getValuePointer();
};

// algebraic functions
Field Field::operator + (Field const &f) const
{
    return rep->operator + (f);
}


Field Field::operator - (Field const &f) const
{
    return rep->operator - (f);
}


Field Field::operator * (Field const &f) const
{
    return rep->operator * (f);
}


Field Field::operator / (Field const &f) const
{
    return rep->operator / (f);
}

// comparison operators
bool Field::operator==(const Field &rhs) const
{
    return rep->operator==(rhs);
}


bool Field::operator!=(const Field &rhs) const
{
    return rep->operator!=(rhs);
}


bool Field::operator<(const Field &rhs) const
{
    return rep->operator<(rhs);
}


bool Field::operator>(const Field &rhs) const
{
    return rep->operator>(rhs);
}


bool Field::operator>=(const Field &rhs) const
{
    return rep->operator>=(rhs);
}


bool Field::operator<=(const Field &rhs) const
{
    return rep->operator<=(rhs);
}


// algebraic functions implementations
Field Field::IntFieldAdd(IntField const &f) const
{
    return rep->IntFieldAdd(f);
}

Field Field::VarcharFieldAdd(VarcharField const &f) const
{
    return rep->VarcharFieldAdd(f);
}


Field Field::IntFieldSub(IntField const &f) const
{
    return rep->IntFieldSub(f);
}


Field Field::VarcharFieldSub(VarcharField const &f) const
{
    return rep->VarcharFieldSub(f);
}


Field Field::IntFieldMul(IntField const &f) const
{
    return rep->IntFieldMul(f);
}

Field Field::VarcharFieldMul(VarcharField const &f) const
{
    return rep->VarcharFieldMul(f);
}


Field Field::IntFieldDiv(IntField const &f) const
{
    return rep->IntFieldDiv(f);
}

Field Field::VarcharFieldDiv(VarcharField const &f) const
{
    return rep->VarcharFieldDiv(f);
}


// comparison operators implementations
bool Field::IntFieldEq(IntField const &f) const
{
    return rep->IntFieldEq(f);
}


bool Field::VarcharFieldEq(VarcharField const &f) const
{
    return rep->VarcharFieldEq(f);
}


bool Field::IntFieldLt(IntField const &f) const
{
    return rep->IntFieldLt(f);
}


bool Field::VarcharFieldLt(VarcharField const &f) const
{
    return rep->VarcharFieldLt(f);
}


bool Field::IntFieldGt(IntField const &f) const
{
    return rep->IntFieldGt(f);
}


bool Field::VarcharFieldGt(VarcharField const &f) const
{
    return rep->VarcharFieldGt(f);
}


// extras

void Field::redefine(FieldPtr f)
{
    rep = f;
}


namespace std
{
    template<>
    void swap(db::data::field::Field& f1, db::data::field::Field& f2)
    {
        f1.swap(f2);
    }
}
