#include <plaintext/data/field/VarcharField.h>
#include <cstring>

using namespace db::data::field;

// tors
VarcharField::VarcharField()
        : value("")
{}


VarcharField::VarcharField(std::string& v)
        : value(v)
{}

VarcharField::VarcharField(std::string&& v)
        : value(v)
{}

VarcharField::VarcharField(const VarcharField& f)
        : value(f.value)
{}


VarcharField::VarcharField(const char * s)
        : value(std::string(s))
{}


VarcharField::VarcharField(const char * s, size_vdb n)
        : value(std::string(s, n))
{}


VarcharField::~VarcharField() {}


// meta functions

bool VarcharField::isTruthy() const
{
    // TODO: this means that all strings, since they cannot be null, are true
    return true;
}


size_vdb VarcharField::size() const {
    return value.size();
}


type::SchemaColumn VarcharField::dataType() const noexcept
{
    return type::SchemaColumn{type::FieldDataType::Varchar, (size_vdb)value.length()};
}



std::string VarcharField::toString() const
{
    return value;
}


int VarcharField::serializeTo(char *buf, size_vdb limit) const
{
    const char * cstr = value.c_str();
    strcpy(buf, cstr);
    return (int) strlen(cstr) + 1;
}

size_vdb VarcharField::serializationSize() const
{
    return strlen(value.c_str()) + 1;
}

Field VarcharField::deserializeFrom(const char *data, size_vdb len)
{
    return Field::makeVarcharField(data);
}

const void * VarcharField::getValuePointer() const
{
    return &value;
};


// algebra functions
Field VarcharField::operator + (Field const &f) const
{
    return f.VarcharFieldAdd(*this);
}

// TODO: throw error for the operators that aren't supported anyway
Field VarcharField::operator - (Field const &f) const
{
    return f.VarcharFieldSub(*this);
}


Field VarcharField::operator * (Field const &f) const
{
    return f.VarcharFieldMul(*this);
}


Field VarcharField::operator / (Field const &f) const
{
    return f.VarcharFieldDiv(*this);
}


// comparison operators
bool VarcharField::operator==(Field const &f) const
{
    return f.VarcharFieldEq(*this);
}


bool VarcharField::operator!=(Field const &f) const
{
    return !(f.VarcharFieldEq(*this));
}


bool VarcharField::operator<=(Field const &f) const
{
    return !(f.VarcharFieldGt(*this));
}


bool VarcharField::operator>=(Field const &f) const
{
    return !(f.VarcharFieldLt(*this));
}


bool VarcharField::operator>(Field const &f) const
{
    return f.VarcharFieldGt(*this);
}


bool VarcharField::operator<(Field const &f) const
{
    return f.VarcharFieldLt(*this);
}


// algebra functions implementations
Field VarcharField::VarcharFieldAdd(VarcharField const &f) const
{
    // TODO: switch to const std:string&&
    std::string temp = f.value + this->value;
    return Field::makeVarcharField(temp);
}


Field VarcharField::IntFieldAdd(IntField const &f) const
{
    throw std::domain_error("VarcharField cannot be added to IntField");
}


Field VarcharField::VarcharFieldSub(VarcharField const &f) const
{
    throw std::domain_error("VarcharField does not support subtraction");
}


Field VarcharField::IntFieldSub(IntField const &f) const
{
    throw std::domain_error("VarcharField does not support subtraction");
}


Field VarcharField::VarcharFieldMul(VarcharField const &f) const
{
    throw std::domain_error("VarcharField does not support multiplication");
}


Field VarcharField::IntFieldMul(IntField const &f) const
{
    throw std::domain_error("VarcharField does not support multiplication");
}


Field VarcharField::VarcharFieldDiv(VarcharField const &f) const
{
    throw std::domain_error("VarcharField does not support division");
}


Field VarcharField::IntFieldDiv(IntField const &f) const
{
    throw std::domain_error("VarcharField does not support division");
}


// comparison operators implementation

bool VarcharField::VarcharFieldEq(VarcharField const &f) const
{
    return f.value == this->value;
}


bool VarcharField::IntFieldEq(IntField const &f) const
{
    return false;
}


bool VarcharField::VarcharFieldLt(VarcharField const &f) const
{
    return f.value < value;
}


bool VarcharField::IntFieldLt(IntField const &f) const
{
    throw std::domain_error("IntField and VarcharField are not relatable");
}


bool VarcharField::VarcharFieldGt(VarcharField const &f) const
{
    return f.value > value;
}


bool VarcharField::IntFieldGt(IntField const &f) const
{
    throw std::domain_error("IntField and VarcharField are not relatable");
}
