#include <plaintext/data/field/IntField.h>

using namespace db::data::field;

// tors
IntField::IntField()
        : value(0)
{}


IntField::IntField(int v)
        : value(v)
{}


IntField::IntField(const IntField& f)
        : value(f.value)
{}

IntField::~IntField() {}


// meta functions

bool IntField::isTruthy() const
{
    return (bool) value;
}


size_vdb IntField::size() const {
    return sizeof(int);
}


type::SchemaColumn IntField::dataType() const noexcept
{
    return type::SchemaColumn{type::FieldDataType::Int, sizeof(int)};
}



std::string IntField::toString() const
{
    return std::to_string(value);
}


int IntField::serializeTo(char * buf, size_vdb limit) const
{
    *(int *) buf = value;
    return sizeof(int);
}

size_vdb IntField::serializationSize() const
{
    return size();
}

Field IntField::deserializeFrom(const char *data, size_vdb len)
{
    return Field::makeIntField(*(int*) data);
}

const void * IntField::getValuePointer() const
{
    return &value;
};

// algebra functions
Field IntField::operator + (Field const &f) const
{
    return f.IntFieldAdd(*this);
}


Field IntField::operator - (Field const &f) const
{
    return f.IntFieldSub(*this);
}


Field IntField::operator * (Field const &f) const
{
    return f.IntFieldMul(*this);
}


Field IntField::operator / (Field const &f) const
{
    return f.IntFieldDiv(*this);
}


// comparison operators
bool IntField::operator==(Field const &f) const
{
    return f.IntFieldEq(*this);
}


bool IntField::operator!=(Field const &f) const
{
    return !(f.IntFieldEq(*this));
}


bool IntField::operator<=(Field const &f) const
{
    return !(f.IntFieldGt(*this));
}


bool IntField::operator>=(Field const &f) const
{
    return !(f.IntFieldLt(*this));
}


bool IntField::operator>(Field const &f) const
{
    return f.IntFieldGt(*this);
}


bool IntField::operator<(Field const &f) const
{
    return f.IntFieldLt(*this);
}


// algebra functions implementations

Field IntField::IntFieldAdd(IntField const &f) const
{
    return Field::makeIntField(f.value + this->value);
}


Field IntField::VarcharFieldAdd(VarcharField const &f) const
{
    throw std::domain_error("IntField cannot be added VarcharField");
}


Field IntField::IntFieldSub(IntField const &f) const
{
    return Field::makeIntField(f.value - this->value);
}


Field IntField::VarcharFieldSub(VarcharField const &f) const
{
    throw std::domain_error("VarcharField cannot be subtracted");
}


Field IntField::IntFieldMul(IntField const &f) const
{
    return Field::makeIntField(f.value * this->value);
}


Field IntField::VarcharFieldMul(VarcharField const &f) const
{
    throw std::domain_error("IntField cannot be multiplied by VarcharField");
}


Field IntField::IntFieldDiv(IntField const &f) const
{
    return Field::makeIntField(f.value / this->value);
}


Field IntField::VarcharFieldDiv(VarcharField const &f) const
{
    throw std::domain_error("IntField cannot be divided by VarcharField");
}

// comparison operators implementation

bool IntField::IntFieldEq(IntField const &f) const
{
    return f.value == value;
}


bool IntField::VarcharFieldEq(VarcharField const &f) const
{
    return false;
}


bool IntField::IntFieldLt(IntField const &f) const
{
    return f.value < value;
}


bool IntField::VarcharFieldLt(VarcharField const &f) const
{
    throw std::domain_error("IntField and VarcharField are not relatable");
}


bool IntField::IntFieldGt(IntField const &f) const
{
    return f.value > value;
}


bool IntField::VarcharFieldGt(VarcharField const &f) const
{
    throw std::domain_error("IntField and VarcharField are not relatable");
}
