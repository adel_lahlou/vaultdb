#include "plaintext/data/TupleTable.h"


// TODO: add checking to see if tuple follows schema

using namespace db::data;
using namespace type;

TupleTable::TupleTable()
{}

TupleTable::TupleTable(const RecordSchema& s)
        : schema(s), tuples(new TupleList), tableid(s.getTableID())
{}

TupleTable::TupleTable(const type::RecordSchema &s, const TupleList &ts)
        : schema(s), tuples(new TupleList(ts)), tableid(s.getTableID())
{}

TupleTable::TupleTable(const TupleTable& tt)
        : schema(tt.getSchema()), tuples(tt.tuples), tableid(tt.tableid)
{}

TupleTable& TupleTable::operator=(const TupleTable &tt)
{
    schema = tt.getSchema();
    tuples = tt.tuples;
    return *this;
}



size_vdb TupleTable::size() const
{
    return tuples->size();
}

const type::RecordSchema& TupleTable::getSchema() const
{
    return schema;
}

std::string TupleTable::toString() const
{
    return std::string();
}


int TupleTable::serializeTo(char *buf, size_vdb limit) const
{
    char * end = buf;

    // schema
    end += schema.serializeTo((unsigned char*)end);

    // tableid
    *(TableID*) end = tableid;
    end += sizeof(TableID);

    // number of tuples
    *(size_vdb*) end = size();
    end += sizeof(size_vdb);

    // the tuples
    for(auto t: *tuples) {
        end += t.serializeTo(end, limit);
    }

    return (int) (end - buf);
}


size_vdb TupleTable::serializationSize() const
{
    // schema size
    size_vdb totalBytes = schema.serializationSize();
    // table ID
    totalBytes += sizeof(TableID);
    // number of tuples
    totalBytes += sizeof(size());
    for(auto t : *tuples){
        totalBytes += t.serializationSize();
    }
    return totalBytes;
}

TableID TupleTable::getTableID()
{
    return tableid;
};
void TupleTable::setTableID(TableID id)
{
    tableid = id;
};

void TupleTable::addTuple(Tuple &t)
{
    tuples->push_back(t);
}

void TupleTable::addTuple(Tuple &&t)
{
    tuples->push_back(t);
}

void TupleTable::setSchema(const type::RecordSchema &s)
{
    schema = s;
}

void TupleTable::erase(size_vdb pos)
{
    tuples->erase(tuples->begin() + pos);
}



Tuple& TupleTable::operator[](size_vdb pos)
{
    return tuples->operator[](pos);
}