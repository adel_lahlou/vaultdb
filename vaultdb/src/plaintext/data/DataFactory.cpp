#include "plaintext/data/DataFactory.h"

using namespace db::data;
using namespace db::data::field;
using namespace type;


Tuple DataFactory::createDefaultTuple(const RecordSchema& schema)
{
    Tuple t;
    for(int i = 0; i < schema.size(); ++i) {
        t.push_back(createDefaultField(schema[i]));
    }

    return t;
}

Field DataFactory::createDefaultField(type::SchemaColumn type)
{
    switch (type.type) {
        case FieldDataType::Bool:
            return Field::makeBoolField(false);
        case FieldDataType::Int:
            return Field::makeIntField(0);
        case FieldDataType::Varchar:
            return Field::makeVarcharField("");
        default:
            // should never happen when all the FieldDataTypes are supported
            // TODO: should return a NULLField or throw an error
            return Field::makeBoolField(false);
    }
}
