#include "plaintext/data/Tuple.h"

using namespace db::data;
using namespace db::data::field;

// tors
Tuple::Tuple()
    : fields(new FieldHolder)
{}


Tuple::Tuple(const FieldHolder &fs)
        : fields(new FieldHolder(fs))
{}

Tuple::Tuple(bool dummy, const FieldHolder &fs)
        : fields(new FieldHolder(fs)), dummyFlag(dummy)
{}


Tuple::Tuple(std::initializer_list<Field> fs)
        : fields(new FieldHolder(fs))
{}


Tuple::Tuple(const Tuple& vector)
        : fields(vector.fields), dummyFlag(vector.dummyFlag)
{}


Tuple& Tuple::operator=(const Tuple &t)
{
    dummyFlag = t.isDummy();
    fields = t.fields;
    return *this;
}


Tuple::~Tuple()
{}


// meta functions
size_vdb Tuple::size() const
{
    return fields->size();
}


bool Tuple::isDummy() const
{
    return dummyFlag;
}


std::string Tuple::toString() const
{
    return std::string();
}


int Tuple::serializeTo(char *buf, size_vdb limit) const
{
    char * end = buf;

    // one byte of dummy flag
    *end = dummyFlag ? 0x01 : 0x00;
    end ++;

    // each of the fields
    for(auto& f : *fields) {
        end += f.serializeTo(end, limit);
    }

    return (int) (end - buf);
}

size_vdb Tuple::serializationSize() const
{
    size_vdb totalBytes = 0;
    // one byte of dummy flag
    totalBytes ++;

    // size of each fields
    for(auto& f : *fields){
        totalBytes += f.serializationSize();
    }

    return totalBytes;
}

void Tuple::swap(Tuple& f) throw()
{
    std::swap(this->fields, f.fields);
    std::swap(this->dummyFlag, f.dummyFlag);
}

void Tuple::setDummyFlag(bool flag)
{
    dummyFlag = flag;
}


void Tuple::push_back(Field f)
{
    if (!fields.unique())
        get_fields_ownership();

    fields->push_back(f);
}

void Tuple::erase(size_vdb pos)
{
    if (!fields.unique())
        get_fields_ownership();

    fields->erase(fields->begin() + pos);
}

void Tuple::project(ProjectList poses)
{
    size_vdb deleted = 0;

    for(size_vdb pos : poses) {
        erase(pos - deleted);
        ++deleted;
    }
}

Tuple Tuple::join(const Tuple &t1, const Tuple &t2)
{
    FieldHolder joined;
    const size_vdb t1_size = t1.size();
    const size_vdb t2_size = t2.size();
    joined.reserve(t1_size + t2_size);

    for(int i = 0; i < t1_size; ++i){
        joined.push_back(t1[i]);
    }

    for(int i = 0; i < t2_size; ++i){
        joined.push_back(t2[i]);
    }

    return Tuple(joined);
}


const Field& Tuple::operator[](size_vdb pos) const
{
    return (*fields)[pos];
}

Field& Tuple::operator[](size_vdb pos)
{
    return fields->operator[](pos);
}


void Tuple::get_fields_ownership()
{
    fields = FieldHolderPtr(new FieldHolder(*fields));
}


int Tuple::compare(const Tuple &t1, const Tuple &t2, const std::vector<size_vdb>& compIndices)
{
    for(auto i : compIndices) {
        if(t1[i] > t2[i])
            return 1;
        else if(t1[i] < t2[i])
            return -1;
    }

    return 0;
}


namespace std
{
    template<>
    void swap(db::data::Tuple& t1, db::data::Tuple& t2)
    {
        t1.swap(t2);
    }
}
