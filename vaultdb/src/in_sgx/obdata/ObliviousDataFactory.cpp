#include "in_sgx/obdata/ObliviousDataFactory.h"

using namespace db::obdata;
using namespace db::obdata::obfield;
using namespace type;


ObliviousTuple ObliviousDataFactory::createDefaultTuple(const RecordSchema& schema)
{
    ObliviousTuple t;
    for(int i = 0, size = schema.size(); i < size; ++i) {
        t.push_back(createDefaultField(schema[i]));
    }
    t.setDefaultFlag(true);
    return t;
}

ObliviousTuple ObliviousDataFactory::createDefaultDummyTuple(const RecordSchema& schema)
{
    ObliviousTuple t;
    for(int i = 0, size = schema.size(); i < size; ++i) {
        t.push_back(createDefaultField(schema[i]));
    }
    t.setDummyFlag(true);
    t.setDefaultFlag(true);
    return t;
}


ObliviousField ObliviousDataFactory::createDefaultField(const type::SchemaColumn & type)
{
    switch (type.type) {
        case FieldDataType::Bool:
            return ObliviousField::makeObliviousBoolField(false);
        case FieldDataType::Int:
            return ObliviousField::makeObliviousIntField(0);
        case FieldDataType::Fixchar:
            return ObliviousField::makeDefaultObliviousFixcharField(type.size);
        case FieldDataType::TimestampNoZone:
            return ObliviousField::makeObliviousTimestampNoZoneField(0);
        default:
            // should never happen when all the FieldDataTypes are supported
            return ObliviousField::makeObliviousBoolField(false);
    }
}
