#include "in_sgx/obdata/ObliviousTupleBlock.h"

using namespace db::obdata;

ObliviousTupleBlock::ObliviousTupleBlock()
: _machineID(0), _tableID(0), _partitionID(0), _blockID(0), _numTuplesPermitted(0)
{};

ObliviousTupleBlock::ObliviousTupleBlock(
        MachineID machineID, TableID tableID,
        PartitionID partitionID, BlockID blockID, size_vdb tuplePermitted)
: _machineID(machineID), _tableID(tableID), _partitionID(partitionID), _blockID(blockID),
  _numTuplesPermitted(tuplePermitted)
{};

ObliviousTupleBlock::ObliviousTupleBlock(
        MachineID machineID, TableID tableID, PartitionID partitionID, BlockID blockID,
        size_vdb tuplePermitted, const ObliviousTupleList &ts)
: _machineID(machineID), _tableID(tableID), _partitionID(partitionID), _blockID(blockID),
  _numTuplesPermitted(tuplePermitted)
{
#if defined(LOCAL_DEBUG_MODE)
    if (ts.size() > _numTuplesPermitted) {
        printf("[FAILURE] ObliviousTupleBlock init cannot ingest %d tuples; limit: %d\n", (size_vdb)ts.size(), _numTuplesPermitted);
        abort();
    }
#endif
    _tuples.insert(_tuples.end(), ts.begin(), ts.end());
};

bool ObliviousTupleBlock::insertTuple(ObliviousTuple & t)
{
    if (_numTuplesPermitted < (size_vdb)(_tuples.size() + 1)) {
        return false;
    } else {
        _tuples.push_back(t);
        return true;
    }
};

size_vdb ObliviousTupleBlock::ingestTuples(const ObliviousTupleList &ts)
{
    if (_numTuplesPermitted < (size_vdb)(_tuples.size() + 1)) {
        return 0;
    } else {
        size_vdb ret = _numTuplesPermitted - (size_vdb)(_tuples.size());
        _tuples.insert(_tuples.end(), ts.begin(), ts.begin() + ret);
        return ret;
    }
};

bool ObliviousTupleBlock::eraseTuple(pos_vdb i) {
    _tuples.erase(_tuples.begin() + i);
};

size_vdb ObliviousTupleBlock::appendMove(ObliviousTupleBlock& appendix) {
    if (_numTuplesPermitted <= (size_vdb)(_tuples.size())) {
        return 0;
    } else {
        size_vdb ret = _numTuplesPermitted - (size_vdb)(_tuples.size());
        _tuples.insert(_tuples.end(), appendix._tuples.begin(), appendix._tuples.begin() + ret);
        appendix._tuples.erase(appendix._tuples.begin(), appendix._tuples.begin() + ret);
        return ret;
    }
};