#include "in_sgx/obdata/ObliviousTuple.h"

using namespace db::obdata;
using namespace db::obdata::obfield;

// tors
ObliviousTuple::ObliviousTuple()
    : fields(new ObliviousFieldHolder)
{}


ObliviousTuple::ObliviousTuple(const ObliviousFieldHolder &fs)
        : fields(new ObliviousFieldHolder(fs))
{}

ObliviousTuple::ObliviousTuple(bool isDummy, const ObliviousFieldHolder &fs)
        : fields(new ObliviousFieldHolder(fs)), dummyFlag(isDummy)
{}

ObliviousTuple::ObliviousTuple(bool isDummy, bool isDefault, const ObliviousFieldHolder &fs)
        : fields(new ObliviousFieldHolder(fs)), dummyFlag(isDummy), defaultFlag(isDefault)
{}


ObliviousTuple::ObliviousTuple(std::initializer_list<ObliviousField> fs)
        : fields(new ObliviousFieldHolder(fs))
{}

ObliviousTuple::ObliviousTuple(const ObliviousTuple& vector)
        : fields(vector.fields), dummyFlag(vector.dummyFlag), vacuousFlag(vector.vacuousFlag)
{}


ObliviousTuple& ObliviousTuple::operator=(const ObliviousTuple &t)
{
    dummyFlag = t.isDummy();
    vacuousFlag = t.isVacuous();
    fields = t.fields;
    return *this;
}


ObliviousTuple::~ObliviousTuple()
{}


// meta functions
size_vdb ObliviousTuple::size() const
{
    return (size_vdb)fields->size();
}


bool ObliviousTuple::isDummy() const
{
    return dummyFlag;
}

bool ObliviousTuple::isVacuous() const
{
    return vacuousFlag;
}

bool ObliviousTuple::isDefaultTuple() const
{
    return defaultFlag;
};


void ObliviousTuple::serializeTo(unsigned char *buf, const type::RecordSchema & schema) const
{
    unsigned char * end = buf;

    // one byte of dummy flag
    unsigned char flag = 0;
    flag |= (unsigned char) (dummyFlag ? 0x01: 0x00);
    flag |= (unsigned char) (defaultFlag ? 0x02: 0x00);
    *end = flag;
    end ++;

    // each of the fields
    for (pos_vdb i = 0, size = (size_vdb)fields.get()->size(); i < size; i++) {
        (fields.get())->at(i).serializeTo(end, schema[i].size);
        if ((fields.get())->at(i).getType() == type::FieldDataType::Fixchar) {
            end++;
        }
        end += schema[i].size;
    }

}

void ObliviousTuple::setDummyFlag(bool flag)
{
    dummyFlag = flag;
}

void ObliviousTuple::setVacuousFlag(bool flag)
{
    vacuousFlag = flag;
};

void ObliviousTuple::setDefaultFlag(bool flag)
{
    defaultFlag = flag;
};

void ObliviousTuple::push_back(ObliviousField f)
{
    if (!fields.unique())
        get_fields_ownership();

    fields->push_back(f);
}

void ObliviousTuple::erase(size_vdb pos)
{
    if (!fields.unique())
        get_fields_ownership();

    fields->erase(fields->begin() + pos);
}

void ObliviousTuple::project(ProjectList poses)
{
    size_vdb deleted = 0;

    for(size_vdb pos : poses) {
        erase(pos - deleted);
        ++deleted;
    }
}

ObliviousTuple ObliviousTuple::join(const ObliviousTuple &t1, const ObliviousTuple &t2)
{
    ObliviousFieldHolder joined;
    const size_vdb t1_size = t1.size();
    const size_vdb t2_size = t2.size();
    joined.reserve(t1_size + t2_size);

    for(int i = 0; i < t1_size; ++i){
        joined.push_back(t1[i]);
    }

    for(int i = 0; i < t2_size; ++i){
        joined.push_back(t2[i]);
    }

    return ObliviousTuple(joined);
}

ObliviousTuple ObliviousTuple::getVacuousTuple()
{
    ObliviousTuple vac;
    vac.setVacuousFlag(true);
    return vac;
};


const ObliviousField& ObliviousTuple::operator[](size_vdb pos) const
{
    return (*fields)[pos];
}

ObliviousField& ObliviousTuple::operator[](size_vdb pos)
{
    return fields->operator[](pos);
}


void ObliviousTuple::get_fields_ownership()
{
    fields = ObliviousFieldHolderPtr(new ObliviousFieldHolder(*fields));
}

std::string ObliviousTuple::toString() const
{
    size_vdb count = (size_vdb)this->fields->size();
    std::string output;
    if (count != 0) {
        output = fields->at(0).toString();
        for (pos_vdb i = 1; i < count; i++) {
            if (fields->at(i).getType() == type::FieldDataType::Fixchar) {
                output += ",\'" + fields->at(i).toString() + "\'";
            } else {
                output += "," + fields->at(i).toString();
            }
        }
    }
    return output;
};;