#include <algorithm>
#include <in_sgx/utilities/InSGXController.h>
#include <in_sgx/obdata/ObliviousDataFactory.h>
#include "in_sgx/obdata/ObliviousTupleTable.h"

using namespace db::obdata;
using namespace type;

ObliviousTupleTable::ObliviousTupleTable() : tuples(new ObliviousTupleList), _partitionsID(DEFAULT_TABLE_PARTITION_ID), _numberOfPartitions(DEFAULT_TABLE_PARTITION_ID)
{}

ObliviousTupleTable::ObliviousTupleTable(const type::RecordSchema& s)
        : schema(s), tuples(new ObliviousTupleList), tableid(s.getTableID()), _partitionsID(DEFAULT_TABLE_PARTITION_ID), _numberOfPartitions(DEFAULT_TABLE_PARTITION_ID)
{}

ObliviousTupleTable::ObliviousTupleTable(const type::RecordSchema &s, const ObliviousTupleList &ts)
        : schema(s), tuples(new ObliviousTupleList(ts)), tableid(s.getTableID()), _partitionsID(DEFAULT_TABLE_PARTITION_ID), _numberOfPartitions(DEFAULT_TABLE_PARTITION_ID)
{}

ObliviousTupleTable::ObliviousTupleTable(const type::RecordSchema &s, pos_vdb partitionID, size_vdb numPartitions, const ObliviousTupleList &ts)
        : schema(s), tuples(new ObliviousTupleList(ts)), tableid(s.getTableID()), _partitionsID(partitionID), _numberOfPartitions(numPartitions)
{}

ObliviousTupleTable::ObliviousTupleTable(const ObliviousTupleTable& tt)
        : schema(tt.getSchema()), tuples(tt.tuples), tableid(tt.tableid), _partitionsID(tt._partitionsID), _numberOfPartitions(tt._numberOfPartitions)
{}

ObliviousTupleTable& ObliviousTupleTable::operator=(const ObliviousTupleTable &tt)
{
    schema = tt.getSchema();
    tuples = tt.tuples;
    tableid = tt.tableid; // FIXME tableid is redundant. change to rely on the tableid of RecordSchema.
    _partitionsID = tt._partitionsID;
    _numberOfPartitions = tt._numberOfPartitions;
    return *this;
}

size_vdb ObliviousTupleTable::size() const
{
    return (size_vdb)tuples->size();
}

const type::RecordSchema& ObliviousTupleTable::getSchema() const
{
    return schema;
}

std::string ObliviousTupleTable::toString() const
{
    return std::string();
}


void ObliviousTupleTable::serializeTo(unsigned char *buf) const
{
    unsigned char * end = buf;

    // schema
    end += schema.serializeTo(end);;

    // tableid
    *(TableID*) end = tableid;
    end += sizeof(TableID);

    // partitionID
    *(pos_vdb*) end = _partitionsID;
    end += sizeof(pos_vdb);

    // number of partitions in total
    *(size_vdb*) end = _numberOfPartitions;
    end += sizeof(size_vdb);

    // number of tuples
    *(size_vdb*) end = size();
    end += sizeof(size_vdb);

    // the tuples
    for(pos_vdb i = 0, size = (size_vdb)tuples.get()->size(), tupleSize = schema.tupleSerializationSize(); i < size; i++) {
        tuples.get()->at(i).serializeTo(end, schema);
        end += tupleSize;
    }

}


size_vdb ObliviousTupleTable::serializationSize() const
{
    // schema size
    size_vdb totalBytes = schema.serializationSize();
    // table ID
    totalBytes += sizeof(TableID);
    // parition id
    totalBytes += sizeof(pos_vdb);
    // number of partitions
    totalBytes += sizeof(size_vdb);
    // number of tuples
    totalBytes += sizeof(size());
    // total size of the uni-size tuples
    totalBytes += tuples->size() * schema.tupleSerializationSize();
    return totalBytes;
}

pos_vdb ObliviousTupleTable::getParitionID() const {
    return _partitionsID;
};

size_vdb ObliviousTupleTable::getNumberOfPartitions() const {
    return _numberOfPartitions;
};

TableID ObliviousTupleTable::getTableID() const
{
    return tableid;
};
void ObliviousTupleTable::setTableID(TableID id)
{
    tableid = id;
};

void ObliviousTupleTable::setParitionID(pos_vdb id) {
    _partitionsID = id;
};

void ObliviousTupleTable::setNumberOfPartitions(size_vdb count) {
    _numberOfPartitions = count;
};

void ObliviousTupleTable::add(ObliviousTuple t)
{
    tuples->push_back(t);
}

void ObliviousTupleTable::addObliviousTuple(ObliviousTuple &t)
{
    tuples->push_back(t);
}

void ObliviousTupleTable::addObliviousTuple(ObliviousTuple &&t)
{
    tuples->push_back(t);
}

void ObliviousTupleTable::setSchema(const type::RecordSchema &s)
{
    schema = s;
    tableid = schema.getTableID();
}

size_vdb ObliviousTupleTable::erase(size_vdb pos)
{
    tuples->erase(tuples->begin() + pos);
    return 1;
}

size_vdb ObliviousTupleTable::erase(ObliviousTupleList::iterator begin, ObliviousTupleList::iterator end)
{
    // use std::distance to get the count
    size_vdb count = (size_vdb)std::distance(begin, end);
    tuples->erase(begin, end);
    return count;
};

ObliviousTupleList::iterator ObliviousTupleTable::begin()
{
    return tuples->begin();
};

ObliviousTupleList::iterator ObliviousTupleTable::end()
{
    return tuples->end();
};

void ObliviousTupleTable::clear()
{
    tuples->clear();
};

ObliviousTuple& ObliviousTupleTable::operator[](size_vdb pos)
{
    return tuples->operator[](pos);
}

ObliviousTuple * ObliviousTupleTable::at(size_vdb pos) {
    return &tuples->at(pos);
};

size_vdb ObliviousTupleTable::append(ObliviousTupleTable appendix)
{
    if (schema == appendix.getSchema()) {
        if (appendix.size() > 0) {
            tuples->insert(tuples->end(), appendix.tuples->begin(), appendix.tuples->end());
        }
        return appendix.size();
    } else {
        return 0;
    }
};

size_vdb ObliviousTupleTable::appendMove(ObliviousTupleTable & appendix)
{
    if (schema == appendix.getSchema()) {
        size_vdb appendSize = appendix.size();
        if (appendSize > 0) {
            tuples->insert(tuples->end(), appendix.tuples->begin(), appendix.tuples->end());
            appendix.clear();
        }
        return appendSize;
    } else {
        return 0;
    }
};

size_vdb ObliviousTupleTable::CutOffLastN(size_vdb n, ObliviousTupleTable &inputAndRemainingOutput,
                                          ObliviousTupleTable &lastNOutput)
{
    if ((size_vdb)inputAndRemainingOutput.size() <= n) {
        size_vdb requiredFillIn = n - (size_vdb)inputAndRemainingOutput.size();
        for (pos_vdb i = 0; i < requiredFillIn; i++) {
            inputAndRemainingOutput.addObliviousTuple(ObliviousDataFactory::createDefaultDummyTuple(inputAndRemainingOutput.getSchema()));
        }
        return requiredFillIn;
    }
    auto firstEnd = inputAndRemainingOutput.tuples.get()->end();
    auto secondEnd = lastNOutput.tuples.get()->end();
    lastNOutput.tuples.get()->insert(secondEnd, firstEnd - n, firstEnd);
    inputAndRemainingOutput.tuples.get()->erase(firstEnd - n, firstEnd);
    return 0;
};

void ObliviousTupleTable::half(ObliviousTupleTable & inputAndFirstHalfOutput, ObliviousTupleTable & secondHalfOutput)
{
    if (inputAndFirstHalfOutput.size() <= 1) {
        secondHalfOutput.setSchema(type::RecordSchema(inputAndFirstHalfOutput.getSchema()));
        return;
    }
    size_vdb totalSize = inputAndFirstHalfOutput.size();
    ObliviousTupleList::iterator cutoff = inputAndFirstHalfOutput.begin() + (totalSize - totalSize / 2);
    ObliviousTupleList rightSplit = ObliviousTupleList(cutoff, inputAndFirstHalfOutput.end());
    secondHalfOutput = ObliviousTupleTable(type::RecordSchema(inputAndFirstHalfOutput.getSchema()), rightSplit);
    inputAndFirstHalfOutput.erase(cutoff, inputAndFirstHalfOutput.end());
};

void ObliviousTupleTable::appendNonObliviousSortHalf(ObliviousTupleTable &firstHalf, ObliviousTupleTable &secondHalf,
                                                     std::vector<pos_vdb> attributes, std::vector<SortOrder> orders)
{
    firstHalf.append(secondHalf);
    secondHalf.clear();
    std::sort(firstHalf.begin(), firstHalf.end(), [attributes, orders](ObliviousTuple & l, ObliviousTuple & r) {
        for (pos_vdb i = 0, size = (size_vdb)attributes.size(); i < size; i++) {
            int comparison = l[attributes[i]].comparesTo(r[attributes[i]], 0);
            if (orders[i] == SortOrder::ASCEND && comparison < 0) {
                return true;
            } else if (orders[i] == SortOrder::DESCEND && comparison > 0) {
                return true;
            } else if (comparison == 0) {
                continue;
            } else {
                return false;
            }
        }
        return false;
    });
    half(firstHalf, secondHalf);
};