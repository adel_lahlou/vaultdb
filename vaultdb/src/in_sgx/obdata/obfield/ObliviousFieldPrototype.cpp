#include "in_sgx/obdata/obfield/ObliviousFieldPrototype.h"

using namespace db::obdata::obfield;

ObliviousField ObliviousFieldPrototype::ObliviousIntFieldAdd(ObliviousIntField const &f) const
{
    throw std::domain_error("This field does not implement addition to ObliviousIntField\n");
};

ObliviousField ObliviousFieldPrototype::ObliviousFixcharFieldAdd(ObliviousFixcharField const &f) const
{
    throw std::domain_error("This field does not implement addition to ObliviousFixcharField\n");
};

ObliviousField ObliviousFieldPrototype::ObliviousTimestampNoZoneFieldAdd(ObliviousTimestampNoZoneField const &f) const
{
    throw std::domain_error("This field does not implement addition to ObliviousTimestampNoZoneField\n");
};

ObliviousField ObliviousFieldPrototype::ObliviousIntFieldSub(ObliviousIntField const &f) const
{
    throw std::domain_error("This field does not implement subtraction to ObliviousIntField\n");
};

ObliviousField ObliviousFieldPrototype::ObliviousFixcharFieldSub(ObliviousFixcharField const &f) const
{
    throw std::domain_error("This field does not implement subtraction to ObliviousFixcharField\n");
};

ObliviousField ObliviousFieldPrototype::ObliviousTimestampNoZoneFieldSub(ObliviousTimestampNoZoneField const &f) const
{
    throw std::domain_error("This field does not implement subtraction to ObliviousTimestampNoZoneField\n");
};

ObliviousField ObliviousFieldPrototype::ObliviousIntFieldMul(ObliviousIntField const &f) const
{
    throw std::domain_error("This field does not implement multiplication to ObliviousIntField\n");
};

ObliviousField ObliviousFieldPrototype::ObliviousFixcharFieldMul(ObliviousFixcharField const &f) const
{
    throw std::domain_error("This field does not implement multiplication to ObliviousFixcharField\n");
};

ObliviousField ObliviousFieldPrototype::ObliviousTimestampNoZoneFieldMul(ObliviousTimestampNoZoneField const &f) const
{
    throw std::domain_error("This field does not implement multiplication to ObliviousTimestampNoZoneField\n");
};

ObliviousField ObliviousFieldPrototype::ObliviousIntFieldDiv(ObliviousIntField const &f) const
{
    throw std::domain_error("This field does not implement division to ObliviousIntField\n");
};

ObliviousField ObliviousFieldPrototype::ObliviousFixcharFieldDiv(ObliviousFixcharField const &f) const
{
    throw std::domain_error("This field does not implement division to ObliviousFixcharField\n");
};

ObliviousField ObliviousFieldPrototype::ObliviousTimestampNoZoneFieldDiv(ObliviousTimestampNoZoneField const &f) const
{
    throw std::domain_error("This field does not implement division to ObliviousTimestampNoZoneField\n");
};

bool ObliviousFieldPrototype::ObliviousIntFieldEq(ObliviousIntField const &f, size_vdb genLevel, bool &actualResult) const
{
    throw std::domain_error("This field does not implement equality with ObliviousIntField\n");
};

bool
ObliviousFieldPrototype::ObliviousFixcharFieldEq(ObliviousFixcharField const &f, size_vdb genLevel, bool &actualResult) const
{
    throw std::domain_error("This field does not implement equality with ObliviousFixcharField\n");
};

bool ObliviousFieldPrototype::ObliviousTimestampNoZoneFieldEq(ObliviousTimestampNoZoneField const &f, size_vdb genLevel,
                                                              bool &actualResult) const
{
    throw std::domain_error("This field does not implement equality with ObliviousTimestampNoZoneField\n");
};

bool ObliviousFieldPrototype::ObliviousIntFieldLt(ObliviousIntField const &f, size_vdb genLevel, bool isNegating, bool &actualResult) const
{
    throw std::domain_error("This field does not implement less than with ObliviousIntField\n");
};

bool
ObliviousFieldPrototype::ObliviousFixcharFieldLt(ObliviousFixcharField const &f, size_vdb genLevel, bool isNegating, bool &actualResult) const
{
    throw std::domain_error("This field does not implement less than with ObliviousFixcharField\n");
};

bool
ObliviousFieldPrototype::ObliviousTimestampNoZoneFieldLt(ObliviousTimestampNoZoneField const &f, size_vdb genLevel, bool isNegating,
                                                         bool &actualResult) const
{
    throw std::domain_error("This field does not implement less than with ObliviousTimestampNoZoneField\n");
};

bool ObliviousFieldPrototype::ObliviousIntFieldGt(ObliviousIntField const &f, size_vdb genLevel, bool isNegating, bool &actualResult) const
{
    throw std::domain_error("This field does not implement greater than with ObliviousIntField\n");
};

bool
ObliviousFieldPrototype::ObliviousFixcharFieldGt(ObliviousFixcharField const &f, size_vdb genLevel, bool isNegating, bool &actualResult) const
{
    throw std::domain_error("This field does not implement greater than with ObliviousFixcharField\n");
};

bool
ObliviousFieldPrototype::ObliviousTimestampNoZoneFieldGt(ObliviousTimestampNoZoneField const &f, size_vdb genLevel, bool isNegating,
                                                         bool &actualResult) const
{
    throw std::domain_error("This field does not implement greater than with ObliviousTimestampNoZoneField\n");
};