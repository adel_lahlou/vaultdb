#include <in_sgx/obdata/obfield/ObliviousField.h>
#include <in_sgx/obdata/obfield/ObliviousIntField.h>
#include <in_sgx/obdata/obfield/ObliviousBoolField.h>
#include <in_sgx/obdata/obfield/ObliviousFixcharField.h>
#include <in_sgx/obdata/obfield/ObliviousTimestampNoZoneField.h>
#include <cstring>

using namespace db::obdata::obfield;

// tors
ObliviousField& ObliviousField::operator = (const ObliviousField &f)
{
    ObliviousField temp(f);
    this->swap(temp);
    return *this;
}


ObliviousField::ObliviousField(BaseConstructor)
        : rep (nullptr)
{}


ObliviousField::ObliviousField()
        : rep(nullptr)
{}


ObliviousField::ObliviousField(const ObliviousField &f)
        : rep(f.rep)
{}


ObliviousField::~ObliviousField()
{}


// factory functions
ObliviousField ObliviousField::makeObliviousIntField(int v)
{
    ObliviousField f;
    ObliviousFieldPtr fp(new ObliviousIntField(v));
    f.redefine(fp);
    return f;
}


ObliviousField ObliviousField::makeObliviousBoolField(bool v)
{
    ObliviousField f;
    ObliviousFieldPtr fp(new ObliviousBoolField(v));
    f.redefine(fp);
    return f;
}

ObliviousField ObliviousField::makeObliviousFixcharField(const char *s, size_vdb capacity)
{
    ObliviousField f;
    ObliviousFieldPtr fp(new ObliviousFixcharField(s, capacity));
    f.redefine(fp);
    return f;
}

ObliviousField ObliviousField::makeDefaultObliviousFixcharField(size_vdb capacity)
{
    ObliviousField f;
    ObliviousFieldPtr fp(new ObliviousFixcharField(capacity));
    f.redefine(fp);
    return f;
}

ObliviousField ObliviousField::makeObliviousTimestampNoZoneField(time_t v)
{
    ObliviousField f;
    ObliviousFieldPtr fp(new ObliviousTimestampNoZoneField(v));
    f.redefine(fp);
    return f;
}

// meta functions

bool ObliviousField::isTruthy() const
{
    return rep->isTruthy();
}


//type::SchemaColumn ObliviousField::datatype() const noexcept
//{
//    return rep->datatype();
//}

type::FieldDataType ObliviousField::getType()
{
    return rep->getType();
}

std::string ObliviousField::toString() const
{
    return rep->toString();
}


void ObliviousField::serializeTo(unsigned char *buf, size_vdb size) const
{
    rep->serializeTo(buf, size);
}


void ObliviousField::swap(ObliviousField& f) throw()
{
    std::swap(this->rep, f.rep);
}


// algebraic functions
ObliviousField ObliviousField::operator + (ObliviousField const &f) const
{
    return rep->operator + (f);
}


ObliviousField ObliviousField::operator - (ObliviousField const &f) const
{
    return rep->operator - (f);
}


ObliviousField ObliviousField::operator * (ObliviousField const &f) const
{
    return rep->operator * (f);
}


ObliviousField ObliviousField::operator / (ObliviousField const &f) const
{
    return rep->operator / (f);
}

// comparison operators
bool ObliviousField::operator==(const ObliviousField &rhs) const
{
    return rep->operator==(rhs);
}


bool ObliviousField::operator!=(const ObliviousField &rhs) const
{
    return rep->operator!=(rhs);
}


bool ObliviousField::operator<(const ObliviousField &rhs) const
{
    return rep->operator<(rhs);
}


bool ObliviousField::operator>(const ObliviousField &rhs) const
{
    return rep->operator>(rhs);
}


bool ObliviousField::operator>=(const ObliviousField &rhs) const
{
    return rep->operator>=(rhs);
}


bool ObliviousField::operator<=(const ObliviousField &rhs) const
{
    return rep->operator<=(rhs);
}

bool ObliviousField::isEqTo(const ObliviousField &rhs, size_vdb genLevel, bool &actualResult)
{
    return rep->isEqTo(rhs, genLevel, actualResult);
};

bool ObliviousField::isLEThan(const ObliviousField &rhs, size_vdb genLevel, bool &actualResult)
{
    return rep->isLEThan(rhs, genLevel, actualResult);
};

bool ObliviousField::isGEThan(const ObliviousField &rhs, size_vdb genLevel, bool &actualResult)
{
    return rep->isGEThan(rhs, genLevel, actualResult);
};

bool ObliviousField::isGThan(const ObliviousField &rhs, size_vdb genLevel, bool &actualResult)
{
    return rep->isGThan(rhs, genLevel, false, actualResult);
};

bool ObliviousField::isLThan(const ObliviousField &rhs, size_vdb genLevel, bool &actualResult)
{
    return rep->isLThan(rhs, genLevel, false, actualResult);
};

bool ObliviousField::isGThan(const ObliviousField &rhs, size_vdb genLevel, bool isNegating, bool &actualResult)
{
    return rep->isGThan(rhs, genLevel, false, actualResult);
};

bool ObliviousField::isLThan(const ObliviousField &rhs, size_vdb genLevel, bool isNegating, bool &actualResult)
{
    return rep->isLThan(rhs, genLevel, false, actualResult);
};

int ObliviousField::comparesTo(const ObliviousField &rhs, size_vdb genLevel) {
    return rep->comparesTo(rhs, genLevel);
};

// algebraic functions implementations
ObliviousField ObliviousField::ObliviousIntFieldAdd(ObliviousIntField const &f) const
{
    return rep->ObliviousIntFieldAdd(f);
}

ObliviousField ObliviousField::ObliviousFixcharFieldAdd(ObliviousFixcharField const &f) const
{
    return rep->ObliviousFixcharFieldAdd(f);
}

ObliviousField ObliviousField::ObliviousTimestampNoZoneFieldAdd(ObliviousTimestampNoZoneField const &f) const
{
    return rep->ObliviousTimestampNoZoneFieldAdd(f);
}

ObliviousField ObliviousField::ObliviousIntFieldSub(ObliviousIntField const &f) const
{
    return rep->ObliviousIntFieldSub(f);
}

ObliviousField ObliviousField::ObliviousFixcharFieldSub(ObliviousFixcharField const &f) const
{
    return rep->ObliviousFixcharFieldSub(f);
}

ObliviousField ObliviousField::ObliviousTimestampNoZoneFieldSub(ObliviousTimestampNoZoneField const &f) const
{
    return rep->ObliviousTimestampNoZoneFieldSub(f);
}

ObliviousField ObliviousField::ObliviousIntFieldMul(ObliviousIntField const &f) const
{
    return rep->ObliviousIntFieldMul(f);
}

ObliviousField ObliviousField::ObliviousFixcharFieldMul(ObliviousFixcharField const &f) const
{
    return rep->ObliviousFixcharFieldMul(f);
}

ObliviousField ObliviousField::ObliviousTimestampNoZoneFieldMul(ObliviousTimestampNoZoneField const &f) const
{
    return rep->ObliviousTimestampNoZoneFieldMul(f);
}

ObliviousField ObliviousField::ObliviousIntFieldDiv(ObliviousIntField const &f) const
{
    return rep->ObliviousIntFieldDiv(f);
}

ObliviousField ObliviousField::ObliviousFixcharFieldDiv(ObliviousFixcharField const &f) const
{
    return rep->ObliviousFixcharFieldDiv(f);
}

ObliviousField ObliviousField::ObliviousTimestampNoZoneFieldDiv(ObliviousTimestampNoZoneField const &f) const
{
    return rep->ObliviousTimestampNoZoneFieldDiv(f);
}

// comparison operators implementations
bool ObliviousField::ObliviousIntFieldEq(ObliviousIntField const &f, size_vdb genLevel, bool &actualResult) const
{
    return rep->ObliviousIntFieldEq(f, genLevel, actualResult);
}


bool ObliviousField::ObliviousFixcharFieldEq(ObliviousFixcharField const &f, size_vdb genLevel, bool &actualResult) const
{
    return rep->ObliviousFixcharFieldEq(f, genLevel, actualResult);
}

bool ObliviousField::ObliviousTimestampNoZoneFieldEq(ObliviousTimestampNoZoneField const &f, size_vdb genLevel,
                                                     bool &actualResult) const
{
    return rep->ObliviousTimestampNoZoneFieldEq(f, genLevel, actualResult);
}

bool ObliviousField::ObliviousIntFieldLt(ObliviousIntField const &f, size_vdb genLevel, bool isNegating, bool &actualResult) const
{
    return rep->ObliviousIntFieldLt(f, genLevel, isNegating, actualResult);
}


bool ObliviousField::ObliviousFixcharFieldLt(ObliviousFixcharField const &f, size_vdb genLevel, bool isNegating, bool &actualResult) const
{
    return rep->ObliviousFixcharFieldLt(f, genLevel, isNegating, actualResult);
}

bool ObliviousField::ObliviousTimestampNoZoneFieldLt(ObliviousTimestampNoZoneField const &f, size_vdb genLevel, bool isNegating,
                                                     bool &actualResult) const
{
    return rep->ObliviousTimestampNoZoneFieldLt(f, genLevel, isNegating, actualResult);
}

bool ObliviousField::ObliviousIntFieldGt(ObliviousIntField const &f, size_vdb genLevel, bool isNegating, bool &actualResult) const
{
    return rep->ObliviousIntFieldGt(f, genLevel, isNegating, actualResult);
}


bool ObliviousField::ObliviousFixcharFieldGt(ObliviousFixcharField const &f, size_vdb genLevel, bool isNegating, bool &actualResult) const
{
    return rep->ObliviousFixcharFieldGt(f, genLevel, isNegating, actualResult);
}

bool ObliviousField::ObliviousTimestampNoZoneFieldGt(ObliviousTimestampNoZoneField const &f, size_vdb genLevel, bool isNegating,
                                                     bool &actualResult) const
{
    return rep->ObliviousTimestampNoZoneFieldGt(f, genLevel, isNegating, actualResult);
}

int ObliviousField::ObliviousIntFieldComparesTo(ObliviousIntField const &f, size_vdb genLevel) const
{
    return rep->ObliviousIntFieldComparesTo(f, genLevel);
};

int ObliviousField::ObliviousFixcharFieldComparesTo(ObliviousFixcharField const &f, size_vdb genLevel) const
{
    return rep->ObliviousFixcharFieldComparesTo(f, genLevel);
};

int ObliviousField::ObliviousTimestampNoZoneFieldComparesTo(ObliviousTimestampNoZoneField const &f, size_vdb genLevel) const
{
    return rep->ObliviousTimestampNoZoneFieldComparesTo(f, genLevel);
};


// extras

void ObliviousField::redefine(ObliviousFieldPtr f)
{
    rep = f;
}


HashNumber ObliviousField::hash()
{
    return rep->hash();
};

std::vector<unsigned char> ObliviousField::convertToUnsignedCharVector() const
{
    return rep->convertToUnsignedCharVector();
};


namespace std
{
    template<>
    void swap(db::obdata::obfield::ObliviousField& f1, db::obdata::obfield::ObliviousField& f2)
    {
        f1.swap(f2);
    }
}
