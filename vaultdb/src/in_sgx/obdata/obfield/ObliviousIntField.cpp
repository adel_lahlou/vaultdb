#include <in_sgx/obdata/obfield/ObliviousIntField.h>
#include <in_sgx/utilities/SecureMove.h>
#include "in_sgx/obdata/obfield/ObliviousTimestampNoZoneField.h"
#include <functional>

using namespace db::obdata::obfield;

ObliviousIntField::ObliviousIntField()
        : value(0)
{}

ObliviousIntField::ObliviousIntField(int v)
        : value(v)
{}

ObliviousIntField::ObliviousIntField(const ObliviousIntField& f)
        : value(f.value)
{}

ObliviousIntField::~ObliviousIntField() {}

bool ObliviousIntField::isTruthy() const
{
    return (bool) value;
}

type::FieldDataType ObliviousIntField::getType()
{
    return type::FieldDataType::Int;
}

//type::SchemaColumn ObliviousIntField::datatype() const noexcept
//{
//    return type::SchemaColumn{type::FieldDataType::Int, sizeof(int)};
//}

std::string ObliviousIntField::toString() const
{
    return std::to_string(value);
}


void ObliviousIntField::serializeTo(unsigned char * buf, size_vdb capacity) const
{
    *(int *) buf = value;
}

ObliviousField ObliviousIntField::deserializeFrom(const unsigned char *data)
{
    return ObliviousField::makeObliviousIntField(*(int*) data);
}

ObliviousField ObliviousIntField::operator + (ObliviousField const &f) const
{
    return f.ObliviousIntFieldAdd(*this);
}

ObliviousField ObliviousIntField::operator - (ObliviousField const &f) const
{
    return f.ObliviousIntFieldSub(*this);
}

ObliviousField ObliviousIntField::operator * (ObliviousField const &f) const
{
    return f.ObliviousIntFieldMul(*this);
}

ObliviousField ObliviousIntField::operator / (ObliviousField const &f) const
{
    return f.ObliviousIntFieldDiv(*this);
}

bool ObliviousIntField::operator==(ObliviousField const &f) const
{
    bool temp;
    return f.ObliviousIntFieldEq(*this, 0, temp);
}

bool ObliviousIntField::operator!=(ObliviousField const &f) const
{
    bool temp;
    return !(f.ObliviousIntFieldEq(*this, 0, temp));
}

bool ObliviousIntField::operator<=(ObliviousField const &f) const
{
    bool temp;
    return !(f.ObliviousIntFieldGt(*this, 0, false, temp));
}

bool ObliviousIntField::operator>=(ObliviousField const &f) const
{
    bool temp;
    return !(f.ObliviousIntFieldLt(*this, 0, false, temp));
}

bool ObliviousIntField::operator>(ObliviousField const &f) const
{
    bool temp;
    return f.ObliviousIntFieldGt(*this, 0, false, temp);
}

bool ObliviousIntField::operator<(ObliviousField const &f) const
{
    bool temp;
    return f.ObliviousIntFieldLt(*this, 0, false, temp);
}

bool ObliviousIntField::isEqTo(const ObliviousField &rhs, size_vdb genLevel, bool &actualResult)
{
    return rhs.ObliviousIntFieldEq(*this, genLevel, actualResult);
};

bool ObliviousIntField::isLEThan(const ObliviousField &rhs, size_vdb genLevel, bool &actualResult)
{
    bool ret = !(rhs.ObliviousIntFieldGt(*this, genLevel, true, actualResult));
    actualResult = !actualResult;
    return ret;
};

bool ObliviousIntField::isGEThan(const ObliviousField &rhs, size_vdb genLevel, bool &actualResult)
{
    bool ret = !(rhs.ObliviousIntFieldLt(*this, genLevel, true, actualResult));
    actualResult = !actualResult;
    return ret;
};

bool ObliviousIntField::isGThan(const ObliviousField &rhs, size_vdb genLevel, bool isNegating, bool &actualResult)
{
    return rhs.ObliviousIntFieldGt(*this, genLevel, isNegating, actualResult);
};

bool ObliviousIntField::isLThan(const ObliviousField &rhs, size_vdb genLevel, bool isNegating, bool &actualResult)
{
    return rhs.ObliviousIntFieldLt(*this, genLevel, isNegating, actualResult);
};

int ObliviousIntField::comparesTo(const ObliviousField &rhs, size_vdb genLevel)
{
    return rhs.ObliviousIntFieldComparesTo(*this, genLevel);
};

ObliviousField ObliviousIntField::ObliviousIntFieldAdd(ObliviousIntField const &f) const
{
    return ObliviousField::makeObliviousIntField(f.value + this->value);
}

ObliviousField ObliviousIntField::ObliviousIntFieldSub(ObliviousIntField const &f) const
{
    return ObliviousField::makeObliviousIntField(f.value - this->value);
}

ObliviousField ObliviousIntField::ObliviousIntFieldMul(ObliviousIntField const &f) const
{
    return ObliviousField::makeObliviousIntField(f.value * this->value);
}

ObliviousField ObliviousIntField::ObliviousIntFieldDiv(ObliviousIntField const &f) const
{
    return ObliviousField::makeObliviousIntField(f.value / this->value);
}

ObliviousField ObliviousIntField::ObliviousTimestampNoZoneFieldAdd(ObliviousTimestampNoZoneField const &f) const
{
    return ObliviousField::makeObliviousTimestampNoZoneField(f.value + value);
};

ObliviousField ObliviousIntField::ObliviousTimestampNoZoneFieldSub(ObliviousTimestampNoZoneField const &f) const
{
    return ObliviousField::makeObliviousTimestampNoZoneField(f.value - value);
};

ObliviousField ObliviousIntField::ObliviousTimestampNoZoneFieldMul(ObliviousTimestampNoZoneField const &f) const
{
    return ObliviousField::makeObliviousIntField(f.value * value);
};

ObliviousField ObliviousIntField::ObliviousTimestampNoZoneFieldDiv(ObliviousTimestampNoZoneField const &f) const
{
    return ObliviousField::makeObliviousIntField(f.value / value);
};

bool ObliviousIntField::ObliviousIntFieldEq(ObliviousIntField const &f, size_vdb genLevel, bool &actualResult) const
{
    actualResult = f.value == value;
    int exponent = genLevel > 9 ? 9 : genLevel;
    return (f.value / int_10_power[exponent]) == (value / int_10_power[exponent]);
}

bool ObliviousIntField::ObliviousIntFieldLt(ObliviousIntField const &f, size_vdb genLevel, bool isNegating, bool &actualResult) const
{
    actualResult = f.value < value;
    int exponent = genLevel > 9 ? 9 : genLevel;
    if (isNegating || genLevel == 0) {
        return (f.value / int_10_power[exponent]) < (value / int_10_power[exponent]);
    } else {
        return (f.value / int_10_power[exponent]) <= (value / int_10_power[exponent]);
    }
}

bool ObliviousIntField::ObliviousIntFieldGt(ObliviousIntField const &f, size_vdb genLevel, bool isNegating, bool &actualResult) const
{
    actualResult = f.value > value;
    int exponent = genLevel > 9 ? 9 : genLevel;
    if (isNegating || genLevel == 0) {
        return (f.value / int_10_power[exponent]) > (value / int_10_power[exponent]);
    } else {
        return (f.value / int_10_power[exponent]) >= (value / int_10_power[exponent]);
    }
}

int ObliviousIntField::ObliviousIntFieldComparesTo(ObliviousIntField const &f, size_vdb genLevel) const
{
    int exponent = genLevel > 9 ? 9 : genLevel;
    int res = (f.value / int_10_power[exponent]) - (value / int_10_power[exponent]);
    return utilities::cmov_int(res, (res >> 31) | 0x01, 0);
};

HashNumber ObliviousIntField::hash()
{
    std::hash<int> ret;
    return ret(this->value);
};

std::vector<unsigned char> ObliviousIntField::convertToUnsignedCharVector() const
{
    auto out = (unsigned char*)(&this->value);
    return std::vector<unsigned char> (out, out + sizeof(int));
};
