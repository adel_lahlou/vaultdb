#include <in_sgx/obdata/obfield/ObliviousFixcharField.h>
#include <in_sgx/utilities/SecureMove.h>
#include <cstring>
#include <shared/type/RecordSchema.h>

using namespace db::obdata::obfield;

// tors
ObliviousFixcharField::ObliviousFixcharField(size_vdb cap)
{
    value = new char[cap+1]{};
    memset(value, 0x20, cap);
    value[cap] = 0;
}

ObliviousFixcharField::ObliviousFixcharField(const ObliviousFixcharField& f, size_vdb capacity)
{
    value = new char[capacity+1]{};
    memcpy(value, f.value, capacity);
}


ObliviousFixcharField::ObliviousFixcharField(const char * s, size_vdb cap)
{
    value = new char[cap+1]{};
    size_vdb slen = (size_vdb)strlen(s);
    pos_vdb offset = cap - slen;
    memset(value, 0x20, offset);
    memcpy(value + offset, s, slen);
    value[cap] = 0;
}

ObliviousFixcharField::~ObliviousFixcharField()
{
    delete[] value;
}


// meta functions

bool ObliviousFixcharField::isTruthy() const
{
    return true;
}

type::FieldDataType ObliviousFixcharField::getType()
{
    return type::FieldDataType::Fixchar;
}


std::string ObliviousFixcharField::toString() const
{
    return std::string(value);
}


void ObliviousFixcharField::serializeTo(unsigned char *buf, size_vdb capacity) const
{
    memcpy(buf, value, capacity);
    *(buf + capacity) = 0;
}

ObliviousField ObliviousFixcharField::deserializeFrom(const unsigned char *data, size_vdb capacity)
{
    return ObliviousField::makeObliviousFixcharField((char *) data, capacity);
}


// algebra functions
ObliviousField ObliviousFixcharField::operator + (ObliviousField const &f) const
{
    return f.ObliviousFixcharFieldAdd(*this);
}

ObliviousField ObliviousFixcharField::operator - (ObliviousField const &f) const
{
    return f.ObliviousFixcharFieldSub(*this);
}


ObliviousField ObliviousFixcharField::operator * (ObliviousField const &f) const
{
    return f.ObliviousFixcharFieldMul(*this);
}


ObliviousField ObliviousFixcharField::operator / (ObliviousField const &f) const
{
    return f.ObliviousFixcharFieldDiv(*this);
}


// comparison operators
bool ObliviousFixcharField::operator==(ObliviousField const &f) const
{
    bool temp;
    return f.ObliviousFixcharFieldEq(*this, 0, temp);
}


bool ObliviousFixcharField::operator!=(ObliviousField const &f) const
{
    bool temp;
    return !(f.ObliviousFixcharFieldEq(*this, 0, temp));
}


bool ObliviousFixcharField::operator<=(ObliviousField const &f) const
{
    bool temp;
    return !(f.ObliviousFixcharFieldGt(*this, 0, false, temp));
}


bool ObliviousFixcharField::operator>=(ObliviousField const &f) const
{
    bool temp;
    return !(f.ObliviousFixcharFieldLt(*this, 0, false, temp));
}


bool ObliviousFixcharField::operator>(ObliviousField const &f) const
{
    bool temp;
    return f.ObliviousFixcharFieldGt(*this, 0, false, temp);
}


bool ObliviousFixcharField::operator<(ObliviousField const &f) const
{
    bool temp;
    return f.ObliviousFixcharFieldLt(*this, 0, false, temp);
}


bool ObliviousFixcharField::isEqTo(const ObliviousField &rhs, size_vdb genLevel, bool &actualResult)
{
    return rhs.ObliviousFixcharFieldEq(*this, genLevel, actualResult);
};

bool ObliviousFixcharField::isLEThan(const ObliviousField &rhs, size_vdb genLevel, bool &actualResult)
{
    bool ret = !(rhs.ObliviousFixcharFieldGt(*this, genLevel, true, actualResult));
    actualResult = !actualResult;
    return ret;
};

bool ObliviousFixcharField::isGEThan(const ObliviousField &rhs, size_vdb genLevel, bool &actualResult)
{
    bool ret = !(rhs.ObliviousFixcharFieldLt(*this, genLevel, true, actualResult));
    actualResult = !actualResult;
    return ret;
};

bool ObliviousFixcharField::isGThan(const ObliviousField &rhs, size_vdb genLevel, bool isNegating, bool &actualResult)
{
    return rhs.ObliviousFixcharFieldGt(*this, genLevel, isNegating, actualResult);
};

bool ObliviousFixcharField::isLThan(const ObliviousField &rhs, size_vdb genLevel, bool isNegating, bool &actualResult)
{
    return rhs.ObliviousFixcharFieldLt(*this, genLevel, isNegating, actualResult);
};

int ObliviousFixcharField::comparesTo(const ObliviousField &rhs, size_vdb genLevel)
{
    return rhs.ObliviousFixcharFieldComparesTo(*this, genLevel);
};

// algebra functions implementations

// comparison operators implementation

static void processEq(char* thisValue, char* fValue, size_vdb remaining, int &hasDifference, pos_vdb &compStartOffset) {
    for (; remaining >= sizeof(int); remaining -= sizeof(int)) {
        int zeroIsEqual = swap_int_endian(*(int*)(thisValue + compStartOffset)) - swap_int_endian(*(int*)(fValue + compStartOffset));
        hasDifference = utilities::cmov_int(zeroIsEqual, 1, hasDifference);
        compStartOffset += sizeof(int);
    }
    if (remaining >= sizeof(short)) {
        short zeroIsEqual = swap_short_endian(*(short*)(thisValue + compStartOffset)) - swap_short_endian(*(short*)(fValue + compStartOffset));
        hasDifference = (int)utilities::cmov_short(zeroIsEqual, 1, (short)hasDifference);
        compStartOffset += sizeof(short);
        remaining -= sizeof(short);
    }
    if (remaining == sizeof(char)) {
        char zeroIsEqual = *(thisValue  + compStartOffset) - *(fValue + compStartOffset);
        hasDifference = utilities::cmov_int((int)zeroIsEqual, 1, hasDifference);
        compStartOffset += sizeof(char);
        // remaining -= sizeof(char);
    }
}

bool
ObliviousFixcharField::ObliviousFixcharFieldEq(ObliviousFixcharField const &f, size_vdb genLevel, bool &actualResult) const
{
    // Fixchar assumes that the expected length is fully filled
    size_vdb maxCompThis = (size_vdb)strlen(value);
    size_vdb maxCompF = (size_vdb)strlen(f.value);

    if (maxCompThis != maxCompF) {
        actualResult = true;
        return false;
    }

    size_vdb fullCompLength = maxCompThis;
    pos_vdb compStartOffset = 0;
    int hasDifference = 0; // no difference;

    if (genLevel > fullCompLength) {
        genLevel = maxCompThis;
    }

    bool ret;
    if (fullCompLength <= genLevel) {
        // full suppression, always return true;
        ret = true;
    } else {
        size_vdb remaining = fullCompLength - genLevel;
        processEq(value, f.value, remaining, hasDifference, compStartOffset);
        ret = hasDifference == 0;
    }
    actualResult = ret;
    if (genLevel > 0) {
        size_vdb remaining = genLevel;
        processEq(value, f.value, remaining, hasDifference, compStartOffset);
        actualResult = hasDifference == 0;
    }
    return ret;
}

static void processGT(char *thisValue, char *fValue, size_vdb remaining, int &isFound, int &isThisGtr,
                      pos_vdb &compStartOffset) {
    for (; remaining >= sizeof(int); remaining -= sizeof(int)) {
        int diff = swap_int_endian(*(int*)(fValue + compStartOffset)) - swap_int_endian(*(int*)(thisValue + compStartOffset));
        int isNeg = (diff >> (sizeof(int) * 8 - 1)) & 0x01;
        int predicate = utilities::cmov_int(isFound, 0, isNeg);
        isFound = utilities::cmov_int(diff, 1, isFound);
        isThisGtr = utilities::cmov_int(predicate, 1, isThisGtr);
        compStartOffset += sizeof(int);
    }
    if (remaining >= sizeof(short)) {
        short diff = swap_short_endian(*(short*)(fValue + compStartOffset)) - swap_short_endian(*(short*)(thisValue + compStartOffset));
        short isNeg = (diff >> (sizeof(short) * 8 - 1)) & (short)0x01;
        short predicate = utilities::cmov_short(isFound, 0, isNeg);
        isFound = (int) utilities::cmov_short(diff, 1, (short)isFound);
        isThisGtr = (int) utilities::cmov_short(predicate, 1, (short)isThisGtr);
        compStartOffset += sizeof(short);
        remaining -= sizeof(short);
    }
    if (remaining == sizeof(char)) {
        char diff = *(fValue + compStartOffset) - *(thisValue + compStartOffset);
        char isNeg = (diff >> (sizeof(char) * 8 - 1)) & (char)0x01;
        int predicate = utilities::cmov_int(isFound, 0, (int)isNeg);
        isFound = utilities::cmov_int((int)diff, 1, isFound);
        isThisGtr = utilities::cmov_int(predicate, 1, (short)isThisGtr);
        compStartOffset += sizeof(char);
    }
};

bool
ObliviousFixcharField::ObliviousFixcharFieldLt(ObliviousFixcharField const &f, size_vdb genLevel, bool isNegating, bool &actualResult) const
{
    // return f.value < value
    // Fixchar assumes that the expected length is fully filled
    size_vdb maxCompThis = (size_vdb)strlen(value);
    size_vdb maxCompF = (size_vdb)strlen(f.value);

    size_vdb fullCompLength = maxCompThis < maxCompF ? maxCompThis : maxCompF;
    pos_vdb compStartOffset = 0;
    bool ret;

    if (genLevel > fullCompLength) {
        genLevel = maxCompThis;
    }

    int isFGtr = 0;
    int isFound = 0;
    if (fullCompLength <= genLevel) {
        // full suppression, always return true;
        ret = true;
    } else {
        size_vdb remaining = fullCompLength - genLevel;
        processGT(f.value, value, remaining, isFound, isFGtr, compStartOffset);
        int notFoundResult = isNegating ? maxCompThis <= maxCompF ? 1 : 0 : 0;
        int retValue = utilities::cmov_int(isFound, isFGtr, notFoundResult);
        ret = retValue == 0;
    }
    actualResult = !ret;
    if (genLevel > 0) {
        size_vdb remaining = genLevel;
        processGT(f.value, value, remaining, isFound, isFGtr, compStartOffset);
        int retValue = utilities::cmov_int(isFound, isFGtr, maxCompThis <= maxCompF ? 1 : 0);
        actualResult = retValue == 0;
    }
    return ret;
}

bool
ObliviousFixcharField::ObliviousFixcharFieldGt(ObliviousFixcharField const &f, size_vdb genLevel, bool isNegating, bool &actualResult) const
{
    // Fixchar assumes that the expected length is fully filled
    size_vdb maxCompThis = (size_vdb)strlen(value);
    size_vdb maxCompF = (size_vdb)strlen(f.value);

    size_vdb fullCompLength = maxCompThis < maxCompF ? maxCompThis : maxCompF;
    pos_vdb compStartOffset = 0;
    bool ret;

    if (genLevel > fullCompLength) {
        genLevel = maxCompThis;
    }

    int isFGtr = 0;
    int isFound = 0;
    if (fullCompLength <= genLevel) {
        // full suppression, always return true;
        ret = true;
    } else {
        size_vdb remaining = fullCompLength - genLevel;
        processGT(f.value, value, remaining, isFound, isFGtr, compStartOffset);
        int notFoundResult = isNegating ? maxCompThis > maxCompF ? 1 : 0 : 1;
        int retValue = utilities::cmov_int(isFound, isFGtr, notFoundResult);
        ret = retValue == 1;
    }
    actualResult = !ret;
    if (genLevel > 0) {
        size_vdb remaining = genLevel;
        processGT(f.value, value, remaining, isFound, isFGtr, compStartOffset);
        int retValue = utilities::cmov_int(isFound, isFGtr, maxCompThis > maxCompF ? 1 : 0);
        actualResult = retValue == 1;
    }
    return ret;
}

static void processCompare(char *thisValue, char *fValue, size_vdb remaining, int &isFound, int &verdict,
                      pos_vdb &compStartOffset) {
    isFound = 0;
    verdict = 0;
    for (; remaining >= sizeof(int); remaining -= sizeof(int)) {
        int diff = swap_int_endian(*(int*)(fValue + compStartOffset)) - swap_int_endian(*(int*)(thisValue + compStartOffset));
        int res = utilities::cmov_int(diff, (diff >> 31) | 0x01, diff);
        verdict = utilities::cmov_int(isFound, verdict, res);
        isFound = utilities::cmov_int(verdict, 1, isFound);
        compStartOffset += sizeof(int);
    }
    if (remaining >= sizeof(short)) {
        short diff = swap_short_endian(*(short*)(fValue + compStartOffset)) - swap_short_endian(*(short*)(thisValue + compStartOffset));
        short res = utilities::cmov_short(diff, (diff >> 15) | (short) 0x01, diff);
        verdict = utilities::cmov_int(isFound, verdict, (int)res);
        isFound = utilities::cmov_int((int)diff, 1, isFound);
        compStartOffset += sizeof(short);
        remaining -= sizeof(short);
    }
    if (remaining == sizeof(char)) {
        char diff = *(fValue + compStartOffset) - *(thisValue + compStartOffset);
        int res = utilities::cmov_int((int)diff, (diff >> 7) | 0x01, (int)diff);
        verdict = utilities::cmov_int(isFound, verdict, res);
        isFound = utilities::cmov_int(verdict, 1, isFound);
        compStartOffset += sizeof(char);
    }
};

int ObliviousFixcharField::ObliviousFixcharFieldComparesTo(ObliviousFixcharField const &f, size_vdb genLevel) const
{
    // return f.value < value
    // Fixchar assumes that the expected length is fully filled
    size_vdb maxCompThis = (size_vdb)strlen(value);
    size_vdb maxCompF = (size_vdb)strlen(f.value);
    size_vdb fullCompLength = maxCompThis < maxCompF ? maxCompThis : maxCompF;

    if (genLevel > fullCompLength) {
        genLevel = maxCompThis;
    }

    int ret;
    int verdict = 0;
    int isFound = 0;
    pos_vdb compStartOffset = 0;
    if (fullCompLength <= genLevel) {
        // full suppression, always return true;
        ret = 0;
    } else {
        size_vdb remaining = fullCompLength - genLevel;
        processCompare(f.value, value, remaining, isFound, verdict, compStartOffset);
        ret = utilities::cmov_int(isFound, verdict, maxCompF < maxCompThis ? -1 : 1);
    }
    return ret;
};

HashNumber ObliviousFixcharField::hash()
{
    std::hash<std::string> ret;
    return ret(this->value);
};

std::vector<unsigned char> ObliviousFixcharField::convertToUnsignedCharVector() const
{
    auto out = (unsigned char*)this->value;
    return std::vector<unsigned char> (out, out + strlen(this->value));
};

// extra

