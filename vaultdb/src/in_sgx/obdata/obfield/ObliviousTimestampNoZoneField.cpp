#include "in_sgx/obdata/obfield/ObliviousTimestampNoZoneField.h"
#include <cstring>
#include <in_sgx/utilities/SecureMove.h>
#include "in_sgx/obdata/obfield/ObliviousIntField.h"
#include <functional>

using namespace db::obdata::obfield;

ObliviousTimestampNoZoneField::ObliviousTimestampNoZoneField() : value(0){};
ObliviousTimestampNoZoneField::ObliviousTimestampNoZoneField(time_t v) : value(v){};
ObliviousTimestampNoZoneField::ObliviousTimestampNoZoneField(const ObliviousTimestampNoZoneField &f) : value(f.value){};
ObliviousTimestampNoZoneField::~ObliviousTimestampNoZoneField(){};

bool ObliviousTimestampNoZoneField::isTruthy() const {return (bool) value;};
//type::SchemaColumn ObliviousTimestampNoZoneField::datatype() const noexcept {return type::SchemaColumn{type::FieldDataType::TimestampNoZone, sizeof(time_t)};};

type::FieldDataType ObliviousTimestampNoZoneField::getType()
{
    return type::FieldDataType::TimestampNoZone;
}

std::string ObliviousTimestampNoZoneField::toString() const
{
    char temp[sizeof(time_t)];
    *(time_t*)temp = value;
    return std::string(temp);
};

void ObliviousTimestampNoZoneField::serializeTo(unsigned char * buf, size_vdb capacity) const
{
    if (capacity < sizeof(time_t)) {
        throw(std::length_error("serialization capacity smaller than sizeof(time_t)"));
    }
    *(time_t *) buf = value;
};

ObliviousField ObliviousTimestampNoZoneField::deserializeFrom(const unsigned char * data)
{
    return ObliviousField::makeObliviousTimestampNoZoneField(*(time_t *) data);
};

ObliviousField ObliviousTimestampNoZoneField::operator + (ObliviousField const &f) const
{
    return f.ObliviousTimestampNoZoneFieldAdd(*this);
}


ObliviousField ObliviousTimestampNoZoneField::operator - (ObliviousField const &f) const
{
    return f.ObliviousTimestampNoZoneFieldSub(*this);
}


ObliviousField ObliviousTimestampNoZoneField::operator * (ObliviousField const &f) const
{
    return f.ObliviousTimestampNoZoneFieldMul(*this);
}


ObliviousField ObliviousTimestampNoZoneField::operator / (ObliviousField const &f) const
{
    return f.ObliviousTimestampNoZoneFieldDiv(*this);
}

// comparison operators
bool ObliviousTimestampNoZoneField::operator==(ObliviousField const &f) const
{
    bool temp;
    return f.ObliviousTimestampNoZoneFieldEq(*this, 0, temp);
}


bool ObliviousTimestampNoZoneField::operator!=(ObliviousField const &f) const
{
    bool temp;
    return !(f.ObliviousTimestampNoZoneFieldEq(*this, 0, temp));
}


bool ObliviousTimestampNoZoneField::operator<=(ObliviousField const &f) const
{
    bool temp;
    return !(f.ObliviousTimestampNoZoneFieldGt(*this, 0, false, temp));
}


bool ObliviousTimestampNoZoneField::operator>=(ObliviousField const &f) const
{
    bool temp;
    return !(f.ObliviousTimestampNoZoneFieldLt(*this, 0, false, temp));
}


bool ObliviousTimestampNoZoneField::operator>(ObliviousField const &f) const
{
    bool temp;
    return f.ObliviousTimestampNoZoneFieldGt(*this, 0, false, temp);
}


bool ObliviousTimestampNoZoneField::operator<(ObliviousField const &f) const
{
    bool temp;
    return f.ObliviousTimestampNoZoneFieldLt(*this, 0, false, temp);
}

bool ObliviousTimestampNoZoneField::isEqTo(const ObliviousField &rhs, size_vdb genLevel, bool &actualResult)
{
    return rhs.ObliviousTimestampNoZoneFieldEq(*this, genLevel, actualResult);
};

bool ObliviousTimestampNoZoneField::isLEThan(const ObliviousField &rhs, size_vdb genLevel, bool &actualResult)
{
    bool ret = !(rhs.ObliviousTimestampNoZoneFieldGt(*this, genLevel, true, actualResult));
    actualResult = !actualResult;
    return ret;
};

bool ObliviousTimestampNoZoneField::isGEThan(const ObliviousField &rhs, size_vdb genLevel, bool &actualResult)
{
    bool ret = !(rhs.ObliviousTimestampNoZoneFieldLt(*this, genLevel, true, actualResult));
    actualResult = !actualResult;
    return ret;
};

bool ObliviousTimestampNoZoneField::isGThan(const ObliviousField &rhs, size_vdb genLevel, bool isNegating, bool &actualResult)
{
    return rhs.ObliviousTimestampNoZoneFieldGt(*this, genLevel, isNegating, actualResult);
};

bool ObliviousTimestampNoZoneField::isLThan(const ObliviousField &rhs, size_vdb genLevel, bool isNegating, bool &actualResult)
{
    return rhs.ObliviousTimestampNoZoneFieldLt(*this, genLevel, isNegating, actualResult);
};

int ObliviousTimestampNoZoneField::comparesTo(const ObliviousField &rhs, size_vdb genLevel)
{
    return rhs.ObliviousTimestampNoZoneFieldComparesTo(*this, genLevel);
};

ObliviousField ObliviousTimestampNoZoneField::ObliviousIntFieldAdd(ObliviousIntField const &f) const
{
    return ObliviousField::makeObliviousTimestampNoZoneField(f.value + value);
};

ObliviousField ObliviousTimestampNoZoneField::ObliviousIntFieldSub(ObliviousIntField const &f) const
{
    return ObliviousField::makeObliviousTimestampNoZoneField(f.value - value);
};

ObliviousField ObliviousTimestampNoZoneField::ObliviousIntFieldMul(ObliviousIntField const &f) const
{
    return ObliviousField::makeObliviousIntField(f.value * value);
};

ObliviousField ObliviousTimestampNoZoneField::ObliviousIntFieldDiv(ObliviousIntField const &f) const
{
    return ObliviousField::makeObliviousIntField(f.value / value);
};

ObliviousField ObliviousTimestampNoZoneField::ObliviousTimestampNoZoneFieldAdd(ObliviousTimestampNoZoneField const &f) const
{
    return ObliviousField::makeObliviousTimestampNoZoneField(f.value + value);
};

ObliviousField ObliviousTimestampNoZoneField::ObliviousTimestampNoZoneFieldSub(ObliviousTimestampNoZoneField const &f) const
{
    return ObliviousField::makeObliviousIntField((int)(f.value - value));
};

ObliviousField ObliviousTimestampNoZoneField::ObliviousTimestampNoZoneFieldMul(ObliviousTimestampNoZoneField const &f) const
{
    return ObliviousField::makeObliviousTimestampNoZoneField(f.value * value);
};

ObliviousField ObliviousTimestampNoZoneField::ObliviousTimestampNoZoneFieldDiv(ObliviousTimestampNoZoneField const &f) const
{
    return ObliviousField::makeObliviousTimestampNoZoneField(f.value / value);
};

bool ObliviousTimestampNoZoneField::ObliviousTimestampNoZoneFieldEq(ObliviousTimestampNoZoneField const &f,
                                                                    size_vdb genLevel, bool &actualResult) const
{
    actualResult = f.value == value;
    int shift = genLevel > 5 ? 5 : genLevel;
    return f.value / time_t_divisor[shift] == value / time_t_divisor[shift];
};

bool ObliviousTimestampNoZoneField::ObliviousTimestampNoZoneFieldLt(ObliviousTimestampNoZoneField const &f, size_vdb genLevel, bool isNegating,
                                                                    bool &actualResult) const
{
    actualResult = f.value < value;
    int shift = genLevel > 5 ? 5 : genLevel;
    if (isNegating || genLevel == 0) {
        return f.value / time_t_divisor[shift] < value / time_t_divisor[shift];
    } else {
        return f.value / time_t_divisor[shift] <= value / time_t_divisor[shift];
    }
}

bool ObliviousTimestampNoZoneField::ObliviousTimestampNoZoneFieldGt(ObliviousTimestampNoZoneField const &f, size_vdb genLevel, bool isNegating,
                                                                    bool &actualResult) const
{
    actualResult = f.value > value;
    int shift = genLevel > 5 ? 5 : genLevel;

    if (isNegating || genLevel == 0) {
        return f.value / time_t_divisor[shift] > value / time_t_divisor[shift];
    } else {
        return f.value / time_t_divisor[shift] >= value / time_t_divisor[shift];
    }
}

int ObliviousTimestampNoZoneField::ObliviousTimestampNoZoneFieldComparesTo(ObliviousTimestampNoZoneField const &f, size_vdb genLevel) const
{
    int shift = genLevel > 5 ? 5 : genLevel;
    long res = ((long) f.value / time_t_divisor[shift]) - ((long)value / time_t_divisor[shift]);
    return utilities::cmov_int((int)res, (int)(res >> (sizeof(long) * 8 - 1)) | 0x01, 0);
};

HashNumber ObliviousTimestampNoZoneField::hash()
{
    std::hash<time_t> ret;
    return ret(value);
};

std::vector<unsigned char> ObliviousTimestampNoZoneField::convertToUnsignedCharVector() const
{
    auto out = (unsigned char*)(&this->value);
    return std::vector<unsigned char> (out, out + sizeof(time_t));
};;
