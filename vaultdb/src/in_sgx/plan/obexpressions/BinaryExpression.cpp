#include <shared/QueryEncodingDefinitions.h>
#include <in_sgx/plan/obexpressions/ObliviousExpressionFactory.h>
#include "in_sgx/plan/obexpressions/BinaryExpression.h"

using namespace plan::obexpressions;
using namespace db::obdata::obfield;


BinaryExpression::BinaryExpression(ObliviousExpression lhs, ObliviousExpression  rhs) : _lhs(lhs), _rhs(rhs), _genLevel(0) {};

bool BinaryExpression::isAggregateFunction() const
{
    return false;
}

size_vdb BinaryExpression::encodeSize() const
{
    return 1 + _lhs.encodeSize() + _rhs.encodeSize();
};

type::SchemaColumn BinaryExpression::outputDatatype() const
{
    return type::SchemaColumn{type::FieldDataType::Bool, sizeof(int)};
};

void BinaryExpression::getInputReferences(std::vector<pos_vdb> & output) const
{
    _lhs.getInputReferences(output);
    _rhs.getInputReferences(output);
};

void BinaryExpression::encodeObliviousExpression(unsigned char * & writeHead) const
{
    *writeHead = ((uint8_vdb) this->getExpressionType());
    writeHead++;

    _lhs.encodeObliviousExpression(writeHead);
    _rhs.encodeObliviousExpression(writeHead);
};

void BinaryExpression::enforceGenLevel(size_vdb genLevel)
{
    _genLevel = genLevel;
    _lhs.enforceGenLevel(genLevel);
    _rhs.enforceGenLevel(genLevel);
};


Eq::Eq(ObliviousExpression lhs, ObliviousExpression  rhs)
        : BinaryExpression(lhs, rhs)
{}

Eq::~Eq() = default;

void Eq::setObliviousTuple(ObliviousTupleBox tp)
{
    _lhs.setObliviousTuple(tp);
    _rhs.setObliviousTuple(tp);
}

ObliviousField Eq::evaluate() const
{
    bool actualRes;
    return ObliviousField::makeObliviousBoolField(_lhs.evaluate().isEqTo(_rhs.evaluate(),_genLevel, actualRes));
}

serialization::ExpressionCode Eq::getExpressionType() const {
    return serialization::ExpressionCode::EQUALS;
};



NEq::NEq(ObliviousExpression lhs, ObliviousExpression  rhs)
        : BinaryExpression(lhs, rhs)
{}

NEq::~NEq() = default;

void NEq::setObliviousTuple(ObliviousTupleBox tp)
{
    _lhs.setObliviousTuple(tp);
    _rhs.setObliviousTuple(tp);
}

ObliviousField NEq::evaluate() const
{
    bool actualRes;
    return ObliviousField::makeObliviousBoolField(!_lhs.evaluate().isEqTo(_rhs.evaluate(), _genLevel, actualRes));
}

serialization::ExpressionCode NEq::getExpressionType() const {
    return serialization::ExpressionCode::NOT_EQUALS;
};




GT::GT(ObliviousExpression lhs, ObliviousExpression  rhs)
        : BinaryExpression(lhs, rhs)
{}

GT::~GT() = default;

void GT::setObliviousTuple(ObliviousTupleBox tp)
{
    _lhs.setObliviousTuple(tp);
    _rhs.setObliviousTuple(tp);
}

ObliviousField GT::evaluate() const
{
    bool actualRes;
    return ObliviousField::makeObliviousBoolField(_lhs.evaluate().isGThan(_rhs.evaluate(), _genLevel, actualRes));
}

serialization::ExpressionCode GT::getExpressionType() const {
    return serialization::ExpressionCode::GREATER_THAN;
};




LT::LT(ObliviousExpression lhs, ObliviousExpression  rhs)
        : BinaryExpression(lhs, rhs)
{}

LT::~LT() = default;

void LT::setObliviousTuple(ObliviousTupleBox tp)
{
    _lhs.setObliviousTuple(tp);
    _rhs.setObliviousTuple(tp);
}

ObliviousField LT::evaluate() const
{
    bool actualRes;
    return ObliviousField::makeObliviousBoolField(_lhs.evaluate().isLThan(_rhs.evaluate(), _genLevel, actualRes));
}

serialization::ExpressionCode LT::getExpressionType() const {
    return serialization::ExpressionCode::LESS_THAN;
};



GTE::GTE(ObliviousExpression lhs, ObliviousExpression  rhs)
        : BinaryExpression(lhs, rhs)
{}

GTE::~GTE() = default;

void GTE::setObliviousTuple(ObliviousTupleBox tp)
{
    _lhs.setObliviousTuple(tp);
    _rhs.setObliviousTuple(tp);
}

ObliviousField GTE::evaluate() const
{
    bool actualRes;
    return ObliviousField::makeObliviousBoolField(_lhs.evaluate().isGEThan(_rhs.evaluate(), _genLevel, actualRes));
}

serialization::ExpressionCode GTE::getExpressionType() const {
    return serialization::ExpressionCode::GREATER_THAN_OR_EQUAL;
};




LTE::LTE(ObliviousExpression lhs, ObliviousExpression  rhs)
        : BinaryExpression(lhs, rhs)
{}

LTE::~LTE() = default;

void LTE::setObliviousTuple(ObliviousTupleBox tp)
{
    _lhs.setObliviousTuple(tp);
    _rhs.setObliviousTuple(tp);
}

ObliviousField LTE::evaluate() const
{
    bool actualRes;
    return ObliviousField::makeObliviousBoolField(_lhs.evaluate().isLEThan(_rhs.evaluate(), _genLevel, actualRes));
}

serialization::ExpressionCode LTE::getExpressionType() const {
    return serialization::ExpressionCode::LESS_THAN_OR_EQUAL;
};



Add::Add(ObliviousExpression lhs, ObliviousExpression rhs)
        : BinaryExpression(lhs, rhs)
{}

Add::~Add() = default;

void Add::setObliviousTuple(ObliviousTupleBox tp)
{
    _lhs.setObliviousTuple(tp);
    _rhs.setObliviousTuple(tp);
}

ObliviousField Add::evaluate() const
{
    return _lhs.evaluate() + _rhs.evaluate();
}

type::SchemaColumn Add::outputDatatype() const
{
    type::SchemaColumn l = _lhs.outputDatatype();
    type::SchemaColumn r = _rhs.outputDatatype();
    if (l.type == r.type) {
        return l;
    } else if (l.type == type::FieldDataType::TimestampNoZone || r.type == type::FieldDataType::TimestampNoZone) {
        return l.type == type::FieldDataType::TimestampNoZone ? l : r;
    } else {
        throw (std::logic_error("Unknown output type"));
    }
}

serialization::ExpressionCode Add::getExpressionType() const {
    return serialization::ExpressionCode::PLUS;
};




Sub::Sub(ObliviousExpression lhs, ObliviousExpression  rhs)
        : BinaryExpression(lhs, rhs)
{}

Sub::~Sub() = default;

void Sub::setObliviousTuple(ObliviousTupleBox tp)
{
    _lhs.setObliviousTuple(tp);
    _rhs.setObliviousTuple(tp);
}

ObliviousField Sub::evaluate() const
{
    return _lhs.evaluate() - _rhs.evaluate();
}

type::SchemaColumn Sub::outputDatatype() const
{
    type::SchemaColumn l = _lhs.outputDatatype();
    type::SchemaColumn r = _rhs.outputDatatype();
    if (l.type == r.type) {
        return (l.type == type::FieldDataType::TimestampNoZone) ? type::SchemaColumn{type::FieldDataType::Int, sizeof(int)} : l;
    } else if (l.type == type::FieldDataType::TimestampNoZone || r.type == type::FieldDataType::TimestampNoZone) {
        return type::SchemaColumn{type::FieldDataType::TimestampNoZone, sizeof(time_t)};
    } else {
        throw (std::logic_error("Unknown output type"));
    }
}

serialization::ExpressionCode Sub::getExpressionType() const {
    return serialization::ExpressionCode::MINUS;
};



Mul::Mul(ObliviousExpression lhs, ObliviousExpression  rhs)
        : BinaryExpression(lhs, rhs)
{}

Mul::~Mul() = default;

void Mul::setObliviousTuple(ObliviousTupleBox tp)
{
    _lhs.setObliviousTuple(tp);
    _rhs.setObliviousTuple(tp);
}

ObliviousField Mul::evaluate() const
{
    return _lhs.evaluate() * _rhs.evaluate();
}

type::SchemaColumn Mul::outputDatatype() const
{
    type::SchemaColumn l = _lhs.outputDatatype();
    type::SchemaColumn r = _rhs.outputDatatype();
    if (l.type == r.type) {
        return l;
    } else if (l.type == type::FieldDataType::TimestampNoZone || r.type == type::FieldDataType::TimestampNoZone) {
        return l.type == type::FieldDataType::TimestampNoZone ? l : r;
    } else {
        throw (std::logic_error("Unknown output type"));
    }
}

serialization::ExpressionCode Mul::getExpressionType() const {
    return serialization::ExpressionCode::TIMES;
};




Div::Div(ObliviousExpression lhs, ObliviousExpression  rhs)
        : BinaryExpression(lhs, rhs)
{}

Div::~Div() = default;

void Div::setObliviousTuple(ObliviousTupleBox tp)
{
    _lhs.setObliviousTuple(tp);
    _rhs.setObliviousTuple(tp);
}

ObliviousField Div::evaluate() const
{
    return _lhs.evaluate() / _rhs.evaluate();
}

type::SchemaColumn Div::outputDatatype() const
{
    type::SchemaColumn l = _lhs.outputDatatype();
    type::SchemaColumn r = _rhs.outputDatatype();
    if (l.type == r.type) {
        return l;
    } else if (l.type == type::FieldDataType::TimestampNoZone || r.type == type::FieldDataType::TimestampNoZone) {
        return l.type == type::FieldDataType::TimestampNoZone ? l : r;
    } else {
        throw (std::logic_error("Unknown output type"));
    }
}

serialization::ExpressionCode Div::getExpressionType() const {
    return serialization::ExpressionCode::DIVIDE;
};



And::And(ObliviousExpression lhs, ObliviousExpression  rhs)
        : BinaryExpression(lhs, rhs)
{}

And::~And() = default;

void And::setObliviousTuple(ObliviousTupleBox tp)
{
    _lhs.setObliviousTuple(tp);
    _rhs.setObliviousTuple(tp);
}

ObliviousField And::evaluate() const
{
    return ObliviousField::makeObliviousBoolField(_lhs.evaluate().isTruthy() && _rhs.evaluate().isTruthy());
}

serialization::ExpressionCode And::getExpressionType() const {
    return serialization::ExpressionCode::AND;
};




Or::Or(ObliviousExpression lhs, ObliviousExpression  rhs)
        : BinaryExpression(lhs, rhs)
{}

Or::~Or() = default;

void Or::setObliviousTuple(ObliviousTupleBox tp)
{
    _lhs.setObliviousTuple(tp);
    _rhs.setObliviousTuple(tp);
}

ObliviousField Or::evaluate() const
{
    return ObliviousField::makeObliviousBoolField(_lhs.evaluate().isTruthy() || _rhs.evaluate().isTruthy());
}

serialization::ExpressionCode Or::getExpressionType() const {
    return serialization::ExpressionCode::OR;
};



JoinObliviousExpression::JoinObliviousExpression(ObliviousExpression lhs, ObliviousExpression rhs)
        : BinaryExpression(lhs, rhs)
{
    _pred = ObliviousExpressionFactory::makeEq(_lhs, _rhs);
}

JoinObliviousExpression::~JoinObliviousExpression()
{}


void JoinObliviousExpression::setObliviousTuple(ObliviousTupleBox tp)
{}

void JoinObliviousExpression::setLeftObliviousTuple(ObliviousTupleBox ltp)
{
    _lhs.setObliviousTuple(ltp);
}

void JoinObliviousExpression::setRightObliviousTuple(ObliviousTupleBox rtp)
{
    _rhs.setObliviousTuple(rtp);
}

db::obdata::obfield::ObliviousField JoinObliviousExpression::evaluate() const
{
    return _pred.evaluate();
}

serialization::ExpressionCode JoinObliviousExpression::getExpressionType() const {
    return serialization::ExpressionCode::JOIN_EQUALITY;
};