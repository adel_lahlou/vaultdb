#include <shared/QueryEncodingDefinitions.h>
#include "in_sgx/plan/obexpressions/Values.h"

using namespace plan::obexpressions;
using namespace db::obdata::obfield;

ValueExpression::ValueExpression(type::SchemaColumn type)
{
    _type = {type.type, type.size};
};

bool ValueExpression::isAggregateFunction() const
{
    return false;
}

Literal::Literal()
        : ValueExpression(type::SchemaColumn{type::FieldDataType::DecideAtRunTime, 0}), _value(ObliviousField::makeObliviousIntField(0))
{}

Literal::Literal(ObliviousField v, type::SchemaColumn type)
        : ValueExpression(type), _value(v)
{}

void Literal::setObliviousTuple(ObliviousTupleBox tp)
{}

ObliviousField Literal::evaluate() const
{
    return _value;
}

void Literal::encodeObliviousExpression(unsigned char * & writeHead) const
{
    *writeHead = (uint8_vdb) _type.type | (uint8_vdb) getExpressionType();
    writeHead++;

    _value.serializeTo( writeHead, _type.size);
    writeHead+= _type.size;
};

size_vdb Literal::encodeSize() const
{
    return 1 + _type.size;
};

void Literal::getInputReferences(std::vector<pos_vdb> & output) const
{};

serialization::ExpressionCode Literal::getExpressionType() const {
    return serialization::ExpressionCode::LITERAL;
};



Column::Column()
        : ValueExpression(type::SchemaColumn{type::FieldDataType::DecideAtRunTime, 0}), col(-1), curTp(nullptr)
{}

Column::Column(size_vdb colNum, type::SchemaColumn s)
        : ValueExpression(s), col(colNum), curTp(nullptr)
{}

void Column::setObliviousTuple(ObliviousTupleBox tp)
{
    curTp = tp;
}


ObliviousField Column::evaluate() const
{
    return (*curTp)[col];
}

pos_vdb Column::getPosition() const
{
    return col;
};

type::SchemaColumn ValueExpression::outputDatatype() const
{
    return _type;
};

void ValueExpression::enforceGenLevel(size_vdb genLevel) {};

void Column::encodeObliviousExpression(unsigned char * & writeHead) const
{
//    type::FieldDataType type = outputDatatype().type;
//    *writeHead = ((uint8_vdb) type) | ((uint8_vdb ) 0x00);

    *writeHead = ((uint8_vdb ) getExpressionType());
    writeHead++;

    *writeHead = (unsigned char)col;
    writeHead++;

    *writeHead = (unsigned char)_type.type;
    writeHead++;

    *(size_vdb*) writeHead = _type.size;
    writeHead+= sizeof(size_vdb);
};

size_vdb Column::encodeSize() const
{
    return 3 + sizeof(size_vdb);
};

void Column::getInputReferences(std::vector<pos_vdb> & output) const
{
    output.push_back(col);
};

serialization::ExpressionCode Column::getExpressionType() const {
    return serialization::ExpressionCode::COLUMN;
};