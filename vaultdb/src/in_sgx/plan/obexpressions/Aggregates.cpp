#include <shared/QueryEncodingDefinitions.h>
#include "in_sgx/plan/obexpressions/Aggregates.h"
#include "in_sgx/utilities/SecureMove.h"

using namespace plan::obexpressions;
using namespace db::obdata::obfield;

AggregateFunction::AggregateFunction()
{}

AggregateFunction::AggregateFunction(const ObliviousExpression &other)
        : ObliviousExpression(other)
{}

bool AggregateFunction::isAggregateFunction() const
{
    return true;
}

void AggregateFunction::update(bool go)
{
    std::static_pointer_cast<AggregateFunction>(rep)->update(go);
}

void AggregateFunction::reset(bool go)
{
    std::static_pointer_cast<AggregateFunction>(rep)->reset(go);
}

void AggregateFunction::enforceGenLevel(size_vdb genLevel) {};


Sum::Sum(size_vdb colNum, type::SchemaColumn r)
        : column(colNum, r),
          sum(0)
{}

Sum::Sum(Column col)
        : column(col),
          sum(0)
{}

Sum::~Sum()
{}

void Sum::setObliviousTuple(ObliviousTupleBox tp)
{
    column.setObliviousTuple(tp);
}

void Sum::update(bool go)
{
    int colValue = std::static_pointer_cast<ObliviousIntField>(column.evaluate().rep)->value;
    sum.value = utilities::cmov_int(go, sum.value + colValue, sum.value);
}

void Sum::reset(bool go)
{
    sum.value = utilities::cmov_int(go, 0, sum.value);
}

ObliviousField Sum::evaluate() const
{
    return ObliviousField::makeObliviousIntField(sum.value);
}

size_vdb Sum::encodeSize() const {
    return 3 + sizeof(size_vdb);
};

void Sum::encodeObliviousExpression(unsigned char * & writeHead) const
{
    *writeHead = ((uint8_vdb) getExpressionType());
    writeHead++;

    *writeHead = (unsigned char)column.getPosition();
    writeHead++;

    type::SchemaColumn s = column.outputDatatype();

    *writeHead = (unsigned char)s.type;
    writeHead++;

    *(size_vdb*) writeHead = s.size;
    writeHead+= sizeof(size_vdb);
};

type::SchemaColumn Sum::outputDatatype() const
{
    // TODO update this when float / double precision is online
    return type::SchemaColumn{type::FieldDataType::Int, sizeof(int)};
};

void Sum::getInputReferences(std::vector<pos_vdb> & output) const
{
    column.getInputReferences(output);
};

serialization::ExpressionCode Sum::getExpressionType() const {
    return serialization::ExpressionCode::SUM;
};




Count::Count(pos_vdb colNum)
        : column(colNum, type::SchemaColumn{type::FieldDataType::Int, sizeof(int)})
{}

Count::Count(Column col)
        : column(col)
{}

Count::~Count()
{}

type::SchemaColumn Count::outputDatatype() const
{
    return type::SchemaColumn{type::FieldDataType::Int, sizeof(int)};
};

void Count::setObliviousTuple(ObliviousTupleBox tp)
{
    column.setObliviousTuple(tp);
}

void Count::update(bool go)
{
    count = utilities::cmov_int(go, count + 1, count);
}

void Count::reset(bool go)
{
    count = utilities::cmov_int(go, 0, count);
}

ObliviousField Count::evaluate() const
{
    return ObliviousField::makeObliviousIntField(count);
}

size_vdb Count::encodeSize() const {
    return 3 + sizeof(size_vdb);
};

void Count::encodeObliviousExpression(unsigned char * & writeHead) const
{
    *writeHead = ((uint8_vdb) getExpressionType());
    writeHead++;

    *writeHead = (unsigned char)column.getPosition();
    writeHead++;

    type::SchemaColumn s = column.outputDatatype();

    *writeHead = (unsigned char)s.type;
    writeHead++;

    *(size_vdb*) writeHead = s.size;
    writeHead+= sizeof(size_vdb);
};

void Count::getInputReferences(std::vector<pos_vdb> & output) const
{
    column.getInputReferences(output);
};

serialization::ExpressionCode Count::getExpressionType() const {
    return serialization::ExpressionCode::COUNT;
};



CountDistinct::CountDistinct(pos_vdb colNum, type::SchemaColumn r)
        : Count(colNum), _lastFieldSerialized(generateInitialField(r)), _isInitialized(false), _fieldSize(r.size)
{};

CountDistinct::CountDistinct(Column col)
        : Count(col), _lastFieldSerialized(generateInitialField(col.outputDatatype())), _isInitialized(false), _fieldSize(col.outputDatatype().size)
{};

CountDistinct::~CountDistinct()
{
    delete[] _lastFieldSerialized;
}

unsigned char * CountDistinct::generateInitialField(type::SchemaColumn r) {
    switch(r.type) {
        case type::FieldDataType::Int:
            return new unsigned char[sizeof(int)+1]{0};
        case type::FieldDataType::Fixchar:
            return new unsigned char [r.size + 1]{0};
        case type::FieldDataType::TimestampNoZone:
            return new unsigned char[sizeof(time_t)+1]{0};
        default:
            throw("[FAILURE] unsupported type in CountDistinct");
    }
}

void CountDistinct::setObliviousTuple(ObliviousTupleBox tp)
{
    _curTp = tp;
}

void CountDistinct::update(bool go)
{
    size_vdb columnSize = column.outputDatatype().size;
    unsigned char comp[columnSize + 1];
    _curTp->operator[](column.getPosition()).serializeTo(comp, columnSize);

    // if not go, don't +1 and don't replace
    // if go, replace old one
    // if go, and not initialized, always +1
    // if go, and  is initialized, only +1 when different;
    int verdict = go;
    int isSame = utilities::cmov_compare_identical_array(_lastFieldSerialized, comp, columnSize);
    verdict = utilities::cmov_int(_isInitialized * (!isSame) + (!_isInitialized), verdict, 0);
    count = utilities::cmov_int(verdict, count + 1, count);

    utilities::cmov_copy_array(go, _lastFieldSerialized, comp, columnSize);
    _isInitialized = (bool)utilities::cmov_int(go, 1, _isInitialized);
}

void CountDistinct::reset(bool go)
{
    _isInitialized = utilities::cmov_int(go, 0, _isInitialized);
    count = utilities::cmov_int(go, 0, count);
}

ObliviousField CountDistinct::evaluate() const
{
    return ObliviousField::makeObliviousIntField(count);
}

serialization::ExpressionCode CountDistinct::getExpressionType() const {
    return serialization::ExpressionCode::COUNT_DIST;
};




RowNumber::RowNumber()
        : _rank(0)
{}

RowNumber::~RowNumber() {}

void RowNumber::setObliviousTuple(ObliviousTupleBox tp) {}

void RowNumber::update(bool go)
{
    _rank = static_cast<pos_vdb>(utilities::cmov_int(go, _rank + 1, _rank));
}

void RowNumber::reset(bool go)
{
    _rank = static_cast<pos_vdb>(utilities::cmov_int(go, 0, _rank));
}

type::SchemaColumn RowNumber::outputDatatype() const
{
    return type::SchemaColumn{type::FieldDataType::Int, sizeof(int)};
};

ObliviousField RowNumber::evaluate() const
{
    return ObliviousField::makeObliviousIntField(_rank);
}

size_vdb RowNumber::encodeSize() const {
    return 1;
};

void RowNumber::encodeObliviousExpression(unsigned char * & writeHead) const
{
    *writeHead = ((uint8_vdb) getExpressionType());
    writeHead++;

    // this aggregation does not reference a column. it just goes up.
};

void RowNumber::getInputReferences(std::vector<pos_vdb> & output) const
{
    // intentionally left blank
};

serialization::ExpressionCode RowNumber::getExpressionType() const {
    return serialization::ExpressionCode::ROW_NUMBER;
};