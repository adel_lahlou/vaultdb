#include "in_sgx/plan/obexpressions/AggregateObliviousExpressionList.h"

using namespace plan::obexpressions;
using namespace db::obdata;

AggregateObliviousExpressionList::AggregateObliviousExpressionList()
{}

AggregateObliviousExpressionList::AggregateObliviousExpressionList(const std::vector<AggregateFunction>& aggs)
        : obexpressions(aggs)
{}

AggregateObliviousExpressionList::AggregateObliviousExpressionList(std::initializer_list<AggregateFunction> aggs)
        : obexpressions(aggs)
{}

AggregateObliviousExpressionList::~AggregateObliviousExpressionList()
{}


bool AggregateObliviousExpressionList::isAllTruthy() const
{
    bool res = true;
    for(auto& e : obexpressions) {
        res = res && e.evaluate().isTruthy();
    }

    return res;
}

bool AggregateObliviousExpressionList::isAnyTruthy() const
{
    bool res = false;

    for(auto& e : obexpressions) {
        res = res || e.evaluate().isTruthy();
    }

    return res;
}

int AggregateObliviousExpressionList::size() const
{
    return obexpressions.size();
}

bool AggregateObliviousExpressionList::processedDummy() const
{
    return _processedDummy;
}

void AggregateObliviousExpressionList::setObliviousTuple(ObliviousTupleBox t)
{
    for(auto &e : obexpressions){
        e.setObliviousTuple(t);
    }
}

void AggregateObliviousExpressionList::update(bool go)
{
    for(auto &e : obexpressions){
        e.update(go);
    }
}

void AggregateObliviousExpressionList::reset(bool go)
{
    for(auto &e : obexpressions){
        e.reset(go);
    }

    _processedDummy = (_processedDummy ^ go) ^ go;
}

ObliviousTuple AggregateObliviousExpressionList::evaluate() const
{
    ObliviousTuple t;
    for(auto& e : obexpressions) {
        t.push_back(e.evaluate());
    }

    return t;
}

void AggregateObliviousExpressionList::push_back(AggregateFunction e)
{
    obexpressions.push_back(e);
}


ObliviousExpression& AggregateObliviousExpressionList::operator[](size_vdb pos)
{
    return obexpressions[pos];
}


const ObliviousExpression& AggregateObliviousExpressionList::operator[](size_vdb pos) const
{
    return obexpressions[pos];
}
