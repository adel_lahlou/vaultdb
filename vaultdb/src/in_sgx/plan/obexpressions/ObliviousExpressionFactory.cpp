#include <in_sgx/plan/obexpressions/Aggregates.h>
#include "in_sgx/plan/obexpressions/ObliviousExpressionFactory.h"

plan::obexpressions::ObliviousExpression plan::obexpressions::ObliviousExpressionFactory::makeLiteral(
        db::obdata::obfield::ObliviousField f, size_vdb size)
{
    ObliviousExpression e;
    ObliviousExpressionPtr ep(new Literal(f, type::SchemaColumn{f.getType(), size}));
    e.redefine(ep);
    return e;
}

plan::obexpressions::ObliviousExpression plan::obexpressions::ObliviousExpressionFactory::makeColumn(size_vdb colNum, type::SchemaColumn s)
{
    ObliviousExpression e;
    ObliviousExpressionPtr ep(new Column(colNum, s));
    e.redefine(ep);
    return e;
}

plan::obexpressions::ObliviousExpression plan::obexpressions::ObliviousExpressionFactory::makeAdd(ObliviousExpression lhs, ObliviousExpression rhs)
{
    ObliviousExpression e;
    ObliviousExpressionPtr ep(new Add(lhs, rhs));
    e.redefine(ep);
    return e;
}

plan::obexpressions::ObliviousExpression plan::obexpressions::ObliviousExpressionFactory::makeSub(ObliviousExpression lhs, ObliviousExpression rhs)
{
    ObliviousExpression e;
    ObliviousExpressionPtr ep(new Sub(lhs, rhs));
    e.redefine(ep);
    return e;
}

plan::obexpressions::ObliviousExpression plan::obexpressions::ObliviousExpressionFactory::makeMul(ObliviousExpression lhs, ObliviousExpression rhs)
{
    ObliviousExpression e;
    ObliviousExpressionPtr ep(new Mul(lhs, rhs));
    e.redefine(ep);
    return e;
}

plan::obexpressions::ObliviousExpression plan::obexpressions::ObliviousExpressionFactory::makeDiv(ObliviousExpression lhs, ObliviousExpression rhs)
{
    ObliviousExpression e;
    ObliviousExpressionPtr ep(new Div(lhs, rhs));
    e.redefine(ep);
    return e;
}

plan::obexpressions::ObliviousExpression plan::obexpressions::ObliviousExpressionFactory::makeEq(ObliviousExpression lhs, ObliviousExpression rhs)
{
    ObliviousExpression e;
    ObliviousExpressionPtr ep(new Eq(lhs, rhs));
    e.redefine(ep);
    return e;
}

plan::obexpressions::ObliviousExpression plan::obexpressions::ObliviousExpressionFactory::makeNEq(ObliviousExpression lhs, ObliviousExpression rhs)
{
    ObliviousExpression e;
    ObliviousExpressionPtr ep(new NEq(lhs, rhs));
    e.redefine(ep);
    return e;
}

plan::obexpressions::ObliviousExpression plan::obexpressions::ObliviousExpressionFactory::makeLT(ObliviousExpression lhs, ObliviousExpression rhs)
{
    ObliviousExpression e;
    ObliviousExpressionPtr ep(new LT(lhs, rhs));
    e.redefine(ep);
    return e;
}

plan::obexpressions::ObliviousExpression plan::obexpressions::ObliviousExpressionFactory::makeGT(ObliviousExpression lhs, ObliviousExpression rhs)
{
    ObliviousExpression e;
    ObliviousExpressionPtr ep(new GT(lhs, rhs));
    e.redefine(ep);
    return e;
}

plan::obexpressions::ObliviousExpression plan::obexpressions::ObliviousExpressionFactory::makeLTE(ObliviousExpression lhs, ObliviousExpression rhs)
{
    ObliviousExpression e;
    ObliviousExpressionPtr ep(new LTE(lhs, rhs));
    e.redefine(ep);
    return e;
}

plan::obexpressions::ObliviousExpression plan::obexpressions::ObliviousExpressionFactory::makeGTE(ObliviousExpression lhs, ObliviousExpression rhs)
{
    ObliviousExpression e;
    ObliviousExpressionPtr ep(new GTE(lhs, rhs));
    e.redefine(ep);
    return e;
}

plan::obexpressions::ObliviousExpression plan::obexpressions::ObliviousExpressionFactory::makeAnd(ObliviousExpression lhs, ObliviousExpression rhs)
{
    ObliviousExpression e;
    ObliviousExpressionPtr ep(new And(lhs, rhs));
    e.redefine(ep);
    return e;
}

plan::obexpressions::ObliviousExpression plan::obexpressions::ObliviousExpressionFactory::makeOr(ObliviousExpression lhs, ObliviousExpression rhs)
{
    ObliviousExpression e;
    ObliviousExpressionPtr ep(new Or(lhs, rhs));
    e.redefine(ep);
    return e;
}

plan::obexpressions::ObliviousExpression plan::obexpressions::ObliviousExpressionFactory::makeNot(ObliviousExpression exp)
{
    ObliviousExpression e;
    ObliviousExpressionPtr ep(new Not(exp));
    e.redefine(ep);
    return e;
}

plan::obexpressions::ObliviousExpression plan::obexpressions::ObliviousExpressionFactory::makeJoin(ObliviousExpression lhs, ObliviousExpression rhs)
{
    ObliviousExpression e;
    ObliviousExpressionPtr ep(new JoinObliviousExpression(lhs, rhs));
    e.redefine(ep);
    return e;
}

plan::obexpressions::ObliviousExpression plan::obexpressions::ObliviousExpressionFactory::makeSum(pos_vdb col, type::SchemaColumn r)
{
    ObliviousExpression e;
    ObliviousExpressionPtr ep(new Sum(col, r));
    e.redefine(ep);
    return e;
}

plan::obexpressions::ObliviousExpression plan::obexpressions::ObliviousExpressionFactory::makeCount(pos_vdb col)
{
    ObliviousExpression e;
    ObliviousExpressionPtr ep(new Count(col));
    e.redefine(ep);
    return e;
}

plan::obexpressions::ObliviousExpression plan::obexpressions::ObliviousExpressionFactory::makeCountDistinct(pos_vdb col, type::SchemaColumn r)
{
    ObliviousExpression e;
    ObliviousExpressionPtr ep(new CountDistinct(col, r));
    e.redefine(ep);
    return e;
}

plan::obexpressions::ObliviousExpression plan::obexpressions::ObliviousExpressionFactory::makeRowNumber()
{
    ObliviousExpression e;
    ObliviousExpressionPtr ep(new RowNumber());
    e.redefine(ep);
    return e;
}