#include <shared/QueryEncodingDefinitions.h>
#include "in_sgx/plan/obexpressions/UnaryExpression.h"

using namespace plan::obexpressions;
using namespace db::obdata::obfield;

UnaryExpression::UnaryExpression(ObliviousExpression exp)
    : _exp(exp)
{}

bool UnaryExpression::isAggregateFunction() const
{
    return false;
}

type::SchemaColumn UnaryExpression::outputDatatype() const
{
    return type::SchemaColumn{type::FieldDataType::Bool, sizeof(int)};
};

size_vdb UnaryExpression::encodeSize() const {
    return 1 + _exp.encodeSize();
};

void UnaryExpression::getInputReferences(std::vector<pos_vdb> & output) const
{
    _exp.getInputReferences(output);
};

void UnaryExpression::enforceGenLevel(size_vdb genLevel)
{
    _exp.enforceGenLevel(genLevel);
};


Not::Not(ObliviousExpression exp)
        : UnaryExpression(exp)
{}

Not::~Not() = default;

void Not::setObliviousTuple(ObliviousTupleBox tp)
{
    _exp.setObliviousTuple(tp);
}

ObliviousField Not::evaluate() const
{
    return ObliviousField::makeObliviousBoolField(!_exp.evaluate().isTruthy());
}

void Not::encodeObliviousExpression(unsigned char * & writeHead) const
{
    *writeHead = ((uint8_vdb) getExpressionType());
    writeHead++;

    _exp.encodeObliviousExpression(writeHead);
};

serialization::ExpressionCode Not::getExpressionType() const {
    return serialization::ExpressionCode::NOT;
};