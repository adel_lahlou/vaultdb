#include <shared/type/RecordSchema.h>
#include "in_sgx/plan/obexpressions/ObliviousExpressionList.h"

using namespace plan::obexpressions;
using namespace db::obdata;

ObliviousExpressionList::ObliviousExpressionList()
{}

ObliviousExpressionList::ObliviousExpressionList(std::vector<ObliviousExpression> es)
        : obexpressions(es)
{}

ObliviousExpressionList::ObliviousExpressionList(std::initializer_list<ObliviousExpression> es)
        : obexpressions(es)
{}

ObliviousExpressionList::~ObliviousExpressionList()
{}


bool ObliviousExpressionList::isAllTruthy() const
{
    for(auto& e : obexpressions) {
        if (!e.evaluate().isTruthy())
            return false;
    }

    return true;
}

bool ObliviousExpressionList::isAnyTruthy() const
{
    for(auto& e : obexpressions) {
        if (e.evaluate().isTruthy())
            return true;
    }

    return false;
}

int ObliviousExpressionList::size() const
{
    return (int)obexpressions.size();
}

void ObliviousExpressionList::setObliviousTuple(ObliviousTupleBox t)
{
    for(auto &e : obexpressions){
        e.setObliviousTuple(t);
    }
}

ObliviousTuple ObliviousExpressionList::evaluate() const
{
    ObliviousTuple t;
    for(auto& e : obexpressions) {
        t.push_back(e.evaluate());
    }

    return t;
}

type::SchemaColumnHolder ObliviousExpressionList::outputDatatype() const
{
    type::SchemaColumnHolder dts;
    for (unsigned int i = 0, size = obexpressions.size(); i < size; i++) {
        auto& e = obexpressions[i];
        dts.push_back(e.outputDatatype());
    }

    return dts;
}

void ObliviousExpressionList::push_back(ObliviousExpression e)
{
    obexpressions.push_back(e);
}


ObliviousExpression& ObliviousExpressionList::operator[](size_vdb pos)
{
    return obexpressions[pos];
}


const ObliviousExpression& ObliviousExpressionList::operator[](size_vdb pos) const
{
    return obexpressions[pos];
}
