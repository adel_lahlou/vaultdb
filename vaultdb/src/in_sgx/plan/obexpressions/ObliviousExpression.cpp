#include "in_sgx/plan/obexpressions/ObliviousExpression.h"
#include "in_sgx/plan/obexpressions/Values.h"
#include "in_sgx/plan/obexpressions/BinaryExpression.h"
#include "in_sgx/plan/obexpressions/UnaryExpression.h"
#include "in_sgx/plan/obexpressions/Aggregates.h"
#include "in_sgx/plan/obexpressions/ObliviousExpressionFactory.h"

using namespace plan::obexpressions;
using namespace db::obdata::obfield;
using namespace db::obdata;

ObliviousExpression::ObliviousExpression()
        : rep(nullptr)
{}

ObliviousExpression::ObliviousExpression(BaseConstructor)
        :rep(nullptr)
{}

ObliviousExpression& ObliviousExpression::operator=(const ObliviousExpression &rhs)
{
    ObliviousExpression temp(rhs);
    this->swap(temp);
    return *this;
}

ObliviousExpression::ObliviousExpression(const ObliviousExpression& e)
        : rep(e.rep)
{}

ObliviousExpression::~ObliviousExpression()
{}

void ObliviousExpression::swap(ObliviousExpression& e) throw()
{
    std::swap(this->rep, e.rep);
}

void ObliviousExpression::setObliviousTuple(ObliviousTupleBox tp)
{
    rep->setObliviousTuple(tp);
}

void ObliviousExpression::redefine(ObliviousExpressionPtr p)
{
    rep = p;
}

db::obdata::obfield::ObliviousField ObliviousExpression::evaluate() const
{
    return rep->evaluate();
}

bool ObliviousExpression::isAggregateFunction() const
{
    return rep->isAggregateFunction();
}

type::SchemaColumn ObliviousExpression::outputDatatype() const
{
    return rep->outputDatatype();
}

void ObliviousExpression::encodeObliviousExpression(unsigned char * & writeHead) const
{
    return rep->encodeObliviousExpression(writeHead);
};

size_vdb ObliviousExpression::encodeSize() const
{
    return rep->encodeSize();
};

void ObliviousExpression::getInputReferences(std::vector<pos_vdb> & output) const
{
    rep->getInputReferences(output);
};

serialization::ExpressionCode ObliviousExpression::getExpressionType() const {
    return rep->getExpressionType();
};

void ObliviousExpression::setGenLevel(std::vector<pos_vdb> &genLevels)
{
    std::vector<pos_vdb> refs;
    rep->getInputReferences(refs);

    size_vdb maxGenLevel = 0;
    for (auto r : refs) {
        auto g = genLevels[r];
        maxGenLevel = g > maxGenLevel ? g : maxGenLevel;
    }
    rep->enforceGenLevel(maxGenLevel);
};

void ObliviousExpression::enforceGenLevel(size_vdb genLevel)
{
    rep->enforceGenLevel(genLevel);
};

namespace std
{
    template<>
    void swap(plan::obexpressions::ObliviousExpression& e1, plan::obexpressions::ObliviousExpression& e2)
    {
        e1.swap(e2);
    }
}
