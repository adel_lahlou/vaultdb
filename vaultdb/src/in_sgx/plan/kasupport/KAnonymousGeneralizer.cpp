#include <in_sgx/plan/obexpressions/ObliviousExpressionList.h>
#include <in_sgx/plan/obexpressions/ObliviousExpressionFactory.h>
#include <in_sgx/plan/kaoperators/KAnonymousSortedGroup.h>
#include <algorithm>
#include <in_sgx/plan/kaoperators/KAnonymousSeqScanMarkNonDummy.h>
#include "in_sgx/plan/kasupport/KAnonymousGeneralizer.h"

using namespace plan::kaoperators;
using namespace plan::obexpressions;
using namespace db::obdata;
using namespace type;


KAnonymousGeneralizer::KAnonymousGeneralizer(TableID tempTableID, size_vdb k, type::RecordSchema inputSchema,
                                           db::obdata::ObliviousTupleTable * inputTable,
                                           std::vector<pos_vdb> control, std::vector<pos_vdb> entities,
                                           utilities::InSGXController * controller)
: _controller(controller), _k(k), _localMachineID(controller == nullptr ? 0 : controller->getMachineID()),
  _tableID(tempTableID), _outSchema(inputSchema), _inputTable(inputTable), _controlFlowAttributes(control), _entityIdentifiers(entities)
{};

KAnonymousGeneralizer::~KAnonymousGeneralizer(){};

std::vector<size_vdb> KAnonymousGeneralizer::computeAndGetGeneralizationLevel() {
    if (_generalizationLevel.size() < _outSchema.size()) {
        deriveGeneralizationLevels();
    }
    return _generalizationLevel;
};

std::vector<std::vector<size_vdb>> KAnonymousGeneralizer::produceDummyGeneralizationLattice(std::vector<pos_vdb> comparisons) {
    std::vector<std::vector<size_vdb>> output;
    for (pos_vdb i = 0; i < GEN_LEVELS_TRIED; i++) {
        std::vector<size_vdb> step;
        for (pos_vdb j = 0, size = (size_vdb) comparisons.size(); j < size; j++) {
            step.push_back(i);
        }
        output.push_back(step);
    }
    return output;
};

void KAnonymousGeneralizer::computeGenLevelNoEntity(ObliviousTupleTable *outputTable)
{
    auto lattice = produceDummyGeneralizationLattice(_controlFlowAttributes);
    pos_vdb genLatticePos = 0;
    size_vdb tableSize = outputTable->size();
    size_vdb latticeSize = (size_vdb)lattice.size();
    size_vdb attrSize = (size_vdb)_controlFlowAttributes.size();
    db::obdata::obfield::ObliviousField kexpr = db::obdata::obfield::ObliviousField::makeObliviousIntField(_k);

    while (genLatticePos < latticeSize) {
        std::vector<size_vdb> currentLevels = lattice[genLatticePos];
        pos_vdb currentTuplePos = 0;
        db::obdata::ObliviousTuple * lastTuplePtr = nullptr;

        while (currentTuplePos < tableSize) {
            ObliviousTuple * currentTuple = outputTable->at(currentTuplePos);

            if (lastTuplePtr != nullptr) {
                bool isSame = true;
                for (pos_vdb i = 0; i < attrSize; i++) {
                    bool dummyOutputBool;
                    if (!(*currentTuple)[i].isEqTo((*lastTuplePtr)[i], currentLevels[i], dummyOutputBool)) {
                        isSame = false;
                        break;
                    };
                }
                if (isSame) {
                    // do the addition and delete
                    (*lastTuplePtr)[attrSize] = (*currentTuple)[attrSize] + (*lastTuplePtr)[attrSize];
                    _controller->eraseTuple(outputTable, currentTuplePos);
                    tableSize = outputTable->size();
                    continue;
                } else {
                    // check if current count is K; if so replace next with current and continue
                    bool dummyOutputBool;
                    if (!(*lastTuplePtr)[attrSize].isLThan(kexpr, 0, dummyOutputBool)) {
                        lastTuplePtr = nullptr;
                    } else {
                        // otherwise go to next genLatticePos
                        genLatticePos ++;
                        break;
                    };

                }
            } else {
                lastTuplePtr = currentTuple;
                currentTuplePos++;
                continue;
            }
        }
        if (currentTuplePos == tableSize && currentTuplePos > 0) {
            bool dummyOutputBool;
            if (!(*outputTable->at(currentTuplePos - 1))[attrSize].isLThan(kexpr, 0, dummyOutputBool)) {
                break;
            } else {
                // otherwise go to next genLatticePos
                genLatticePos ++;
            };
        } else if (currentTuplePos == 0) {
            break;
        };
    }
    if (genLatticePos >= latticeSize) {
        genLatticePos = latticeSize - 1;
    }
    auto finalLattice = &lattice[genLatticePos];
    for (pos_vdb i = 0, size = finalLattice->size(); i < size; i++) {
        _generalizationLevel[_controlFlowAttributes[i]] = (*finalLattice)[i];
    }
}

void KAnonymousGeneralizer::computeGenLevelWithEntity(db::obdata::ObliviousTupleTable *outputTable, std::vector<pos_vdb> controlAndEntity)
{
    auto lattice = produceDummyGeneralizationLattice(controlAndEntity);
    pos_vdb genLatticePos = 0;
    size_vdb tableSize = outputTable->size();
    size_vdb latticeSize = (size_vdb)lattice.size();
    size_vdb comparisonSize = (size_vdb)controlAndEntity.size() ;
    size_vdb nonEntitySize = (size_vdb)controlAndEntity.size() - (size_vdb)_entityIdentifiers.size();
    db::obdata::obfield::ObliviousField kexpr = db::obdata::obfield::ObliviousField::makeObliviousIntField(_k);

    while (genLatticePos < latticeSize) {
        std::vector<size_vdb> currentLevels = lattice[genLatticePos];
        pos_vdb currentTuplePos = 0;
        size_vdb currentAccumulation = 0;
        db::obdata::ObliviousTuple * lastTuplePtr = nullptr;

        while (currentTuplePos < tableSize) {
            ObliviousTuple * currentTuple = outputTable->at(currentTuplePos);

            if (lastTuplePtr != nullptr) {

                bool isViewIdentical = true;
                bool isIdentityValueIdentical = true;
                for (pos_vdb i = 0; i < comparisonSize; i++) {
                    bool realEquality;
                    if (!(*currentTuple)[i].isEqTo((*lastTuplePtr)[i], currentLevels[i], realEquality)) {
                        isViewIdentical = false;
                        break;
                    };
                    if (i >= nonEntitySize) {
                        isIdentityValueIdentical &= realEquality;
                    }
                }
                if (isViewIdentical) {
                    // increase count if distinct
                    if (!isIdentityValueIdentical) {
                        currentAccumulation++;
                    }
                    lastTuplePtr = currentTuple;
                    currentTuplePos++;
                } else {
                    // check if current count is K; if so replace next with current and continue
                    if (currentAccumulation >= _k) {
                        currentAccumulation = 0;
                        lastTuplePtr = nullptr;
                    } else {
                        genLatticePos++;
                        break;
                    }
                }
            } else {
                lastTuplePtr = currentTuple;
                currentAccumulation++;
                currentTuplePos++;
            }
        }
        if (currentTuplePos == tableSize && currentTuplePos > 0) {
            if (currentAccumulation >= _k) {
                break;
            } else {
                // otherwise go to next genLatticePos
                genLatticePos ++;
            };
        }
    }
    if (genLatticePos >= latticeSize) {
        genLatticePos = latticeSize - 1;
    }
    auto finalLattice = &lattice[genLatticePos];
    for (pos_vdb i = 0, size = (size_vdb)finalLattice->size(); i < size; i++) {
        _generalizationLevel[controlAndEntity[i]] = (*finalLattice)[i];
    }
}

void KAnonymousGeneralizer::deriveGenLevelNoEntity(ObliviousTupleTable *outputTable) {
    // if no entity specifier is provided, then each control flow combination corresponds to k tuples
    ObliviousExpressionList list;
    for (auto a : _controlFlowAttributes) {
        list.push_back(ObliviousExpressionFactory::makeColumn(a, _outSchema[a]));
    }
    list.push_back(ObliviousExpressionFactory::makeCount(0));

    KAnonymousSeqScanMarkNonDummy * scan = new KAnonymousSeqScanMarkNonDummy(0, 0, _inputTable);
    KAnonymousSortedGroup agg(_tableID, 0, scan, list, _controlFlowAttributes, _controller);
    while (agg.next() == OperatorStatus::Ok) {
        ObliviousTuple t;
        agg.getCurrentTuple(t);
        _controller->insertTuple(outputTable,t);
    }
    computeGenLevelNoEntity(outputTable);
}

void KAnonymousGeneralizer::deriveGenLevelDisjointCase(ObliviousTupleTable *outputTable)
{
    // if control flow variables does not include ID, then each control flow combination correspond to k entity values
    // aggregation: distinct control flows and entities.
    // computation: compare everything before IDs, and count distinct IDs

    ObliviousExpressionList list;
    for (auto a : _controlFlowAttributes) {
        list.push_back(ObliviousExpressionFactory::makeColumn(a, _outSchema[a]));
    }
    for (auto a : _entityIdentifiers) {
        list.push_back(ObliviousExpressionFactory::makeColumn(a, _outSchema[a]));
    }

    auto comparisons(_controlFlowAttributes);
    comparisons.insert(comparisons.end(), _entityIdentifiers.begin(), _entityIdentifiers.end());

    KAnonymousSeqScanMarkNonDummy * scan = new KAnonymousSeqScanMarkNonDummy(0, 0, _inputTable);
    KAnonymousSortedGroup agg(_tableID, 0, scan, list, comparisons, _controller);
    while (agg.next() == OperatorStatus::Ok) {
        ObliviousTuple t;
        agg.getCurrentTuple(t);
        _controller->insertTuple(outputTable,t);
    }
    computeGenLevelWithEntity(outputTable, comparisons);
};

void KAnonymousGeneralizer::deriveGenLevelIntersectCase(ObliviousTupleTable *outputTable)
{
    // if control flow variables include ID, then each variable combination excluding entity correspond to k entity values
    // aggregation: distinct control flows (less entityID), remainder, followed by IDs.
    // computation: compare everything before IDs, and count distinct IDs

    auto controls(_controlFlowAttributes);
    std::sort(controls.begin(), controls.end());
    std::vector<pos_vdb> remainders;
    for (pos_vdb r = 0, c = 0, rsize = _outSchema.size(), csize = (size_vdb)controls.size(); r < rsize && c < csize; r++) {
        if (controls[c] > r) {
            remainders.push_back(r);
        } else {
            c++;
        }
    }

    ObliviousExpressionList list;

    std::vector<pos_vdb> comparisons;
    std::set<pos_vdb> entityIDset(_entityIdentifiers.begin(), _entityIdentifiers.end());
    for (pos_vdb i = 0, size = (size_vdb) _outSchema.size(); i < size; i++) {
        if (entityIDset.find(i) == entityIDset.end()) {
            comparisons.push_back(i);
            list.push_back(ObliviousExpressionFactory::makeColumn(i, _outSchema[i]));
        }
    }
    for (auto a : remainders) {
        list.push_back(ObliviousExpressionFactory::makeColumn(a, _outSchema[a]));
        comparisons.push_back(a);
    }
    for (auto a : _entityIdentifiers) {
        list.push_back(ObliviousExpressionFactory::makeColumn(a, _outSchema[a]));
        comparisons.push_back(a);
    }

    KAnonymousSeqScanMarkNonDummy * scan = new KAnonymousSeqScanMarkNonDummy(0, 0, _inputTable);
    KAnonymousSortedGroup agg(_tableID, 0, scan, list, comparisons, _controller);
    while (agg.next() == OperatorStatus::Ok) {
        ObliviousTuple t;
        agg.getCurrentTuple(t);
        _controller->insertTuple(outputTable,t);
    }

    computeGenLevelWithEntity(outputTable, comparisons);
};

void KAnonymousGeneralizer::deriveGeneralizationLevels()
{
    for (pos_vdb i = 0, size = (size_vdb)_outSchema.size(); i < size; i++) {
        _generalizationLevel.push_back(DEFAULT_MAX_GEN_LEVEL);
    }

    auto c(_controlFlowAttributes);
    auto e(_entityIdentifiers);
    std::vector<pos_vdb> intersection;
    std::sort(c.begin(), c.end());
    std::sort(e.begin(), e.end());
    std::set_intersection(c.begin(), c.end(), e.begin(), e.end(), std::back_inserter(intersection));

    ObliviousTupleTable * outputTable = _controller->create(_localMachineID, _tableID, DEFAULT_TABLE_PARTITION_ID, 1,
                                                            _outSchema);

    if (_entityIdentifiers.empty()) {
        deriveGenLevelNoEntity(outputTable);
    } else if (intersection.empty()) {
        deriveGenLevelDisjointCase(outputTable);
    } else {
        deriveGenLevelIntersectCase(outputTable);
    }

    _controller->erase(_localMachineID, _tableID, DEFAULT_TABLE_PARTITION_ID);
};