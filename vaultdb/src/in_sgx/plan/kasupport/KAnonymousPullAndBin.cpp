#include <in_sgx/utilities/InSGXController.h>
#include <in_sgx/plan/kasupport/KAnonymousPullAndBin.h>
#include <in_sgx/utilities/DummyInSGXHashPartitioner.h>
#include "in_sgx/plan/kasupport/KAnonymousPullAndBin.h"
#include "in_sgx/obdata/ObliviousTupleTable.h"

using namespace plan::kaoperators;

#if defined(LOCAL_DEBUG_MODE) || defined(OUTSIDE_COMPILATION)
#else
#include "Enclave_t.h"
#endif

KAnonymousPullAndBin::KAnonymousPullAndBin(TableID operatorID, KAnonymousOperator *input, size_vdb k, std::vector<pos_vdb> groupBys,
                                           bool createBinOnDemand, utilities::InSGXController *controller)
: _tableID(operatorID), _child(input), _groupBys(groupBys), _controller(controller), _isFetched(false), _k(k), _createBinOnDemand(createBinOnDemand)
{};

KAnonymousPullAndBin::~KAnonymousPullAndBin() {
    if (!_binHolder.empty()) {
        for (pos_vdb i = 0, size = (size_vdb) _binHolder.size(); i < size; i++) {
            _controller->erase(_controller->getMachineID(), _tableID, i);
        }
        _binHolder.clear();
    }
}

void KAnonymousPullAndBin::setInput(KAnonymousOperator *input) {
    _child = input;
};

void KAnonymousPullAndBin::pullAndBin() {
    db::obdata::ObliviousTupleTable * allTuples =
            _controller->create(_controller->getMachineID(), _tableID + 1, DEFAULT_TABLE_PARTITION_ID, 1, _child->getSchema());

    size_vdb count = 0;

    db::obdata::ObliviousTuple tempTuple;
    while (_child->next() == OperatorStatus::Ok) {
        _child->getCurrentTuple(tempTuple);
        _controller->insertTuple(allTuples, tempTuple);
        count++;
    }

    // create the bins ahead
    size_vdb bins = count / _k + (count % _k == 0 ? 0 : 1); // we made the assumption that each bin will have at least k entries;
    for (pos_vdb i = 0; i < bins; i++) {
        _binHolder.push_back(_controller->create(_controller->getMachineID(), _tableID, i, 1, _child->getSchema()));
    }

    pos_vdb nextBinPosition = 0;
    for (pos_vdb i = 0; i < count; i++) {
        auto tupleHolder = allTuples->at(i);

        HashNumber hash = 0;
        for (auto gb : _groupBys) {
            hash = (hash << 1) ^ (*tupleHolder)[gb].hash();
        }


        if (_binTranslator.find(hash) == _binTranslator.end()) {
            /**
             * This is a hack.
             * Finding a very good hash function is very difficult
             * so we use this to match the hash number to the pre-allocated bins.
             */
            _binTranslator[hash] = nextBinPosition;
            nextBinPosition++;
        }

        if (_binTranslator[hash] == _binHolder.size()) {
            /**
             * This is a second hack that ensures we can run the test queries.
             * In a production environment, HB should vet for queries that may add bins
             * later -- which fail the k-ano criteria and should be rejected
             */
            if (_createBinOnDemand) {
                _binHolder.push_back(_controller->create(_controller->getMachineID(), _tableID, (size_vdb) _binHolder.size(), 1, _child->getSchema()));
            } else {
#if defined(LOCAL_DEBUG_MODE) || defined(OUTSIDE_COMPILATION)
                printf("[FAILURE] adding new bins is not permitted for this KAnonymousPullAndBin.\n");
#else
                ocall_print_string(std::string("<><><> [FAILURE] adding new bins is not permitted for this KAnonymousPullAndBin. <><><>\n").c_str());
#endif
                abort();
            }
        } else if (_binTranslator[hash] > _binHolder.size()) {
#if defined(LOCAL_DEBUG_MODE) || defined(OUTSIDE_COMPILATION)
            printf("[FAILURE] bin translator calls position larger than _binHolder.size()\n");
#else
            ocall_print_string(std::string("<><><> [FAILURE] bin translator calls position larger than _binHolder.size() <><><>\n").c_str());
#endif
            abort();
        }

        auto tableHolder = _binHolder[_binTranslator[hash]];
        _controller->insertTuple(tableHolder, *tupleHolder);
    }

    _controller->erase(_controller->getMachineID(), _tableID + 1, DEFAULT_TABLE_PARTITION_ID);
    _binIterator = _binHolder.begin(); // here isFetched is already false
};

bool KAnonymousPullAndBin::hasMore() {
    if (_isFetched) {
        do {
            _binIterator ++;
        } while (_binIterator != _binHolder.end() && (*_binIterator)->size() == 0);
        _isFetched = false;
    }
    return _binIterator != _binHolder.end();
};

db::obdata::ObliviousTupleTable * KAnonymousPullAndBin::get() {
    if (!_isFetched) {
        _isFetched = true;
    }
    return *_binIterator;
};

std::vector<pos_vdb> KAnonymousPullAndBin::getGroupByColumns() const {
    return _groupBys;
};