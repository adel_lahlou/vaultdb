#include <in_sgx/plan/obexpressions/ObliviousExpressionFactory.h>
#include <in_sgx/obdata/ObliviousDataFactory.h>
#include "in_sgx/plan/kaoperators/ClusterKAnonymousBinnedAggregation.h"
#include <in_sgx/plan/kaoperators/ClusterKAnonymousRepartition.h>
#include <in_sgx/obdata/ObliviousTuple.h>
#include <in_sgx/plan/obexpressions/Aggregates.h>

using namespace plan::kaoperators;
using namespace db::obdata;

ClusterKAnonymousBinnedAggregation::ClusterKAnonymousBinnedAggregation(MachineID machineId, TableID opId,
                                                                       TransmitterID transmitterID, size_vdb k,
                                                                       KAnonymousOperatorPtr child,
                                                                       obexpressions::ObliviousExpressionList exps,
                                                                       std::vector<pos_vdb> groupBys,
                                                                       bool isRepartitionSorting,
                                                                       bool createBinOnDemand,
                                                                       utilities::InSGXController *dispatcher)
        : KAnonymousOperator(
            opId,
            child->getSchema(),
            k,
            true,
            KAnonymousOperatorPtrList{}),
          ClusterOperator(machineId, transmitterID, dispatcher, dispatcher == nullptr ? 0 : dispatcher->getNumberOfMachines()),
          _pullAndBin(opId + 1, nullptr, k, groupBys, createBinOnDemand, dispatcher),
          _exps(exps),
          _isRepartitionSorting(isRepartitionSorting),
          _createBinOnDemand(createBinOnDemand)
{
    /**
     * Aggregations do not observe the generalization level; we enforce zeros so that repartition doesn't derive it.
     */
    std::vector<size_vdb> imposedGeneralizationLevels;
    for (pos_vdb i = 0, size = (size_vdb) child->getSchema().size(); i < size; i++) {
        imposedGeneralizationLevels.push_back(0);
    }
    _children.push_back(new ClusterKAnonymousRepartition(machineId, 0, transmitterID, k, child, std::vector<pos_vdb>(groupBys), {},
                                                         imposedGeneralizationLevels, isRepartitionSorting, dispatcher));
    _children[0]->setParent(this);
    _pullAndBin.setInput(_children[0]);

    _curAggTuple = new ObliviousTuple;
    *_curAggTuple = ObliviousDataFactory::createDefaultTuple(_outSchema);
    _exps.setObliviousTuple(_curAggTuple);
    _outSchema = type::RecordSchema(opId, _exps.outputDatatype());
    _exps.setObliviousTuple(_curAggTuple);

    for(int i = 0, size = _exps.size(); i < size; ++i) {
        if(_exps[i].isAggregateFunction()){
            _aggs.push_back(plan::obexpressions::AggregateFunction(_exps[i]));
        }
    }
};

ClusterKAnonymousBinnedAggregation::~ClusterKAnonymousBinnedAggregation() {
    delete _curAggTuple;
};

OperatorStatus ClusterKAnonymousBinnedAggregation::next()
{
    if(_status == OperatorStatus::NotInitialized) {
        _pullAndBin.pullAndBin();
        _status = OperatorStatus::Ok;
    } else if (_status == OperatorStatus::NoMoreTuples) {
        return _status;
    }
    if (_pullAndBin.hasMore()) {
        auto table = _pullAndBin.get();
        bool isAllDummy = true;
        _aggs.reset(true);
        for (pos_vdb i = 0, size = table->size(); i < size; i++) {
            *_curAggTuple = *table->at(i);
            _aggs.update(!_curAggTuple->isDummy());
            isAllDummy &= _curAggTuple->isDummy();
        }
        _currentTuple = _exps.evaluate();
        _currentTuple.setDummyFlag(isAllDummy);
        return _status;
    } else {
        _status = OperatorStatus::NoMoreTuples;
    }

    return OperatorStatus::NoMoreTuples;
}

void ClusterKAnonymousBinnedAggregation::reset() {
    KAnonymousOperator::reset();
    _aggs.reset(true);
}

size_vdb ClusterKAnonymousBinnedAggregation::getGeneralizationLevel(pos_vdb index) const {
    return 0;
};

OperatorStatus ClusterKAnonymousBinnedAggregation::receiveCallBack(MachineID src_machine_id, TableID src_tid, db::obdata::ObliviousTupleTable *table) {
    return ((ClusterOperator *) _children[0])->receiveCallBack(src_machine_id, src_tid, table);
};

size_vdb ClusterKAnonymousBinnedAggregation::getSerializationOverHead(std::set<TableID> &baseTables) const {
    size_vdb ret = 5 + sizeof(TableID) + sizeof(size_vdb);
    ret += _children[0]->getSerializationOverHead(baseTables);
    ret += _pullAndBin.getGroupByColumns().size();
    for (pos_vdb i = 0, outputSize = (pos_vdb)_exps.size(); i < outputSize; i++) {
        ret += _exps[i].encodeSize();
    }
    ret += sizeof(TransmitterID);
    return ret;
};

void ClusterKAnonymousBinnedAggregation::encodeOperator(unsigned char *&writeHead) const {
    // the type of operator it is
    *writeHead = (unsigned char) serialization::OperatorCode::KANO_CLUSTER_BINNED_AGGREGATION;
    writeHead++;

    // the number of child
    *writeHead = (unsigned char) 1;
    writeHead++;

    // child
    _children[0]->getChild(0)->encodeOperator(writeHead);

    // operatorID
    *(TableID*)writeHead = _operatorId;
    writeHead += sizeof(TableID);

    // k
    *(size_vdb*)writeHead = _k;
    writeHead += sizeof(size_vdb);

    // extra info: order bys, outputs, isRepartitionSorting
    std::vector<pos_vdb> columns = _pullAndBin.getGroupByColumns();
    pos_vdb groupBySize = (pos_vdb)columns.size();
    *writeHead = (unsigned char) groupBySize;
    writeHead++;
    for (pos_vdb i = 0; i < groupBySize; i++) {
        *writeHead = (unsigned char) columns[i];
        writeHead++;
    }
    pos_vdb outputSize = (pos_vdb)_exps.size();
    *writeHead = (unsigned char) outputSize;
    writeHead++;
    for (pos_vdb i = 0; i < outputSize; i++) {
        _exps[i].encodeObliviousExpression(writeHead);
    }

    *(TransmitterID *)writeHead = _children[0]->getOutputTableID();
    writeHead += sizeof(TransmitterID);

    *writeHead = _isRepartitionSorting ? 0x01 : 0x00;
    *writeHead |= _createBinOnDemand ? 0x02 : 0x00;
    writeHead++;
};