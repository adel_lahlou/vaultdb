#include <in_sgx/serialization/ObliviousSerializer.h>
#include <in_sgx/obdata/ObliviousDataFactory.h>
#include <in_sgx/plan/obexpressions/ObliviousExpressionFactory.h>
#include <in_sgx/plan/kaoperators/KAnonymousSeqScanMarkNonDummy.h>
#include <stack>
#include <algorithm>
#include <in_sgx/plan/kasupport/KAnonymousGeneralizer.h>
#include <in_sgx/plan/kaoperators/KAnonymousSeqScan.h>
#include <in_sgx/plan/kaoperators/KAnonymousBitonicSort.h>
#include "in_sgx/plan/kaoperators/ClusterKAnonymousRepartition.h"
#include "in_sgx/utilities/InSGXController.h"

using namespace plan::kaoperators;
using namespace plan::obexpressions;
using namespace db::obdata;
using namespace type;

ClusterKAnonymousRepartition::ClusterKAnonymousRepartition(MachineID machineId, TableID opID,
                                                           TransmitterID transmitterID, size_vdb k,
                                                           KAnonymousOperatorPtr child,
                                                           std::vector<pos_vdb> controlFlowAttributes,
                                                           std::vector<pos_vdb> entityIdentifiers,
                                                           std::vector<size_vdb> imposedGenLevels,
                                                           bool isSortingFirst,
                                                           utilities::InSGXController *dispatcher)
        : KAnonymousOperator(
            opID == 0 ? transmitterID : opID,
            child->getSchema(),
            k,
            true,
            KAnonymousOperatorPtrList{child}),
          ClusterOperator(machineId, transmitterID, dispatcher, dispatcher == nullptr ? 0 : dispatcher->getNumberOfMachines()),
          _controlFlowAttributes(controlFlowAttributes),
          _entityIdentifiers(entityIdentifiers),
          _mergedTable(nullptr),
          _partitioner(dispatcher == nullptr ? 0 : dispatcher->getNumberOfMachines(), _transmitterID, _controlFlowAttributes),
          _pos (INIT_TABLE_INDEX),
          _isDerivingGenLevel(imposedGenLevels.empty()),
          _isSortingFirst(isSortingFirst)
{
    if (_localDispatcher != nullptr) {
        _mergedTable = _localDispatcher->create(_localMachineID, _operatorId + 1, DEFAULT_TABLE_PARTITION_ID, 1,
                                                _outSchema);

        if (!imposedGenLevels.empty()) {
            for (pos_vdb i = 0, size = (size_vdb) _outSchema.size(); i < size; i ++) {
                _generalizationLevels.push_back(imposedGenLevels[i]);
            }
        }

        // Dispatch requests
        for (int i = 1, end = _totalMachineCount; i <= end; i++) {
            dispatcher->requestTable(i, _transmitterID, _localMachineID, this);
        }
    }

};

ClusterKAnonymousRepartition::~ClusterKAnonymousRepartition() {
    if (_localDispatcher != nullptr) {
        _localDispatcher->erase(_localMachineID, _operatorId + 1, DEFAULT_TABLE_PARTITION_ID);
    }
};

size_vdb ClusterKAnonymousRepartition::getTableCount() {
    std::unique_lock<std::mutex> guard(_tablesMutex);
    return (size_vdb) _tablePartCounts.size();
}

OperatorStatus ClusterKAnonymousRepartition::receiveCallBack(MachineID src_machine_id, TableID src_tid, db::obdata::ObliviousTupleTable *table) {
    std::unique_lock<std::mutex> localGuard(_tablesMutex);
    if (src_tid == _transmitterID) {
        _mergedTable->appendMove(*table);
        _localDispatcher->erase(src_machine_id, src_tid, table->getParitionID());
        _tablePartCounts.emplace(src_machine_id, 1);
    } else {
        return OperatorStatus::FatalError;
    }
    return OperatorStatus::Ok;
};

OperatorStatus ClusterKAnonymousRepartition::sortHashAndDispatch() {

    type::RecordSchema oldSchema(_outSchema);
    type::RecordSchema newSchema(_outSchema);
    newSchema.addField(type::SchemaColumn{FieldDataType::Int, sizeof(int)});

    KAnonymousOperatorPtr input = _children[0];
    ObliviousTuple inputTuple;
    ObliviousTupleTable * tempTable = _localDispatcher->create(_localMachineID, _operatorId + 2, 0, _totalMachineCount,
                                                               newSchema);
    while (input->next() == OperatorStatus::Ok) {
        input->getCurrentTuple(inputTuple);
        MachineID dst_id = (MachineID) _partitioner.hashSelectObliviousFields(inputTuple) + 1; // Add 1 because HB is always 0
        inputTuple.push_back(obfield::ObliviousField::makeObliviousIntField(dst_id));
        _localDispatcher->insertTuple(tempTable, inputTuple);
    }

    pos_vdb siteInfoPos = newSchema.size() - 1;

    std::vector<pos_vdb> orderbys(_controlFlowAttributes);
    orderbys.push_back(siteInfoPos);
    std::vector<SortOrder> sortOrders;
    for (auto a : orderbys) {
        sortOrders.push_back(SortOrder::ASCEND);
    }

    auto sort = new KAnonymousBitonicSort(_operatorId + 3,_k,
                                          new KAnonymousSeqScan(_operatorId,_k,tempTable),orderbys,sortOrders,_localDispatcher);
    std::map<MachineID, ObliviousTupleTable*> tuplesOfEachMachine;
    for (pos_vdb i = 1; i <= _totalMachineCount; i++) {
        tuplesOfEachMachine[i] = _localDispatcher->create(_localMachineID, _transmitterID, i, _totalMachineCount,
                                                          oldSchema);
    }
    int i = 0;
    while (sort->next() == OperatorStatus::Ok) {
        sort->getCurrentTuple(inputTuple);
        MachineID dst_id = (MachineID)inputTuple[siteInfoPos].hash(); // this is a hack; since the field value is likely small, the hash value will likely just be the actual integer value
        inputTuple.erase(siteInfoPos);
        _localDispatcher->insertTuple(_localMachineID, _transmitterID, dst_id, inputTuple);
        i++;
    }
        _localDispatcher->erase(_localMachineID, _operatorId + 2, 0);
    delete sort;

    oldSchema.setTableID(_transmitterID);
    return dispatchAll(tuplesOfEachMachine, oldSchema);
}

OperatorStatus ClusterKAnonymousRepartition::dispatchAll(
        std::map<MachineID, ObliviousTupleTable *> &tuplesOfEachMachine,
        type::RecordSchema &dispatchSchema) {
    // Dispatch all; fill later
    std::unique_ptr<unsigned char> buffer(nullptr);
    for (auto p : tuplesOfEachMachine) {
        p.second->setSchema(dispatchSchema);
        if (p.first == _localMachineID) {
            receiveCallBack(_localMachineID, _transmitterID, p.second);
            continue;
        }
        size_vdb blockSize = GET_NEAREST_CACHELINE_MULTIPLE(p.second->serializationSize());
        buffer.reset(new unsigned char[blockSize]);
        p.second->serializeTo(buffer.get());
        _localDispatcher->dispatch(p.first, _transmitterID, blockSize, buffer.get());
        _localDispatcher->erase(_localMachineID, _transmitterID, p.first);
    }

    _status = OperatorStatus::WaitingForTuples;
    return _status;
};

OperatorStatus ClusterKAnonymousRepartition::hashAndDispatch() {
    KAnonymousOperatorPtr input = _children[0];
    ObliviousTuple inputTuple;
    type::RecordSchema schema(this->getSchema());

    // hash all values into bins
    std::map<MachineID, ObliviousTupleTable*> tuplesOfEachMachine;
    for (pos_vdb i = 1; i <= _totalMachineCount; i++) {
        tuplesOfEachMachine[i] = _localDispatcher->create(_localMachineID, _transmitterID, i, _totalMachineCount,
                                                          schema);
    }
    while (input->next() == OperatorStatus::Ok) {
        input->getCurrentTuple(inputTuple);
        MachineID dst_id = (MachineID) _partitioner.hashSelectObliviousFields(inputTuple) + 1; // Add 1 because HB is always 0
        _localDispatcher->insertTuple(_localMachineID, _transmitterID, dst_id, inputTuple);
    }

    schema.setTableID(_transmitterID);
    return dispatchAll(tuplesOfEachMachine, schema);
};

void ClusterKAnonymousRepartition::acquireGeneralizationLevels()
{
    if (_generalizationLevels.empty()) {
        _generalizationLevels = KAnonymousGeneralizer(_operatorId + 2, _k, _outSchema, _mergedTable,
                                                      _controlFlowAttributes, _entityIdentifiers,
                                                      _localDispatcher).computeAndGetGeneralizationLevel();
    }
};

size_vdb ClusterKAnonymousRepartition::getGeneralizationLevel(pos_vdb index) const
{
    return _generalizationLevels[index];
};


OperatorStatus ClusterKAnonymousRepartition::next() {
    if (_status == OperatorStatus::NotInitialized) {
        if (_isSortingFirst) {
            sortHashAndDispatch();
        } else {
            hashAndDispatch();
        }
        while (_status == OperatorStatus::WaitingForTuples) {
            if (getTableCount() ==  _totalMachineCount) {
                // Fill to k
                if (_mergedTable->size() < _k) {
                    for (pos_vdb i = 0, fill = _k - _mergedTable->size(); i < fill; i++) {
                        ObliviousTuple t = ObliviousDataFactory::createDefaultDummyTuple(_outSchema);
                        _localDispatcher->insertTuple(_mergedTable, t);
                    }
                }
                if (_isDerivingGenLevel) {
                    acquireGeneralizationLevels();
                }
                break;
            };
        }
    } else if (_status == OperatorStatus::NoMoreTuples) {
        return _status;
    }

    // move in for the next
    _pos++;


    if (_pos <= _mergedTable->size()) {
        _currentTuple = (*_mergedTable)[GET_TABLE_READ_POSITION(_pos)];
        _status = OperatorStatus::Ok;
        return OperatorStatus::Ok;
    }
    _status = OperatorStatus::NoMoreTuples;
    return _status;
}

void ClusterKAnonymousRepartition::reset() {
    std::unique_lock<std::mutex> guard(_tablesMutex);
    _pos = INIT_TABLE_INDEX;
    _status = OperatorStatus :: Ok;
}


size_vdb ClusterKAnonymousRepartition::getTotalTupleCount() {
    return _mergedTable->size();
};


size_vdb ClusterKAnonymousRepartition::getSerializationOverHead(std::set<TableID> &baseTables) const {
    // opcode, child#, control, entity
    return 4 + sizeof(TableID) + sizeof(size_vdb) + sizeof(TransmitterID)
           + _children[0]->getSerializationOverHead(baseTables)
           + _controlFlowAttributes.size() + _entityIdentifiers.size()
          // imposed generalization Levels
          + 1 + _generalizationLevels.size()
            // the two flags
            + 1;
};

void ClusterKAnonymousRepartition::encodeOperator(unsigned char *&writeHead) const
{
    // the type of operator it is
    *writeHead = (unsigned char) serialization::OperatorCode::KANO_CLUSTER_REPARTITION;
    writeHead++;

    // the number of child
    *writeHead = (unsigned char) 1;
    writeHead++;

    // any child
    _children[0]->encodeOperator(writeHead);

    // tableID
    *(TableID*)writeHead = _operatorId;
    writeHead += sizeof(TableID);

    // extra info: k, control flow, entities
    *(size_vdb*)writeHead = _k;
    writeHead += sizeof(size_vdb);

    *(TransmitterID*) writeHead = _transmitterID;
    writeHead += sizeof(TransmitterID);

    pos_vdb size = (pos_vdb) _controlFlowAttributes.size();
    *writeHead = (unsigned char) size;
    writeHead++;
    for (pos_vdb i = 0; i < size; i++) {
        *writeHead = (unsigned char)_controlFlowAttributes[i];
        writeHead++;
    }
    size = (pos_vdb) _entityIdentifiers.size();
    *writeHead = (unsigned char) size;
    writeHead++;
    for (pos_vdb i = 0; i < size; i++) {
        *writeHead = (unsigned char)_entityIdentifiers[i];
        writeHead++;
    }

    // imposed generalization Levels
    *writeHead = (unsigned char)_generalizationLevels.size();
    writeHead ++;
    for (auto a : _generalizationLevels) {
        *writeHead = (unsigned char)a;
        writeHead ++;
    }

    // the two flags
    unsigned char flags = (unsigned char) (_isSortingFirst ? 0x01 : 0x00);
    *writeHead = flags;
    writeHead++;
}
