#include <shared/QueryEncodingDefinitions.h>
#include <set>
#include "in_sgx/plan/kaoperators/KAnonymousSeqScan.h"

using namespace plan::kaoperators;
using namespace db::obdata;

KAnonymousSeqScan::KAnonymousSeqScan(TableID opId, size_vdb k, db::obdata::ObliviousTupleTable * inputTable)
        : KAnonymousOperator(opId, inputTable->getSchema(), k, false, KAnonymousOperatorPtrList{}),
          _inputTable(inputTable)
{}

KAnonymousSeqScan::~KAnonymousSeqScan() {}


OperatorStatus KAnonymousSeqScan::next()
{
    if(_scanPos >= _inputTable->size()) {
        _status = OperatorStatus::NoMoreTuples;
    } else {
        _currentTuple = *_inputTable->at(_scanPos);
        _scanPos++;
        _status = OperatorStatus::Ok;
    }

    return _status;
}

void KAnonymousSeqScan::reset()
{
    _scanPos = 0;
    KAnonymousOperator::reset();
}

size_vdb KAnonymousSeqScan::getGeneralizationLevel(pos_vdb index) const
{
    return DEFAULT_MAX_GEN_LEVEL;
};

size_vdb KAnonymousSeqScan::getSerializationOverHead(std::set<TableID> &baseTables) const {
    baseTables.insert(_inputTable->getTableID());
    return 2 + sizeof(TableID) + sizeof(size_vdb) + sizeof(TableID);
};

void KAnonymousSeqScan::encodeOperator(unsigned char *&writeHead) const
{
    // the type of operator it is
    *writeHead = (unsigned char) serialization::OperatorCode::KANO_SCAN;
    writeHead++;

    // the number of child
    *writeHead = (unsigned char) 0;
    writeHead++;

    // any child

    // operator id
    *(TableID*)writeHead = _operatorId;
    writeHead += sizeof(TableID);

    // extra info: k
    *(size_vdb*)writeHead = _k;
    writeHead += sizeof(size_vdb);

    // tableID
    *(TableID*)writeHead = _inputTable->getTableID();
    writeHead += sizeof(TableID);
};