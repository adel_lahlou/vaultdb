#include <in_sgx/obdata/ObliviousDataFactory.h>
#include <in_sgx/obdata/BitonicSorter.h>
#include <in_sgx/utilities/SortHelper.h>
#include <shared/QueryEncodingDefinitions.h>
#include "in_sgx/plan/kaoperators/ClusterKAnonymousColumnSort.h"

using namespace plan::kaoperators;

ClusterKAnonymousColumnSort::ClusterKAnonymousColumnSort(MachineID machineId, TableID opId, TableID transmitterID,
                                                         size_vdb k, KAnonymousOperatorPtr child,
                                                         std::vector<pos_vdb> orderByEntries,
                                                         std::vector<SortOrder> sortOrders,
                                                         size_vdb blockTupleCount,
                                                         utilities::InSGXController *dispatcher)
        : KAnonymousOperator(opId, child->getSchema(), k, true, KAnonymousOperatorPtrList{child}),
          ClusterOperator(machineId, transmitterID, dispatcher, dispatcher == nullptr ? 0 : dispatcher->getNumberOfMachines()),
          _orderByEntries(orderByEntries), _sortOrders(sortOrders),
          _pos(0), _finalTupleBuffer(nullptr), _r(0), _tupleBlockSize(blockTupleCount), _defaultTransmissionSchema(child->getSchema())
{
    _defaultTransmissionSchema.setTableID(_transmitterID);
};

ClusterKAnonymousColumnSort::~ClusterKAnonymousColumnSort() {
    if (_localDispatcher != nullptr && _finalTupleBuffer != nullptr) {
        _localDispatcher->erase(_localMachineID, _transmitterID, DEFAULT_TABLE_PARTITION_ID);
        _finalTupleBuffer = nullptr;
    }
};

OperatorStatus ClusterKAnonymousColumnSort::next() {
    if (_status == OperatorStatus::NotInitialized) {
        init();
    } else if (_status == OperatorStatus::NoMoreTuples) {
        return _status;
    }
    _pos++;
    if (_pos <= _finalTupleBuffer->size()) {
        _currentTuple = (*_finalTupleBuffer)[GET_TABLE_READ_POSITION(_pos)];
        _status = OperatorStatus::Ok;
        return OperatorStatus::Ok;
    }
    _status = OperatorStatus::NoMoreTuples;
    return _status;
};

size_vdb ClusterKAnonymousColumnSort::getReceivedTableCount() {
    std::unique_lock<std::mutex> guard(_tablesMutex);
    return _partitionReceiveCount;
};

size_vdb ClusterKAnonymousColumnSort::getReceivedR() {
    std::unique_lock<std::mutex> guard(_tablesMutex);
    return _r;
};

OperatorStatus
ClusterKAnonymousColumnSort::receiveCallBack(MachineID src_machine_id, TableID src_tid, db::obdata::ObliviousTupleTable *table) {
    std::unique_lock<std::mutex> localGuard(_tablesMutex);
    if (src_tid == _transmitterID) {
        // dispatched table
        auto newTablePtr = _localDispatcher->create(_localMachineID, src_tid, table->getParitionID(), _r, _defaultTransmissionSchema);
        newTablePtr->appendMove(*table);
        _partitionReceiveCount++;
        _localDispatcher->erase(src_machine_id, src_tid, table->getParitionID());
    } else {
        return OperatorStatus::FatalError;
    }
    return OperatorStatus::Ok;
};

OperatorStatus
ClusterKAnonymousColumnSort::receiveGenericMessage(MachineID src_machine_id, TableID src_tid, unsigned char *data, size_vdb msg_size) {
    std::unique_lock<std::mutex> localGuard(_tablesMutex);
    if (src_machine_id == 0 && src_tid == _transmitterID) {
        _r = *(size_vdb*)((Message*)data)->data;
    } else {
        return OperatorStatus::FatalError;
    }
    return OperatorStatus::Ok;
};


void ClusterKAnonymousColumnSort::determineR(size_vdb currentPartitionCount) {

    _localDispatcher->requestMessage(0, _transmitterID, this);
    size_vdb msgSize = sizeof(Message) + sizeof(size_vdb);
    unsigned char msg[msgSize];
    ((Message *)msg)->src_tid = _transmitterID;
    *(size_vdb*)((Message *)msg)->data = currentPartitionCount; // number of partition given block size;
    _localDispatcher->dispatchMessage(0, msgSize, msg);
    while (getReceivedR() == 0) {
        // busy wait
    }
}

void ClusterKAnonymousColumnSort::init() {
    /**
     * Here, reach consensus on how many tuples everyone has, and thus decide on r and s and block size
     * Perhaps let HB compute it.
     * There must be at least (_r * _s) tuples across all hosts;
     * otherwise space is not an issue and a merge_sort should be performed
     * FIXME _s should be the same as _totalMachineCount
     * Also, at this point everyone should have the same number of blocks
     * FIXME implement consensus on _r
     */
    pos_vdb nextPartitionID = DEFAULT_TABLE_PARTITION_ID;
    db::obdata::ObliviousTupleTable * currentPartition = _localDispatcher->create(_localMachineID, _transmitterID, nextPartitionID, _r, _defaultTransmissionSchema);
    nextPartitionID++;

    size_vdb remainingSlots = _tupleBlockSize;
    while (_children[0]->next() == OperatorStatus::Ok) {
        ObliviousTuple t;
        _children[0]->getCurrentTuple(t);

        if (remainingSlots == 0) {
            currentPartition = _localDispatcher->create(_localMachineID, _transmitterID, nextPartitionID, _r, _defaultTransmissionSchema);
            nextPartitionID++;
            remainingSlots = _tupleBlockSize;
        }
        _localDispatcher->insertTuple(currentPartition, t);
        remainingSlots--;
    }
    while(remainingSlots > 0) {
        ObliviousTuple t = db::obdata::ObliviousDataFactory::createDefaultDummyTuple(_defaultTransmissionSchema);
        _localDispatcher->insertTuple(currentPartition, t);
        remainingSlots--;
    }

    /**
     * _r should be decided here
     */
    auto a = 1;
    determineR(nextPartitionID);

    while (_r > nextPartitionID) {
        currentPartition = _localDispatcher->create(_localMachineID, _transmitterID, nextPartitionID, _r, _defaultTransmissionSchema);
        nextPartitionID++;
        remainingSlots = _tupleBlockSize;
        while(remainingSlots > 0) {
            ObliviousTuple t = db::obdata::ObliviousDataFactory::createDefaultDummyTuple(_defaultTransmissionSchema);
            _localDispatcher->insertTuple(currentPartition, t);
            remainingSlots--;
        }
    }

    /**
     * Step 1: Sort the local column. There should be '_r' number of blocks
     */
    sortLocalTuples(DEFAULT_TABLE_PARTITION_ID, DEFAULT_TABLE_PARTITION_ID + _r);
    /**
     * Step 2: transposes the mesh and then reshapes the resulting s × r matrix back into an r × s mesh
     *         by taking each row of r entries and mapping it to a consecutive set of r/s rows.
     */
    step2();
    /**
     * Step 3: Sort the local column. There should be '_nextPartitionID' number of blocks
     */
    sortLocalTuples(DEFAULT_TABLE_PARTITION_ID, DEFAULT_TABLE_PARTITION_ID + _r);
    /**
     * Step 4: performs the inverse of the permutation performed in step 2: treat each consecutive set of r/s
     *         rows as a single row in an s × r matrix, and transpose this matrix into the r × s mesh.
     */
    step4();
    /**
     * Step 5: Sort the local column. There should be '_nextPartitionID' number of blocks
     */
    sortLocalTuples(DEFAULT_TABLE_PARTITION_ID, DEFAULT_TABLE_PARTITION_ID + _r);
    /**
     * Step 6: shifts each column down by r/2 positions. That is, the bottom half of each column moves to
     *         the top half of the next column to the right, and the top half of each column moves to the bottom half
     *         of the column. The evacuated top half of the leftmost column (column 0) is filled with the value −inf.
     *         A new rightmost column (column s) is created, receiving the bottom half of column s − 1, and the
     *         bottom half of this new column is filled with the value inf
     */
    step6();
    /**
     * Step 7: Sort the local column. There should be '_nextPartitionID' number of blocks
     */
    if (_localMachineID == 1) {
        sortLocalTuples(DEFAULT_TABLE_PARTITION_ID, DEFAULT_TABLE_PARTITION_ID + _r / 2);
        sortLocalTuples(DEFAULT_TABLE_PARTITION_ID + _r / 2, DEFAULT_TABLE_PARTITION_ID + _r);
    } else {
        sortLocalTuples(DEFAULT_TABLE_PARTITION_ID, DEFAULT_TABLE_PARTITION_ID + _r);
    }
    /**
     * Step 8: performs the inverse of the permutation performed in step 6: shift each column up by r/2 positions
     */
    step8();
    /**
     * Collect everything into the same partition?
     * TODO when auto repartition management is available, change this
     */
    _finalTupleBuffer = mergePartitionToFirst(DEFAULT_TABLE_PARTITION_ID, DEFAULT_TABLE_PARTITION_ID + _r);
    /**
     * Cleaning up the other intermediate result tables
     */
    for (pos_vdb i = DEFAULT_TABLE_PARTITION_ID + 1; i < DEFAULT_TABLE_PARTITION_ID + _r; i++) {
        _localDispatcher->erase(_localMachineID, _transmitterID, i);
    }
};

db::obdata::ObliviousTupleTable * ClusterKAnonymousColumnSort::mergePartitionToFirst(pos_vdb start_pos, pos_vdb last_pos_plus_1) {
    auto firstPos = _localDispatcher->fetch(_localMachineID, _transmitterID, start_pos);
    // starting from 1 because we want to append everything to the firstPos (0th) partition
    for (pos_vdb i = start_pos + 1; i < last_pos_plus_1; i++) {
        firstPos->appendMove(*_localDispatcher->fetch(_localMachineID, _transmitterID, i));
    }
    return firstPos;
}

void ClusterKAnonymousColumnSort::sortLocalTuples(pos_vdb start_pos, pos_vdb last_pos_plus_1) {
    auto firstPos = mergePartitionToFirst(start_pos, last_pos_plus_1);
    BitonicSorter<db::obdata::ObliviousTuple, type::RecordSchema>(_orderByEntries, _sortOrders, _defaultTransmissionSchema, SortHelper::TupleComp).sort(firstPos->begin(), firstPos->end());
    for (pos_vdb i = last_pos_plus_1 - 1; i > start_pos; i--) {
        db::obdata::ObliviousTupleTable::CutOffLastN(_tupleBlockSize, *firstPos, *_localDispatcher->fetch(_localMachineID, _transmitterID, i));
    }
};

void ClusterKAnonymousColumnSort::step2() {
    /**
     * Step 2: transposes the mesh and then reshapes the resulting s × r matrix back into an r × s mesh
     *         by taking each row of r entries and mapping it to a consecutive set of r/s rows.
     *
     * Note: This means that, fill the top _r / _s rows with the transposed column of that of machine 1,
     *       second _r / _s rows with that of machine 2, etc.
     */
    // ship
    _status = OperatorStatus::WaitingForTuples;
    // dispatch
    auto localGuard = new std::unique_lock<std::mutex> (_tablesMutex);
    _partitionReceiveCount = 0;
    _expectedPartitionReceiveCount = _r - _r / _totalMachineCount;
    for (pos_vdb i = DEFAULT_TABLE_PARTITION_ID; i < DEFAULT_TABLE_PARTITION_ID + _r; i++) {
        MachineID dst_machine_id = i % _totalMachineCount + 1;
        size_vdb dst_partition_id = _r / _totalMachineCount * (_localMachineID - 1) + i / _totalMachineCount;
        if (dst_machine_id != _localMachineID) {

            auto fetched = _localDispatcher->fetch(_localMachineID, _transmitterID, i);
            fetched->setParitionID(dst_partition_id);
            auto ssize = fetched->serializationSize();
            unsigned char data[ssize];
            fetched->serializeTo(data);
            _localDispatcher->dispatch(dst_machine_id, _transmitterID, ssize, data);
            _localDispatcher->erase(_localMachineID, _transmitterID, i);
        } else {
            // draft shift
            if (dst_partition_id == i) {
                continue;
            }
            auto oldPos = _localDispatcher->fetch(_localMachineID, _transmitterID, i);
            auto current = _localDispatcher->create(_localMachineID, _transmitterID, dst_partition_id + _r, _r,
                                                    _defaultTransmissionSchema);
            current->appendMove(*oldPos);
            _localDispatcher->erase(_localMachineID, _transmitterID, i);
        }
    }
    // actual shift
    for (pos_vdb i = DEFAULT_TABLE_PARTITION_ID; i < DEFAULT_TABLE_PARTITION_ID + _r; i++) {
        MachineID dst_machine_id = i % _totalMachineCount + 1;
        if (dst_machine_id == _localMachineID) {
            size_vdb dst_partition_id = _r / _totalMachineCount * (_localMachineID - 1) + i / _totalMachineCount;
            if (dst_partition_id == i) {
                continue;
            }
            auto oldPos = _localDispatcher->fetch(_localMachineID, _transmitterID, dst_partition_id + _r);
            auto current = _localDispatcher->create(_localMachineID, _transmitterID, dst_partition_id, _r,
                                                    _defaultTransmissionSchema);
            current->appendMove(*oldPos);
            _localDispatcher->erase(_localMachineID, _transmitterID, dst_partition_id + _r);
        }
    }
    delete localGuard;
    // place requests; this needs to be done after local copies are  deleted
    for (pos_vdb i = DEFAULT_TABLE_PARTITION_ID; i < DEFAULT_TABLE_PARTITION_ID + _r; i++) {
        MachineID src_machine_id = i / (_r / _totalMachineCount) + 1;
        if (src_machine_id != _localMachineID) {
            _localDispatcher->requestTable(src_machine_id, _transmitterID, i, this);
        }
    }
    // wait
    while (_status == OperatorStatus::WaitingForTuples) {
        if (getReceivedTableCount() == _expectedPartitionReceiveCount) {
            break;
        };
    }
    /**
     * Choose synchronization over increasing nextPartitionID to avoid impact to sortLocalTuples.
     */
    _localDispatcher->syncWithOtherHosts(2);
};

void ClusterKAnonymousColumnSort::step4() {
    /**
     * Step 4: performs the inverse of the permutation performed in step 2: treat each consecutive set of r/s
     *         rows as a single row in an s × r matrix, and transpose this matrix into the r × s mesh.
     */
    // ship
    _status = OperatorStatus::WaitingForTuples;
    // dispatch
    auto localGuard = new std::unique_lock<std::mutex> (_tablesMutex);
    _partitionReceiveCount = 0;
    _expectedPartitionReceiveCount = _r - _r / _totalMachineCount;
    for (pos_vdb i = DEFAULT_TABLE_PARTITION_ID; i < DEFAULT_TABLE_PARTITION_ID + _r; i++) {
        MachineID dst_machine_id = i / (_r / _totalMachineCount) + 1;
        size_vdb dst_partition_id = i % (_r / _totalMachineCount) * _totalMachineCount + _localMachineID - 1;
        if (dst_machine_id != _localMachineID) {
            auto fetched = _localDispatcher->fetch(_localMachineID, _transmitterID, i);
            fetched->setParitionID(dst_partition_id);
            auto ssize = fetched->serializationSize();
            unsigned char data[ssize];
            fetched->serializeTo(data);
            _localDispatcher->dispatch(dst_machine_id, _transmitterID, ssize, data);
            _localDispatcher->erase(_localMachineID, _transmitterID, i);
        } else {
            // draft shift
            if (dst_partition_id == i) {
                continue;
            }

            auto oldPos = _localDispatcher->fetch(_localMachineID, _transmitterID, i);
            auto current = _localDispatcher->create(_localMachineID, _transmitterID, dst_partition_id + _r, _r,
                                                    _defaultTransmissionSchema);
            current->appendMove(*oldPos);
            _localDispatcher->erase(_localMachineID, _transmitterID, i);
        }
    }
    // shift restore
    for (pos_vdb i = DEFAULT_TABLE_PARTITION_ID; i < DEFAULT_TABLE_PARTITION_ID + _r; i++) {
        MachineID dst_machine_id = i / (_r / _totalMachineCount) + 1;
        if (dst_machine_id == _localMachineID) {
            size_vdb dst_partition_id = i % (_r / _totalMachineCount) * _totalMachineCount + _localMachineID - 1;
            if (dst_partition_id == i) {
                continue;
            }

            auto oldPos = _localDispatcher->fetch(_localMachineID, _transmitterID, dst_partition_id + _r);
            auto current = _localDispatcher->create(_localMachineID, _transmitterID, dst_partition_id, _r,
                                                    _defaultTransmissionSchema);
            current->appendMove(*oldPos);
            _localDispatcher->erase(_localMachineID, _transmitterID, dst_partition_id + _r);
        }
    }
    delete localGuard;
    // place requests; this needs to be done after local copies are  deleted
    for (pos_vdb i = DEFAULT_TABLE_PARTITION_ID; i < DEFAULT_TABLE_PARTITION_ID + _r; i++) {
        MachineID src_machine_id = i % _totalMachineCount + 1;
        if (src_machine_id != _localMachineID) {
            _localDispatcher->requestTable(src_machine_id, _transmitterID, i, this);
        }
    }
    // wait
    while (_status == OperatorStatus::WaitingForTuples) {
        if (getReceivedTableCount() == _expectedPartitionReceiveCount) {
            break;
        };
    }
    /**
     * Choose synchronization over increasing nextPartitionID to avoid impact to sortLocalTuples.
     */
    _localDispatcher->syncWithOtherHosts(4);
};

void ClusterKAnonymousColumnSort::step6() {
    /**
     * Step 6: shifts each column down by r/2 positions. That is, the bottom half of each column moves to
     *         the top half of the next column to the right, and the top half of each column moves to the bottom half
     *         of the column. The evacuated top half of the leftmost column (column 0) is filled with the value −inf.
     *         A new rightmost column (column s) is created, receiving the bottom half of column s − 1, and the
     *         bottom half of this new column is filled with the value inf
     */
    // ship
    _status = OperatorStatus::WaitingForTuples;
    MachineID dst_machine_id = _localMachineID == _totalMachineCount ? 1 : _localMachineID + 1;
    // dispatch
    auto localGuard = new std::unique_lock<std::mutex> (_tablesMutex);
    _partitionReceiveCount = 0;
    _expectedPartitionReceiveCount = _r / 2;
    for (pos_vdb i = DEFAULT_TABLE_PARTITION_ID + _r / 2; i < DEFAULT_TABLE_PARTITION_ID + _r; i++) {
        auto fetched = _localDispatcher->fetch(_localMachineID, _transmitterID, i);
        size_vdb dst_partition_id = i - _r / 2;
        fetched->setParitionID(dst_partition_id);
        auto ssize = fetched->serializationSize();
        unsigned char data[ssize];
        fetched->serializeTo(data);
        _localDispatcher->dispatch(dst_machine_id, _transmitterID, ssize, data);
        _localDispatcher->erase(_localMachineID, _transmitterID, i);
    }
    // shift
    for (pos_vdb i = DEFAULT_TABLE_PARTITION_ID; i < DEFAULT_TABLE_PARTITION_ID + _r / 2; i++) {
        auto oldPos = _localDispatcher->fetch(_localMachineID, _transmitterID, i);
        auto current = _localDispatcher->create(_localMachineID, _transmitterID, i + _r / 2, _r, _defaultTransmissionSchema);
        current->appendMove(*oldPos);
        _localDispatcher->erase(_localMachineID, _transmitterID, i);
    }
    delete localGuard;
    // place requests
    MachineID src_machine_id = _localMachineID == 1 ? _totalMachineCount : _localMachineID - 1;
    for (pos_vdb i = DEFAULT_TABLE_PARTITION_ID; i < DEFAULT_TABLE_PARTITION_ID + _r / 2; i++) {
        _localDispatcher->requestTable(src_machine_id, _transmitterID, i, this);
    }
    // wait
    while (_status == OperatorStatus::WaitingForTuples) {
        if (getReceivedTableCount() == _expectedPartitionReceiveCount) {
            break;
        };
    }
    /**
     * Choose synchronization over increasing nextPartitionID to avoid impact to sortLocalTuples.
     */
    _localDispatcher->syncWithOtherHosts(6);
};

void ClusterKAnonymousColumnSort::step8() {
    /**
     * Step 8: performs the inverse of the permutation performed in step 6: shift each column up by r/2 positions
     */
    // ship
    _status = OperatorStatus::WaitingForTuples;
    MachineID dst_machine_id = _localMachineID == 1 ? _totalMachineCount : _localMachineID - 1;
    // dispatch
    auto localGuard = new std::unique_lock<std::mutex> (_tablesMutex);
    _partitionReceiveCount = 0;
    _expectedPartitionReceiveCount = _r / 2;
    for (pos_vdb i = DEFAULT_TABLE_PARTITION_ID; i < DEFAULT_TABLE_PARTITION_ID + _r / 2; i++) {
        auto fetched = _localDispatcher->fetch(_localMachineID, _transmitterID, i);
        size_vdb dst_partition_id = i + _r / 2;
        fetched->setParitionID(dst_partition_id);
        auto ssize = fetched->serializationSize();
        unsigned char data[ssize];
        fetched->serializeTo(data);
        _localDispatcher->dispatch(dst_machine_id, _transmitterID, ssize, data);
        _localDispatcher->erase(_localMachineID, _transmitterID, i);
    }
    // shift
    for (pos_vdb i = DEFAULT_TABLE_PARTITION_ID + _r / 2; i < DEFAULT_TABLE_PARTITION_ID + _r; i++) {
        auto oldPos = _localDispatcher->fetch(_localMachineID, _transmitterID, i);
        auto current = _localDispatcher->create(_localMachineID, _transmitterID, i - _r / 2, _r, _defaultTransmissionSchema);
        current->appendMove(*oldPos);
        _localDispatcher->erase(_localMachineID, _transmitterID, i);
    }
    delete localGuard;
    // place requests
    MachineID src_machine_id = _localMachineID == _totalMachineCount ? 1 : _localMachineID + 1;
    for (pos_vdb i = DEFAULT_TABLE_PARTITION_ID + _r / 2; i < DEFAULT_TABLE_PARTITION_ID + _r; i++) {
        _localDispatcher->requestTable(src_machine_id, _transmitterID, i, this);
    }
    // wait
    while (_status == OperatorStatus::WaitingForTuples) {
        if (getReceivedTableCount() == _expectedPartitionReceiveCount) {
            break;
        };
    }
    /**
     * Choose synchronization over increasing nextPartitionID to avoid impact to sortLocalTuples.
     */
    _localDispatcher->syncWithOtherHosts(8);
};

void ClusterKAnonymousColumnSort::reset() {
    _partitionReceiveCount = 0;
    _expectedPartitionReceiveCount = 0;
};

size_vdb ClusterKAnonymousColumnSort::getSerializationOverHead(std::set<TableID> &baseTables) const {
    return KAnonymousOperator::getSerializationOverHead(baseTables)
           + 3 + sizeof(TableID) + sizeof(size_vdb) /* k */ + sizeof(size_vdb) /* blockSize */ + 2 * _orderByEntries.size() + sizeof(TransmitterID) ;
};

void ClusterKAnonymousColumnSort::encodeOperator(unsigned char *&writeHead) const
{
    // the type of operator it is
    *writeHead = (unsigned char) serialization::OperatorCode::KANO_CLUSTER_COLUMN_SORT;
    writeHead++;

    // the number of child
    *writeHead = (unsigned char) 1;
    writeHead++;

    // any child
    _children[0]->encodeOperator(writeHead);

    // tableID
    *(TableID*)writeHead = _operatorId;
    writeHead += sizeof(TableID);

    // k
    *(size_vdb*)writeHead = _k;
    writeHead += sizeof(size_vdb);

    // extra info: transmitterID, block size, indices, orders
    *(TableID*)writeHead = _transmitterID;
    writeHead += sizeof(TableID);

    *(size_vdb*)writeHead = _tupleBlockSize;
    writeHead += sizeof(size_vdb);

    size_vdb size = (size_vdb)_orderByEntries.size();
    *writeHead = (unsigned char) size;
    writeHead++;
    for (pos_vdb i = 0; i < size; i++) {
        *writeHead = (unsigned char)_orderByEntries[i];
        writeHead++;
    }
    for (pos_vdb i = 0; i < size; i++) {
        *writeHead = (unsigned char)_sortOrders[i];
        writeHead++;
    }
}