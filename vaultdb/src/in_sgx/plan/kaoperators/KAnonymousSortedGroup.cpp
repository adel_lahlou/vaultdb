#include <in_sgx/plan/kaoperators/KAnonymousBitonicSort.h>
#include <in_sgx/utilities/SecureMove.h>
#include <shared/QueryEncodingDefinitions.h>
#include "in_sgx/plan/kaoperators/KAnonymousSortedGroup.h"
#include "in_sgx/obdata/ObliviousDataFactory.h"
#include "in_sgx/plan/obexpressions/ObliviousExpressionFactory.h"

using namespace plan::kaoperators;
using namespace plan::obexpressions;
using namespace db::obdata;


KAnonymousSortedGroup::KAnonymousSortedGroup(TableID opId, size_vdb k, KAnonymousOperatorPtr child,
                                             obexpressions::ObliviousExpressionList exps,
                                             std::vector<pos_vdb> groupBys,
                                             utilities::InSGXController *constroller)
        : KAnonymousOperator(
            opId,
            child->getSchema(),
            k,
            false,
            KAnonymousOperatorPtrList{}
        ),
        _exps(exps), _isCurrentGroupAllDummy(true), _isOperatorWindowAggregation(false)
{
    std::vector<pos_vdb> orderBys(groupBys);
    std::vector<SortOrder> sortOrders;
    for(int i = 0, size = (size_vdb)groupBys.size(); i < size; i++){
        sortOrders.push_back(SortOrder::ASCEND);
    }
    for (int i = 0, size = exps.size(); i < size; i++) {
        ObliviousExpression e = exps[i];
        if (e.getExpressionType() == serialization::ExpressionCode::COUNT_DIST) {
            std::vector<pos_vdb> refs;
            e.getInputReferences(refs);
            orderBys.push_back(refs[0]);
            sortOrders.push_back(SortOrder::ASCEND);
        } 
    }

    _children.push_back(
            new KAnonymousBitonicSort(opId + 1, k, child, orderBys, sortOrders, constroller)
    );

    _lastAggTuple = new ObliviousTuple;
    _curAggTuple = new ObliviousTuple;

    *_curAggTuple = ObliviousDataFactory::createDefaultTuple(_outSchema);
    _exps.setObliviousTuple(_curAggTuple);
    _outSchema = type::RecordSchema(opId, _exps.outputDatatype());

    _exps.setObliviousTuple(_curAggTuple);

    for(auto col : groupBys) {
        ObliviousExpression lhs = ObliviousExpressionFactory::makeColumn(col, _outSchema[col]);
        ObliviousExpression rhs = ObliviousExpressionFactory::makeColumn(col, _outSchema[col]);

        lhs.setObliviousTuple(_lastAggTuple);
        rhs.setObliviousTuple(_curAggTuple);
        ObliviousExpression eq = ObliviousExpressionFactory::makeEq(lhs, rhs);
        _checkInGroup.push_back(eq);
    }

    for(int i = 0, size = _exps.size(); i < size; ++i) {
        if(_exps[i].isAggregateFunction()){
            _aggs.push_back(AggregateFunction(_exps[i]));
        }
    }
}

KAnonymousSortedGroup::KAnonymousSortedGroup(
        TableID opId,
        size_vdb k,
        KAnonymousOperatorPtr child,
        bool isOperatorWindowAggregation
) : KAnonymousOperator(
        opId,
        child->getSchema(),
        k,
        false,
        KAnonymousOperatorPtrList{}
), _isOperatorWindowAggregation(isOperatorWindowAggregation) {};

KAnonymousSortedGroup::~KAnonymousSortedGroup() {
    delete _lastAggTuple;
    delete _curAggTuple;
}


OperatorStatus KAnonymousSortedGroup::next()
{
    if(_firstPull) {
        _firstPull = false;

        if(_children[0]->next() == OperatorStatus::Ok) {
            _children[0]->getCurrentTuple(*_curAggTuple);
            _aggs.update(!_curAggTuple->isDummy());
            _status = OperatorStatus::Ok;
        }
    }

    while (_children[0]->next() == OperatorStatus::Ok) {
        _countTowardK++;
        _nextMiddle();

        if (_countTowardK > _k && _currentTuple.isDummy()) {
            continue;
        }

        _status = OperatorStatus::Ok;
        return OperatorStatus::Ok;
    }

    if(_status == OperatorStatus::Ok && !_finishedPulling) {
        _countTowardK++;
        _nextEnd();
        _finishedPulling = true;
        if (_countTowardK > _k && _currentTuple.isDummy()) {
            _status = OperatorStatus::NoMoreTuples;
        } else {
            _status = OperatorStatus::Ok;
        }
        return _status;
    }

    _status = OperatorStatus::NoMoreTuples;
    return OperatorStatus::NoMoreTuples;
}

void KAnonymousSortedGroup::reset() {
    KAnonymousOperator::reset();
    _aggs.reset(true);
    _firstPull = true;
    _finishedPulling = false;
    _isCurrentGroupAllDummy = true;
}

size_vdb KAnonymousSortedGroup::getGeneralizationLevel(pos_vdb index) const {
    return 0;
};

void KAnonymousSortedGroup::_nextMiddle()
{
    *_lastAggTuple = *_curAggTuple;
    _isCurrentGroupAllDummy &= _lastAggTuple->isDummy();
    _children[0]->getCurrentTuple(*_curAggTuple);

    bool isNewAgg = !_checkInGroup.isAllTruthy();
    _evaluate();

    _currentTuple.setDummyFlag(!(isNewAgg || _isOperatorWindowAggregation) || _isCurrentGroupAllDummy );

    _aggs.reset(isNewAgg);
    _aggs.update(!_curAggTuple->isDummy());
    _isCurrentGroupAllDummy |= isNewAgg || _isOperatorWindowAggregation;

    _countTowardK = (size_vdb) utilities::cmov_int((int)!_checkInGroup.isAllTruthy(), 0, _countTowardK);
}

void KAnonymousSortedGroup::_nextEnd()
{
    _currentTuple = _exps.evaluate();
    _currentTuple.setDummyFlag(_currentTuple.isDummy() || (_curAggTuple->isDummy() && _isCurrentGroupAllDummy));
}

void KAnonymousSortedGroup::_evaluate()
{
    ObliviousTuple tmp = *_curAggTuple;
    *_curAggTuple = *_lastAggTuple;
    _currentTuple = _exps.evaluate();
    *_curAggTuple = tmp;
}

ObliviousExpressionList KAnonymousSortedGroup::getExpressions() const
{
    return _exps;
};

size_vdb KAnonymousSortedGroup::getSerializationOverHead(std::set<TableID> &baseTables) const {
    size_vdb ret = 4 + sizeof(TableID) + sizeof(size_vdb);
    ret += _children[0]->getSerializationOverHead(baseTables);
    ret += ((KAnonymousBitonicSort*)_children[0])->getOrderColumns().size();
    for (pos_vdb i = 0, outputSize = (pos_vdb)_exps.size(); i < outputSize; i++) {
        ret += _exps[i].encodeSize();
    }
    return ret;
};

void KAnonymousSortedGroup::encodeOperator(unsigned char *&writeHead) const
{
    // the type of operator it is
    *writeHead = (unsigned char) serialization::OperatorCode::KANO_AGGREGATE;
    writeHead++;

    // the number of child
    *writeHead = (unsigned char) 1;
    writeHead++;

    // child: child of the artificially added BitonicSort
    KAnonymousBitonicSort * sort = (KAnonymousBitonicSort*)_children[0];
    sort->getChild(0)->encodeOperator(writeHead);

    // operatorID
    *(TableID*)writeHead = _operatorId;
    writeHead += sizeof(TableID);

    // k
    *(size_vdb*)writeHead = _k;
    writeHead += sizeof(size_vdb);

    // extra info: order bys, outputs
    std::vector<pos_vdb> columns = sort->getOrderColumns();
    pos_vdb groupBySize = (pos_vdb)columns.size();
    *writeHead = (unsigned char) groupBySize;
    writeHead++;
    for (pos_vdb i = 0; i < groupBySize; i++) {
        *writeHead = (unsigned char) columns[i];
        writeHead++;
    }
    pos_vdb outputSize = (pos_vdb)_exps.size();
    *writeHead = (unsigned char) outputSize;
    writeHead++;
    for (pos_vdb i = 0; i < outputSize; i++) {
        _exps[i].encodeObliviousExpression(writeHead);
    }
}