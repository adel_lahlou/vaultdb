#include <shared/QueryEncodingDefinitions.h>
#include "in_sgx/plan/kaoperators/KAnonymousLimit.h"

using namespace plan::kaoperators;

KAnonymousLimit::KAnonymousLimit(
        TableID opId,
        size_vdb k,
        size_vdb tupleLimit,
        KAnonymousOperatorPtrList children
) : KAnonymousOperator(
        opId,
        children[0]->getSchema(),
        k,
        false,
        children
), _tupleLimit(tupleLimit), _outputTupleCount(0)
{};

KAnonymousLimit::~KAnonymousLimit() {};

OperatorStatus KAnonymousLimit::next() {
    _status = OperatorStatus :: NoMoreTuples;
    if (_outputTupleCount < _tupleLimit) {
        while (_children[0]->next() != OperatorStatus::NoMoreTuples) {
            _children[0]->getCurrentTuple(_currentTuple);
            if (!_currentTuple.isDummy()) {
                _outputTupleCount++;
                _status = OperatorStatus::Ok;
                break;
            }
        }
    }

    return _status;
};

void KAnonymousLimit::reset()
{
    KAnonymousOperator::reset();
    _outputTupleCount = 0;
};

size_vdb KAnonymousLimit::getSerializationOverHead(std::set<TableID> &baseTables) const {
    return 2 + sizeof(TableID) + sizeof(size_vdb)
           + _children[0]->getSerializationOverHead(baseTables)
           + sizeof(size_vdb);
};

void KAnonymousLimit::encodeOperator(unsigned char *&writeHead) const
{
    // the type of operator it is
    *writeHead = (unsigned char) serialization::OperatorCode::KANO_LIMIT;
    writeHead++;

    // the number of child
    *writeHead = (unsigned char) 1;
    writeHead++;

    // any child
    _children[0]->encodeOperator(writeHead);

    // tableID
    *(TableID*)writeHead = _operatorId;
    writeHead += sizeof(TableID);

    // extra info: k, limit number
    *(size_vdb*)writeHead = _k;
    writeHead += sizeof(size_vdb);

    *(size_vdb*)writeHead = _tupleLimit;
    writeHead += sizeof(size_vdb);
}