#include <in_sgx/obdata/ObliviousDataFactory.h>
#include <shared/QueryEncodingDefinitions.h>
#include "in_sgx/plan/kaoperators/KAnonymousUnion.h"

using namespace db::obdata::obfield;
using namespace db::obdata;
using namespace plan::kaoperators;


KAnonymousUnion::KAnonymousUnion(
        TableID opId,
        size_vdb k,
        KAnonymousOperatorPtrList children
)
: KAnonymousOperator(
    opId,
    children[0]->getSchema(),
    k,
    false,
    children), _curChild(0)
{
    _outSchema.setTableID(opId);
}

KAnonymousUnion::~KAnonymousUnion() {}


OperatorStatus KAnonymousUnion::next()
{
    if (_status == OperatorStatus::NotInitialized) {
        _status = OperatorStatus::Ok;
    } else if (_status == OperatorStatus::NoMoreTuples) {
        return _status;
    }

    for(;_curChild < _children.size(); _curChild++) {
        if(_children[_curChild]->next() == OperatorStatus::Ok){
            _children[_curChild]->getCurrentTuple(_currentTuple);
            _outputCountLowerBound++;
            return _status;
        }
    }

    if (_outputCountLowerBound < _k) {
        _currentTuple = db::obdata::ObliviousDataFactory::createDefaultDummyTuple(_outSchema);
        _outputCountLowerBound++;
        return _status;
    }

    _status = OperatorStatus::NoMoreTuples;
    return OperatorStatus::NoMoreTuples;
}

void KAnonymousUnion::reset()
{
    KAnonymousOperator::reset();
    _curChild = 0;
    _outputCountLowerBound = 0;
}


size_vdb KAnonymousUnion::getSerializationOverHead(std::set<TableID> &baseTables) const {
    size_vdb ret = 0;
    for (pos_vdb i = 0, size = (size_vdb) _children.size(); i < size; i ++) {
        ret += _children[i]->getSerializationOverHead(baseTables);
    }
    return 2 + sizeof(TableID) + ret + sizeof(TableID) + sizeof(size_vdb);
};

void KAnonymousUnion::encodeOperator(unsigned char *&writeHead) const
{
    // the type of operator it is
    *writeHead = (unsigned char) serialization::OperatorCode::KANO_UNION;
    writeHead++;

    // the number of child
    *writeHead = (unsigned char) _children.size();
    writeHead++;

    // any child
    for (pos_vdb i = 0, size = (size_vdb) _children.size(); i < size; i ++) {
        _children[i]->encodeOperator(writeHead);
    }

    // operator id
    *(TableID*)writeHead = _operatorId;
    writeHead += sizeof(TableID);

    // extra info: k
    *(size_vdb*)writeHead = _k;
    writeHead += sizeof(size_vdb);
};
