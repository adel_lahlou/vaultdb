#include <set>
#include "in_sgx/plan/kaoperators/KAnonymousOperator.h"

using namespace plan::kaoperators;
using namespace type;

unsigned char * KAnonymousOperator::EncodeQueryToNewArray(KAnonymousOperator * root, size_vdb & encodingSize) {
    std::set<TableID> tables;
    size_vdb encodeSize = root->getSerializationOverHead(tables);
    size_vdb overheadSize = sizeof(size_vdb) + sizeof(TableID) * tables.size();
    unsigned char * output = new unsigned char [overheadSize + encodeSize];
    unsigned char * writeHead = output;

    *(size_vdb *)writeHead = (size_vdb) tables.size();
    writeHead += sizeof(size_vdb);
    for (auto t : tables) {
        *(TableID *)writeHead = t;
        writeHead += sizeof(TableID);
    }
    root->encodeOperator(writeHead);
    encodingSize = overheadSize + encodeSize;
    return output;
};

KAnonymousOperator::KAnonymousOperator()
        : _k(DEFAULT_K) {}

KAnonymousOperator::KAnonymousOperator(const KAnonymousOperator &o)
        : _k(o._k) {}

KAnonymousOperator::KAnonymousOperator(
        TableID opId,
        RecordSchema outSchema,
        size_vdb k,
        bool blocking,
        KAnonymousOperatorPtrList children
) : _operatorId(opId), _outSchema(outSchema),
    _blocking(blocking), _k(k)
{
    _status = OperatorStatus::NotInitialized;
    _countTowardK = 0;
    _outSchema.setTableID(opId);

    for (auto c : children) {
        c->setParent(this);
    }

    _children = children;
    _parent = nullptr;

}

KAnonymousOperator::~KAnonymousOperator() {
    for (auto p : _children)
        delete p;
}


const OperatorStatus KAnonymousOperator::getStatus() const {
    return _status;
}

const KAnonymousOperator *KAnonymousOperator::getChild(size_vdb child) const {
    return _children[child];
}

size_vdb KAnonymousOperator::getSerializationOverHead(std::set<TableID> &baseTables) const {
    size_vdb ret = 0;
    for (auto c : _children) {
        ret += c->getSerializationOverHead(baseTables);
    }
    return ret;
};

const std::vector<KAnonymousOperator *> &KAnonymousOperator::getChildren() const {
    return _children;
}

const RecordSchema &KAnonymousOperator::getChildSchema(int child) const {
    return getChild(child)->getSchema();
}

const RecordSchema &KAnonymousOperator::getSchema() const {
    return _outSchema;
}

const TableID KAnonymousOperator::getOutputTableID() const {
    return _operatorId;
};

void KAnonymousOperator::setOutputTableID(TableID id) {
    _operatorId = id;
};

const KAnonymousOperator *KAnonymousOperator::getParent() const {
    return _parent;
}

bool KAnonymousOperator::isBlocking() const {
    return _blocking;
}


OperatorStatus KAnonymousOperator::getCurrentTuple(db::obdata::ObliviousTuple &out) const {
    out = _currentTuple;
    return _status;
}

void KAnonymousOperator::setParent(KAnonymousOperator *p) {
    _parent = p;
}

void KAnonymousOperator::reset() {
    for (auto c : _children) {
        c->reset();
    }
    _countTowardK = 0;
    _status = OperatorStatus::NotInitialized;
}

size_vdb KAnonymousOperator::getGeneralizationLevel(pos_vdb index) const
{
    return _children[0]->getGeneralizationLevel(index);
};

void KAnonymousOperator::encodeOperator(unsigned char *&writeHead) const
{
    throw(std::domain_error("KAnonymousOperator cannot be encoded."));
};