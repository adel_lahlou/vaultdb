#include "in_sgx/utilities/InSGXController.h"
#include "in_sgx/plan/kaoperators/ClusterKAnonymousLDAggregation.h"
#include "in_sgx/plan/obexpressions/ObliviousExpressionFactory.h"
#include <in_sgx/plan/kaoperators/ClusterKAnonymousSimpleHashRepart.h>
#include <shared/QueryEncodingDefinitions.h>
#include <in_sgx/plan/kaoperators/ClusterKAnonymousRepartition.h>
#include <in_sgx/plan/kaoperators/KAnonymousSortedGroup.h>
#include <in_sgx/plan/kaoperators/KAnonymousBitonicSort.h>


using namespace plan::kaoperators;

ClusterKAnonymousLDAggregation::ClusterKAnonymousLDAggregation(MachineID machineId, TableID opId, TransmitterID transmitterID, size_vdb k,
                                                               KAnonymousOperatorPtr child, std::vector<obexpressions::ObliviousExpression> exps,
                                                               std::vector<pos_vdb> groupBys, utilities::InSGXController *dispatcher)
        : KAnonymousOperator(
        opId,
        child->getSchema(),
        k,
        false,
        KAnonymousOperatorPtrList{}),
          ClusterOperator(machineId, transmitterID, dispatcher, dispatcher == nullptr ? 0 : dispatcher->getNumberOfMachines())
{
    // bottom aggregation
    std::vector<pos_vdb> bottomGroupBy(groupBys);
    obexpressions::ObliviousExpressionList bottomExps;
    std::vector<pos_vdb> countDistinctCounts;
    for (auto e : exps) {
        if (e.getExpressionType() == serialization::ExpressionCode::COUNT_DIST) {
            std::vector<pos_vdb> refs;
            e.getInputReferences(refs);
            bottomExps.push_back(obexpressions::ObliviousExpressionFactory::makeColumn(refs[0], child->getSchema()[refs[0]]));
            bottomGroupBy.push_back(refs[0]);
            countDistinctCounts.push_back((pos_vdb)(groupBys.size() + countDistinctCounts.size()));
        } else {
            bottomExps.push_back(e);
        }
    }
    KAnonymousSortedGroup * bottomAgg = new KAnonymousSortedGroup(opId + 1, k, child, bottomExps, bottomGroupBy, dispatcher);

    // repartition
    std::vector<pos_vdb> partitionBy;
    for (pos_vdb i = 0, size = (size_vdb) groupBys.size(); i < size; i++) {
        partitionBy.push_back(i);
    }
    KAnonymousOperator * rightBelowTopAgg = new ClusterKAnonymousRepartition(machineId, 0, transmitterID, k, bottomAgg,
                                                                             partitionBy, {},
                                                                             {}, false, dispatcher);

    // top aggregation
    obexpressions::ObliviousExpressionList topExps;
    std::vector<pos_vdb> topGroupBy;
    int processedCountDistinct = 0;
    for (pos_vdb i = 0, size = (size_vdb)exps.size(); i < size; i++) {
        if (exps[i].isAggregateFunction()) {
            if (exps[i].getExpressionType() == serialization::ExpressionCode::COUNT_DIST) {
                std::vector<pos_vdb> refs;
                exps[i].getInputReferences(refs);
                topExps.push_back(obexpressions::ObliviousExpressionFactory::makeCountDistinct(
                        countDistinctCounts[processedCountDistinct], child->getSchema()[refs[0]]));
                processedCountDistinct ++;
            } else {
                topExps.push_back(obexpressions::ObliviousExpressionFactory::makeSum(i+processedCountDistinct, rightBelowTopAgg->getSchema()[i+processedCountDistinct]));
            }
        } else {
            topExps.push_back(obexpressions::ObliviousExpressionFactory::makeColumn(i+processedCountDistinct, rightBelowTopAgg->getSchema()[i+processedCountDistinct]));
            topGroupBy.push_back(i);
        }
    }

    _children.push_back(new KAnonymousSortedGroup(opId + 5, k, rightBelowTopAgg, topExps, topGroupBy, dispatcher));
    _children[0]->setParent(this);
    _outSchema = _children[0]->getSchema();
    // TODO we currently don't have AVG, so it must be constructed from SUM and COUNT
    // TODO we also don't have FLOAT type; we don't have CAST
};

ClusterKAnonymousLDAggregation::ClusterKAnonymousLDAggregation(MachineID machineId, TableID opId,
                                                               TransmitterID transmitterID, size_vdb k,
                                                               KAnonymousOperatorPtr child,
                                                               utilities::InSGXController *dispatcher)
        : KAnonymousOperator(
        opId,
        child->getSchema(),
        k,
        false,
        KAnonymousOperatorPtrList{}
), ClusterOperator(machineId, transmitterID, dispatcher, dispatcher->getNumberOfMachines())
{};

ClusterKAnonymousLDAggregation::~ClusterKAnonymousLDAggregation() {};

OperatorStatus ClusterKAnonymousLDAggregation::next()
{
    if (_children[0]->next() == OperatorStatus::Ok) {
        _children[0]->getCurrentTuple(_currentTuple);
    };
    _status = _children[0]->getStatus();
    return _status;
}

void ClusterKAnonymousLDAggregation::reset()
{
    KAnonymousOperator::reset();
}

size_vdb ClusterKAnonymousLDAggregation::getSerializationOverHead(std::set<TableID> &baseTables) const {
    size_vdb ret = 4 + sizeof(TableID) + sizeof(size_vdb) + sizeof(TransmitterID);
    KAnonymousSortedGroup * bottomAgg = (KAnonymousSortedGroup *)_children[0]->getChild(0)->getChild(0)->getChild(0);
    KAnonymousBitonicSort * sort = (KAnonymousBitonicSort*) bottomAgg->getChild(0);
    ret += sort->getChild(0)->getSerializationOverHead(baseTables);
    ret += sort->getOrderColumns().size();

    obexpressions::ObliviousExpressionList exps = bottomAgg->getExpressions();
    pos_vdb outputSize = (pos_vdb)exps.size();
    for (pos_vdb i = 0; i < outputSize; i++) {
        ret += exps[i].encodeSize();
    }

    return ret;
};

void ClusterKAnonymousLDAggregation::encodeOperator(unsigned char *&writeHead) const
{
    // the type of operator it is
    *writeHead = (unsigned char) serialization::OperatorCode::KANO_CLUSTER_LOCAL_DISTRO_AGGREGATION;
    writeHead++;

    // the number of child
    *writeHead = (unsigned char) 1;
    writeHead++;

    // child: child of the artificially added BitonicSort of the lower added sorted group
    KAnonymousSortedGroup * bottomAgg = (KAnonymousSortedGroup *)_children[0]->getChild(0)->getChild(0)->getChild(0);
    KAnonymousBitonicSort * sort = (KAnonymousBitonicSort*) bottomAgg->getChild(0);
    sort->getChild(0)->encodeOperator(writeHead);

    // operatorID
    *(TableID*)writeHead = _operatorId;
    writeHead += sizeof(TableID);

    // k
    *(size_vdb*)writeHead = _k;
    writeHead += sizeof(size_vdb);

    // extra info: transmitterID, order bys, outputs
    *(TransmitterID*)writeHead = _transmitterID;
    writeHead += sizeof(TransmitterID);

    std::vector<pos_vdb> columns = sort->getOrderColumns();
    pos_vdb groupBySize = (pos_vdb)columns.size();
    *writeHead = (unsigned char) groupBySize;
    writeHead++;
    for (pos_vdb i = 0; i < groupBySize; i++) {
        *writeHead = (unsigned char) columns[i];
        writeHead++;
    }

    obexpressions::ObliviousExpressionList exps = bottomAgg->getExpressions();
    pos_vdb outputSize = (pos_vdb)exps.size();
    *writeHead = (unsigned char) outputSize;
    writeHead++;
    for (pos_vdb i = 0; i < outputSize; i++) {
        exps[i].encodeObliviousExpression(writeHead);
    }
}

OperatorStatus ClusterKAnonymousLDAggregation::receiveCallBack(MachineID src_machine_id, TableID src_tid, db::obdata::ObliviousTupleTable *table) {
    return ((ClusterOperator *) _children[0]->getChild(0))->receiveCallBack(src_machine_id, src_tid, table);
};

size_vdb ClusterKAnonymousLDAggregation::getGeneralizationLevel(pos_vdb index) const {
    KAnonymousSortedGroup * bottomAgg = (KAnonymousSortedGroup *)_children[0]->getChild(0)->getChild(0)->getChild(0);
    const KAnonymousOperator * actualChild = bottomAgg->getChild(0)->getChild(0);
    std::vector<pos_vdb> references;
    bottomAgg->getExpressions()[index].getInputReferences(references);
    size_vdb maxGen;
    if (references.empty()) {
        maxGen = DEFAULT_MAX_GEN_LEVEL;
    } else {
        maxGen = 0;
        for (auto l : references) {
            size_vdb temp = actualChild->getGeneralizationLevel(l);
            maxGen = temp > maxGen ? temp : maxGen;
        }
    }
    return maxGen;
}