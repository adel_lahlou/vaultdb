#include <shared/QueryEncodingDefinitions.h>
#include "in_sgx/plan/kaoperators/KAnonymousFilter.h"

using namespace plan::kaoperators;
using namespace plan::obexpressions;
using namespace db::obdata;


KAnonymousFilter::KAnonymousFilter(
        TableID opId,
        size_vdb k,
        KAnonymousOperatorPtr child,
        ObliviousExpression filterPred
)
        : KAnonymousOperator(opId, child->getSchema(), k, false, KAnonymousOperatorPtrList{child}),
          _filterPred(filterPred)
{
    _filterTuple = new ObliviousTuple;
    _filterPred.setObliviousTuple(_filterTuple);
}

KAnonymousFilter::~KAnonymousFilter() {
    delete _filterTuple;
}


OperatorStatus KAnonymousFilter::next()
{
    if(_children[0]->next() == OperatorStatus::Ok) {
        _children[0]->getCurrentTuple(_currentTuple);
        *_filterTuple = _currentTuple;
        _currentTuple.setDummyFlag(
                !_filterPred.evaluate().isTruthy() ||
                _currentTuple.isDummy()
        );
        _status = OperatorStatus::Ok;
        return _status;
    }

    _status = OperatorStatus::NoMoreTuples;
    return _status;
}

void KAnonymousFilter::reset() {
    KAnonymousOperator::reset();
}

size_vdb KAnonymousFilter::getSerializationOverHead(std::set<TableID> &baseTables) const {
    return 2 + sizeof(TableID) + sizeof(size_vdb)
           + _children[0]->getSerializationOverHead(baseTables)
           + _filterPred.encodeSize();
};

void KAnonymousFilter::encodeOperator(unsigned char *&writeHead) const
{
    // the type of operator it is
    *writeHead = (unsigned char) serialization::OperatorCode::KANO_FILTER;
    writeHead++;

    // the number of child
    *writeHead = (unsigned char) 1;
    writeHead++;

    // any child
    _children[0]->encodeOperator(writeHead);

    // tableID
    *(TableID*)writeHead = _operatorId;
    writeHead += sizeof(TableID);

    // extra info: k, filter expression
    *(size_vdb*)writeHead = _k;
    writeHead += sizeof(size_vdb);

    _filterPred.encodeObliviousExpression(writeHead);
}