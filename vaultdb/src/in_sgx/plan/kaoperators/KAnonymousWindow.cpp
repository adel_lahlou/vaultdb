#include <in_sgx/plan/kaoperators/KAnonymousBitonicSort.h>
#include <in_sgx/obdata/ObliviousDataFactory.h>
#include "in_sgx/plan/kaoperators/KAnonymousWindow.h"
#include "in_sgx/plan/obexpressions/ObliviousExpressionFactory.h"

using namespace plan::kaoperators;
using namespace plan::obexpressions;
using namespace db::obdata;

KAnonymousWindow::KAnonymousWindow(TableID opId, size_vdb k, KAnonymousOperatorPtr child,
                                   obexpressions::ObliviousExpressionList exps, std::vector<pos_vdb> partitions,
                                   std::vector<pos_vdb> orderBys, std::vector<SortOrder> sortOrderOrOrderBys,
                                   utilities::InSGXController *controller) : KAnonymousSortedGroup(opId, k, child, true)
{
    _exps = exps;
    _isCurrentGroupAllDummy = true;

    std::vector<pos_vdb> sortColumnIndices;
    std::vector<SortOrder> sortOrders;

    for(size_vdb i = 0, size = partitions.size(); i < size; ++i){
        sortColumnIndices.push_back(partitions[i]);
        sortOrders.push_back(SortOrder::ASCEND);
    }
    for(size_vdb i = 0, size = orderBys.size(); i < size; ++i){
        sortColumnIndices.push_back(orderBys[i]);
        sortOrders.push_back(sortOrderOrOrderBys[i]);
    }

    _children.push_back(
            new KAnonymousBitonicSort(opId + 1, k, child, sortColumnIndices, sortOrders, controller)
    );

    _lastAggTuple = new ObliviousTuple;
    _curAggTuple = new ObliviousTuple;

    *_curAggTuple = ObliviousDataFactory::createDefaultTuple(_outSchema);
    _exps.setObliviousTuple(_curAggTuple);
    _outSchema = type::RecordSchema(opId, _exps.outputDatatype());

    _exps.setObliviousTuple(_curAggTuple);

    for(auto col : partitions) {
        ObliviousExpression lhs = ObliviousExpressionFactory::makeColumn(col, _outSchema[col]);
        ObliviousExpression rhs = ObliviousExpressionFactory::makeColumn(col, _outSchema[col]);

        lhs.setObliviousTuple(_lastAggTuple);
        rhs.setObliviousTuple(_curAggTuple);
        ObliviousExpression eq = ObliviousExpressionFactory::makeEq(lhs, rhs);
        _checkInGroup.push_back(eq);
    }

    for(int i = 0; i < _exps.size(); ++i) {
        if(_exps[i].isAggregateFunction()){
            _aggs.push_back(AggregateFunction(_exps[i]));
        }
    }
}

KAnonymousWindow::~KAnonymousWindow() {}
