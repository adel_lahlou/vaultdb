#include "in_sgx/plan/kaoperators/KAnonymousLocalGenFilter.h"
#include <shared/QueryEncodingDefinitions.h>
#include <in_sgx/plan/kasupport/KAnonymousGeneralizer.h>

using namespace plan::kaoperators;
using namespace plan::obexpressions;
using namespace db::obdata;

KAnonymousLocalGenFilter::KAnonymousLocalGenFilter(TableID opId, size_vdb k, KAnonymousOperatorPtr child,
                                                   plan::obexpressions::ObliviousExpression filterPred,
                                                   plan::obexpressions::ObliviousExpression filterPredCopy,
                                                   std::vector<pos_vdb> entities,
                                                   std::vector<size_vdb> imposedGenLevels,
                                                   utilities::InSGXController *controller)
        : KAnonymousOperator(opId, child->getSchema(), k, true, KAnonymousOperatorPtrList{child}),
          _filterViewPred(filterPred), _filterActualPred(filterPredCopy),
          _entityIdentifiers(entities), _inputTable(nullptr), _controller(controller)
{
    _filterTuple = new ObliviousTuple;
    _filterViewPred.setObliviousTuple(_filterTuple);
    _filterActualPred.setObliviousTuple(_filterTuple);

    if (!imposedGenLevels.empty()) {
        for (pos_vdb i = 0, size = (size_vdb) _outSchema.size(); i < size; i ++) {
            _generalizationLevels.push_back(imposedGenLevels[i]);
        }
    }
}

KAnonymousLocalGenFilter::~KAnonymousLocalGenFilter() {
    delete _filterTuple;
    if (_inputTable != nullptr) {
        _controller->erase(_controller->getMachineID(), _operatorId+1, DEFAULT_TABLE_PARTITION_ID);
    }
}

OperatorStatus KAnonymousLocalGenFilter::next()
{
    if (_status == OperatorStatus::NotInitialized) {
        _inputTable = _controller->create(_controller->getMachineID(), _operatorId + 1, DEFAULT_TABLE_PARTITION_ID, 1, _outSchema);
        while (_children[0]->next() == OperatorStatus::Ok) {
            ObliviousTuple t;
            _children[0]->getCurrentTuple(t);
            _controller->insertTuple(_inputTable, t);
        }
        std::vector<pos_vdb> refs;
        _filterViewPred.getInputReferences(refs);


        if (_generalizationLevels.empty()) {
            bool isAnyNotGeneralized = false;
            for (pos_vdb i = 0, size = _outSchema.size(); i < size; i++) {
                isAnyNotGeneralized |= _children[0]->getGeneralizationLevel(i) == DEFAULT_MAX_GEN_LEVEL;
            }
            if (isAnyNotGeneralized) {
                _generalizationLevels = KAnonymousGeneralizer
                        (_operatorId + 2, _k, _outSchema, _inputTable, refs, _entityIdentifiers,
                         _controller).computeAndGetGeneralizationLevel();
            } else {
                for (pos_vdb i = 0, size = _outSchema.size(); i < size; i++) {
                    _generalizationLevels.push_back(_children[0]->getGeneralizationLevel(i));
                }
            }
        }

        _filterViewPred.setGenLevel(_generalizationLevels);
        _inputTableIterator = _inputTable->begin();
    } else if (_status == OperatorStatus::NoMoreTuples) {
        return _status;
    } else {
        _inputTableIterator++;
    }

    while(_inputTableIterator != _inputTable->end()) {
        *_filterTuple = *_inputTableIterator;
        if (!_filterViewPred.evaluate().isTruthy()) {
            _inputTableIterator++;
            continue;
        }
        _currentTuple = *_inputTableIterator;
        _currentTuple.setDummyFlag(
                !_filterActualPred.evaluate().isTruthy() ||
                _currentTuple.isDummy()
        );
        _status = OperatorStatus::Ok;
        return _status;
    }

    _status = OperatorStatus::NoMoreTuples;
    return _status;
}

void KAnonymousLocalGenFilter::reset() {
    KAnonymousOperator::reset();
}

size_vdb KAnonymousLocalGenFilter::getSerializationOverHead(std::set<TableID> &baseTables) const {
    return 2 + sizeof(TableID) + sizeof(size_vdb)
           + _children[0]->getSerializationOverHead(baseTables)
           + _filterViewPred.encodeSize() * 2
           + 1 + _entityIdentifiers.size()
        // imposed generalization Levels
            + 1 + _generalizationLevels.size();
};

void KAnonymousLocalGenFilter::encodeOperator(unsigned char *&writeHead) const
{
    // the type of operator it is
    *writeHead = (unsigned char) serialization::OperatorCode::KANO_GEN_FILTER;
    writeHead++;

    // the number of child
    *writeHead = (unsigned char) 1;
    writeHead++;

    // any child
    _children[0]->encodeOperator(writeHead);

    // tableID
    *(TableID*)writeHead = _operatorId;
    writeHead += sizeof(TableID);

    // extra info: k, filter expressions, entity identifier
    *(size_vdb*)writeHead = _k;
    writeHead += sizeof(size_vdb);

    _filterViewPred.encodeObliviousExpression(writeHead);
    _filterViewPred.encodeObliviousExpression(writeHead);

    *writeHead = (unsigned char)_entityIdentifiers.size();
    writeHead ++;
    for (pos_vdb i = 0, size = (size_vdb) _entityIdentifiers.size(); i < size; i++) {
        *writeHead = (unsigned char) _entityIdentifiers[i];
        writeHead ++;
    }

    // imposed generalization Levels
    *writeHead = (unsigned char)_generalizationLevels.size();
    writeHead ++;
    for (pos_vdb i = 0, size = (size_vdb) _generalizationLevels.size(); i < size; i++) {
        *writeHead = (unsigned char) _generalizationLevels[i];
        writeHead ++;
    }
}