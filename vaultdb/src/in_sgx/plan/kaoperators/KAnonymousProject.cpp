#include <shared/QueryEncodingDefinitions.h>
#include "in_sgx/plan/kaoperators/KAnonymousProject.h"
#include "in_sgx/obdata/ObliviousDataFactory.h"

using namespace plan::obexpressions;
using namespace plan::kaoperators;
using namespace db::obdata;

KAnonymousProject::KAnonymousProject(
        TableID opId,
        size_vdb k,
        KAnonymousOperatorPtr child,
        ObliviousExpressionList exps
)
        : KAnonymousOperator(opId, child->getSchema(), k, false, KAnonymousOperatorPtrList{child}),
          _exps(exps), _projectTuple(new ObliviousTuple)
{
    ObliviousTuple dummy = ObliviousDataFactory::createDefaultTuple(_outSchema);
    _exps.setObliviousTuple(&dummy);
    _outSchema = type::RecordSchema(_outSchema.getTableID(), _exps.outputDatatype());
    _exps.setObliviousTuple(_projectTuple);
}

KAnonymousProject::~KAnonymousProject() {
    delete _projectTuple;
}

OperatorStatus KAnonymousProject::next()
{
    if(_children[0]->next() == OperatorStatus::NoMoreTuples) {
        _status = OperatorStatus::NoMoreTuples;
        return OperatorStatus::NoMoreTuples;
    } else {
        _children[0]->getCurrentTuple(*_projectTuple);
        _currentTuple = _exps.evaluate();
        _currentTuple.setDummyFlag(_projectTuple->isDummy());
        _status = OperatorStatus::Ok;
        return OperatorStatus::Ok;
    }
}

void KAnonymousProject::reset()
{
    KAnonymousOperator::reset();
}

size_vdb KAnonymousProject::getGeneralizationLevel(pos_vdb index) const {
    std::vector<pos_vdb> references;
    _exps[index].getInputReferences(references);
    size_vdb maxGen;
    if (references.empty()) {
        maxGen = DEFAULT_MAX_GEN_LEVEL;
    } else {
        maxGen = 0;
        for (auto l : references) {
            size_vdb temp = _children[0]->getGeneralizationLevel(l);
            maxGen = temp > maxGen ? temp : maxGen;
        }
    }
    return maxGen;
};

size_vdb KAnonymousProject::getSerializationOverHead(std::set<TableID> &baseTables) const {
    size_vdb ret = 3 + sizeof(TableID) + sizeof(size_vdb);
    ret += _children[0]->getSerializationOverHead(baseTables);
    for (pos_vdb i = 0, outputSize = (pos_vdb)_exps.size(); i < outputSize; i++) {
        ret += _exps[i].encodeSize();
    }
    return ret;
};

void KAnonymousProject::encodeOperator(unsigned char *&writeHead) const
{
    // the type of operator it is
    *writeHead = (unsigned char) serialization::OperatorCode::KANO_PROJECT;
    writeHead++;

    // the number of child
    *writeHead = (unsigned char) 1;
    writeHead++;

    // any child
    _children[0]->encodeOperator(writeHead);

    // tableID
    *(TableID*)writeHead = _operatorId;
    writeHead += sizeof(TableID);

    // extra info: k, expressions
    *(size_vdb*)writeHead = _k;
    writeHead += sizeof(size_vdb);

    size_vdb size = (size_vdb)_exps.size();
    *writeHead = (unsigned char) size;
    writeHead++;
    for (pos_vdb i = 0; i < size; i++) {
        _exps[i].encodeObliviousExpression(writeHead);
    }
}