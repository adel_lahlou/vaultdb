#include "in_sgx/plan/kaoperators/KAnonymousSeqScanMarkNonDummy.h"

using namespace plan::kaoperators;
using namespace db::obdata;

KAnonymousSeqScanMarkNonDummy::KAnonymousSeqScanMarkNonDummy(TableID opId, size_vdb k, db::obdata::ObliviousTupleTable * inputTable)
        : KAnonymousOperator(opId, inputTable->getSchema(), k, false, KAnonymousOperatorPtrList{}),
          _inputTable(inputTable)
{}

KAnonymousSeqScanMarkNonDummy::~KAnonymousSeqScanMarkNonDummy() {}


OperatorStatus KAnonymousSeqScanMarkNonDummy::next()
{
    if(_scanPos >= _inputTable->size()) {
        _status = OperatorStatus::NoMoreTuples;
    } else {
        _currentTuple = *_inputTable->at(_scanPos++);
        _currentTuple.setDummyFlag(false);
        _status = OperatorStatus::Ok;
    }

    return _status;
}

void KAnonymousSeqScanMarkNonDummy::reset()
{
    _scanPos = 0;
    KAnonymousOperator::reset();
}

size_vdb KAnonymousSeqScanMarkNonDummy::getGeneralizationLevel(pos_vdb index) const
{
    return DEFAULT_MAX_GEN_LEVEL;
};