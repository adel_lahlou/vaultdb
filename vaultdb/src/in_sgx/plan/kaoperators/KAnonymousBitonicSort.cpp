#include "in_sgx/plan/kaoperators/KAnonymousBitonicSort.h"
#include <cstring>
#include <shared/QueryEncodingDefinitions.h>
#include <in_sgx/utilities/InSGXController.h>
#include <in_sgx/obdata/BitonicSorter.h>
#include <in_sgx/utilities/SortHelper.h>

using namespace plan::kaoperators;
using namespace db::obdata;
using namespace utilities;

#if defined(LOCAL_DEBUG_MODE) || defined(OUTSIDE_COMPILATION)
#else
#include "Enclave_t.h"
#endif

KAnonymousBitonicSort::KAnonymousBitonicSort(TableID opId, size_vdb k, KAnonymousOperatorPtr child,
                                             std::vector<pos_vdb> orderByEntries, std::vector<SortOrder> sortOrders,
                                             utilities::InSGXController *controller)
        : KAnonymousOperator(opId, child->getSchema(), k, true, KAnonymousOperatorPtrList{child}),
            _orderByEntries(orderByEntries),
            _sortOrders(sortOrders),
          _controller(controller),
          _tupleBuffer(nullptr)
{
}

KAnonymousBitonicSort::~KAnonymousBitonicSort() {
    if (_controller != nullptr && _tupleBuffer != nullptr) {
        _controller->erase(_controller->getMachineID(), _operatorId + 1, DEFAULT_TABLE_PARTITION_ID);
        _tupleBuffer = nullptr;
    }
}

std::vector<pos_vdb> KAnonymousBitonicSort::getOrderColumns() const
{
    return _orderByEntries;
};

void KAnonymousBitonicSort::reset() {
    if (_tupleBuffer != nullptr && _tupleBuffer->size() > 0) {
        _outputPos = 0;
        _status = OperatorStatus::Ok;
    }
}

OperatorStatus KAnonymousBitonicSort::next() {
    ObliviousTuple input;
    OperatorStatus status;

    if (_status == OperatorStatus::NotInitialized) {
        _tupleBuffer = _controller->create(_controller->getMachineID(), _operatorId + 1, DEFAULT_TABLE_PARTITION_ID, 1, _outSchema);
        _tupleBuffer->setSchema(getSchema());

        while ((status = _children[0]->next()) == OperatorStatus::Ok) {
            _children[0]->getCurrentTuple(input);
            _controller->insertTuple(_tupleBuffer, input);
        }

        if (_tupleBuffer->size() == 0) {
            _status = OperatorStatus ::NoMoreTuples;
            return _status;
        }

#if defined(LOCAL_DEBUG_MODE) || defined(OUTSIDE_COMPILATION)
#else
        ocall_print_string((std::string("<><><> Before Bitonic sort pads, table ") + std::to_string(_operatorId) + std::string(" there are ") + std::to_string(_tupleBuffer->size()) + std::string(" tuples in the tuple buffer <><><>\n")).c_str());
#endif

        if (status == OperatorStatus::FatalError) {
            return status;
        }

        // ensure inputBuffer contains 2 power entries by adding NULL pointers
        // The NULL add/delete process could be public because input cardinality is either already masked or unprotected information
        unsigned int v = (unsigned int) _tupleBuffer->size();
        v--;
        v |= v >> 1;
        v |= v >> 2;
        v |= v >> 4;
        v |= v >> 8;
        v |= v >> 16;
        v++;
        v = v - (unsigned int) _tupleBuffer->size();

        for (int counter = 0; counter < v; counter++) {
            auto t = ObliviousTuple::getVacuousTuple();
            _controller->insertTuple(_tupleBuffer, t);
        }


#if defined(LOCAL_DEBUG_MODE) || defined(OUTSIDE_COMPILATION)
#else
        ocall_print_string((std::string("<><><> After Bitonic sort pads, there are ") + std::to_string(_tupleBuffer->size()) + std::string(" tuples in the tuple buffer <><><>\n")).c_str());

        ocall_print_string((std::string("<><><> The schema used in sorting include ") + std::to_string(_tupleBuffer->getSchema().size()) + std::string(" fields, include: ")).c_str());
        for (pos_vdb i = 0, size = _tupleBuffer->getSchema().size(); i < size; i ++ ) {
            ocall_print_string((std::to_string((int)_tupleBuffer->getSchema()[i].type) + std::string(", ")).c_str());
        }
        ocall_print_string(std::string(" <><><>\n").c_str());
#endif

        BitonicSorter<db::obdata::ObliviousTuple, type::RecordSchema>(_orderByEntries, _sortOrders, _outSchema, SortHelper::TupleComp).sort(_tupleBuffer->begin(), _tupleBuffer->end());

        for (int i = 0, size = _tupleBuffer->size(); i < size;) {
            if (_tupleBuffer->at(i)->isVacuous()) {
                _controller->eraseTuple(_tupleBuffer, i);
                size--;
                continue;
            }
            i++;
        }
        _outputPos = -1;
        _status = OperatorStatus::Ok;
    }

    // if outputBuffer is not empty, let the iterator return the next
    if (_tupleBuffer->size() > 0 && _outputPos < (int) (_tupleBuffer->size() - 1)) {
        _outputPos++;
        _currentTuple = *_tupleBuffer->at(_outputPos);
    } else {
        _status = OperatorStatus::NoMoreTuples;
    }

    return _status;
};

size_vdb KAnonymousBitonicSort::getSerializationOverHead(std::set<TableID> &baseTables) const {
    return 3 + sizeof(TableID) + sizeof(size_vdb)
           + _children[0]->getSerializationOverHead(baseTables) + _orderByEntries.size() * 2;
};

void KAnonymousBitonicSort::encodeOperator(unsigned char *&writeHead) const
{
    // the type of operator it is
    *writeHead = (unsigned char) serialization::OperatorCode::KANO_SORT;
    writeHead++;

    // the number of child
    *writeHead = (unsigned char) 1;
    writeHead++;

    // any child
    _children[0]->encodeOperator(writeHead);

    // tableID
    *(TableID*)writeHead = _operatorId;
    writeHead += sizeof(TableID);

    // extra info: k, indices, orders
    *(size_vdb*)writeHead = _k;
    writeHead += sizeof(size_vdb);

    pos_vdb size = (pos_vdb)_orderByEntries.size();
    *writeHead = (unsigned char) size;
    writeHead++;
    for (pos_vdb i = 0; i < size; i++) {
        *writeHead = (unsigned char)_orderByEntries[i];
        writeHead++;
    }
    for (pos_vdb i = 0; i < size; i++) {
        *writeHead = (unsigned char)_sortOrders[i];
        writeHead++;
    }
}