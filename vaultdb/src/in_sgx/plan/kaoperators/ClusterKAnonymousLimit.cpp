#include <in_sgx/plan/kaoperators/KAnonymousLimit.h>
#include <in_sgx/obdata/ObliviousDataFactory.h>
#include <in_sgx/obdata/BitonicSorter.h>
#include <in_sgx/utilities/SortHelper.h>
#include <shared/QueryEncodingDefinitions.h>
#include "in_sgx/plan/kaoperators/ClusterKAnonymousLimit.h"
#include "in_sgx/utilities/InSGXController.h"

using namespace plan::kaoperators;
using namespace db::obdata;

ClusterKAnonymousLimit::ClusterKAnonymousLimit(MachineID machineID, TableID opId, TransmitterID transmitterID,
                                               size_vdb k, KAnonymousOperatorPtr child, size_vdb tupleLimit,
                                               std::vector<pos_vdb> orderByEntries,
                                               std::vector<SortOrder> sortOrders, MachineID designatedCollector,
                                               utilities::InSGXController *dispatcher)
        : KAnonymousOperator(opId, child->getSchema(), k, true, KAnonymousOperatorPtrList{new KAnonymousLimit(0, k, tupleLimit, {child})}),
         ClusterOperator(machineID, transmitterID, dispatcher, dispatcher == nullptr ? 0 : dispatcher->getNumberOfMachines()),
         _orderByEntries(orderByEntries), _sortOrders(sortOrders), _tupleCountLimit(tupleLimit), _designatedCollector(designatedCollector),
          _finalTupleBuffer(nullptr), _pos(0), _receivedPartitionCount(0), _outputTupleCount(0)
{};

ClusterKAnonymousLimit::~ClusterKAnonymousLimit(){
    if (_localDispatcher != nullptr && _finalTupleBuffer != nullptr) {
        _localDispatcher->erase(_localMachineID, _transmitterID, DEFAULT_TABLE_PARTITION_ID);
        _finalTupleBuffer = nullptr;
    }
};

OperatorStatus ClusterKAnonymousLimit::next() {
    if (_status == OperatorStatus::NotInitialized) {
        init();
        if (_status == OperatorStatus::NoMoreTuples) {
            return _status;
        }
    } else if (_status == OperatorStatus::NoMoreTuples) {
        return _status;
    }
    _pos++;
    while ( _outputTupleCount < _tupleCountLimit && _pos <= _finalTupleBuffer->size()) {
        if (((*_finalTupleBuffer)[GET_TABLE_READ_POSITION(_pos)].isVacuous()
             || (*_finalTupleBuffer)[GET_TABLE_READ_POSITION(_pos)].isDummy())
            && _pos <= _finalTupleBuffer->size()) {
            _pos++;
            continue;
        }
        _currentTuple = (*_finalTupleBuffer)[GET_TABLE_READ_POSITION(_pos)];
        _outputTupleCount++;
        _status = OperatorStatus::Ok;
        return OperatorStatus::Ok;
    }

    if (_outputTupleCount < _tupleCountLimit && _pos > _finalTupleBuffer->size()) {
        _currentTuple = ObliviousDataFactory::createDefaultDummyTuple(_outSchema);
        _outputTupleCount ++;
        _status = OperatorStatus::Ok;
        return _status;
    }
    _pos--;
    _status = OperatorStatus::NoMoreTuples;
    return _status;
};

size_vdb ClusterKAnonymousLimit::getReceivedTableCount() {
    std::unique_lock<std::mutex> guard(_tablesMutex);
    return _receivedPartitionCount;
};

OperatorStatus
ClusterKAnonymousLimit::receiveCallBack(MachineID src_machine_id, TableID src_tid, db::obdata::ObliviousTupleTable *table) {
    std::unique_lock<std::mutex> localGuard(_tablesMutex);
    if (src_tid == _transmitterID) {
        if (src_machine_id == _localMachineID) {
            _receivedPartitionCount++;
        } else {
            auto localTablePtr = _localDispatcher->fetch(_localMachineID, _transmitterID, DEFAULT_TABLE_PARTITION_ID);
            localTablePtr->appendMove(*table);
            _receivedPartitionCount++;
            _localDispatcher->erase(src_machine_id, src_tid, table->getParitionID());
        }
    } else {
        return OperatorStatus::FatalError;
    }
    return OperatorStatus::Ok;
};

void ClusterKAnonymousLimit::init() {
    // preparing for shipping table
    db::obdata::ObliviousTupleTable * buffer = _localDispatcher->create(_localMachineID, _transmitterID, DEFAULT_TABLE_PARTITION_ID, 1, _outSchema);
    type::RecordSchema schema(_outSchema);
    schema.setTableID(_transmitterID);
    buffer->setSchema(schema);
    // fetch tuples
    size_vdb remainingSlots = _tupleCountLimit;
    while (remainingSlots > 0) {
        ObliviousTuple t;
        if (_children[0]->next() == OperatorStatus::Ok) {
            _children[0]->getCurrentTuple(t);
        } else {
            t = ObliviousDataFactory::createDefaultDummyTuple(_outSchema);
        }
        _localDispatcher->insertTuple(buffer, t);
        remainingSlots--;
    }
    if (_designatedCollector == _localMachineID) {
        _status = OperatorStatus::WaitingForTuples;
        _finalTupleBuffer = _localDispatcher->fetch(_localMachineID, _transmitterID, DEFAULT_TABLE_PARTITION_ID);
        // Dispatch requests
        for (int i = 1, end = _totalMachineCount; i <= end; i++) {
            if (i == _localMachineID) {
                receiveCallBack(_localMachineID, _transmitterID, buffer);
                continue;
            }
            _localDispatcher->requestTable(i, _transmitterID, DEFAULT_TABLE_PARTITION_ID, this);
        }
    } else {
        _status = OperatorStatus :: NoMoreTuples;
        // dispatch
        auto ssize = buffer->serializationSize();
        unsigned char data[ssize];
        buffer->serializeTo(data);
        _localDispatcher->dispatch(_designatedCollector, _transmitterID, ssize, data);
        _localDispatcher->erase(_localMachineID, _transmitterID, DEFAULT_TABLE_PARTITION_ID);
        return;
    }

    // wait; but only for designatedCollector
    while (_status == OperatorStatus::WaitingForTuples) {
        if (getReceivedTableCount() == _totalMachineCount) {
            _isAllDataCollected = true;
            break;
        };
    }

    // sort
    if (!_orderByEntries.empty()) {
        BitonicSorter<db::obdata::ObliviousTuple, type::RecordSchema> sorter(_orderByEntries, _sortOrders, _outSchema, SortHelper::TupleComp);
        size_vdb padCountRequired = sorter.requiredPadCount(_finalTupleBuffer->begin(), _finalTupleBuffer->end());
        while (padCountRequired > 0) {
            ObliviousTuple t = ObliviousTuple::getVacuousTuple();
            _localDispatcher->insertTuple(buffer, t);
            padCountRequired--;
        }
        sorter.sort(_finalTupleBuffer->begin(), _finalTupleBuffer->end());
    }
};

void ClusterKAnonymousLimit::reset() {
    _pos = INIT_TABLE_INDEX;
    _outputTupleCount = 0;
    _receivedPartitionCount = 0;
    if (_isAllDataCollected) {
        _status = OperatorStatus::Ok;
    } else {
        if (_finalTupleBuffer != nullptr) {
            _localDispatcher->erase(_localMachineID, _transmitterID, DEFAULT_TABLE_PARTITION_ID);
        }
        _status = OperatorStatus::NotInitialized;
    }
};

size_vdb ClusterKAnonymousLimit::getSerializationOverHead(std::set<TableID> &baseTables) const {
    return KAnonymousOperator::getSerializationOverHead(baseTables) + sizeof(size_vdb) /* tuple limit */ +
           + 3 + sizeof(TableID) + sizeof(size_vdb) + sizeof(MachineID) + 2 * _orderByEntries.size() + sizeof(TransmitterID) ;
};

void ClusterKAnonymousLimit::encodeOperator(unsigned char *&writeHead) const
{
    // the type of operator it is
    *writeHead = (unsigned char) serialization::OperatorCode::KANO_CLUSTER_LIMIT;
    writeHead++;

    // the number of child
    *writeHead = (unsigned char) 1;
    writeHead++;

    // any child
    _children[0]->getChild(0)->encodeOperator(writeHead);

    // tableID
    *(TableID*)writeHead = _operatorId;
    writeHead += sizeof(TableID);

    // k
    *(size_vdb*)writeHead = _k;
    writeHead += sizeof(size_vdb);

    // extra info: tupleLimit, transmitterID, designated collector, indices, orders
    *(size_vdb*)writeHead = _tupleCountLimit;
    writeHead += sizeof(size_vdb);

    *(TableID*)writeHead = _transmitterID;
    writeHead += sizeof(TableID);

    *(MachineID*)writeHead = _designatedCollector;
    writeHead += sizeof(MachineID);

    size_vdb size = (size_vdb)_orderByEntries.size();
    *writeHead = (unsigned char) size;
    writeHead++;
    for (pos_vdb i = 0; i < size; i++) {
        *writeHead = (unsigned char)_orderByEntries[i];
        writeHead++;
    }
    for (pos_vdb i = 0; i < size; i++) {
        *writeHead = (unsigned char)_sortOrders[i];
        writeHead++;
    }
}