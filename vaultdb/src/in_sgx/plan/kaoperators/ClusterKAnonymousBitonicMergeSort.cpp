#include <shared/QueryEncodingDefinitions.h>
#include <in_sgx/plan/kaoperators/KAnonymousBitonicSort.h>
#include "in_sgx/plan/kaoperators/ClusterKAnonymousBitonicMergeSort.h"
#include "in_sgx/obdata/ObliviousTupleTable.h"

using namespace plan::kaoperators;

ClusterKAnonymousBitonicMergeSort::ClusterKAnonymousBitonicMergeSort(
        MachineID machineId, TableID opId,
        TableID transmitterID, size_vdb k,
        KAnonymousOperatorPtr child,
        std::vector<pos_vdb> orderByEntries,
        std::vector<SortOrder> sortOrders,
        utilities::InSGXController *dispatcher,
        MachineID designatedCollector)
        : _orderByEntries(orderByEntries),
          _sortOrders(sortOrders),
          _designatedCollector(designatedCollector),
          _pos(INIT_TABLE_INDEX),
          KAnonymousOperator(
            opId,
            child->getSchema(),
            k,
            true,
            KAnonymousOperatorPtrList{
                    new KAnonymousBitonicSort(opId + 1, k, child, orderByEntries, sortOrders, dispatcher)
            }),
          // nullptr could occur in the HB query formulation phase.
          ClusterOperator(machineId, transmitterID, dispatcher, 
		  dispatcher == nullptr ? 0 : dispatcher->getNumberOfMachines())
{
    // Dispatch requests
    if (designatedCollector == _localMachineID && dispatcher != nullptr) {
        for (int i = 1, end = _totalMachineCount; i <= end; i++) {
            dispatcher->requestTable(i, _transmitterID, i, this);
        }
    }
};

ClusterKAnonymousBitonicMergeSort::~ClusterKAnonymousBitonicMergeSort(){};

size_vdb ClusterKAnonymousBitonicMergeSort::getTableCount()
{
    std::unique_lock<std::mutex> guard(_tablesMutex);
    return (size_vdb)_tables.size();
}

OperatorStatus ClusterKAnonymousBitonicMergeSort::receiveCallBack(MachineID src_machine_id, TableID src_tid, db::obdata::ObliviousTupleTable *table) {
    std::unique_lock<std::mutex> localGuard(_tablesMutex);
    if (src_tid == _transmitterID) {
        _tables.emplace(src_machine_id, table);
    } else {
        return OperatorStatus::FatalError;
    }
    return OperatorStatus::Ok;
};

OperatorStatus ClusterKAnonymousBitonicMergeSort::dispatch(
        MachineID dst_machine_id,
        ObliviousTupleTable *table
) {
    type::RecordSchema schema(this->getSchema());
    schema.setTableID(_transmitterID);
    table->setSchema(schema);
    table->setParitionID(_localMachineID);
    size_vdb blockSize = GET_NEAREST_CACHELINE_MULTIPLE(table->serializationSize());
    auto buffer = new unsigned char[blockSize];
    table->serializeTo(buffer);
    _localDispatcher->dispatch(dst_machine_id, _transmitterID, blockSize, buffer);
    return OperatorStatus :: Ok;
}

void ClusterKAnonymousBitonicMergeSort::initializeIterationTracker() {
    std::unique_lock<std::mutex> guard(_tablesMutex);
    auto it = _tables.begin();
    while (it != _tables.end()) {
        _tablesTracker.emplace(it->first, REGULAR_INIT_INDEX);
        it++;
    }
}

static bool isRightLowerRank(
        ObliviousTuple & left,
        ObliviousTuple & right,
        std::vector<pos_vdb> &orderByOrdinals,
        std::vector<SortOrder> &sortingOrders
) {
    //// note: assume neither left or right are vacuous tuple
    for (int i = 0, size = orderByOrdinals.size(); i < size; i++) {
        int pos = orderByOrdinals[i];
        bool asc = sortingOrders.at(i) == SortOrder::ASCEND;
        if ((asc && left[pos] > right[pos]) || ((!asc) && left[pos] < right[pos])) {
            return true;
        } else if ((asc && left[pos] < right[pos]) || ((!asc) && left[pos] > right[pos])) {
            return false;
        }
    }
    return false;
};

OperatorStatus ClusterKAnonymousBitonicMergeSort::MergeForOneTuple() {
    if (_isAllDataCollected){
        if (++_pos > _finalTupleBuffer.size()) {
            _status = OperatorStatus :: NoMoreTuples;
            return _status;
        }
        _status = OperatorStatus :: Ok;
    } else {
        auto it = _tablesTracker.begin();
        auto end = _tablesTracker.end();
        std::unique_lock<std::mutex> * guard;
        std::vector<MachineID> deletes;

        bool isFirstFetched = false;
        ObliviousTuple currentSelection;
        MachineID currenSelectionMachineID;

        while (it != end) {
            guard = new std::unique_lock<std::mutex>(_tablesMutex);
            if (it->second == _tables[it->first]->size()) {
                delete guard;
                // table is fully iterated, delete from the entry;
                deletes.push_back(it->first);
            } else if (!isFirstFetched) {
                currenSelectionMachineID = it->first;
                currentSelection = (* _tables[currenSelectionMachineID])[it->second];
                delete guard;
                isFirstFetched = true;
            } else {
                MachineID candidateMachineID = it->first;
                ObliviousTuple currentCandidate = (*_tables[candidateMachineID])[it->second];
                delete guard;
                if (isRightLowerRank(currentSelection, currentCandidate, _orderByEntries, _sortOrders)) {
                    currentSelection = currentCandidate;
                    currenSelectionMachineID = candidateMachineID;
                }
            }
            ++it;
        }
        if (!isFirstFetched) {
            _status = OperatorStatus::NoMoreTuples;
            _isAllDataCollected = true;
        } else {
            ++(_tablesTracker[currenSelectionMachineID]);
            _currentTuple = currentSelection;
            _status = OperatorStatus::Ok;
        }
        for (MachineID id : deletes) {
            _tablesTracker.erase(id);
        }
    }
    return _status;
}

OperatorStatus ClusterKAnonymousBitonicMergeSort::next() {
    KAnonymousBitonicSort * child = (KAnonymousBitonicSort*)_children[0];
    if (_status == OperatorStatus::NotInitialized) {
        while (child->next() != OperatorStatus::NoMoreTuples) {
            // keep going
        }

        dispatch(_designatedCollector, child->_tupleBuffer);

        // there can be only one
        if (_localMachineID != _designatedCollector) {
            _status = OperatorStatus :: NoMoreTuples;
            return _status;
        }
        _status = OperatorStatus::WaitingForTuples;
    } else if (_status == OperatorStatus::NoMoreTuples) {
        return _status;
    }

    // only happens at the designated collector
    while (_status == OperatorStatus::WaitingForTuples) {
        if (getTableCount() ==  _totalMachineCount) {
            initializeIterationTracker();
            break;
        };
    }
    return MergeForOneTuple();
};

void ClusterKAnonymousBitonicMergeSort::reset(){
    if (_isAllDataCollected) {
        _pos = POST_INIT_INIT_INDEX;
        _status = OperatorStatus::Ok;
    } else {
        std::unique_lock<std::mutex> guard(_tablesMutex);
        _tables.clear();
        _pos = INIT_TABLE_INDEX;
        _status = OperatorStatus::NotInitialized;
    }
};

size_vdb ClusterKAnonymousBitonicMergeSort::getSerializationOverHead(std::set<TableID> &baseTables) const {
    return KAnonymousOperator::getSerializationOverHead(baseTables)
           + 3 + sizeof(TableID) + sizeof(size_vdb) + sizeof(MachineID) + 2 * _orderByEntries.size() + sizeof(TransmitterID) ;
};

void ClusterKAnonymousBitonicMergeSort::encodeOperator(unsigned char *&writeHead) const
{
    // the type of operator it is
    *writeHead = (unsigned char) serialization::OperatorCode::KANO_CLUSTER_BITONIC_MERGE_SORT;
    writeHead++;

    // the number of child
    *writeHead = (unsigned char) 1;
    writeHead++;

    // any child
    _children[0]->getChild(0)->encodeOperator(writeHead);

    // tableID
    *(TableID*)writeHead = _operatorId;
    writeHead += sizeof(TableID);

    // k
    *(size_vdb*)writeHead = _k;
    writeHead += sizeof(size_vdb);

    // extra info: transmitterID, designated collector, indices, orders
    *(TableID*)writeHead = _transmitterID;
    writeHead += sizeof(TableID);

    *(MachineID*)writeHead = _designatedCollector;
    writeHead += sizeof(MachineID);

    size_vdb size = (size_vdb)_orderByEntries.size();
    *writeHead = (unsigned char) size;
    writeHead++;
    for (pos_vdb i = 0; i < size; i++) {
        *writeHead = (unsigned char)_orderByEntries[i];
        writeHead++;
    }
    for (pos_vdb i = 0; i < size; i++) {
        *writeHead = (unsigned char)_sortOrders[i];
        writeHead++;
    }
}

size_vdb ClusterKAnonymousBitonicMergeSort::getGeneralizationLevel(pos_vdb index) const {
    return _children[0]->getChild(0)->getGeneralizationLevel(index);
}
