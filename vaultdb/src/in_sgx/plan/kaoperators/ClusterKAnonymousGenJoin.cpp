#include <in_sgx/plan/kaoperators/ClusterKAnonymousRepartition.h>
#include <shared/QueryEncodingDefinitions.h>
#include "in_sgx/plan/kaoperators/ClusterKAnonymousGenJoin.h"

using namespace plan::kaoperators;
using namespace plan::obexpressions;
using namespace db::obdata;
using namespace type;

#if defined(LOCAL_DEBUG_MODE) || defined(OUTSIDE_COMPILATION)
#else
#include "Enclave_t.h"
#endif

ClusterKAnonymousGenJoin::ClusterKAnonymousGenJoin(MachineID machineId, TableID opId,
                                                   TransmitterID leftChildTransmissionID,
                                                   TransmitterID rightChildTransmissionID, size_vdb k,
                                                   KAnonymousOperatorPtr leftChild,
                                                   KAnonymousOperatorPtr rightChild,
                                                   std::vector<pos_vdb> leftJoinAttributes,
                                                   std::vector<pos_vdb> rightJoinAttributes,
                                                   std::vector<pos_vdb> leftEntityIdentifier,
                                                   std::vector<pos_vdb> rightEntityIdentifier,
                                                   std::vector<size_vdb> leftImposedGenLevels,
                                                   std::vector<size_vdb> rightImposedGenLevels,
                                                   bool isSortingBeforeRepart,
                                                   utilities::InSGXController *dispatcher)
        : KAnonymousOperator(
                opId,
                RecordSchema::join(leftChild->getSchema(), rightChild->getSchema()),
                k,
                true,
                KAnonymousOperatorPtrList{
                        new ClusterKAnonymousRepartition(machineId, opId + 1, leftChildTransmissionID, k, leftChild,
                                                         leftJoinAttributes, leftEntityIdentifier,
                                                         leftImposedGenLevels, isSortingBeforeRepart, dispatcher),
                        new ClusterKAnonymousRepartition(machineId, opId + 6, rightChildTransmissionID, k, rightChild,
                                                         rightJoinAttributes, rightEntityIdentifier,
                                                         rightImposedGenLevels, isSortingBeforeRepart, dispatcher)}
        ),
          ClusterOperator(machineId, leftChildTransmissionID, dispatcher, dispatcher == nullptr ? 0 : dispatcher->getNumberOfMachines()),
          _leftAttributes(leftJoinAttributes),
          _rightAttributes(rightJoinAttributes),
          _leftEntityIdentifier(leftEntityIdentifier),
          _rightEntityIdentifier(rightEntityIdentifier),
          _doesLeftHaveMore(true),
          _isSortingBeforeRepart(isSortingBeforeRepart),
          _leftImposedGenLevel(leftImposedGenLevels),
          _rightImposedGenLevel(rightImposedGenLevels)
{};

ClusterKAnonymousGenJoin::~ClusterKAnonymousGenJoin() {};


OperatorStatus ClusterKAnonymousGenJoin::next() {
    while(_status != OperatorStatus::NoMoreTuples) {
        KAnonymousOperator * outer = _doesLeftHaveMore ? _children[0] : _children[1];
        KAnonymousOperator * inner = _doesLeftHaveMore ? _children[1] : _children[0];
        if (_status == OperatorStatus::NotInitialized) {
            KAnonymousOperator * left = _children[0];
            left->next();
            KAnonymousOperator * right = _children[1];
            right->next();

            // acquiring generalization level
            for (pos_vdb i = 0, size = _outSchema.size(); i < size; i++) {
                _generalizationLevels.push_back(DEFAULT_MAX_GEN_LEVEL);
            }
            for (pos_vdb i = 0, size = (size_vdb) _leftAttributes.size(); i < size; i++) {
                _generalizationLevels[_leftAttributes[i]] = left->getGeneralizationLevel(_leftAttributes[i]);
            }
            // reconcile generalization levels
            size_vdb leftSchemaSize = left->getSchema().size();
            for (pos_vdb i = 0, size = (size_vdb) _rightAttributes.size(); i < size; i++) {
                auto current = _generalizationLevels[_leftAttributes[i]];
                auto comparison = right->getGeneralizationLevel(_rightAttributes[i]);
                comparison = current < comparison ? comparison : current;
                _generalizationLevels[_leftAttributes[i]] = comparison;
                _generalizationLevels[_rightAttributes[i] + leftSchemaSize] = comparison;
            }

            _doesLeftHaveMore = ((ClusterKAnonymousRepartition *) outer)->getTotalTupleCount()
                                >= ((ClusterKAnonymousRepartition *) inner)->getTotalTupleCount();
            outer = _doesLeftHaveMore ? _children[0] : _children[1];
            inner = _doesLeftHaveMore ? _children[1] : _children[0];
            _status = OperatorStatus::WaitingForTuples;

#if defined(LOCAL_DEBUG_MODE) || defined(OUTSIDE_COMPILATION)
#else
            ocall_print_string(std::string("<><><> Gen join gen levels: ->|").c_str());
            for (pos_vdb i = 0, size = (size_vdb) _generalizationLevels.size(); i < size; i++) {
                ocall_print_string((std::to_string(_generalizationLevels[i]) + std::string(" ")).c_str());
            }
            ocall_print_string(std::string("|<- <><><>\n").c_str());
#endif

        } else if (_status == OperatorStatus::NoMoreTuples) {
            return _status;
        } else {
            while (outer->getStatus() != OperatorStatus::NoMoreTuples) {
                if (inner->next() == OperatorStatus::NoMoreTuples) {
                    inner->reset();
                    outer->next();
                    continue;
                } else {
                    break;
                }
            }
        }

        if (outer->getStatus() == OperatorStatus::NoMoreTuples) {
            _status = OperatorStatus::NoMoreTuples;
            return _status;
        }

        ObliviousTuple outerTuple;
        ObliviousTuple innerTuple;
        outer->getCurrentTuple(outerTuple);
        inner->getCurrentTuple(innerTuple);

        bool isThisReal = !(outerTuple.isDummy() || innerTuple.isDummy());
        bool isViewMatch = true;
        std::vector<pos_vdb> * outerAttributes = _doesLeftHaveMore ? &_leftAttributes : &_rightAttributes;
        std::vector<pos_vdb> * innerAttributes = _doesLeftHaveMore ? &_rightAttributes : &_leftAttributes;

        for (size_vdb i = 0, size = outerAttributes->size(); i < size; i++) {
            bool currentResult;
            size_vdb genLevel = getGeneralizationLevel(_leftAttributes[i]); // works because left and right genLevels are unified
            if (!outerTuple[outerAttributes->at(i)].isEqTo(innerTuple[innerAttributes->at(i)], genLevel, currentResult)) {
                isViewMatch = false;
                break;
            };
            isThisReal &= currentResult;
        }

        if (!isViewMatch) {
            continue;
        }

        try {
            _currentTuple = _doesLeftHaveMore ? ObliviousTuple::join(outerTuple, innerTuple) : ObliviousTuple::join(
                    innerTuple, outerTuple);
        } catch (const std::exception & e) {
#if defined(LOCAL_DEBUG_MODE) || defined(OUTSIDE_COMPILATION)
#else
            ocall_print_string((std::string("<><><> GenJoin throwing excpetion: ") + std::string(e.what()) + std::string(" <><><>\n")).c_str());
#endif
            abort();
        }
        _currentTuple.setDummyFlag(!isThisReal);
        _status = OperatorStatus::Ok;
        break;
    }
    return _status;
}

void ClusterKAnonymousGenJoin::reset() {
    KAnonymousOperator::reset();
    _status = OperatorStatus :: Ok;
}

size_vdb ClusterKAnonymousGenJoin::getSerializationOverHead(std::set<TableID> &baseTables) const {
    return KAnonymousOperator::getSerializationOverHead(baseTables)
           + 6 + sizeof(TableID) + sizeof(size_vdb) + _leftAttributes.size() + _rightAttributes.size() + sizeof(TransmitterID) + sizeof(TransmitterID)
            + _leftEntityIdentifier.size() + _rightEntityIdentifier.size()
           // sort before repart
           + 1
           // imposed generalization levels
           + 2 + _generalizationLevels.size();
};

void ClusterKAnonymousGenJoin::encodeOperator(unsigned char *&writeHead) const
{
    // the type of operator it is
    *writeHead = (unsigned char) serialization::OperatorCode::KANO_CLUSTER_GEN_HASH_JOIN;
    writeHead++;

    // the number of child
    *writeHead = (unsigned char) 2;
    writeHead++;

    // any child
    ClusterKAnonymousRepartition * left = (ClusterKAnonymousRepartition*)_children[0];
    ClusterKAnonymousRepartition * right = (ClusterKAnonymousRepartition*)_children[1];
    left->getChild(0)->encodeOperator(writeHead);
    right->getChild(0)->encodeOperator(writeHead);

    // tableID
    *(TableID*)writeHead = _operatorId;
    writeHead += sizeof(TableID);

    // extra info: k, joinType, number of attributes, left and right attributes
    *(size_vdb*)writeHead = _k;
    writeHead += sizeof(size_vdb);

    *writeHead = (unsigned char)serialization::JoinType::INNER;
    writeHead++;

    *(TransmitterID*)writeHead = left->getTransmitterID();
    writeHead += sizeof(TransmitterID);

    *(TransmitterID*)writeHead = right->getTransmitterID();
    writeHead += sizeof(TransmitterID);

    unsigned char size = (unsigned char)_leftAttributes.size();
    *writeHead = size;
    writeHead ++;

    for (unsigned char i = 0; i < size; i ++) {
        *writeHead = (unsigned char)_leftAttributes[i];
        writeHead++;
    }

    for (unsigned char i = 0; i < size; i ++) {
        *writeHead = (unsigned char)_rightAttributes[i];
        writeHead++;
    }

    size = (unsigned char)_leftEntityIdentifier.size();
    *writeHead = size;
    writeHead ++;

    for (unsigned char i = 0; i < size; i ++) {
        *writeHead = (unsigned char)_leftEntityIdentifier[i];
        writeHead++;
    }

    size = (unsigned char)_rightEntityIdentifier.size();
    *writeHead = size;
    writeHead ++;

    for (unsigned char i = 0; i < size; i ++) {
        *writeHead = (unsigned char)_rightEntityIdentifier[i];
        writeHead++;
    }

    // sort before repart
    *writeHead = (unsigned char)_isSortingBeforeRepart;
    writeHead++;

    // imposed genLevels;
    auto leftGenSize = (unsigned char)_leftImposedGenLevel.size();
    *writeHead = leftGenSize;
    writeHead ++;
    for (pos_vdb i = 0; i < leftGenSize; i++) {
        *writeHead = _leftImposedGenLevel[i];
        writeHead++;
    }
    auto rightGenSize = (unsigned char)_rightImposedGenLevel.size();
    *writeHead = rightGenSize;
    writeHead ++;
    for (pos_vdb i = 0; i < rightGenSize; i++) {
        *writeHead = _rightImposedGenLevel[i + leftGenSize];
        writeHead++;
    }
}

OperatorStatus ClusterKAnonymousGenJoin::receiveCallBack(MachineID src_machine_id, TableID src_tid, db::obdata::ObliviousTupleTable *table) {
    if (src_machine_id != _localMachineID) {
        return OperatorStatus ::FatalError;
    }
    if (_children[0]->getOutputTableID() == src_tid) {
        return ((ClusterOperator *) _children[0])->receiveCallBack(src_machine_id, src_tid, table);
    } else if (_children[1]->getOutputTableID() == src_tid) {
        return ((ClusterOperator *) _children[1])->receiveCallBack(src_machine_id, src_tid, table);
    } else {
        return OperatorStatus :: FatalError;
    }
};

size_vdb ClusterKAnonymousGenJoin::getGeneralizationLevel(pos_vdb index) const
{
    return _generalizationLevels[index];
};