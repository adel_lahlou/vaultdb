#include <shared/QueryEncodingDefinitions.h>
#include "in_sgx/plan/kaoperators/ClusterKAnonymousHashNestedLoopJoin.h"

using namespace plan::kaoperators;
using namespace plan::obexpressions;
using namespace db::obdata;
using namespace type;

ClusterKAnonymousHashNestedLoopJoin::ClusterKAnonymousHashNestedLoopJoin(
        MachineID machineId, TableID opId,
        TransmitterID leftChildTransmissionID,
        TransmitterID rightChildTransmissionID, size_vdb k,
        KAnonymousOperatorPtr leftChild,
        KAnonymousOperatorPtr rightChild,
        std::vector<pos_vdb> leftJoinAttributes,
        std::vector<pos_vdb> rightJoinAttributes,
        utilities::InSGXController *dispatcher) : KAnonymousOperator(
        opId,
        RecordSchema::join(leftChild->getSchema(), rightChild->getSchema()),
        k,
        true,
        KAnonymousOperatorPtrList{
                new ClusterKAnonymousSimpleHashRepart(machineId, leftChildTransmissionID, k, leftChild, leftJoinAttributes, dispatcher),
                new ClusterKAnonymousSimpleHashRepart(machineId, rightChildTransmissionID, k, rightChild, rightJoinAttributes, dispatcher)}
),
                                                   ClusterOperator(machineId, leftChildTransmissionID, dispatcher,
                                                                   dispatcher->getNumberOfMachines()),
                                                                                               _leftAttributes(leftJoinAttributes),
                                                                                               _rightAttributes(rightJoinAttributes),
                                                                                               _doesLeftHaveMore(true)
{
};

ClusterKAnonymousHashNestedLoopJoin::~ClusterKAnonymousHashNestedLoopJoin() {};

void callHashJoinChild(KAnonymousOperator * op) {
    op->next();
}

OperatorStatus ClusterKAnonymousHashNestedLoopJoin::next() {
    KAnonymousOperator * outer = _doesLeftHaveMore ? _children[0] : _children[1];
    KAnonymousOperator * inner = _doesLeftHaveMore ? _children[1] : _children[0];

    while(_status != OperatorStatus::NoMoreTuples) {
        if (_status == OperatorStatus::NotInitialized) {
	    outer->next();
	    inner->next();
            _doesLeftHaveMore = ((ClusterKAnonymousSimpleHashRepart *) outer)->getTotalTupleCount()
                                >= ((ClusterKAnonymousSimpleHashRepart *) inner)->getTotalTupleCount();
            outer = _doesLeftHaveMore ? _children[0] : _children[1];
            inner = _doesLeftHaveMore ? _children[1] : _children[0];
            _status = OperatorStatus::WaitingForTuples;

        } else if (_status == OperatorStatus::NoMoreTuples) {
            return _status;
        } else {
            while (outer->getStatus() != OperatorStatus::NoMoreTuples) {
                if (inner->next() == OperatorStatus::NoMoreTuples) {
                    inner->reset();
                    outer->next();
                    _countTowardK = 0;
                    continue;
                } else {
                    break;
                }
            }
        }

        _countTowardK++;

        if (outer->getStatus() == OperatorStatus::NoMoreTuples) {
            _status = OperatorStatus::NoMoreTuples;
            return _status;
        }

        ObliviousTuple outerTuple;
        ObliviousTuple innerTuple;
        outer->getCurrentTuple(outerTuple);
        inner->getCurrentTuple(innerTuple);

        bool isThisReal = !(outerTuple.isDummy() || innerTuple.isDummy());
        std::vector<pos_vdb> * outerAttributes = _doesLeftHaveMore ? &_leftAttributes : &_rightAttributes;
        std::vector<pos_vdb> * innerAttributes = _doesLeftHaveMore ? &_rightAttributes : &_leftAttributes;
        for (size_vdb i = 0, size = outerAttributes->size(); i < size; i++) {
            isThisReal &= outerTuple[outerAttributes->at(i)] == innerTuple[innerAttributes->at(i)];
        }

        if (_countTowardK > _k && !isThisReal) {
            continue;
        }

        _currentTuple = _doesLeftHaveMore ? ObliviousTuple::join(outerTuple, innerTuple) : ObliviousTuple::join(innerTuple, outerTuple);
        _currentTuple.setDummyFlag(!isThisReal);
        _status = OperatorStatus::Ok;
        break;
    }
    return _status;
}

void ClusterKAnonymousHashNestedLoopJoin::reset() {
    KAnonymousOperator::reset();
    _status = OperatorStatus :: Ok;
}

size_vdb ClusterKAnonymousHashNestedLoopJoin::getSerializationOverHead(std::set<TableID> &baseTables) const {
    return KAnonymousOperator::getSerializationOverHead(baseTables)
           + 4 + sizeof(TableID) + sizeof(size_vdb) + _leftAttributes.size() + _rightAttributes.size() + sizeof(TransmitterID) + sizeof(TransmitterID) ;
};

void ClusterKAnonymousHashNestedLoopJoin::encodeOperator(unsigned char *&writeHead) const
{
    // the type of operator it is
    *writeHead = (unsigned char) serialization::OperatorCode::KANO_CLUSTER_HASH_NESTED_LOOP_JOIN;
    writeHead++;

    // the number of child
    *writeHead = (unsigned char) 2;
    writeHead++;

    // any child
    ClusterKAnonymousSimpleHashRepart * left = (ClusterKAnonymousSimpleHashRepart*)_children[0];
    ClusterKAnonymousSimpleHashRepart * right = (ClusterKAnonymousSimpleHashRepart*)_children[1];
    left->getChild(0)->encodeOperator(writeHead);
    right->getChild(0)->encodeOperator(writeHead);

    // tableID
    *(TableID*)writeHead = _operatorId;
    writeHead += sizeof(TableID);

    // extra info: k, joinType, number of attributes, left and right attributes
    *(size_vdb*)writeHead = _k;
    writeHead += sizeof(size_vdb);

    *writeHead = (unsigned char)serialization::JoinType::INNER;
    writeHead++;

    *(TransmitterID*)writeHead = left->getTransmitterID();
    writeHead += sizeof(_transmitterID);

    *(TransmitterID*)writeHead = right->getTransmitterID();
    writeHead += sizeof(_transmitterID);

    unsigned char size = (unsigned char)_leftAttributes.size();
    *writeHead = size;
    writeHead ++;

    for (unsigned char i = 0; i < size; i ++) {
        *writeHead = (unsigned char)_leftAttributes[i];
        writeHead++;
    }

    for (unsigned char i = 0; i < size; i ++) {
        *writeHead = (unsigned char)_rightAttributes[i];
        writeHead++;
    }
}

OperatorStatus ClusterKAnonymousHashNestedLoopJoin::receiveCallBack(MachineID src_machine_id, TableID src_tid, db::obdata::ObliviousTupleTable *table) {
    if (src_machine_id != _localMachineID) {
        return OperatorStatus ::FatalError;
    }
    if (_children[0]->getOutputTableID() == src_tid) {
        return ((ClusterOperator *) _children[0])->receiveCallBack(src_machine_id, src_tid, table);
    } else if (_children[1]->getOutputTableID() == src_tid) {
        return ((ClusterOperator *) _children[1])->receiveCallBack(src_machine_id, src_tid, table);
    } else {
        return OperatorStatus :: FatalError;
    }
};
