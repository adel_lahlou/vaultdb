#include "in_sgx/plan/kaoperators/KAnonymousNestedJoin.h"

using namespace plan::kaoperators;
using namespace plan::obexpressions;
using namespace db::obdata;
using namespace type;


KAnonymousNestedJoin::KAnonymousNestedJoin(
        TableID opId,
        size_vdb k,
        KAnonymousOperatorPtr leftChild,
        KAnonymousOperatorPtr rightChild,
        JoinObliviousExpression joinPred
)   : KAnonymousOperator(
        opId,
        RecordSchema::join(leftChild->getSchema(), rightChild->getSchema()),
        k,
        false,
        KAnonymousOperatorPtrList{leftChild, rightChild}
),
      _joinPred(joinPred),
      _leftJoinTuple(new ObliviousTuple),
      _rightJoinTuple(new ObliviousTuple)
{
    _joinPred.setLeftObliviousTuple(_leftJoinTuple);
    _joinPred.setRightObliviousTuple(_rightJoinTuple);
    getLeftChild()->next();
}

KAnonymousNestedJoin::~KAnonymousNestedJoin()
{}

KAnonymousOperatorPtr KAnonymousNestedJoin::getLeftChild() const
{
    return _children[0];
}

KAnonymousOperatorPtr KAnonymousNestedJoin::getRightChild() const
{
    return _children[1];
}

OperatorStatus KAnonymousNestedJoin::next()
{
    while(getLeftChild()->getCurrentTuple(*_leftJoinTuple) == OperatorStatus::Ok){
        while(getRightChild()->next() == OperatorStatus::Ok) {
            getRightChild()->getCurrentTuple(*_rightJoinTuple);

            _countTowardK++;

            bool isThisReal = !(_leftJoinTuple->isDummy() || _rightJoinTuple->isDummy());
            isThisReal &= _joinPred.evaluate().isTruthy();

            if (_countTowardK > _k && !isThisReal) {
                continue;
            }

            _currentTuple = ObliviousTuple::join(*_leftJoinTuple, *_rightJoinTuple);
            _currentTuple.setDummyFlag(
                    !_joinPred.evaluate().isTruthy() ||
                    _leftJoinTuple->isDummy() ||
                    _rightJoinTuple->isDummy()
            );

            _status = OperatorStatus::Ok;
            return OperatorStatus::Ok;
        }

        getRightChild()->reset();
        getLeftChild()->next();
        _countTowardK = 0;
    }

    _status = OperatorStatus::NoMoreTuples;
    return OperatorStatus::NoMoreTuples;
}

void KAnonymousNestedJoin::reset()
{
    KAnonymousOperator::reset();
    getLeftChild()->next();
}
