#include <in_sgx/plan/kaoperators/ClusterKAnonymousRepartition.h>
#include "in_sgx/plan/kaoperators/ClusterKAnonymousWindow.h"
#include "in_sgx/plan/kaoperators/ClusterKAnonymousSimpleHashRepart.h"
#include "in_sgx/plan/kaoperators/KAnonymousWindow.h"

using namespace plan::kaoperators;

ClusterKAnonymousWindow::ClusterKAnonymousWindow(MachineID machineId, TableID opId, TransmitterID transmitterID,
                                                 size_vdb k, KAnonymousOperatorPtr child,
                                                 obexpressions::ObliviousExpressionList exps,
                                                 std::vector<pos_vdb> partitions, std::vector<pos_vdb> orderBys,
                                                 std::vector<SortOrder> sortOrderOrOrderBys,
                                                 bool isRepartitionSorting, utilities::InSGXController *dispatcher)
        : ClusterKAnonymousSortedAggregation(machineId, opId, k, child, isRepartitionSorting, dispatcher),
          ClusterOperator(machineId, transmitterID, dispatcher, dispatcher == nullptr ? 0 : dispatcher->getNumberOfMachines()),
          _exps(exps), _partitions(partitions), _orderBys(orderBys), _sortOrderOrOrderBys(sortOrderOrOrderBys)
{
    std::vector<pos_vdb> repartitionBys;
    for (pos_vdb i = 0, size = partitions.size(); i < size; i++) {
        repartitionBys.push_back(i);
    }
    _children = KAnonymousOperatorPtrList{
            new KAnonymousWindow(opId + 1, k,
                                 new ClusterKAnonymousRepartition(
                                         machineId, 0, transmitterID, k, child, repartitionBys,
                                         {}, {}, isRepartitionSorting, dispatcher), exps, partitions, orderBys,
                                 sortOrderOrOrderBys, dispatcher)};
    _children[0]->setParent(this);
    _outSchema = type::RecordSchema(_children[0]->getSchema());
    _outSchema.setTableID(opId);
};

ClusterKAnonymousWindow::~ClusterKAnonymousWindow(){};

size_vdb ClusterKAnonymousWindow::getGeneralizationLevel(pos_vdb index) const {
    KAnonymousWindow * bottomAgg = (KAnonymousWindow *)_children[0];
    const KAnonymousOperator * actualChild = bottomAgg->getChild(0)->getChild(0);
    std::vector<pos_vdb> references;
    bottomAgg->getExpressions()[index].getInputReferences(references);
    size_vdb maxGen;
    if (references.empty()) {
        maxGen = DEFAULT_MAX_GEN_LEVEL;
    } else {
        maxGen = 0;
        for (auto l : references) {
            size_vdb temp = actualChild->getGeneralizationLevel(l);
            maxGen = temp > maxGen ? temp : maxGen;
        }
    }
    return maxGen;
}

size_vdb ClusterKAnonymousWindow::getSerializationOverHead(std::set<TableID> &baseTables) const {
    size_vdb exprSize = 0;
    for (unsigned char i = 0, size = (unsigned char) _exps.size(); i < size; i++) {
        exprSize += _exps[i].encodeSize();
    }
    return 2 + _children[0]->getChild(0)->getChild(0)->getSerializationOverHead(baseTables)
           + sizeof(TableID) + sizeof(size_vdb)
            + 1 + _partitions.size() + 1 + _orderBys.size() * 2
            + 1 + exprSize
            + sizeof(TransmitterID) + 1;
};

void ClusterKAnonymousWindow::encodeOperator(unsigned char *&writeHead) const {
    // the type of operator it is
    *writeHead = (unsigned char) serialization::OperatorCode::KANO_CLUSTER_SORTED_WINDOW;
    writeHead++;

    // the number of child
    *writeHead = (unsigned char) 1;
    writeHead++;

    // window        sort        repart        child
    _children[0]->getChild(0)->getChild(0)->getChild(0)->encodeOperator(writeHead);

    // tableID
    *(TableID*)writeHead = _operatorId;
    writeHead += sizeof(TableID);

    // extra info: k, transmitterID, exprs, group bys, is partition sorting
    *(size_vdb*) writeHead = _k;
    writeHead += sizeof(size_vdb);

    // partition bys
    auto partitionBySize = (unsigned char)_partitions.size();
    *writeHead = partitionBySize;
    writeHead++;
    for (unsigned char i = 0; i < partitionBySize; i++) {
        *writeHead = _partitions[i];
        writeHead++;
    }

    // order bys
    auto orderBySize = (unsigned char)_orderBys.size();
    *writeHead = orderBySize;
    writeHead++;
    for (unsigned char i = 0; i < orderBySize; i++) {
        *writeHead = _orderBys[i];
        writeHead++;
    }
    for (unsigned char i = 0; i < orderBySize; i++) {
        *writeHead = (unsigned char)_sortOrderOrOrderBys[i];
        writeHead++;
    }

    // exprs
    auto exprSize = (unsigned char)_exps.size();
    *writeHead = exprSize;
    writeHead++;
    for (unsigned char i = 0; i < exprSize; i++) {
        _exps[i].encodeObliviousExpression(writeHead);
    }

    // transmitter id
    *(TransmitterID*) writeHead = _transmitterID;
    writeHead += sizeof(_transmitterID);

    // is partition sorting
    *writeHead = (unsigned char) _isRepartitionSorting;
    writeHead++;
}