#include <in_sgx/plan/kaoperators/KAnonymousSortedGroup.h>
#include <in_sgx/plan/kaoperators/ClusterKAnonymousSimpleHashRepart.h>
#include <in_sgx/utilities/SecureMove.h>
#include <in_sgx/plan/kaoperators/ClusterKAnonymousRepartition.h>
#include <in_sgx/plan/kaoperators/KAnonymousBitonicSort.h>
#include "in_sgx/plan/kaoperators/ClusterKAnonymousSortedAggregation.h"

using namespace plan::kaoperators;

ClusterKAnonymousSortedAggregation::ClusterKAnonymousSortedAggregation(MachineID machineId, TableID opId,
                                                                               TransmitterID transmitterID,
                                                                               size_vdb k,
                                                                               KAnonymousOperatorPtr child,
                                                                               obexpressions::ObliviousExpressionList exps,
                                                                               std::vector<pos_vdb> groupBys,
                                                                               bool isRepartitionSorting,
                                                                               utilities::InSGXController *dispatcher)
        : KAnonymousOperator(
        opId,
        child->getSchema(),
        k,
        false,
        KAnonymousOperatorPtrList{}),
          ClusterOperator(machineId, transmitterID, dispatcher, dispatcher == nullptr ? 0 : dispatcher->getNumberOfMachines()),
          _isRepartitionSorting(isRepartitionSorting)
{
    /**
     * Aggregations do not observe the generalization level; we enforce zeros so that repartition doesn't derive it.
     */
    std::vector<size_vdb> imposedGeneralizationLevels;
    for (pos_vdb i = 0, size = (size_vdb) child->getSchema().size(); i < size; i++) {
        imposedGeneralizationLevels.push_back(0);
    }
    _children.push_back(new KAnonymousSortedGroup(opId + 1, k,
                                                  new ClusterKAnonymousRepartition(machineId, 0, transmitterID, k, child,
                                                                                   groupBys, {}, imposedGeneralizationLevels,
                                                                                   isRepartitionSorting, dispatcher),
                                                  exps, groupBys, dispatcher));
    _children[0]->setParent(this);


    _children[0]->setParent(this);
    _outSchema = type::RecordSchema( _children[0]->getSchema());
    _outSchema.setTableID(_operatorId);
    // TODO we currently don't have AVG, so it must be constructed from SUM and COUNT
    // TODO we also don't have FLOAT type; we don't have CAST
};

ClusterKAnonymousSortedAggregation::ClusterKAnonymousSortedAggregation(MachineID machineId, TableID opId,
                                                                       size_vdb k, KAnonymousOperatorPtr child,
                                                                       bool isRepartitionSorting,
                                                                       utilities::InSGXController *dispatcher)
        : KAnonymousOperator(
        opId,
        child->getSchema(),
        k,
        false,
        KAnonymousOperatorPtrList{}
), ClusterOperator(machineId, 0, dispatcher, dispatcher == nullptr ? 0 : dispatcher->getNumberOfMachines()), _isRepartitionSorting(isRepartitionSorting)
{};

ClusterKAnonymousSortedAggregation::~ClusterKAnonymousSortedAggregation() {};

OperatorStatus ClusterKAnonymousSortedAggregation::next()
{
    if (_children[0]->next() == OperatorStatus::Ok) {
        _children[0]->getCurrentTuple(_currentTuple);
    };
    _status = _children[0]->getStatus();
    return _status;
}

void ClusterKAnonymousSortedAggregation::reset()
{
    KAnonymousOperator::reset();
}

size_vdb ClusterKAnonymousSortedAggregation::getSerializationOverHead(std::set<TableID> &baseTables) const {
    // basics
    size_vdb ret = 2 + _children[0]->getSerializationOverHead(baseTables) + sizeof(TableID) + sizeof(size_vdb);

    //transmitter id
    ret += sizeof(TransmitterID);

    // exprs
    KAnonymousSortedGroup * child = (KAnonymousSortedGroup*)_children[0];
    auto exprs = child->getExpressions();
    ret ++;
    for (unsigned char i = 0, size = (unsigned char) exprs.size(); i < size; i++) {
        ret += exprs[i].encodeSize();
    }
    // group bys
    ret += 1 + ((KAnonymousBitonicSort*)child->getChild(0))->getOrderColumns().size();
    // is partition sorting
    ret++;
    return ret;
};

void ClusterKAnonymousSortedAggregation::encodeOperator(unsigned char *&writeHead) const
{
    // the type of operator it is
    *writeHead = (unsigned char) serialization::OperatorCode::KANO_CLUSTER_SORTED_AGGREGATION;
    writeHead++;

    // the number of child
    *writeHead = (unsigned char) 1;
    writeHead++;

    // sortedGroup, sort, repartition, child
    _children[0]->getChild(0)->getChild(0)->getChild(0)->encodeOperator(writeHead);

    // tableID
    *(TableID*)writeHead = _operatorId;
    writeHead += sizeof(TableID);

    // extra info: k, transmitterID, exprs, group bys, is partition sorting
    *(size_vdb*) writeHead = _k;
    writeHead += sizeof(size_vdb);

    // group bys
    KAnonymousSortedGroup * child = (KAnonymousSortedGroup*)_children[0];
    auto groupBys = ((KAnonymousBitonicSort*)child->getChild(0))->getOrderColumns();
    auto groupBySize = (unsigned char)groupBys.size();
    *writeHead = groupBySize;
    writeHead++;
    for (unsigned char i = 0; i < groupBySize; i++) {
        *writeHead = groupBys[i];
        writeHead++;
    }

    // exprs
    auto exprs = child->getExpressions();
    auto exprSize = (unsigned char)exprs.size();
    *writeHead = exprSize;
    writeHead++;
    for (unsigned char i = 0; i < exprSize; i++) {
        exprs[i].encodeObliviousExpression(writeHead);
    }

    // transmitter id
    *(TransmitterID*) writeHead = _transmitterID;
    writeHead += sizeof(_transmitterID);

    // is partition sorting
    *writeHead = (unsigned char) _isRepartitionSorting;
    writeHead++;
}

OperatorStatus ClusterKAnonymousSortedAggregation::receiveCallBack(MachineID src_machine_id, TableID src_tid, db::obdata::ObliviousTupleTable *table) {
    return ((ClusterOperator *) _children[0]->getChild(0))->receiveCallBack(src_machine_id, src_tid, table);
};