#include <in_sgx/plan/kaoperators/ClusterKAnonymousSimpleHashRepart.h>
#include <in_sgx/obdata/ObliviousDataFactory.h>
#include <shared/QueryEncodingDefinitions.h>
#include "in_sgx/plan/kaoperators/ClusterKAnonymousPooledJoin.h"
#include "in_sgx/obdata/ObliviousTuple.h"

using namespace plan::kaoperators;

ClusterKAnonymousPooledJoin::ClusterKAnonymousPooledJoin(MachineID machineId, TableID opId,
                                                                 TableID leftChildTransmissionID,
                                                                 TableID rightChildTransmissionID, size_vdb k,
                                                                 KAnonymousOperatorPtr leftChild,
                                                                 KAnonymousOperatorPtr rightChild,
                                                                 std::vector<pos_vdb> leftJoinAttributes,
                                                                 std::vector<pos_vdb> rightJoinAttributes,
                                                                 std::vector<pos_vdb> leftPartitionKey,
                                                                 std::vector<pos_vdb> rightPartitionKey,
                                                                 utilities::InSGXController *dispatcher)
        : KAnonymousOperator(
        opId,
        type::RecordSchema::join(leftChild->getSchema(), rightChild->getSchema()),
        k,
        true,
        KAnonymousOperatorPtrList{
                new ClusterKAnonymousSimpleHashRepart(machineId, leftChildTransmissionID, k, leftChild, leftJoinAttributes, dispatcher),
                new ClusterKAnonymousSimpleHashRepart(machineId, rightChildTransmissionID, k, rightChild, rightJoinAttributes, dispatcher)}
), ClusterOperator(machineId, leftChildTransmissionID, dispatcher, dispatcher->getNumberOfMachines()),
          _leftAttributes(leftJoinAttributes),
          _rightAttributes(rightJoinAttributes),
          _leftPartitionKey(leftPartitionKey),
          _rightPartitionKey(rightPartitionKey),
          _fetchIndex(0)
{
    leftChild->setParent(this);
    rightChild->setParent(this);
};

ClusterKAnonymousPooledJoin::~ClusterKAnonymousPooledJoin() {};

OperatorStatus ClusterKAnonymousPooledJoin::next() {

    if (_status == OperatorStatus::NotInitialized) {

        bool isFirstPull = true;

        KAnonymousOperator *outer = _children[0];
        KAnonymousOperator *inner = _children[1];
        std::vector<pos_vdb> * outerAttributes;
        std::vector<pos_vdb> * innerAttributes;
        std::vector<pos_vdb> * outerProtectedPtr;
        std::vector<pos_vdb> * innerProtectedPtr;

        std::map<HashNumber, size_vdb> hashHold;
        std::map<HashNumber, std::unique_ptr<db::obdata::ObliviousTuple[]>> kHold;

        while (true) {
            if (isFirstPull) {
		outer->next();
		inner->next();
                _doesLeftHaveMore = ((ClusterKAnonymousSimpleHashRepart *) outer)->getTotalTupleCount()
                                    >= ((ClusterKAnonymousSimpleHashRepart *) inner)->getTotalTupleCount();
                outer = _doesLeftHaveMore ? _children[0] : _children[1];
                inner = _doesLeftHaveMore ? _children[1] : _children[0];
                outerAttributes = _doesLeftHaveMore ? &_leftAttributes : &_rightAttributes;
                innerAttributes = _doesLeftHaveMore ? &_rightAttributes : &_leftAttributes;
                outerProtectedPtr =_doesLeftHaveMore ? &_leftPartitionKey : &_rightPartitionKey;
                innerProtectedPtr =_doesLeftHaveMore ? &_rightPartitionKey : &_leftPartitionKey;

                _status = OperatorStatus::NoMoreTuples;

                isFirstPull = false;

            } else {
                while (outer->getStatus() != OperatorStatus::NoMoreTuples) {
                    if (inner->next() == OperatorStatus::NoMoreTuples) {
                        inner->reset();
                        outer->next();
                        continue;
                    } else {
                        break;
                    }
                }
            }

            if (outer->getStatus() == OperatorStatus::NoMoreTuples) {
                break;
            }

            db::obdata::ObliviousTuple outerTuple;
            db::obdata::ObliviousTuple innerTuple;
            outer->getCurrentTuple(outerTuple);
            inner->getCurrentTuple(innerTuple);

            bool isThisReal = !(outerTuple.isDummy() || innerTuple.isDummy());

            for (size_vdb i = 0, size = outerAttributes->size(); i < size; i++) {
                isThisReal &= outerTuple[outerAttributes->at(i)] == innerTuple[innerAttributes->at(i)];
            }


            // find the hash number
            HashNumber h = 0;
            for (size_vdb i = 0, size = outerProtectedPtr->size(); i < size; i++) {
                h = (h << 1) ^ outerTuple[outerProtectedPtr->at(i)].hash();
            }
            for (size_vdb i = 0, size = innerProtectedPtr->size(); i < size; i++) {
                h = (h << 1) ^ innerTuple[innerProtectedPtr->at(i)].hash();
            }

            auto search = hashHold.find(h);
            if (search == hashHold.end()) {
                hashHold[h] = 0;
                kHold[h] = std::unique_ptr<db::obdata::ObliviousTuple[]>(
                        new db::obdata::ObliviousTuple [_k]());
                for (pos_vdb i = 0; i < _k; i ++) {
                    kHold[h].get()[i] = db::obdata::ObliviousDataFactory::createDefaultDummyTuple(_outSchema);
                }
            }
            size_vdb hashCount = hashHold.at(h);
            if (hashCount >= _k && !isThisReal){
                continue;
            }

            // note the tuple
            db::obdata::ObliviousTuple temp = _doesLeftHaveMore ? db::obdata::ObliviousTuple::join(outerTuple, innerTuple)
                                              : db::obdata::ObliviousTuple::join(innerTuple, outerTuple);
            temp.setDummyFlag(!isThisReal);
            if (hashCount < _k) {
                kHold[h].get()[hashCount] = temp;
                hashHold[h] = hashCount + 1;
            } else {
                _overFlowPool.push_back(temp);
//                hashHold[h] = hashCount + 1;
            }
            _status = OperatorStatus::Ok;
            // continue to the next tuple
        }
        // Squeeze all hashed to overflow
        auto begin = kHold.begin();
        while (begin != kHold.end()) {
            for (pos_vdb i = 0; i < _k; i++) {
                _overFlowPool.push_back(begin->second.get()[i]);
            }
            begin++;
        }
        
//        if (_overFlowPool.size() < _k*_k) {
//            size_vdb fill = 2*_k -_overFlowPool.size();
//            for (size_vdb i = 0; i < fill; i++) {
//                _overFlowPool.push_back(db::obdata::ObliviousDataFactory::createDefaultDummyTuple(_outSchema));
//            }
//        }
        _status = OperatorStatus ::Ok;
    }

    if (_status == OperatorStatus::NoMoreTuples) {
        return _status;
    }

    if (_fetchIndex < _overFlowPool.size()) {
        _currentTuple = _overFlowPool[_fetchIndex];
        _fetchIndex++;
    } else {
        _status = OperatorStatus ::NoMoreTuples;
    }

    return _status;
}

void ClusterKAnonymousPooledJoin::reset() {
    KAnonymousOperator::reset();
    _status = _overFlowPool.empty() ? OperatorStatus ::NoMoreTuples : OperatorStatus :: Ok;
    _fetchIndex = 0;
}

OperatorStatus ClusterKAnonymousPooledJoin::receiveCallBack(MachineID src_machine_id, TableID src_tid, db::obdata::ObliviousTupleTable *table) {
    if (src_machine_id != _localMachineID) {
        return OperatorStatus ::FatalError;
    }
    if (_children[0]->getOutputTableID() == src_tid) {
        return ((ClusterOperator *) _children[0])->receiveCallBack(src_machine_id, src_tid, table);
    } else if (_children[1]->getOutputTableID() == src_tid) {
        return ((ClusterOperator *) _children[1])->receiveCallBack(src_machine_id, src_tid, table);
    } else {
        return OperatorStatus :: FatalError;
    }
};

size_vdb ClusterKAnonymousPooledJoin::getSerializationOverHead(std::set<TableID> &baseTables) const {
    return KAnonymousOperator::getSerializationOverHead(baseTables)
           + 6 + sizeof(TableID) + sizeof(size_vdb) + sizeof(TransmitterID) + sizeof(TransmitterID)
           + _leftAttributes.size() + _rightAttributes.size()
           + _leftPartitionKey.size() + _rightPartitionKey.size();
};

void ClusterKAnonymousPooledJoin::encodeOperator(unsigned char *&writeHead) const
{
    // the type of operator it is
    *writeHead = (unsigned char) serialization::OperatorCode::KANO_CLUSTER_POOLED_JOIN;
    writeHead++;

    // the number of child
    *writeHead = (unsigned char) 2;
    writeHead++;

    // any child
    ClusterKAnonymousSimpleHashRepart * left = (ClusterKAnonymousSimpleHashRepart*)_children[0];
    ClusterKAnonymousSimpleHashRepart * right = (ClusterKAnonymousSimpleHashRepart*)_children[1];
    left->getChild(0)->encodeOperator(writeHead);
    right->getChild(1)->encodeOperator(writeHead);

    // tableID
    *(TableID*)writeHead = _operatorId;
    writeHead += sizeof(TableID);

    // extra info: k, joinType, transmitter ids, number of attributes, left and right attributes
    *(size_vdb*)writeHead = _k;
    writeHead += sizeof(size_vdb);

    *writeHead = (unsigned char)serialization::JoinType::INNER;
    writeHead++;

    *(TransmitterID*)writeHead = left->getTransmitterID();
    writeHead += sizeof(_transmitterID);

    *(TransmitterID*)writeHead = right->getTransmitterID();
    writeHead += sizeof(_transmitterID);

    unsigned char size = (unsigned char)_leftAttributes.size();
    *writeHead = size;
    writeHead ++;

    for (unsigned char i = 0; i < size; i ++) {
        *writeHead = (unsigned char)_leftAttributes[i];
        writeHead++;
    }

    for (unsigned char i = 0; i < size; i ++) {
        *writeHead = (unsigned char)_rightAttributes[i];
        writeHead++;
    }

    size = (unsigned char)_leftPartitionKey.size();
    *writeHead = size;
    writeHead ++;

    for (unsigned char i = 0; i < size; i ++) {
        *writeHead = (unsigned char)_leftPartitionKey[i];
        writeHead++;
    }

    size = (unsigned char)_rightPartitionKey.size();
    *writeHead = size;
    writeHead ++;

    for (unsigned char i = 0; i < size; i ++) {
        *writeHead = (unsigned char)_rightPartitionKey[i];
        writeHead++;
    }
}
