#include <in_sgx/serialization/ObliviousSerializer.h>
#include "in_sgx/plan/oboperators/ClusterObliviousHashing.h"

using namespace plan::oboperators;
using namespace plan::obexpressions;
using namespace db::obdata;
using namespace type;

ClusterObliviousHashing::ClusterObliviousHashing(MachineID machineId, TransmitterID transmitterID,
                                                 ObliviousOperatorPtr child,
                                                 std::vector<pos_vdb> partitionAttributes,
                                                 utilities::InSGXController *dispatcher)
        : ObliviousOperator(
        transmitterID,
        child->getSchema(),
        true,
        ObliviousOperatorPtrList{child}),
                   ClusterOperator(machineId, transmitterID, dispatcher, dispatcher == nullptr ? 0 : dispatcher->getNumberOfMachines()),
                   _attributes(partitionAttributes),
                   _isTableIteratorInit(false)
{
    // Dispatch requests
    for (int i = 1, end = _totalMachineCount; i <= end; i++) {
        dispatcher->requestTable(i, _transmitterID, DEFAULT_TABLE_PARTITION_ID, this);
    }
};

ClusterObliviousHashing::~ClusterObliviousHashing() {};

size_vdb ClusterObliviousHashing::getTableCount() {
    std::unique_lock<std::mutex> guard(_tablesMutex);
    return (size_vdb) _tables.size();
}

OperatorStatus ClusterObliviousHashing::receiveCallBack(MachineID src_machine_id, TableID src_tid, db::obdata::ObliviousTupleTable *table) {
    std::unique_lock<std::mutex> localGuard(_tablesMutex);
    if (src_tid == _transmitterID) {
        _tables.emplace(src_machine_id, table);
    } else {
        return OperatorStatus::FatalError;
    }
    return OperatorStatus::Ok;
};

OperatorStatus ClusterObliviousHashing::hashAndDispatch() {
    ObliviousOperatorPtr input = _children[0];
    ObliviousTuple inputTuple;

    // hash
    std::map<HashNumber, ObliviousTupleList> tuples;
    std::map<MachineID, ObliviousTupleList> tuplesOfEachMachine;
    size_vdb attributeSize = _attributes.size();
    while (input->next() == OperatorStatus::Ok) {
        input->getCurrentTuple(inputTuple);
        HashNumber h = 0;
        for (int i = 0; i < attributeSize; i++) {
            h = (h << 1) ^ inputTuple[i].hash();
        }

        auto search = tuples.find(h);
        if (search == tuples.end()) {
            tuples[h] = ObliviousTupleList();
        }
        tuples.at(h).push_back(inputTuple);
    }

    // create a hashed table
    for (auto it = tuples.begin(); it != tuples.end(); it++) {
        MachineID dst_id = (MachineID) it->first % _totalMachineCount + 1; // Add 1 because HB is always 0
        if (tuplesOfEachMachine.find(dst_id) == tuplesOfEachMachine.end()) {
            tuplesOfEachMachine[dst_id] = ObliviousTupleList();
        }
        tuplesOfEachMachine[dst_id].insert(tuplesOfEachMachine[dst_id].end(), it->second.begin(), it->second.end());
    }
    std::set<MachineID> remaining;
    for (MachineID i = 1, size =_totalMachineCount; i <= size; i++) {
        remaining.insert(i);
    }
    std::unique_ptr<unsigned char[]> buffer(nullptr);
    type::RecordSchema schema(this->getSchema());
    schema.setTableID(_transmitterID);
    for (auto it = tuplesOfEachMachine.begin(); it != tuplesOfEachMachine.end(); it++) {
        ObliviousTupleTable block(schema, it->second);
        size_vdb blockSize = GET_NEAREST_CACHELINE_MULTIPLE(block.serializationSize());
        buffer.reset(new unsigned char[blockSize]);
        block.serializeTo(buffer.get());
        _localDispatcher->dispatch(it->first, getOutputTableID(), blockSize, buffer.get());
        remaining.erase(it->first);
    }
    // pad with empty table
    for (auto it = remaining.begin(); it != remaining.end(); it++) {
        ObliviousTupleTable block(schema, ObliviousTupleList());
        size_vdb blockSize = GET_NEAREST_CACHELINE_MULTIPLE(block.serializationSize());
        buffer.reset(new unsigned char[blockSize]);
        block.serializeTo(buffer.get());
        _localDispatcher->dispatch(*it, getOutputTableID(), blockSize, buffer.get());
    }

    return OperatorStatus::Ok;
};

OperatorStatus ClusterObliviousHashing::ensureTableIteratorReadyForUse() {
    if (!_isTableIteratorInit) {
        std::unique_lock<std::mutex> guard(_tablesMutex);
        _tableIterator = _tables.begin();
        while (_tableIterator != _tables.end()
               && _tableIterator->second->size() == 0) {
            _tableIterator++;
        }
        if (_tableIterator == _tables.end()) {
            _status = OperatorStatus::NoMoreTuples;
            return _status;
        }
        _pos = INIT_TABLE_INDEX;
        _isTableIteratorInit = true;
        _tablePtr = _tableIterator->second;
    }
    return OperatorStatus::Ok;
}


void ClusterObliviousHashing::reachForNonEmptyTable() {
    std::unique_lock<std::mutex> * guard;
    while (_tableIterator != _tables.end()
           && _tableIterator->second->size() == 0) {
        guard = new std::unique_lock<std::mutex> (_tablesMutex);
        ++_tableIterator;
        delete guard;
        _pos = INIT_TABLE_INDEX;
    }
}

void ClusterObliviousHashing::findIterator() {
    reachForNonEmptyTable();
    std::unique_lock<std::mutex> * guard = new std::unique_lock<std::mutex> (_tablesMutex);
    if (_tableIterator == _tables.end()) {
        delete guard;
        _pos = INIT_TABLE_INDEX;
        _status = OperatorStatus :: NoMoreTuples;
    } else {
        _tablePtr = _tableIterator->second;
        delete guard;
    }
}

void ClusterObliviousHashing::findNextTuple() {

    // right positions exhausted
    if ((++_pos) > _tablePtr->size()) {
        auto guard = new std::unique_lock<std::mutex> (_tablesMutex);
        // next right table
        ++_tableIterator;
        delete guard;
        _pos = INIT_TABLE_INDEX;
        findIterator();

        if (_status == OperatorStatus::NoMoreTuples) {
            return;
        }

        // update the pointers
        _tablePtr = _tableIterator->second;
    }
    return;
}

OperatorStatus ClusterObliviousHashing::next() {
    if (_status == OperatorStatus::NotInitialized) {
        hashAndDispatch();
        _status = OperatorStatus::WaitingForTuples;
    } else if (_status == OperatorStatus::NoMoreTuples) {
        return _status;
    }

    while (_status == OperatorStatus::WaitingForTuples) {
        if (getTableCount() ==  _totalMachineCount) {
            break;
        };
    }

    // find a valid machineID on each side
    if (ensureTableIteratorReadyForUse() == OperatorStatus::NoMoreTuples) {
        return _status;
    }

    findNextTuple();
    if (_pos <= _tablePtr->size()) {

        if (_status == OperatorStatus::NoMoreTuples) {
            return _status;
        }

        if (_pos == 0) {
            _pos++;
        }
        _currentTuple = _tablePtr->operator[](GET_TABLE_READ_POSITION(_pos));

        _status = OperatorStatus::Ok;
        return OperatorStatus::Ok;
    }
    _status = OperatorStatus::NoMoreTuples;
    return _status;
}

void ClusterObliviousHashing::reset() {
    _tableIterator = _tables.begin();
    _tablePtr = _tableIterator->second;
    _pos = INIT_TABLE_INDEX;
    _status = OperatorStatus :: Ok;
}
