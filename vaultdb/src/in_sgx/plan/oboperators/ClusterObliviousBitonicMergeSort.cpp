#include "in_sgx/plan/oboperators/ClusterObliviousBitonicMergeSort.h"
#include "in_sgx/plan/oboperators/BitonicSort.h"

using namespace plan::oboperators;

ClusterObliviousBitonicMergeSort::ClusterObliviousBitonicMergeSort(
        MachineID machineId, TableID opId,
       TransmitterID transmitterID,
       ObliviousOperatorPtr child,
       std::vector<pos_vdb> orderByEntries,
       std::vector<SortOrder> sortOrders,
       utilities::InSGXController *dispatcher)
        : _orderByEntries(orderByEntries),
          _sortOrders(sortOrders),
          ObliviousOperator(
            opId,
            child->getSchema(),
            true,
            ObliviousOperatorPtrList{
                    new BitonicSort(opId, child, orderByEntries, sortOrders)
            }),
          ClusterOperator(machineId, transmitterID, dispatcher, dispatcher->getNumberOfMachines())
{
    // Dispatch requests
    if (_localMachineID == DESIGNATED_COLLECTOR) {
        for (int i = 1, end = _totalMachineCount; i <= end; i++) {
            dispatcher->requestTable(i, transmitterID, DEFAULT_TABLE_PARTITION_ID, this);
        }
    }
};

ClusterObliviousBitonicMergeSort::~ClusterObliviousBitonicMergeSort(){};

size_vdb ClusterObliviousBitonicMergeSort::getTableCount()
{
    std::unique_lock<std::mutex> guard(_tablesMutex);
    return _tables.size();
}
OperatorStatus ClusterObliviousBitonicMergeSort::dispatch(
        MachineID dst_machine_id,
        ObliviousTupleList & tupleList
) {
    type::RecordSchema schema(this->getSchema());
    schema.setTableID(_transmitterID);
    ObliviousTupleTable block(schema, tupleList);
    size_vdb blockSize = GET_NEAREST_CACHELINE_MULTIPLE(block.serializationSize());
    std::unique_ptr<unsigned char[]> buffer(new unsigned char[blockSize]);
    block.serializeTo(buffer.get());
    _localDispatcher->dispatch(dst_machine_id, getOutputTableID(), blockSize, buffer.get());
    return OperatorStatus :: Ok;
}

void ClusterObliviousBitonicMergeSort::initializeIteratonTracker() {
    std::unique_lock<std::mutex> guard(_tablesMutex);
    auto it = _tables.begin();
    while (it != _tables.end()) {
        _tablesTracker.emplace(it->first, REGULAR_INIT_INDEX);
        it++;
    }
}

bool isRightLowerRank(
        ObliviousTuple & left,
        ObliviousTuple & right,
        std::vector<pos_vdb> &orderByOrdinals,
        std::vector<SortOrder> &sortingOrders
) {
    //// note: assume neither left or right are vacuous tuple
    for (int i = 0, size = orderByOrdinals.size(); i < size; i++) {
        int pos = orderByOrdinals[i];
        bool asc = sortingOrders.at(i) == SortOrder::ASCEND;
        if ((asc && left[pos] > right[pos]) || ((!asc) && left[pos] < right[pos])) {
            return true;
        } else if ((asc && left[pos] < right[pos]) || ((!asc) && left[pos] > right[pos])) {
            return false;
        }
    }
    return false;
};

OperatorStatus ClusterObliviousBitonicMergeSort::MergeForOneTuple() {
    if (_isAllDataCollected){
        if (++_pos > _finalTupleBuffer.size()) {
            _status = OperatorStatus :: NoMoreTuples;
            return _status;
        }
        _status = OperatorStatus :: Ok;
    } else {
        auto it = _tablesTracker.begin();
        auto end = _tablesTracker.end();
        std::unique_lock<std::mutex> * guard;
        std::vector<MachineID> deletes;

        bool isFirstFetched = false;
        ObliviousTuple currentSelection;
        MachineID currenSelectionMachineID;

        while (it != end) {
            guard = new std::unique_lock<std::mutex>(_tablesMutex);
            if (it->second == _tables[it->first]->size()) {
                delete guard;
                // table is fully iterated, delete from the entry;
                deletes.push_back(it->first);
            } else if (!isFirstFetched) {
                currenSelectionMachineID = it->first;
                currentSelection = *_tables[currenSelectionMachineID]->at(it->second);
                delete guard;
                isFirstFetched = true;
            } else {
                MachineID candidateMachineID = it->first;
                ObliviousTuple currentCandidate = *_tables[candidateMachineID]->at(it->second);
                delete guard;
                if (isRightLowerRank(currentSelection, currentCandidate, _orderByEntries, _sortOrders)) {
                    currentSelection = currentCandidate;
                    currenSelectionMachineID = candidateMachineID;
                }
            }
            ++it;
        }
        if (!isFirstFetched) {
            _status = OperatorStatus::NoMoreTuples;
            _isAllDataCollected = true;
        } else {
            ++(_tablesTracker[currenSelectionMachineID]);
            _currentTuple = currentSelection;
            _status = OperatorStatus::Ok;
        }
        for (MachineID id : deletes) {
            _tablesTracker.erase(id);
        }
    }
    return _status;
}

OperatorStatus ClusterObliviousBitonicMergeSort::next() {
    BitonicSort * child = (BitonicSort*)_children[0];
    if (_status == OperatorStatus::NotInitialized) {
        while (child->next() != OperatorStatus::NoMoreTuples) {
            // keep going
        }

        dispatch(DESIGNATED_COLLECTOR, child->_tupleBuffer);

        // there can be only one
        if (_localMachineID != DESIGNATED_COLLECTOR) {
            _status = OperatorStatus :: NoMoreTuples;
            return _status;
        }
        _status = OperatorStatus::WaitingForTuples;
    } else if (_status == OperatorStatus::NoMoreTuples) {
        return _status;
    }

    // only happens at the designated collector
    while (_status == OperatorStatus::WaitingForTuples) {
        if (getTableCount() ==  _totalMachineCount) {
            initializeIteratonTracker();
            break;
        };
    }
    return MergeForOneTuple();
};

void ClusterObliviousBitonicMergeSort::reset(){
    if (_isAllDataCollected) {
        _pos = POST_INIT_INIT_INDEX;
        _status = OperatorStatus::Ok;
    } else {
        std::unique_lock<std::mutex> guard(_tablesMutex);
        _tables.clear();
        _pos = INIT_TABLE_INDEX;
        _status = OperatorStatus::NotInitialized;
    }
};

OperatorStatus ClusterObliviousBitonicMergeSort::receiveCallBack(MachineID src_machine_id, TableID src_tid, db::obdata::ObliviousTupleTable *table) {
    std::unique_lock<std::mutex> localGuard(_tablesMutex);
    if (src_tid == _operatorId) {
        _tables.emplace(src_machine_id, table);
    } else {
        return OperatorStatus::FatalError;
    }
    return OperatorStatus::Ok;
};