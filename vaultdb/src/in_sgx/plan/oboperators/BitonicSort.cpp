#include <cstring>
#include <shared/QueryEncodingDefinitions.h>
#include <set>
#include "in_sgx/plan/oboperators/BitonicSort.h"
#include "in_sgx/utilities/SecureMove.h"
#include "in_sgx/serialization/ObliviousSerializer.h"

using namespace plan::oboperators;
using namespace db::obdata;
using namespace utilities;

BitonicSort::BitonicSort(TableID opId,
                         ObliviousOperatorPtr child,
                         std::vector<pos_vdb> orderByEntries,
                         std::vector<SortOrder> sortOrders
) : ObliviousOperator(opId, child->getSchema(), true, ObliviousOperatorPtrList{child}) {
    _orderByEntries = orderByEntries;
    _sortOrders = sortOrders;
}

BitonicSort::~BitonicSort() {}

void BitonicSort::reset() {
    if (_tupleBuffer.size() > 0) {
        _outputPos = 0;
        _status = OperatorStatus::Ok;
    }
}

OperatorStatus BitonicSort::next() {
    ObliviousTuple input;
    OperatorStatus status;
    ObliviousTupleList inputBuffer;
    ObliviousTupleList outputBuffer;

    if (_status == OperatorStatus::NotInitialized) {

        while ((status = _children[0]->next()) == OperatorStatus::Ok) {
            _children[0]->getCurrentTuple(input);
            inputBuffer.push_back(input);
        }
        if (status == OperatorStatus::FatalError) {
            return status;
        }

        // ensure inputBuffer contains 2 power entries by adding NULL pointers
        // The NULL add/delete process could be public because input cardinality is either already masked or unprotected information
        unsigned int v = (unsigned int) inputBuffer.size();
        v--;
        v |= v >> 1;
        v |= v >> 2;
        v |= v >> 4;
        v |= v >> 8;
        v |= v >> 16;
        v++;
        v = v - (unsigned int) inputBuffer.size();

        for (int counter = 0; counter < v; counter++) {
            inputBuffer.push_back(ObliviousTuple::getVacuousTuple());
        }

        sort(getSchema(), inputBuffer, outputBuffer, _orderByEntries, _sortOrders);

        for (int i = 0, size = outputBuffer.size(); i < size; i++) {
            if (!outputBuffer.at(i).isVacuous()) {
                ObliviousTuple p = outputBuffer.at(i);
                _tupleBuffer.push_back(p);
            }
        }
        _outputPos = -1;
        _status = OperatorStatus::Ok;
    }

    // if outputBuffer is not empty, let the iterator return the next
    if (_tupleBuffer.size() > 0 && _outputPos < (int) (_tupleBuffer.size() - 1)) {
        _outputPos++;
        _currentTuple = _tupleBuffer.at(_outputPos);
    } else {
        _status = OperatorStatus::NoMoreTuples;
    }

    return _status;
};


void BitonicSort::sort(
        const type::RecordSchema &schema,
        ObliviousTupleList &inputBuffer,
        ObliviousTupleList &outputBuffer,
        std::vector<pos_vdb> &orderByOrdinals,
        std::vector<SortOrder> &sortingOrders
) {
    if (inputBuffer.size() <= 1) {
        if (inputBuffer.size() == 1) {
            ObliviousTuple p = inputBuffer.at(0);
            outputBuffer.push_back(p);
        }
        return;
    }

    std::vector<SortOrder> originalOrder;
    std::vector<SortOrder> reverseOrder;
    std::vector<SortOrder> orderSwap;
    for (int i = 0, size = sortingOrders.size(); i < size; i++) {
        if (sortingOrders.at(i) == SortOrder::ASCEND) {
            originalOrder.push_back(SortOrder::ASCEND);
            reverseOrder.push_back(SortOrder::DESCEND);
        } else {
            originalOrder.push_back(SortOrder::DESCEND);
            reverseOrder.push_back(SortOrder::ASCEND);
        }
    }
    if (sortingOrders.at(0) == SortOrder::DESCEND) {
        orderSwap = originalOrder;
        originalOrder = reverseOrder;
        reverseOrder = orderSwap;
    }

    ObliviousTupleList::iterator cutoff = inputBuffer.begin() + inputBuffer.size() / 2;

    ObliviousTupleList leftSplit = ObliviousTupleList(inputBuffer.begin(), cutoff);
    ObliviousTupleList leftSorted;
    sort(schema, leftSplit, leftSorted, orderByOrdinals, originalOrder);

    ObliviousTupleList rightSplit = ObliviousTupleList(cutoff, inputBuffer.end());
    ObliviousTupleList rightSorted;
    sort(schema, rightSplit, rightSorted, orderByOrdinals, reverseOrder);

    leftSorted.insert(leftSorted.end(), rightSorted.begin(), rightSorted.end());
    merge(schema, leftSorted, outputBuffer, orderByOrdinals, sortingOrders);
};

void BitonicSort::merge(
        const type::RecordSchema &schema,
        ObliviousTupleList &concat,
        ObliviousTupleList &outputBuffer,
        std::vector<pos_vdb> &orderByOrdinals,
        std::vector<SortOrder> &sortingOrders
) {
    if (concat.size() == 1) {
        ObliviousTuple p = concat.at(0);
        outputBuffer.push_back(p);
        return;
    }

    compare(schema, concat, orderByOrdinals, sortingOrders);

    ObliviousTupleList::iterator cutoff = concat.begin() + concat.size() / 2;

    ObliviousTupleList leftSplit = ObliviousTupleList(concat.begin(), cutoff);
    ObliviousTupleList leftSorted;
    merge(schema, leftSplit, leftSorted, orderByOrdinals, sortingOrders);

    ObliviousTupleList rightSplit = ObliviousTupleList(cutoff, concat.end());
    ObliviousTupleList rightSorted;
    merge(schema, rightSplit, rightSorted, orderByOrdinals, sortingOrders);

    outputBuffer.insert(outputBuffer.end(), leftSorted.begin(), leftSorted.end());
    outputBuffer.insert(outputBuffer.end(), rightSorted.begin(), rightSorted.end());
};


void BitonicSort::compare(
        const type::RecordSchema &schema,
        ObliviousTupleList &concat,
        std::vector<pos_vdb> &orderByOrdinals,
        std::vector<SortOrder> &sortingOrders
) {
    int dist = (int) (concat.size() / 2);

    for (int i = 0; i < dist; i++) {

        if (concat.at(i).isVacuous()) {
            if (sortingOrders.at(0) == SortOrder::ASCEND) {
                ObliviousTuple left = concat.at(i);
                concat[i] = concat[i + dist];
                concat[i + dist] = left;
            }
            continue;
        }
        if (concat.at(i + dist).isVacuous()) {
            if (sortingOrders.at(0) == SortOrder::DESCEND) {
                ObliviousTuple left = concat.at(i);
                concat[i] = concat[i + dist];
                concat[i + dist] = left;
            }
            continue;
        }

        ////IMPLEMENT
        int verdict = compareAgainstPseudoOblivious(
                schema,
                concat.at(i), concat.at(i + dist),
                orderByOrdinals, sortingOrders);

        ObliviousTuple temp = concat.at(i);

        size_vdb size = schema.tupleSerializationSize();
        if (size % 4 != 0) size = (size / sizeof(int) + 1) * sizeof(int);
        size_vdb intSteps = size / sizeof(int);

        unsigned char leftSerial[size];
        memset(leftSerial, 0, size);
        temp.serializeTo(leftSerial, schema);

        temp = concat.at(i + dist);
        unsigned char rightSerial[size];
        memset(rightSerial, 0, size);
        temp.serializeTo(rightSerial, schema);

        int predicate = (int) (verdict < 0);
        for (int i = 0; i < intSteps; i++) {
            int tempLeftInt = *(int *) (leftSerial + i * sizeof(int));
            int rightInt = *(int *) (rightSerial + i * sizeof(int));
            *(int *) (leftSerial + i * sizeof(int)) = cmov_int(predicate, rightInt, tempLeftInt);
            *(int *) (rightSerial + i * sizeof(int)) = cmov_int(predicate, tempLeftInt, rightInt);
        }

        concat[i] = ObliviousSerializer::deserializeObliviousTuple(schema, leftSerial);
        concat[i + dist] = ObliviousSerializer::deserializeObliviousTuple(schema, rightSerial);

    }
};


int BitonicSort::compareAgainstPseudoOblivious(
        const type::RecordSchema &schema,
        ObliviousTuple left,
        ObliviousTuple right,
        std::vector<pos_vdb> &orderByOrdinals,
        std::vector<SortOrder> &sortingOrders
) {
    // int predicate = (int) ((verdict < 0 && isAscend) || (verdict > 0 && (!isAscend)));
    int ret = 0;

    //// note: assume neither left or right are vacuous tuple

    for (int i = 0, size = orderByOrdinals.size(); i < size; i++) {
        int pos = orderByOrdinals[i];
        bool asc = sortingOrders.at(i) == SortOrder::ASCEND;
        int tempRet = 1;
        int condition = (int) ((left[pos] < right[pos] && asc) || (left[pos] > right[pos] && (!asc)));
        tempRet = cmov_int(condition, tempRet, -1);
        tempRet = cmov_int((int) (left[pos] == right[pos]), 0, tempRet);
        ret = cmov_int((int) (ret == 0 && tempRet != 0), tempRet, ret);
    }
    return ret;
};

std::vector<pos_vdb> BitonicSort::getOrderColumns()
{
    return std::vector<pos_vdb> (_orderByEntries);
};

size_vdb BitonicSort::getSerializationOverHead(std::set<TableID> &baseTables) const {
    return 3 + sizeof(TableID) + sizeof(size_vdb)
           + _children[0]->getSerializationOverHead(baseTables) + _orderByEntries.size() * 2;
};

void BitonicSort::encodeOperator(unsigned char *&writeHead) const
{
    // the type of operator it is
    *writeHead = (unsigned char) serialization::OperatorCode::OBLIVIOUS_SORT;
    writeHead++;

    // the number of child
    *writeHead = (unsigned char) 1;
    writeHead++;

    // any child
    _children[0]->encodeOperator(writeHead);

    // tableID
    *(TableID*)writeHead = _operatorId;
    writeHead += sizeof(TableID);

    // extra info: indices, orders
    pos_vdb size = _orderByEntries.size();
    *writeHead = (unsigned char) size;
    writeHead++;
    for (pos_vdb i = 0; i < size; i++) {
        *writeHead = (unsigned char)_orderByEntries[i];
        writeHead++;
    }
    for (pos_vdb i = 0; i < size; i++) {
        *writeHead = (unsigned char)_sortOrders[i];
        writeHead++;
    }
}