#include <in_sgx/plan/oboperators/ObliviousSortedGroup.h>
#include <in_sgx/plan/oboperators/ClusterObliviousHashing.h>
#include "in_sgx/plan/oboperators/ClusterObliviousHashSortedAggregation.h"

using namespace plan::oboperators;

ClusterObliviousHashSortedAggregation::ClusterObliviousHashSortedAggregation(MachineID machineId, TableID opId,
                                                                             TransmitterID transmitterID,
                                                                             ObliviousOperatorPtr child,
                                                                             obexpressions::ObliviousExpressionList exps,
                                                                             std::vector<pos_vdb> groupBys,
                                                                             utilities::InSGXController *dispatcher)
        : ObliviousOperator(
        opId,
        child->getSchema(),
        false,
        ObliviousOperatorPtrList{
                new ObliviousSortedGroup(
                        opId,
                        new ClusterObliviousHashing(machineId, transmitterID, child, groupBys, dispatcher),
                        exps,
                        groupBys)}),
          ClusterOperator(machineId, transmitterID, dispatcher, dispatcher->getNumberOfMachines())
{
    _children[0]->setParent(this);
    _outSchema = _children[0]->getSchema();
    // TODO we currently don't have AVG, so it must be constructed from SUM and COUNT
    // TODO we also don't have FLOAT type; we don't have CAST
};

ClusterObliviousHashSortedAggregation::~ClusterObliviousHashSortedAggregation() {};

OperatorStatus ClusterObliviousHashSortedAggregation::next()
{
    while (_children[0]->next() == OperatorStatus::Ok) {
        _children[0]->getCurrentTuple(_currentTuple);
        if (_currentTuple.isDummy()) {
            continue;
        } else {
            break;
        }
    };
    _status = _children[0]->getStatus();
    return _status;
}

void ClusterObliviousHashSortedAggregation::reset()
{
    ObliviousOperator::reset();
}

OperatorStatus ClusterObliviousHashSortedAggregation::receiveCallBack(MachineID src_machine_id, TableID src_tid, db::obdata::ObliviousTupleTable *table) {
    return ((ClusterOperator *) _children[0]->getChild(0))->receiveCallBack(src_machine_id, src_tid, nullptr);
};
