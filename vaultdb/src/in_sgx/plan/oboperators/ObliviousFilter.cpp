#include <shared/QueryEncodingDefinitions.h>
#include <set>
#include "in_sgx/plan/oboperators/ObliviousFilter.h"

using namespace plan::oboperators;
using namespace plan::obexpressions;
using namespace db::obdata;


ObliviousFilter::ObliviousFilter(
        TableID opId,
        ObliviousOperatorPtr child,
        ObliviousExpression filterPred
)
        : ObliviousOperator(opId, child->getSchema(), false, ObliviousOperatorPtrList{child}),
          _filterPred(filterPred)
{
    _filterTuple = new ObliviousTuple;
    _filterPred.setObliviousTuple(_filterTuple);
}

ObliviousFilter::~ObliviousFilter() {
    delete _filterTuple;
}


OperatorStatus ObliviousFilter::next()
{
    ObliviousTuple input;

    if(_children[0]->next() == OperatorStatus::Ok) {
        _children[0]->getCurrentTuple(input);
        *_filterTuple = input;

        _currentTuple = input;
        _currentTuple.setDummyFlag(
                !_filterPred.evaluate().isTruthy() ||
                _currentTuple.isDummy()
        );
        _status = OperatorStatus::Ok;
        return _status;
    }

    _status = OperatorStatus::NoMoreTuples;
    return _status;
}

void ObliviousFilter::reset() {
    ObliviousOperator::reset();
}

size_vdb ObliviousFilter::getSerializationOverHead(std::set<TableID> &baseTables) const {
    return 2 + sizeof(TableID) + sizeof(size_vdb)
           + _children[0]->getSerializationOverHead(baseTables)
           + _filterPred.encodeSize();
};

void ObliviousFilter::encodeOperator(unsigned char *&writeHead) const
{
    // the type of operator it is
    *writeHead = (unsigned char) serialization::OperatorCode::OBLIVIOUS_FILTER;
    writeHead++;

    // the number of child
    *writeHead = (unsigned char) 1;
    writeHead++;

    // any child
    _children[0]->encodeOperator(writeHead);

    // tableID
    *(TableID*)writeHead = _operatorId;
    writeHead += sizeof(TableID);

    // extra info: filter expression
    _filterPred.encodeObliviousExpression(writeHead);
}