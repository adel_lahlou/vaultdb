#include <shared/QueryEncodingDefinitions.h>
#include "in_sgx/plan/oboperators/ObliviousKAnonymous.h"

using namespace plan::oboperators;
using namespace db::obdata;

ObliviousKAnonymous::ObliviousKAnonymous(
        plan::kaoperators::KAnonymousOperatorPtr child
) : ObliviousOperator(1, child->getSchema(), false, ObliviousOperatorPtrList{})
{
    _child = child;
};
ObliviousKAnonymous::~ObliviousKAnonymous(){
    if (_projectTuple != nullptr) {
        delete(_projectTuple);
    }
    delete _child;
};

OperatorStatus ObliviousKAnonymous::next() {
    if (_status == OperatorStatus:: NoMoreTuples) {
        return _status;
    } else if(_child->next() == OperatorStatus::NoMoreTuples) {
        _status = OperatorStatus::NoMoreTuples;
        return OperatorStatus::NoMoreTuples;
    } else {
        _child->getCurrentTuple(_currentTuple);
        _status = OperatorStatus::Ok;
        return OperatorStatus::Ok;
    }
};

void ObliviousKAnonymous::reset(){
    ObliviousKAnonymous::reset();
};

void ObliviousKAnonymous::encodeOperator(unsigned char *&writeHead) const
{
    // the type of operator it is
    *writeHead = (unsigned char) serialization::OperatorCode::OBLIVIOUS_SOURCE_FROM_KANO;
    writeHead++;

    // the number of child, not written

    // any child
    _child->encodeOperator(writeHead);

    // extra info: none
};