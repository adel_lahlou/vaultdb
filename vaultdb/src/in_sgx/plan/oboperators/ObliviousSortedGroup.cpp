#include <shared/QueryEncodingDefinitions.h>
#include <set>
#include "in_sgx/plan/oboperators/ObliviousSortedGroup.h"
#include "in_sgx/plan/oboperators/BitonicSort.h"
#include "in_sgx/obdata/ObliviousDataFactory.h"
#include "in_sgx/plan/obexpressions/ObliviousExpressionFactory.h"

using namespace plan::oboperators;
using namespace plan::obexpressions;
using namespace db::obdata;


ObliviousSortedGroup::ObliviousSortedGroup(
        TableID opId,
        ObliviousOperatorPtr child,
        obexpressions::ObliviousExpressionList exps,
        std::vector<pos_vdb> groupBys
)
        : ObliviousOperator(
            opId,
            child->getSchema(),
            false,
            ObliviousOperatorPtrList{}
        ),
        _exps(exps), _isCurrentGroupAllDummy(true)
{
    std::vector<SortOrder> sortOrders(groupBys.size());

    for(int i = 0; i < groupBys.size(); ++i){
        sortOrders.push_back(SortOrder::ASCEND);
    }

    _children.push_back(
            new BitonicSort(
                    0,
                    child,
                    groupBys,
                    sortOrders
            )
    );

    _lastAggTuple = new ObliviousTuple;
    _curAggTuple = new ObliviousTuple;

    *_curAggTuple = ObliviousDataFactory::createDefaultTuple(_outSchema);
    _exps.setObliviousTuple(_curAggTuple);
    _outSchema = type::RecordSchema(opId, _exps.outputDatatype());

    _exps.setObliviousTuple(_curAggTuple);

    for(auto col : groupBys) {
        ObliviousExpression lhs = ObliviousExpressionFactory::makeColumn(col, _outSchema[col]);
        ObliviousExpression rhs = ObliviousExpressionFactory::makeColumn(col, _outSchema[col]);

        lhs.setObliviousTuple(_lastAggTuple);
        rhs.setObliviousTuple(_curAggTuple);
        ObliviousExpression eq = ObliviousExpressionFactory::makeEq(lhs, rhs);
        _checkInGroup.push_back(eq);
    }

    for(int i = 0; i < _exps.size(); ++i) {
        if(_exps[i].isAggregateFunction()){
            _aggs.push_back(AggregateFunction(_exps[i]));
        }
    }
}

ObliviousSortedGroup::~ObliviousSortedGroup() {
    delete _lastAggTuple;
    delete _curAggTuple;
}


OperatorStatus ObliviousSortedGroup::next()
{
    if(_firstPull) {
        _firstPull = false;

        if(_children[0]->next() == OperatorStatus::Ok) {
            _children[0]->getCurrentTuple(*_curAggTuple);
            _aggs.update(!_curAggTuple->isDummy());
            _status = OperatorStatus::Ok;
        }
    }

    if(_children[0]->next() == OperatorStatus::Ok) {
        _nextMiddle();
        _status = OperatorStatus::Ok;
        return OperatorStatus::Ok;
    }

    if(_status == OperatorStatus::Ok && !_finishedPulling) {
        _nextEnd();
        _finishedPulling = true;
        _status = OperatorStatus::Ok;
        return OperatorStatus::Ok;
    }

    _status = OperatorStatus::NoMoreTuples;
    return OperatorStatus::NoMoreTuples;
}

void ObliviousSortedGroup::reset() {
    ObliviousOperator::reset();
    _aggs.reset(true);
    _firstPull = true;
    _finishedPulling = false;
    _isCurrentGroupAllDummy = true;
}

void ObliviousSortedGroup::_nextMiddle()
{
    *_lastAggTuple = *_curAggTuple;
    _isCurrentGroupAllDummy &= _lastAggTuple->isDummy();
    _children[0]->getCurrentTuple(*_curAggTuple);

    bool isNewAgg = !_checkInGroup.isAllTruthy();
    _evaluate();
    _currentTuple.setDummyFlag(!isNewAgg || _isCurrentGroupAllDummy);
    _aggs.reset(isNewAgg);
    _aggs.update(!_curAggTuple->isDummy());
    _isCurrentGroupAllDummy |= isNewAgg;
}

void ObliviousSortedGroup::_nextEnd()
{
    _currentTuple = _exps.evaluate();
}

void ObliviousSortedGroup::_evaluate()
{
    ObliviousTuple tmp = *_curAggTuple;
    *_curAggTuple = *_lastAggTuple;
    _currentTuple = _exps.evaluate();
    *_curAggTuple = tmp;
}

size_vdb ObliviousSortedGroup::getSerializationOverHead(std::set<TableID> &baseTables) const {
    size_vdb ret = 4 + sizeof(TableID) + sizeof(size_vdb);
    ret += _children[0]->getSerializationOverHead(baseTables);
    ret += ((BitonicSort*)_children[0])->getOrderColumns().size();
    for (pos_vdb i = 0, outputSize = (pos_vdb)_exps.size(); i < outputSize; i++) {
        ret += _exps[i].encodeSize();
    }
    return ret;
};

void ObliviousSortedGroup::encodeOperator(unsigned char *&writeHead) const
{
    // the type of operator it is
    *writeHead = (unsigned char) serialization::OperatorCode::OBLIVIOUS_AGGREGATE;
    writeHead++;

    // the number of child
    *writeHead = (unsigned char) 1;
    writeHead++;

    // child: child of the artificially added BitonicSort
    BitonicSort * sort = (BitonicSort*)_children[0];
    sort->getChild(0)->encodeOperator(writeHead);

    // operatorID
    *(TableID*)writeHead = _operatorId;
    writeHead += sizeof(TableID);

    // extra info: order bys, outputs
    std::vector<pos_vdb> columns = sort->getOrderColumns();
    pos_vdb groupBySize = (pos_vdb)columns.size();
    *writeHead = (unsigned char) groupBySize;
    writeHead++;
    for (pos_vdb i = 0; i < groupBySize; i++) {
        *writeHead = (unsigned char) columns[i];
        writeHead++;
    }
    pos_vdb outputSize = (pos_vdb)_exps.size();
    *writeHead = (unsigned char) outputSize;
    writeHead++;
    for (pos_vdb i = 0; i < outputSize; i++) {
        _exps[i].encodeObliviousExpression(writeHead);
    }
}