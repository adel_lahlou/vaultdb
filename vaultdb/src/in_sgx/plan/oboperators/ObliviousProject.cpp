#include <shared/QueryEncodingDefinitions.h>
#include <set>
#include "in_sgx/plan/oboperators/ObliviousProject.h"
#include "in_sgx/obdata/ObliviousDataFactory.h"

using namespace plan::obexpressions;
using namespace plan::oboperators;
using namespace db::obdata;

ObliviousProject::ObliviousProject(
        TableID opId,
        ObliviousOperatorPtr child,
        ObliviousExpressionList exps
)
        : ObliviousOperator(opId, child->getSchema(), false, ObliviousOperatorPtrList{child}),
          _exps(exps), _projectTuple(new ObliviousTuple)
{
    ObliviousTuple dummy = ObliviousDataFactory::createDefaultTuple(_outSchema);
    _exps.setObliviousTuple(&dummy);
    _outSchema = type::RecordSchema(_outSchema.getTableID(), _exps.outputDatatype());
    _exps.setObliviousTuple(_projectTuple);
}

ObliviousProject::~ObliviousProject() {
    delete _projectTuple;
}


OperatorStatus ObliviousProject::next()
{
    if(_children[0]->next() == OperatorStatus::NoMoreTuples) {
        _status = OperatorStatus::NoMoreTuples;
        return OperatorStatus::NoMoreTuples;
    } else {
        _children[0]->getCurrentTuple(*_projectTuple);
        _currentTuple = _exps.evaluate();
        _currentTuple.setDummyFlag(_projectTuple->isDummy());
        _status = OperatorStatus::Ok;
        return OperatorStatus::Ok;
    }
}

void ObliviousProject::reset()
{
    ObliviousOperator::reset();
}

size_vdb ObliviousProject::getSerializationOverHead(std::set<TableID> &baseTables) const {
    size_vdb ret = 3 + sizeof(TableID) + sizeof(size_vdb);
    ret += _children[0]->getSerializationOverHead(baseTables);
    for (pos_vdb i = 0, outputSize = (pos_vdb)_exps.size(); i < outputSize; i++) {
        ret += _exps[i].encodeSize();
    }
    return ret;
};

void ObliviousProject::encodeOperator(unsigned char *&writeHead) const
{
    // the type of operator it is
    *writeHead = (unsigned char) serialization::OperatorCode::OBLIVIOUS_PROJECT;
    writeHead++;

    // the number of child
    *writeHead = (unsigned char) 1;
    writeHead++;

    // any child
    _children[0]->encodeOperator(writeHead);

    // tableID
    *(TableID*)writeHead = _operatorId;
    writeHead += sizeof(TableID);

    // extra info: expressions
    pos_vdb size = _exps.size();
    *writeHead = (unsigned char) size;
    writeHead++;
    for (pos_vdb i = 0; i < size; i++) {
        _exps[i].encodeObliviousExpression(writeHead);
    }
}