#include "in_sgx/plan/oboperators/ClusterObliviousHashJoin.h"

using namespace plan::oboperators;
using namespace plan::obexpressions;
using namespace db::obdata;
using namespace type;

ClusterObliviousHashJoin::ClusterObliviousHashJoin(MachineID machineId, TableID opId,
                                                   TransmitterID leftTransmitterID,
                                                   TransmitterID rightTransmitterID,
                                                   ObliviousOperatorPtr leftChild,
                                                   ObliviousOperatorPtr rightChild,
                                                   std::vector<pos_vdb> leftJoinAttributes,
                                                   std::vector<pos_vdb> rightJoinAttributes,
                                                   utilities::InSGXController *dispatcher) : ObliviousOperator(
        opId,
        RecordSchema::join(leftChild->getSchema(), rightChild->getSchema()),
        true,
        ObliviousOperatorPtrList{
                new ClusterObliviousHashing(machineId, leftTransmitterID, leftChild, leftJoinAttributes, dispatcher),
                new ClusterObliviousHashing(machineId, rightTransmitterID, rightChild, rightJoinAttributes, dispatcher)}
),
                                                                                             ClusterOperator(machineId, 0, dispatcher, dispatcher->getNumberOfMachines()),
                                                                                             _leftAttributes(leftJoinAttributes),
                                                                                             _rightAttributes(rightJoinAttributes)
{
    leftChild->setParent(this);
    rightChild->setParent(this);
};

ClusterObliviousHashJoin::~ClusterObliviousHashJoin() {};


OperatorStatus ClusterObliviousHashJoin::next() {
    ObliviousOperator * left = _children[0];
    ObliviousOperator * right = _children[1];

    if (_status == OperatorStatus::NotInitialized) {
	left->next();
	right->next();
        _status = OperatorStatus::WaitingForTuples;
    } else if (_status == OperatorStatus::NoMoreTuples) {
        return _status;
    } else {
        while (left->getStatus() != OperatorStatus::NoMoreTuples) {
            if (right->next() == OperatorStatus::NoMoreTuples) {
                right->reset();
                left->next();
                continue;
            } else {
                break;
            }
        }
    }

    if (left->getStatus() == OperatorStatus::NoMoreTuples) {
        _status = OperatorStatus :: NoMoreTuples;
        return _status;
    }

    ObliviousTuple leftTuple;
    ObliviousTuple rightTuple;
    _children[0]->getCurrentTuple(leftTuple);
    _children[1]->getCurrentTuple(rightTuple);

    bool isThisReal = true;

    for (int i = 0, size = _leftAttributes.size(); i < size ; i++) {
        isThisReal &= leftTuple[_leftAttributes[i]] == rightTuple[_rightAttributes[i]];
    }
    _currentTuple = ObliviousTuple::join(leftTuple, rightTuple);
    _currentTuple.setDummyFlag(leftTuple.isDummy() || rightTuple.isDummy() || !isThisReal);
    _status = OperatorStatus::Ok;
    return OperatorStatus::Ok;
}

void ClusterObliviousHashJoin::reset() {
    ObliviousOperator::reset();
    _status = OperatorStatus :: Ok;
}

OperatorStatus ClusterObliviousHashJoin::receiveCallBack(MachineID src_machine_id, TableID src_tid, db::obdata::ObliviousTupleTable *table) {
    if (src_machine_id != _localMachineID) {
        return OperatorStatus ::FatalError;
    }
    if (_children[0]->getOutputTableID() == src_tid) {
        return ((ClusterOperator *) _children[0])->receiveCallBack(src_machine_id, src_tid, nullptr);
    } else if (_children[1]->getOutputTableID() == src_tid) {
        return ((ClusterOperator *) _children[1])->receiveCallBack(src_machine_id, src_tid, nullptr);
    } else {
        return OperatorStatus :: FatalError;
    }
};
