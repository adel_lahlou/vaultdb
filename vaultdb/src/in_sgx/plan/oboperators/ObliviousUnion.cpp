#include "in_sgx/plan/oboperators/ObliviousUnion.h"

using namespace db::obdata::obfield;
using namespace db::obdata;
using namespace plan::oboperators;


ObliviousUnion::ObliviousUnion(TableID opId, ObliviousOperatorPtrList children)
        : ObliviousOperator(
        opId,
        children[0]->getSchema(),
        false,
        children
    )
{}

ObliviousUnion::~ObliviousUnion() {}


OperatorStatus ObliviousUnion::next()
{
    for(;_curChild < _children.size(); ++ _curChild) {
        if(getCurrentChild()->next() == OperatorStatus::Ok){
            getCurrentChild()->getCurrentTuple(_currentTuple);
            _status = OperatorStatus::Ok;
            return OperatorStatus::Ok;
        }
    }

    _status = OperatorStatus::NoMoreTuples;
    return OperatorStatus::NoMoreTuples;
}

void ObliviousUnion::reset()
{
    ObliviousOperator::reset();
    _curChild = 0;
}


ObliviousOperatorPtr ObliviousUnion::getCurrentChild() const
{
    return _children[_curChild];
}
