#include "in_sgx/plan/oboperators/ObliviousNestedJoin.h"

using namespace plan::oboperators;
using namespace plan::obexpressions;
using namespace db::obdata;
using namespace type;


ObliviousNestedJoin::ObliviousNestedJoin(
        TableID opId,
        ObliviousOperatorPtr leftChild,
        ObliviousOperatorPtr rightChild,
        JoinObliviousExpression joinPred
)   : ObliviousOperator(
        opId,
        RecordSchema::join(leftChild->getSchema(), rightChild->getSchema()),
        false,
        ObliviousOperatorPtrList{leftChild, rightChild}
),
      _joinPred(joinPred),
      _leftJoinTuple(new ObliviousTuple),
      _rightJoinTuple(new ObliviousTuple)
{
    _joinPred.setLeftObliviousTuple(_leftJoinTuple);
    _joinPred.setRightObliviousTuple(_rightJoinTuple);
    getLeftChild()->next();
}

ObliviousNestedJoin::~ObliviousNestedJoin()
{
    delete _leftJoinTuple;
    delete _rightJoinTuple;
}

ObliviousOperatorPtr ObliviousNestedJoin::getLeftChild() const
{
    return _children[0];
}

ObliviousOperatorPtr ObliviousNestedJoin::getRightChild() const
{
    return _children[1];
}

OperatorStatus ObliviousNestedJoin::next()
{
    while(getLeftChild()->getCurrentTuple(*_leftJoinTuple) == OperatorStatus::Ok){
        while(getRightChild()->next() == OperatorStatus::Ok) {
            getRightChild()->getCurrentTuple(*_rightJoinTuple);


            _currentTuple = ObliviousTuple::join(*_leftJoinTuple, *_rightJoinTuple);
            _currentTuple.setDummyFlag(
                    !_joinPred.evaluate().isTruthy() ||
                    _leftJoinTuple->isDummy() ||
                    _rightJoinTuple->isDummy()
            );

            _status = OperatorStatus::Ok;
            return OperatorStatus::Ok;
        }

        getRightChild()->reset();
        getLeftChild()->next();
    }

    _status = OperatorStatus::NoMoreTuples;
    return OperatorStatus::NoMoreTuples;
}

void ObliviousNestedJoin::reset()
{
    ObliviousOperator::reset();
    getLeftChild()->next();
}
