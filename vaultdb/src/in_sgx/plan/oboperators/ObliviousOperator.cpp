#include <set>
#include "in_sgx/plan/oboperators/ObliviousOperator.h"

using namespace plan::oboperators;
using namespace type;


ObliviousOperator::ObliviousOperator() {}

ObliviousOperator::ObliviousOperator(const ObliviousOperator &o) {}

ObliviousOperator::ObliviousOperator(
    TableID opId,
    RecordSchema outSchema,
    bool blocking,
    ObliviousOperatorPtrList children
)       : _operatorId(opId), _outSchema(outSchema), _blocking(blocking)
{
    _status = OperatorStatus::NotInitialized;

    for (auto c : children){
        c->setParent(this);
    }

    _children = children;
    _parent = nullptr;

}

ObliviousOperator::~ObliviousOperator()
{
    for(auto p : _children)
        delete p;
}


const OperatorStatus ObliviousOperator::getStatus() const
{
    return _status;
}

const ObliviousOperator* ObliviousOperator::getChild(size_vdb child) const
{
    return _children[child];
}

const std::vector<ObliviousOperator*>& ObliviousOperator::getChildren() const
{
    return _children;
}

const RecordSchema& ObliviousOperator::getChildSchema(int child) const
{
    return getChild(child)->getSchema();
}

const RecordSchema& ObliviousOperator::getSchema() const
{
    return _outSchema;
}

const TableID ObliviousOperator::getOutputTableID() const
{
    return _operatorId;
};

void ObliviousOperator::setOutputTableID(TableID id)
{
    _operatorId = id;
};

const ObliviousOperator* ObliviousOperator::getParent() const
{
    return _parent;
}

bool ObliviousOperator::isBlocking() const
{
    return _blocking;
}


OperatorStatus ObliviousOperator::getCurrentTuple(db::obdata::ObliviousTuple &out) const
{
    out = _currentTuple;
    return _status;
}

void ObliviousOperator::setParent(ObliviousOperator* p)
{
    _parent = p;
}

void ObliviousOperator::reset()
{
    for(auto c : _children) {
        c->reset();
    }

    _status = OperatorStatus::NotInitialized;
}

void ObliviousOperator::encodeOperator(unsigned char *&writeHead) const
{
    throw(std::domain_error("ObliviousOperator class does not encode."));
}

size_vdb ObliviousOperator::getSerializationOverHead(std::set<TableID> &baseTables) const {
    size_vdb ret = 0;
    for (auto c : _children) {
        ret += c->getSerializationOverHead(baseTables);
    }
    return ret;
};

unsigned char * ObliviousOperator::EncodeQueryToNewArray(ObliviousOperator * root, size_vdb & encodingSize) {
    std::set<TableID> tables;
    size_vdb encodeSize = root->getSerializationOverHead(tables);
    size_vdb overheadSize = sizeof(size_vdb) + sizeof(TableID) * tables.size();
    unsigned char * output = new unsigned char [overheadSize + encodeSize];
    unsigned char * writeHead = output;

    *(size_vdb *)writeHead = (size_vdb) tables.size();
    writeHead += sizeof(size_vdb);
    for (auto t : tables) {
        *(TableID *)writeHead = t;
        writeHead += sizeof(TableID);
    }
    root->encodeOperator(writeHead);
    encodingSize = overheadSize + encodeSize;
    return output;
};