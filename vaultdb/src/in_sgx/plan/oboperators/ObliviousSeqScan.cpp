#include <shared/QueryEncodingDefinitions.h>
#include <set>
#include "in_sgx/plan/oboperators/ObliviousSeqScan.h"

using namespace plan::oboperators;
using namespace db::obdata;

ObliviousSeqScan::ObliviousSeqScan(TableID opId, db::obdata::ObliviousTupleTable * inputTable)
        : ObliviousOperator(opId, inputTable->getSchema(), false, ObliviousOperatorPtrList{}),
          _inputTable(inputTable)
{}

ObliviousSeqScan::~ObliviousSeqScan() {}


OperatorStatus ObliviousSeqScan::next()
{
    if(_scanPos >= _inputTable->size()) {
        _status = OperatorStatus::NoMoreTuples;
    } else {
        _currentTuple = *_inputTable->at(_scanPos++);
        _status = OperatorStatus::Ok;
    }

    return _status;
}

void ObliviousSeqScan::reset()
{
    _scanPos = 0;
    ObliviousOperator::reset();
}

size_vdb ObliviousSeqScan::getSerializationOverHead(std::set<TableID> &baseTables) const {
    baseTables.insert(_inputTable->getTableID());
    return 2 + sizeof(TableID) + sizeof(size_vdb) + sizeof(TableID);
};

void ObliviousSeqScan::encodeOperator(unsigned char *&writeHead) const
{
    // the type of operator it is
    *writeHead = (unsigned char) serialization::OperatorCode::OBLIVIOUS_SCAN;
    writeHead++;

    // the number of child
    *writeHead = (unsigned char) 0;
    writeHead++;

    // any child

    // operatorID
    *(TableID*)writeHead = _operatorId;
    writeHead += sizeof(TableID);

    // extra info: tableID
    *(TableID*)writeHead = _inputTable->getTableID();
    writeHead += sizeof(TableID);
};