#include <cstdarg>
#include <cstdio>
#include <in_sgx/obdata/ObliviousTupleTable.h>
#include "in_sgx/plan/clusterops/ClusterOperator.h"


ClusterOperator::ClusterOperator(MachineID machineID, TransmitterID transmitterID, utilities::InSGXController *dispatcher,
                                 size_vdb numMachines) {
    _localMachineID = machineID;
    _isAllDataCollected = false;
    _localDispatcher = dispatcher;
    _transmitterID = transmitterID;
    _totalMachineCount = numMachines;
}

ClusterOperator::~ClusterOperator()
{};

OperatorStatus ClusterOperator::receiveCallBack(MachineID src_machine_id, TableID src_tid, db::obdata::ObliviousTupleTable *table)
{
    return OperatorStatus::FatalError;
};

/**
 * Some operators (Column sort) opt to send short messages to acquire consensus;
 * This is the function for those kind of messages.
 * The message sent will be exactly the message received, including the TableID header.
 * @param src_machine_id
 * @param src_tid
 * @param data
 * @param msg_size
 * @return
 */
OperatorStatus
ClusterOperator::receiveGenericMessage(MachineID src_machine_id, TableID src_tid, unsigned char *data,
                                       size_vdb msg_size)
{
    return OperatorStatus::FatalError;
};

MachineID ClusterOperator::getLocalMachineID() const
{
    return _localMachineID;
};

bool ClusterOperator::isAllDataCollected() const
{
    return _isAllDataCollected;
};

TransmitterID ClusterOperator::getTransmitterID() const {
    return _transmitterID;
};
