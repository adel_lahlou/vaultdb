#include <in_sgx/plan/kaoperators/ClusterKAnonymousBitonicMergeSort.h>
#include <in_sgx/plan/kaoperators/ClusterKAnonymousSortedAggregation.h>
#include <in_sgx/plan/kaoperators/ClusterKAnonymousColumnSort.h>
#include "in_sgx/serialization/InSGXQueryDecoder.h"
#include "in_sgx/serialization/ObliviousSerializer.h"
#include "in_sgx/utilities/InSGXController.h"
#include "in_sgx/plan/obexpressions/ObliviousExpressionFactory.h"
#include "in_sgx/plan/kaoperators/ClusterKAnonymousBinnedAggregation.h"
#include "in_sgx/plan/kaoperators/KAnonymousLocalGenFilter.h"
#include "in_sgx/plan/kaoperators/ClusterKAnonymousGenJoin.h"

using namespace db::obdata;
using namespace db::obdata::obfield;
using namespace plan::oboperators;
using namespace plan::obexpressions;
using namespace type;

using namespace serialization;

#if defined(LOCAL_DEBUG_MODE) || defined(OUTSIDE_COMPILATION)
#else
#include "Enclave_t.h"
#endif

/*
#ifndef PARSER_DEBUG
#define PARSER_DEBUG
#endif
*/

// Core

ParserStatus InSGXQueryDecoder::parseQuery(unsigned char* instruction)
{
    std::unique_lock<std::mutex> guard(_parseMutex);
    unsigned char* instructionPointer = instruction;

    // Number input tables required
    size_vdb tableCount = *(size_vdb*)instructionPointer;
    instructionPointer += sizeof(size_vdb);

    // check the tables are all there
    for (size_vdb i = 0; i < tableCount; i++) {
        if (!_dispatcher->hasTable(*(TableID*)instructionPointer)) {
#if defined(LOCAL_DEBUG_MODE) || defined(OUTSIDE_COMPILATION)
#ifdef PARSER_DEBUG
            fprintf(stdout, "[INFO] missing table for decoder: 0x%x; parsing delayed\n", *(TableID*)instructionPointer);
#endif
#else
            ocall_print_string((std::string("[INFO] missing table for decoder: ")
                        + std::to_string(*(TableID*)instructionPointer)
                        + std::string("; parsing delayed\n")).c_str());
#endif
            _dispatcher->requestBaseTable(*(TableID*)instructionPointer);
            return ParserStatus :: TableMissing;
        };
        instructionPointer += sizeof(TableID);
    }
    // parse the query

    if (*instructionPointer >= OBLIVIOUS_SCAN) {
        if (_parsedObliviousQueryRoot != nullptr) {
            delete _parsedObliviousQueryRoot;
        }
        _parsedObliviousQueryRoot = parseObliviousOperator(instructionPointer);
        _type = OBLIVIOUS;
    } else if (*instructionPointer >= KANO_SCAN) {
        if (_parsedKAnonymousQueryRoot != nullptr) {
            delete _parsedKAnonymousQueryRoot;
        }
        _parsedKAnonymousQueryRoot = parseKAnonymousOperator(instructionPointer);
        _type = KANONYMOUS;
    } else {
        throw ("Plaintext operators are not yet implemented");
    }
    return ParserStatus :: Ok;
};

ObliviousOperator * InSGXQueryDecoder::acquireObliviousQueryRoot()
{
    if (_type == OperatorType::OBLIVIOUS) {
        return _parsedObliviousQueryRoot;
    } else {
        return nullptr;
    }
};

KAnonymousOperator * InSGXQueryDecoder::acquireKAnonymousQueryRoot()
{
    if ( _type == OperatorType::KANONYMOUS) {
        return _parsedKAnonymousQueryRoot;
    } else {
        return nullptr;
    }
};

OperatorType InSGXQueryDecoder::getQueryType()
{
    return _type;
}

// Logistics

InSGXQueryDecoder::InSGXQueryDecoder(utilities::InSGXController *dispatcher)
        : _parsedObliviousQueryRoot(nullptr),
          _parsedKAnonymousQueryRoot(nullptr),
          _type(UNKNOWN),
          _machineID(dispatcher->_localMachineID),
          _dispatcher(dispatcher)
{};

InSGXQueryDecoder::~InSGXQueryDecoder()
{
    if (_type == OperatorType::OBLIVIOUS && _parsedObliviousQueryRoot != nullptr)
        delete _parsedObliviousQueryRoot;
    else if (_type == OperatorType::KANONYMOUS && _parsedKAnonymousQueryRoot != nullptr)
        delete _parsedKAnonymousQueryRoot;
    else if (_type == OperatorType::PLAINTEXT)
        throw("Exception in destructor deleteing Plaintext operator pointer.");
};

#define TYPE_MASK 0x3f

// Expressions

ObliviousExpression InSGXQueryDecoder::parseExpression(unsigned char* &instruction)
{
    switch ((*instruction) & 0xc0) {
        case 0x00:
            return parseColumn(instruction);
        case 0x40:
            return parseLiteral(instruction);
        case 0x80:
            return parseAlgebra(instruction);
        case 0xc0: {
            return parseAggregate(instruction);
        }
        default:
            throw("Impossible scenario; there can be only four cases with parseExpressions");
    }
};

ObliviousExpression InSGXQueryDecoder::parseColumn(unsigned char *&instruction)
{
    // column and type marker; currently not used
//    FieldDataType type = (FieldDataType)((*instruction) & TYPE_MASK);
    instruction ++;

    // ordinal
    size_vdb ordinal = (size_vdb)(*instruction);
    instruction++;

    // type
    type::FieldDataType type = (type::FieldDataType) *instruction;
    instruction++;

    // size
    size_vdb size = *(size_vdb*) instruction;
    instruction += sizeof(size_vdb);

    // wrap up
    return ObliviousExpressionFactory::makeColumn(ordinal, type::SchemaColumn{type, size});
};

ObliviousExpression InSGXQueryDecoder::parseLiteral(unsigned char* &instruction)
{
    // literal marker
    FieldDataType type = (FieldDataType)((*instruction) & TYPE_MASK);
    instruction ++;

    // Field
    switch (type) {
        case FieldDataType::Int: {
            unsigned char* oldInstruction = instruction;
            instruction += sizeof(int);
            return ObliviousExpressionFactory::makeLiteral(ObliviousField::makeObliviousIntField(*(int*)oldInstruction), sizeof(int));
        }
        case FieldDataType::Fixchar: {
            size_vdb capacity = *(size_vdb*)instruction;
            instruction += sizeof(size_vdb);
            unsigned char* oldInstruction = instruction;
            instruction += capacity;
            instruction ++;
            return ObliviousExpressionFactory::makeLiteral(
                    ObliviousField::makeObliviousFixcharField((char *) oldInstruction, capacity), capacity);
        }
        case FieldDataType::TimestampNoZone: {
            unsigned char* oldInstruction = instruction;
            instruction += sizeof(time_t);
            return ObliviousExpressionFactory::makeLiteral(ObliviousField::makeObliviousTimestampNoZoneField(*(time_t*)oldInstruction), sizeof(time_t));
        }
        default:
            throw("Unsupported literal type: "+ std::to_string((int)type));
    }
};

ObliviousExpression InSGXQueryDecoder::parseAlgebra(unsigned char* &instruction)
{
    // Algebra type
    unsigned char type = (*instruction);
    instruction++;

    // parsing.. Unary
    switch (type) {
        case NOT: {
            return ObliviousExpressionFactory::makeNot(parseExpression(instruction));
        }
        default:;
    }

    ObliviousExpression left = parseExpression(instruction);
    ObliviousExpression right = parseExpression(instruction);

    // parsing... binary
    ObliviousExpression * p;
    switch (type) {
        // arithmetic
        case PLUS:
            return ObliviousExpressionFactory::makeAdd(left, right);
        case MINUS:
            return ObliviousExpressionFactory::makeSub(left, right);
        case TIMES:
            return ObliviousExpressionFactory::makeMul(left, right);
        case DIVIDE:
            return ObliviousExpressionFactory::makeDiv(left, right);
        // comparison
        case EQUALS:
            return ObliviousExpressionFactory::makeEq(left, right);
        case NOT_EQUALS:
            return ObliviousExpressionFactory::makeNEq(left, right);
        case GREATER_THAN:
            return ObliviousExpressionFactory::makeGT(left, right);
        case LESS_THAN:
            return ObliviousExpressionFactory::makeLT(left, right);
        case GREATER_THAN_OR_EQUAL:
            return ObliviousExpressionFactory::makeGTE(left, right);
        case LESS_THAN_OR_EQUAL:
            return ObliviousExpressionFactory::makeLTE(left, right);
        // logical
        case AND:
            return ObliviousExpressionFactory::makeAnd(left, right);
        case OR:
            return ObliviousExpressionFactory::makeOr(left, right);
        default:
            throw("Unsupported Algebraic Expression: "+std::to_string(type));
    }
};

ObliviousExpression InSGXQueryDecoder::parseAggregate(unsigned char* &instruction)
{
    // agg type
    unsigned char type = (*instruction);
    instruction++;

    // parsing
    AggregateFunction * p;
    switch (type) {
        case COUNT: {
            pos_vdb position = (pos_vdb)*instruction;
            instruction++;

            type::FieldDataType type = (type::FieldDataType)*instruction;
            instruction++;

            size_vdb size = *(size_vdb*) instruction;
            instruction+= sizeof(size_vdb);

            return ObliviousExpressionFactory::makeCount(position);
        }
        case SUM: {
            pos_vdb position = (pos_vdb)*instruction;
            instruction++;

            type::FieldDataType type = (type::FieldDataType)*instruction;
            instruction++;

            size_vdb size = *(size_vdb*) instruction;
            instruction+= sizeof(size_vdb);
            return ObliviousExpressionFactory::makeSum(position, type::SchemaColumn{type, size});
        }
        case ROW_NUMBER: {
            return ObliviousExpressionFactory::makeRowNumber();
        }
        default: {
            throw("Unimplemented aggregate type: "+ std::to_string(type));
        }
    }
};

// Oblivious Operator

ObliviousOperator * InSGXQueryDecoder::parseObliviousOperator(unsigned char* &instruction)
{
    OperatorCode operatorIdentifier = (OperatorCode)*instruction;
    instruction++;

    // type of operator parsed processed
    switch (operatorIdentifier) {
        case OBLIVIOUS_SCAN:
            return parseObliviousSeqScan(instruction);
        case OBLIVIOUS_FILTER:
            return parseObliviousFilter(instruction);
        case OBLIVIOUS_AGGREGATE:
            return parseObliviousSortedGroup(instruction);
        case OBLIVIOUS_PROJECT:
            return parseObliviousProject(instruction);
        case OBLIVIOUS_SOURCE_FROM_KANO:
            return parseObliviousKAnonymous(instruction);
        case OBLIVIOUS_SORT:
            return parseObliviousSort(instruction);
        default: {
            char buffer[40];
            throw "Unimplemented Operator\n";
        }
    }
};

void InSGXQueryDecoder::parseObliviousOperatorBasicInfo(
        unsigned char* &instruction,
        ObliviousOperatorPtrList &children,
        TableID & tableID
)
{
    // operator identifier; already skipped

    // num child
    unsigned char numChild = *instruction;
    instruction++;

    // parse child
    for (unsigned char i = 0; i < numChild; i++) {
        children.push_back(parseObliviousOperator(instruction));
    }

    // tableID
    tableID = *(TableID*)instruction;
    instruction += sizeof(TableID);
}

ObliviousSeqScan * InSGXQueryDecoder::parseObliviousSeqScan(unsigned char *&instruction)
{
    TableID id;
    ObliviousOperatorPtrList children;
    parseObliviousOperatorBasicInfo(instruction, children, id);

    TableID tableID = *(TableID*) instruction;
    instruction += sizeof(TableID);

    ObliviousTupleTable * table = _dispatcher->fetchLocalSinglePartitionTable(tableID);
    return new ObliviousSeqScan(id, table);
};

ObliviousFilter * InSGXQueryDecoder::parseObliviousFilter(unsigned char *&instruction)
{
    TableID opId;
    ObliviousOperatorPtrList children;
    parseObliviousOperatorBasicInfo(instruction, children, opId);
    ObliviousOperator * child = children[0];

    return new ObliviousFilter(opId, child, parseExpression(instruction));
};

ObliviousSortedGroup * InSGXQueryDecoder::parseObliviousSortedGroup(unsigned char *&instruction)
{
    TableID opId;
    ObliviousOperatorPtrList children;
    parseObliviousOperatorBasicInfo(instruction, children, opId);
    ObliviousOperator * child = children[0];

    // num groupbys
    unsigned char numGroupBys = *instruction;
    instruction++;

    // group bys
    std::vector<pos_vdb> groupBys;
    for (unsigned char i = 0; i < numGroupBys; i++) {
        groupBys.push_back((pos_vdb)*instruction);
        instruction++;
    }

    // num of outputs
    unsigned char numOutputs = *instruction;
    instruction++;

    // outputs
    ObliviousExpressionList exps;
    for (unsigned char i = 0; i < numOutputs; i++) {
        exps.push_back(parseExpression(instruction));
    }

    // wrap up
    return new ObliviousSortedGroup(opId, child, exps, groupBys);
};

ObliviousKAnonymous * InSGXQueryDecoder::parseObliviousKAnonymous(unsigned char *&instruction)
{
    // num child, not given; known to be one

    // parse child and return
    return new ObliviousKAnonymous(parseKAnonymousOperator(instruction));
};

BitonicSort * InSGXQueryDecoder::parseObliviousSort(unsigned char *&instruction)
{
    TableID id;
    ObliviousOperatorPtrList children = {};
    parseObliviousOperatorBasicInfo(instruction, children, id);
    ObliviousOperator * child = children[0];

    // extra info: indices, orders

    pos_vdb size = (pos_vdb)*instruction;
    instruction++;
    std::vector<pos_vdb> orderByEntries;
    for (pos_vdb i = 0; i < size; i++) {
        orderByEntries.push_back((size_vdb)*instruction);
        instruction++;
    }
    std::vector<SortOrder> sortOrders;
    for (pos_vdb i = 0; i < size; i++) {
        sortOrders.push_back((SortOrder)*instruction);
        instruction++;
    }

    return new BitonicSort(id,children[0],orderByEntries,sortOrders);
};

ObliviousProject * InSGXQueryDecoder::parseObliviousProject(unsigned char *&instruction)
{
    TableID opId;
    ObliviousOperatorPtrList children;
    parseObliviousOperatorBasicInfo(instruction, children, opId);
    ObliviousOperator * child = children[0];

    // number of output items
    unsigned char numOutputs = *instruction;
    instruction ++;

    // the types
    ObliviousExpressionList exps;
    for (unsigned char i = 0; i < numOutputs; i++) {
        exps.push_back(parseExpression(instruction));
    }

    // wrap up
    return new ObliviousProject(opId, child, exps);
};

KAnonymousOperator * InSGXQueryDecoder::parseKAnonymousOperator(unsigned char* &instruction)
{
    OperatorCode operatorIdentifier = (OperatorCode)*instruction;
    instruction++;

    // type of operator parsed processed
    switch (operatorIdentifier) {
        case KANO_SCAN:
            return parseKAnonymousSeqScan(instruction);
        case KANO_FILTER:
            return parseKAnonymousFilter(instruction);
        case KANO_SORT:
            return parseKAnonymousBitonicSort(instruction);
        case KANO_PROJECT:
            return parseKAnonymousProject(instruction);
        case KANO_AGGREGATE:
            return parseKAnonymousSortedGroup(instruction);
        case KANO_GEN_FILTER:
            return parseKAnonymousLocalGenFilter(instruction);
        case KANO_UNION:
            return parseKAnonymousUnion(instruction);
        case KANO_CLUSTER_LOCAL_DISTRO_AGGREGATION:
            return parseKAnonymousLocalDistroAgg(instruction);
        case KANO_CLUSTER_POOLED_JOIN:
            return parseKAnonymousHashPooledJoin(instruction);
        case KANO_CLUSTER_BITONIC_MERGE_SORT:
            return parseKAnonymousBitonicMergeSort(instruction);
        case KANO_CLUSTER_REPARTITION:
            return parseKAnonymousRepartition(instruction);
        case KANO_CLUSTER_SORTED_AGGREGATION:
            return parseClusterKAnonymousSortedAggregation(instruction);
        case KANO_CLUSTER_BINNED_AGGREGATION:
            return parseClusterKAnonymousBinnedAggregation(instruction);
        case KANO_CLUSTER_GEN_HASH_JOIN:
            return parseClusterKAnonymousGenJoin(instruction);
        case KANO_CLUSTER_LIMIT:
            return parseClusterKAnonymousLimit(instruction);
        case KANO_CLUSTER_COLUMN_SORT:
            return parseKAnonymousColumnSort(instruction);
        case KANO_CLUSTER_SORTED_WINDOW:
            return parseClusterKAnonymousWindow(instruction);
        default: {
            char buffer[40];
            throw "Unimplemented Operator\n";
        }
    }
};

void InSGXQueryDecoder::parseKAnonymousOperatorBasicInfo(
        unsigned char* &instruction,
        KAnonymousOperatorPtrList &children,
        TableID & tableID,
        size_vdb & k)
{
    // operator identifier; already skipped

    // num child
    unsigned char numChild = *instruction;
    instruction++;

    // parse child
    for (unsigned char i = 0; i < numChild; i++) {
        children.push_back(parseKAnonymousOperator(instruction));
    }

    // tableID
    tableID = *(TableID*)instruction;
    instruction += sizeof(TableID);

    // k
    k = *(size_vdb*)instruction;
    instruction += sizeof(size_vdb);
};

KAnonymousSeqScan * InSGXQueryDecoder::parseKAnonymousSeqScan(unsigned char *&instruction)
{
    TableID id;
    size_vdb k;
    KAnonymousOperatorPtrList children;
    parseKAnonymousOperatorBasicInfo(instruction, children, id, k);

    TableID tableID = *(TableID *) instruction;
    instruction += sizeof(TableID);

    ObliviousTupleTable * table = _dispatcher->fetchLocalSinglePartitionTable(tableID);
    return new KAnonymousSeqScan(id, k, table);
};

KAnonymousFilter * InSGXQueryDecoder::parseKAnonymousFilter(unsigned char *&instruction)
{
    TableID id;
    size_vdb k;
    KAnonymousOperatorPtrList children;
    parseKAnonymousOperatorBasicInfo(instruction, children, id, k);

    return new KAnonymousFilter(
            id, k, children[0], parseExpression(instruction));
};

KAnonymousBitonicSort * InSGXQueryDecoder::parseKAnonymousBitonicSort(unsigned char *&instruction)
{
    TableID id;
    size_vdb k;
    KAnonymousOperatorPtrList children;
    parseKAnonymousOperatorBasicInfo(instruction, children, id, k);

    // extra info: # order by entries, order by entries, sor orders
    pos_vdb size = (pos_vdb) *instruction;
    instruction++;

    std::vector<pos_vdb> orderByEntries;
    for (pos_vdb i = 0; i < size; i++) {
        orderByEntries.push_back((pos_vdb)*instruction);
        instruction++;
    }
    std::vector<SortOrder> sortOrders;
    for (pos_vdb i = 0; i < size; i++) {
        sortOrders.push_back( *instruction == 0 ? SortOrder::ASCEND : SortOrder::DESCEND);
        instruction++;
    }

    return new KAnonymousBitonicSort(id, k, children[0], orderByEntries, sortOrders, _dispatcher);
};

KAnonymousProject * InSGXQueryDecoder::parseKAnonymousProject(unsigned char *&instruction)
{
    TableID id;
    size_vdb k;
    KAnonymousOperatorPtrList children;
    parseKAnonymousOperatorBasicInfo(instruction, children, id, k);

    pos_vdb size = (pos_vdb)*instruction;
    instruction++;
    ObliviousExpressionList exps;
    for (pos_vdb i = 0; i < size; i++) {
        exps.push_back(parseExpression(instruction));
    }

    return new KAnonymousProject(id,k,children[0],exps);
};

KAnonymousSortedGroup * InSGXQueryDecoder::parseKAnonymousSortedGroup(unsigned char *&instruction)
{
    TableID id;
    size_vdb k;
    KAnonymousOperatorPtrList children;
    parseKAnonymousOperatorBasicInfo(instruction, children, id, k);

    // num groupbys
    unsigned char numGroupBys = *instruction;
    instruction++;

    // group bys
    std::vector<pos_vdb> groupBys;
    for (unsigned char i = 0; i < numGroupBys; i++) {
        groupBys.push_back((pos_vdb)*instruction);
        instruction++;
    }

    // num of outputs
    unsigned char numOutputs = *instruction;
    instruction++;

    // outputs
    ObliviousExpressionList exps;
    for (unsigned char i = 0; i < numOutputs; i++) {
        exps.push_back(parseExpression(instruction));
    }

    // wrap up
    return new KAnonymousSortedGroup(id, k, children[0], exps, groupBys, _dispatcher);
};

KAnonymousLocalGenFilter * InSGXQueryDecoder::parseKAnonymousLocalGenFilter(unsigned char *&instruction)
{
    TableID id;
    size_vdb k;
    KAnonymousOperatorPtrList children;
    parseKAnonymousOperatorBasicInfo(instruction, children, id, k);

    auto view = parseExpression(instruction);
    auto actual = parseExpression(instruction);

    std::vector<pos_vdb> entities;
    auto entitySize = (size_vdb) *instruction;
    instruction++;
    for (pos_vdb i = 0; i < entitySize; i++) {
        entities.push_back((pos_vdb)*instruction);
        instruction++;
    }

    std::vector<pos_vdb> imposedGenLevels;
    auto genSize = (size_vdb) *instruction;
    instruction++;
    for (pos_vdb i = 0; i < genSize; i++) {
        imposedGenLevels.push_back((size_vdb)*instruction);
        instruction++;
    }

    return new KAnonymousLocalGenFilter(id, k, children[0], view, actual, entities, imposedGenLevels, _dispatcher);
};

KAnonymousUnion * InSGXQueryDecoder::parseKAnonymousUnion(unsigned char *&instruction) {
    TableID id;
    size_vdb k;
    KAnonymousOperatorPtrList children;
    parseKAnonymousOperatorBasicInfo(instruction, children, id, k);

    return new KAnonymousUnion(id, k, children);
};

ClusterKAnonymousLDAggregation * InSGXQueryDecoder::parseKAnonymousLocalDistroAgg(unsigned char *&instruction)
{
    TableID id;
    size_vdb k;
    KAnonymousOperatorPtrList children;
    parseKAnonymousOperatorBasicInfo(instruction, children, id, k);

    // TransmitterID
    TransmitterID transmitterID = *(TransmitterID*) instruction;
    instruction += sizeof(TransmitterID);

    // num groupbys
    unsigned char numGroupBys = *instruction;
    instruction++;

    // group bys
    std::vector<pos_vdb> groupBys = {};
    for (unsigned char i = 0; i < numGroupBys; i++) {
        groupBys.push_back((pos_vdb)*instruction);
        instruction++;
    }

    // num of outputs
    unsigned char numOutputs = *instruction;
    instruction++;

    // outputs
    std::vector<ObliviousExpression> exps = {};
    for (unsigned char i = 0; i < numOutputs; i++) {
        exps.push_back(parseExpression(instruction));
    }

    // wrap up
    return new ClusterKAnonymousLDAggregation(_machineID, id, transmitterID, k, children[0], exps, groupBys,
                                              _dispatcher);
};

ClusterKAnonymousPooledJoin * InSGXQueryDecoder::parseKAnonymousHashPooledJoin(unsigned char *&instruction)
{
    TableID id;
    size_vdb k;
    KAnonymousOperatorPtrList children;
    parseKAnonymousOperatorBasicInfo(instruction, children, id, k);

    // join type
    instruction++;

    // TransmitterIDs
    TransmitterID leftTransmitterID = *(TransmitterID*) instruction;
    instruction += sizeof(TransmitterID);
    TransmitterID rightTransmitterID = *(TransmitterID*) instruction;
    instruction += sizeof(TransmitterID);

    // number of join
    pos_vdb joinSize = (pos_vdb)*(instruction);
    instruction++;

    // left attribute
    std::vector<pos_vdb> leftAttribute = {};
    for (unsigned char i = 0; i < joinSize; i++) {
        leftAttribute.push_back((pos_vdb)*instruction);
        instruction++;
    }
    // right attributes
    std::vector<pos_vdb> rightAttribute = {};
    for (unsigned char i = 0; i < joinSize; i++) {
        rightAttribute.push_back((pos_vdb)*instruction);
        instruction++;
    }

    // number of left protected
    unsigned char numOfProtected = *instruction;
    instruction++;

    // leftProtected
    std::vector<pos_vdb> leftProtected = {};
    for (unsigned char i = 0; i < numOfProtected; i++) {
        leftProtected.push_back((pos_vdb)*instruction);
        instruction++;
    }

    // number of right protected;
    numOfProtected = *instruction;
    instruction++;

    // leftProtected
    std::vector<pos_vdb> rightProtected = {};
    for (unsigned char i = 0; i < numOfProtected; i++) {
        rightProtected.push_back((pos_vdb)*instruction);
        instruction++;
    }

    // wrap up
    return new ClusterKAnonymousPooledJoin(_machineID, id, leftTransmitterID, rightTransmitterID, k, children[0], children[1], leftAttribute,
                                               rightAttribute,
                                               leftProtected, rightProtected, _dispatcher);
};

ClusterKAnonymousBitonicMergeSort * InSGXQueryDecoder::parseKAnonymousBitonicMergeSort(unsigned char *& instruction) {

    TableID id;
    size_vdb k;
    KAnonymousOperatorPtrList children;
    parseKAnonymousOperatorBasicInfo(instruction, children, id, k);

    // extra info: transmitterID, designated collector, indices, orders
    TransmitterID transmitterID = *(TransmitterID *)instruction;
    instruction += sizeof(TransmitterID);

    MachineID desig = *(MachineID *)instruction;
    instruction += sizeof(MachineID);

    pos_vdb size = (pos_vdb)*instruction;
    instruction++;
    std::vector<pos_vdb> orderByEntries;
    for (pos_vdb i = 0; i < size; i++) {
        orderByEntries.push_back((size_vdb)*instruction);
        instruction++;
    }
    std::vector<SortOrder> sortOrders;
    for (pos_vdb i = 0; i < size; i++) {
        sortOrders.push_back((SortOrder)*instruction);
        instruction++;
    }

    // wrap up
    return new ClusterKAnonymousBitonicMergeSort(_machineID, id, transmitterID, k, children[0], orderByEntries, sortOrders,
                                                 _dispatcher, desig);
};

ClusterKAnonymousRepartition * InSGXQueryDecoder::parseKAnonymousRepartition(unsigned char *& instruction)
{
    TableID id;
    size_vdb k;
    KAnonymousOperatorPtrList children;
    parseKAnonymousOperatorBasicInfo(instruction, children, id, k);

    // extra info: transmitterID, control flow, entities
    TransmitterID tid = (TransmitterID)*instruction;
    instruction += sizeof(TransmitterID);

    pos_vdb controlSize = (pos_vdb)*instruction;
    instruction++;
    std::vector<pos_vdb> controlFlow;
    for (pos_vdb i = 0; i < controlSize; i++) {
        controlFlow.push_back((pos_vdb)*instruction);
        instruction++;
    }
    pos_vdb entitySize = (pos_vdb)*instruction;
    instruction++;
    std::vector<pos_vdb> entities;
    for (pos_vdb i = 0; i < entitySize; i++) {
        entities.push_back((pos_vdb)*instruction);
        instruction++;
    }

    std::vector<pos_vdb> imposedGenLevels;
    auto genSize = (size_vdb) *instruction;
    instruction++;
    for (pos_vdb i = 0; i < genSize; i++) {
        imposedGenLevels.push_back((size_vdb)*instruction);
        instruction++;
    }

    // the two flags
    unsigned char flags = *instruction;
    instruction++;

    // wrap up
    return new ClusterKAnonymousRepartition(_machineID, id, tid, k, children[0], controlFlow, entities,
                                            imposedGenLevels, (bool) (flags & 0x01), _dispatcher);
};

ClusterKAnonymousSortedAggregation * InSGXQueryDecoder::parseClusterKAnonymousSortedAggregation(
        unsigned char *&instruction)
{
    TableID id;
    size_vdb k;
    KAnonymousOperatorPtrList children;
    parseKAnonymousOperatorBasicInfo(instruction, children, id, k);

    // num groupbys
    unsigned char numGroupBys = *instruction;
    instruction++;

    // group bys
    std::vector<pos_vdb> groupBys;
    for (unsigned char i = 0; i < numGroupBys; i++) {
        groupBys.push_back((pos_vdb)*instruction);
        instruction++;
    }

    // num of outputs
    unsigned char numOutputs = *instruction;
    instruction++;

    // outputs
    ObliviousExpressionList exps;
    for (unsigned char i = 0; i < numOutputs; i++) {
        exps.push_back(parseExpression(instruction));
    }

    // transmitter ID as well as the _isRepartitionSorting
    TransmitterID tid = *(TransmitterID*)instruction;
    instruction += sizeof(TransmitterID);
    bool isRepartionSorting = *instruction;
    instruction++;

    // wrap up
    return new ClusterKAnonymousSortedAggregation(_machineID, id, tid, k, children[0], exps, groupBys, isRepartionSorting, _dispatcher);
};

ClusterKAnonymousWindow * InSGXQueryDecoder::parseClusterKAnonymousWindow(unsigned char *&instruction) {
    TableID id;
    size_vdb k;
    KAnonymousOperatorPtrList children;
    parseKAnonymousOperatorBasicInfo(instruction, children, id, k);

    // num partition bys
    unsigned char numPartitionBys = *instruction;
    instruction++;

    // group bys
    std::vector<pos_vdb> partitionBys;
    for (unsigned char i = 0; i < numPartitionBys; i++) {
        partitionBys.push_back((pos_vdb)*instruction);
        instruction++;
    }

    // num order bys
    unsigned char numOrderBys = *instruction;
    instruction++;

    // order bys
    std::vector<pos_vdb> orderBys;
    std::vector<SortOrder> sortOrders;
    for (unsigned char i = 0; i < numOrderBys; i++) {
        orderBys.push_back((pos_vdb)*instruction);
        instruction++;
    }
    for (unsigned char i = 0; i < numOrderBys; i++) {
        sortOrders.push_back((SortOrder)*instruction);
        instruction++;
    }

    // num of outputs
    unsigned char numOutputs = *instruction;
    instruction++;

    // outputs
    ObliviousExpressionList exps;
    for (unsigned char i = 0; i < numOutputs; i++) {
        exps.push_back(parseExpression(instruction));
    }

    // transmitter ID as well as the _isRepartitionSorting
    TransmitterID tid = *(TransmitterID*)instruction;
    instruction += sizeof(TransmitterID);
    bool isRepartionSorting = *instruction;
    instruction++;

    // wrap up
    return new ClusterKAnonymousWindow(_machineID, id, tid, k, children[0], exps, partitionBys, orderBys, sortOrders, isRepartionSorting, _dispatcher);
};

ClusterKAnonymousBinnedAggregation * InSGXQueryDecoder::parseClusterKAnonymousBinnedAggregation(unsigned char *&instruction)
{
    TableID id;
    size_vdb k;
    KAnonymousOperatorPtrList children;
    parseKAnonymousOperatorBasicInfo(instruction, children, id, k);

    // num groupbys
    unsigned char numGroupBys = *instruction;
    instruction++;

    // group bys
    std::vector<pos_vdb> groupBys;
    for (unsigned char i = 0; i < numGroupBys; i++) {
        groupBys.push_back((pos_vdb)*instruction);
        instruction++;
    }

    // num of outputs
    unsigned char numOutputs = *instruction;
    instruction++;

    // outputs
    ObliviousExpressionList exps;
    for (unsigned char i = 0; i < numOutputs; i++) {
        exps.push_back(parseExpression(instruction));
    }

    TransmitterID tid = *(TransmitterID*)instruction;
    instruction += sizeof(TransmitterID);

    unsigned char flags = *instruction;
    instruction++;

    // wrap up
    return new ClusterKAnonymousBinnedAggregation(_machineID, id, tid, k, children[0], exps, groupBys,
                                                  (bool)(flags & 0x01), (bool)(flags & 0x02), _dispatcher);
};

ClusterKAnonymousGenJoin * InSGXQueryDecoder::parseClusterKAnonymousGenJoin(unsigned char *&instruction)
{
    TableID id;
    size_vdb k;
    KAnonymousOperatorPtrList children;
    parseKAnonymousOperatorBasicInfo(instruction, children, id, k);

    // extras
    auto type = (serialization::JoinType)*instruction;
    instruction++;

    TransmitterID leftTID = *(TransmitterID*)instruction;
    instruction += sizeof(TransmitterID);

    TransmitterID rightTID = *(TransmitterID*)instruction;
    instruction += sizeof(TransmitterID);

    unsigned char attrSize = *instruction;
    instruction++;
    std::vector<pos_vdb> leftAttr;
    for (unsigned char i = 0; i < attrSize; i ++) {
        leftAttr.push_back((pos_vdb)*instruction);
        instruction++;
    }

    std::vector<pos_vdb> rightAttr;
    for (unsigned char i = 0; i < attrSize; i ++) {
        rightAttr.push_back((pos_vdb)*instruction);
        instruction++;
    }

    unsigned char leftEntitySize = *instruction;
    instruction ++;
    std::vector<pos_vdb> leftEntity;
    for (unsigned char i = 0; i < leftEntitySize; i ++) {
        leftEntity.push_back((pos_vdb)*instruction);
        instruction++;
    }

    unsigned char rightEntitySize = *instruction;
    instruction ++;
    std::vector<pos_vdb> rightEntity;
    for (unsigned char i = 0; i < rightEntitySize; i ++) {
        rightEntity.push_back((pos_vdb)*instruction);
        instruction++;
    }

    // sort before repart
    bool isSortingBeforeRepart = (bool) *instruction;
    instruction++;

    // imposed genLevels;
    std::vector<pos_vdb> leftImposedGenLevels;
    auto leftGenSize = (size_vdb) *instruction;
    instruction++;
    for (pos_vdb i = 0; i < leftGenSize; i++) {
        leftImposedGenLevels.push_back((size_vdb)*instruction);
        instruction++;
    }

    std::vector<pos_vdb> rightImposedGenLevels;
    auto rightGenSize = (size_vdb) *instruction;
    instruction++;
    for (pos_vdb i = 0; i < rightGenSize; i++) {
        rightImposedGenLevels.push_back((size_vdb)*instruction);
        instruction++;
    }

    return new ClusterKAnonymousGenJoin(_machineID, id, leftTID, rightTID, k, children[0], children[1],
                                        leftAttr, rightAttr, leftEntity, rightEntity, leftImposedGenLevels, rightImposedGenLevels, isSortingBeforeRepart, _dispatcher);
};

ClusterKAnonymousLimit * InSGXQueryDecoder::parseClusterKAnonymousLimit(unsigned char *& instruction) {

    TableID id;
    size_vdb k;
    KAnonymousOperatorPtrList children;
    parseKAnonymousOperatorBasicInfo(instruction, children, id, k);

    // extra info: tupleLimit, transmitterID, designated collector, indices, orders
    size_vdb tupleLimit = *(size_vdb *)instruction;
    instruction += sizeof(size_vdb);

    TransmitterID transmitterID = *(TransmitterID *)instruction;
    instruction += sizeof(TransmitterID);

    MachineID desig = *(MachineID *)instruction;
    instruction += sizeof(MachineID);

    pos_vdb size = (pos_vdb)*instruction;
    instruction++;
    std::vector<pos_vdb> orderByEntries;
    for (pos_vdb i = 0; i < size; i++) {
        orderByEntries.push_back((size_vdb)*instruction);
        instruction++;
    }
    std::vector<SortOrder> sortOrders;
    for (pos_vdb i = 0; i < size; i++) {
        sortOrders.push_back((SortOrder)*instruction);
        instruction++;
    }

    // wrap up
    return new ClusterKAnonymousLimit(_machineID, id, transmitterID, k, children[0], tupleLimit, orderByEntries, sortOrders,
                                      desig, _dispatcher);
};

ClusterKAnonymousColumnSort * InSGXQueryDecoder::parseKAnonymousColumnSort(unsigned char *& instruction) {

    TableID id;
    size_vdb k;
    KAnonymousOperatorPtrList children;
    parseKAnonymousOperatorBasicInfo(instruction, children, id, k);

    // extra info: transmitterID, block size, indices, orders
    TransmitterID transmitterID = *(TransmitterID *)instruction;
    instruction += sizeof(TransmitterID);

    size_vdb blockSize = *(size_vdb *)instruction;
    instruction += sizeof(size_vdb);

    pos_vdb size = (pos_vdb)*instruction;
    instruction++;
    std::vector<pos_vdb> orderByEntries;
    for (pos_vdb i = 0; i < size; i++) {
        orderByEntries.push_back((size_vdb)*instruction);
        instruction++;
    }
    std::vector<SortOrder> sortOrders;
    for (pos_vdb i = 0; i < size; i++) {
        sortOrders.push_back((SortOrder)*instruction);
        instruction++;
    }

    // wrap up
    return new ClusterKAnonymousColumnSort(_machineID, id, transmitterID, k, children[0], orderByEntries, sortOrders,
                                           blockSize, _dispatcher);
};