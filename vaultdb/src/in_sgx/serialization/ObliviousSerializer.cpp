#include <in_sgx/obdata/obfield/ObliviousTimestampNoZoneField.h>
#include "in_sgx/serialization/ObliviousSerializer.h"
#include "in_sgx/obdata/ObliviousTupleTable.h"
#include "in_sgx/obdata/obfield/ObliviousIntField.h"
#include "in_sgx/obdata/obfield/ObliviousFixcharField.h"

using namespace db::obdata;
using namespace db::obdata::obfield;
using namespace type;

ObliviousSerializer::ObliviousSerializer()
{}

unsigned char* ObliviousSerializer::serializeObliviousTupleTable(const ObliviousTupleTable &tt, size_vdb & serializedSize)
{
    size_vdb tt_size = tt.serializationSize();
    unsigned char * tt_data = new unsigned char[tt_size];
    tt.serializeTo(tt_data);
    serializedSize = tt_size;
    return tt_data;
}

ObliviousTupleTable ObliviousSerializer::deserializeObliviousTupleTable(const unsigned char * data, size_vdb size)
{
    size_vdb offset = 0;

    // parse schema
    RecordSchema s = RecordSchema::deserializeFrom(data);
    offset += s.serializationSize();

    // parse table id from tuple side
    offset += sizeof(size_vdb);

    // partitionID
    pos_vdb partitionID = *(pos_vdb*) (data + offset);
    offset += sizeof(pos_vdb);

    // number of partitions in total
    size_vdb numPartitions = *(size_vdb*) (data + offset);
    offset += sizeof(size_vdb);

    // parse number of tuples
    size_vdb numTuples = *(size_vdb*) (data + offset);
    offset += sizeof(size_vdb);

    // parse each tuple
    ObliviousTupleList tuples;
    for(int i = 0; i < numTuples; ++i) {
        ObliviousTuple out = deserializeObliviousTuple(s, (data + offset));
        offset += s.tupleSerializationSize();

        tuples.push_back(out);
    }

    return ObliviousTupleTable(s, partitionID, numPartitions, tuples);
}


ObliviousTuple ObliviousSerializer::deserializeObliviousTuple(const RecordSchema& s, const unsigned char * data)
{
    ObliviousFieldHolder fields;
    size_vdb numFields = s.size();
    size_vdb offset = 0;

    bool dummyFlag = (*data & 0x01) == 1;
    bool defaultFlag = (*data & 0x02) == 0x02;
    offset++;

    for(int f = 0; f < numFields; ++f) {
        size_vdb size = s[f].size;
        switch (s[f].type) {
            case FieldDataType::Int:
                fields.push_back(ObliviousIntField::deserializeFrom(data + offset));
                break;
            case FieldDataType::Fixchar:
                fields.push_back(ObliviousFixcharField::deserializeFrom((data + offset), size));
                size += 1;
                break;
            case FieldDataType::TimestampNoZone:
                fields.push_back(ObliviousTimestampNoZoneField::deserializeFrom(data + offset));
                break;
            default:
                throw;
        };
        offset += size;
    }

    return ObliviousTuple(dummyFlag, defaultFlag, fields);
};