#include <in_sgx/utilities/SecureMove.h>
#include <cstring>
#include <in_sgx/serialization/ObliviousSerializer.h>
#include "in_sgx/utilities/SortHelper.h"

using namespace db::obdata;

void SortHelper::ObliviousTupleSwap(int predicate, type::RecordSchema &schema, ObliviousTuple &left, ObliviousTuple &right) {
    ObliviousTuple temp = left;
    size_vdb size = schema.tupleSerializationSize();
    size_vdb capacity = size;
    if (capacity % 4 != 0) capacity = (capacity / sizeof(int) + 1) * sizeof(int);
    size_vdb intSteps = capacity / sizeof(int);

    unsigned char leftSerial[capacity];
    memset(leftSerial, 0, capacity);
    temp.serializeTo(leftSerial, schema);
    temp = right;
    unsigned char rightSerial[capacity];
    memset(rightSerial, 0, capacity);
    temp.serializeTo(rightSerial, schema);

    for (int i = 0; i < intSteps; i++) {
        int tempLeftInt = *(int *) (leftSerial + i * sizeof(int));
        int rightInt = *(int *) (rightSerial + i * sizeof(int));
        *(int *) (leftSerial + i * sizeof(int)) = utilities::cmov_int(predicate, rightInt, tempLeftInt);
        *(int *) (rightSerial + i * sizeof(int)) = utilities::cmov_int(predicate, tempLeftInt, rightInt);
    }

    left = ObliviousSerializer::deserializeObliviousTuple(schema, leftSerial);
    right = ObliviousSerializer::deserializeObliviousTuple(schema, rightSerial);
};

void SortHelper::TupleComp(
        std::vector<ObliviousTuple>::iterator first,
        std::vector<ObliviousTuple>::iterator second,
        type::RecordSchema & schema,
        std::vector<pos_vdb> & orderBys,
        std::vector<SortOrder> & sortOrders
){
    if (first->isVacuous()) {
        if (sortOrders.at(0) == SortOrder::ASCEND) {
            ObliviousTuple left (*first);
            *first = *second;
            *second = left;
        }
        return;
    }
    if (second->isVacuous()) {
        if (sortOrders.at(0) == SortOrder::DESCEND) {
            ObliviousTuple left (*first);
            *first = *second;
            *second = left;
        }
        return;
    }

    int verdict = 0;
    for (int i = 0, size = (size_vdb) orderBys.size(); i < size; i++) {
        int pos = orderBys[i];
        bool asc = sortOrders.at(i) == SortOrder::ASCEND;
        int tempRet = 1;
        int condition = (int) (((*first)[pos] < (*second)[pos] && asc) || ((*first)[pos] > (*second)[pos] && (!asc)));
        tempRet = utilities::cmov_int(condition, tempRet, -1);
        tempRet = utilities::cmov_int((int) ((*first)[pos] == (*second)[pos]), 0, tempRet);
        verdict = utilities::cmov_int((int) (verdict == 0 && tempRet != 0), tempRet, verdict);
    }

    SortHelper::ObliviousTupleSwap(verdict < 0, schema, *first, *second);
} ;