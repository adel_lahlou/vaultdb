#include <in_sgx/utilities/DummyInSGXKeyManager.h>
#include <in_sgx/obdata/ObliviousTuple.h>
#if defined(LOCAL_DEBUG_MODE) || defined(OUTSIDE_COMPILATION)
#include <in_sgx/utilities/picosha2.h>
#else
#include "sgx_tcrypto.h"
#endif
#include "in_sgx/utilities/DummyInSGXHashPartitioner.h"

using namespace utilities;
DummyInSGXHashPartitioner::DummyInSGXHashPartitioner(size_vdb numHosts, TransmitterID transmitterID, std::vector<pos_vdb> hashAttributes)
: _numHosts(numHosts), _transmitterID(transmitterID), _attributes(hashAttributes)
{};

pos_vdb DummyInSGXHashPartitioner::hashObliviousField(const db::obdata::obfield::ObliviousField & field)
{
#if defined(LOCAL_DEBUG_MODE) || defined(OUTSIDE_COMPILATION)
    //any STL sequantial containers (vector, list, dequeue...)
    std::vector<unsigned char> input = field.convertToUnsignedCharVector();
    std::vector<unsigned char> output(32);
    // in: iterator pair, out: contaner
    picosha2::hash256(input.begin(), input.end(), output, DummyInSGXKeyManager::instance().getAddConstant(_transmitterID), DummyInSGXKeyManager::instance().getInitialMessageDigest(_transmitterID));
    return output[0] % _numHosts;

#else
    sgx_sha256_hash_t hash_output;
    std::vector<unsigned char> input = field.convertToUnsignedCharVector();
    sgx_sha256_msg(reinterpret_cast<unsigned char*>(input.data()),input.size(), &hash_output);
    return hash_output[0] % _numHosts;
    //TODO: IMPLEMENT HASH FUNCTIONALITY
#endif
};

pos_vdb DummyInSGXHashPartitioner::hashSelectObliviousFields(const db::obdata::ObliviousTuple & tuple)
{
#if defined(LOCAL_DEBUG_MODE) || defined(OUTSIDE_COMPILATION)
    std::vector<unsigned char> input;
    for (pos_vdb i = 0, size = (size_vdb)_attributes.size(); i < size; i ++) {
        std::vector<unsigned char> interm = tuple[_attributes[i]].convertToUnsignedCharVector();
        input.insert(input.end(), interm.begin(), interm.end());
    }
    std::vector<unsigned char> output(32);
    picosha2::hash256(
            input.begin(), input.end(), output,
            DummyInSGXKeyManager::instance().getAddConstant(_transmitterID),
            DummyInSGXKeyManager::instance().getInitialMessageDigest(_transmitterID));
    return output[0] % _numHosts;
#else
    std::vector<unsigned char> input;
    for (pos_vdb i = 0, size = (size_vdb)_attributes.size(); i < size; i ++) {
        std::vector<unsigned char> interm = tuple[_attributes[i]].convertToUnsignedCharVector();
        input.insert(input.end(), interm.begin(), interm.end());
    }
    sgx_sha256_hash_t hash_output;
    sgx_sha256_msg(reinterpret_cast<unsigned char*>(input.data()),input.size(), &hash_output);
    return hash_output[0] % _numHosts;
#endif
};
