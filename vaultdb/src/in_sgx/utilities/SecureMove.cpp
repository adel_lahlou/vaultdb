#include "in_sgx/utilities/SecureMove.h"

int utilities::cmov_int(int zeroIsFalse, int t_val, int f_val) {
    // NOTE: only 16 bits and 32 bits integers supported
    int result;
    __asm__ volatile (
        "test   %1, %1;"
        "mov    %2, %0;"
        "cmovz  %3, %0;"
        "test   %2, %2;"
        : "=r" (result)
        : "r" (zeroIsFalse), "r" (t_val), "r" (f_val)
        : "cc"
    );
    return result;
};

short utilities::cmov_short(short zeroIsFalse, short t_val, short f_val) {
    // NOTE: only 16 bits and 32 bits integers supported
    short result;
    __asm__ volatile (
        "test   %1, %1;"
        "mov    %2, %0;"
        "cmovz  %3, %0;"
        "test   %2, %2;"
        : "=r" (result)
        : "r" (zeroIsFalse), "r" (t_val), "r" (f_val)
        : "cc"
    );
    return result;
};

int utilities::cmov_compare_identical_array(unsigned char * left, unsigned char * right, unsigned int length)
{
    unsigned int processed = 0;
    int isDifferent = 0;
    while (length - processed >= sizeof(int)) {
        isDifferent = cmov_int(*(int *)(left + processed) - *(int *)(right + processed), 1, isDifferent);
        processed += sizeof(int);
    }

    if (length - processed >= sizeof(int)) {
        isDifferent = cmov_short(*(short *)(left + processed) - *(short *)(right + processed), 1, isDifferent);
        processed += sizeof(short);
    }

    if (length - processed >= sizeof(char)) {
        isDifferent = cmov_int((int)*(left + processed) - (int)*(right + processed), 1, isDifferent);
    }
    return 1 - isDifferent;
}

void utilities::cmov_copy_array(int go, unsigned char *dst, unsigned char *origin, unsigned int length)
{
    unsigned int processed = 0;
    while (length - processed >= sizeof(int)) {
        *(int *)(dst + processed) = cmov_int(go, *(int *)(origin + processed), *(int *)(dst + processed));
        processed += sizeof(int);
    }
    if (length - processed >= sizeof(int)) {
        *(short *)(dst + processed) = cmov_short((short)go, *(short *)(origin + processed), *(short *)(dst + processed));
        processed += sizeof(short);
    }
    if (length - processed >= sizeof(char)) {
        *(dst + processed) = (unsigned char)cmov_int(go, (int)*(origin + processed), (short)*(dst + processed));
    }
};

