#include <cstring>
#include <in_sgx/serialization/ObliviousSerializer.h>
#include <in_sgx/serialization/InSGXQueryDecoder.h>
#if defined(LOCAL_DEBUG_MODE) || defined(OUTSIDE_COMPILATION)
#else
#include "Enclave_t.h"
#include "Crypto.h"
#include "sgx_tcrypto.h"
#endif
#include <in_sgx/utilities/InSGXController.h>

using namespace utilities;

size_vdb InSGXController::PermittedStorageInBytes = 20971520;

InSGXController::InSGXController(
        MachineID localID,
        uint64_t enclaveID
) : _localMachineID(localID),
    _enclave_id(enclaveID),
    _decoder(new serialization::InSGXQueryDecoder(this)),
    _bufferPool(new BufferPool(PermittedStorageInBytes))
{
    _registeredMachines.insert(_localMachineID);
};

InSGXController::~InSGXController() {
    _decoder.reset();
    _bufferPool.reset();
};

void InSGXController::registerMachine(MachineID newMachine)
{
    _registeredMachines.insert(newMachine);
};

void InSGXController::registerHonestBroker(MachineID hbMachineID)
{
    _honestBrokerID = hbMachineID;
};

MachineID InSGXController::getMachineID()
{
    return _localMachineID;
};

BufferPool * InSGXController::getBufferPool()
{
    return _bufferPool.get();
};

GenericReturnStatus InSGXController::dispatch(MachineID dst_machine_id, TableID outputTableID, size_vdb size, unsigned char *data)
{
    if (dst_machine_id == _localMachineID) {
        collect(_localMachineID, size, data);
    } else {
#if defined(LOCAL_DEBUG_MODE) || defined(OUTSIDE_COMPILATION)
        printf("\n\n\nInSGXController::dispatch called\n\n\n");
        abort();
#else
	uint32_t cipher_size = size + SGX_AESGCM_IV_SIZE + SGX_AESGCM_MAC_SIZE;
	uint8_t * ciphertext = (uint8_t *) malloc(cipher_size);
	encrypt(data, size, ciphertext);
	ocall_transmit_oblivious_table(dst_machine_id, outputTableID , ciphertext, cipher_size, 0);
	free(ciphertext);
#endif
    }
    return GenericReturnStatus::Ok;
};

GenericReturnStatus
InSGXController::dispatchMessage(MachineID dst_machine_id, size_vdb size, unsigned char *data)
{
    if (dst_machine_id == _localMachineID) {
        collectMessage(_localMachineID, size, data);
    } else {
#if defined(LOCAL_DEBUG_MODE) || defined(OUTSIDE_COMPILATION)
        printf("\n\n\nInSGXController::dispatchMessage called\n\n\n");
        abort();
#else 
	ocall_transmit_oblivious_table(dst_machine_id, -1 , data, size, 0);
#endif
    }
    return GenericReturnStatus::Ok;
};

void InSGXController::syncWithOtherHosts(MessageID messageID)
{
    size_vdb sendSize = sizeof(Message) + sizeof(messageID);
    unsigned char data[sendSize];
    ((Message*)data)->src_tid = SYNC_MESSAGE_TABLEID;
    *(MessageID*)((Message*)data)->data = messageID;
    size_vdb dispatches = 0;
    std::unique_lock<std::mutex> localGuard(_messageMutex);
    for (auto a : _registeredMachines) {
        if (a != _localMachineID) {
            dispatchMessage(a, sendSize, data);
            dispatches++;
        }
    }
    // TODO replace with condition variable
    while (_syncSets[messageID].size() < dispatches) {
        // busy waiting
    }
    _syncSets.erase(messageID);
};

bool InSGXController::hasTable(TableID id)
{
    return _bufferPool->hasTablePartition(_localMachineID, id, DEFAULT_TABLE_PARTITION_ID);
};

ObliviousTupleTable * InSGXController::fetchLocalSinglePartitionTable(TableID id)
{
    return _bufferPool->fetch(_localMachineID, id, DEFAULT_TABLE_PARTITION_ID);
};

bool InSGXController::insertTuple(db::obdata::ObliviousTupleTable *table, db::obdata::ObliviousTuple &t)
{
    return _bufferPool->insertTuple(table, t);
};

bool InSGXController::insertTuple(MachineID machineID, TableID tableID, pos_vdb partitionID, db::obdata::ObliviousTuple &t)
{
    return _bufferPool->insertTuple(_bufferPool->fetch(machineID, tableID, partitionID), t);
};

bool InSGXController::eraseTuple(db::obdata::ObliviousTupleTable *table, pos_vdb i) {
    return _bufferPool->eraseTuple(table, i);
}

db::obdata::ObliviousTupleTable *
InSGXController::create(MachineID machineID, TableID tableID, pos_vdb partitionID, size_vdb numPartitions, type::RecordSchema schema)
{
    return _bufferPool->create(machineID, tableID, partitionID, numPartitions, schema);
};

db::obdata::ObliviousTupleTable * InSGXController::fetch(MachineID machineID, TableID tableID, pos_vdb partitionID)
{
    return _bufferPool->fetch(machineID, tableID, partitionID);
};

bool InSGXController::erase(MachineID machineID, TableID tableID, pos_vdb partitionID)
{
    return _bufferPool->erase(machineID, tableID, partitionID);
};

void InSGXController::collect(
        MachineID src_machine_id,
        size_vdb size,
        unsigned char * data)
{
#if defined(LOCAL_DEBUG_MODE) || defined(OUTSIDE_COMPILATION)
#else
    uint8_t * plaintext;
    if (src_machine_id != _localMachineID) {
	plaintext = (uint8_t *)malloc(size - SGX_AESGCM_IV_SIZE - SGX_AESGCM_MAC_SIZE);
	decrypt(data, size, plaintext);
	data = plaintext;
	size = size - SGX_AESGCM_IV_SIZE - SGX_AESGCM_MAC_SIZE;
    }
#endif

    type::RecordSchema temp = type::RecordSchema::deserializeFrom(data);
    TableID src_tid = temp.getTableID();

    if (src_machine_id == _localMachineID
        && _decoderTableRequest.find(src_tid) != _decoderTableRequest.end()) {
        std::unique_lock<std::mutex> guard(_queryCollectionMutex);
        _decoderTableRequest.erase(src_tid);
        _bufferPool->convertAndStore(src_machine_id, data, size);
        _queryInputDeliveryCV.notify_all();
        return;
    }

    // if order is in place, deliver immediately
    MachineTableIDPair key(src_machine_id, src_tid);
    std::map<pos_vdb, ClusterOperator *> erasure;
    int i = 0;
    ObliviousTupleTable * res;
    std::unique_lock<std::mutex> guard(_intraOpCollectionMutex);
    res = _bufferPool->convertAndStore(src_machine_id, data, size);
    pos_vdb partitionID = res->getParitionID();
    for (auto it : _clusterOpRequests[key]) {
        if (it.first != partitionID) {
            continue;
        }
        it.second->receiveCallBack(src_machine_id, src_tid, res);
        erasure.insert(it);
        i++;
    }
    if (erasure.empty()) {
        _unProcessedIncomingTable[key].insert(partitionID);
    }
    for (auto e : erasure){
        _clusterOpRequests[key].erase(e.first);
    }
#if defined(LOCAL_DEBUG_MODE) || defined(OUTSIDE_COMPILATION)
#else
    if (src_machine_id != _localMachineID) {
	free(plaintext);
    }
#endif
};

void InSGXController::collectMessage(MachineID src_machine_id, size_vdb size, unsigned char *data)
{
    TableID src_tid = ((Message*)data)->src_tid;
    if (src_tid == SYNC_MESSAGE_TABLEID) {
        MessageID messageID = *(MessageID*)((Message*)data)->data;
        _syncSets[messageID].insert(src_machine_id);
        return;
    }

    MachineTableIDPair key(src_machine_id, src_tid);
    std::set<ClusterOperator *> erasure;
    std::unique_lock<std::mutex> guard(_messageMutex);
    for (auto it : _clusterOpMessageRequests[key]) {
        it->receiveGenericMessage(src_machine_id, src_tid, data, size);
        erasure.insert(it);
    }
    if (erasure.empty()) {
        auto ptr = std::shared_ptr<unsigned char>(new unsigned char[size], [](unsigned char * c){delete[] c;});
        memcpy(ptr.get(), data, size);
        _unProcessedMessages[key] = {size, ptr};
    }
    for (auto e : erasure){
        _clusterOpMessageRequests[key].erase(e);
    }
};

size_vdb InSGXController::getNumberOfMachines()
{
    return (size_vdb)_registeredMachines.size();
}

void InSGXController::requestTable(MachineID src_machine_id, TableID src_tid, pos_vdb partitionID,
                                   ClusterOperator *callBackPointer)
{
    MachineTableIDPair key(src_machine_id, src_tid);
    std::unique_lock<std::mutex> localGuard(_intraOpCollectionMutex);
    if (_unProcessedIncomingTable.find(key) != _unProcessedIncomingTable.end()
            && _unProcessedIncomingTable[key].find(partitionID) != _unProcessedIncomingTable[key].end()) {
        // We assume each operator will have a different TableID, therefore it's safe to erase the record
        callBackPointer->receiveCallBack(src_machine_id, src_tid, _bufferPool->fetch(src_machine_id, src_tid, partitionID));
        _unProcessedIncomingTable[key].erase(partitionID);
        // we leave the decision of erasing a table to the operator
    } else {
        MachineTableIDPair key(src_machine_id, src_tid);
        _clusterOpRequests[key].emplace(partitionID, callBackPointer);
    }
};

void InSGXController::requestMessage(MachineID src_machine_id, TableID src_tid, ClusterOperator *callBackPointer)
{
    MachineTableIDPair key(src_machine_id, src_tid);
    std::unique_lock<std::mutex> localGuard(_messageMutex);
    if (_unProcessedMessages.find(key) != _unProcessedMessages.end()) {
        auto data = _unProcessedMessages[key].data;
        auto size = (size_vdb)_unProcessedMessages[key].size;
        callBackPointer->receiveGenericMessage(src_machine_id, src_tid, data.get(), size);
        _unProcessedMessages.erase(key);
    } else {
        _clusterOpMessageRequests[key].insert(callBackPointer);
    }
};

void InSGXController::requestBaseTable(TableID src_tid) {
    _decoderTableRequest.insert(src_tid);
};

void InSGXController::receiveQuery(unsigned char * data, size_vdb size, int isFinalQuery)
{
    unsigned char * readHead = data;
    std::set<TableID> erasureSet;

    // Number input tables required
    size_vdb tableCount = *(size_vdb*)readHead;
    readHead += sizeof(size_vdb);

    std::unique_lock<std::mutex> guard(_queryCollectionMutex);
    // check the tables are all there
    for (size_vdb i = 0; i < tableCount; i++) {
        if (!hasTable(*(TableID*)readHead)) {
            requestBaseTable(*(TableID*)readHead);
        };
        erasureSet.insert(*(TableID*)readHead);
        readHead += sizeof(TableID);
    }
    while (_decoder->parseQuery(data) == serialization::ParserStatus::TableMissing) {
        _queryInputDeliveryCV.wait(guard, [this, data]{return _decoder->parseQuery(data) != serialization::ParserStatus::TableMissing;});
    }
    // run now
    if (_decoder->getQueryType() == serialization::OperatorType::KANONYMOUS) {
        runKAnoQueryObliviousTupleTable(_decoder->acquireKAnonymousQueryRoot(), erasureSet, isFinalQuery);
    } else {
        runObliviousObliviousTupleTable(_decoder->acquireObliviousQueryRoot(), erasureSet, isFinalQuery);
    }
};;

// FIXME implement the run functions

void InSGXController::runKAnoQueryObliviousTupleTable(plan::kaoperators::KAnonymousOperator *op, std::set<TableID> erasureSet,
                                                      int isFinalQuery) {
    TableID tableID = op->getOutputTableID();
        ObliviousTupleTable * output = create(_localMachineID, tableID, DEFAULT_TABLE_PARTITION_ID, 1, op->getSchema());

    int i = 0;

#if defined(LOCAL_DEBUG_MODE) || defined(OUTSIDE_COMPILATION)
#else
    ocall_print_string((std::string("<><><> InSGXController pulling... before the first one!!!! <><><>\n")).c_str());
#endif

    while (op->next() == OperatorStatus::Ok) {
#if defined(LOCAL_DEBUG_MODE) || defined(OUTSIDE_COMPILATION)
#else
        if (i % 10000000 == 0) {
        ocall_print_string((std::string("<><><> InSGXController pulling the ") + std::to_string(i) + std::string("th output tuple <><><>\n")).c_str());
        }
#endif
        i++;
        ObliviousTuple t;
        op->getCurrentTuple(t);
        insertTuple(output, t);
    }
    for (auto a : erasureSet) {
        erase(_localMachineID, a, DEFAULT_TABLE_PARTITION_ID);
    }
    dispatchQueryResult(op->getOutputTableID(), *output, isFinalQuery);
};

void InSGXController::runObliviousObliviousTupleTable(plan::oboperators::ObliviousOperator *op, std::set<TableID> erasureSet,
                                                      int isFinalQuery) {
    TableID tableID = op->getSchema().getTableID();
    ObliviousTupleTable * output = create(_localMachineID, tableID, DEFAULT_TABLE_PARTITION_ID, 1, op->getSchema());
    output -> setSchema(op->getSchema());
    ObliviousTuple t;

    int i = 0;

#if defined(LOCAL_DEBUG_MODE) || defined(OUTSIDE_COMPILATION)
#else
    ocall_print_string((std::string("<><><> InSGXController pulling... before the first one!!!! <><><>\n")).c_str());
#endif

    while (op->next() == OperatorStatus::Ok) {
#if defined(LOCAL_DEBUG_MODE) || defined(OUTSIDE_COMPILATION)
#else
        ocall_print_string((std::string("<><><> InSGXController pulling the ") + std::to_string(i) + std::string("th output tuple <><><>\n")).c_str());
#endif
        i++;
        op->getCurrentTuple(t);
        insertTuple(output, t);
    }
    for (auto a : erasureSet) {
        erase(_localMachineID, a, DEFAULT_TABLE_PARTITION_ID);
    }
    dispatchQueryResult(op->getOutputTableID(), *output, isFinalQuery);
};

void InSGXController::dispatchQueryResult(TableID opID, ObliviousTupleTable &output, int isFinalQuery) {
    size_vdb size = output.serializationSize();
    unsigned char resultChars[size];
    output.serializeTo(resultChars);
#if defined(LOCAL_DEBUG_MODE) || defined(OUTSIDE_COMPILATION)
#else
    ocall_print_string((std::string("<><><> Query ") + std::to_string(opID) + std::string(" on host ") + std::to_string(_localMachineID) +
                        std::string(" completed. Result contains ") + std::to_string(output.size()) + std::string(" tuples <><><>\n")).c_str());
#endif
    erase(_localMachineID, opID, DEFAULT_TABLE_PARTITION_ID);
#if defined(LOCAL_DEBUG_MODE) || defined(OUTSIDE_COMPILATION)
	printf("\n\n\nInSGXController::dispatchQueryResult called\n\n\n");
	abort();
#else
    // FIXME this is such a hack. Make use of the depositTable function in OutsideController.
    if (isFinalQuery == STEP_QUERY) {
        collect(_localMachineID, size, resultChars);
        unsigned char temp[1];
        ocall_transmit_oblivious_table(_honestBrokerID, opID , temp, 1, isFinalQuery);
    } else {
        ocall_transmit_oblivious_table(_honestBrokerID, opID , resultChars, size, isFinalQuery);
    }
#endif
};
