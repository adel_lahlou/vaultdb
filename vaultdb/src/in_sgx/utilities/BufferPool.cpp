#include <in_sgx/serialization/ObliviousSerializer.h>
#include "in_sgx/utilities/BufferPool.h"

using namespace utilities;

#ifndef BUFFER_POOL_DEBUG
//#define BUFFER_POOL_DEBUG
#endif

BufferPool::BufferPool(size_vdb storageLimitInBytes)
: _permittedStorage(storageLimitInBytes), _usedStorage(0)
{};

BufferPool::~BufferPool(){
    for (auto m : _storage) {
        for (auto t : m.second) {
            for (auto p : t.second) {
                if (hasTablePartition(m.first, t.first, p.first)) {
                    erase(m.first, t.first, p.first);
                }
            }
        }
    }
};

bool BufferPool::hasTablePartition(MachineID machineID, TableID tableID, pos_vdb partitionID) {
    return _presenceFlag[machineID][tableID].find(partitionID) != _presenceFlag[machineID][tableID].end();
};

db::obdata::ObliviousTupleTable * BufferPool::convertAndStore(MachineID machineID, unsigned char *table, size_vdb size)
{
    auto tablePtr = new db::obdata::ObliviousTupleTable(db::obdata::ObliviousSerializer::deserializeObliviousTupleTable(table, size));
    auto tableID = tablePtr->getTableID();
    auto partitionID = tablePtr->getParitionID();
    if (_presenceFlag[machineID][tableID][partitionID]) {
#ifdef LOCAL_DEBUG_MODE
        printf("Abort. Trying to deserialize table [%d, %d, %d]; table already exists.\n", machineID, tableID, partitionID);
        abort();
#endif
    }
    _presenceFlag[machineID][tableID][partitionID] = true;
    _storage[machineID][tableID][partitionID] = std::shared_ptr<db::obdata::ObliviousTupleTable>(tablePtr);
    _usedStorage += tablePtr->serializationSize();
    return tablePtr;
};

db::obdata::ObliviousTupleTable * BufferPool::fetch(MachineID machineID, TableID tableID, pos_vdb partitionID)
{
    if (!hasTablePartition(machineID, tableID, partitionID)) {
#ifdef LOCAL_DEBUG_MODE
        printf("Abort. Trying to fetch table [%d, %d, %d]; table does NOT exist.\n", machineID, tableID, partitionID);
        abort();
#endif
    }
    return _storage[machineID][tableID][partitionID].get();
};

bool BufferPool::insertTuple(db::obdata::ObliviousTupleTable *table, db::obdata::ObliviousTuple &t)
{
    // check for size remaining
    size_vdb tupleSize = table->getSchema().tupleSerializationSize();
    _usedStorage += tupleSize;
    table->add(t);
    return true;
};;

db::obdata::ObliviousTupleTable * BufferPool::create(MachineID machineID, TableID tableID, pos_vdb partitionID, size_vdb numPartitions, type::RecordSchema schema)
{
    if (hasTablePartition(machineID, tableID, partitionID)) {
#ifdef LOCAL_DEBUG_MODE
        printf("Abort. Trying to create empty table [%d, %d, %d]; table already exists.\n", machineID, tableID, partitionID);
        abort();
#endif
    }
    auto tablePtr = new db::obdata::ObliviousTupleTable(schema, {});
    auto serializationSize = tablePtr->serializationSize();
    _usedStorage += serializationSize;
    tablePtr->setParitionID(partitionID);
    tablePtr->setNumberOfPartitions(numPartitions);
    _presenceFlag[machineID][tableID][partitionID] = true;
    _storage[machineID][tableID][partitionID] = std::shared_ptr<db::obdata::ObliviousTupleTable>(tablePtr);
    return tablePtr;
};

bool BufferPool::eraseTuple(db::obdata::ObliviousTupleTable *table, pos_vdb i) {
    size_vdb tupleSize = table->getSchema().tupleSerializationSize();
    if (tupleSize > _usedStorage) {
#ifdef LOCAL_DEBUG_MODE
        printf("Abort. Erasing too many bytes when erasing a tuple from a table; total known / erasing: %d / %d\n", _usedStorage, tupleSize);
        abort();
#endif
    }
    table->erase(i);
    _usedStorage -= tupleSize;
    return true;
}

bool BufferPool::erase(MachineID machineID, TableID tableID, pos_vdb partitionID)
{
    if (hasTablePartition(machineID, tableID, partitionID)) {
        _presenceFlag[machineID][tableID][partitionID] = false; // debug
        _presenceFlag[machineID][tableID].erase(partitionID);
        auto minus = _storage[machineID][tableID][partitionID].get()->serializationSize();
        if (minus > _usedStorage) {
#ifdef LOCAL_DEBUG_MODE
            printf("Abort. Erasing too many bytes when erasing partition [%d, %d, %d]; total known / erasing: %d / %d\n", machineID, tableID, partitionID, _usedStorage, minus);
            abort();
#endif
        }
        _usedStorage -= minus;
        _storage[machineID][tableID].erase(partitionID);
        return true;
    } else {
#ifdef LOCAL_DEBUG_MODE
        printf("Abort. Erasing Non existent table [%d, %d, %d];\n", machineID, tableID, partitionID);
        abort();
#endif
    }
};

#ifdef LOCAL_DEBUG_MODE
void BufferPool::printContent(MachineID localMachineID) {
    printf("Host %d buffer pool content: data remaining: %d; ", localMachineID, _usedStorage);
    for (auto m : _storage) {
        for (auto t : m.second) {
            for (auto p : t.second) {
                printf("[%d, %d, %d: %d] ", m.first, t.first, p.first, p.second->serializationSize());
            }
        }
    }
    printf("\n");
}
#endif