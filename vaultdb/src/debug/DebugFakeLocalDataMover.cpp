#include "debug/DebugFakeLocalDataMover.h"

#include <thread>
#include <in_sgx/serialization/ObliviousSerializer.h>
#include <cstring>
#include <utilities/ConfigManager.h>

using namespace utilities;

std::shared_ptr<DebugFakeLocalDataMover> DebugFakeLocalDataMover::_instance;
bool DebugFakeLocalDataMover::_isInitialized = false;

DebugFakeLocalDataMover * DebugFakeLocalDataMover::instance()
{
    if (!_isInitialized) {
        _instance = std::shared_ptr<DebugFakeLocalDataMover>(new DebugFakeLocalDataMover());
        _isInitialized = true;
    }
    return _instance.get();
};

DebugFakeLocalDataMover::DebugFakeLocalDataMover()
        : _postgres(ConfigManager::instance()->getPgUser().get(),ConfigManager::instance()->getPgUserPass().get(),ConfigManager::instance()->getPgDB().get())
{}

void DebugFakeLocalDataMover::registerHostGlobally(MachineID newMachine, InSGXController *dispatcher)
{
    std::unique_lock<std::mutex> localGuard(_registryLock);
    for (auto it = _globalMachineRegistry.begin();
         it != _globalMachineRegistry.end(); it++) {
        it->second->registerMachine(newMachine);
        dispatcher->registerMachine(it->first);
    }
    _globalMachineRegistry.emplace(newMachine, dispatcher);
}

void DebugFakeLocalDataMover::queryLocalPostgreAndSendIntoEnclave(MachineID machineID, type::RecordSchema &schema, const char *query, int isFinalQuery)
{
    db::obdata::ObliviousTupleTable out;
    if ( _postgres.singleQueryWithObliviousTupleTable(out, schema, query) == RETURN_SUCCESS) {
        size_vdb size = out.serializationSize();
        unsigned char data[size];
        out.serializeTo(data);
        if (isFinalQuery == FINAL_QUERY) {
            fprintf(stderr, "[FAILURE] Plaintext queries are not supported\n");
        } else {
            auto guard = new std::unique_lock<std::mutex> (_registryLock);
            auto p = _globalMachineRegistry.at(machineID);
            delete guard;
            p->collect(machineID, size, data);
        }
    } else {
        fprintf(stderr, "[FAILURE] Cannot query postgres to create table\n");
    };
};

void DebugFakeLocalDataMover::deploySecureNewQuery(MachineID machineID, TableID outputTableID, TableID input0,
                                                   TableID input1, unsigned char *inputBuffer,
                                                   size_vdb size,
                                                   int isFinalQuery) {
    InSGXController* controller;
    auto l = new std::unique_lock<std::mutex>(_registryLock);
    controller = _globalMachineRegistry[machineID];
    if (_waitForCounts.find(outputTableID) == _waitForCounts.end()) {
        _waitForCounts[outputTableID] = 0;
    }
    _waitForCounts[outputTableID]++;
    delete l;

    if (input0 != INPUT_TEST_TABLE_NOT_SPECIFIED) {
        MachineTableIDPair key(machineID, input0);
        while (_intermediateResults.find(key) == _intermediateResults.end()) {
            // busy waiting
        }
        auto pair = _intermediateResults[key];
        controller->collect(machineID, pair.size, pair.data.get());
        _intermediateResults.erase(key);
    }
    if (input1 != INPUT_TEST_TABLE_NOT_SPECIFIED) {
        MachineTableIDPair key(machineID, input1);
        while (_intermediateResults.find(key) == _intermediateResults.end()) {
            // busy waiting
        }
        auto pair = _intermediateResults[key];
        controller->collect(machineID, pair.size, pair.data.get());
        _intermediateResults.erase(key);
    }
    controller->receiveQuery(inputBuffer, size, isFinalQuery);
};

GenericReturnStatus
DebugFakeLocalDataMover::dispatch(MachineID dst_machineid, MachineID src_machineid, TableID outputTableID, size_vdb size, unsigned char *data,
                                  bool isFinalQuery)
{
    std::unique_lock<std::mutex> guard(_registryLock);
    if (dst_machineid == 0) {
        if (isFinalQuery) {
            if (_finalTable.getSchema().size() == 0) {
                _finalTable = db::obdata::ObliviousSerializer::deserializeObliviousTupleTable(data,size);
            } else {
                _finalTable.append(db::obdata::ObliviousSerializer::deserializeObliviousTupleTable(data,size));
            }
        } else {
            unsigned char * temp = new unsigned char[size];
            memcpy(temp, data, size);
            _intermediateResults[MachineTableIDPair(src_machineid, outputTableID)] = {size, std::shared_ptr<unsigned char>(temp, [](unsigned char * p){delete[] p;})};
        }
        _waitForCounts[outputTableID]--;
        if (_waitForCounts[outputTableID] == 0 ){
            _waitForCounts.erase(outputTableID);
        }
        return GenericReturnStatus::Ok;
    }

//    std::unique_lock<std::mutex> guard(_registryLock);
    InSGXController *c = _globalMachineRegistry.at(dst_machineid);
    c->collect(src_machineid, size, data);
    return GenericReturnStatus::Ok;
};

GenericReturnStatus
DebugFakeLocalDataMover::dispatchMessage(MachineID dst_machineid, MachineID src_machineid, size_vdb size,
                                         unsigned char *data) {
    std::unique_lock<std::mutex> guard(_registryLock);
    TableID tableID = ((Message*)data)->src_tid;
    if (tableID == SYNC_MESSAGE_TABLEID) {
        InSGXController *c = _globalMachineRegistry.at(dst_machineid);
        c->collectMessage(src_machineid, size, data);
        return GenericReturnStatus::Ok;
    } else {
        if (_columnSortRs.find(tableID) == _columnSortRs.end()) {
            _columnSortRs[tableID].count = 0;
            _columnSortRs[tableID].r = 0;
        }
        _columnSortRs[tableID].count ++;
        size_vdb partitionCount =  *(size_vdb*) ((Message*)data)->data;
        _columnSortRs[tableID].r = _columnSortRs[tableID].r < partitionCount ? partitionCount : _columnSortRs[tableID].r;
        size_vdb machineCount = (size_vdb)_globalMachineRegistry.size();
        if (_columnSortRs[tableID].count == machineCount) {
            // compute and dispatch
            auto r = _columnSortRs[tableID].r;
            r += r < 2 * machineCount * machineCount ? 2 * machineCount * machineCount - r : 0;
            r += r % machineCount == 0 ? 0 : machineCount - r % machineCount;

            size_vdb msgSize = sizeof(Message) + sizeof(size_vdb);
            unsigned char msg[msgSize];
            ((Message *)msg)->src_tid = tableID;
            *(size_vdb*)((Message *)msg)->data = r; // number of partition given block size;

            for (pos_vdb i = 1 ; i <= machineCount; i++) {
                InSGXController *c = _globalMachineRegistry.at(i);
                c->collectMessage(0, msgSize, msg);
            }
        }
        return GenericReturnStatus ::Ok;
    }
};

void DebugFakeLocalDataMover::ingestDebugTable(MachineID dst_machineid, TableID outputTableID, size_vdb size, unsigned char *data)
{
    InSGXController * host;
    auto l = new std::unique_lock<std::mutex>(_registryLock);
    host = _globalMachineRegistry[dst_machineid];
    delete l;
    host->collect(dst_machineid, size, data);
};

bool DebugFakeLocalDataMover::isMachineRegistered(MachineID machineID)
{
    std::unique_lock<std::mutex> guard(_registryLock);
    return _globalMachineRegistry.find(machineID)
           != _globalMachineRegistry.end();
}

bool DebugFakeLocalDataMover::isDone() {
    std::unique_lock<std::mutex> guard(_registryLock);
    return _waitForCounts.empty();
};

db::obdata::ObliviousTupleTable & DebugFakeLocalDataMover::getFinalOutput() {
    return _finalTable;
};

void DebugFakeLocalDataMover::clear() {
    std::unique_lock<std::mutex> guard(_registryLock);
#ifdef PRINT_BUFFER_POOL_STATUS
    for (auto m : _globalMachineRegistry) {
        m.second->getBufferPool()->printContent(m.first);
    }
#endif
    _globalMachineRegistry.clear();
    _columnSortRs.clear();
    _intermediateResults.clear();
    _finalTable = db::obdata::ObliviousTupleTable();
    _waitForCounts.clear();
#ifdef PRINT_BUFFER_POOL_STATUS
    printf("All cleared.\n\n");
#endif
}