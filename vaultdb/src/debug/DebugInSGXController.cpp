#include "debug/DebugFakeLocalDataMover.h"
#include "debug/DebugInSGXController.h"
#include <in_sgx/obdata/ObliviousTupleTable.h>

using namespace utilities;

#include <in_sgx/serialization/InSGXQueryDecoder.h>

DebugInSGXController::DebugInSGXController(
        MachineID localID,
        uint64_t enclaveID
) : InSGXController(localID,
                    enclaveID)
{
    DebugFakeLocalDataMover::instance()->registerHostGlobally(localID, this);
};

GenericReturnStatus DebugInSGXController::dispatch(MachineID dst_machine_id, TableID outputTableID, size_vdb size, unsigned char *data)
{
    if (dst_machine_id == _localMachineID) {
        collect(_localMachineID, size, data);
    } else {
        DebugFakeLocalDataMover::instance()->dispatch(dst_machine_id, _localMachineID, outputTableID, size, data, false);
    }
    return GenericReturnStatus::Ok;
};

GenericReturnStatus DebugInSGXController::dispatchMessage(MachineID dst_machine_id, size_vdb size, unsigned char *data)
{
    if (dst_machine_id == _localMachineID) {
        collectMessage(_localMachineID, size, data);
    } else {
        DebugFakeLocalDataMover::instance()->dispatchMessage(dst_machine_id, _localMachineID, size, data);
    }
    return GenericReturnStatus::Ok;
};

void DebugInSGXController::dispatchQueryResult(TableID opID, db::obdata::ObliviousTupleTable &output, int isFinalQuery)
{
    size_vdb size = output.serializationSize();
    unsigned char resultChars[size];
    output.serializeTo(resultChars);
    _bufferPool->erase(_localMachineID, opID, DEFAULT_TABLE_PARTITION_ID);
    DebugFakeLocalDataMover::instance()->dispatch(0, _localMachineID, opID, size, resultChars, isFinalQuery);
};

plan::kaoperators::KAnonymousOperator * DebugInSGXController::encodeKAnonymousOperatorAndParse(plan::kaoperators::KAnonymousOperator * input) {
    size_vdb encodingSize;
    std::shared_ptr<unsigned char> query (
            plan::kaoperators::KAnonymousOperator::EncodeQueryToNewArray(
                    input, encodingSize), [](unsigned char * p){delete[] p;});
    _decoder->parseQuery(query.get());
    return _decoder->acquireKAnonymousQueryRoot();
};

void DebugInSGXController::collectTablePointer(db::obdata::ObliviousTupleTable * table) {
    auto temp = _bufferPool->create(_localMachineID, table->getTableID(), table->getParitionID(), 1, table->getSchema());
    for (pos_vdb i = 0, size = (size_vdb) table->size(); i < size; i++) {
        _bufferPool->insertTuple(temp, (*table)[i]);
    }
};