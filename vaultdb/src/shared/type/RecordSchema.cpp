#include "shared/type/RecordSchema.h"

using namespace type;

RecordSchema::RecordSchema()
{}


RecordSchema::RecordSchema(TableID id, const SchemaColumnHolder &ts)
        : types(ts), tableid(id)
{}


//RecordSchema::RecordSchema(TableID id, std::initializer_list<FieldDataType> ts)
//        : types(ts), tableid(id)
//{}


RecordSchema::RecordSchema(const RecordSchema &rs)
        : types(rs.types), tableid(rs.tableid)
{}


RecordSchema& RecordSchema::operator=(const RecordSchema &rs)
{
    tableid = rs.tableid;
    types = rs.types;
    return *this;
}


void RecordSchema::addField(SchemaColumn t)
{
    types.push_back(t);
}


void RecordSchema::erase(size_vdb pos)
{
    types.erase(types.begin() + pos);
}


void RecordSchema::project(std::vector<size_vdb> poses)
{
    size_vdb deleted = 0;
    for(size_vdb pos : poses){
        erase(pos - deleted);
        ++deleted;
    }
}

RecordSchema RecordSchema::join(const RecordSchema &r1, const RecordSchema &r2)
{
    SchemaColumnHolder joined;
    const size_vdb r1_size = r1.size();
    const size_vdb r2_size = r2.size();
    joined.reserve(r1_size + r2_size);

    for(int i = 0; i < r1_size; ++i){
        joined.push_back(r1[i]);
    }

    for(int i = 0; i < r2_size; ++i){
        joined.push_back(r2[i]);
    }

    return RecordSchema(0, joined);
}

const TableID RecordSchema::getTableID() const
{
    return tableid;
};

void RecordSchema::setTableID(TableID id)
{
    tableid = id;
};

size_vdb RecordSchema::serializeTo(unsigned char *buf) const
{
    TableID id = tableid;

    unsigned char * end = buf;

    // table id
    *(TableID*) end = tableid;
    end += sizeof(TableID);

    // number of columns
    *(size_vdb*) end = size();
    end += sizeof(size_vdb);

    // each column
    for(auto f : types) {
        *(FieldDataType *) end = f.type;
        end += sizeof(FieldDataType);
        *(size_vdb*) end = f.size;
        end += sizeof(size_vdb);
    }

    return (size_vdb) (end - buf);
}

size_vdb RecordSchema::serializationSize() const
{
    return sizeof(TableID) + sizeof(size_vdb) + (size() * (sizeof(FieldDataType) + sizeof(size_vdb)));
}

size_vdb RecordSchema::tupleSerializationSize() const
{
    size_vdb ret = sizeof(char); // dummy flag
    for (pos_vdb i = 0, size = (size_vdb)types.size(); i < size; i++) {
        if (types[i].type == FieldDataType::Fixchar) {
            ret += types[i].size+1;
        } else {
            ret += types[i].size;
        }
    }
    return ret;
};

RecordSchema RecordSchema::deserializeFrom(const unsigned char *data)
{
    size_vdb offset = 0;

    TableID id = *(TableID*) data;
    offset += sizeof(TableID);
    size_vdb numFields = *(size_vdb*) (data + offset);
    offset += sizeof(size_vdb);

    SchemaColumnHolder fields;
    for(int i = 0; i < numFields; ++i) {
        FieldDataType t = *(FieldDataType*) (data + offset);
        offset += sizeof(FieldDataType);
        size_vdb s = *(size_vdb*)(data + offset);
        offset += sizeof(size_vdb);
        fields.push_back(SchemaColumn{t,s});
    }

    return RecordSchema(id, fields);
}

size_vdb RecordSchema::size() const
{
    return types.size();
}

const SchemaColumn& RecordSchema::operator[](size_vdb pos) const
{
    return types[pos];
}

SchemaColumn& RecordSchema::operator[](size_vdb pos)
{
    return types[pos];
}

bool RecordSchema::operator==(RecordSchema comparison) {
    if(size() != comparison.size())
        return false;
    for(size_vdb i = 0, size = this->size(); i < size; ++i){
        if(this->types[i].type != comparison[i].type)
            return false;
    }
    return true;
};

bool RecordSchema::operator==(const RecordSchema & comparison) const {
    if(size() != comparison.size())
        return false;
    for(size_vdb i = 0, size = this->size(); i < size; ++i){
        if(this->types[i].type != comparison[i].type)
            return false;
    }
    return true;
};