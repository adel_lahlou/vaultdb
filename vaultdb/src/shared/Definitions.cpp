#include "shared/Definitions.h"

int swap_int_endian(int u)
{
    /**
     * Grabbed from https://stackoverflow.com/a/4956493
     * Original text by Alexandre C.
     */
    //    static_assert (CHAR_BIT == 8, "CHAR_BIT != 8");

    union
    {
        int u;
        unsigned char u8[sizeof(int)];
    } source, dest;

    source.u = u;

    for (size_vdb k = 0; k < sizeof(int); k++)
    dest.u8[k] = source.u8[sizeof(int) - k - 1];

    return dest.u;
}

short swap_short_endian(short u)
{
    /**
     * Grabbed from https://stackoverflow.com/a/4956493
     * Original text by Alexandre C.
     */
//    static_assert (CHAR_BIT == 8, "CHAR_BIT != 8");

    union
    {
        short u;
        unsigned char u8[sizeof(short)];
    } source, dest;

    source.u = u;

    for (size_vdb k = 0; k < sizeof(short); k++)
        dest.u8[k] = source.u8[sizeof(short) - k - 1];

    return dest.u;
}

int int_10_power[10] = {
        1, 10, 100, 1000, 10000, 100000, 1000000, 10000000, 100000000, 1000000000
};

int time_t_divisor[6] = {
        1, 3600, 86400, 2592000, 31104000, 311040000
};


MachineTableIDPair::MachineTableIDPair(MachineID mid, TableID tid)
: machineID(mid), tableID(tid) {};

MachineTableIDPair& MachineTableIDPair::operator=(const MachineTableIDPair &comp) {
    machineID = comp.machineID;
    tableID = comp.tableID;
    return *this;
};

bool MachineTableIDPair::operator<(const MachineTableIDPair &comp) const {
    if (this->machineID < comp.machineID) {
        return true;
    } else if (this->machineID == comp.machineID) {
        return this->tableID < comp.tableID;
    } else {
        return false;
    }
};
bool MachineTableIDPair::operator==(const MachineTableIDPair &comp) const {
    return this->machineID == comp.machineID && this->tableID == comp.tableID;
};