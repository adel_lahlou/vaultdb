#ifndef VAULTDB_PROJ_DEBUGFAKELOCALDATAMOVER_H
#define VAULTDB_PROJ_DEBUGFAKELOCALDATAMOVER_H

#include "in_sgx/utilities/InSGXController.h"
#include "utilities/PostgreSQLHandler.h"
#include <mutex>

namespace utilities {

    struct ColumnSortMessageCountAndR {
        size_vdb count;
        size_vdb r;
    };

    // TODO this is quite self-contained; update this to result in a full-fledged local tester
    class DebugFakeLocalDataMover {

        friend class InSGXController;
        friend class DebugInSGXController;

    public:
        static DebugFakeLocalDataMover * instance();
        void queryLocalPostgreAndSendIntoEnclave(MachineID machineID, type::RecordSchema &schema, const char *query, int isFinalQuery);
        void deploySecureNewQuery(MachineID machineID, TableID outputTableID, TableID input0, TableID input1,
                                  unsigned char *inputBuffer, size_vdb size,
                                  int isFinalQuery);
        GenericReturnStatus
        dispatch(MachineID dst_machineid, MachineID src_machineid, TableID outputTableID, size_vdb size, unsigned char *data,
                 bool isFinalQuery);
        GenericReturnStatus
        dispatchMessage(MachineID dst_machineid, MachineID src_machineid, size_vdb size, unsigned char *data);
        void ingestDebugTable(MachineID dst_machineid, TableID outputTableID, size_vdb size, unsigned char *data);
        void clear();
        bool isDone();
        db::obdata::ObliviousTupleTable & getFinalOutput();
    protected:
        DebugFakeLocalDataMover();
        void registerHostGlobally(MachineID machineid, InSGXController *machine);
        static std::shared_ptr<DebugFakeLocalDataMover> _instance;
        static bool _isInitialized;
    private:
        std::mutex _registryLock;
        std::map<MachineID, InSGXController *> _globalMachineRegistry;
        std::map<MachineTableIDPair, DataSizePair> _intermediateResults;
        bool isMachineRegistered(MachineID machineID);
        PostgreSQLHandler _postgres;
        db::obdata::ObliviousTupleTable _finalTable;
        std::map<TableID, size_vdb> _waitForCounts;

        std::map<TableID, ColumnSortMessageCountAndR> _columnSortRs;
    };
}


#endif //VAULTDB_PROJ_DEBUGFAKELOCALDATAMOVER_H
