#ifndef VAULTDB_PROJ_DEBUGINSGXCONTROLLER_H
#define VAULTDB_PROJ_DEBUGINSGXCONTROLLER_H

#include "in_sgx/utilities/InSGXController.h"

namespace utilities {
    class DebugInSGXController : public InSGXController {
    public:
        DebugInSGXController(MachineID localID, uint64_t enclaveID);
        GenericReturnStatus
        dispatch(MachineID dst_machine_id, TableID outputTableID, size_vdb size, unsigned char *data) override;
        GenericReturnStatus
        dispatchMessage(MachineID dst_machine_id, size_vdb size, unsigned char *data) override;
        void dispatchQueryResult(TableID opID, db::obdata::ObliviousTupleTable &output, int isFinalQuery) override;
        plan::kaoperators::KAnonymousOperator * encodeKAnonymousOperatorAndParse(plan::kaoperators::KAnonymousOperator * input);
        void collectTablePointer(db::obdata::ObliviousTupleTable * table);
    };
}

#endif //VAULTDB_PROJ_DEBUGINSGXCONTROLLER_H
