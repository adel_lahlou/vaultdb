#ifndef VAULTDB_OBLIVIOUSSERIALIZER_H
#define VAULTDB_OBLIVIOUSSERIALIZER_H


#include <cstddef>
#include "shared/type/RecordSchema.h"

namespace type {
    class RecordSchema;
}

namespace db {
namespace obdata {

    class ObliviousTupleTable;
    class ObliviousTuple;

    class ObliviousSerializer {
    public:
        /* Serialization checks for adequate size and fails before writing.
         * Checks only occur in serializeTupleTable currently. Contract
         * is for future use. No metadata is saved so a RecordSchema is necessary.
         *
         * @param  {char}   buf - pointer to store bytes
         * @param  {size_vdb} limit - max number of bytes to add
         *
         * @return {int}    < 0 indicates failure, otherwise, the number of bytes written
         */
        static unsigned char * serializeObliviousTupleTable(const ObliviousTupleTable& tt, size_vdb& serializedSize);
        static ObliviousTupleTable deserializeObliviousTupleTable(const unsigned char * data, size_vdb size);
        static ObliviousTuple deserializeObliviousTuple(const type::RecordSchema& s, const unsigned char * data);

    private:
        ObliviousSerializer();
    };
}
}
#endif //VAULTDB_OBLIVIOUSSERIALIZER_H
