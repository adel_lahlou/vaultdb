#ifndef VAULTDB_OBLIVIOUSQUERYDECODER_H
#define VAULTDB_OBLIVIOUSQUERYDECODER_H

#include <in_sgx/plan/kaoperators/KAnonymousSeqScan.h>
#include <in_sgx/plan/kaoperators/KAnonymousFilter.h>
#include <in_sgx/plan/kaoperators/KAnonymousNestedJoin.h>
#include <in_sgx/plan/kaoperators/KAnonymousSortedGroup.h>
#include <in_sgx/plan/kaoperators/KAnonymousUnion.h>
#include <in_sgx/plan/kaoperators/KAnonymousProject.h>
#include <in_sgx/plan/kaoperators/KAnonymousBitonicSort.h>
#include <shared/QueryEncodingDefinitions.h>
#include <in_sgx/plan/oboperators/ObliviousKAnonymous.h>
#include <plaintext/plan/operators/SeqScan.h>
#include <in_sgx/plan/kaoperators/ClusterKAnonymousLDAggregation.h>
#include <in_sgx/plan/kaoperators/ClusterKAnonymousPooledJoin.h>
#include <in_sgx/plan/kaoperators/ClusterKAnonymousRepartition.h>
#include <in_sgx/plan/kaoperators/KAnonymousLocalGenFilter.h>
#include <in_sgx/plan/kaoperators/ClusterKAnonymousBitonicMergeSort.h>
#include <in_sgx/plan/kaoperators/ClusterKAnonymousSortedAggregation.h>
#include <in_sgx/plan/kaoperators/ClusterKAnonymousBinnedAggregation.h>
#include <in_sgx/plan/kaoperators/ClusterKAnonymousGenJoin.h>
#include <in_sgx/plan/kaoperators/ClusterKAnonymousLimit.h>
#include <in_sgx/plan/kaoperators/ClusterKAnonymousColumnSort.h>
#include <in_sgx/plan/kaoperators/ClusterKAnonymousWindow.h>
#include "in_sgx/plan/oboperators/ObliviousOperator.h"
#include "in_sgx/plan/oboperators/BitonicSort.h"
#include "in_sgx/plan/oboperators/ObliviousSeqScan.h"
#include "in_sgx/plan/oboperators/ObliviousFilter.h"
#include "in_sgx/plan/oboperators/ObliviousNestedJoin.h"
#include "in_sgx/plan/oboperators/ObliviousSortedGroup.h"
#include "in_sgx/plan/oboperators/ObliviousProject.h"
#include "in_sgx/plan/oboperators/ObliviousUnion.h"
#include "in_sgx/plan/kaoperators/KAnonymousOperator.h"
#include "in_sgx/plan/obexpressions/obexpressions.h"
#include "in_sgx/plan/obexpressions/Aggregates.h"
#include "shared/type/RecordSchema.h"
#include "shared/Definitions.h"

namespace type {
    class RecordSchema;
};

namespace plan{
    namespace oboperators{
        class ObliviousOperator;
        class ObliviousSeqScan;
        class BitonicSort;
    };
    namespace expressions {
        class Expression;
        class Literal;
        class Column;
        class Add;
        class Sub;
        class Mul;
        class Div;
        class Eq;
        class NEq;
        class LT;
        class GT;
        class LTE;
        class GTE;
        class And;
        class Or;
        class Not;
    };
};

namespace utilities{
    class InSGXController;
}

using namespace plan::oboperators;
using namespace plan::obexpressions;
using namespace plan::kaoperators;

namespace serialization {

    class InSGXQueryDecoder {
    public:
        InSGXQueryDecoder(utilities::InSGXController *dispatcher);
        ~InSGXQueryDecoder();
        ParserStatus parseQuery(unsigned char* instruction);
        ObliviousOperator * acquireObliviousQueryRoot();
        KAnonymousOperator * acquireKAnonymousQueryRoot();
        OperatorType getQueryType();
    private:
        ObliviousOperator * _parsedObliviousQueryRoot;
        KAnonymousOperator * _parsedKAnonymousQueryRoot;
        OperatorType _type;
        MachineID _machineID;
        utilities::InSGXController * _dispatcher;
        std::mutex _parseMutex;

        KAnonymousOperator * parseKAnonymousOperator(unsigned char* &instruction);
        void parseKAnonymousOperatorBasicInfo(unsigned char* &instruction, KAnonymousOperatorPtrList &children, TableID & tableID, size_vdb & k);
        KAnonymousSeqScan * parseKAnonymousSeqScan(unsigned char *&instruction);
        KAnonymousFilter * parseKAnonymousFilter(unsigned char *&instruction);
        KAnonymousBitonicSort * parseKAnonymousBitonicSort(unsigned char *&instruction);
        KAnonymousProject * parseKAnonymousProject(unsigned char *&instruction);
        KAnonymousSortedGroup * parseKAnonymousSortedGroup(unsigned char *&instruction);
        KAnonymousLocalGenFilter * parseKAnonymousLocalGenFilter(unsigned char *&instruction);
        KAnonymousUnion * parseKAnonymousUnion(unsigned char *&instruction);
        ClusterKAnonymousLDAggregation * parseKAnonymousLocalDistroAgg(unsigned char *&instruction);
        ClusterKAnonymousPooledJoin *  parseKAnonymousHashPooledJoin(unsigned char *&instruction);
        ClusterKAnonymousBitonicMergeSort * parseKAnonymousBitonicMergeSort(unsigned char *& instruction);
        ClusterKAnonymousRepartition * parseKAnonymousRepartition(unsigned char *& instruction);
        ClusterKAnonymousSortedAggregation * parseClusterKAnonymousSortedAggregation(unsigned char *&instruction);
        ClusterKAnonymousBinnedAggregation * parseClusterKAnonymousBinnedAggregation(unsigned char *&instruction);
        ClusterKAnonymousGenJoin * parseClusterKAnonymousGenJoin(unsigned char *&instruction);
        ClusterKAnonymousLimit * parseClusterKAnonymousLimit(unsigned char *&instruction);
        ClusterKAnonymousColumnSort * parseKAnonymousColumnSort(unsigned char *& instruction);
        ClusterKAnonymousWindow * parseClusterKAnonymousWindow(unsigned char *&instruction);

        ObliviousOperator * parseObliviousOperator(unsigned char* &instruction);
        void parseObliviousOperatorBasicInfo(unsigned char* &instruction, ObliviousOperatorPtrList &children, TableID & tableID);
        ObliviousSeqScan * parseObliviousSeqScan(unsigned char *&instruction);
        ObliviousFilter * parseObliviousFilter(unsigned char *&instruction);
        ObliviousSortedGroup * parseObliviousSortedGroup(unsigned char *&instruction);
        ObliviousKAnonymous * parseObliviousKAnonymous(unsigned char *&instruction);
        BitonicSort * parseObliviousSort(unsigned char *&instruction);
        ObliviousProject * parseObliviousProject(unsigned char *&instruction);

        ObliviousExpression parseExpression(unsigned char* &instruction);
        ObliviousExpression parseColumn(unsigned char* &instruction);
        ObliviousExpression parseLiteral(unsigned char* &instruction);
        ObliviousExpression parseAlgebra(unsigned char* &instruction);
        ObliviousExpression parseAggregate(unsigned char* &instruction);
    };
};

#endif //VAULTDB_OBLIVIOUSQUERYDECODER_H
