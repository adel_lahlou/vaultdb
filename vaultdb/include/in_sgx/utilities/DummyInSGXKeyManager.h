#ifndef VAULTDB_PROJ_DUMMYINSGXKEYMANAGER_H
#define VAULTDB_PROJ_DUMMYINSGXKEYMANAGER_H


#include <shared/Definitions.h>
#include <map>
#include <memory>

namespace utilities {
    class DummyInSGXKeyManager {
    public:
        static DummyInSGXKeyManager instance(){
            static DummyInSGXKeyManager manager;
            return manager;
        }
        unsigned long * getAddConstant(TransmitterID transmitterID) const;
        unsigned long * getInitialMessageDigest(TransmitterID transmitterID) const;
    private:
        DummyInSGXKeyManager();
        std::map<TransmitterID, std::shared_ptr<unsigned long>> _addConstants;
        std::map<TransmitterID, std::shared_ptr<unsigned long>> _initialMessageDigest;
    };
}


#endif //VAULTDB_PROJ_DUMMYINSGXKEYMANAGER_H
