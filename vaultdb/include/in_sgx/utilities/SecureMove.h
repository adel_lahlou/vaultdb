#ifndef VAULTDB_SECUREMOVE_H
#define VAULTDB_SECUREMOVE_H

namespace utilities {
    int cmov_int(int zeroIsFalse, int t_val, int f_val) ;
    short cmov_short(short zeroIsFalse, short t_val, short f_val);
    int cmov_compare_identical_array(unsigned char * left, unsigned char * right, unsigned int length);
    void cmov_copy_array(int go, unsigned char *dst, unsigned char *origin, unsigned int length);
}

#endif //VAULTDB_SECUREMOVE_H
