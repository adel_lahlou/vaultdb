#ifndef VAULTDB_PROJ_BUFFERPOOL_H
#define VAULTDB_PROJ_BUFFERPOOL_H

#include <map>
#include <shared/Definitions.h>
#include <in_sgx/obdata/ObliviousTuple.h>
#include <in_sgx/obdata/ObliviousTupleTable.h>
#include <mutex>

namespace utilities {
    class BufferPool {
    public:
        BufferPool(size_vdb storageLimitInBytes);
        ~BufferPool();
        db::obdata::ObliviousTupleTable * convertAndStore(MachineID machineID, unsigned char *table, size_vdb size);
        bool hasTablePartition(MachineID machineID, TableID tableID, pos_vdb partitionID);
        db::obdata::ObliviousTupleTable * fetch(MachineID machineID, TableID tableID, pos_vdb partitionID);
        bool insertTuple(db::obdata::ObliviousTupleTable *table, db::obdata::ObliviousTuple &t);
        bool eraseTuple(db::obdata::ObliviousTupleTable *table, pos_vdb i);
        db::obdata::ObliviousTupleTable * create(MachineID machineID, TableID tableID, pos_vdb partitionID, size_vdb numPartitions, type::RecordSchema schema);
        bool erase(MachineID machineID, TableID tableID, pos_vdb partitionID);
#ifdef LOCAL_DEBUG_MODE
        void printContent(MachineID localMachineID);
#endif
    private:
        size_vdb _permittedStorage;
        size_vdb _usedStorage;
        std::map<MachineID, std::map<TableID, std::map<pos_vdb, bool>>> _presenceFlag;
        std::map<MachineID, std::map<TableID, std::map<pos_vdb, std::shared_ptr<db::obdata::ObliviousTupleTable>>>> _storage;
    };
}

#endif //VAULTDB_PROJ_BUFFERPOOL_H
