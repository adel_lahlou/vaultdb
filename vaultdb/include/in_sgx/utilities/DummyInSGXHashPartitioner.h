#ifndef VAULTDB_PROJ_DUMMYHASHPARTITIONER_H
#define VAULTDB_PROJ_DUMMYHASHPARTITIONER_H


#include <shared/Definitions.h>
#include <in_sgx/obdata/obfield/ObliviousField.h>
#include <in_sgx/obdata/ObliviousTuple.h>

namespace utilities {
    class DummyInSGXHashPartitioner {
    public:
        DummyInSGXHashPartitioner(size_vdb numHosts, TransmitterID transmitterID, std::vector<pos_vdb> hashAttributes);

        ~DummyInSGXHashPartitioner() = default;

        pos_vdb hashObliviousField(const db::obdata::obfield::ObliviousField &field);
        pos_vdb hashSelectObliviousFields(const db::obdata::ObliviousTuple & tuple);

    private:
        size_vdb _numHosts;
        TransmitterID _transmitterID;
        std::vector<pos_vdb> _attributes;
    };
}


#endif //VAULTDB_PROJ_DUMMYHASHPARTITIONER_H
