#ifndef VAULTDB_PROJ_SORTHELPER_H
#define VAULTDB_PROJ_SORTHELPER_H

#include <in_sgx/obdata/ObliviousTuple.h>
#include <shared/type/RecordSchema.h>

using db::obdata::ObliviousTuple;

class SortHelper {
public:
    static void TupleComp(
            std::vector<ObliviousTuple>::iterator,
            std::vector<ObliviousTuple>::iterator,
            type::RecordSchema &,
            std::vector<pos_vdb> &,
            std::vector<SortOrder> &);
private:
    static void ObliviousTupleSwap(int predicate, type::RecordSchema &schema, db::obdata::ObliviousTuple &left, db::obdata::ObliviousTuple &right);
};

#endif //VAULTDB_PROJ_SORTHELPER_H
