#ifndef VAULTDB_INSGXCONTROLLER_H
#define VAULTDB_INSGXCONTROLLER_H

#include <map>
#include <utility>
#include <set>
#include <memory>
#include <queue>
#include <mutex>
#include <condition_variable>
#include "shared/Definitions.h"
#include "in_sgx/plan/clusterops/ClusterOperator.h"
#include "in_sgx/plan/oboperators/ObliviousOperator.h"
#include "in_sgx/plan/kaoperators/KAnonymousOperator.h"
#include "BufferPool.h"

namespace serialization {
    class InSGXQueryDecoder;
}

namespace db {
    namespace obdata {
        class ObliviousTupleTable;
    }
}

namespace utilities {
    class InSGXController {

        friend class ClusterOperator;
        friend class serialization::InSGXQueryDecoder;

    public:
        // dispatcher carries information about how many machines are out there
        InSGXController(MachineID localID, uint64_t enclaveID);
        ~InSGXController();
        void registerMachine(MachineID newMachine);
        void registerHonestBroker(MachineID hbMachineID);
        MachineID getMachineID();
        BufferPool * getBufferPool();
#ifdef LOCAL_DEBUG_MODE
        virtual
#endif
        GenericReturnStatus
        dispatch(MachineID dst_machine_id, TableID outputTableID, size_vdb size, unsigned char *data);
#ifdef LOCAL_DEBUG_MODE
        virtual
#endif
        GenericReturnStatus
        dispatchMessage(MachineID dst_machine_id, size_vdb size, unsigned char *data);
        void syncWithOtherHosts(MessageID messageID);
        void collect(MachineID src_machine_id, size_vdb size, unsigned char *data);
        void collectMessage(MachineID src_machine_id, size_vdb size, unsigned char *data);
        size_vdb getNumberOfMachines();
        void requestTable(MachineID src_machine_id, TableID src_tid, pos_vdb partitionID,
                                  ClusterOperator *callBackPointer);
        void requestMessage(MachineID src_machine_id, TableID src_tid, ClusterOperator *callBackPointer);
        void requestBaseTable(TableID src_tid);
        void receiveQuery(unsigned char * data, size_vdb size, int isFinalQuery);
        bool hasTable(TableID id);
        db::obdata::ObliviousTupleTable * fetchLocalSinglePartitionTable(TableID id);

        bool insertTuple(db::obdata::ObliviousTupleTable *table, db::obdata::ObliviousTuple &t);
        bool insertTuple(MachineID machineID, TableID tableID, pos_vdb partitionID, db::obdata::ObliviousTuple &t);
        bool eraseTuple(db::obdata::ObliviousTupleTable *table, pos_vdb i);
        db::obdata::ObliviousTupleTable *
        create(MachineID machineID, TableID tableID, pos_vdb partitionID, size_vdb numPartitions, type::RecordSchema schema);
        db::obdata::ObliviousTupleTable * fetch(MachineID machineID, TableID tableID, pos_vdb partitionID);
        bool erase(MachineID machineID, TableID tableID, pos_vdb partitionID);

    protected:
        static size_vdb PermittedStorageInBytes; // 20 MB = 20971520, 40 MB = 41943040, 80 MB = 83886080

        void runKAnoQueryObliviousTupleTable(plan::kaoperators::KAnonymousOperator *op, std::set<TableID> erasureSet,
                                                     int isFinalQuery);
        void runObliviousObliviousTupleTable(plan::oboperators::ObliviousOperator *op, std::set<TableID> erasureSet,
                                                     int isFinalQuery);
#ifdef LOCAL_DEBUG_MODE
        virtual
#endif
        void dispatchQueryResult(TableID opID, db::obdata::ObliviousTupleTable &output, int isFinalQuery);

        MachineID _localMachineID;
        MachineID _honestBrokerID;
        uint64_t _enclave_id;
        std::set<MachineID> _registeredMachines;
        std::set<TableID> _decoderTableRequest;
        std::shared_ptr<serialization::InSGXQueryDecoder> _decoder;
        std::shared_ptr<BufferPool> _bufferPool;
        std::queue<std::pair<std::shared_ptr<unsigned char>, int>> _outstandingQueries;

        std::mutex _intraOpCollectionMutex;
        std::mutex _messageMutex;
        std::mutex _queryCollectionMutex;

        std::map<MessageID, std::set<MachineID>> _syncSets;
        std::map<MachineTableIDPair, std::map<pos_vdb, ClusterOperator *>> _clusterOpRequests;
        std::map<MachineTableIDPair, std::set<pos_vdb>> _unProcessedIncomingTable;
        std::map<MachineTableIDPair, std::set<ClusterOperator *>> _clusterOpMessageRequests;
        std::map<MachineTableIDPair, DataSizePair> _unProcessedMessages;

        std::condition_variable _queryInputDeliveryCV;
    };

}

#endif //VAULTDB_INSGXCONTROLLER_H
