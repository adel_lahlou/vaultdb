#ifndef VAULTDB_PROJ_BITONICSORTER_H
#define VAULTDB_PROJ_BITONICSORTER_H

#include <iterator>
#include <shared/Definitions.h>

using db::obdata::ObliviousTuple;

template<typename T, typename S>
class BitonicSorter {
public:
    BitonicSorter<T, S>(std::vector<pos_vdb> orderBys, std::vector<SortOrder> sortOrders, S schema,
                  void (*comp)( typename std::vector<T>::iterator, typename std::vector<T>::iterator, S &, std::vector<pos_vdb> & , std::vector<SortOrder> &))
    : _orderBys(orderBys), _sortOrders(sortOrders), _schema(schema), _comp(comp) {
        for (int i = 0, size = (size_vdb) _sortOrders.size(); i < size; i++) {
            _reverseSortOrders.push_back(_sortOrders.at(i) == SortOrder::ASCEND ? SortOrder::DESCEND : SortOrder::ASCEND);
        }
    };
    unsigned int requiredPadCount(typename std::vector<T>::iterator first, typename std::vector<T>::iterator last) {
        auto dist = (size_vdb) std::distance(first, last);
        unsigned int v = dist;
        v--;
        v |= v >> 1;
        v |= v >> 2;
        v |= v >> 4;
        v |= v >> 8;
        v |= v >> 16;
        v++;
        return v - dist;
    };
    void sort(typename std::vector<T>::iterator first, typename std::vector<T>::iterator last) {
        internalSort(first, last, true);
    };
private:
    void internalSort(typename std::vector<T>::iterator first, typename std::vector<T>::iterator last, bool forward) {
        if (std::distance(first, last) <= 1) {
            return;
        }
        auto half = first + (std::distance(first, last) - std::distance(first, last) / 2);
        internalSort(first, half, forward);
        internalSort(half, last, !forward);
        internalMerge(first, last, forward);
    };
    void internalMerge(typename std::vector<T>::iterator first, typename std::vector<T>::iterator last, bool forward) {
        if (std::distance(first, last) <= 1) {
            return;
        }
        auto half = first + (std::distance(first, last) - std::distance(first, last) / 2);
        auto firstReadHead = first;
        auto secondReadHead = half;
        while (firstReadHead != half) {
            _comp(firstReadHead, secondReadHead, _schema, _orderBys, forward ? _sortOrders : _reverseSortOrders);
            firstReadHead++;
            secondReadHead++;
        }
        internalMerge(first, half, forward);
        internalMerge(half, last, forward);
    };

    // data
    std::vector<pos_vdb> _orderBys;
    std::vector<SortOrder> _sortOrders;
    std::vector<SortOrder> _reverseSortOrders;
    S _schema;
    void (*_comp)( typename std::vector<T>::iterator, typename std::vector<T>::iterator, S &, std::vector<pos_vdb> &, std::vector<SortOrder> &);
};

#endif //VAULTDB_PROJ_BITONICSORTER_H
