#ifndef VAULTDB_OBLIVIOUSTUPLETABLE_H
#define VAULTDB_OBLIVIOUSTUPLETABLE_H

#include <vector>
#include <string>
#include <initializer_list>
#include "ObliviousTuple.h"
#include "shared/type/RecordSchema.h"

namespace utilities {
    class BufferPool;
}

namespace db {
namespace obdata {
    typedef std::vector<ObliviousTuple> ObliviousTupleList;
    typedef std::shared_ptr<ObliviousTupleList> ObliviousTupleListPtr;

    class ObliviousTupleTable {
        friend class utilities::BufferPool;
    public:
        ObliviousTupleTable();
        ObliviousTupleTable(const type::RecordSchema& s);
        ObliviousTupleTable(const type::RecordSchema& s, const ObliviousTupleList& ts);
        ObliviousTupleTable(const type::RecordSchema &s, pos_vdb partitionID, size_vdb numPartitions, const ObliviousTupleList &ts);
        ObliviousTupleTable(const ObliviousTupleTable& tt);
        ObliviousTupleTable& operator=(const ObliviousTupleTable& tt);

        size_vdb size() const;
        const type::RecordSchema& getSchema() const;
        std::string toString() const;
        void serializeTo(unsigned char * buf) const;
        size_vdb serializationSize() const;

        pos_vdb getParitionID() const ;
        size_vdb getNumberOfPartitions() const;
        TableID getTableID() const;
        void setTableID(TableID id);
        void setParitionID(pos_vdb id);
        void setNumberOfPartitions(size_vdb count);

        void setSchema(const type::RecordSchema& s);

        ObliviousTupleList::iterator begin();
        ObliviousTupleList::iterator end();
        void clear();

        ObliviousTuple& operator[](size_vdb pos);
        ObliviousTuple * at(size_vdb pos);
        size_vdb append(ObliviousTupleTable appendix);
        size_vdb appendMove(ObliviousTupleTable & appendix);


        static size_vdb CutOffLastN(size_vdb n, ObliviousTupleTable &inputAndRemainingOutput,
                                    ObliviousTupleTable &lastNOutput);

        static void half(ObliviousTupleTable & inputAndFirstHalfOutput, ObliviousTupleTable & secondHalfOutput);
        static void appendNonObliviousSortHalf(ObliviousTupleTable &firstHalf, ObliviousTupleTable &secondHalf,
                                               std::vector<pos_vdb> attributes, std::vector<SortOrder> orders);
    protected:
        void add(ObliviousTuple t);
        void addObliviousTuple(ObliviousTuple& t);
        void addObliviousTuple(ObliviousTuple&& t);
        size_vdb erase(size_vdb pos);
        size_vdb erase(ObliviousTupleList::iterator begin, ObliviousTupleList::iterator end);
    private:
        ObliviousTupleListPtr tuples;
        type::RecordSchema schema;
        TableID tableid;
        pos_vdb _partitionsID;
        size_vdb _numberOfPartitions;
    };
}
}
#endif //VAULTDB_OBLIVIOUSTUPLETABLE_H
