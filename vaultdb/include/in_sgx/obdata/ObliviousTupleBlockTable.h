#ifndef VAULTDB_PROJ_OBLIVIOUSTUPLEBLOCKTABLE_H
#define VAULTDB_PROJ_OBLIVIOUSTUPLEBLOCKTABLE_H

#include <shared/Definitions.h>
#include <in_sgx/utilities/BufferPool.h>
#include "ObliviousTupleBlock.h"

namespace utilities {
    class BufferPool;
}

namespace db {
namespace obdata {
    class ObliviousTupleBlockTable {
        friend class BufferPool;
    public:
        class const_iterator {
        public:
            const_iterator(ObliviousTupleBlockTable * table);
        private:
            MachineID _machineID;
            TableID _tableID;
            PartitionID _partitionID;
            BlockID _currentBlockID;

            std::vector<ObliviousTuple>::iterator _pageIt;
            utilities::BufferPool _bufferPool;
        };


        ~ObliviousTupleBlockTable() = default;

        void lockDownForRandomAccess(bool isLockDown);
        void fillVacuousTupleToNearestTwoPower();
        void removeTrailingVacuousTuples();
    protected:
        ObliviousTupleBlockTable(utilities::BufferPool * bufferPool);
        void push_back(ObliviousTuple & tuple);
    private:
        MachineID _machineID;
        TableID _tableID;
        PartitionID _partitionID;
        BlockID _lastKnownBlockBlockID;

        size_vdb _knownBlockCount;
        size_vdb _blockSize;

        bool _isLockDownForRandomAccess;

        utilities::BufferPool _bufferPool;
    };
}
}


#endif //VAULTDB_PROJ_OBLIVIOUSTUPLEBLOCKTABLE_H
