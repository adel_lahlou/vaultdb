#ifndef VAULTDB_OBLIVIOUSTUPLE_H
#define VAULTDB_OBLIVIOUSTUPLE_H

#include <string>
#include <vector>
#include <initializer_list>
#include <shared/type/RecordSchema.h>
#include "in_sgx/obdata/obfield/ObliviousField.h"
#include "shared/Definitions.h"

namespace db {
namespace obdata {
    typedef std::vector<obfield::ObliviousField> ObliviousFieldHolder;
    typedef std::shared_ptr<ObliviousFieldHolder> ObliviousFieldHolderPtr;
    typedef std::vector<size_vdb> ProjectList;

    class ObliviousTuple {
    public:
        ObliviousTuple();
        ObliviousTuple(const ObliviousFieldHolder &fs);
        ObliviousTuple(std::initializer_list<obfield::ObliviousField> fs);
        ObliviousTuple(bool isDummy, const ObliviousFieldHolder &fs);
        ObliviousTuple(bool isDummy, bool isDefault, const ObliviousFieldHolder &fs);
        ObliviousTuple(const ObliviousTuple &t);
        ObliviousTuple& operator =(const ObliviousTuple& t);
        ~ObliviousTuple();

        size_vdb size() const;
        bool isDummy() const;
        bool isVacuous() const;
        bool isDefaultTuple() const;
        std::string toString() const;
        void serializeTo(unsigned char * buf, const type::RecordSchema & schema) const;

        void setDummyFlag(bool flag);
        void setVacuousFlag(bool flag);
        void setDefaultFlag(bool flag);
        void push_back(obfield::ObliviousField f);
        void erase(size_vdb pos);
        void project(ProjectList poses);
        static ObliviousTuple join(const ObliviousTuple& t1, const ObliviousTuple& t2);

        static ObliviousTuple getVacuousTuple();

        const obfield::ObliviousField &operator[](size_vdb pos) const;
        obfield::ObliviousField &operator[](size_vdb pos);

    private:
        void get_fields_ownership();
        bool dummyFlag = false;
        bool vacuousFlag = false;
        bool defaultFlag = false;
        ObliviousFieldHolderPtr fields;
    };
}
}

#endif //VAULTDB_OBLIVIOUSTUPLE_H
