#ifndef VAULTDB_TUPLEFACTORY_H
#define VAULTDB_TUPLEFACTORY_H

#include "ObliviousTuple.h"
#include "shared/type/RecordSchema.h"

namespace db{
namespace obdata {
    class ObliviousDataFactory {
    public:
        static ObliviousTuple createDefaultTuple(const type::RecordSchema& schema);
        static ObliviousTuple createDefaultDummyTuple(const type::RecordSchema& schema);
        static obfield::ObliviousField createDefaultField(const type::SchemaColumn & type);
    };
}
}
#endif //VAULTDB_TUPLEFACTORY_H
