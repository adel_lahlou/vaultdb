#ifndef VAULTDB_PROJ_OBLIVIOUSTUPLEBLOCK_H
#define VAULTDB_PROJ_OBLIVIOUSTUPLEBLOCK_H

#include "ObliviousTuple.h"
#include "ObliviousTupleTable.h"

namespace utilities {
    class BufferPool;
}

namespace db {
namespace obdata {
    class ObliviousTupleBlock {
        friend class BufferPool;
#ifdef LOCAL_DEBUG_MODE
        friend class ObliviousTupleTableTest;
#endif
    public:
        class iterator {
        public:
            typedef iterator self_type;
            typedef ObliviousTuple value_type;
            typedef ObliviousTuple& reference;
            typedef ObliviousTuple* pointer;
            typedef std::random_access_iterator_tag iterator_category;
            typedef int difference_type;
            iterator(std::vector<ObliviousTuple> * tuplesPtr, pos_vdb pos) : _pos(pos), _tuplesPtr(tuplesPtr) {}
            self_type operator++() { _pos++; return *this; }
            self_type operator++(int unused) { self_type i = *this; _pos++; return i; }
            self_type operator--() { _pos--; return *this; }
            self_type operator--(int unused) { self_type i = *this; _pos--; return i; }
            reference operator*() { return _tuplesPtr->at(_pos); }
            pointer operator->() { return &_tuplesPtr->at(_pos); }
            self_type operator+(pos_vdb offset) { return iterator(_tuplesPtr, _pos + offset);}
            self_type operator-(pos_vdb offset) { return iterator(_tuplesPtr, _pos - offset);}
            difference_type operator-(const self_type& other ) { return (int) _pos - (int)other._pos; }
            bool operator==(const self_type& rhs) const { return _pos == rhs._pos;}
            bool operator!=(const self_type& rhs) const { return _pos != rhs._pos;}
            bool operator<(const self_type& other ) { return _pos < other._pos; }
            bool operator>(const self_type& other ) { return _pos > other._pos; }
            bool operator<=(const self_type& other ) { return _pos <= other._pos; }
            bool operator>=(const self_type& other ) { return _pos >= other._pos; }
            self_type & operator=(const self_type& other) { _tuplesPtr = other._tuplesPtr; _pos = other._pos; return *this; }
        private:
            pos_vdb _pos;
            std::vector<ObliviousTuple> * _tuplesPtr;
        };

        ObliviousTupleBlock();
        ObliviousTupleBlock(MachineID machineID, TableID tableID, PartitionID partitionID, BlockID blockID, size_vdb tuplePermitted);
        ObliviousTupleBlock(MachineID machineID, TableID tableID, PartitionID partitionID, BlockID blockID, size_vdb tuplePermitted, const ObliviousTupleList &ts);
        ~ObliviousTupleBlock() = default;

        iterator begin() {return iterator(&_tuples, 0);};
        iterator end() {return iterator(&_tuples, (pos_vdb)_tuples.size());};

        size_vdb size() const {return (size_vdb)_tuples.size();};
    protected:
        ObliviousTuple & operator[](pos_vdb pos) {return _tuples[pos];};
        bool insertTuple(ObliviousTuple & t);
        size_vdb ingestTuples(const ObliviousTupleList &ts);
        bool eraseTuple(pos_vdb i);
        size_vdb appendMove(ObliviousTupleBlock& appendix);
    private:
        MachineID _machineID;
        TableID _tableID;
        PartitionID _partitionID;
        BlockID _blockID;

        std::vector<ObliviousTuple> _tuples;
        size_vdb _numTuplesPermitted;
    };
}
}


#endif //VAULTDB_PROJ_OBLIVIOUSTUPLEBLOCK_H
