#ifndef VAULTDB_OBLIVIOUSTIMESTAMPNOZONEFIELD_H
#define VAULTDB_OBLIVIOUSTIMESTAMPNOZONEFIELD_H

#include <ctime>
#include <shared/Definitions.h>
#include <shared/type/FieldDataType.h>

#include "ObliviousField.h"
#include "ObliviousFieldPrototype.h"

namespace db{
    namespace obdata {
        namespace obfield {

            class ObliviousTimestampNoZoneField : public ObliviousFieldPrototype {
                friend class ObliviousField;
                friend class ObliviousFieldPrototype;
                friend class ObliviousIntField;
            public:
                ObliviousTimestampNoZoneField();
                ObliviousTimestampNoZoneField(time_t v);
                ObliviousTimestampNoZoneField(const ObliviousTimestampNoZoneField &f);
                virtual ~ObliviousTimestampNoZoneField();

                // meta functions
                virtual bool isTruthy() const override;
                type::FieldDataType getType() override;
//                type::SchemaColumn datatype() const noexcept;
                std::string toString() const override;
                void serializeTo(unsigned char * buf, size_vdb capacity) const override;
                static ObliviousField deserializeFrom(const unsigned char * data);

                // algebraic functions
                virtual ObliviousField operator + (ObliviousField const &f) const override;
                virtual ObliviousField operator - (ObliviousField const &f) const override;
                virtual ObliviousField operator * (ObliviousField const &f) const override;
                virtual ObliviousField operator / (ObliviousField const &f) const override;

                // comparison operators
                virtual bool operator==(const ObliviousField& rhs) const override;
                virtual bool operator!=(const ObliviousField& rhs) const override;
                virtual bool operator<=(const ObliviousField& rhs) const override;
                virtual bool operator>=(const ObliviousField& rhs) const override;
                virtual bool operator>(const ObliviousField& rhs) const override;
                virtual bool operator<(const ObliviousField& rhs) const override;

                virtual bool isEqTo(const ObliviousField &rhs, size_vdb genLevel, bool &actualResult) override;
                virtual bool isLEThan(const ObliviousField &rhs, size_vdb genLevel, bool &actualResult) override;
                virtual bool isGEThan(const ObliviousField &rhs, size_vdb genLevel, bool &actualResult) override;
                virtual bool
                isGThan(const ObliviousField &rhs, size_vdb genLevel, bool isNegating, bool &actualResult) override;
                virtual bool
                isLThan(const ObliviousField &rhs, size_vdb genLevel, bool isNegating, bool &actualResult) override;

                virtual int comparesTo(const ObliviousField &rhs, size_vdb genLevel) override;

                // algebraic functions implementations
                virtual ObliviousField ObliviousIntFieldAdd(ObliviousIntField const &f) const override;
                virtual ObliviousField ObliviousIntFieldSub(ObliviousIntField const &f) const override;
                virtual ObliviousField ObliviousIntFieldMul(ObliviousIntField const &f) const override;
                virtual ObliviousField ObliviousIntFieldDiv(ObliviousIntField const &f) const override;
                virtual ObliviousField ObliviousTimestampNoZoneFieldAdd(ObliviousTimestampNoZoneField const &f) const override;
                virtual ObliviousField ObliviousTimestampNoZoneFieldSub(ObliviousTimestampNoZoneField const &f) const override;
                virtual ObliviousField ObliviousTimestampNoZoneFieldMul(ObliviousTimestampNoZoneField const &f) const override;
                virtual ObliviousField ObliviousTimestampNoZoneFieldDiv(ObliviousTimestampNoZoneField const &f) const override;

                // comparison operators implementations
                virtual bool ObliviousTimestampNoZoneFieldEq(ObliviousTimestampNoZoneField const &f, size_vdb genLevel,
                                                             bool &actualResult) const override;
                virtual bool
                ObliviousTimestampNoZoneFieldLt(ObliviousTimestampNoZoneField const &f, size_vdb genLevel, bool isNegating,
                                                                bool &actualResult) const override;
                virtual bool
                ObliviousTimestampNoZoneFieldGt(ObliviousTimestampNoZoneField const &f, size_vdb genLevel, bool isNegating,
                                                                bool &actualResult) const override;

                virtual int ObliviousTimestampNoZoneFieldComparesTo(ObliviousTimestampNoZoneField const &f, size_vdb genLevel) const override;

                virtual HashNumber hash() override;
                virtual std::vector<unsigned char> convertToUnsignedCharVector() const override;


                time_t value;
            };
        }
    }
}


#endif //VAULTDB_OBLIVIOUSTIMESTAMPNOZONEFIELD_H
