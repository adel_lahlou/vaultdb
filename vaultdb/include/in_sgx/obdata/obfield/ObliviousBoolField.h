#ifndef VAULTDB_OBLIVIOUSBOOLFIELD_H
#define VAULTDB_OBLIVIOUSBOOLFIELD_H

namespace db {
namespace obdata {
namespace obfield {
    typedef ObliviousIntField ObliviousBoolField;
}
}
}
#endif //VAULTDB_BOOLFIELD_H
