//
// Created by Zuohao She on 4/8/17.
//

#ifndef VAULTDB_OBLIVIOUSFIELD_H
#define VAULTDB_OBLIVIOUSFIELD_H

#include <string>
#include <memory>
#include "shared/type/FieldDataType.h"
#include "shared/Definitions.h"

namespace plan {
    namespace obexpressions {
        class Sum;
    }
}

namespace db {
namespace obdata {
namespace obfield {

    struct BaseConstructor { BaseConstructor(int=0) {} };

    class ObliviousIntField;
    class ObliviousFixcharField;
    class ObliviousTimestampNoZoneField;
    class ObliviousFieldPrototype;
    class ObliviousField;

    extern void swap(db::obdata::obfield::ObliviousField& f1, db::obdata::obfield::ObliviousField& f2);
    typedef std::shared_ptr<ObliviousField> ObliviousFieldPtr;

    class ObliviousField
    {
        friend class plan::obexpressions::Sum;
        friend class ObliviousIntField;
        friend class ObliviousFixcharField;
        friend class ObliviousTimestampNoZoneField;

    public:
        ObliviousField& operator=(const ObliviousField& rhs);
        ObliviousField(const ObliviousField& f);
        virtual ~ObliviousField();

        // factory functions
        static ObliviousField makeObliviousIntField(int v);
        static ObliviousField makeObliviousBoolField(bool v);
        static ObliviousField makeDefaultObliviousFixcharField(size_vdb capacity);
        static ObliviousField makeObliviousFixcharField(const char *s, size_vdb capacity);
        static ObliviousField makeObliviousTimestampNoZoneField(time_t v);

        // meta functions
        virtual bool isTruthy() const;
//        virtual type::SchemaColumn datatype() const noexcept;
        virtual type::FieldDataType getType();
        virtual std::string toString() const;
        virtual void serializeTo(unsigned char * buf, size_vdb size) const;
        void swap(ObliviousField& f) throw();

        // algebraic funcs
        virtual ObliviousField operator + (ObliviousField const &f) const;
        virtual ObliviousField operator - (ObliviousField const &f) const;
        virtual ObliviousField operator * (ObliviousField const &f) const;
        virtual ObliviousField operator / (ObliviousField const &f) const;

        // comparison operators
        virtual bool operator==(const ObliviousField& rhs) const;
        virtual bool operator!=(const ObliviousField& rhs) const;
        virtual bool operator<=(const ObliviousField& rhs) const;
        virtual bool operator>=(const ObliviousField& rhs) const;
        virtual bool operator>(const ObliviousField& rhs) const;
        virtual bool operator<(const ObliviousField& rhs) const;

        virtual bool isEqTo(const ObliviousField &rhs, size_vdb genLevel, bool &actualResult);
        virtual bool isLEThan(const ObliviousField &rhs, size_vdb genLevel, bool &actualResult);
        virtual bool isGEThan(const ObliviousField &rhs, size_vdb genLevel, bool &actualResult);
        virtual bool isGThan(const ObliviousField &rhs, size_vdb genLevel, bool &actualResult);
        virtual bool isLThan(const ObliviousField &rhs, size_vdb genLevel, bool &actualResult);
        virtual bool isGThan(const ObliviousField &rhs, size_vdb genLevel, bool isNegating, bool &actualResult);
        virtual bool isLThan(const ObliviousField &rhs, size_vdb genLevel, bool isNegating, bool &actualResult);

        virtual int comparesTo(const ObliviousField &rhs, size_vdb genLevel);

        virtual HashNumber hash();
        virtual std::vector<unsigned char> convertToUnsignedCharVector() const;
    protected:
        ObliviousField();
        ObliviousField(BaseConstructor);
    private:
        void redefine(ObliviousFieldPtr fp);

        // algebraic functions implementations
        virtual ObliviousField ObliviousIntFieldAdd(ObliviousIntField const &f) const;
        virtual ObliviousField ObliviousFixcharFieldAdd(ObliviousFixcharField const &f) const;
        virtual ObliviousField ObliviousTimestampNoZoneFieldAdd(ObliviousTimestampNoZoneField const &f) const;
        virtual ObliviousField ObliviousIntFieldSub(ObliviousIntField const &f) const;
        virtual ObliviousField ObliviousFixcharFieldSub(ObliviousFixcharField const &f) const;
        virtual ObliviousField ObliviousTimestampNoZoneFieldSub(ObliviousTimestampNoZoneField const &f) const;
        virtual ObliviousField ObliviousIntFieldMul(ObliviousIntField const &f) const;
        virtual ObliviousField ObliviousFixcharFieldMul(ObliviousFixcharField const &f) const;
        virtual ObliviousField ObliviousTimestampNoZoneFieldMul(ObliviousTimestampNoZoneField const &f) const;
        virtual ObliviousField ObliviousIntFieldDiv(ObliviousIntField const &f) const;
        virtual ObliviousField ObliviousFixcharFieldDiv(ObliviousFixcharField const &f) const;
        virtual ObliviousField ObliviousTimestampNoZoneFieldDiv(ObliviousTimestampNoZoneField const &f) const;

        // comparison operators implementations
        virtual bool ObliviousIntFieldEq(ObliviousIntField const &f, size_vdb genLevel, bool &actualResult) const;
        virtual bool ObliviousFixcharFieldEq(ObliviousFixcharField const &f, size_vdb genLevel, bool &actualResult) const;
        virtual bool
        ObliviousTimestampNoZoneFieldEq(ObliviousTimestampNoZoneField const &f, size_vdb genLevel, bool &actualResult) const;
        virtual bool
        ObliviousIntFieldLt(ObliviousIntField const &f, size_vdb genLevel, bool isNegating, bool &actualResult) const;
        virtual bool ObliviousFixcharFieldLt(ObliviousFixcharField const &f, size_vdb genLevel, bool isNegating, bool &actualResult) const;
        virtual bool
        ObliviousTimestampNoZoneFieldLt(ObliviousTimestampNoZoneField const &f, size_vdb genLevel, bool isNegating,
                                                bool &actualResult) const;
        virtual bool
        ObliviousIntFieldGt(ObliviousIntField const &f, size_vdb genLevel, bool isNegating, bool &actualResult) const;
        virtual bool ObliviousFixcharFieldGt(ObliviousFixcharField const &f, size_vdb genLevel, bool isNegating, bool &actualResult) const;
        virtual bool
        ObliviousTimestampNoZoneFieldGt(ObliviousTimestampNoZoneField const &f, size_vdb genLevel, bool isNegating,
                                                bool &actualResult) const;

        virtual int ObliviousIntFieldComparesTo(ObliviousIntField const &f, size_vdb genLevel) const;
        virtual int ObliviousFixcharFieldComparesTo(ObliviousFixcharField const &f, size_vdb genLevel) const;
        virtual int ObliviousTimestampNoZoneFieldComparesTo(ObliviousTimestampNoZoneField const &f, size_vdb genLevel) const;

        ObliviousFieldPtr rep;
    };

}
}
}

#endif //VAULTDB_OBLIVIOUSFIELD_H
