#ifndef VAULTDB_OBLIVIOUSVARCHARFIELD_H
#define VAULTDB_OBLIVIOUSVARCHARFIELD_H

#include <shared/type/RecordSchema.h>
#include "ObliviousField.h"
#include "ObliviousFieldPrototype.h"

namespace db{
namespace obdata {
namespace obfield {

    class ObliviousFixcharField : public ObliviousFieldPrototype
    {
        friend class ObliviousField;
    public:
        ObliviousFixcharField(size_vdb capacity);
        ObliviousFixcharField(const char * s, size_vdb capacity);
        ObliviousFixcharField(const ObliviousFixcharField &f, size_vdb capacity);
        virtual ~ObliviousFixcharField();

        // meta functions
        virtual bool isTruthy() const override;
        type::FieldDataType getType() override;
        std::string toString() const override;
        void serializeTo(unsigned char * buf, size_vdb capacity) const override;
        static ObliviousField deserializeFrom(const unsigned char * data, size_vdb capacity);

        // algebraic functions
        virtual ObliviousField operator + (ObliviousField const &f) const override;
        virtual ObliviousField operator - (ObliviousField const &f) const override;
        virtual ObliviousField operator * (ObliviousField const &f) const override;
        virtual ObliviousField operator / (ObliviousField const &f) const override;

        // comparison operators
        virtual bool operator==(const ObliviousField& rhs) const override;
        virtual bool operator!=(const ObliviousField& rhs) const override;
        virtual bool operator<=(const ObliviousField& rhs) const override;
        virtual bool operator>=(const ObliviousField& rhs) const override;
        virtual bool operator>(const ObliviousField& rhs) const override;
        virtual bool operator<(const ObliviousField& rhs) const override;

        virtual bool isEqTo(const ObliviousField &rhs, size_vdb genLevel, bool &actualResult) override;
        virtual bool isLEThan(const ObliviousField &rhs, size_vdb genLevel, bool &actualResult) override;
        virtual bool isGEThan(const ObliviousField &rhs, size_vdb genLevel, bool &actualResult) override;
        virtual bool isGThan(const ObliviousField &rhs, size_vdb genLevel, bool isNegating, bool &actualResult) override;
        virtual bool isLThan(const ObliviousField &rhs, size_vdb genLevel, bool isNegating, bool &actualResult) override;

        virtual int comparesTo(const ObliviousField &rhs, size_vdb genLevel) override;
        // algebraic functions implementations

        // comparison operators implementations
        bool ObliviousFixcharFieldEq(ObliviousFixcharField const &f, size_vdb genLevel, bool &actualResult) const override;
        bool ObliviousFixcharFieldLt(ObliviousFixcharField const &f, size_vdb genLevel, bool isNegating, bool &actualResult) const override;
        bool ObliviousFixcharFieldGt(ObliviousFixcharField const &f, size_vdb genLevel, bool isNegating, bool &actualResult) const override;

        virtual int ObliviousFixcharFieldComparesTo(ObliviousFixcharField const &f, size_vdb genLevel) const override;

        virtual HashNumber hash() override;
        virtual std::vector<unsigned char> convertToUnsignedCharVector() const override;


        char* value;
    };
}
}
}

#endif //VAULTDB_OBLIVIOUSVARCHARFIELD_H
