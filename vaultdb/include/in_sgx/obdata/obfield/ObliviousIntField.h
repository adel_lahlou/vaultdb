#ifndef VAULTDB_OBLIVIOUSINTFIELD_H
#define VAULTDB_OBLIVIOUSINTFIELD_H

#include "ObliviousField.h"
#include "ObliviousFieldPrototype.h"

namespace db{
namespace obdata {
namespace obfield {

    class ObliviousIntField : public ObliviousFieldPrototype
    {
        friend class ObliviousField;
    public:
        ObliviousIntField();
        ObliviousIntField(int v);
        ObliviousIntField(const ObliviousIntField &f);
        virtual ~ObliviousIntField();

        // meta functions
        virtual bool isTruthy() const override;
        type::FieldDataType getType() override;
        std::string toString() const override;
        void serializeTo(unsigned char * buf, size_vdb capacity) const override;
        static ObliviousField deserializeFrom(const unsigned char * data);

        // algebraic functions
        virtual ObliviousField operator + (ObliviousField const &f) const override;
        virtual ObliviousField operator - (ObliviousField const &f) const override;
        virtual ObliviousField operator * (ObliviousField const &f) const override;
        virtual ObliviousField operator / (ObliviousField const &f) const override;

        // comparison operators
        virtual bool operator==(const ObliviousField& rhs) const override;
        virtual bool operator!=(const ObliviousField& rhs) const override;
        virtual bool operator<=(const ObliviousField& rhs) const override;
        virtual bool operator>=(const ObliviousField& rhs) const override;
        virtual bool operator>(const ObliviousField& rhs) const override;
        virtual bool operator<(const ObliviousField& rhs) const override;

        virtual bool isEqTo(const ObliviousField &rhs, size_vdb genLevel, bool &actualResult) override;
        virtual bool isLEThan(const ObliviousField &rhs, size_vdb genLevel, bool &actualResult) override;
        virtual bool isGEThan(const ObliviousField &rhs, size_vdb genLevel, bool &actualResult) override;
        virtual bool isGThan(const ObliviousField &rhs, size_vdb genLevel, bool isNegating, bool &actualResult) override;
        virtual bool isLThan(const ObliviousField &rhs, size_vdb genLevel, bool isNegating, bool &actualResult) override;

        virtual int comparesTo(const ObliviousField &rhs, size_vdb genLevel) override;

        // algebraic functions implementations
        virtual ObliviousField ObliviousIntFieldAdd(ObliviousIntField const &f) const override;
        virtual ObliviousField ObliviousIntFieldSub(ObliviousIntField const &f) const override;
        virtual ObliviousField ObliviousIntFieldMul(ObliviousIntField const &f) const override;
        virtual ObliviousField ObliviousIntFieldDiv(ObliviousIntField const &f) const override;
        virtual ObliviousField ObliviousTimestampNoZoneFieldAdd(ObliviousTimestampNoZoneField const &f) const override;
        virtual ObliviousField ObliviousTimestampNoZoneFieldSub(ObliviousTimestampNoZoneField const &f) const override;
        virtual ObliviousField ObliviousTimestampNoZoneFieldMul(ObliviousTimestampNoZoneField const &f) const override;
        virtual ObliviousField ObliviousTimestampNoZoneFieldDiv(ObliviousTimestampNoZoneField const &f) const override;

        // comparison operators implementations
        virtual bool ObliviousIntFieldEq(ObliviousIntField const &f, size_vdb genLevel, bool &actualResult) const override;
        virtual bool
        ObliviousIntFieldLt(ObliviousIntField const &f, size_vdb genLevel, bool isNegating, bool &actualResult) const override;
        virtual bool
        ObliviousIntFieldGt(ObliviousIntField const &f, size_vdb genLevel, bool isNegating, bool &actualResult) const override;

        virtual int ObliviousIntFieldComparesTo(ObliviousIntField const &f, size_vdb genLevel) const override;

        virtual HashNumber hash() override;
        virtual std::vector<unsigned char> convertToUnsignedCharVector() const override;


        int value;
    };
}
}
}

#endif //VAULTDB_OBLIVIOUSINTFIELD_H
