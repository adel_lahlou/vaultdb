#ifndef VAULTDB_OBLIVIOUSFIELDBASE_H
#define VAULTDB_OBLIVIOUSFIELDBASE_H

#include "ObliviousField.h"

namespace db {
    namespace obdata {
        namespace obfield {

            class ObliviousFieldPrototype : public ObliviousField {
            protected:
                virtual ObliviousField ObliviousIntFieldAdd(ObliviousIntField const &f) const;
                virtual ObliviousField ObliviousFixcharFieldAdd(ObliviousFixcharField const &f) const;
                virtual ObliviousField ObliviousTimestampNoZoneFieldAdd(ObliviousTimestampNoZoneField const &f) const;
                virtual ObliviousField ObliviousIntFieldSub(ObliviousIntField const &f) const;
                virtual ObliviousField ObliviousFixcharFieldSub(ObliviousFixcharField const &f) const;
                virtual ObliviousField ObliviousTimestampNoZoneFieldSub(ObliviousTimestampNoZoneField const &f) const;
                virtual ObliviousField ObliviousIntFieldMul(ObliviousIntField const &f) const;
                virtual ObliviousField ObliviousFixcharFieldMul(ObliviousFixcharField const &f) const;
                virtual ObliviousField ObliviousTimestampNoZoneFieldMul(ObliviousTimestampNoZoneField const &f) const;
                virtual ObliviousField ObliviousIntFieldDiv(ObliviousIntField const &f) const;
                virtual ObliviousField ObliviousFixcharFieldDiv(ObliviousFixcharField const &f) const;
                virtual ObliviousField ObliviousTimestampNoZoneFieldDiv(ObliviousTimestampNoZoneField const &f) const;

                // comparison operators implementations
                virtual bool ObliviousIntFieldEq(ObliviousIntField const &f, size_vdb genLevel, bool &actualResult) const;
                virtual bool
                ObliviousFixcharFieldEq(ObliviousFixcharField const &f, size_vdb genLevel, bool &actualResult) const;
                virtual bool ObliviousTimestampNoZoneFieldEq(ObliviousTimestampNoZoneField const &f, size_vdb genLevel,
                                                             bool &actualResult) const;
                virtual bool ObliviousIntFieldLt(ObliviousIntField const &f, size_vdb genLevel, bool isNegating, bool &actualResult) const;
                virtual bool
                ObliviousFixcharFieldLt(ObliviousFixcharField const &f, size_vdb genLevel, bool isNegating, bool &actualResult) const;
                virtual bool
                ObliviousTimestampNoZoneFieldLt(ObliviousTimestampNoZoneField const &f, size_vdb genLevel, bool isNegating,
                                                                bool &actualResult) const;
                virtual bool ObliviousIntFieldGt(ObliviousIntField const &f, size_vdb genLevel, bool isNegating, bool &actualResult) const;
                virtual bool
                ObliviousFixcharFieldGt(ObliviousFixcharField const &f, size_vdb genLevel, bool isNegating, bool &actualResult) const;
                virtual bool
                ObliviousTimestampNoZoneFieldGt(ObliviousTimestampNoZoneField const &f, size_vdb genLevel, bool isNegating,
                                                                bool &actualResult) const;
            };
        }
    }
}

#endif //VAULTDB_OBLIVIOUSFIELDBASE_H
