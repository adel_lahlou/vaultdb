#ifndef VAULTDB_PROJ_KANONYMOUSGENERALIZE_H
#define VAULTDB_PROJ_KANONYMOUSGENERALIZE_H

#include <shared/Definitions.h>
#include <in_sgx/obdata/ObliviousTupleTable.h>
#include <in_sgx/utilities/InSGXController.h>

namespace plan {
namespace kaoperators {
    class KAnonymousGeneralizer {
    public:
        KAnonymousGeneralizer(TableID tempTableID, size_vdb k, type::RecordSchema inputSchema, db::obdata::ObliviousTupleTable * inputTable,
                             std::vector<pos_vdb> control,
                             std::vector<pos_vdb> entities,
                             utilities::InSGXController * controller);
        ~KAnonymousGeneralizer();
        std::vector<size_vdb> computeAndGetGeneralizationLevel();

    private:
        std::vector<std::vector<size_vdb>> produceDummyGeneralizationLattice(std::vector<pos_vdb> comparisons);
        void computeGenLevelNoEntity(db::obdata::ObliviousTupleTable *outputTable);
        void
        computeGenLevelWithEntity(db::obdata::ObliviousTupleTable *outputTable, std::vector<pos_vdb> controlAndEntity);
        void deriveGenLevelNoEntity(db::obdata::ObliviousTupleTable *outputTable);
        void deriveGenLevelDisjointCase(db::obdata::ObliviousTupleTable *outputTable);
        void deriveGenLevelIntersectCase(db::obdata::ObliviousTupleTable *outputTable);
        void deriveGeneralizationLevels();

        std::vector<pos_vdb> _controlFlowAttributes;
        std::vector<pos_vdb> _entityIdentifiers;
        std::vector<size_vdb> _generalizationLevel;
        size_vdb _k;

        db::obdata::ObliviousTupleTable * _inputTable;

        type::RecordSchema _outSchema;
        TableID _tableID;
        MachineID _localMachineID;
        utilities::InSGXController * _controller;
    };
}
}


#endif //VAULTDB_PROJ_KANONYMOUSGENERALIZE_H
