#ifndef VAULTDB_PROJ_KANONYMOUSPULLANDBIN_H
#define VAULTDB_PROJ_KANONYMOUSPULLANDBIN_H


#include <shared/Definitions.h>
#include <in_sgx/obdata/ObliviousTupleTable.h>
#include <map>
#include <in_sgx/utilities/DummyInSGXHashPartitioner.h>
#include "in_sgx/plan/kaoperators/KAnonymousOperator.h"

namespace plan {
    namespace kaoperators {
        class KAnonymousPullAndBin {
        public:
            KAnonymousPullAndBin(TableID operatorID, KAnonymousOperator *input, size_vdb k, std::vector<pos_vdb> groupBys,
                                             bool createBinOnDemand, utilities::InSGXController *controller);
            ~KAnonymousPullAndBin();
            void setInput(KAnonymousOperator *input);
            void pullAndBin();
            bool hasMore();
            db::obdata::ObliviousTupleTable * get();
            std::vector<pos_vdb> getGroupByColumns() const;
        private:
            std::vector<pos_vdb> _groupBys;
            std::vector<db::obdata::ObliviousTupleTable *> _binHolder;
            std::map<HashNumber, pos_vdb> _binTranslator;

            KAnonymousOperator * _child;
            TableID _tableID;
            bool _isFetched;
            bool _createBinOnDemand;
            size_vdb _k;

            std::vector<db::obdata::ObliviousTupleTable *>::iterator _binIterator;
            utilities::InSGXController * _controller;
        };
    }
}

#endif //VAULTDB_PROJ_KANONYMOUSPULLANDBIN_H
