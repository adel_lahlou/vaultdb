#ifndef VAULTDB_BITONICSORT_H
#define VAULTDB_BITONICSORT_H

#include <string>
#include "ObliviousOperator.h"
#include "in_sgx/obdata/ObliviousTupleTable.h"
#include "shared/type/RecordSchema.h"
#include "in_sgx/obdata/ObliviousTuple.h"
#include "shared/Definitions.h"

using namespace db::obdata;

namespace plan {
namespace oboperators {

    class BitonicSort : public ObliviousOperator {
        friend class ClusterObliviousBitonicMergeSort;
    public:
        BitonicSort(TableID opId,
                ObliviousOperatorPtr child,
                std::vector<pos_vdb> orderByEntries,
                std::vector<SortOrder> sortOrders
        );
        ~BitonicSort() override;
        void reset() override;
        std::vector<pos_vdb> getOrderColumns();

        OperatorStatus next() override;
    protected:
        size_vdb getSerializationOverHead(std::set<TableID> &baseTables) const override;
        void encodeOperator(unsigned char *&writeHead) const override;
    private:
        ObliviousTupleList _tupleBuffer;
        std::vector<pos_vdb> _orderByEntries;
        std::vector<SortOrder> _sortOrders;

        int _outputPos;

        void sort(
                const type::RecordSchema &schema,
                ObliviousTupleList &inputBuffer,
                ObliviousTupleList &outputBuffer,
                std::vector<pos_vdb> &orderByOrdinals,
                std::vector<SortOrder> &sortingOrders);
        void merge(
                const type::RecordSchema &schema,
                ObliviousTupleList &concat,
                ObliviousTupleList &outputBuffer,
                std::vector<pos_vdb> &orderByOrdinals,
                std::vector<SortOrder> &sortingOrders);
        void compare(
                const type::RecordSchema &schema,
                ObliviousTupleList &concat,
                std::vector<pos_vdb> &orderByOrdinals,
                std::vector<SortOrder> &sortingOrders);
        int compareAgainstPseudoOblivious(
                const type::RecordSchema& schema,
                db::obdata::ObliviousTuple left,
                db::obdata::ObliviousTuple right,
                std::vector<pos_vdb>& orderByOrdinals,
                std::vector<SortOrder>& sortingOrders);
    };
}
}

#endif //VAULTDB_BITONICSORT_H
