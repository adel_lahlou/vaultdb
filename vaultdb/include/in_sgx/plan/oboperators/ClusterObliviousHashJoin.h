#ifndef VAULTDB_CLUSTEROBLIVIOUSHASHJOIN_H
#define VAULTDB_CLUSTEROBLIVIOUSHASHJOIN_H

#include <in_sgx/plan/clusterops/ClusterOperator.h>
#include "ObliviousOperator.h"
#include "in_sgx/obdata/ObliviousTuple.h"
#include "shared/type/RecordSchema.h"
#include "in_sgx/plan/obexpressions/ObliviousExpression.h"
#include "in_sgx/utilities/InSGXController.h"
#include "in_sgx/obdata/ObliviousTupleTable.h"
#include "in_sgx/serialization/ObliviousSerializer.h"
#include "ClusterObliviousHashing.h"
#include <mutex>

#ifdef CLUSTER_OPERATOR_DEBUG
#ifdef CLUSTER_OB_HASH_JOIN_DEBUG
//#define CLUSTER_OB_HASH_JOIN_DEBUG
#endif
#endif

namespace plan {
    namespace oboperators {

        class ClusterObliviousHashJoin : public ObliviousOperator, public virtual ClusterOperator {

        public:
            ClusterObliviousHashJoin(MachineID machineId, TableID opId,
                                                 TransmitterID leftTransmitterID,
                                                 TransmitterID rightTransmitterID, ObliviousOperatorPtr leftChild,
                                                 ObliviousOperatorPtr rightChild,
                                                 std::vector<pos_vdb> leftJoinAttributes,
                                                 std::vector<pos_vdb> rightJoinAttributes,
                                                 utilities::InSGXController *dispatcher);
            ~ClusterObliviousHashJoin() override;
            OperatorStatus next() override;
            void reset() override;

            OperatorStatus
            receiveCallBack(MachineID src_machine_id, TableID src_tid, db::obdata::ObliviousTupleTable *table) override;
        private:
            // innate properties
            std::vector<pos_vdb> _leftAttributes;
            std::vector<pos_vdb> _rightAttributes;
        };

    }
}


#endif //VAULTDB_CLUSTEROBLIVIOUSHASHJOIN_H
