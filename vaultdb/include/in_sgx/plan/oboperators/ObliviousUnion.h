#ifndef VAULTDB_OBLIVIOUSUNION_H
#define VAULTDB_OBLIVIOUSUNION_H


#include <string>
#include "ObliviousOperator.h"

namespace plan {
namespace oboperators {
    class ObliviousUnion : public ObliviousOperator {
    public:
        ObliviousUnion(TableID opId, ObliviousOperatorPtrList children);
        ~ObliviousUnion();

        OperatorStatus next();
        void reset();
    private:
        ObliviousOperatorPtr getCurrentChild() const;
        int _curChild = 0;
    };
}
}

#endif //VAULTDB_OBLIVIOUSUNION_H
