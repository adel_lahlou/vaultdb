#ifndef VAULTDB_OBLIVIOUSOPERATOR_H
#define VAULTDB_OBLIVIOUSOPERATOR_H

#include <string>
#include <vector>
#include <map>
#include <set>
#include <in_sgx/obdata/ObliviousTupleTable.h>
#include "in_sgx/obdata/ObliviousTuple.h"
#include "shared/type/RecordSchema.h"

namespace plan {
namespace oboperators {

    class ObliviousOperator;

    struct BaseConstructor { BaseConstructor(int=0) {} };
    typedef ObliviousOperator * ObliviousOperatorPtr;
    typedef std::vector<ObliviousOperatorPtr> ObliviousOperatorPtrList;
    typedef std::map<MachineID, db::obdata::ObliviousTupleTable *> MachineTablePtrMap;

    class ObliviousOperator {
    public:
        ObliviousOperator(
            TableID opId,
            type::RecordSchema outSchema,
            bool blocking,
            ObliviousOperatorPtrList children);
        virtual ~ObliviousOperator();

        const OperatorStatus getStatus() const;
        const ObliviousOperator* getChild(size_vdb child) const;
        const std::vector<ObliviousOperator*>& getChildren() const;
        const type::RecordSchema& getChildSchema(int child) const;
        const type::RecordSchema& getSchema() const;
        const TableID getOutputTableID() const;
        const ObliviousOperator* getParent() const;
        bool isBlocking() const;

        OperatorStatus getCurrentTuple(db::obdata::ObliviousTuple& out) const;

        void setOutputTableID(TableID id);
        void setParent(ObliviousOperator* p);
        virtual OperatorStatus next()=0;
        virtual void reset();

        virtual size_vdb getSerializationOverHead(std::set<TableID> &baseTables) const;

        virtual void encodeOperator(unsigned char *&writeHead) const;
        static unsigned char * EncodeQueryToNewArray(ObliviousOperator * root, size_vdb & encodingSize);
    protected:
        ObliviousOperator(const ObliviousOperator& o);
        ObliviousOperator();

        ObliviousOperatorPtr _parent;
        std::vector<ObliviousOperator*> _children;
        type::RecordSchema _outSchema;
        OperatorStatus _status;
        TableID _operatorId;

        bool _blocking;
        db::obdata::ObliviousTuple _currentTuple;
    };
}
}
#endif //VAULTDB_OBLIVIOUSOPERATOR_H
