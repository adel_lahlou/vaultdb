#ifndef VAULTDB_CLUSTEROBLIVIOUSHASHSORTEDAGGREGATION_H
#define VAULTDB_CLUSTEROBLIVIOUSHASHSORTEDAGGREGATION_H

#include <mutex>
#include <in_sgx/plan/obexpressions/ObliviousExpressionList.h>
#include "in_sgx/plan/clusterops/ClusterOperator.h"
#include "in_sgx/plan/oboperators/ObliviousOperator.h"
#include "in_sgx/utilities/InSGXController.h"

#ifdef CLUSTER_OPERATOR_DEBUG
#ifndef CLUSTER_OB_SORTED_AGGREGATION_DEBUG
//#define CLUSTER_OB_SORTED_AGGREGATION_DEBUG
#endif
#endif

namespace plan {
    namespace oboperators {
        class ClusterObliviousHashSortedAggregation  : public ObliviousOperator, public virtual ClusterOperator {
        public:
            ClusterObliviousHashSortedAggregation(MachineID machineId, TableID opId,
                                                              TransmitterID transmitterID,
                                                              ObliviousOperatorPtr child,
                                                              obexpressions::ObliviousExpressionList exps,
                                                              std::vector<pos_vdb> groupBys,
                                                              utilities::InSGXController *dispatcher);
            ~ClusterObliviousHashSortedAggregation() override;
            OperatorStatus next() override;
            void reset() override;

            OperatorStatus
            receiveCallBack(MachineID src_machine_id, TableID src_tid, db::obdata::ObliviousTupleTable *table) override;
        };
    }
}


#endif //VAULTDB_CLUSTEROBLIVIOUSHASHSORTEDAGGREGATION_H
