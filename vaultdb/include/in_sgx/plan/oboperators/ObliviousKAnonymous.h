#ifndef VAULTDB_OBLIVIOUSKANONYMOUS_H
#define VAULTDB_OBLIVIOUSKANONYMOUS_H

#include <in_sgx/plan/kaoperators/KAnonymousOperator.h>
#include <in_sgx/plan/obexpressions/ObliviousExpression.h>
#include "ObliviousOperator.h"

namespace plan {
    namespace oboperators {
        class ObliviousKAnonymous : public ObliviousOperator {
        public:
            ObliviousKAnonymous(plan::kaoperators::KAnonymousOperatorPtr child);
            ~ObliviousKAnonymous();

            OperatorStatus next() override;
            void reset() override;

            void encodeOperator(unsigned char *&writeHead) const override;
        private:
            obexpressions::ObliviousTupleBox _projectTuple = new db::obdata::ObliviousTuple();
            plan::kaoperators::KAnonymousOperatorPtr _child;
        };
    }}

#endif //VAULTDB_OBLIVIOUSKANONYMOUS_H
