#ifndef VAULTDB_OBLIVIOUSSEQSCAN_H
#define VAULTDB_OBLIVIOUSSEQSCAN_H

#include <string>
#include "ObliviousOperator.h"
#include "in_sgx/obdata/ObliviousTupleTable.h"
#include "shared/type/RecordSchema.h"

namespace plan {
namespace oboperators {
    class ObliviousSeqScan : public ObliviousOperator {
    public:
        ObliviousSeqScan(TableID opId, db::obdata::ObliviousTupleTable * inputTable);
        ~ObliviousSeqScan();

        OperatorStatus next() override;
        void reset() override;
    protected:
        size_vdb getSerializationOverHead(std::set<TableID> &baseTables) const override;
        void encodeOperator(unsigned char *&writeHead) const override;
    private:
        size_vdb _scanPos = 0;
        db::obdata::ObliviousTupleTable * _inputTable;
    };
}
}

#endif // VAULTDB_OBLIVIOUSSEQSCAN_H
