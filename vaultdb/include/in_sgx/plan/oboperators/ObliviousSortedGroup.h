#ifndef VAULTDB_OBLIVIOUSSORTEDGROUP_H
#define VAULTDB_OBLIVIOUSSORTEDGROUP_H

#include <string>
#include "ObliviousOperator.h"
#include "in_sgx/obdata/ObliviousTupleTable.h"
#include "shared/type/RecordSchema.h"
#include "in_sgx/plan/obexpressions/ObliviousExpression.h"
#include "in_sgx/plan/obexpressions/AggregateObliviousExpressionList.h"
#include "in_sgx/plan/obexpressions/ObliviousExpressionList.h"

namespace plan {
namespace oboperators {

    class ObliviousSortedGroup : public ObliviousOperator {
    public:
        ObliviousSortedGroup(
                TableID opId,
                ObliviousOperatorPtr child,
                obexpressions::ObliviousExpressionList exps,
                std::vector<pos_vdb> groupBys
        );
        ~ObliviousSortedGroup();

        OperatorStatus next() override;
        void reset() override;
    protected:
        size_vdb getSerializationOverHead(std::set<TableID> &baseTables) const override;
        void encodeOperator(unsigned char *&writeHead) const override;
    private:
        void _nextMiddle();
        void _nextEnd();
        void _evaluate();

        bool _isCurrentGroupAllDummy;

        obexpressions::ObliviousTupleBox _lastAggTuple;
        obexpressions::ObliviousTupleBox _curAggTuple;
        obexpressions::AggregateObliviousExpressionList _aggs;
        obexpressions::ObliviousExpressionList _exps;
        obexpressions::ObliviousExpressionList _checkInGroup;
        bool _firstPull = true;
        bool _finishedPulling = false;
    };
}
}


#endif //VAULTDB_OBLIVIOUSSORTEDGROUP_H
