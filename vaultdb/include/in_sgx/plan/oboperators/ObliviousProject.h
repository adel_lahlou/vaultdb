#ifndef VAULTDB_OBLIVIOUSPROJECT_H
#define VAULTDB_OBLIVIOUSPROJECT_H

#include <string>
#include <in_sgx/plan/obexpressions/ObliviousExpression.h>
#include "ObliviousOperator.h"
#include "in_sgx/plan/obexpressions/ObliviousExpressionList.h"

namespace type {
    class RecordSchema;
}

namespace plan {
namespace oboperators {
    class ObliviousProject : public ObliviousOperator {
    public:
        ObliviousProject(TableID opId,
                ObliviousOperatorPtr child,
                plan::obexpressions::ObliviousExpressionList exps
        );
        ~ObliviousProject();

        OperatorStatus next() override;
        void reset() override;
    protected:
        size_vdb getSerializationOverHead(std::set<TableID> &baseTables) const override;
        void encodeOperator(unsigned char *&writeHead) const override;
    private:
        obexpressions::ObliviousTupleBox _projectTuple;
        plan::obexpressions::ObliviousExpressionList _exps;
    };
}
}

#endif //VAULTDB_OBLIVIOUSPROJECT_H
