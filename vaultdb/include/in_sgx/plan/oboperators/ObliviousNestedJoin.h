#ifndef VAULTDB_OBLIVIOUSJOIN_H
#define VAULTDB_OBLIVIOUSJOIN_H

#include <string>
#include <in_sgx/plan/obexpressions/obexpressions.h>
#include "ObliviousOperator.h"
#include "in_sgx/obdata/ObliviousTuple.h"
#include "shared/type/RecordSchema.h"

namespace plan {
    namespace oboperators {

        class ObliviousNestedJoin : public ObliviousOperator {
        public:
            ObliviousNestedJoin(TableID opId,
                       ObliviousOperatorPtr leftChild,
                       ObliviousOperatorPtr rightChild,
                       plan::obexpressions::JoinObliviousExpression joinPred
            );
            ~ObliviousNestedJoin();

            ObliviousOperatorPtr getLeftChild() const;
            ObliviousOperatorPtr getRightChild() const;

            OperatorStatus next();
            void reset();
        private:
            plan::obexpressions::ObliviousTupleBox _leftJoinTuple;
            plan::obexpressions::ObliviousTupleBox _rightJoinTuple;
            plan::obexpressions::JoinObliviousExpression _joinPred;
        };
    }
}

#endif //VAULTDB_OBLIVIOUSJOIN_H