#ifndef VAULTDB_CLUSTEROBLIVIOUSBITONICMERGESORT_H
#define VAULTDB_CLUSTEROBLIVIOUSBITONICMERGESORT_H

#include <in_sgx/plan/clusterops/ClusterOperator.h>
#include "ObliviousOperator.h"
#include "in_sgx/obdata/ObliviousTuple.h"
#include "shared/type/RecordSchema.h"
#include "in_sgx/plan/obexpressions/ObliviousExpression.h"
#include "in_sgx/utilities/InSGXController.h"
#include "in_sgx/obdata/ObliviousTupleTable.h"
#include "in_sgx/serialization/ObliviousSerializer.h"
#include <mutex>

#ifdef CLUSTER_OPERATOR_DEBUG
#ifndef CLUSTER_OB_MERGE_SORT_DEBUG
//#define CLUSTER_OB_MERGE_SORT_DEBUG
#endif
#endif

#define REGULAR_INIT_INDEX 0
#define POST_INIT_INIT_INDEX 1

namespace plan {
    namespace oboperators {
        class ClusterObliviousBitonicMergeSort : public ObliviousOperator, public virtual ClusterOperator {

        public:
            ClusterObliviousBitonicMergeSort(MachineID machineId, TableID opId,
                                                         TransmitterID transmitterID,
                                                         ObliviousOperatorPtr child,
                                                         std::vector<pos_vdb> orderByEntries,
                                                         std::vector<SortOrder> sortOrders,
                                                         utilities::InSGXController *dispatcher);
            ~ClusterObliviousBitonicMergeSort() override;
            OperatorStatus next() override;
            void reset() override;

            OperatorStatus
            receiveCallBack(MachineID src_machine_id, TableID src_tid, db::obdata::ObliviousTupleTable *table) override;
        private:
            size_vdb getTableCount();
            OperatorStatus dispatch(MachineID dst_machine_id, db::obdata::ObliviousTupleList & tupleList);
            void initializeIteratonTracker();
            OperatorStatus MergeForOneTuple();

            db::obdata::ObliviousTupleList _finalTupleBuffer;
            pos_vdb _pos; // Range: 1 ~ size. Actual reference requires _leftPos - 1
            std::vector<pos_vdb> _orderByEntries;
            std::vector<SortOrder> _sortOrders;

            // storage structures
            MachineTablePtrMap _tables;
            std::map<MachineID, pos_vdb> _tablesTracker;

            // overheads
            std::mutex _tablesMutex;
        };

    }
}


#endif //VAULTDB_CLUSTEROBLIVIOUSBITONICMERGESORT_H
