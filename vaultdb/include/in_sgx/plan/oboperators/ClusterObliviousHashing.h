#ifndef VAULTDB_CLUSTEROBLIVIOUSHASHING_H
#define VAULTDB_CLUSTEROBLIVIOUSHASHING_H

#include "in_sgx/plan/clusterops/ClusterOperator.h"
#include "in_sgx/plan/oboperators/ObliviousOperator.h"
#include "in_sgx/utilities/InSGXController.h"
#include <mutex>

#ifdef CLUSTER_OPERATOR_DEBUG
#ifdef CLUSTER_OB_HASHING_DEBUG
//#define CLUSTER_OB_HASHING_DEBUG
#endif
#endif

namespace plan {
    namespace oboperators {
        class ClusterObliviousHashing : public ObliviousOperator, public virtual ClusterOperator{
        public:
            ClusterObliviousHashing(MachineID machineId, TransmitterID transmitterID,
                                                ObliviousOperatorPtr child,
                                                std::vector<pos_vdb> partitionAttributes,
                                                utilities::InSGXController *dispatcher);
            ~ClusterObliviousHashing() override;
            OperatorStatus next() override;
            void reset() override;

            OperatorStatus
            receiveCallBack(MachineID src_machine_id, TableID src_tid, db::obdata::ObliviousTupleTable *table) override;
        private:
            OperatorStatus hashAndDispatch();
            size_vdb getTableCount();
            void reachForNonEmptyTable();
            OperatorStatus ensureTableIteratorReadyForUse();
            void findIterator();
            void findNextTuple();

            // innate properties
            std::vector<pos_vdb> _attributes;

            // storage structures
            MachineTablePtrMap _tables;

            // overheads
            db::obdata::ObliviousTupleTable * _tablePtr;
            bool _isTableIteratorInit;
            MachineTablePtrMap::iterator _tableIterator;
            pos_vdb _pos; // Range: 1 ~ size. Actual reference requires _leftPos - 1
            std::mutex _tablesMutex;
        };
    }
}

#endif //VAULTDB_OBLIVIOUSHASHING_H
