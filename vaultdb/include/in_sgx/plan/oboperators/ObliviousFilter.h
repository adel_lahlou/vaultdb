#ifndef VAULTDB_OBLIVIOUSFILTER_H
#define VAULTDB_OBLIVIOUSFILTER_H

#include <string>
#include "ObliviousOperator.h"
#include "shared/type/RecordSchema.h"
#include "in_sgx/plan/obexpressions/ObliviousExpression.h"

namespace plan {
namespace oboperators {
    class ObliviousFilter : public ObliviousOperator {
    public:
        ObliviousFilter(
                TableID opId,
                ObliviousOperatorPtr child,
                plan::obexpressions::ObliviousExpression filterPred
        );
        ~ObliviousFilter();

        OperatorStatus next() override;
        void reset() override;
    protected:
        size_vdb getSerializationOverHead(std::set<TableID> &baseTables) const override;
        void encodeOperator(unsigned char *&writeHead) const override;
    private:
        plan::obexpressions::ObliviousTupleBox _filterTuple;
        plan::obexpressions::ObliviousExpression _filterPred;
    };
}
}

#endif //VAULTDB_OBLIVIOUSFILTER_H
