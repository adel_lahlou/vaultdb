#ifndef VAULTDB_KANONYMOUSOPERATOR_H
#define VAULTDB_KANONYMOUSOPERATOR_H

#include <string>
#include <vector>
#include <map>
#include <set>
#include <in_sgx/obdata/ObliviousTupleTable.h>
#include "in_sgx/obdata/ObliviousTuple.h"
#include "shared/type/RecordSchema.h"

namespace plan {
namespace kaoperators {

    class KAnonymousOperator;

    struct BaseConstructor { BaseConstructor(int=0) {} };
    typedef KAnonymousOperator * KAnonymousOperatorPtr;
    typedef std::vector<KAnonymousOperatorPtr> KAnonymousOperatorPtrList;
    typedef std::map<MachineID, db::obdata::ObliviousTupleTable*> MachineTableMap;

    class KAnonymousOperator {
    public:
        static unsigned char * EncodeQueryToNewArray(KAnonymousOperator * root, size_vdb & encodingSize);
        KAnonymousOperator(
            TableID opId,
            type::RecordSchema outSchema,
            size_vdb k,
            bool blocking,
            KAnonymousOperatorPtrList children);
        virtual ~KAnonymousOperator();

        const OperatorStatus getStatus() const;
        const KAnonymousOperator* getChild(size_vdb child) const;
        const std::vector<KAnonymousOperator*>& getChildren() const;
        const type::RecordSchema& getChildSchema(int child) const;
        const type::RecordSchema& getSchema() const;
        const TableID getOutputTableID() const;
        const KAnonymousOperator* getParent() const;
        bool isBlocking() const;

        OperatorStatus getCurrentTuple(db::obdata::ObliviousTuple& out) const;

        void setOutputTableID(TableID id);
        void setParent(KAnonymousOperator* p);
        virtual OperatorStatus next()=0;
        virtual void reset();

        virtual size_vdb getGeneralizationLevel(pos_vdb index) const;

        virtual size_vdb getSerializationOverHead(std::set<TableID> &baseTables) const;
        virtual void encodeOperator(unsigned char *&writeHead) const;
    protected:
        KAnonymousOperator(const KAnonymousOperator& o);
        KAnonymousOperator();

        const size_vdb _k;
        size_vdb _countTowardK;

        KAnonymousOperatorPtr _parent;
        std::vector<KAnonymousOperator*> _children;
        type::RecordSchema _outSchema;
        OperatorStatus _status;
        TableID _operatorId;

        bool _blocking;
        db::obdata::ObliviousTuple _currentTuple;
    };
}
}
#endif //VAULTDB_KANONYMOUSOPERATOR_H
