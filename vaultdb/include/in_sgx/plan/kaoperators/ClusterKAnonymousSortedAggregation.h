#ifndef VAULTDB_CLUSTERKANONYMOUSHASHSORTEDAGGREGATION_H
#define VAULTDB_CLUSTERKANONYMOUSHASHSORTEDAGGREGATION_H

#include <mutex>
#include <in_sgx/plan/obexpressions/ObliviousExpressionList.h>
#include "in_sgx/plan/clusterops/ClusterOperator.h"
#include "KAnonymousOperator.h"
#include "in_sgx/utilities/InSGXController.h"

#ifdef CLUSTER_OPERATOR_DEBUG
#ifndef CLUSTER_OB_SORTED_AGGREGATION_DEBUG
//#define CLUSTER_OB_SORTED_AGGREGATION_DEBUG
#endif
#endif

namespace plan {
    namespace kaoperators {
        class ClusterKAnonymousSortedAggregation : public KAnonymousOperator, public virtual ClusterOperator {
        public:
            ClusterKAnonymousSortedAggregation(MachineID machineId, TableID opId,
                                                               TransmitterID transmissionID,
                                                               size_vdb k,
                                                               KAnonymousOperatorPtr child,
                                                               obexpressions::ObliviousExpressionList exps,
                                                               std::vector<pos_vdb> groupBys,
                                                               bool isRepartitionSorting,
                                                               utilities::InSGXController *dispatcher);
            ~ClusterKAnonymousSortedAggregation() override;
            OperatorStatus next() override;
            void reset() override;

            OperatorStatus
            receiveCallBack(MachineID src_machine_id, TableID src_tid, db::obdata::ObliviousTupleTable *table) override;
        protected:
            ClusterKAnonymousSortedAggregation(MachineID machineId, TableID opId,
                                                           size_vdb k, KAnonymousOperatorPtr child,
                                                           bool isRepartitionSorting,
                                                           utilities::InSGXController *dispatcher);
            size_vdb getSerializationOverHead(std::set<TableID> &baseTables) const override;
            void encodeOperator(unsigned char *&writeHead) const override;

            bool _isRepartitionSorting;
        };
    }
}


#endif //VAULTDB_CLUSTERKANONYMOUSHASHSORTEDAGGREGATION_H
