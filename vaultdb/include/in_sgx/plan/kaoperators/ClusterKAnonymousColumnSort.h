#ifndef VAULTDB_PROJ_CLUSTERKANONYMOUSCOLUMNSORT_H
#define VAULTDB_PROJ_CLUSTERKANONYMOUSCOLUMNSORT_H

#include <shared/Definitions.h>
#include <in_sgx/obdata/ObliviousTupleTable.h>
#include <in_sgx/utilities/InSGXController.h>

namespace plan {
namespace kaoperators {
    class ClusterKAnonymousColumnSort : public KAnonymousOperator, public virtual ClusterOperator {
        /**
         * Parameters
         * 1. r rows
         * 2. s columns
         * 3. b size of block
         *
         * Requirements
         * 1. r % 2 == 0
         * 2. r % s == 0
         * 3. r >= 2 * s * s
         * 4. b is power of 2
         *
         * Steps
         * 0. Ensure that each of the s hosts have r blocks with size b; pad if needed
         * 1. Sort the local column
         * 2. transposes the mesh and then reshapes the resulting s × r matrix back into an r × s mesh
         *    by taking each row of r entries and mapping it to a consecutive set of r/s rows.
         * 3. Sort the local column (same as 1)
         * 4. performs the inverse of the permutation performed in step 2: treat each consecutive set of r/s
         *    rows as a single row in an s × r matrix, and transpose this matrix into the r × s mesh.
         * 5. Sort the local column (same as 1)
         * 6. shifts each column down by r/2 positions. That is, the bottom half of each column moves to
         *    the top half of the next column to the right, and the top half of each column moves to the bottom half
         *    of the column. The evacuated top half of the leftmost column (column 0) is filled with the value −inf.
         *    A new rightmost column (column s) is created, receiving the bottom half of column s − 1, and the
         *    bottom half of this new column is filled with the value inf
         * 7. Sort the local column (same as 1)
         * 8. performs the inverse of the permutation performed in step 6: shift each column up by r/2 positions
         */
    public:
        ClusterKAnonymousColumnSort(MachineID machineId, TableID opId, TableID transmitterID,
                                            size_vdb k, KAnonymousOperatorPtr child,
                                            std::vector<pos_vdb> orderByEntries,
                                            std::vector<SortOrder> sortOrders,
                                            size_vdb blockTupleCount,
                                            utilities::InSGXController *dispatcher);
        ~ClusterKAnonymousColumnSort() override;
        OperatorStatus next() override;
        void reset() override;
        OperatorStatus
        receiveCallBack(MachineID src_machine_id, TableID src_tid, db::obdata::ObliviousTupleTable *table) override;
        OperatorStatus
        receiveGenericMessage(MachineID src_machine_id, TableID src_tid, unsigned char *data, size_vdb msg_size) override;
    protected:
        size_vdb getSerializationOverHead(std::set<TableID> &baseTables) const override;
        void encodeOperator(unsigned char *&writeHead) const override;
    private:
        void init();
        db::obdata::ObliviousTupleTable * mergePartitionToFirst(pos_vdb start_pos, pos_vdb last_pos_plus_1);
        void sortLocalTuples(pos_vdb start_pos, pos_vdb last_pos_plus_1);
        size_vdb getReceivedTableCount();
        size_vdb getReceivedR();
        void determineR(size_vdb currentPartitionCount);

        void step2();
        void step4();
        void step6();
        void step8();

        size_vdb _r;
        size_vdb _tupleBlockSize;

        std::vector<pos_vdb> _orderByEntries;
        std::vector<SortOrder> _sortOrders;
        pos_vdb _pos; // Range: 1 ~ size. Actual reference requires _leftPos - 1

        db::obdata::ObliviousTupleTable * _finalTupleBuffer;
        type::RecordSchema _defaultTransmissionSchema;
        std::mutex _tablesMutex;

        size_vdb _partitionReceiveCount;
        size_vdb _expectedPartitionReceiveCount;
    };
}
}


#endif //VAULTDB_PROJ_CLUSTERKANONYMOUSCOLUMNSORT_H
