#ifndef VAULTDB_CLUSTERKABITONICMERGESORT_H
#define VAULTDB_CLUSTERKABITONICMERGESORT_H

#include <in_sgx/plan/clusterops/ClusterOperator.h>
#include "KAnonymousOperator.h"
#include "in_sgx/obdata/ObliviousTuple.h"
#include "shared/type/RecordSchema.h"
#include "in_sgx/plan/obexpressions/ObliviousExpression.h"
#include "in_sgx/utilities/InSGXController.h"
#include "in_sgx/serialization/ObliviousSerializer.h"
#include <mutex>

#ifdef CLUSTER_OPERATOR_DEBUG
#ifndef CLUSTER_KA_MERGE_SORT_DEBUG
//#define CLUSTER_KA_MERGE_SORT_DEBUG
#endif
#endif

#define REGULAR_INIT_INDEX 0
#define POST_INIT_INIT_INDEX 1

namespace db {
    namespace obdata {
        class ObliviousTupleTable;
    }
}

namespace plan {
    namespace kaoperators {

        class ClusterKAnonymousBitonicMergeSort : public KAnonymousOperator, public virtual ClusterOperator {

        public:
            ClusterKAnonymousBitonicMergeSort(MachineID machineId, TableID opId,
                                                          TableID transmitterID, size_vdb k,
                                                          KAnonymousOperatorPtr child,
                                                          std::vector<pos_vdb> orderByEntries,
                                                          std::vector<SortOrder> sortOrders,
                                                          utilities::InSGXController *dispatcher,
                                                          MachineID designatedCollector);
            ~ClusterKAnonymousBitonicMergeSort() override;
            OperatorStatus next() override;
            void reset() override;
            OperatorStatus
            receiveCallBack(MachineID src_machine_id, TableID src_tid, db::obdata::ObliviousTupleTable *table) override;
            size_vdb getGeneralizationLevel(pos_vdb index) const override;
        protected:
            size_vdb getSerializationOverHead(std::set<TableID> &baseTables) const override;
            void encodeOperator(unsigned char *&writeHead) const override;
        private:
            size_vdb getTableCount();
            OperatorStatus dispatch(MachineID dst_machine_id, db::obdata::ObliviousTupleTable *table);
            void initializeIterationTracker();
            OperatorStatus MergeForOneTuple();

            db::obdata::ObliviousTupleList _finalTupleBuffer;
            pos_vdb _pos; // Range: 1 ~ size. Actual reference requires _leftPos - 1
            std::vector<pos_vdb> _orderByEntries;
            std::vector<SortOrder> _sortOrders;
            MachineID _designatedCollector;

            // storage structures
            MachineTableMap _tables;
            std::map<MachineID, pos_vdb> _tablesTracker;

            // overheads
            std::mutex _tablesMutex;
        };

    }
}


#endif //VAULTDB_CLUSTERKABITONICMERGESORT_H
