#ifndef VAULTDB_CLUSTERKAHASHJOIN_H
#define VAULTDB_CLUSTERKAHASHJOIN_H

#include <in_sgx/plan/clusterops/ClusterOperator.h>
#include "KAnonymousOperator.h"
#include "in_sgx/obdata/ObliviousTuple.h"
#include "shared/type/RecordSchema.h"
#include "in_sgx/plan/obexpressions/ObliviousExpression.h"
#include "in_sgx/utilities/InSGXController.h"
#include "in_sgx/obdata/ObliviousTupleTable.h"
#include "in_sgx/serialization/ObliviousSerializer.h"
#include "ClusterKAnonymousSimpleHashRepart.h"
#include <mutex>

#ifdef CLUSTER_OPERATOR_DEBUG
#ifdef CLUSTER_OB_HASH_JOIN_DEBUG
//#define CLUSTER_OB_HASH_JOIN_DEBUG
#endif
#endif

namespace plan {
    namespace kaoperators {

        class ClusterKAnonymousHashNestedLoopJoin : public KAnonymousOperator, public virtual ClusterOperator {

        public:
            ClusterKAnonymousHashNestedLoopJoin(MachineID machineId, TableID opId,
                                      TransmitterID leftChildTransmissionID,
                                      TransmitterID rightChildTransmissionID, size_vdb k,
                                      KAnonymousOperatorPtr leftChild,
                                      KAnonymousOperatorPtr rightChild,
                                      std::vector<pos_vdb> leftJoinAttributes,
                                      std::vector<pos_vdb> rightJoinAttributes,
                                      utilities::InSGXController *dispatcher);
            ~ClusterKAnonymousHashNestedLoopJoin() override;
            OperatorStatus next() override;
            void reset() override;

            OperatorStatus
            receiveCallBack(MachineID src_machine_id, TableID src_tid, db::obdata::ObliviousTupleTable *table) override;

        protected:
            size_vdb getSerializationOverHead(std::set<TableID> &baseTables) const override;
            void encodeOperator(unsigned char *&writeHead) const override;
        private:
            // innate properties
            std::vector<pos_vdb> _leftAttributes;
            std::vector<pos_vdb> _rightAttributes;
            bool _doesLeftHaveMore;
        };

    }
}


#endif //VAULTDB_CLUSTERKAHASHJOIN_H
