#ifndef VAULTDB_KANONYMOUSWINDOW_H
#define VAULTDB_KANONYMOUSWINDOW_H

#include <in_sgx/plan/obexpressions/ObliviousExpressionList.h>
#include <in_sgx/plan/obexpressions/AggregateObliviousExpressionList.h>
#include "KAnonymousOperator.h"
#include "KAnonymousSortedGroup.h"

namespace plan {
    namespace kaoperators {
        class KAnonymousWindow : public KAnonymousSortedGroup {
        public:
            KAnonymousWindow(TableID opId, size_vdb k, KAnonymousOperatorPtr child,
                                         obexpressions::ObliviousExpressionList exps, std::vector<pos_vdb> partitions,
                                         std::vector<pos_vdb> orderBys, std::vector<SortOrder> sortOrderOrOrderBys,
                                         utilities::InSGXController *controller);
            ~KAnonymousWindow();
        };
    }
}

#endif //VAULTDB_KANONYMOUSWINDOW_H
