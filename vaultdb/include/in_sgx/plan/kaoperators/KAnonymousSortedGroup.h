#ifndef VAULTDB_KANONYMOUSSORTEDGROUP_H
#define VAULTDB_KANONYMOUSSORTEDGROUP_H

#include <string>
#include <in_sgx/utilities/InSGXController.h>
#include "KAnonymousOperator.h"
#include "in_sgx/obdata/ObliviousTupleTable.h"
#include "shared/type/RecordSchema.h"
#include "in_sgx/plan/obexpressions/ObliviousExpression.h"
#include "in_sgx/plan/obexpressions/AggregateObliviousExpressionList.h"
#include "in_sgx/plan/obexpressions/ObliviousExpressionList.h"

namespace utilities {
    class InSGXController;
}

namespace plan {
namespace kaoperators {

    class KAnonymousSortedGroup : public KAnonymousOperator {
    public:
        KAnonymousSortedGroup(TableID opId, size_vdb k, KAnonymousOperatorPtr child,
                                      obexpressions::ObliviousExpressionList exps,
                                      std::vector<pos_vdb> groupBys,
                                      utilities::InSGXController *constroller);
        ~KAnonymousSortedGroup();

        OperatorStatus next() override;
        void reset() override;
        obexpressions::ObliviousExpressionList getExpressions() const;
        size_vdb getGeneralizationLevel(pos_vdb index) const override;
    protected:
        size_vdb getSerializationOverHead(std::set<TableID> &baseTables) const override;
        void encodeOperator(unsigned char *&writeHead) const override;
    protected:
        KAnonymousSortedGroup(
                TableID opId,
                size_vdb k,
                KAnonymousOperatorPtr child,
                bool isOperatorWindowAggregation
        );
        void _nextMiddle();
        void _nextEnd();
        void _evaluate();

        bool _isCurrentGroupAllDummy;
        bool _isOperatorWindowAggregation;

        obexpressions::ObliviousTupleBox _lastAggTuple;
        obexpressions::ObliviousTupleBox _curAggTuple;
        obexpressions::AggregateObliviousExpressionList _aggs;
        obexpressions::ObliviousExpressionList _exps;
        obexpressions::ObliviousExpressionList _checkInGroup;
        bool _firstPull = true;
        bool _finishedPulling = false;
    };
}
}


#endif //VAULTDB_KANONYMOUSSORTEDGROUP_H
