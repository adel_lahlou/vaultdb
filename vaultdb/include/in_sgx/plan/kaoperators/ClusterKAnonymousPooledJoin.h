#ifndef VAULTDB_CLUSTERKANONYMOUSHASHPOOLEDJOIN_H
#define VAULTDB_CLUSTERKANONYMOUSHASHPOOLEDJOIN_H

#include <in_sgx/plan/clusterops/ClusterOperator.h>
#include "KAnonymousOperator.h"

namespace plan {
namespace kaoperators {
    class ClusterKAnonymousPooledJoin : public KAnonymousOperator, public virtual ClusterOperator {
    public:
        ClusterKAnonymousPooledJoin(MachineID machineId, TableID opId,
                                        TransmitterID leftChildTransmissionID,
                                        TransmitterID rightChildTransmissionID, size_vdb k,
                                        KAnonymousOperatorPtr leftChild,
                                        KAnonymousOperatorPtr rightChild,
                                        std::vector<pos_vdb> leftJoinAttributes,
                                        std::vector<pos_vdb> rightJoinAttributes,
                                        std::vector<pos_vdb> leftPartitionKey,
                                        std::vector<pos_vdb> rightPartitionKey,
                                        utilities::InSGXController *dispatcher);
        ~ClusterKAnonymousPooledJoin() override;
        OperatorStatus next() override;
        void reset() override;

        OperatorStatus
        receiveCallBack(MachineID src_machine_id, TableID src_tid, db::obdata::ObliviousTupleTable *table) override;
    protected:
        size_vdb getSerializationOverHead(std::set<TableID> &baseTables) const override;
        void encodeOperator(unsigned char *&writeHead) const override;
    private:
        std::vector<pos_vdb> _leftAttributes;
        std::vector<pos_vdb> _rightAttributes;
        std::vector<pos_vdb> _leftPartitionKey;
        std::vector<pos_vdb> _rightPartitionKey;
        bool _doesLeftHaveMore;

        db::obdata::ObliviousTupleList _overFlowPool;
        pos_vdb _fetchIndex;
    };
}}


#endif //VAULTDB_CLUSTERKANONYMOUSHASHPOOLEDJOIN_H
