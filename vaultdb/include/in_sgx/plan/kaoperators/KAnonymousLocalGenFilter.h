//
// Created by k2i on 1/25/18.
//

#ifndef VAULTDB_PROJ_KANONYMOUSLOCALGENFILTER_H
#define VAULTDB_PROJ_KANONYMOUSLOCALGENFILTER_H


#include <in_sgx/plan/obexpressions/ObliviousExpression.h>
#include <set>
#include <in_sgx/utilities/InSGXController.h>
#include "KAnonymousOperator.h"

namespace plan {
namespace kaoperators {
    class KAnonymousLocalGenFilter : public KAnonymousOperator {
    public:
        KAnonymousLocalGenFilter(TableID opId, size_vdb k, KAnonymousOperatorPtr child,
                                 plan::obexpressions::ObliviousExpression filterPred,
                                 plan::obexpressions::ObliviousExpression filterPredCopy,
                                 std::vector<pos_vdb> entities,
                                 std::vector<size_vdb> imposedGenLevels,
                                 utilities::InSGXController *controller);
        ~KAnonymousLocalGenFilter();

        OperatorStatus next() override;
        void reset() override;
    protected:
        size_vdb getSerializationOverHead(std::set<TableID> &baseTables) const override;
        void encodeOperator(unsigned char *&writeHead) const override;
    private:
        plan::obexpressions::ObliviousTupleBox _filterTuple;
        plan::obexpressions::ObliviousExpression _filterActualPred;
        plan::obexpressions::ObliviousExpression _filterViewPred;
        std::vector<pos_vdb> _entityIdentifiers;
        std::vector<pos_vdb> _generalizationLevels;
        db::obdata::ObliviousTupleTable * _inputTable;
        std::vector<db::obdata::ObliviousTuple>::iterator _inputTableIterator;
        utilities::InSGXController * _controller;
    };
}
}


#endif //VAULTDB_PROJ_KANONYMOUSLOCALGENFILTER_H
