#ifndef VAULTDB_KANONYMOUSJOIN_H
#define VAULTDB_KANONYMOUSJOIN_H

#include <string>
#include "KAnonymousOperator.h"
#include "in_sgx/obdata/ObliviousTuple.h"
#include "shared/type/RecordSchema.h"
#include <in_sgx/plan/obexpressions/obexpressions.h>

namespace plan {
    namespace kaoperators {

        class KAnonymousNestedJoin : public KAnonymousOperator {
        public:
            KAnonymousNestedJoin(
                    TableID opId,
                    size_vdb k,
                    KAnonymousOperatorPtr leftChild,
                    KAnonymousOperatorPtr rightChild,
                    plan::obexpressions::JoinObliviousExpression joinPred
            );

            ~KAnonymousNestedJoin();

            KAnonymousOperatorPtr getLeftChild() const;

            KAnonymousOperatorPtr getRightChild() const;

            OperatorStatus next();

            void reset();

        private:
            plan::obexpressions::ObliviousTupleBox _leftJoinTuple;
            plan::obexpressions::ObliviousTupleBox _rightJoinTuple;
            plan::obexpressions::JoinObliviousExpression _joinPred;
        };
    }
}

#endif //VAULTDB_KANONYMOUSJOIN_H