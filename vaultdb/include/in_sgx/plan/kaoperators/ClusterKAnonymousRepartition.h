#ifndef VAULTDB_PROJ_CLUSTERKANONYMOUSREPARTITION_H
#define VAULTDB_PROJ_CLUSTERKANONYMOUSREPARTITION_H

#include <in_sgx/plan/clusterops/ClusterOperator.h>
#include <mutex>
#include <in_sgx/utilities/DummyInSGXHashPartitioner.h>
#include "KAnonymousOperator.h"
#include "in_sgx/utilities/InSGXController.h"
#include "in_sgx/obdata/ObliviousTupleTable.h"

namespace plan {
namespace kaoperators {
    class ClusterKAnonymousRepartition : public KAnonymousOperator, public virtual ClusterOperator{
    public:
        ClusterKAnonymousRepartition(MachineID machineId, TableID opID,
                                             TransmitterID transmitterID, size_vdb k,
                                             KAnonymousOperatorPtr child,
                                             std::vector<pos_vdb> controlFlowAttributes,
                                             std::vector<pos_vdb> entityIdentifiers,
                                             std::vector<size_vdb> imposedGenLevels,
                                             bool isSortingFirst,
                                             utilities::InSGXController *dispatcher);
        ~ClusterKAnonymousRepartition() override;
        OperatorStatus next() override;
        void reset() override;

        OperatorStatus
        receiveCallBack(MachineID src_machine_id, TableID src_tid, db::obdata::ObliviousTupleTable *table) override;
        size_vdb getGeneralizationLevel(pos_vdb index) const override;

        size_vdb getTotalTupleCount();
    protected:
        size_vdb getSerializationOverHead(std::set<TableID> &baseTables) const override;
        void encodeOperator(unsigned char *&writeHead) const override;
    private:
        OperatorStatus dispatchAll(std::map<MachineID, db::obdata::ObliviousTupleTable *> &tuplesOfEachMachine,
                                   type::RecordSchema &dispatchSchema);
        OperatorStatus sortHashAndDispatch();
        OperatorStatus hashAndDispatch();
        size_vdb getTableCount();
        void acquireGeneralizationLevels();

        // storage structures
        db::obdata::ObliviousTupleTable * _mergedTable;

        // overheads
        std::vector<pos_vdb> _controlFlowAttributes;
        std::vector<pos_vdb> _entityIdentifiers;
        std::vector<size_vdb> _generalizationLevels;
        std::map<MachineID, size_vdb> _tablePartCounts; // consider changing this for fragmented execution
        pos_vdb _pos; // Range: 1 ~ size. Actual reference requires _leftPos - 1
        std::mutex _tablesMutex;
        utilities::DummyInSGXHashPartitioner _partitioner;
        bool _isDerivingGenLevel;
        bool _isSortingFirst;
    };
}
}


#endif //VAULTDB_PROJ_CLUSTERKANONYMOUSREPARTITION_H
