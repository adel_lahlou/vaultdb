#ifndef VAULTDB_PROJ_KANONYMOUSSEQSCANMARKNONDUMMY_H
#define VAULTDB_PROJ_KANONYMOUSSEQSCANMARKNONDUMMY_H

#include "KAnonymousOperator.h"

namespace plan {
    namespace kaoperators {
        class KAnonymousSeqScanMarkNonDummy : public KAnonymousOperator {
        public:
            KAnonymousSeqScanMarkNonDummy(TableID
                opId,
                size_vdb k, db::obdata::ObliviousTupleTable * inputTable);

            ~KAnonymousSeqScanMarkNonDummy();

            OperatorStatus next() override;

            void reset() override;

            size_vdb getGeneralizationLevel(pos_vdb index) const override;
        private:
            size_vdb _scanPos = 0;
            db::obdata::ObliviousTupleTable * _inputTable;
        };
    }
}

#endif //VAULTDB_PROJ_KANONYMOUSSEQSCANMARKNONDUMMY_H
