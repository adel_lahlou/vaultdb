#ifndef VAULTDB_CLUSTERKAHASHING_H
#define VAULTDB_CLUSTERKAHASHING_H

#include "in_sgx/plan/clusterops/ClusterOperator.h"
#include "KAnonymousOperator.h"
#include "in_sgx/utilities/InSGXController.h"
#include <mutex>

#ifdef CLUSTER_OPERATOR_DEBUG
#ifdef CLUSTER_OB_HASHING_DEBUG
//#define CLUSTER_OB_HASHING_DEBUG
#endif
#endif

namespace plan {
    namespace kaoperators {
        class ClusterKAnonymousSimpleHashRepart : public KAnonymousOperator, public virtual ClusterOperator{
        public:
            ClusterKAnonymousSimpleHashRepart(MachineID machineId, TransmitterID transmitterID, size_vdb k, KAnonymousOperatorPtr child,
                                                 std::vector<pos_vdb> partitionAttributes, utilities::InSGXController *dispatcher);
            ~ClusterKAnonymousSimpleHashRepart() override;
            OperatorStatus next() override;
            void reset() override;

            size_vdb getTotalTupleCount();

            OperatorStatus
            receiveCallBack(MachineID src_machine_id, TableID src_tid, db::obdata::ObliviousTupleTable *table) override;
        private:
            OperatorStatus hashAndDispatch();
            size_vdb getTableCount();
            void reachForNonEmptyTable();
            OperatorStatus ensureTableIteratorReadyForUse();
            void findIterator();
            void findNextTuple();

            // innate properties
            std::vector<pos_vdb> _attributes;

            // storage structures
            MachineTableMap _tables;

            // overheads
            db::obdata::ObliviousTupleTable * _tablePtr;
            bool _isTableIteratorInit;
            MachineTableMap::iterator _tableIterator;
            pos_vdb _pos; // Range: 1 ~ size. Actual reference requires _leftPos - 1
            std::mutex _tablesMutex;
        };
    }
}

#endif //VAULTDB_OBLIVIOUSHASHING_H
