#ifndef VAULTDB_CLUSTERKANONYMOUSLOCALDISTRIBUTEDAGGREGATION_H
#define VAULTDB_CLUSTERKANONYMOUSLOCALDISTRIBUTEDAGGREGATION_H


#include <in_sgx/plan/clusterops/ClusterOperator.h>
#include <in_sgx/plan/obexpressions/ObliviousExpressionList.h>
#include "KAnonymousOperator.h"

namespace plan {
    namespace kaoperators {
        class ClusterKAnonymousLDAggregation : public KAnonymousOperator, public virtual ClusterOperator {
        public:
            ClusterKAnonymousLDAggregation(MachineID machineId, TableID opId, TransmitterID transmitterID, size_vdb k,
                                                       KAnonymousOperatorPtr child, std::vector<obexpressions::ObliviousExpression> exps,
                                                       std::vector<pos_vdb> groupBys, utilities::InSGXController *dispatcher);
            ~ClusterKAnonymousLDAggregation() override;
            OperatorStatus next() override;
            void reset() override;

            OperatorStatus
            receiveCallBack(MachineID src_machine_id, TableID src_tid, db::obdata::ObliviousTupleTable *table) override;
            size_vdb getGeneralizationLevel(pos_vdb index) const override;
        protected:
            ClusterKAnonymousLDAggregation(MachineID machineId, TableID opId,
                                                       TransmitterID transmitterID, size_vdb k,
                                                       KAnonymousOperatorPtr child,
                                                       utilities::InSGXController *dispatcher);
            size_vdb getSerializationOverHead(std::set<TableID> &baseTables) const override;
            void encodeOperator(unsigned char *&writeHead) const override;
        };
    }
}


#endif //VAULTDB_CLUSTERKANONYMOUSLOCALDISTRIBUTEDAGGREGATION_H
