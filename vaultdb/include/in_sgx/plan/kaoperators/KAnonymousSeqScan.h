#ifndef VAULTDB_KANONYMOUSSEQSCAN_H
#define VAULTDB_KANONYMOUSSEQSCAN_H

#include <string>
#include "KAnonymousOperator.h"
#include "in_sgx/obdata/ObliviousTupleTable.h"
#include "shared/type/RecordSchema.h"

namespace plan {
namespace kaoperators {
    class KAnonymousSeqScan : public KAnonymousOperator {
    public:
        KAnonymousSeqScan(TableID opId, size_vdb k, db::obdata::ObliviousTupleTable * inputTable);
        ~KAnonymousSeqScan();

        OperatorStatus next() override;
        void reset() override;

        size_vdb getGeneralizationLevel(pos_vdb index) const override;

    protected:
        size_vdb getSerializationOverHead(std::set<TableID> &baseTables) const override;
        void encodeOperator(unsigned char *&writeHead) const override;
    private:
        size_vdb _scanPos = 0;
        db::obdata::ObliviousTupleTable * _inputTable;
    };
}
}

#endif // VAULTDB_KANONYMOUSSEQSCAN_H
