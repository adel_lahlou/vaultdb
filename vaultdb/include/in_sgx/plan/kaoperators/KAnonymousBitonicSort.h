#ifndef VAULTDB_KA_BITONICSORT_H
#define VAULTDB_KA_BITONICSORT_H

#include <string>
#include <in_sgx/utilities/InSGXController.h>
#include <in_sgx/utilities/BufferPool.h>
#include "KAnonymousOperator.h"
#include "in_sgx/obdata/ObliviousTupleTable.h"
#include "shared/type/RecordSchema.h"
#include "in_sgx/obdata/ObliviousTuple.h"
#include "shared/Definitions.h"

using namespace db::obdata;

namespace utilities{
    class InSGXController;
}

namespace plan {
    namespace kaoperators {

        class KAnonymousBitonicSort : public KAnonymousOperator {
            friend class ClusterKAnonymousBitonicMergeSort;

        public:
            KAnonymousBitonicSort(TableID opId, size_vdb k, KAnonymousOperatorPtr child,
                                              std::vector<pos_vdb> orderByEntries, std::vector<SortOrder> sortOrders,
                                              utilities::InSGXController *controller);

            ~KAnonymousBitonicSort();

            OperatorStatus next() override;
            void reset() override;
            std::vector<pos_vdb> getOrderColumns() const;
        protected:
            size_vdb getSerializationOverHead(std::set<TableID> &baseTables) const override;
            void encodeOperator(unsigned char *&writeHead) const override;
        private:
            ObliviousTupleTable * _tupleBuffer;
            std::vector<pos_vdb> _orderByEntries;
            std::vector<SortOrder> _sortOrders;
            utilities::InSGXController * _controller;

            int _outputPos;
        };
    }
}

#endif //VAULTDB_BITONICSORT_H
