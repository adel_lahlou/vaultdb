#ifndef VAULTDB_PROJ_CLUSTERKANONYMOUSGENJOIN_H
#define VAULTDB_PROJ_CLUSTERKANONYMOUSGENJOIN_H

#include <shared/Definitions.h>
#include <in_sgx/plan/clusterops/ClusterOperator.h>
#include "KAnonymousOperator.h"

namespace plan {
    namespace kaoperators {
        class ClusterKAnonymousGenJoin : public KAnonymousOperator, public virtual ClusterOperator {

        public:
            ClusterKAnonymousGenJoin(MachineID machineId, TableID opId,
                                     TransmitterID leftChildTransmissionID,
                                     TransmitterID rightChildTransmissionID, size_vdb k,
                                     KAnonymousOperatorPtr leftChild,
                                     KAnonymousOperatorPtr rightChild,
                                     std::vector<pos_vdb> leftJoinAttributes,
                                     std::vector<pos_vdb> rightJoinAttributes,
                                     std::vector<pos_vdb> leftEntityIdentifier,
                                     std::vector<pos_vdb> rightEntityIdentifier,
                                     std::vector<size_vdb> leftImposedGenLevels,
                                     std::vector<size_vdb> rightImposedGenLevels,
                                     bool isSortingBeforeRepart,
                                     utilities::InSGXController *dispatcher);

            ~ClusterKAnonymousGenJoin() override;

            OperatorStatus next() override;

            void reset() override;

            OperatorStatus
            receiveCallBack(MachineID src_machine_id, TableID src_tid, db::obdata::ObliviousTupleTable *table) override;

            size_vdb getGeneralizationLevel(pos_vdb index) const override;
        protected:
            size_vdb getSerializationOverHead(std::set <TableID> &baseTables) const override;
            void encodeOperator(unsigned char *&writeHead) const override;

        private:
            // innate properties
            std::vector <pos_vdb> _leftAttributes;
            std::vector <pos_vdb> _rightAttributes;
            std::vector <pos_vdb> _leftEntityIdentifier;
            std::vector <pos_vdb> _rightEntityIdentifier;
            std::vector <pos_vdb> _leftImposedGenLevel;
            std::vector <pos_vdb> _rightImposedGenLevel;
            bool _doesLeftHaveMore;
            std::vector<size_vdb> _generalizationLevels;
            bool _isSortingBeforeRepart;
        };
    }}

#endif //VAULTDB_PROJ_CLUSTERKANONYMOUSGENJOIN_H
