#ifndef VAULTDB_CLUSTERKANONYMOUSHASHWINDOW_H
#define VAULTDB_CLUSTERKANONYMOUSHASHWINDOW_H

#include "ClusterKAnonymousSortedAggregation.h"

namespace plan {
    namespace kaoperators {
        class ClusterKAnonymousWindow : public ClusterKAnonymousSortedAggregation{
        public:
            ClusterKAnonymousWindow(MachineID machineId, TableID opId, TransmitterID transmitterID,
                                                size_vdb k, KAnonymousOperatorPtr child,
                                                obexpressions::ObliviousExpressionList exps,
                                                std::vector<pos_vdb> partitions, std::vector<pos_vdb> orderBys,
                                                std::vector<SortOrder> sortOrderOrOrderBys,
                                                bool isRepartitionSorting, utilities::InSGXController *dispatcher);
            ~ClusterKAnonymousWindow();
            size_vdb getGeneralizationLevel(pos_vdb index) const override;

        protected:
            size_vdb getSerializationOverHead(std::set<TableID> &baseTables) const override;
            void encodeOperator(unsigned char *&writeHead) const override;
        private:
            obexpressions::ObliviousExpressionList _exps;
            std::vector<pos_vdb> _partitions;
            std::vector<pos_vdb> _orderBys;
            std::vector<SortOrder> _sortOrderOrOrderBys;
        };
    }
}


#endif //VAULTDB_CLUSTERKANONYMOUSHASHWINDOW_H
