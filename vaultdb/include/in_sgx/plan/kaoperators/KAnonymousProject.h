#ifndef VAULTDB_KANONYMOUSPROJECT_H
#define VAULTDB_KANONYMOUSPROJECT_H

#include <string>
#include <in_sgx/plan/obexpressions/ObliviousExpression.h>
#include "KAnonymousOperator.h"
#include "in_sgx/plan/obexpressions/ObliviousExpressionList.h"

namespace type {
    class RecordSchema;
}

namespace plan {
namespace kaoperators {
    class KAnonymousProject : public KAnonymousOperator {
    public:
        KAnonymousProject(
                TableID opId,
                size_vdb k,
                KAnonymousOperatorPtr child,
                plan::obexpressions::ObliviousExpressionList exps
        );
        ~KAnonymousProject();

        OperatorStatus next() override;
        void reset() override;
        size_vdb getGeneralizationLevel(pos_vdb index) const override;
    protected:
        size_vdb getSerializationOverHead(std::set<TableID> &baseTables) const override;
        void encodeOperator(unsigned char *&writeHead) const override;
    private:
        obexpressions::ObliviousTupleBox _projectTuple;
        plan::obexpressions::ObliviousExpressionList _exps;
    };
}
}

#endif //VAULTDB_KANONYMOUSPROJECT_H
