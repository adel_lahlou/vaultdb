#ifndef VAULTDB_KANONYMOUSUNION_H
#define VAULTDB_KANONYMOUSUNION_H


#include <string>
#include "KAnonymousOperator.h"

namespace plan {
namespace kaoperators {
    class KAnonymousUnion : public KAnonymousOperator {
    public:
        KAnonymousUnion(TableID opId, size_vdb k, KAnonymousOperatorPtrList children);
        ~KAnonymousUnion();

        OperatorStatus next() override;
        void reset() override;

    protected:
        size_vdb getSerializationOverHead(std::set<TableID> &baseTables) const override;
        void encodeOperator(unsigned char *&writeHead) const override;
    private:
        int _curChild;
        size_vdb _outputCountLowerBound;
    };
}
}

#endif //VAULTDB_KANONYMOUSUNION_H
