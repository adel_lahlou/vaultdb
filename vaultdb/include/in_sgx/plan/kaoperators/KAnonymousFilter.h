#ifndef VAULTDB_KANONYMOUSFILTER_H
#define VAULTDB_KANONYMOUSFILTER_H

#include <string>
#include "KAnonymousOperator.h"
#include "shared/type/RecordSchema.h"
#include "in_sgx/plan/obexpressions/ObliviousExpression.h"

namespace plan {
namespace kaoperators {
    class KAnonymousFilter : public KAnonymousOperator {
    public:
        KAnonymousFilter(
                TableID opId,
                size_vdb k,
                KAnonymousOperatorPtr child,
                plan::obexpressions::ObliviousExpression filterPred
        );
        ~KAnonymousFilter();

        OperatorStatus next() override;
        void reset() override;
    protected:
        size_vdb getSerializationOverHead(std::set<TableID> &baseTables) const override;
        void encodeOperator(unsigned char *&writeHead) const override;
    private:
        plan::obexpressions::ObliviousTupleBox _filterTuple;
        plan::obexpressions::ObliviousExpression _filterPred;
    };
}
}

#endif //VAULTDB_KANONYMOUSFILTER_H
