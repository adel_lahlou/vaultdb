#ifndef VAULTDB_PROJ_CLUSTERKANONYMOUSLIMIT_H
#define VAULTDB_PROJ_CLUSTERKANONYMOUSLIMIT_H


#include <in_sgx/plan/clusterops/ClusterOperator.h>
#include <mutex>
#include "KAnonymousOperator.h"

namespace plan {
    namespace kaoperators {
        class ClusterKAnonymousLimit : public KAnonymousOperator, public virtual ClusterOperator {
        public:
            ClusterKAnonymousLimit(MachineID machineID, TableID opId, TransmitterID transmitterID,
                                               size_vdb k, KAnonymousOperatorPtr child, size_vdb tupleLimit,
                                               std::vector<pos_vdb> orderByEntries,
                                               std::vector<SortOrder> sortOrders, MachineID designatedCollector,
                                               utilities::InSGXController *dispatcher);
            ~ClusterKAnonymousLimit();

            OperatorStatus next() override;
            void reset() override;

            OperatorStatus
            receiveCallBack(MachineID src_machine_id, TableID src_tid, db::obdata::ObliviousTupleTable *table) override;
        protected:
            size_vdb getSerializationOverHead(std::set <TableID> &baseTables) const override;
            void encodeOperator(unsigned char *&writeHead) const override;
            size_vdb getReceivedTableCount();
        private:
            void init();

            std::vector<pos_vdb> _orderByEntries;
            std::vector<SortOrder> _sortOrders;
            size_vdb _tupleCountLimit;
            MachineID _designatedCollector;

            std::mutex _tablesMutex;
            size_vdb _receivedPartitionCount;

            pos_vdb _pos; // Range: 1 ~ size. Actual reference requires _leftPos - 1
            pos_vdb _outputTupleCount;
            db::obdata::ObliviousTupleTable * _finalTupleBuffer;
        };
    }
}


#endif //VAULTDB_PROJ_CLUSTERKANONYMOUSLIMIT_H
