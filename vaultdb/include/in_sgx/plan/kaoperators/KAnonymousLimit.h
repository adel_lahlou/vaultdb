#ifndef VAULTDB_KANONYMOUSLIMIT_H
#define VAULTDB_KANONYMOUSLIMIT_H

#include "KAnonymousOperator.h"

namespace plan {
    namespace kaoperators {
        class KAnonymousLimit : public KAnonymousOperator {
        public:
            KAnonymousLimit(TableID opId, size_vdb k, size_vdb tupleLimit, KAnonymousOperatorPtrList children);
            ~KAnonymousLimit();

            OperatorStatus next() override;
            void reset() override;
        protected:
            size_vdb getSerializationOverHead(std::set<TableID> &baseTables) const override;
            void encodeOperator(unsigned char *&writeHead) const override;
        private:
            size_vdb _outputTupleCount;
            size_vdb _tupleLimit;
        };
    }
}


#endif //VAULTDB_KANONYMOUSLIMIT_H
