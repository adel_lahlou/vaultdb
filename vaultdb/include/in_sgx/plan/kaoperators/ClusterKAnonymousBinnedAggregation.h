#ifndef VAULTDB_PROJ_CLUSTERKANONYMOUSBINNEDAGGREGATION_H
#define VAULTDB_PROJ_CLUSTERKANONYMOUSBINNEDAGGREGATION_H

#include <in_sgx/plan/clusterops/ClusterOperator.h>
#include <in_sgx/plan/obexpressions/ObliviousExpressionList.h>
#include <in_sgx/plan/obexpressions/AggregateObliviousExpressionList.h>
#include "KAnonymousOperator.h"
#include "in_sgx/plan/kasupport/KAnonymousPullAndBin.h"

namespace plan {
    namespace kaoperators {
        class ClusterKAnonymousBinnedAggregation : public KAnonymousOperator, public virtual ClusterOperator {
        public:
            ClusterKAnonymousBinnedAggregation(MachineID machineId, TableID opId,
                                                           TransmitterID transmissionID, size_vdb k,
                                                           KAnonymousOperatorPtr child,
                                                           obexpressions::ObliviousExpressionList exps,
                                                           std::vector<pos_vdb> groupBys,
                                                           bool isRepartitionSorting,
                                                           bool createBinOnDemand,
                                                           utilities::InSGXController *dispatcher);
            ~ClusterKAnonymousBinnedAggregation();
            OperatorStatus next() override;
            void reset() override;

            size_vdb getGeneralizationLevel(pos_vdb index) const override;
            OperatorStatus
            receiveCallBack(MachineID src_machine_id, TableID src_tid, db::obdata::ObliviousTupleTable *table) override;
        protected:
            size_vdb getSerializationOverHead(std::set<TableID> &baseTables) const override;
            void encodeOperator(unsigned char *&writeHead) const override;

            KAnonymousPullAndBin _pullAndBin;

            obexpressions::ObliviousExpressionList _exps;
            obexpressions::ObliviousTupleBox _curAggTuple;
            obexpressions::AggregateObliviousExpressionList _aggs;

            bool _isRepartitionSorting;
            bool _createBinOnDemand;
        };
    }
}


#endif //VAULTDB_PROJ_CLUSTERKANONYMOUSBINNEDAGGREGATION_H
