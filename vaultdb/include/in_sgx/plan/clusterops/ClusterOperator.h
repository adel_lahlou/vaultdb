#ifndef VAULTDB_CLUSTEROPERATOR_H
#define VAULTDB_CLUSTEROPERATOR_H

#include <in_sgx/obdata/ObliviousTuple.h>
#include <in_sgx/obdata/ObliviousTupleTable.h>
#include "shared/Definitions.h"

#define CACHE_LINE_SIZE_IN_DIGITS 6
#define INIT_TABLE_INDEX 0
#define GET_TABLE_READ_POSITION(x) x-1
#define GET_NEAREST_CACHELINE_MULTIPLE(x) ((x >> CACHE_LINE_SIZE_IN_DIGITS) + 1) << CACHE_LINE_SIZE_IN_DIGITS

#define DESIGNATED_COLLECTOR 0

#ifndef CLUSTER_OPERATOR_DEBUG
#define CLUSTER_OPERATOR_DEBUG
#define PRINTING_MACHINE_ID 0
#endif

namespace utilities {
    class InSGXController;
}

class ClusterOperator {

public:
    ClusterOperator(MachineID machineID, TransmitterID transmitterID, utilities::InSGXController *dispatcher,
                    size_vdb numMachines);
    virtual ~ClusterOperator();
    virtual OperatorStatus receiveCallBack(MachineID src_machine_id, TableID src_tid, db::obdata::ObliviousTupleTable *table);
    virtual OperatorStatus receiveGenericMessage(MachineID src_machine_id, TableID src_tid, unsigned char *data, size_vdb msg_size);
    MachineID getLocalMachineID() const;
    bool isAllDataCollected() const;
    TransmitterID getTransmitterID() const;
protected:
    MachineID _localMachineID;
    bool _isAllDataCollected;
    size_vdb _totalMachineCount;
    TransmitterID _transmitterID;
    utilities::InSGXController * _localDispatcher;
};


#endif //VAULTDB_CLUSTEROPERATOR_H
