#ifndef VAULTDB_OBLIVIOUSLOGICAL_H
#define VAULTDB_OBLIVIOUSLOGICAL_H

#include "ObliviousExpression.h"
namespace plan{
namespace obexpressions {
    class UnaryExpression : public ObliviousExpression {
    public:
        UnaryExpression(ObliviousExpression exp);
        bool isAggregateFunction() const override;
        virtual void setObliviousTuple(ObliviousTupleBox tp)  override = 0;
        virtual db::obdata::obfield::ObliviousField evaluate() const override = 0;
        type::SchemaColumn outputDatatype() const override;
        size_vdb encodeSize() const override;
        void getInputReferences(std::vector<pos_vdb> & output) const override;
        virtual void enforceGenLevel(size_vdb genLevel) override;
    protected:
        ObliviousExpression _exp;
    };

    class Not : public UnaryExpression {
    public:
        Not(ObliviousExpression e);
        ~Not();

        void setObliviousTuple(ObliviousTupleBox tp) override;
        db::obdata::obfield::ObliviousField evaluate() const override;
        void encodeObliviousExpression(unsigned char * & writeHead) const override;
        serialization::ExpressionCode getExpressionType() const override;
    };
}
}

#endif //VAULTDB_OBLIVIOUSLOGICAL_H