#ifndef VAULTDB_OBLIVIOUSAGGREGATE_H
#define VAULTDB_OBLIVIOUSAGGREGATE_H

#include "ObliviousExpression.h"
#include "in_sgx/obdata/obfield/ObliviousIntField.h"
#include "Values.h"

namespace plan {
namespace obexpressions {
    class Sum;
    class Count;
    class RowNumber;

    class AggregateFunction;

    typedef std::shared_ptr<AggregateFunction> AggregateFunctionPtr;

    class AggregateFunction : public ObliviousExpression {
        friend class ObliviousField;
        friend class Sum;
        friend class Count;
        friend class CountDistinct;
        friend class RowNumber;
    public:
        AggregateFunction();
        AggregateFunction(const ObliviousExpression& other);
        bool isAggregateFunction() const override;
        using ObliviousExpression::setObliviousTuple;
        using ObliviousExpression::evaluate;
        using ObliviousExpression::encodeObliviousExpression;

        virtual void update(bool go);
        virtual void reset(bool go);

        virtual void enforceGenLevel(size_vdb genLevel) override;
    };

    class Sum : public AggregateFunction {
    public:
        Sum(size_vdb colNum, type::SchemaColumn r);
        Sum(Column col);
        ~Sum();

        void setObliviousTuple(ObliviousTupleBox tp) override;
        void update(bool go) override;
        void reset(bool go) override;
        type::SchemaColumn outputDatatype() const override;
        db::obdata::obfield::ObliviousField evaluate() const override;
        size_vdb encodeSize() const override;
        void encodeObliviousExpression(unsigned char * & writeHead) const override;
        void getInputReferences(std::vector<pos_vdb> & output) const override;
        serialization::ExpressionCode getExpressionType() const override;
    private:
        Column column;
        db::obdata::obfield::ObliviousIntField sum;
    };

    class Count : public AggregateFunction {
    public:
        Count(size_vdb colNum);
        Count(Column col);
        ~Count();

        void setObliviousTuple(ObliviousTupleBox tp) override;
        void update(bool go) override;
        void reset(bool go) override;
        db::obdata::obfield::ObliviousField evaluate() const override;
        type::SchemaColumn outputDatatype() const override;
        size_vdb encodeSize() const override;
        void encodeObliviousExpression(unsigned char * & writeHead) const override;
        void getInputReferences(std::vector<pos_vdb> & output) const override;
        serialization::ExpressionCode getExpressionType() const override;
    protected:
        Column column;
        int count = 0;
    };

    class CountDistinct : public Count {
    public:
        CountDistinct(size_vdb colNum, type::SchemaColumn r);
        CountDistinct(Column col);
        ~CountDistinct();

        void setObliviousTuple(ObliviousTupleBox tp) override;
        void update(bool go) override;
        void reset(bool go) override;
        db::obdata::obfield::ObliviousField evaluate() const override;
        using Count::outputDatatype;
        using Count::encodeSize;
        using Count::encodeObliviousExpression;
        using Count::getInputReferences;
        serialization::ExpressionCode getExpressionType() const override;
    private:
        static unsigned char * generateInitialField(type::SchemaColumn r);
        unsigned char * _lastFieldSerialized;
        size_vdb _fieldSize;
        int _isInitialized;
        ObliviousTupleBox _curTp;
    };

    class RowNumber : public AggregateFunction {
    public:
        RowNumber();
        ~RowNumber();

        void setObliviousTuple(ObliviousTupleBox tp) override;
        void update(bool go) override;
        void reset(bool go) override;
        db::obdata::obfield::ObliviousField evaluate() const override;
        type::SchemaColumn outputDatatype() const override;
        size_vdb encodeSize() const override;
        void encodeObliviousExpression(unsigned char * & writeHead) const override;
        void getInputReferences(std::vector<pos_vdb> & output) const override;
        serialization::ExpressionCode getExpressionType() const override;
    private:
        pos_vdb _rank;
    };
}
}
#endif //VAULTDB_OBLIVIOUSAGGREGATE_H