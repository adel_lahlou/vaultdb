#ifndef VAULTDB_OBLIVIOUSVALUES_H
#define VAULTDB_OBLIVIOUSVALUES_H

#include "ObliviousExpression.h"
#include "in_sgx/obdata/obfield/ObliviousField.h"

namespace plan {
namespace obexpressions {
    class ValueExpression : public ObliviousExpression {
    public:
        bool isAggregateFunction() const override;
        virtual void setObliviousTuple(ObliviousTupleBox tp) override = 0;
        virtual db::obdata::obfield::ObliviousField evaluate() const override = 0;
        type::SchemaColumn outputDatatype() const override;
        virtual void enforceGenLevel(size_vdb genLevel) override;
    protected:
        ValueExpression(type::SchemaColumn type);
        type::SchemaColumn _type;
    };

    class Literal : public ValueExpression {
        friend class ObliviousExpression;
    public:
        Literal(db::obdata::obfield::ObliviousField v, type::SchemaColumn type);

        void setObliviousTuple(ObliviousTupleBox tp) override;
        db::obdata::obfield::ObliviousField evaluate() const override;

        void encodeObliviousExpression(unsigned char * & writeHead) const override;
        size_vdb encodeSize() const override;
        void getInputReferences(std::vector<pos_vdb> & output) const override;
        serialization::ExpressionCode getExpressionType() const override;
    private:
        Literal();
        db::obdata::obfield::ObliviousField _value;
    };


    class Column : public ValueExpression {
        friend class ObliviousExpression;
        friend class ObliviousExpressionList;
    public:
        Column(size_vdb colNum, type::SchemaColumn type);

        void setObliviousTuple(ObliviousTupleBox tp) override;
        db::obdata::obfield::ObliviousField evaluate() const override;
        pos_vdb getPosition() const;

        void encodeObliviousExpression(unsigned char * & writeHead) const override;
        size_vdb encodeSize() const override;
        void getInputReferences(std::vector<pos_vdb> & output) const override;
        serialization::ExpressionCode getExpressionType() const override;
    private:
        Column();
        ObliviousTupleBox curTp;
        size_vdb col;
    };
}
}

#endif