#ifndef VAULTDB_OBLIVIOUSEXPRESSIONFACTORY_H
#define VAULTDB_OBLIVIOUSEXPRESSIONFACTORY_H

#include "BinaryExpression.h"
#include "UnaryExpression.h"

namespace plan {
    namespace obexpressions {
        class ObliviousExpressionFactory {
            friend class ObliviousExpression;
            friend class Literal;
            friend class Column;
            friend class Add;
            friend class Sub;
            friend class Mul;
            friend class Div;
            friend class Eq;
            friend class NEq;
            friend class LT;
            friend class GT;
            friend class LTE;
            friend class GTE;
            friend class And;
            friend class Or;
            friend class Not;
            friend class JoinObliviousExpression;
        public:
            static ObliviousExpression makeLiteral(db::obdata::obfield::ObliviousField f, size_vdb size);
            static ObliviousExpression makeColumn(size_vdb colNum, type::SchemaColumn s);
            static ObliviousExpression makeAdd(ObliviousExpression lhs, ObliviousExpression rhs);
            static ObliviousExpression makeSub(ObliviousExpression lhs, ObliviousExpression rhs);
            static ObliviousExpression makeMul(ObliviousExpression lhs, ObliviousExpression rhs);
            static ObliviousExpression makeDiv(ObliviousExpression lhs, ObliviousExpression rhs);
            static ObliviousExpression makeEq(ObliviousExpression lhs, ObliviousExpression rhs);
            static ObliviousExpression makeNEq(ObliviousExpression lhs, ObliviousExpression rhs);
            static ObliviousExpression makeLT(ObliviousExpression lhs, ObliviousExpression rhs);
            static ObliviousExpression makeGT(ObliviousExpression lhs, ObliviousExpression rhs);
            static ObliviousExpression makeLTE(ObliviousExpression lhs, ObliviousExpression rhs);
            static ObliviousExpression makeGTE(ObliviousExpression lhs, ObliviousExpression rhs);
            static ObliviousExpression makeAnd(ObliviousExpression lhs, ObliviousExpression rhs);
            static ObliviousExpression makeOr(ObliviousExpression lhs, ObliviousExpression rhs);
            static ObliviousExpression makeNot(ObliviousExpression exp);
            static ObliviousExpression makeJoin(ObliviousExpression lhs, ObliviousExpression rhs);
            static ObliviousExpression makeSum(pos_vdb colNum, type::SchemaColumn r);
            static ObliviousExpression makeCount(pos_vdb colNum);
            static ObliviousExpression makeCountDistinct(pos_vdb col, type::SchemaColumn r);
            static ObliviousExpression makeRowNumber();
        };
    }
}

#endif //VAULTDB_OBLIVIOUSEXPRESSIONFACTORY_H
