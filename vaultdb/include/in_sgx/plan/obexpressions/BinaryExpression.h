#ifndef VAULTDB_OBLIVIOUSCOMPARISON_H
#define VAULTDB_OBLIVIOUSCOMPARISON_H

#include <shared/QueryEncodingDefinitions.h>
#include "ObliviousExpression.h"

namespace plan{
namespace obexpressions {
    class BinaryExpression : public ObliviousExpression {
        friend class ObliviousExpressionFactory;
    public:
        bool isAggregateFunction() const override;
        virtual void setObliviousTuple(ObliviousTupleBox tp) override = 0;
        virtual db::obdata::obfield::ObliviousField evaluate() const override = 0;
        virtual type::SchemaColumn outputDatatype() const override;
        size_vdb encodeSize() const override;
        void getInputReferences(std::vector<pos_vdb> & output) const override;
        void encodeObliviousExpression(unsigned char * & writeHead) const override;
        virtual void enforceGenLevel(size_vdb genLevel) override;
    protected:
        BinaryExpression(ObliviousExpression lhs, ObliviousExpression  rhs);
        ObliviousExpression
                _lhs,
                _rhs;
        size_vdb _genLevel;
    };

    class Eq : public BinaryExpression {
    public:
        Eq(ObliviousExpression lhs, ObliviousExpression rhs);
        ~Eq();

        void setObliviousTuple(ObliviousTupleBox tp) override;
        db::obdata::obfield::ObliviousField evaluate() const override;
        serialization::ExpressionCode getExpressionType() const override;
    };


    class NEq : public BinaryExpression {
    public:
        NEq(ObliviousExpression lhs, ObliviousExpression rhs);
        ~NEq();

        void setObliviousTuple(ObliviousTupleBox tp) override;
        db::obdata::obfield::ObliviousField evaluate() const override;
        serialization::ExpressionCode getExpressionType() const override;
    };


    class GT : public BinaryExpression {
    public:
        GT(ObliviousExpression lhs, ObliviousExpression rhs);
        ~GT();

        void setObliviousTuple(ObliviousTupleBox tp) override;
        db::obdata::obfield::ObliviousField evaluate() const override;
        serialization::ExpressionCode getExpressionType() const override;
    };


    class LT : public BinaryExpression {
    public:
        LT(ObliviousExpression lhs, ObliviousExpression rhs);
        ~LT();

        void setObliviousTuple(ObliviousTupleBox tp) override;
        db::obdata::obfield::ObliviousField evaluate() const override;
        serialization::ExpressionCode getExpressionType() const override;
    };

    class GTE : public BinaryExpression {
    public:
        GTE(ObliviousExpression lhs, ObliviousExpression rhs);
        ~GTE();

        void setObliviousTuple(ObliviousTupleBox tp) override;
        db::obdata::obfield::ObliviousField evaluate() const override;
        serialization::ExpressionCode getExpressionType() const override;
    };


    class LTE : public BinaryExpression {
    public:
        LTE(ObliviousExpression lhs, ObliviousExpression rhs);
        ~LTE();

        void setObliviousTuple(ObliviousTupleBox tp) override;
        db::obdata::obfield::ObliviousField evaluate() const override;
        serialization::ExpressionCode getExpressionType() const override;
    };

    class Add : public BinaryExpression {
        friend class ObliviousExpression;
    public:
        Add(ObliviousExpression lhs, ObliviousExpression rhs);
        ~Add();

        void setObliviousTuple(ObliviousTupleBox tp) override;
        db::obdata::obfield::ObliviousField evaluate() const override;
        type::SchemaColumn outputDatatype() const override;
        serialization::ExpressionCode getExpressionType() const override;
    };


    class Sub : public BinaryExpression {
    public:
        Sub(ObliviousExpression lhs, ObliviousExpression rhs);
        ~Sub();

        void setObliviousTuple(ObliviousTupleBox tp) override;
        db::obdata::obfield::ObliviousField evaluate() const override;
        type::SchemaColumn outputDatatype() const override;
        serialization::ExpressionCode getExpressionType() const override;
    };


    class Mul : public BinaryExpression {
    public:
        Mul(ObliviousExpression lhs, ObliviousExpression rhs);
        ~Mul();

        void setObliviousTuple(ObliviousTupleBox tp) override;
        db::obdata::obfield::ObliviousField evaluate() const override;
        type::SchemaColumn outputDatatype() const override;
        serialization::ExpressionCode getExpressionType() const override;
    };


    class Div : public BinaryExpression {
    public:
        Div(ObliviousExpression lhs, ObliviousExpression rhs);
        ~Div();

        void setObliviousTuple(ObliviousTupleBox tp) override;
        db::obdata::obfield::ObliviousField evaluate() const override;
        type::SchemaColumn outputDatatype() const override;
        serialization::ExpressionCode getExpressionType() const override;
    };

    class And : public BinaryExpression {
    public:
        And(ObliviousExpression lhs, ObliviousExpression rhs);
        ~And();

        void setObliviousTuple(ObliviousTupleBox tp) override;
        db::obdata::obfield::ObliviousField evaluate() const override;
        serialization::ExpressionCode getExpressionType() const override;
    };


    class Or : public BinaryExpression {
    public:
        Or(ObliviousExpression lhs, ObliviousExpression rhs);
        ~Or();

        void setObliviousTuple(ObliviousTupleBox tp) override;
        db::obdata::obfield::ObliviousField evaluate() const override;
        serialization::ExpressionCode getExpressionType() const override;
    };

    class JoinObliviousExpression : public BinaryExpression {
        friend class ObliviousExpression;
    public:
        JoinObliviousExpression(ObliviousExpression lhs, ObliviousExpression rhs);
        ~JoinObliviousExpression();

        void setObliviousTuple(ObliviousTupleBox tp) override;
        void setLeftObliviousTuple(ObliviousTupleBox ltp);
        void setRightObliviousTuple(ObliviousTupleBox rtp);

        db::obdata::obfield::ObliviousField evaluate() const override;
        serialization::ExpressionCode getExpressionType() const override;
    private:
        ObliviousExpression _pred;
    };
}
}

#endif //VAULTDB_OBLIVIOUSCOMPARISON_H