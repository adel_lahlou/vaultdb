#ifndef VAULTDB_OBLIVIOUSEXPRESSIONLIST_H
#define VAULTDB_OBLIVIOUSEXPRESSIONLIST_H

#include <vector>
#include "ObliviousExpression.h"
#include "in_sgx/obdata/ObliviousTuple.h"

namespace plan {
namespace obexpressions {

    class ObliviousExpressionList {
    public:
        ObliviousExpressionList();
        ObliviousExpressionList(std::vector<ObliviousExpression> es);
        ObliviousExpressionList(std::initializer_list<ObliviousExpression> exps);
        ~ObliviousExpressionList();

        bool isAllTruthy() const;
        bool isAnyTruthy() const;
        int size() const;
        void setObliviousTuple(ObliviousTupleBox t);
        db::obdata::ObliviousTuple evaluate() const;
        type::SchemaColumnHolder outputDatatype() const;

        void push_back(ObliviousExpression e);

        ObliviousExpression& operator[](size_vdb pos);
        const ObliviousExpression& operator[](size_vdb pos) const;
    protected:
        std::vector<ObliviousExpression> obexpressions;
    };
}
}

#endif //VAULTDB_OBLIVIOUSEXPRESSIONLIST_H