#ifndef VAULTDB_AGGREGATEOBLIVIOUSEXPRESSIONLIST_H
#define VAULTDB_AGGREGATEOBLIVIOUSEXPRESSIONLIST_H

#include <vector>
#include "ObliviousExpression.h"
#include "Aggregates.h"
#include "in_sgx/obdata/ObliviousTuple.h"

namespace plan {
namespace obexpressions {

    class AggregateObliviousExpressionList {
    public:
        AggregateObliviousExpressionList();
        AggregateObliviousExpressionList(const std::vector<AggregateFunction>& aggs);
        AggregateObliviousExpressionList(std::initializer_list<AggregateFunction> aggs);
        ~AggregateObliviousExpressionList();

        bool isAllTruthy() const;
        bool isAnyTruthy() const;
        int size() const;
        bool processedDummy() const;
        void setObliviousTuple(ObliviousTupleBox t);
        void update(bool go);
        void reset(bool go);
        db::obdata::ObliviousTuple evaluate() const;

        void push_back(AggregateFunction e);

        ObliviousExpression& operator[](size_vdb pos);
        const ObliviousExpression& operator[](size_vdb pos) const;
    private:
        std::vector<AggregateFunction> obexpressions;
        bool _processedDummy = false;
    };
}
}

#endif //VAULTDB_AGGREGATEOBLIVIOUSEXPRESSIONLIST_H
