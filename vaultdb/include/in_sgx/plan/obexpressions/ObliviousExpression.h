#ifndef VAULTDB_OBLIVIOUSEXPRESSION_H
#define VAULTDB_OBLIVIOUSEXPRESSION_H

#include "in_sgx/obdata/obfield/ObliviousField.h"
#include "in_sgx/obdata/ObliviousTuple.h"
#include <shared/QueryEncodingDefinitions.h>

// TODO: should not use a shared pointer
// TODO: move factory functions into seperate class

namespace plan {
namespace obexpressions {

    struct BaseConstructor { BaseConstructor(int=0) {} };

    class ObliviousExpression;

    typedef db::obdata::ObliviousTuple* ObliviousTupleBox;
    typedef std::shared_ptr<ObliviousExpression> ObliviousExpressionPtr;
    extern void swap(plan::obexpressions::ObliviousExpression& e1, plan::obexpressions::ObliviousExpression& e2);

    class ObliviousExpression {
        friend class BinaryExpression;
        friend class UnaryExpression;
        friend class ValueExpression;

        friend class Literal;
        friend class Column;

        friend class Add;
        friend class Sub;
        friend class Mul;
        friend class Div;

        friend class Eq;
        friend class NEq;
        friend class LT;
        friend class GT;
        friend class LTE;
        friend class GTE;

        friend class And;
        friend class Or;
        friend class Not;

        friend class JoinObliviousExpression;
        friend class ObliviousExpressionFactory;
    public:
        ObliviousExpression&operator=(const ObliviousExpression& rhs);
        ObliviousExpression(const ObliviousExpression& e);
        virtual ~ObliviousExpression();

        void swap(ObliviousExpression& e) throw();

        virtual void setObliviousTuple(ObliviousTupleBox tp);

        virtual db::obdata::obfield::ObliviousField evaluate() const;
        virtual bool isAggregateFunction() const;
        virtual type::SchemaColumn outputDatatype() const;

        virtual void encodeObliviousExpression(unsigned char * & writeHead) const;
        virtual size_vdb encodeSize() const;
        virtual void getInputReferences(std::vector<pos_vdb> & output) const;

        virtual serialization::ExpressionCode getExpressionType() const;

        void setGenLevel(std::vector<pos_vdb> &genLevels);
    protected:
        ObliviousExpression();
        ObliviousExpression(BaseConstructor);
        virtual void enforceGenLevel(size_vdb genLevel);

        ObliviousExpressionPtr rep;
    private:
        void redefine(ObliviousExpressionPtr p);
    };
}
}
#endif //VAULTDB_OBLIVIOUSEXPRESSION_H