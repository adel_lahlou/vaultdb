#ifndef VAULTDB_QUERYENCODINGDEFINITIONS_H
#define VAULTDB_QUERYENCODINGDEFINITIONS_H

namespace serialization {

    enum ExpressionCode {
        UNSPECIFIED = 0xFF,

        COLUMN = 0x00,

        LITERAL = 0x40,

        PLUS = 0x81,
        MINUS,
        TIMES,
        DIVIDE,
        MINUS_PREFIX,
        EQUALS,
        LESS_THAN,
        GREATER_THAN,
        LESS_THAN_OR_EQUAL,
        GREATER_THAN_OR_EQUAL,
        AND,
        OR,
        NOT,
        NOT_EQUALS,
        JOIN_EQUALITY,

        COUNT = 0xc1,
        COUNT_STAR,
        COUNT_DIST,
        SUM,
        AVG,
        MAX,
        MIN,
        ROW_NUMBER,
        RANK,
    };

    enum OperatorCode {
        PLAINTEXT_SCAN = 0x1,
        PLAINTEXT_JOIN,
        PLAINTEXT_SORT,
        PLAINTEXT_AGGREGATE,
        PLAINTEXT_FILTER, // 0x5
        PLAINTEXT_LIMIT,
        PLAINTEXT_UNION,
        PLAINTEXT_PROJECT,
        KANO_SCAN = 0x21,
        KANO_JOIN,
        KANO_SORT,
        KANO_AGGREGATE,
        KANO_FILTER,
        KANO_GEN_FILTER,
        KANO_LIMIT,
        KANO_UNION,
        KANO_PROJECT,
        KANO_SOURCE_FROM_PLAINTEXT,
        KANO_CLUSTER_HASHING,
        KANO_CLUSTER_REPARTITION,
        KANO_CLUSTER_BITONIC_MERGE_SORT,
        KANO_CLUSTER_GEN_HASH_JOIN,
        KANO_CLUSTER_HASH_NESTED_LOOP_JOIN,
        KANO_CLUSTER_POOLED_JOIN,
        KANO_CLUSTER_SORTED_AGGREGATION,
        KANO_CLUSTER_LOCAL_DISTRO_AGGREGATION,
        KANO_CLUSTER_BINNED_AGGREGATION,
        KANO_CLUSTER_LIMIT,
        KANO_CLUSTER_COLUMN_SORT,
        KANO_CLUSTER_SORTED_WINDOW,
        OBLIVIOUS_SCAN = 0x91,
        OBLIVIOUS_JOIN,
        OBLIVIOUS_SORT,
        OBLIVIOUS_AGGREGATE,
        OBLIVIOUS_FILTER,
        OBLIVIOUS_LIMIT,
        OBLIVIOUS_UNION,
        OBLIVIOUS_PROJECT,
        OBLIVIOUS_SOURCE_FROM_PLAINTEXT,
        OBLIVIOUS_SOURCE_FROM_KANO,
        OBLIVIOUS_CLUSTER_HASHING,
        OBLIVIOUS_CLUSTER_HASH_MERGE_SORT,
        OBLIVIOUS_CLUSTER_HASH_JOIN,
        OBLIVIOUS_CLUSTER_HASH_SORTED_AGGREGATION,
    };

    enum JoinType {
        INNER = 0x1,
        LEFT,
        RIGHT,
        CROSS_PRODUCT,
    };

    enum OperatorType {
        UNKNOWN = 0x0,
        PLAINTEXT,
        KANONYMOUS,
        OBLIVIOUS,
    };

    enum ParserStatus {
        Ok = 0x1,
        FatalFailure,
        TableMissing,
    };
}
#endif //VAULTDB_QUERYENCODINGDEFINITIONS_H
