#ifndef VAULTDB_FIELDDATATYPE_H
#define VAULTDB_FIELDDATATYPE_H

#include <vector>
#include "shared/Definitions.h"

namespace type {

    enum class FieldDataType : uint8_vdb {
        Invalid = 0,
        DecideAtRunTime,
        Bool,
        Int,
        BigInt,
        Real,
        DoublePrecision,
        Fixchar,
        Varchar,
        TimestampNoZone,
    };

    struct SchemaColumn {
        FieldDataType type;
        size_vdb size;
    } ;

    typedef std::vector<SchemaColumn> SchemaColumnHolder;
    typedef std::vector<FieldDataType> FieldDataTypeHolder;
}

#endif //VAULTDB_FIELDDATATYPE_H
