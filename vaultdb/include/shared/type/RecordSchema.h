#ifndef VAULTDB_RECORDSCHEMA_H
#define VAULTDB_RECORDSCHEMA_H

#include <string>
#include <vector>
#include <initializer_list>
#include "shared/Definitions.h"
#include "FieldDataType.h"

namespace type {

        class RecordSchema {
            public:
                RecordSchema();
                RecordSchema(TableID id, const SchemaColumnHolder &ts);
                RecordSchema(const RecordSchema& rs);
                RecordSchema& operator =(const RecordSchema& rs);

                void addField(SchemaColumn t);
                void erase(size_vdb pos);
                void project(std::vector<size_vdb> poses);
                static RecordSchema join(const RecordSchema& r1, const RecordSchema& r2);
                const TableID getTableID() const;
                void setTableID(TableID id);
                size_vdb serializeTo(unsigned char * buf) const;
                size_vdb serializationSize() const;
                size_vdb tupleSerializationSize() const;
                static RecordSchema deserializeFrom(const unsigned char * data);

                size_vdb size() const;
                const SchemaColumn& operator[](size_vdb pos) const;
                SchemaColumn& operator[](size_vdb pos);

                bool operator==(RecordSchema comparison);
                bool operator==(const RecordSchema & comparison) const;
            private:
                SchemaColumnHolder types;
                TableID tableid;
            };
    }

#endif //VAULTDB_RECORDSCHEMA_H
