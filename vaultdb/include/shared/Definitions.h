#ifndef VAULTDB_DEFINITIONS_H
#define VAULTDB_DEFINITIONS_H

#include <cstdint>
#include <vector>
#include <ctime>
#include <memory>

typedef uint8_t     uint8_vdb;
typedef uint32_t    pos_vdb;
typedef uint32_t    size_vdb;
typedef uint32_t    TableID;
typedef uint32_t    MachineID;
typedef uint32_t    TransmitterID;
typedef uint64_t    HashNumber;
typedef uint32_t    MessageID;
typedef uint32_t    PartitionID;
typedef uint32_t    BlockID;

struct Message {
    TableID src_tid;
    unsigned char data[];
};

struct DataSizePair {
    size_vdb size;
    std::shared_ptr<unsigned char> data;
};

class MachineTableIDPair {
public:
    MachineID machineID;
    TableID tableID;
    MachineTableIDPair(MachineID mid, TableID tid);
    MachineTableIDPair& operator=(const MachineTableIDPair &comp);
    bool operator<(const MachineTableIDPair &comp) const;
    bool operator==(const MachineTableIDPair &comp) const;
};

enum class TransmissionType : unsigned char {
    OneStepQuery = 0x12,
    OneFinalQuery = 0x23,
    OneStepPostgreSQLQuery = 0x28,
    OneFinalPostgreSQLQuery = 0x2A,
    WholeObliviousTupleTable = 0x45,
    WholeObliviousPagedTable = 0x68,
    ResultObliviousTupleTable = 0x93,
    ResultObliviousPagedTable = 0x95,
    AcknowledgeQueryComplete = 0xa2
};

enum class GenericReturnStatus {
    FatalError = -1,
    Ok = 0
};

enum class OperatorStatus {
    FatalError = -1,
    Ok = 0,
    NoMoreTuples = 1,
    NotInitialized = 2,
    WaitingForTuples = 3
};

enum class SortOrder {
    ASCEND = 0,
    DESCEND = 1
};

template <typename T>
T swap_endian(T u)
{
    /**
     * Grabbed from https://stackoverflow.com/a/4956493
     * Original text by Alexandre C.
     */
//    static_assert (CHAR_BIT == 8, "CHAR_BIT != 8");

    union
    {
        T u;
        unsigned char u8[sizeof(T)];
    } source, dest;

    source.u = u;

    for (size_vdb k = 0; k < sizeof(T); k++)
        dest.u8[k] = source.u8[sizeof(T) - k - 1];

    return dest.u;
}

int swap_int_endian(int u);

short swap_short_endian(short u);

extern int int_10_power[10];

extern int time_t_divisor[6];

//#define GATEWAY_DEBUG
#define DEFAULT_TABLE_PARTITION_ID 0

#define RETURN_FAILURE -1
#define RETURN_SUCCESS 0

#define INT_FIELD_SIZE sizeof(int)
#define TIMENOZONE_FIELD_SIZE sizeof(time_t)

#define FINAL_QUERY 1
#define STEP_QUERY 0

#define DEFAULT_MAX_GEN_LEVEL 255
#define GEN_LEVELS_TRIED 10

#define DEFAULT_K 5

#define INPUT_TEST_TABLE_NOT_SPECIFIED 0

#define SYNC_MESSAGE_TABLEID 0

#define FIVE_MEGA_BYTES 5242880

#endif //VAULTDB_DEFINITIONS_H
