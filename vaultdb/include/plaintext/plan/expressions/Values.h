#ifndef VAULTDB_VALUES_H
#define VAULTDB_VALUES_H

#include "Expression.h"
#include "plaintext/data/field/Field.h"

namespace plan {
namespace expressions {
    class ValueExpression : public Expression {
        bool isAggregateFunction() const;
        virtual void setTuple(TupleBox tp)=0;
        virtual db::data::field::Field evaluate() const=0;
    };

    class Literal : public ValueExpression {
        friend class Expression;
    public:
        Literal(db::data::field::Field v);

        void setTuple(TupleBox tp);
        db::data::field::Field evaluate() const;
    private:
        Literal();
        db::data::field::Field value;
    };


    class Column : public ValueExpression {
        friend class Expression;
    public:
        Column(size_vdb colNum);

        void setTuple(TupleBox tp);

        db::data::field::Field evaluate() const;
    private:
        Column();
        TupleBox curTp;
        size_vdb col;
    };
}
}

#endif