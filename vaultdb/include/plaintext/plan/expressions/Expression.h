#ifndef VAULTDB_EXPRESSION_H
#define VAULTDB_EXPRESSION_H

#include "plaintext/data/field/Field.h"
#include "plaintext/data/Tuple.h"

// TODO: move factory functions into seperate class

namespace plan {
namespace expressions {

    struct BaseConstructor { BaseConstructor(int=0) {} };

    class Expression;

    class Literal;
    class Column;

    class Add;
    class Sub;
    class Mul;
    class Div;

    class Eq;
    class NEq;
    class LT;
    class GT;
    class LTE;
    class GTE;

    class And;
    class Or;
    class Not;

    class JoinExpression;

    typedef db::data::Tuple* TupleBox;
    typedef std::shared_ptr<Expression> ExpressionPtr;
    extern void swap(plan::expressions::Expression& e1, plan::expressions::Expression& e2);

    class Expression {
        friend class Literal;
        friend class Column;

        friend class Add;
        friend class Sub;
        friend class Mul;
        friend class Div;

        friend class Eq;
        friend class NEq;
        friend class LT;
        friend class GT;
        friend class LTE;
        friend class GTE;

        friend class And;
        friend class Or;
        friend class Not;

        friend class JoinExpression;
        friend class Sum;
        friend class Count;
    public:
        Expression&operator=(const Expression& rhs);
        Expression(const Expression& e);
        virtual ~Expression();

        void swap(Expression& e) throw();

        virtual void setTuple(TupleBox tp);

        static Expression makeLiteral(db::data::field::Field f);
        static Expression makeColumn(size_vdb colNum);
        static Expression makeAdd(Expression lhs, Expression rhs);
        static Expression makeSub(Expression lhs, Expression rhs);
        static Expression makeMul(Expression lhs, Expression rhs);
        static Expression makeDiv(Expression lhs, Expression rhs);
        static Expression makeEq(Expression lhs, Expression rhs);
        static Expression makeNEq(Expression lhs, Expression rhs);
        static Expression makeLT(Expression lhs, Expression rhs);
        static Expression makeGT(Expression lhs, Expression rhs);
        static Expression makeLTE(Expression lhs, Expression rhs);
        static Expression makeGTE(Expression lhs, Expression rhs);
        static Expression makeAnd(Expression lhs, Expression rhs);
        static Expression makeOr(Expression lhs, Expression rhs);
        static Expression makeNot(Expression exp);
        static Expression makeJoin(Expression lhs, Expression rhs);
        static Expression makeSum(size_vdb colNum);
        static Expression makeCount(size_vdb colNum);

        virtual bool isAggregateFunction() const;
        virtual db::data::field::Field evaluate() const;
        type::SchemaColumn outputDatatype() const;
    protected:
        Expression();
        Expression(BaseConstructor);
        ExpressionPtr rep;
    private:
        void redefine(ExpressionPtr p);
    };
}
}
#endif //VAULTDB_EXPRESSION_H
