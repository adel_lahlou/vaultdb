#ifndef VAULTDB_COMPARISON_H
#define VAULTDB_COMPARISON_H

#include "Expression.h"

namespace plan{
namespace expressions {
    class ComparisonExpression : public Expression {
        bool isAggregateFunction() const;
        virtual void setTuple(TupleBox tp)=0;
        virtual db::data::field::Field evaluate() const=0;
    };

    class Eq : public ComparisonExpression {
    public:
        Eq(Expression lhs, Expression rhs);
        ~Eq();

        void setTuple(TupleBox tp);
        db::data::field::Field evaluate() const;
    private:
        Eq();
        Expression
                _lhs,
                _rhs;
    };


    class NEq : public ComparisonExpression {
    public:
        NEq(Expression lhs, Expression rhs);
        ~NEq();

        void setTuple(TupleBox tp);
        db::data::field::Field evaluate() const;
    private:
        NEq();
        Expression
                _lhs,
                _rhs;
    };


    class GT : public ComparisonExpression {
    public:
        GT(Expression lhs, Expression rhs);
        ~GT();

        void setTuple(TupleBox tp);
        db::data::field::Field evaluate() const;
    private:
        GT();
        Expression
                _lhs,
                _rhs;
    };


    class LT : public ComparisonExpression {
    public:
        LT(Expression lhs, Expression rhs);
        ~LT();

        void setTuple(TupleBox tp);
        db::data::field::Field evaluate() const;
    private:
        LT();
        Expression
                _lhs,
                _rhs;
    };

    class GTE : public ComparisonExpression {
    public:
        GTE(Expression lhs, Expression rhs);
        ~GTE();

        void setTuple(TupleBox tp);
        db::data::field::Field evaluate() const;
    private:
        GTE();
        Expression
                _lhs,
                _rhs;
    };


    class LTE : public ComparisonExpression {
    public:
        LTE(Expression lhs, Expression rhs);
        ~LTE();

        void setTuple(TupleBox tp);
        db::data::field::Field evaluate() const;
    private:
        LTE();
        Expression
                _lhs,
                _rhs;
    };
}
}

#endif //VAULTDB_COMPARISON_H
