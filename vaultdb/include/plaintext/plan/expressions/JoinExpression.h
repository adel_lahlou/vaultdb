#ifndef VAULTDB_JOINEXPRESSION_H
#define VAULTDB_JOINEXPRESSION_H

#include <vector>
#include "Expression.h"

namespace plan {
namespace expressions {
    class JoinExpression : public Expression {
        friend class Expression;
    public:
        JoinExpression(Expression lhs, Expression rhs);
        JoinExpression();
        ~JoinExpression();

        void setTuple(TupleBox tp);
        void setLeftTuple(TupleBox ltp);
        void setRightTuple(TupleBox rtp);

        db::data::field::Field evaluate() const;
    private:
        Expression
                _pred,
                _lhs,
                _rhs;
    };
}
}

#endif //VAULTDB_JOINEXPRESSION_H
