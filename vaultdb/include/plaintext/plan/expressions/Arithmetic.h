#ifndef VAULTDB_ARITHMETIC_H
#define VAULTDB_ARITHMETIC_H

#include "Expression.h"
// TODO: investigate a BinaryOp parent class with lambda func

namespace plan{
namespace expressions {
    class ArithmeticExpression : public Expression {
        bool isAggregateFunction() const;
        virtual void setTuple(TupleBox tp)=0;
        virtual db::data::field::Field evaluate() const=0;
    };

    class Add : public ArithmeticExpression {
        friend class Expression;
    public:
        Add(Expression lhs, Expression rhs);
        ~Add();

        void setTuple(TupleBox tp);
        db::data::field::Field evaluate() const;
    private:
        Add();
        Expression
                _lhs,
                _rhs;
    };


    class Sub : public ArithmeticExpression {
    public:
        Sub(Expression lhs, Expression rhs);
        ~Sub();

        void setTuple(TupleBox tp);
        db::data::field::Field evaluate() const;
    private:
        Sub();
        Expression
                _lhs,
                _rhs;
    };


    class Mul : public ArithmeticExpression {
    public:
        Mul(Expression lhs, Expression rhs);
        ~Mul();

        void setTuple(TupleBox tp);
        db::data::field::Field evaluate() const;
    private:
        Mul();
        Expression
               _lhs,
               _rhs;
    };


    class Div : public ArithmeticExpression {
    public:
        Div(Expression lhs, Expression rhs);
        ~Div();

        void setTuple(TupleBox tp);
        db::data::field::Field evaluate() const;
    private:
        Div();
        Expression
               _lhs,
               _rhs;
    };
}
}

#endif //VAULTDB_ARITHMETIC_H
