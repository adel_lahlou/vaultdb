#ifndef VAULTDB_AGGREGATE_H
#define VAULTDB_AGGREGATE_H

#include "Expression.h"
#include "Values.h"

namespace plan {
namespace expressions {
    class Sum;
    class Count;

    class AggregateFunction : public Expression {
    public:
        AggregateFunction();
        AggregateFunction(const Expression& other);
        bool isAggregateFunction() const;
        using Expression::setTuple;
        using Expression::evaluate;

        virtual void update();
        virtual void reset();
    };

    class Sum : public AggregateFunction {
    public:
        Sum(size_vdb colNum);
        Sum(Column col);
        ~Sum();

        void setTuple(TupleBox tp);
        void update();
        void reset();
        db::data::field::Field evaluate() const;
    private:
        Column column;
        db::data::field::Field sum = db::data::field::Field::makeIntField(0);
    };

    class Count : public AggregateFunction {
    public:
        Count(size_vdb colNum);
        Count(Column col);
        ~Count();

        void setTuple(TupleBox tp);
        void update();
        void reset();
        db::data::field::Field evaluate() const;
    private:
        Column column;
        int count = 0;
    };
}
}
#endif //VAULTDB_AGGREGATE_H
