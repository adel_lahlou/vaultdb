#ifndef VAULTDB_EXPRESSIONLIST_H
#define VAULTDB_EXPRESSIONLIST_H

#include <vector>
#include "Expression.h"
#include "plaintext/data/Tuple.h"

namespace plan {
namespace expressions {

    class ExpressionList {
    public:
        ExpressionList();
        ExpressionList(std::vector<Expression> es);
        ExpressionList(std::initializer_list<Expression> es);
        ~ExpressionList();

        bool isAllTruthy() const;
        bool isAnyTruthy() const;
        int size() const;
        void setTuple(TupleBox t);
        db::data::Tuple evaluate() const;
        type::SchemaColumnHolder outputDatatype() const;

        void push_back(Expression e);

        Expression& operator[](size_vdb pos);
        const Expression& operator[](size_vdb pos) const;
    protected:
        std::vector<Expression> expressions;
    };
}
}

#endif //VAULTDB_EXPRESSIONLIST_H
