#ifndef VAULTDB_AGGREGATEEXPRESSIONLIST_H
#define VAULTDB_AGGREGATEEXPRESSIONLIST_H

#include <vector>
#include "Expression.h"
#include "Aggregates.h"
#include "plaintext/data/Tuple.h"

namespace plan {
namespace expressions {

    class AggregateExpressionList {
    public:
        AggregateExpressionList();
        AggregateExpressionList(const std::vector<AggregateFunction>& aggs);
        AggregateExpressionList(std::initializer_list<AggregateFunction> aggs);
        ~AggregateExpressionList();

        bool isAllTruthy() const;
        bool isAnyTruthy() const;
        int size() const;
        void setTuple(TupleBox t);
        void update();
        void reset();
        db::data::Tuple evaluate() const;

        void push_back(AggregateFunction e);

        Expression& operator[](size_vdb pos);
        const Expression& operator[](size_vdb pos) const;
    private:
        std::vector<AggregateFunction> expressions;
    };
}
}
#endif //VAULTDB_AGGREGATEEXPRESSIONLIST_H
