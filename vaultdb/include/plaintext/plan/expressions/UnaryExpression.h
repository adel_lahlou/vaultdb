#ifndef VAULTDB_LOGICAL_H
#define VAULTDB_LOGICAL_H

#include "Expression.h"
namespace plan{
namespace expressions {
    class LogicalExpression : public Expression {
        bool isAggregateFunction() const;
        virtual void setTuple(TupleBox tp)=0;
        virtual db::data::field::Field evaluate() const=0;
    };

    class And : public LogicalExpression {
    public:
        And(Expression lhs, Expression rhs);
        ~And();

        void setTuple(TupleBox tp);
        db::data::field::Field evaluate() const;
    private:
        And();
        Expression
                _lhs,
                _rhs;
    };


    class Or : public LogicalExpression {
    public:
        Or(Expression lhs, Expression rhs);
        ~Or();

        void setTuple(TupleBox tp);
        db::data::field::Field evaluate() const;
    private:
        Or();
        Expression
                _lhs,
                _rhs;
    };


    class Not : public LogicalExpression {
    public:
        Not(Expression e);
        ~Not();

        void setTuple(TupleBox tp);
        db::data::field::Field evaluate() const;
    private:
        Not();
        Expression _exp;
    };
}
}

#endif //VAULTDB_LOGICAL_H
