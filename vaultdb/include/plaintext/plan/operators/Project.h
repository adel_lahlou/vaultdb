#ifndef VAULTDB_PROJECT_H
#define VAULTDB_PROJECT_H


#include <string>
#include "Operator.h"
#include "plaintext/plan/expressions/ExpressionList.h"

namespace type {
    class RecordSchema;
}

namespace plan {
    namespace operators {
        class Project : public Operator {
        public:
            Project(TableID opId,
                    OperatorPtr child,
                    plan::expressions::ExpressionList exps
            );
            ~Project();

            OperatorStatus next();
            void reset();
        private:
            expressions::TupleBox _projectTuple;
            plan::expressions::ExpressionList _exps;
        };
    }
}

#endif //VAULTDB_PROJECT_H
