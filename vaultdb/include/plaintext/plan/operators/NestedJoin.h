#ifndef VAULTDB_JOIN_H_H
#define VAULTDB_JOIN_H_H

#include <string>
#include "Operator.h"
#include "plaintext/data/Tuple.h"
#include "shared/type/RecordSchema.h"
#include "plaintext/plan/expressions/JoinExpression.h"

namespace plan {
namespace operators {

    class NestedJoin : public Operator {
    public:
        NestedJoin(TableID opId,
             OperatorPtr leftChild,
             OperatorPtr rightChild,
             plan::expressions::JoinExpression joinPred
        );
        ~NestedJoin();

        OperatorPtr getLeftChild() const;
        OperatorPtr getRightChild() const;

        OperatorStatus next();
        void reset();
    private:
        plan::expressions::TupleBox _leftJoinTuple;
        plan::expressions::TupleBox _rightJoinTuple;
        plan::expressions::JoinExpression _joinPred;
    };
}
}

#endif //VAULTDB_JOIN_H_H
