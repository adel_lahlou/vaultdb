#ifndef VAULTDB_SEQSCAN_H
#define VAULTDB_SEQSCAN_H

#include <string>
#include "Operator.h"
#include "plaintext/data/TupleTable.h"
#include "shared/type/RecordSchema.h"
#include "plaintext/plan/expressions/Expression.h"

namespace plan {
namespace operators {
    class SeqScan : public Operator {
    public:
        SeqScan(TableID opId,
                type::RecordSchema& outSchema,
                OperatorPtr parent,
                db::data::TupleTable inputTable
        );
        ~SeqScan();

        OperatorStatus next();
        void reset();
    private:
        size_vdb _scanPos = 0;
        db::data::TupleTable _inputTable;
    };
}
}

#endif //VAULTDB_SEQSCAN_H
