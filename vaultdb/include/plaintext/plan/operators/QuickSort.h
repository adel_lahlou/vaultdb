#ifndef VAULTDB_QUICKSORT_H
#define VAULTDB_QUICKSORT_H

#include <string>
#include "Operator.h"
#include "plaintext/data/Tuple.h"
#include "plaintext/plan/expressions/Expression.h"

namespace plan {
namespace operators {

    enum SortDirection {
        Ascending = 1,
        Descending = -1
    };

    class QuickSort : public Operator {
    public:
        QuickSort(TableID opId,
                  OperatorPtr child,
                  std::vector<size_vdb> sortIndices,
                  SortDirection direction
        );
        ~QuickSort();

        OperatorStatus next();
        void reset();
    private:
        bool _hasTuplesLeft() const;
        void _readInChild();
        void _quicksort(int low, int high);
        int _partition(int low, int high);
        std::vector<db::data::Tuple> _output;
        std::vector<size_vdb> _sortIndices;
        SortDirection _direction;
        size_vdb _scanPos = 0;
        bool _sorted = false;
    };
}
}

#endif //VAULTDB_QUICKSORT_H
