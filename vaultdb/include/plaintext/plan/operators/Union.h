#ifndef VAULTDB_UNION_H
#define VAULTDB_UNION_H


#include <string>
#include "Operator.h"


namespace plan {
namespace operators {
    class Union : public Operator {
    public:
        Union(TableID opId, OperatorPtrList children);
        ~Union();

        OperatorStatus next();
        void reset();
    private:
        OperatorPtr getCurrentChild() const;
        int _curChild = 0;
    };
}
}

#endif //VAULTDB_UNION_H
