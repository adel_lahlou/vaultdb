#ifndef VAULTDB_OPERATOR_H
#define VAULTDB_OPERATOR_H

#include <string>
#include <vector>
#include "plaintext/data/Tuple.h"
#include "shared/type/RecordSchema.h"

namespace plan {
namespace operators {
    class Operator;

    struct BaseConstructor { BaseConstructor(int=0) {} };
    typedef Operator * OperatorPtr;
    typedef std::vector<OperatorPtr> OperatorPtrList;

    class Operator {
    public:
        Operator(
            TableID opId,
            type::RecordSchema outSchema,
            bool blocking,
            Operator* parent,
            OperatorPtrList children);
        virtual ~Operator();

        const OperatorStatus getStatus() const;
        const Operator* getChild(size_vdb child) const;
        const std::vector<Operator*>& getChildren() const;
        const type::RecordSchema& getChildSchema(int child) const;
        const type::RecordSchema& getSchema() const;
        const Operator* getParent() const;
        bool isBlocking() const;

        OperatorStatus getCurrentTuple(db::data::Tuple& out) const;

        void setParent(Operator* p);
        virtual OperatorStatus next()=0;
        virtual void reset();

        virtual void encodeOperator(unsigned char *&writeHead);

    protected:
        Operator(const Operator& o);
        Operator();

        Operator * _parent;
        std::vector<Operator*> _children;
        type::RecordSchema _outSchema;
        OperatorStatus _status;
        TableID _operatorId;

        bool _blocking;
        db::data::Tuple _currentTuple;
    };
}
}
#endif //VAULTDB_OPERATOR_H
