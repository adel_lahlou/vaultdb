#ifndef VAULTDB_SORTEDGROUP_H
#define VAULTDB_SORTEDGROUP_H

#include <string>
#include "Operator.h"
#include "plaintext/data/TupleTable.h"
#include "shared/type/RecordSchema.h"
#include "plaintext/plan/expressions/Expression.h"
#include "plaintext/plan/expressions/AggregateExpressionList.h"
#include "plaintext/plan/expressions/ExpressionList.h"

namespace plan {
namespace operators {

    class SortedGroup : public Operator {
    public:
        SortedGroup(
                TableID opId,
                OperatorPtr child,
                expressions::ExpressionList aggs,
                std::vector<size_vdb> groupBys
        );
        ~SortedGroup();

        OperatorStatus next();
        void reset();
    private:
        expressions::TupleBox _lastAggTuple;
        expressions::TupleBox _curAggTuple;
        expressions::AggregateExpressionList _aggs;
        expressions::ExpressionList _exps;
        expressions::ExpressionList _checkInGroup;
        bool _firstPull = true;
    };
}
}

#endif // VAULTDB_SORTEDGROUP_H
