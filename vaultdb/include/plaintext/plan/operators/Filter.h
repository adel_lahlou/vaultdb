#ifndef VAULTDB_FILTER_H
#define VAULTDB_FILTER_H

#include <string>
#include "Operator.h"
#include "plaintext/data/TupleTable.h"
#include "shared/type/RecordSchema.h"
#include "plaintext/plan/expressions/Expression.h"

namespace plan {
namespace operators {
    class Filter : public Operator {
    public:
        Filter(TableID opId,
                type::RecordSchema& outSchema,
                OperatorPtr parent,
                OperatorPtr child,
                plan::expressions::Expression filterPred
        );
        ~Filter();

        OperatorStatus next();
        void reset();
    private:
        plan::expressions::TupleBox _filterTuple;
        plan::expressions::Expression _filterPred;
    };
}
}

#endif //VAULTDB_FILTER_H
