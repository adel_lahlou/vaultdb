#ifndef VAULTDB_TUPLE_H
#define VAULTDB_TUPLE_H

#include <string>
#include <vector>
#include <initializer_list>
#include "plaintext/data/field/Field.h"
#include "shared/Definitions.h"

namespace db {
namespace data {
    typedef std::vector<field::Field> FieldHolder;
    typedef std::shared_ptr<FieldHolder> FieldHolderPtr;
    typedef std::vector<size_vdb> ProjectList;

    class Tuple {
    public:
        Tuple();
        Tuple(const FieldHolder &fs);
        Tuple(bool dummy, const FieldHolder &fs);
        Tuple(std::initializer_list<field::Field> fs);
        Tuple(const Tuple &t);
        Tuple& operator =(const Tuple& t);
        ~Tuple();

        size_vdb size() const;
        bool isDummy() const;
        std::string toString() const;
        int serializeTo(char * buf, size_vdb limit) const;
        size_vdb serializationSize() const;
        void swap(Tuple& t) throw();

        void setDummyFlag(bool flag);
        void push_back(field::Field f);
        void erase(size_vdb pos);
        void project(ProjectList poses);
        static Tuple join(const Tuple& t1, const Tuple& t2);

        const field::Field &operator[](size_vdb pos) const;
        field::Field &operator[](size_vdb pos);
        static int compare(const Tuple& t1, const Tuple& t2, const std::vector<size_vdb>& compIndices);
    private:
        void get_fields_ownership();
        bool dummyFlag = false;
        FieldHolderPtr fields;
    };
}
}

#endif //VAULTDB_TUPLE_H
