#ifndef VAULTDB_TUPLEFACTORY_H
#define VAULTDB_TUPLEFACTORY_H

#include "Tuple.h"
#include "shared/type/RecordSchema.h"

namespace db{
namespace data {
    class DataFactory {
    public:
        static Tuple createDefaultTuple(const type::RecordSchema& schema);
        static field::Field createDefaultField(type::SchemaColumn type);
    };
}
}
#endif //VAULTDB_TUPLEFACTORY_H
