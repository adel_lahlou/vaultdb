#ifndef VAULTDB_TUPLETABLE_H
#define VAULTDB_TUPLETABLE_H

#include <vector>
#include <string>
#include <initializer_list>
#include "Tuple.h"
#include "shared/type/RecordSchema.h"
#include "shared/Definitions.h"

namespace db {
namespace data {
    typedef std::vector<Tuple> TupleList;
    typedef std::shared_ptr<TupleList> TupleListPtr;

    class TupleTable {
    public:
        TupleTable(const type::RecordSchema& s);
        TupleTable(const type::RecordSchema& s, const TupleList& ts);
        TupleTable(const TupleTable& tt);
        TupleTable& operator=(const TupleTable& tt);

        size_vdb size() const;
        const type::RecordSchema& getSchema() const;
        std::string toString() const;
        int serializeTo(char * buf, size_vdb limit) const;
        size_vdb serializationSize() const;

        TableID getTableID();
        void setTableID(TableID id);
        void addTuple(Tuple& t);
        void addTuple(Tuple&& t);
        void setSchema(const type::RecordSchema& s);
        void erase(size_vdb pos);

        Tuple& operator[](size_vdb pos);
    private:
        TupleTable();
        TupleListPtr tuples;
        type::RecordSchema schema;
        TableID tableid;
    };
}
}
#endif //VAULTDB_TUPLETABLE_H
