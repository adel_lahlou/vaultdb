#ifndef VAULTDB_INTFIELD_H
#define VAULTDB_INTFIELD_H

#include "Field.h"

namespace db{
namespace data {
namespace field {

    class IntField : public Field
    {
        friend class Field;
        friend class VarcharField;
    public:
        IntField();
        IntField(int v);
        IntField(const IntField &f);
        virtual ~IntField();

        // meta functions
        virtual bool isTruthy() const;
        size_vdb size() const;
        type::SchemaColumn dataType() const noexcept;
        std::string toString() const;
        virtual int serializeTo(char * buf, size_vdb limit) const;
        virtual size_vdb serializationSize() const;
        static Field deserializeFrom(const char * data, size_vdb len);
        const void * getValuePointer() const;

        // algebraic functions
        virtual Field operator + (Field const &f) const;
        virtual Field operator - (Field const &f) const;
        virtual Field operator * (Field const &f) const;
        virtual Field operator / (Field const &f) const;

        // comparison operators
        virtual bool operator==(const Field& rhs) const;
        virtual bool operator!=(const Field& rhs) const;
        virtual bool operator<=(const Field& rhs) const;
        virtual bool operator>=(const Field& rhs) const;
        virtual bool operator>(const Field& rhs) const;
        virtual bool operator<(const Field& rhs) const;

        // algebraic functions implementations
        virtual Field IntFieldAdd(IntField const &f) const;
        virtual Field VarcharFieldAdd(VarcharField const &f) const;
        virtual Field IntFieldSub(IntField const &f) const;
        virtual Field VarcharFieldSub(VarcharField const &f) const;
        virtual Field IntFieldMul(IntField const &f) const;
        virtual Field VarcharFieldMul(VarcharField const &f) const;
        virtual Field IntFieldDiv(IntField const &f) const;
        virtual Field VarcharFieldDiv(VarcharField const &f) const;

        // comparison operators implementations
        virtual bool IntFieldEq(IntField const &f) const;
        virtual bool VarcharFieldEq(VarcharField const &f) const;
        virtual bool IntFieldLt(IntField const &f) const;
        virtual bool VarcharFieldLt(VarcharField const &f) const;
        virtual bool IntFieldGt(IntField const &f) const;
        virtual bool VarcharFieldGt(VarcharField const &f) const;

        int value;
    };
}
}
}

#endif //VAULTDB_INTFIELD_H
