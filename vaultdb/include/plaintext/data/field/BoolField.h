#ifndef VAULTDB_BOOLFIELD_H
#define VAULTDB_BOOLFIELD_H

namespace db {
namespace data {
namespace field {
    // TODO: change to a proper BoolField so getDatatype() is bool
    typedef IntField BoolField;
}
}
}
#endif //VAULTDB_BOOLFIELD_H
