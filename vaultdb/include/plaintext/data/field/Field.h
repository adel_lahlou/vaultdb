#ifndef VAULTDB_FIELD_H
#define VAULTDB_FIELD_H

#include <string>
#include <memory>
#include "shared/type/FieldDataType.h"
#include "shared/Definitions.h"

// TODO: make it so the field copy constructor can use derived classes as well, currently nulls
namespace db {
namespace data {
namespace field {

    struct BaseConstructor { BaseConstructor(int=0) {} };

    class IntField;
    class VarcharField;
    class Field;

    extern void swap(db::data::field::Field& f1, db::data::field::Field& f2);
    typedef std::shared_ptr<Field> FieldPtr;

    class Field
    {
        friend class IntField;
        friend class VarcharField;

    public:
        Field& operator=(const Field& rhs);
        Field(const Field& f);
        virtual ~Field();

        // factory functions
        static Field makeIntField(int v);
        static Field makeBoolField(bool v);
        static Field makeVarcharField(std::string& v);
        static Field makeVarcharField(std::string&& v);
        static Field makeVarcharField(const char * s);
        static Field makeVarcharField(const char * s, size_vdb n);

        // meta functions
        virtual bool isTruthy() const;
        virtual size_vdb size() const;
        virtual type::SchemaColumn dataType() const noexcept;
        virtual std::string toString() const;
        virtual int serializeTo(char * buf, size_vdb limit) const;
        virtual size_vdb serializationSize() const;
        void swap(Field& f) throw();
        virtual const void * getValuePointer() const;

        // algebraic funcs
        virtual Field operator + (Field const &f) const;
        virtual Field operator - (Field const &f) const;
        virtual Field operator * (Field const &f) const;
        virtual Field operator / (Field const &f) const;

        // comparison operators
        virtual bool operator==(const Field& rhs) const;
        virtual bool operator!=(const Field& rhs) const;
        virtual bool operator<=(const Field& rhs) const;
        virtual bool operator>=(const Field& rhs) const;
        virtual bool operator>(const Field& rhs) const;
        virtual bool operator<(const Field& rhs) const;

    protected:
        Field();
        Field(BaseConstructor);
    private:
        void redefine(FieldPtr fp);

        // algebraic functions implementations
        virtual Field IntFieldAdd(IntField const &f) const;
        virtual Field VarcharFieldAdd(VarcharField const &f) const;
        virtual Field IntFieldSub(IntField const &f) const;
        virtual Field VarcharFieldSub(VarcharField const &f) const;
        virtual Field IntFieldMul(IntField const &f) const;
        virtual Field VarcharFieldMul(VarcharField const &f) const;
        virtual Field IntFieldDiv(IntField const &f) const;
        virtual Field VarcharFieldDiv(VarcharField const &f) const;

        // comparison operators implementations
        virtual bool IntFieldEq(IntField const &f) const;
        virtual bool VarcharFieldEq(VarcharField const &f) const;
        virtual bool IntFieldLt(IntField const &f) const;
        virtual bool VarcharFieldLt(VarcharField const &f) const;
        virtual bool IntFieldGt(IntField const &f) const;
        virtual bool VarcharFieldGt(VarcharField const &f) const;

        FieldPtr rep;
    };
}
}
}


#endif //VAULTDB_FIELD_H
