#ifndef VAULTDB_SERIALIZER_H
#define VAULTDB_SERIALIZER_H


#include <cstddef>
#include "shared/Definitions.h"

namespace type {
    class RecordSchema;
}

namespace db {
namespace data {
    class TupleTable;
    class Tuple;

    class Serializer {
    public:
        /* Serialization checks for adequate size and fails before writing.
         * Checks only occur in serializeTupleTable currently. Contract
         * is for future use. No metadata is saved so a RecordSchema is necessary.
         *
         * @param  {char}   buf - pointer to store bytes
         * @param  {size_vdb} limit - max number of bytes to add
         *
         * @return {int}    < 0 indicates failure, otherwise, the number of bytes written
         */
        static char * serializeTupleTable(const TupleTable& tt, size_vdb& serializedSize);
        static TupleTable deserializeTupleTable(const char * data, size_vdb size);
        static Tuple deserializeTuple(const type::RecordSchema& s, const char * data, size_vdb size);

    private:
        Serializer();
    };
}
}
#endif //VAULTDB_SERIALIZER_H
