#ifndef VAULTDB_GATEWAYBETWEENTWOWORLDS_H
#define VAULTDB_GATEWAYBETWEENTWOWORLDS_H

#include "utilities/OutsideController.h"

int InitializeInSGXController(MachineID localID, uint64_t enclaveID);
int LastMileObliviousTupleTableDeliveryIntoEnclave(MachineID src_machine_id, size_vdb size, unsigned char * data);
int LastMileQueryDeliveryIntoEnclave(unsigned char * data, size_vdb size, int isFinalQuery);
int RegisterRemoteMachineInSGX(MachineID machineID);
int RegisterHonestBrokerInSGX(MachineID machineID);
int FirstStepSendObliviousTupleOutOfEnclave(MachineID dst_machine_id, TableID opID, size_vdb size,
                                            unsigned char *data, int isFinalQuery);

// From outside of an enclave
int DeliverInSGXControllerConstructionRequest(MachineID localID, uint64_t enclaveID);
int DeliverQueryIntoEnclave(uint64_t enclaveID, unsigned char * data, size_vdb size, int isFinalQuery);
int DeliveryObliviousTupleTableIntoEnclave(uint64_t enclaveID, MachineID src_machine_id, size_vdb size, unsigned char * data);
int DeliverRequestToRegisterRemoteMachine(uint64_t enclaveID, MachineID machineID);
int DeliverRequestToRegisterHonestBroker(uint64_t enclaveID, MachineID machineID);
int PassOnObliviousTupleTableFromEnclave(MachineID dst_machine_id, TableID opID, size_vdb size,
                                         unsigned char *data, int isFinalQuery);

#endif //VAULTDB_GATEWAYBETWEENTWOWORLDS_H
