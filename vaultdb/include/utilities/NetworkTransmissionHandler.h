#ifndef VAULTDB_NETWORKTRANSMISSIONHANDLER_H
#define VAULTDB_NETWORKTRANSMISSIONHANDLER_H

#include "OutsideController.h"

class NetworkTransmissionHandler {
    friend class OutsideController;
public:
    ~NetworkTransmissionHandler();
    void registerHonestBroker(MachineID machineID, std::string hostNamePort);
    int sendObliviousTupleTable(MachineID dst_machineid, unsigned char *table, size_vdb size);
    void registerMachine(MachineID machineID, std::string hostNamePort);
    void serverProvideService(MachineID srcMachineID, uint8_t type, unsigned char * buffer, unsigned int size);
#ifdef HONEST_BROKER_INSTANCE
    int sendQuery(MachineID dst_machineid, unsigned char *query, size_vdb size, TransmissionType queryType);
    void handleNewQuery(unsigned char *inputBuffer, size_vdb size, int isFinalQuery);
    void handleWholeObliviousTupleTable(MachineID src_machine_id, unsigned char * inputBuffer, size_vdb size);
    void handlePostgresIntoEnclaveQuery(unsigned char *inputBuffer, size_vdb size, int isFinalQuery);
    void handleResultObliviousTupleTable(MachineID src_machine_id, unsigned char * inputBuffer, size_vdb size);
    void handleCompletionAck(MachineID src_machine_id, TableID tableID);
protected:
    NetworkTransmissionHandler(OutsideController * localConstroller, MachineID localID, std::string port);
#else
protected:
#endif
    void stopServer();
    int startClientTransmission(TransmissionType type, std::string hostNamePort,
                                    unsigned char *data, size_vdb size);
    void acknowledgeHonestBrokerOfRecentFinish(TableID opID);
private:

    OutsideController * _localConstroller;
    std::mutex _registryLock;
    std::map<MachineID, std::string> _registry;
    MachineID _localMachineID;
    MachineID _honestBrokerID;

    std::unique_ptr<std::thread> _runningServerProcess;
};


#endif //VAULTDB_NETWORKTRANSMISSIONHANDLER_H
