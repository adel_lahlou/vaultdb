#ifndef VAULTDB_PROJ_TUPLEPRINTER_H
#define VAULTDB_PROJ_TUPLEPRINTER_H


#include <cstring>
#include <string>
#include <shared/type/RecordSchema.h>
#include <in_sgx/obdata/ObliviousTuple.h>

class TuplePrinter {
public:
    static std::string convert(const type::RecordSchema * schema, db::obdata::ObliviousTuple & tuple);
};


#endif //VAULTDB_PROJ_TUPLEPRINTER_H
