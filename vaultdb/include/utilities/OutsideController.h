#ifndef VAULTDB_OUTSIDECONTROLLER_H
#define VAULTDB_OUTSIDECONTROLLER_H

#include <in_sgx/obdata/ObliviousTupleTable.h>
#include <map>
#include <mutex>
#include <in_sgx/plan/oboperators/ObliviousOperator.h>
#include <in_sgx/plan/kaoperators/KAnonymousOperator.h>
#include <thread>
#include "PostgreSQLHandler.h"

#ifdef LOCAL_DEBUG_MODE
class UtilityTestFixture;
#endif
class NetworkTransmissionHandler;

void pass_on_oblivious_table(unsigned int dst_machine_id, unsigned int opT_id, unsigned char * table, unsigned int size, int is_final_query);
void deposit_table_to_outside_storage(int machine_id, int op_id, int partition_id, unsigned char * data, int len);
void retrieve_oblivious_table(int * ret, int machine_id, int op_id, int partition_id, unsigned char * data, int len);

class OutsideController {
#ifdef LOCAL_DEBUG_MODE
    friend class UtilityTestFixture;
#endif
    friend class NetworkTransmissionHandler;
public:
    static void StartServer(MachineID localID, std::string hostNameAndPort);
    static void StopServer();
    static void RegisterMachine(MachineID machineID, std::string hostNamePort);
    static void RegisterHonestBroker(MachineID machineID, std::string hostNamePort);
    static void SendObliviousTupleTable(MachineID dst_machineid, TableID outputTableID, unsigned char *table,
                                        size_vdb size, int isFinalQuery);
    static void DepositTable(MachineID machineID, TableID tableID, pos_vdb partitionID, unsigned char *table, size_vdb size);
    static void RetrieveTableIntoEnclave(int *ret, int machine_id, int op_id, int partition_id, unsigned char *data,
                                             int len);
#ifdef HONEST_BROKER_INSTANCE
    static GenericReturnStatus SendQuery(MachineID dst_machineid, TableID tableID, unsigned char *query, size_vdb size,
                                         int isFinalQuery);
    static GenericReturnStatus SendPostgreSQLQuery(MachineID dst_machine_id, TableID queryOutputTableID,
                                                   type::RecordSchema &psqlSchema, const char *sqlQuery,
                                                   int isFinalQuery);
    static GenericReturnStatus HBDataEntry(time_t queryTime, int query, size_vdb k, std::string tag, double value_w_double_type,
                                               std::string comment);
    static db::obdata::ObliviousTupleTable & GetResultObliviousTupleTable();
    static bool IsDone();
#endif
protected:
    void sendObliviousTupleTable(MachineID dst_machineid, TableID outputTableID, unsigned char * table, size_vdb size, int isFinalQuery);
    void depositTable(MachineID machineID, TableID tableID, pos_vdb partitionID, unsigned char *table, size_vdb size);
    void retrieveTableIntoEnclave(int *ret, int machine_id, int op_id, int partition_id, unsigned char *data, int len);

    OutsideController(MachineID localID, std::string addressAndPort);
    ~OutsideController();
    void launchEnclaves();
    void destroyEnclaves();
    void registerMachine(MachineID machineID, std::string hostNamePort);
    void registerHonestBroker(MachineID machineID, std::string hostNamePort);
    void queryLocalPostgreAndSendIntoEnclave(type::RecordSchema & schema, const char * query, int isFinalQuery);
    int receiveObliviousTupleTable(MachineID src_machineid, unsigned char * inputBuffer, size_vdb size);
    int receiveSecureNewQuery(unsigned char *inputBuffer, size_vdb size, int isFinalQuery);
#ifdef HONEST_BROKER_INSTANCE
    int receiveOutputResult(MachineID src_machineid, db::obdata::ObliviousTupleTable table);
    void receiveCompletionAck(MachineID src_machineid, TableID tableID);
    GenericReturnStatus honestBrokerDataEntry(time_t queryTime, int queryNumber, size_vdb k, std::string tag, double value_w_double_type,
                                                  std::string comment);
    db::obdata::ObliviousTupleTable _tupleTableResult;
    std::map<TableID, size_vdb> _waitForCount;
    bool _isTupleTableInitialized;
    std::mutex _mergeLock;
#endif
private:
    std::mutex _registryLock;
    std::set<MachineID> _machineRegistry;
    std::vector<uint64_t> _enclaveRegistry;
    std::map<MachineID, std::map<TableID, std::map<pos_vdb, DataSizePair>>> _tableRepository;
    std::map<MachineID, std::map<TableID, std::map<pos_vdb, size_vdb>>> _tableRequestRegistry;
    MachineID _localMachineID;
    MachineID _honestBrokerID;

    utilities::PostgreSQLHandler _postgres;
    NetworkTransmissionHandler * _network;

    static OutsideController * _instance;
};


#endif //VAULTDB_OUTSIDECONTROLLER_H
