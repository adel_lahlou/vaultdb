#ifndef POSTGRESQLHANDLER_H
#define POSTGRESQLHANDLER_H

#include <libpq-fe.h>
#include "in_sgx/obdata/ObliviousTupleTable.h"

namespace utilities {
	class PostgreSQLHandler {
	public:
		PostgreSQLHandler(const char *dbuser, const char *dbpass, const char *dbname);
		~PostgreSQLHandler();
        int singleQueryWithObliviousTupleTable(db::obdata::ObliviousTupleTable & out, type::RecordSchema outputSchema, const char * query);
		int singleQueryWithNoResult(const char * query);
    protected:
        int connect();
        void disconnect();
        int query(const char *command);
        int startTransaction();
        void endTransaction();
	private:
		const char * _user;
		const char * _pass;
        const char * _db;
		PGconn * _conn;
		PGresult * _res;
		bool _inTransaction;
		bool _isCleanupIssued;
		std::mutex _queryMutex;

		void clearResults();
		void cleanup();

	};
}
#endif
