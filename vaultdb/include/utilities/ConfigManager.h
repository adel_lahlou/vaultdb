#ifndef VAULTDB_PROJ_CONFIGURATIONMANAGER_H
#define VAULTDB_PROJ_CONFIGURATIONMANAGER_H

#include <memory>
#include "INIReader.h"

class ConfigManager {
public:
    static ConfigManager * instance();
    std::shared_ptr<char> getCipherList();
    std::shared_ptr<char> getCertFilePath();
    std::shared_ptr<char> getKeyFilePath();
    std::shared_ptr<char> getCAFilePath();
    std::shared_ptr<char> getKeyPW();
    std::shared_ptr<char> getPgUser();
    std::shared_ptr<char> getPgUserPass();
    std::shared_ptr<char> getPgDB();
private:
    ConfigManager();
    void addEntry(int key, string value);
    static std::shared_ptr<ConfigManager> _instance;
    static bool _isInitialized;

    INIReader _reader;
    std::map<int, std::shared_ptr<char>> _data;
};


#endif //VAULTDB_PROJ_CONFIGURATIONMANAGER_H
