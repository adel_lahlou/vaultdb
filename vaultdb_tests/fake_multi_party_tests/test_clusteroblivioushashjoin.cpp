#include <gtest/gtest.h>
#include <gmock/gmock.h>

#ifndef PRINT_CLUSTER_OPERATOR_OUTPUT
//#define PRINT_CLUSTER_OPERATOR_OUTPUT
#endif

#include "fakeMultiPartyTestFixture.h"
#include "in_sgx/plan/oboperators/ObliviousSeqScan.h"
#include <in_sgx/plan/oboperators/BitonicSort.h>
#include "in_sgx/plan/obexpressions/obexpressions.h"
#include "in_sgx/plan/oboperators/ClusterObliviousHashJoin.h"
#include <thread>
#include <debug/DebugInSGXController.h>
#include "debug/DebugFakeLocalDataMover.h"

using namespace plan::oboperators;
using namespace plan::obexpressions;


TEST_F(FakeMultiPartyTestFixture, ThreadedTwoPartyJoin)
{
    ObliviousTupleList tuplesOfMachine0 {
//            2,'bigger','otter',2,5,'otter'
//            2,'swift','execution',2,5,'otter'
//            4,'foo','bar',4,2,'bar'
//            4,'foo','bar',4,2,'execution1'
//            4,'foo','bar',4,2,'execution2'
//            4,'foo','bar',4,5,'bar'
//            4,'secure','execution',4,2,'bar'
//            4,'secure','execution',4,2,'execution1'
//            4,'secure','execution',4,2,'execution2'
//            4,'secure','execution',4,5,'bar'
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("swift", vc_size),
                    ObliviousField::makeObliviousFixcharField("execution", vc_size),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousFixcharField("otter", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("bigger", vc_size),
                    ObliviousField::makeObliviousFixcharField("otter", vc_size),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousFixcharField("otter", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousFixcharField("foo", vc_size),
                    ObliviousField::makeObliviousFixcharField("bar", vc_size),
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("bar", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousFixcharField("foo", vc_size),
                    ObliviousField::makeObliviousFixcharField("bar", vc_size),
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("execution1", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousFixcharField("foo", vc_size),
                    ObliviousField::makeObliviousFixcharField("bar", vc_size),
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("execution2", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousFixcharField("foo", vc_size),
                    ObliviousField::makeObliviousFixcharField("bar", vc_size),
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousFixcharField("bar", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousFixcharField("secure", vc_size),
                    ObliviousField::makeObliviousFixcharField("execution", vc_size),
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("bar", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousFixcharField("secure", vc_size),
                    ObliviousField::makeObliviousFixcharField("execution", vc_size),
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("execution1", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousFixcharField("secure", vc_size),
                    ObliviousField::makeObliviousFixcharField("execution", vc_size),
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("execution2", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousFixcharField("secure", vc_size),
                    ObliviousField::makeObliviousFixcharField("execution", vc_size),
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousFixcharField("bar", vc_size),
            },
    };

    ObliviousTupleList tuplesOfMachine1 {
//            1,'bigger','better',1,2,'bar'
//            1,'bigger','better',1,2,'better2'
//            1,'bigger','better',1,2,'world1'
//            1,'hard','bar',1,2,'bar'
//            1,'hard','bar',1,2,'better2'
//            1,'hard','bar',1,2,'world1'
//            1,'hello','world',1,2,'bar'
//            1,'hello','world',1,2,'better2'
//            1,'hello','world',1,2,'world1'
//            3,'secure','world',3,2,'world'
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("hard", vc_size),
                    ObliviousField::makeObliviousFixcharField("bar", vc_size),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("bar", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("hard", vc_size),
                    ObliviousField::makeObliviousFixcharField("bar", vc_size),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("world1", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("hard", vc_size),
                    ObliviousField::makeObliviousFixcharField("bar", vc_size),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("better2", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("hello", vc_size),
                    ObliviousField::makeObliviousFixcharField("world", vc_size),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("bar", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("hello", vc_size),
                    ObliviousField::makeObliviousFixcharField("world", vc_size),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("world1", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("hello", vc_size),
                    ObliviousField::makeObliviousFixcharField("world", vc_size),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("better2", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("bigger", vc_size),
                    ObliviousField::makeObliviousFixcharField("better", vc_size),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("bar", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("bigger", vc_size),
                    ObliviousField::makeObliviousFixcharField("better", vc_size),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("world1", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("bigger", vc_size),
                    ObliviousField::makeObliviousFixcharField("better", vc_size),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("better2", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousFixcharField("secure", vc_size),
                    ObliviousField::makeObliviousFixcharField("world", vc_size),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("world", vc_size),
            },
    };

    utilities::DebugInSGXController m1(machineOneID, 1);
    utilities::DebugInSGXController m2(machineTwoID, 1);

    std::vector<pos_vdb> orderBys;
    std::vector<SortOrder> sortOrder;
    for (pos_vdb i = 0; i < 6; i++) {
        orderBys.push_back(i);
        sortOrder.push_back(SortOrder::ASCEND);
    }

    TableID joinID = 3;
    TransmitterID tid1 = 14;
    TransmitterID tid2 = 15;
    ObliviousSeqScan * scan01 = new ObliviousSeqScan(0, & oinput01);
    ObliviousSeqScan * scan02 = new ObliviousSeqScan(0, & oinput02);
    ClusterObliviousHashJoin * join1 = new ClusterObliviousHashJoin(machineOneID, joinID, tid1, tid2, scan01, scan02, std::vector<pos_vdb>{0}, std::vector<pos_vdb>{0}, &m1);
    BitonicSort * sort1 = new BitonicSort(4, join1, orderBys, sortOrder);

    ObliviousSeqScan * scan11 = new ObliviousSeqScan(0, & oinput11);
    ObliviousSeqScan * scan12 = new ObliviousSeqScan(0, & oinput12);
    ClusterObliviousHashJoin * join2 = new ClusterObliviousHashJoin(machineTwoID, joinID, tid1, tid2, scan11, scan12, std::vector<pos_vdb>{0}, std::vector<pos_vdb>{0}, &m2);
    BitonicSort * sort2 = new BitonicSort(4, join2, orderBys, sortOrder);

    std::mutex print_lock;

    std::thread first(
            FakeMultiPartyTestFixture::callObliviousOperatorAndVerifyOutput,
            &print_lock,
            sort1,
            machineOneID,
            &tuplesOfMachine0
    );
    std::thread second(
            FakeMultiPartyTestFixture::callObliviousOperatorAndVerifyOutput,
            &print_lock,
            sort2,
            machineTwoID,
            &tuplesOfMachine1
    );

    first.join();
    second.join();
    delete sort1;
    delete sort2;
    utilities::DebugFakeLocalDataMover::instance()->clear();
}

TEST_F(FakeMultiPartyTestFixture, ThreadedThreePartyJoin) {
    ObliviousTupleList tuplesOfMachine0 {
//            3	"secure"	"world"	3	2	"world"
            ObliviousTuple {
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousFixcharField("secure", vc_size),
                    ObliviousField::makeObliviousFixcharField("world", vc_size),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("world", vc_size),
            },
    };

    ObliviousTupleList tuplesOfMachine1 {
//            1	"bigger"	"better"	1	2	"bar"
//            1	"bigger"	"better"	1	2	"better2"
//            1	"bigger"	"better"	1	2	"world1"
//            1	"hard"	"bar"	1	2	"bar"
//            1	"hard"	"bar"	1	2	"better2"
//            1	"hard"	"bar"	1	2	"world1"
//            1	"hello"	"world"	1	2	"bar"
//            1	"hello"	"world"	1	2	"better2"
//            1	"hello"	"world"	1	2	"world1"
//            4	"foo"	"bar"	4	2	"bar"
//            4	"foo"	"bar"	4	2	"execution1"
//            4	"foo"	"bar"	4	2	"execution2"
//            4	"foo"	"bar"	4	5	"bar"
//            4	"secure"	"execution"	4	2	"bar"
//            4	"secure"	"execution"	4	2	"execution1"
//            4	"secure"	"execution"	4	2	"execution2"
//            4	"secure"	"execution"	4	5	"bar"
            ObliviousTuple {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("hard", vc_size),
                    ObliviousField::makeObliviousFixcharField("bar", vc_size),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("bar", vc_size),
            },
            ObliviousTuple {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("hard", vc_size),
                    ObliviousField::makeObliviousFixcharField("bar", vc_size),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("world1", vc_size),
            },
            ObliviousTuple {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("hard", vc_size),
                    ObliviousField::makeObliviousFixcharField("bar", vc_size),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("better2", vc_size),
            },
            ObliviousTuple {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("hello", vc_size),
                    ObliviousField::makeObliviousFixcharField("world", vc_size),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("bar", vc_size),
            },
            ObliviousTuple {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("hello", vc_size),
                    ObliviousField::makeObliviousFixcharField("world", vc_size),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("world1", vc_size),
            },
            ObliviousTuple {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("hello", vc_size),
                    ObliviousField::makeObliviousFixcharField("world", vc_size),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("better2", vc_size),
            },
            ObliviousTuple {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("bigger", vc_size),
                    ObliviousField::makeObliviousFixcharField("better", vc_size),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("bar", vc_size),
            },
            ObliviousTuple {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("bigger", vc_size),
                    ObliviousField::makeObliviousFixcharField("better", vc_size),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("world1", vc_size),
            },
            ObliviousTuple {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("bigger", vc_size),
                    ObliviousField::makeObliviousFixcharField("better", vc_size),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("better2", vc_size),
            },
            ObliviousTuple {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousFixcharField("foo", vc_size),
                    ObliviousField::makeObliviousFixcharField("bar", vc_size),
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("bar", vc_size),
            },
            ObliviousTuple {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousFixcharField("foo", vc_size),
                    ObliviousField::makeObliviousFixcharField("bar", vc_size),
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("execution1", vc_size),
            },
            ObliviousTuple {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousFixcharField("foo", vc_size),
                    ObliviousField::makeObliviousFixcharField("bar", vc_size),
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("execution2", vc_size),
            },
            ObliviousTuple {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousFixcharField("foo", vc_size),
                    ObliviousField::makeObliviousFixcharField("bar", vc_size),
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousFixcharField("bar", vc_size),
            },
            ObliviousTuple {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousFixcharField("secure", vc_size),
                    ObliviousField::makeObliviousFixcharField("execution", vc_size),
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("bar", vc_size),
            },
            ObliviousTuple {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousFixcharField("secure", vc_size),
                    ObliviousField::makeObliviousFixcharField("execution", vc_size),
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("execution1", vc_size),
            },
            ObliviousTuple {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousFixcharField("secure", vc_size),
                    ObliviousField::makeObliviousFixcharField("execution", vc_size),
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("execution2", vc_size),
            },
            ObliviousTuple {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousFixcharField("secure", vc_size),
                    ObliviousField::makeObliviousFixcharField("execution", vc_size),
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousFixcharField("bar", vc_size),
            },
    };

    ObliviousTupleList tuplesOfMachine2 {
//            2	"bigger"	"otter"	2	2	"mushroom"
//            2	"bigger"	"otter"	2	2	"mushroom"
//            2	"bigger"	"otter"	2	2	"tower"
//            2	"bigger"	"otter"	2	2	"tower"
//            2	"bigger"	"otter"	2	5	"otter"
//            2	"bigger"	"otter"	2	5	"otter"
//            2	"swift"	"execution"	2	2	"mushroom"
//            2	"swift"	"execution"	2	2	"tower"
//            2	"swift"	"execution"	2	5	"otter"
//            5	"foo"	"fighter"	5	2	"fighter"
//            5	"foo"	"fighter"	5	5	"fighter"
//            5	"hello"	"it is me"	5	2	"fighter"
//            5	"hello"	"it is me"	5	5	"fighter"
            ObliviousTuple {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("swift", vc_size),
                    ObliviousField::makeObliviousFixcharField("execution", vc_size),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("tower", vc_size),
            },
            ObliviousTuple {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("swift", vc_size),
                    ObliviousField::makeObliviousFixcharField("execution", vc_size),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("mushroom", vc_size),
            },
            ObliviousTuple {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("swift", vc_size),
                    ObliviousField::makeObliviousFixcharField("execution", vc_size),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousFixcharField("otter", vc_size),
            },
            ObliviousTuple {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("bigger", vc_size),
                    ObliviousField::makeObliviousFixcharField("otter", vc_size),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("tower", vc_size),
            },
            ObliviousTuple {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("bigger", vc_size),
                    ObliviousField::makeObliviousFixcharField("otter", vc_size),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("tower", vc_size),
            },
            ObliviousTuple {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("bigger", vc_size),
                    ObliviousField::makeObliviousFixcharField("otter", vc_size),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("mushroom", vc_size),
            },
            ObliviousTuple {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("bigger", vc_size),
                    ObliviousField::makeObliviousFixcharField("otter", vc_size),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("mushroom", vc_size),
            },
            ObliviousTuple {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("bigger", vc_size),
                    ObliviousField::makeObliviousFixcharField("otter", vc_size),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousFixcharField("otter", vc_size),
            },
            ObliviousTuple {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("bigger", vc_size),
                    ObliviousField::makeObliviousFixcharField("otter", vc_size),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousFixcharField("otter", vc_size),
            },
            ObliviousTuple {
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousFixcharField("foo", vc_size),
                    ObliviousField::makeObliviousFixcharField("fighter", vc_size),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("fighter", vc_size),
            },
            ObliviousTuple {
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousFixcharField("foo", vc_size),
                    ObliviousField::makeObliviousFixcharField("fighter", vc_size),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousFixcharField("fighter", vc_size),
            },
            ObliviousTuple {
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousFixcharField("hello", vc_size),
                    ObliviousField::makeObliviousFixcharField("it is me", vc_size),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("fighter", vc_size),
            },
            ObliviousTuple {
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousFixcharField("hello", vc_size),
                    ObliviousField::makeObliviousFixcharField("it is me", vc_size),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousFixcharField("fighter", vc_size),
            },
    };

    utilities::DebugInSGXController m1(machineOneID, 1);
    utilities::DebugInSGXController m2(machineTwoID, 1);
    utilities::DebugInSGXController m3(machineThreeID, 1);

    std::vector<pos_vdb> orderBys;
    std::vector<SortOrder> sortOrder;
    for (pos_vdb i = 0; i < 6; i++) {
        orderBys.push_back(i);
        sortOrder.push_back(SortOrder::ASCEND);
    }

    TableID joinID = 3;
    TransmitterID tid1 = 14;
    TransmitterID tid2 = 15;
    ObliviousSeqScan * scan01 = new ObliviousSeqScan(0, & oinput01);
    ObliviousSeqScan * scan02 = new ObliviousSeqScan(0, & oinput02);
    ClusterObliviousHashJoin * join1 = new ClusterObliviousHashJoin(machineOneID, joinID, tid1, tid2, scan01, scan02, std::vector<pos_vdb>{0}, std::vector<pos_vdb>{0}, &m1);
    BitonicSort * sort1 = new BitonicSort(4, join1, orderBys, sortOrder);

    ObliviousSeqScan * scan11 = new ObliviousSeqScan(0, & oinput11);
    ObliviousSeqScan * scan12 = new ObliviousSeqScan(0, & oinput12);
    ClusterObliviousHashJoin * join2 = new ClusterObliviousHashJoin(machineTwoID, joinID, tid1, tid2, scan11, scan12, std::vector<pos_vdb>{0}, std::vector<pos_vdb>{0}, &m2);
    BitonicSort * sort2 = new BitonicSort(4, join2, orderBys, sortOrder);

    ObliviousSeqScan * scan21 = new ObliviousSeqScan(0, & oinput21);
    ObliviousSeqScan * scan22 = new ObliviousSeqScan(0, & oinput22);
    ClusterObliviousHashJoin * join3 = new ClusterObliviousHashJoin(machineThreeID, joinID, tid1, tid2, scan21, scan22, std::vector<pos_vdb>{0}, std::vector<pos_vdb>{0}, &m3);
    BitonicSort * sort3 = new BitonicSort(4, join3, orderBys, sortOrder);

    std::mutex print_lock;

    std::thread first(FakeMultiPartyTestFixture::callObliviousOperatorAndVerifyOutput, &print_lock, sort1, machineOneID, &tuplesOfMachine0);
    std::thread second(FakeMultiPartyTestFixture::callObliviousOperatorAndVerifyOutput, &print_lock, sort2, machineTwoID, &tuplesOfMachine1);
    std::thread third(FakeMultiPartyTestFixture::callObliviousOperatorAndVerifyOutput, &print_lock, sort3, machineThreeID, &tuplesOfMachine2);

    first.join();
    second.join();
    third.join();
    delete sort1;
    delete sort2;
    delete sort3;
    utilities::DebugFakeLocalDataMover::instance()->clear();
}




TEST_F(FakeMultiPartyTestFixture, ThreadedTwoPartyOneEmpty) {

    ObliviousTupleList tuplesOfMachine0 {
//            2	"bigger"	"execution" 2	5	"mint"
//            2	"bigger"	"execution" 2	5	"otter"
//            2	"swift"	"giraffe" 2	5	"mint"
//            2	"swift"	"giraffe" 2	5	"otter"
//            2	"swift"	"sloth" 2	5	"mint"
//            2	"swift"	"sloth" 2	5	"otter"
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("swift", vc_size),
                    ObliviousField::makeObliviousFixcharField("sloth", vc_size),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousFixcharField("mint", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("swift", vc_size),
                    ObliviousField::makeObliviousFixcharField("sloth", vc_size),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousFixcharField("otter", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("swift", vc_size),
                    ObliviousField::makeObliviousFixcharField("giraffe", vc_size),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousFixcharField("mint", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("swift", vc_size),
                    ObliviousField::makeObliviousFixcharField("giraffe", vc_size),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousFixcharField("otter", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("bigger", vc_size),
                    ObliviousField::makeObliviousFixcharField("execution", vc_size),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousFixcharField("mint", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("bigger", vc_size),
                    ObliviousField::makeObliviousFixcharField("execution", vc_size),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousFixcharField("otter", vc_size),
            },
    };
    ObliviousTupleList tuplesOfMachine1 {
    };

    utilities::DebugInSGXController m1(machineOneID, 1);
    utilities::DebugInSGXController m2(machineTwoID, 1);

    std::vector<pos_vdb> orderBys;
    std::vector<SortOrder> sortOrder;
    for (pos_vdb i = 0; i < 6; i++) {
        orderBys.push_back(i);
        sortOrder.push_back(SortOrder::ASCEND);
    }

    TableID joinID = 3;
    TransmitterID tid1 = 14;
    TransmitterID tid2 = 15;
    ObliviousSeqScan * scan01 = new ObliviousSeqScan(0, & oinputshort01);
    ObliviousSeqScan * scan02 = new ObliviousSeqScan(0, & oinputshort02);
    ClusterObliviousHashJoin * join1 = new ClusterObliviousHashJoin(machineOneID, joinID, tid1, tid2, scan01, scan02, std::vector<pos_vdb>{0}, std::vector<pos_vdb>{0}, &m1);
    BitonicSort * sort1 = new BitonicSort(4, join1, orderBys, sortOrder);

    ObliviousSeqScan * scan11 = new ObliviousSeqScan(0, & oinputshort11);
    ObliviousSeqScan * scan12 = new ObliviousSeqScan(0, & oinputshort12);
    ClusterObliviousHashJoin * join2 = new ClusterObliviousHashJoin(machineTwoID, joinID, tid1, tid2, scan11, scan12, std::vector<pos_vdb>{0}, std::vector<pos_vdb>{0}, &m2);
    BitonicSort * sort2 = new BitonicSort(4, join2, orderBys, sortOrder);

    std::mutex print_lock;

    std::thread first(
            FakeMultiPartyTestFixture::callObliviousOperatorAndVerifyOutput,
            &print_lock,
            sort1,
            machineOneID,
            &tuplesOfMachine0
    );
    std::thread second(
            FakeMultiPartyTestFixture::callObliviousOperatorAndVerifyOutput,
            &print_lock,
            sort2,
            machineTwoID,
            &tuplesOfMachine1
    );

    first.join();
    second.join();
    delete sort1;
    delete sort2;
    utilities::DebugFakeLocalDataMover::instance()->clear();
}

TEST_F(FakeMultiPartyTestFixture, ThreadedTwoPartyBothEmpty) {
    ObliviousTupleList tuplesOfMachine0{
    };

    ObliviousTupleList tuplesOfMachine1{
    };

    utilities::DebugInSGXController m1(machineOneID, 1);
    utilities::DebugInSGXController m2(machineTwoID, 1);

    std::vector<pos_vdb> orderBys;
    std::vector<SortOrder> sortOrder;
    for (pos_vdb i = 0; i < 6; i++) {
        orderBys.push_back(i);
        sortOrder.push_back(SortOrder::ASCEND);
    }

    TableID joinID = 3;
    TransmitterID tid1 = 14;
    TransmitterID tid2 = 15;
    ObliviousTupleTable t1(s1, ObliviousTupleList());
    ObliviousTupleTable t2(s2, ObliviousTupleList());
    ObliviousSeqScan * scan01 = new ObliviousSeqScan(0, & t1);
    ObliviousSeqScan * scan02 = new ObliviousSeqScan(0, & t2);
    ClusterObliviousHashJoin * join1 = new ClusterObliviousHashJoin(machineOneID, joinID, tid1, tid2, scan01, scan02, std::vector<pos_vdb>{0}, std::vector<pos_vdb>{0}, &m1);
    BitonicSort * sort1 = new BitonicSort(4, join1, orderBys, sortOrder);

    ObliviousSeqScan * scan11 = new ObliviousSeqScan(0, & t1);
    ObliviousSeqScan * scan12 = new ObliviousSeqScan(0, & t2);
    ClusterObliviousHashJoin * join2 = new ClusterObliviousHashJoin(machineTwoID, joinID, tid1, tid2, scan11, scan12, std::vector<pos_vdb>{0}, std::vector<pos_vdb>{0}, &m2);
    BitonicSort * sort2 = new BitonicSort(4, join2, orderBys, sortOrder);


    std::mutex print_lock;

    std::thread first(
            FakeMultiPartyTestFixture::callObliviousOperatorAndVerifyOutput,
            &print_lock,
            sort1,
            machineOneID,
            &tuplesOfMachine0
    );
    std::thread second(
            FakeMultiPartyTestFixture::callObliviousOperatorAndVerifyOutput,
            &print_lock,
            sort2,
            machineTwoID,
            &tuplesOfMachine1
    );

    first.join();
    second.join();
    delete sort1;
    delete sort2;
    utilities::DebugFakeLocalDataMover::instance()->clear();
}