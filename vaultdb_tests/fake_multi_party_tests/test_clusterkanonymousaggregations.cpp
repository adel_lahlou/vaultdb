#include <gtest/gtest.h>
#include <gmock/gmock.h>

#ifndef PRINT_CLUSTER_OPERATOR_OUTPUT
//#define PRINT_CLUSTER_OPERATOR_OUTPUT
#endif

#include "fakeMultiPartyTestFixture.h"
#include <in_sgx/plan/kaoperators/ClusterKAnonymousBitonicMergeSort.h>
#include "in_sgx/plan/kaoperators/KAnonymousSeqScan.h"
#include "in_sgx/plan/obexpressions/obexpressions.h"
#include "debug/DebugFakeLocalDataMover.h"
#include <thread>
#include <in_sgx/plan/kaoperators/ClusterKAnonymousSortedAggregation.h>
#include <in_sgx/plan/kaoperators/ClusterKAnonymousBinnedAggregation.h>
#include <in_sgx/plan/kaoperators/KAnonymousBitonicSort.h>
#include <in_sgx/plan/kaoperators/KAnonymousSortedGroup.h>
#include <in_sgx/plan/kaoperators/ClusterKAnonymousLDAggregation.h>
#include <in_sgx/serialization/InSGXQueryDecoder.h>
#include <in_sgx/plan/obexpressions/ObliviousExpressionFactory.h>
#include <debug/DebugInSGXController.h>
#include <in_sgx/plan/kaoperators/ClusterKAnonymousWindow.h>


using namespace plan::kaoperators;
using namespace plan::obexpressions;

TEST_F(FakeMultiPartyTestFixture, ThreadedTwoPartyKAHashSortedAggregation) {

    ObliviousExpressionList exps = {
            ObliviousExpressionFactory::makeColumn(0, type::SchemaColumn{type::FieldDataType::Int, sizeof(int)}),
            ObliviousExpressionFactory::makeSum(1, type::SchemaColumn{type::FieldDataType::Int, sizeof(int)}),
            ObliviousExpressionFactory::makeCount(1),
    };

    size_vdb expected_machine0_dummy = 0;
    ObliviousTupleList tuplesOfMachine0{
//            k = 2
//            Query: col_0, sum(col_1), count(col_1)
//            D - 0,0,0 // each machine receives at least k
//            R - 2,1,1
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(1),
            }),
//            ObliviousTuple(true, {
//                    ObliviousField::makeObliviousIntField(4),
//                    ObliviousField::makeObliviousIntField(1),
//                    ObliviousField::makeObliviousIntField(1),
//            }),
//            ObliviousTuple(true, {
//                    ObliviousField::makeObliviousIntField(4),
//                    ObliviousField::makeObliviousIntField(2),
//                    ObliviousField::makeObliviousIntField(2),
//            }),
    };

    size_vdb expected_machine1_dummy = 5;
    ObliviousTupleList tuplesOfMachine1{
//            k = 2
//            Query: col_0, sum(col_1), count(col_1)
//            D - 1,1,1
//            D - 1,3,2
//             X - 1,5,3
//             X - 1,8,4
//             X - 1,11,5
//            R - 1,14,6
//            D - 3,1,1
//            R - 3,3,2
//            D - 4,1,1
//            D - 4,2,2
//            R - 4,3,3
//            ObliviousTuple(true, {
//                    ObliviousField::makeObliviousIntField(1),
//                    ObliviousField::makeObliviousIntField(1),
//                    ObliviousField::makeObliviousIntField(1),
//            }),
//            ObliviousTuple(true, {
//                    ObliviousField::makeObliviousIntField(1),
//                    ObliviousField::makeObliviousIntField(3),
//                    ObliviousField::makeObliviousIntField(2),
//            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(14),
                    ObliviousField::makeObliviousIntField(6),
            }),
//            ObliviousTuple(true, {
//                    ObliviousField::makeObliviousIntField(3),
//                    ObliviousField::makeObliviousIntField(1),
//                    ObliviousField::makeObliviousIntField(1),
//            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousIntField(2),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousIntField(3),
            }),
    };

    utilities::DebugInSGXController m1(machineOneID, 1);
    utilities::DebugInSGXController m2(machineTwoID, 1);

    std::vector<pos_vdb> groupBys{0};
    std::vector<ObliviousExpression> expressions1{
            ObliviousExpressionFactory::makeColumn(0, type::SchemaColumn{type::FieldDataType::Int, sizeof(int)}),
            ObliviousExpressionFactory::makeSum(1, type::SchemaColumn{type::FieldDataType::Int, sizeof(int)}),
            ObliviousExpressionFactory::makeCount(1),
    };

    std::vector<ObliviousExpression> expressions2{
            ObliviousExpressionFactory::makeColumn(0, type::SchemaColumn{type::FieldDataType::Int, sizeof(int)}),
            ObliviousExpressionFactory::makeSum(1, type::SchemaColumn{type::FieldDataType::Int, sizeof(int)}),
            ObliviousExpressionFactory::makeCount(1),
    };

    TableID aggID = 19;
    TransmitterID tid = 14;

    KAnonymousSeqScan *scan02 = new KAnonymousSeqScan(0, k, & oinputka02);
    ClusterKAnonymousSortedAggregation *agg1 = new ClusterKAnonymousSortedAggregation(machineOneID, aggID, tid,
                                                                                              k, scan02, expressions1,
                                                                                              groupBys, false,
                                                                                              &m1);
    KAnonymousBitonicSort * sort1 = new KAnonymousBitonicSort(4, k, agg1, groupBys,
                                                              std::vector<SortOrder> {SortOrder::ASCEND}, &m1);

    KAnonymousSeqScan *scan12 = new KAnonymousSeqScan(0, k, & oinputka12);
    ClusterKAnonymousSortedAggregation *agg2 = new ClusterKAnonymousSortedAggregation(machineTwoID, aggID, tid,
                                                                                              k, scan12, expressions2,
                                                                                              groupBys, false,
                                                                                              &m2);
    KAnonymousBitonicSort * sort2 = new KAnonymousBitonicSort(4, k, agg2, groupBys,
                                                              std::vector<SortOrder> {SortOrder::ASCEND}, &m2);


    std::mutex print_lock;

    std::thread first(
            FakeMultiPartyTestFixture::callKAnonymousOperatorAndVerifyOutput,
            &print_lock,
            sort1,
            machineOneID,
            &tuplesOfMachine0,
            expected_machine0_dummy
    );
    std::thread second(
            FakeMultiPartyTestFixture::callKAnonymousOperatorAndVerifyOutput,
            &print_lock,
            sort2,
            machineTwoID
            ,
            &tuplesOfMachine1,
            expected_machine1_dummy
    );

    first.join();
    second.join();
    delete sort1;
    delete sort2;
    utilities::DebugFakeLocalDataMover::instance()->clear();
}


TEST_F(FakeMultiPartyTestFixture, ThreadedTwoPartyKALocalDistroAggregation) {

    size_vdb expected_machine1_dummy = 0;
    ObliviousTupleList tuplesOfMachine0{
//            k = 2
//            Query: col_0, sum(col_1), count(col_1), count(distinct col_0)
//            D - 0,0,0 // each machine receives at least k
//            R - 2,1,1
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(1),
            }),
    };

    size_vdb expected_machine2_dummy = 5;
    ObliviousTupleList tuplesOfMachine1{
//            k = 2
//            Query: col_0, sum(col_1), count(col_1)
//            D - 1,1,1
//            D - 1,3,2
//             X - 1,5,3
//             X - 1,8,4
//             X - 1,11,5
//            R - 1,14,6
//            D - 3,1,1
//            R - 3,3,2

//            D - 4,1,1
//            D - 4,2,2
//            R - 4,3,3
//            ObliviousTuple(true, {
//                    ObliviousField::makeObliviousIntField(1),
//                    ObliviousField::makeObliviousIntField(1),
//                    ObliviousField::makeObliviousIntField(1),
//            }),
//            ObliviousTuple(true, {
//                    ObliviousField::makeObliviousIntField(1),
//                    ObliviousField::makeObliviousIntField(3),
//                    ObliviousField::makeObliviousIntField(2),
//            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(14),
                    ObliviousField::makeObliviousIntField(6),
                    ObliviousField::makeObliviousIntField(3),
            }),
//            ObliviousTuple(true, {
//                    ObliviousField::makeObliviousIntField(3),
//                    ObliviousField::makeObliviousIntField(1),
//                    ObliviousField::makeObliviousIntField(1),
//            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(2),
            }),
//            ObliviousTuple(true, {
//                    ObliviousField::makeObliviousIntField(4),
//                    ObliviousField::makeObliviousIntField(1),
//                    ObliviousField::makeObliviousIntField(1),
//            }),
//            ObliviousTuple(true, {
//                    ObliviousField::makeObliviousIntField(4),
//                    ObliviousField::makeObliviousIntField(2),
//                    ObliviousField::makeObliviousIntField(2),
//            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousIntField(1),
            }),
    };

    utilities::DebugInSGXController m1(machineOneID, 1);
    utilities::DebugInSGXController m2(machineTwoID, 1);

    std::vector<pos_vdb> groupBys{0};
    std::vector<ObliviousExpression> expressions1{
            ObliviousExpressionFactory::makeColumn(0, type::SchemaColumn{type::FieldDataType::Int, sizeof(int)}),
            ObliviousExpressionFactory::makeSum(1, type::SchemaColumn{type::FieldDataType::Int, sizeof(int)}),
            ObliviousExpressionFactory::makeCount(1),
            ObliviousExpressionFactory::makeCountDistinct(1, type::SchemaColumn{type::FieldDataType::Int, sizeof(int)}),
    };

    std::vector<ObliviousExpression> expressions2{
            ObliviousExpressionFactory::makeColumn(0, type::SchemaColumn{type::FieldDataType::Int, sizeof(int)}),
            ObliviousExpressionFactory::makeSum(1, type::SchemaColumn{type::FieldDataType::Int, sizeof(int)}),
            ObliviousExpressionFactory::makeCount(1),
            ObliviousExpressionFactory::makeCountDistinct(1, type::SchemaColumn{type::FieldDataType::Int, sizeof(int)}),
    };

    TableID aggID = 19;
    TransmitterID tid = 14;

    KAnonymousSeqScan *scan02 = new KAnonymousSeqScan(0, k, & oinputka02);
    ClusterKAnonymousLDAggregation *agg1 = new ClusterKAnonymousLDAggregation(machineOneID, aggID, tid, k, scan02,
                                                                              expressions1, groupBys, &m1);
    KAnonymousBitonicSort * sort1 = new KAnonymousBitonicSort(4, k, agg1, groupBys,
                                                              std::vector<SortOrder> {SortOrder::ASCEND}, &m1);

    KAnonymousSeqScan *scan12 = new KAnonymousSeqScan(0, k, & oinputka12);
    ClusterKAnonymousLDAggregation *agg2 = new ClusterKAnonymousLDAggregation(machineTwoID, aggID, tid, k, scan12,
                                                                              expressions2, groupBys, &m2);
    KAnonymousBitonicSort * sort2 = new KAnonymousBitonicSort(4, k, agg2, groupBys,
                                                              std::vector<SortOrder> {SortOrder::ASCEND}, &m2);


    std::mutex print_lock;

    std::thread first(
            FakeMultiPartyTestFixture::callKAnonymousOperatorAndVerifyOutput,
            &print_lock,
            sort1,
            machineOneID,
            &tuplesOfMachine0,
            expected_machine1_dummy
    );
    std::thread second(
            FakeMultiPartyTestFixture::callKAnonymousOperatorAndVerifyOutput,
            &print_lock,
            sort2,
            machineTwoID
            ,
            &tuplesOfMachine1,
            expected_machine2_dummy
    );

    first.join();
    second.join();
    delete sort1;
    delete sort2;
    utilities::DebugFakeLocalDataMover::instance()->clear();
}


TEST_F(FakeMultiPartyTestFixture, ThreadedTwoPartyKABinnedAggregation) {

    ObliviousExpressionList exps = {
            ObliviousExpressionFactory::makeColumn(0, type::SchemaColumn{type::FieldDataType::Int, sizeof(int)}),
            ObliviousExpressionFactory::makeSum(1, type::SchemaColumn{type::FieldDataType::Int, sizeof(int)}),
            ObliviousExpressionFactory::makeCount(1),
    };

    size_vdb expected_machine1_dummy = 0;
    ObliviousTupleList tuplesOfMachine1{
//            k = 2
//            Query: col_0, sum(col_1), count(col_1)
//            D - 0,0,0 // each machine receives at least k
//            R - 2,1,1
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(1),
            }),
//            ObliviousTuple(true, {
//                    ObliviousField::makeObliviousIntField(4),
//                    ObliviousField::makeObliviousIntField(1),
//                    ObliviousField::makeObliviousIntField(1),
//            }),
//            ObliviousTuple(true, {
//                    ObliviousField::makeObliviousIntField(4),
//                    ObliviousField::makeObliviousIntField(2),
//                    ObliviousField::makeObliviousIntField(2),
//            }),
    };

    size_vdb expected_machine2_dummy = 0;
    ObliviousTupleList tuplesOfMachine2{
//            k = 2
//            Query: col_0, sum(col_1), count(col_1)
//            D - 1,1,1
//            D - 1,3,2
//             X - 1,5,3
//             X - 1,8,4
//             X - 1,11,5
//            R - 1,14,6
//            D - 3,1,1
//            R - 3,3,2
//            D - 4,1,1
//            D - 4,2,2
//            R - 4,3,3
//            ObliviousTuple(true, {
//                    ObliviousField::makeObliviousIntField(1),
//                    ObliviousField::makeObliviousIntField(1),
//                    ObliviousField::makeObliviousIntField(1),
//            }),
//            ObliviousTuple(true, {
//                    ObliviousField::makeObliviousIntField(1),
//                    ObliviousField::makeObliviousIntField(3),
//                    ObliviousField::makeObliviousIntField(2),
//            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(14),
                    ObliviousField::makeObliviousIntField(6),
            }),
//            ObliviousTuple(true, {
//                    ObliviousField::makeObliviousIntField(3),
//                    ObliviousField::makeObliviousIntField(1),
//                    ObliviousField::makeObliviousIntField(1),
//            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousIntField(2),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousIntField(3),
            }),
    };

    utilities::DebugInSGXController m1(machineOneID, 1);
    utilities::DebugInSGXController m2(machineTwoID, 1);

    std::vector<pos_vdb> groupBys{0};
    std::vector<ObliviousExpression> expressions1{
            ObliviousExpressionFactory::makeColumn(0, type::SchemaColumn{type::FieldDataType::Int, sizeof(int)}),
            ObliviousExpressionFactory::makeSum(1, type::SchemaColumn{type::FieldDataType::Int, sizeof(int)}),
            ObliviousExpressionFactory::makeCount(1),
    };

    std::vector<ObliviousExpression> expressions2{
            ObliviousExpressionFactory::makeColumn(0, type::SchemaColumn{type::FieldDataType::Int, sizeof(int)}),
            ObliviousExpressionFactory::makeSum(1, type::SchemaColumn{type::FieldDataType::Int, sizeof(int)}),
            ObliviousExpressionFactory::makeCount(1),
    };

    TableID aggID = 19;
    TransmitterID tid = 14;

    KAnonymousSeqScan *scan02 = new KAnonymousSeqScan(0, k, & oinputka02);
    ClusterKAnonymousBinnedAggregation *agg1 = new ClusterKAnonymousBinnedAggregation(machineOneID, aggID, tid, k,
                                                                                      scan02, expressions1, groupBys,
                                                                                      false, true,
                                                                                      &m1);
    KAnonymousBitonicSort * sort1 = new KAnonymousBitonicSort(4, k, agg1, groupBys,
                                                              std::vector<SortOrder> {SortOrder::ASCEND}, &m1);

    KAnonymousSeqScan *scan12 = new KAnonymousSeqScan(0, k, & oinputka12);
    ClusterKAnonymousBinnedAggregation *agg2 = new ClusterKAnonymousBinnedAggregation(machineTwoID, aggID, tid, k,
                                                                                      scan12, expressions2, groupBys,
                                                                                      false, true,
                                                                                      &m2);
    KAnonymousBitonicSort * sort2 = new KAnonymousBitonicSort(4, k, agg2, groupBys,
                                                              std::vector<SortOrder> {SortOrder::ASCEND}, &m2);


    std::mutex print_lock;

    std::thread first(
            FakeMultiPartyTestFixture::callKAnonymousOperatorAndVerifyOutput,
            &print_lock,
            sort1,
            machineOneID,
            &tuplesOfMachine1,
            expected_machine1_dummy
    );
    std::thread second(
            FakeMultiPartyTestFixture::callKAnonymousOperatorAndVerifyOutput,
            &print_lock,
            sort2,
            machineTwoID
            ,
            &tuplesOfMachine2,
            expected_machine2_dummy
    );

    first.join();
    second.join();
    delete sort1;
    delete sort2;
    utilities::DebugFakeLocalDataMover::instance()->clear();
}

TEST_F(FakeMultiPartyTestFixture, ThreadedTwoPartyKABinnedAggWithFixcharGroup) {

    /**
     * Note:
     *
     * This test specifically created additional test input data because binned aggregation
     * assumes each group to correspond to k inputs.
     */

    size_vdb expected_machine1_dummy = 0;
    ObliviousTupleList tuplesOfMachine1{
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousFixcharField("     giant", vc_size),
                    ObliviousField::makeObliviousIntField(77),
                    ObliviousField::makeObliviousIntField(2),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousFixcharField("   fighter", vc_size),
                    ObliviousField::makeObliviousIntField(14),
                    ObliviousField::makeObliviousIntField(2),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousFixcharField("  mushroom", vc_size),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
            }),
    };

    size_vdb expected_machine2_dummy = 0;
    ObliviousTupleList tuplesOfMachine2{
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousFixcharField("     peace", vc_size),
                    ObliviousField::makeObliviousIntField(6),
                    ObliviousField::makeObliviousIntField(2),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousFixcharField("     smart", vc_size),
                    ObliviousField::makeObliviousIntField(100),
                    ObliviousField::makeObliviousIntField(2),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousFixcharField("   tornado", vc_size),
                    ObliviousField::makeObliviousIntField(11),
                    ObliviousField::makeObliviousIntField(2),
            }),
    };

    utilities::DebugInSGXController m1(machineOneID, 1);
    utilities::DebugInSGXController m2(machineTwoID, 1);

    std::vector<pos_vdb> groupBys{2};
    std::vector<ObliviousExpression> expressions1{
            ObliviousExpressionFactory::makeColumn(2, type::SchemaColumn{type::FieldDataType::Fixchar, vc_size}),
            ObliviousExpressionFactory::makeSum(1, type::SchemaColumn{type::FieldDataType::Int, sizeof(int)}),
            ObliviousExpressionFactory::makeCount(1),
    };

    std::vector<ObliviousExpression> expressions2{
            ObliviousExpressionFactory::makeColumn(2, type::SchemaColumn{type::FieldDataType::Fixchar, vc_size}),
            ObliviousExpressionFactory::makeSum(1, type::SchemaColumn{type::FieldDataType::Int, sizeof(int)}),
            ObliviousExpressionFactory::makeCount(1),
    };

    TableID aggID = 19;
    TransmitterID tid = 14;


    ObliviousTupleTable table1 (s2, {
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("     peace", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("     smart", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(99),
                    ObliviousField::makeObliviousFixcharField("     smart", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("     giant", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousFixcharField("     peace", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousIntField(6),
                    ObliviousField::makeObliviousFixcharField("   tornado", vc_size),
            }),
    });

    ObliviousTupleTable table2 (s2, {
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(76),
                    ObliviousField::makeObliviousFixcharField("     giant", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(0),
                    ObliviousField::makeObliviousFixcharField("  mushroom", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(7),
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousFixcharField("   fighter", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(7),
                    ObliviousField::makeObliviousIntField(10),
                    ObliviousField::makeObliviousFixcharField("   fighter", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousFixcharField("   tornado", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("  mushroom", vc_size),
            }),
    });

    KAnonymousSeqScan *scan02 = new KAnonymousSeqScan(0, k, &table1);
    ClusterKAnonymousBinnedAggregation *agg1 = new ClusterKAnonymousBinnedAggregation(machineOneID, aggID, tid, k,
                                                                                      scan02, expressions1, groupBys,
                                                                                      false, false,
                                                                                      &m1);
    KAnonymousBitonicSort * sort1 = new KAnonymousBitonicSort(4, k, agg1, {0},
                                                              std::vector<SortOrder> {SortOrder::ASCEND}, &m1);

    KAnonymousSeqScan *scan12 = new KAnonymousSeqScan(0, k, & table2);
    ClusterKAnonymousBinnedAggregation *agg2 = new ClusterKAnonymousBinnedAggregation(machineTwoID, aggID, tid, k,
                                                                                      scan12, expressions2, groupBys,
                                                                                      false, false,
                                                                                      &m2);
    KAnonymousBitonicSort * sort2 = new KAnonymousBitonicSort(4, k, agg2, {0},
                                                              std::vector<SortOrder> {SortOrder::ASCEND}, &m2);


    std::mutex print_lock;

    std::thread first(
            FakeMultiPartyTestFixture::callKAnonymousOperatorAndVerifyOutput,
            &print_lock,
            sort1,
            machineOneID,
            &tuplesOfMachine1,
            expected_machine1_dummy
    );
    std::thread second(
            FakeMultiPartyTestFixture::callKAnonymousOperatorAndVerifyOutput,
            &print_lock,
            sort2,
            machineTwoID
            ,
            &tuplesOfMachine2,
            expected_machine2_dummy
    );

    first.join();
    second.join();
    delete sort1;
    delete sort2;
    utilities::DebugFakeLocalDataMover::instance()->clear();
}

TEST_F(FakeMultiPartyTestFixture, ThreadedTwoPartyKABinnedAggWithFixcharParsed) {

    /**
     * Note:
     *
     * This test specifically created additional test input data because binned aggregation
     * assumes each group to correspond to k inputs.
     */

    size_vdb expected_machine1_dummy = 0;
    ObliviousTupleList tuplesOfMachine1{
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousFixcharField("     giant", vc_size),
                    ObliviousField::makeObliviousIntField(77),
                    ObliviousField::makeObliviousIntField(2),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousFixcharField("   fighter", vc_size),
                    ObliviousField::makeObliviousIntField(14),
                    ObliviousField::makeObliviousIntField(2),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousFixcharField("  mushroom", vc_size),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
            }),
    };

    size_vdb expected_machine2_dummy = 0;
    ObliviousTupleList tuplesOfMachine2{
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousFixcharField("     peace", vc_size),
                    ObliviousField::makeObliviousIntField(6),
                    ObliviousField::makeObliviousIntField(2),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousFixcharField("     smart", vc_size),
                    ObliviousField::makeObliviousIntField(100),
                    ObliviousField::makeObliviousIntField(2),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousFixcharField("   tornado", vc_size),
                    ObliviousField::makeObliviousIntField(11),
                    ObliviousField::makeObliviousIntField(2),
            }),
    };

    utilities::DebugInSGXController m1(machineOneID, 1);
    utilities::DebugInSGXController m2(machineTwoID, 1);

    std::vector<pos_vdb> groupBys{2};
    std::vector<ObliviousExpression> expressions1{
            ObliviousExpressionFactory::makeColumn(2, type::SchemaColumn{type::FieldDataType::Fixchar, vc_size}),
            ObliviousExpressionFactory::makeSum(1, type::SchemaColumn{type::FieldDataType::Int, sizeof(int)}),
            ObliviousExpressionFactory::makeCount(1),
    };

    std::vector<ObliviousExpression> expressions2{
            ObliviousExpressionFactory::makeColumn(2, type::SchemaColumn{type::FieldDataType::Fixchar, vc_size}),
            ObliviousExpressionFactory::makeSum(1, type::SchemaColumn{type::FieldDataType::Int, sizeof(int)}),
            ObliviousExpressionFactory::makeCount(1),
    };



    type::RecordSchema schema1(s2);
    schema1.setTableID(100);
    ObliviousTupleTable table1 (schema1, {
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("     peace", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("     smart", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(99),
                    ObliviousField::makeObliviousFixcharField("     smart", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("     giant", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousFixcharField("     peace", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousIntField(6),
                    ObliviousField::makeObliviousFixcharField("   tornado", vc_size),
            }),
    });

    type::RecordSchema schema2(s2);
    schema2.setTableID(120);
    ObliviousTupleTable table2 (schema2, {
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(76),
                    ObliviousField::makeObliviousFixcharField("     giant", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(0),
                    ObliviousField::makeObliviousFixcharField("  mushroom", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(7),
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousFixcharField("   fighter", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(7),
                    ObliviousField::makeObliviousIntField(10),
                    ObliviousField::makeObliviousFixcharField("   fighter", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousFixcharField("   tornado", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("  mushroom", vc_size),
            }),
    });

    TableID aggID = 19;
    TransmitterID tid = 14;

    KAnonymousSeqScan *scan02 = new KAnonymousSeqScan(0, k, &table1);
    ClusterKAnonymousBinnedAggregation *agg1 = new ClusterKAnonymousBinnedAggregation(machineOneID, aggID, tid, k,
                                                                                      scan02, expressions1, groupBys,
                                                                                      false, true,
                                                                                      nullptr);
    KAnonymousBitonicSort * sort1 = new KAnonymousBitonicSort(4, k, agg1, {0},
                                                              std::vector<SortOrder> {SortOrder::ASCEND}, nullptr);

    KAnonymousSeqScan *scan12 = new KAnonymousSeqScan(0, k, & table2);
    ClusterKAnonymousBinnedAggregation *agg2 = new ClusterKAnonymousBinnedAggregation(machineTwoID, aggID, tid, k,
                                                                                      scan12, expressions2, groupBys,
                                                                                      false, true,
                                                                                      nullptr);
    KAnonymousBitonicSort * sort2 = new KAnonymousBitonicSort(4, k, agg2, {0},
                                                              std::vector<SortOrder> {SortOrder::ASCEND}, nullptr);

    m1.collectTablePointer(&table1);
    m2.collectTablePointer(&table2);

    auto parsedSort1 = m1.encodeKAnonymousOperatorAndParse(sort1);
    auto parsedSort2 = m2.encodeKAnonymousOperatorAndParse(sort2);


    std::mutex print_lock;

    std::thread first(
            FakeMultiPartyTestFixture::callKAnonymousOperatorAndVerifyOutput,
            &print_lock,
            parsedSort1,
            machineOneID,
            &tuplesOfMachine1,
            expected_machine1_dummy
    );
    std::thread second(
            FakeMultiPartyTestFixture::callKAnonymousOperatorAndVerifyOutput,
            &print_lock,
            parsedSort2,
            machineTwoID
            ,
            &tuplesOfMachine2,
            expected_machine2_dummy
    );

    first.join();
    second.join();
    delete sort1;
    delete sort2;
    utilities::DebugFakeLocalDataMover::instance()->clear();
}

TEST_F(FakeMultiPartyTestFixture, ThreadedTwoPartyKAWindow) {

    size_vdb expected_dummy_count0 = 0;
    ObliviousTupleList tuplesOfMachine0{
//            2	5	"otter" 1
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousFixcharField("otter", vc_size),
                    ObliviousField::makeObliviousIntField(1),
            }),
    };
    size_vdb expected_dummy_count1 = 0;
    ObliviousTupleList tuplesOfMachine1{
//            1	2	"bar"   1
//            1	2	"world1"    2
//            1	2	"better2"   3
//            3	2	"world" 1
//            4	2	"bar"   1
//            4	2	"execution1"    3
//            4	2	"execution2"    4
//            4	5	"bar"   2
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("bar", vc_size),
                    ObliviousField::makeObliviousIntField(1),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("world1", vc_size),
                    ObliviousField::makeObliviousIntField(2),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("better2", vc_size),
                    ObliviousField::makeObliviousIntField(3),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("world", vc_size),
                    ObliviousField::makeObliviousIntField(1),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousFixcharField("bar", vc_size),
                    ObliviousField::makeObliviousIntField(1),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("bar", vc_size),
                    ObliviousField::makeObliviousIntField(2),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("execution1", vc_size),
                    ObliviousField::makeObliviousIntField(3),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("execution2", vc_size),
                    ObliviousField::makeObliviousIntField(4),
            }),
    };


    utilities::DebugInSGXController m1(machineOneID, 1);
    utilities::DebugInSGXController m2(machineTwoID, 1);

    std::vector<pos_vdb> partitions{0};
    std::vector<pos_vdb> orderBys{2};
    std::vector<SortOrder> sortOrderOrOrderBys{SortOrder ::ASCEND};
    std::vector<ObliviousExpression> expressions1{
            ObliviousExpressionFactory::makeColumn(0, oinput02.getSchema()[0]),
            ObliviousExpressionFactory::makeColumn(1, oinput02.getSchema()[1]),
            ObliviousExpressionFactory::makeColumn(2, oinput02.getSchema()[2]),
            ObliviousExpressionFactory::makeRowNumber(),
    };

    std::vector<ObliviousExpression> expressions2{
            ObliviousExpressionFactory::makeColumn(0, oinput12.getSchema()[0]),
            ObliviousExpressionFactory::makeColumn(1, oinput12.getSchema()[2]),
            ObliviousExpressionFactory::makeColumn(2, oinput12.getSchema()[2]),
            ObliviousExpressionFactory::makeRowNumber(),
    };

    TableID windowID = 3;
    TransmitterID tid = 14;
    KAnonymousSeqScan *scan02 = new KAnonymousSeqScan(0, k, & oinput02);
    ClusterKAnonymousWindow * window1 = new ClusterKAnonymousWindow(machineOneID, windowID, tid, k, scan02,
                                                                    expressions1, partitions, orderBys,
                                                                    sortOrderOrOrderBys, false, &m1);
//    KAnonymousBitonicSort * sort1 = new KAnonymousBitonicSort(4, k, window1, partitions, std::vector<SortOrder> {SortOrder ::ASCEND}, &m1);

    KAnonymousSeqScan *scan12 = new KAnonymousSeqScan(0, k, & oinput12);
    ClusterKAnonymousWindow * window2 = new ClusterKAnonymousWindow(machineTwoID, windowID, tid, k, scan12,
                                                                    expressions2, partitions, orderBys,
                                                                    sortOrderOrOrderBys, false, &m2);
//    KAnonymousBitonicSort * sort2 = new KAnonymousBitonicSort(4, k, window2, partitions, std::vector<SortOrder> {SortOrder ::ASCEND}, &m2);


    std::mutex print_lock;

    std::thread first(
            FakeMultiPartyTestFixture::callKAnonymousOperatorAndVerifyOutput,
            &print_lock,
            window1,
            machineOneID,
            &tuplesOfMachine0,
            expected_dummy_count0
    );
    std::thread second(
            FakeMultiPartyTestFixture::callKAnonymousOperatorAndVerifyOutput,
            &print_lock,
            window2,
            machineTwoID,
            &tuplesOfMachine1,
            expected_dummy_count1
    );

    first.join();
    second.join();
    delete window1;
    delete window2;
    utilities::DebugFakeLocalDataMover::instance()->clear();
}

TEST_F(FakeMultiPartyTestFixture, ThreadedTwoPartyKAWindowWithFixcharPartition) {

    size_vdb expected_machine1_dummy = 0;
    ObliviousTupleList tuplesOfMachine1{
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousFixcharField("     smart", vc_size),
                    ObliviousField::makeObliviousIntField(1),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousFixcharField("     tower", vc_size),
                    ObliviousField::makeObliviousIntField(1),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousFixcharField("  mushroom", vc_size),
                    ObliviousField::makeObliviousIntField(1),
            }),
    };

    size_vdb expected_machine2_dummy = 0;
    ObliviousTupleList tuplesOfMachine2{
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousFixcharField("     giant", vc_size),
                    ObliviousField::makeObliviousIntField(1),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousFixcharField("     peace", vc_size),
                    ObliviousField::makeObliviousIntField(1),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousFixcharField("     peace", vc_size),
                    ObliviousField::makeObliviousIntField(2),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousFixcharField("     peace", vc_size),
                    ObliviousField::makeObliviousIntField(3),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousFixcharField("   fighter", vc_size),
                    ObliviousField::makeObliviousIntField(1),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousFixcharField("   fighter", vc_size),
                    ObliviousField::makeObliviousIntField(2),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousFixcharField("   tornado", vc_size),
                    ObliviousField::makeObliviousIntField(1),
            }),
    };

    utilities::DebugInSGXController m1(machineOneID, 1);
    utilities::DebugInSGXController m2(machineTwoID, 1);

    std::vector<pos_vdb> groupBys{2};
    std::vector<ObliviousExpression> expressions1{
            ObliviousExpressionFactory::makeColumn(2, type::SchemaColumn{type::FieldDataType::Fixchar, vc_size}),
            ObliviousExpressionFactory::makeRowNumber(),
    };

    std::vector<ObliviousExpression> expressions2{
            ObliviousExpressionFactory::makeColumn(2, type::SchemaColumn{type::FieldDataType::Fixchar, vc_size}),
            ObliviousExpressionFactory::makeRowNumber(),
    };

    TableID aggID = 19;
    TransmitterID tid = 14;

    KAnonymousSeqScan *scan02 = new KAnonymousSeqScan(0, k, & oinput22);
    auto agg1 = new ClusterKAnonymousWindow(machineOneID, aggID, tid, k, scan02, expressions1, groupBys, groupBys,
                                            {SortOrder::ASCEND}, false, &m1);
    KAnonymousBitonicSort * sort1 = new KAnonymousBitonicSort(4, k, agg1, {0, 1}, {SortOrder::ASCEND, SortOrder::ASCEND}, &m1);

    KAnonymousSeqScan *scan12 = new KAnonymousSeqScan(0, k, & oinputka12);
    auto agg2 = new ClusterKAnonymousWindow(machineTwoID, aggID, tid, k, scan12, expressions2, groupBys, groupBys,
                                            {SortOrder::ASCEND}, false, &m2);
    KAnonymousBitonicSort * sort2 = new KAnonymousBitonicSort(4, k, agg2, {0, 1}, {SortOrder::ASCEND, SortOrder::ASCEND}, &m2);


    std::mutex print_lock;

    std::thread first(
            FakeMultiPartyTestFixture::callKAnonymousOperatorAndVerifyOutput,
            &print_lock,
            sort1,
            machineOneID,
            &tuplesOfMachine1,
            expected_machine1_dummy
    );
    std::thread second(
            FakeMultiPartyTestFixture::callKAnonymousOperatorAndVerifyOutput,
            &print_lock,
            sort2,
            machineTwoID
            ,
            &tuplesOfMachine2,
            expected_machine2_dummy
    );

    first.join();
    second.join();
    delete sort1;
    delete sort2;
    utilities::DebugFakeLocalDataMover::instance()->clear();
}

TEST_F(FakeMultiPartyTestFixture, ThreadedTwoPartyKAWindowWithFixcharPartitionEncode) {

    size_vdb expected_machine1_dummy = 0;
    ObliviousTupleList tuplesOfMachine1{
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousFixcharField("     smart", vc_size),
                    ObliviousField::makeObliviousIntField(1),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousFixcharField("     tower", vc_size),
                    ObliviousField::makeObliviousIntField(1),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousFixcharField("  mushroom", vc_size),
                    ObliviousField::makeObliviousIntField(1),
            }),
    };

    size_vdb expected_machine2_dummy = 0;
    ObliviousTupleList tuplesOfMachine2{
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousFixcharField("     giant", vc_size),
                    ObliviousField::makeObliviousIntField(1),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousFixcharField("     peace", vc_size),
                    ObliviousField::makeObliviousIntField(1),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousFixcharField("     peace", vc_size),
                    ObliviousField::makeObliviousIntField(2),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousFixcharField("   fighter", vc_size),
                    ObliviousField::makeObliviousIntField(1),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousFixcharField("   tornado", vc_size),
                    ObliviousField::makeObliviousIntField(1),
            }),
    };

    ObliviousTupleList t22{
//            2	2	"mushroom"
//            2	2	"tower"
//            5	5	"fighter"
//            5	2	"fighter"
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("  mushroom", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("     tower", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousFixcharField("   fighter", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("   fighter", vc_size),
            }),
    };

    ObliviousTupleList tka12{
//                    3,1,"tornado"
//                    1,3,"peace"
//                    1,3,"peace"
//                    2,1,"smart"
//                    3,2,"giant"
//                    1,3,"peace"
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("   tornado", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousFixcharField("     peace", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousFixcharField("     peace", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("     smart", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("     giant", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousFixcharField("     peace", vc_size),
            }),
    };

    utilities::DebugInSGXController m1(machineOneID, 1);
    utilities::DebugInSGXController m2(machineTwoID, 1);

    std::vector<pos_vdb> groupBys{2};
    std::vector<ObliviousExpression> expressions1{
            ObliviousExpressionFactory::makeColumn(2, type::SchemaColumn{type::FieldDataType::Fixchar, vc_size}),
            ObliviousExpressionFactory::makeRowNumber(),
    };

    std::vector<ObliviousExpression> expressions2{
            ObliviousExpressionFactory::makeColumn(2, type::SchemaColumn{type::FieldDataType::Fixchar, vc_size}),
            ObliviousExpressionFactory::makeRowNumber(),
    };

    TableID aggID = 19;
    TransmitterID tid = 28;

    type::RecordSchema r22(oinput22.getSchema());
    r22.setTableID(100);
    type::RecordSchema rka12(oinputka12.getSchema());
    rka12.setTableID(120);
    ObliviousTupleTable input1(r22, t22);
    ObliviousTupleTable input2(rka12, tka12);

    auto scan02 = new KAnonymousSeqScan(0, k, & input1);
    auto agg1 = new ClusterKAnonymousWindow(machineOneID, aggID, tid, k, scan02, expressions1, groupBys, {1},
                                            {SortOrder::ASCEND}, false, nullptr);
    KAnonymousBitonicSort sort1(64, k, agg1, {0, 1}, {SortOrder::ASCEND, SortOrder::ASCEND}, nullptr);

    auto scan12 = new KAnonymousSeqScan(0, k, & input2);
    auto agg2 = new ClusterKAnonymousWindow(machineTwoID, aggID, tid, k, scan12, expressions2, groupBys, {1},
                                            {SortOrder::ASCEND}, false, nullptr);
    KAnonymousBitonicSort sort2(64, k, agg2, {0, 1}, {SortOrder::ASCEND, SortOrder::ASCEND}, nullptr);

    m1.collectTablePointer(&input1);
    m2.collectTablePointer(&input2);

    auto sort21 = m1.encodeKAnonymousOperatorAndParse(&sort1);
    auto sort22 = m2.encodeKAnonymousOperatorAndParse(&sort2);

    std::mutex print_lock;

    std::thread first(
            FakeMultiPartyTestFixture::callKAnonymousOperatorAndVerifyOutput,
            &print_lock,
            sort21,
            machineOneID,
            &tuplesOfMachine1,
            expected_machine1_dummy
    );
    std::thread second(
            FakeMultiPartyTestFixture::callKAnonymousOperatorAndVerifyOutput,
            &print_lock,
            sort22,
            machineTwoID
            ,
            &tuplesOfMachine2,
            expected_machine2_dummy
    );

    first.join();
    second.join();
    utilities::DebugFakeLocalDataMover::instance()->clear();
}