#include <gtest/gtest.h>
#include <gmock/gmock.h>

#ifndef PRINT_CLUSTER_OPERATOR_OUTPUT
//#define PRINT_CLUSTER_OPERATOR_OUTPUT
#endif

#include "in_sgx/plan/kaoperators/KAnonymousSeqScan.h"
#include <in_sgx/plan/kaoperators/KAnonymousBitonicSort.h>
#include "debug/DebugFakeLocalDataMover.h"
#include "fakeMultiPartyTestFixture.h"
#include <debug/DebugInSGXController.h>
#include <in_sgx/plan/kaoperators/ClusterKAnonymousRepartition.h>

#ifdef VERIFY_CLUSTER_OPERATOR_OUTPUT
//#undef VERIFY_CLUSTER_OPERATOR_OUTPUT
#endif

using namespace plan::kaoperators;
using namespace plan::obexpressions;

TEST_F(FakeMultiPartyTestFixture, ThreadedKATwoPartyRepartitionNoGenLevel)
{
    size_vdb expected_machine1_dummy = 0;
    ObliviousTupleList tuplesOfMachine0 {
//                    4,1,"fire pit"
//                    1,1,"world"
//                    4,1,"chimney"
//                    1,2,"lasting"
//                    4,1,"rogue"
//                    1,2,"better"
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("     smart", vc_size),
            }),
    };

    size_vdb expected_machine2_dummy = 0;
    ObliviousTupleList tuplesOfMachine1 {
//                    3,1,"tornado"
//                    1,3,"peace"
//                    1,3,"peace"
//                    2,1,"smart"
//                    3,2,"giant"
//                    1,3,"peace"
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("     world", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("    better", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("   lasting", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousFixcharField("     peace", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousFixcharField("     peace", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousFixcharField("     peace", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("   tornado", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("     giant", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("     rogue", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("   chimney", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("  fire pit", vc_size),
            }),

    };

    utilities::DebugInSGXController m1(machineOneID, 1);
    utilities::DebugInSGXController m2(machineTwoID, 1);

    std::vector<pos_vdb> orderBys;
    std::vector<SortOrder> sortOrder;
    for (pos_vdb i = 0, size = s2.size(); i < size; i++) {
        orderBys.push_back(i);
        sortOrder.push_back(SortOrder::ASCEND);
    }

    TableID repartTableID = 45;
    TransmitterID tid = 45;
    KAnonymousSeqScan * scan02 = new KAnonymousSeqScan(0, k, & oinputka02);
    ClusterKAnonymousRepartition * repart1 = new ClusterKAnonymousRepartition(machineOneID, repartTableID, tid, k,
                                                                              scan02, {0}, std::vector<pos_vdb>(),
                                                                              {}, false, &m1);
    KAnonymousBitonicSort * sort1 = new KAnonymousBitonicSort(4, k, repart1, orderBys, sortOrder, &m1);

    KAnonymousSeqScan * scan12 = new KAnonymousSeqScan(0, k, & oinputka12);
    ClusterKAnonymousRepartition * repart2 = new ClusterKAnonymousRepartition(machineTwoID, repartTableID, tid, k,
                                                                              scan12, {0}, std::vector<pos_vdb>(),
                                                                              {}, false, &m2);
    KAnonymousBitonicSort * sort2 = new KAnonymousBitonicSort(4, k, repart2, orderBys, sortOrder, &m2);

    std::mutex print_lock;

    std::thread first(
            FakeMultiPartyTestFixture::callKAnonymousOperatorAndVerifyOutput,
            &print_lock,
            sort1,
            machineOneID,
            &tuplesOfMachine0,
            expected_machine1_dummy
    );
    std::thread second(
            FakeMultiPartyTestFixture::callKAnonymousOperatorAndVerifyOutput,
            &print_lock,
            sort2,
            machineTwoID,
            &tuplesOfMachine1,
            expected_machine2_dummy
    );

    first.join();
    second.join();
    delete sort1;
    delete sort2;
    utilities::DebugFakeLocalDataMover::instance()->clear();
}

TEST_F(FakeMultiPartyTestFixture, ThreadedKATwoPartyRepartition)
{
    size_vdb localK = 2;
    size_vdb expected_machine1_dummy = 0;
    ObliviousTupleList tuplesOfMachine1 {
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("     trout", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousFixcharField("     peace", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(7),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousFixcharField(" bar bar a", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(26),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("   giggles", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(55),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("     smart", vc_size),
            }),
    };

    size_vdb expected_machine2_dummy = 0;
    ObliviousTupleList tuplesOfMachine2 {
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("   tornado", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("     giant", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("    stereo", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(10),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("    snooze", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(10),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("   tornada", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(13),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("     peace", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(14),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousFixcharField("       bar", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(19),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("     smart", vc_size),
            }),
    };

    utilities::DebugInSGXController m1(machineOneID, 1);
    utilities::DebugInSGXController m2(machineTwoID, 1);

    std::vector<pos_vdb> orderBys;
    std::vector<SortOrder> sortOrder;
    for (pos_vdb i = 0, size = s2.size(); i < size; i++) {
        orderBys.push_back(i);
        sortOrder.push_back(SortOrder::ASCEND);
    }

    // this should also work if repartition assumes 0 TableID -- it should just copy the value of _transmitterID
    // Note that this is only viable when ClusterKAnonymousRepartition is not the last operator in the segment
    TransmitterID tid = 44;
    KAnonymousSeqScan * scan02 = new KAnonymousSeqScan(0, localK, & oinputka22);
    ClusterKAnonymousRepartition * repart1 = new ClusterKAnonymousRepartition(machineOneID, 0, tid, localK, scan02, {0},
                                                                              {0},
                                                                              {}, false, &m1);
    KAnonymousBitonicSort * sort1 = new KAnonymousBitonicSort(4, localK, repart1, orderBys, sortOrder, &m1);

    KAnonymousSeqScan * scan12 = new KAnonymousSeqScan(0, localK, & oinputka32);
    ClusterKAnonymousRepartition * repart2 = new ClusterKAnonymousRepartition(machineTwoID, 0, tid, localK, scan12, {0},
                                                                              {0},
                                                                              {}, false, &m2);
    KAnonymousBitonicSort * sort2 = new KAnonymousBitonicSort(4, localK, repart2, orderBys, sortOrder, &m2);

    std::mutex print_lock;

    std::thread first(
            FakeMultiPartyTestFixture::callKAnonymousOperatorAndVerifyOutput,
            &print_lock,
            sort1,
            machineOneID,
            &tuplesOfMachine1,
            expected_machine1_dummy
    );
    std::thread second(
            FakeMultiPartyTestFixture::callKAnonymousOperatorAndVerifyOutput,
            &print_lock,
            sort2,
            machineTwoID,
            &tuplesOfMachine2,
            expected_machine2_dummy
    );

    first.join();
    second.join();

    auto printLock = new std::unique_lock<std::mutex>(print_lock);
#ifdef PRINT_CLUSTER_OPERATOR_OUTPUT
    fprintf(stdout, "\nMachine 1 generalization level:\n");
#endif
    std::vector<size_vdb> expected_machine1_genLevels {9, 9, 9};
//    std::vector<size_vdb> expected_machine1_genLevels {2, DEFAULT_MAX_GEN_LEVEL, DEFAULT_MAX_GEN_LEVEL}; // when entity is empty
    for (pos_vdb i = 0; i < 3; i++) {
#ifdef PRINT_CLUSTER_OPERATOR_OUTPUT
        fprintf(stdout, "Expected / Actual: %d / %d\n", expected_machine1_genLevels[i], sort1->getGeneralizationLevel(i));
#endif
#ifdef VERIFY_CLUSTER_OPERATOR_OUTPUT
        ASSERT_EQ(expected_machine1_genLevels[i], sort1->getGeneralizationLevel(i));
#endif
    }
#ifdef PRINT_CLUSTER_OPERATOR_OUTPUT
    fprintf(stdout, "\nMachine 2 generalization level:\n");
#endif
    std::vector<size_vdb> expected_machine2_genLevels {6, 6, 6};
//    std::vector<size_vdb> expected_machine2_genLevels {1, DEFAULT_MAX_GEN_LEVEL, DEFAULT_MAX_GEN_LEVEL}; // when entity is empty
    for (pos_vdb i = 0; i < 3; i++) {
#ifdef PRINT_CLUSTER_OPERATOR_OUTPUT
        fprintf(stdout, "Expected / Actual: %d / %d\n", expected_machine2_genLevels[i], sort2->getGeneralizationLevel(i));
#endif
#ifdef VERIFY_CLUSTER_OPERATOR_OUTPUT
        ASSERT_EQ(expected_machine2_genLevels[i], sort2->getGeneralizationLevel(i));
#endif
    }
    delete printLock;
    delete sort1;
    delete sort2;
    utilities::DebugFakeLocalDataMover::instance()->clear();
}