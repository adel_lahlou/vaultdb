#include <gtest/gtest.h>
#include <gmock/gmock.h>

#ifndef PRINT_CLUSTER_OPERATOR_OUTPUT
//#define PRINT_CLUSTER_OPERATOR_OUTPUT
#endif

#include "clusterOperatorTestFixture.h"
#include "in_sgx/plan/kaoperators/KAnonymousSeqScan.h"
#include <in_sgx/plan/kaoperators/KAnonymousBitonicSort.h>
#include "in_sgx/plan/obexpressions/obexpressions.h"
#include "debug/DebugFakeLocalDataMover.h"
#include "in_sgx/plan/kaoperators/ClusterKAnonymousHashNestedLoopJoin.h"
#include <thread>

using namespace plan::kaoperators;
using namespace plan::obexpressions;
#ifdef VAULTDB_INSGXCONTROLLER_TEST
TEST_F(ClusterOperatorTestFixture, ThreadedKATwoPartyJoin)
{
    size_vdb k = 2;
    size_vdb expected_machine0_dummy = 4;
    ObliviousTupleList tuplesOfMachine0 {
//            2,'bigger','otter',2,5,'otter'
//            2,'swift','execution',2,5,'otter'
//            4,'foo','bar',4,2,'bar'
//            4,'foo','bar',4,2,'execution1'
//            4,'foo','bar',4,2,'execution2'
//            4,'foo','bar',4,5,'bar'
//            4,'secure','execution',4,2,'bar'
//            4,'secure','execution',4,2,'execution1'
//            4,'secure','execution',4,2,'execution2'
//            4,'secure','execution',4,5,'bar'
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousVarcharField("bigger", vc_size),
                    ObliviousField::makeObliviousVarcharField("otter", vc_size),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousVarcharField("otter", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousVarcharField("swift", vc_size),
                    ObliviousField::makeObliviousVarcharField("execution", vc_size),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousVarcharField("otter", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousVarcharField("foo", vc_size),
                    ObliviousField::makeObliviousVarcharField("bar", vc_size),
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousVarcharField("bar", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousVarcharField("foo", vc_size),
                    ObliviousField::makeObliviousVarcharField("bar", vc_size),
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousVarcharField("execution1", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousVarcharField("foo", vc_size),
                    ObliviousField::makeObliviousVarcharField("bar", vc_size),
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousVarcharField("execution2", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousVarcharField("foo", vc_size),
                    ObliviousField::makeObliviousVarcharField("bar", vc_size),
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousVarcharField("bar", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousVarcharField("secure", vc_size),
                    ObliviousField::makeObliviousVarcharField("execution", vc_size),
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousVarcharField("bar", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousVarcharField("secure", vc_size),
                    ObliviousField::makeObliviousVarcharField("execution", vc_size),
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousVarcharField("execution1", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousVarcharField("secure", vc_size),
                    ObliviousField::makeObliviousVarcharField("execution", vc_size),
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousVarcharField("execution2", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousVarcharField("secure", vc_size),
                    ObliviousField::makeObliviousVarcharField("execution", vc_size),
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousVarcharField("bar", vc_size),
            }),
    };

    size_vdb expected_machine1_dummy = 2;
    ObliviousTupleList tuplesOfMachine1 {
//            1,'bigger','better',1,2,'bar'
//            1,'bigger','better',1,2,'better2'
//            1,'bigger','better',1,2,'world1'
//            1,'hard','bar',1,2,'bar'
//            1,'hard','bar',1,2,'better2'
//            1,'hard','bar',1,2,'world1'
//            1,'hello','world',1,2,'bar'
//            1,'hello','world',1,2,'better2'
//            1,'hello','world',1,2,'world1'
//            3,'secure','world',3,2,'world'
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousVarcharField("bigger", vc_size),
                    ObliviousField::makeObliviousVarcharField("better", vc_size),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousVarcharField("bar", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousVarcharField("bigger", vc_size),
                    ObliviousField::makeObliviousVarcharField("better", vc_size),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousVarcharField("better2", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousVarcharField("bigger", vc_size),
                    ObliviousField::makeObliviousVarcharField("better", vc_size),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousVarcharField("world1", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousVarcharField("hard", vc_size),
                    ObliviousField::makeObliviousVarcharField("bar", vc_size),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousVarcharField("bar", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousVarcharField("hard", vc_size),
                    ObliviousField::makeObliviousVarcharField("bar", vc_size),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousVarcharField("better2", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousVarcharField("hard", vc_size),
                    ObliviousField::makeObliviousVarcharField("bar", vc_size),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousVarcharField("world1", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousVarcharField("hello", vc_size),
                    ObliviousField::makeObliviousVarcharField("world", vc_size),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousVarcharField("bar", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousVarcharField("hello", vc_size),
                    ObliviousField::makeObliviousVarcharField("world", vc_size),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousVarcharField("better2", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousVarcharField("hello", vc_size),
                    ObliviousField::makeObliviousVarcharField("world", vc_size),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousVarcharField("world1", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousVarcharField("secure", vc_size),
                    ObliviousField::makeObliviousVarcharField("world", vc_size),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousVarcharField("world", vc_size),
            }),
    };

    MachineID firstMachine = 0;
    MachineID secondMachine = 1;
    utilities::InSGXController m1(firstMachine, 0);
    utilities::InSGXController m2(secondMachine, 0);

    std::vector<pos_vdb> orderBys;
    std::vector<SortOrder> sortOrder;
    for (pos_vdb i = 0; i < 6; i++) {
        orderBys.push_back(i);
        sortOrder.push_back(SortOrder::ASCEND);
    }

    KAnonymousSeqScan * scan01 = new KAnonymousSeqScan(0, k, oinput01);
    KAnonymousSeqScan * scan02 = new KAnonymousSeqScan(0, k, oinput02);
    ClusterKAnonymousHashJoin * join1 = new ClusterKAnonymousHashJoin(firstMachine, 3, k, scan01, scan02, std::vector<pos_vdb>{0}, std::vector<pos_vdb>{0}, &m1);
    KAnonymousBitonicSort * sort1 = new KAnonymousBitonicSort(4, k, join1, orderBys, sortOrder);

    KAnonymousSeqScan * scan11 = new KAnonymousSeqScan(0, k, oinput11);
    KAnonymousSeqScan * scan12 = new KAnonymousSeqScan(0, k, oinput12);
    ClusterKAnonymousHashJoin * join2 = new ClusterKAnonymousHashJoin(secondMachine, 3, k, scan11, scan12, std::vector<pos_vdb>{0}, std::vector<pos_vdb>{0}, &m2);
    KAnonymousBitonicSort * sort2 = new KAnonymousBitonicSort(4, k, join2, orderBys, sortOrder);

    std::mutex print_lock;

    std::thread first(
            ClusterOperatorTestFixture::callKAnonymousOperatorAndVerifyOutput,
            &print_lock,
            sort1,
            firstMachine,
            &tuplesOfMachine0,
            expected_machine0_dummy
    );
    std::thread second(
            ClusterOperatorTestFixture::callKAnonymousOperatorAndVerifyOutput,
            &print_lock,
            sort2,
            secondMachine,
            &tuplesOfMachine1,
            expected_machine1_dummy
    );

    first.join();
    second.join();
    delete sort1;
    delete sort2;
    utilities::DebugFakeLocalDataMover::clear();
}

TEST_F(ClusterOperatorTestFixture, ThreadedKAThreePartyJoin) {
    size_vdb k = 2;
    size_vdb expected_machine0_dummy = 12;
    ObliviousTupleList tuplesOfMachine0 {
//            3	"secure"	"world"	3	2	"world"
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousVarcharField("secure", vc_size),
                    ObliviousField::makeObliviousVarcharField("world", vc_size),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousVarcharField("world", vc_size),
            }),
    };

    size_vdb expected_machine1_dummy = 10;
    ObliviousTupleList tuplesOfMachine1 {
//            1	"bigger"	"better"	1	2	"bar"
//            1	"bigger"	"better"	1	2	"better2"
//            1	"bigger"	"better"	1	2	"world1"
//            1	"hard"	"bar"	1	2	"bar"
//            1	"hard"	"bar"	1	2	"better2"
//            1	"hard"	"bar"	1	2	"world1"
//            1	"hello"	"world"	1	2	"bar"
//            1	"hello"	"world"	1	2	"better2"
//            1	"hello"	"world"	1	2	"world1"
//            4	"foo"	"bar"	4	2	"bar"
//            4	"foo"	"bar"	4	2	"execution1"
//            4	"foo"	"bar"	4	2	"execution2"
//            4	"foo"	"bar"	4	5	"bar"
//            4	"secure"	"execution"	4	2	"bar"
//            4	"secure"	"execution"	4	2	"execution1"
//            4	"secure"	"execution"	4	2	"execution2"
//            4	"secure"	"execution"	4	5	"bar"
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousVarcharField("bigger", vc_size),
                    ObliviousField::makeObliviousVarcharField("better", vc_size),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousVarcharField("bar", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousVarcharField("bigger", vc_size),
                    ObliviousField::makeObliviousVarcharField("better", vc_size),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousVarcharField("better2", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousVarcharField("bigger", vc_size),
                    ObliviousField::makeObliviousVarcharField("better", vc_size),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousVarcharField("world1", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousVarcharField("hard", vc_size),
                    ObliviousField::makeObliviousVarcharField("bar", vc_size),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousVarcharField("bar", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousVarcharField("hard", vc_size),
                    ObliviousField::makeObliviousVarcharField("bar", vc_size),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousVarcharField("better2", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousVarcharField("hard", vc_size),
                    ObliviousField::makeObliviousVarcharField("bar", vc_size),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousVarcharField("world1", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousVarcharField("hello", vc_size),
                    ObliviousField::makeObliviousVarcharField("world", vc_size),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousVarcharField("bar", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousVarcharField("hello", vc_size),
                    ObliviousField::makeObliviousVarcharField("world", vc_size),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousVarcharField("better2", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousVarcharField("hello", vc_size),
                    ObliviousField::makeObliviousVarcharField("world", vc_size),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousVarcharField("world1", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousVarcharField("foo", vc_size),
                    ObliviousField::makeObliviousVarcharField("bar", vc_size),
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousVarcharField("bar", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousVarcharField("foo", vc_size),
                    ObliviousField::makeObliviousVarcharField("bar", vc_size),
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousVarcharField("execution1", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousVarcharField("foo", vc_size),
                    ObliviousField::makeObliviousVarcharField("bar", vc_size),
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousVarcharField("execution2", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousVarcharField("foo", vc_size),
                    ObliviousField::makeObliviousVarcharField("bar", vc_size),
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousVarcharField("bar", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousVarcharField("secure", vc_size),
                    ObliviousField::makeObliviousVarcharField("execution", vc_size),
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousVarcharField("bar", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousVarcharField("secure", vc_size),
                    ObliviousField::makeObliviousVarcharField("execution", vc_size),
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousVarcharField("execution1", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousVarcharField("secure", vc_size),
                    ObliviousField::makeObliviousVarcharField("execution", vc_size),
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousVarcharField("execution2", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousVarcharField("secure", vc_size),
                    ObliviousField::makeObliviousVarcharField("execution", vc_size),
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousVarcharField("bar", vc_size),
            }),
    };

    size_vdb expected_machine2_dummy = 14;
    ObliviousTupleList tuplesOfMachine2 {
//            2	"bigger"	"otter"	2	2	"mushroom"
//            2	"bigger"	"otter"	2	2	"mushroom"
//            2	"bigger"	"otter"	2	2	"tower"
//            2	"bigger"	"otter"	2	2	"tower"
//            2	"bigger"	"otter"	2	5	"otter"
//            2	"bigger"	"otter"	2	5	"otter"
//            2	"swift"	"execution"	2	2	"mushroom"
//            2	"swift"	"execution"	2	2	"tower"
//            2	"swift"	"execution"	2	5	"otter"
//            5	"foo"	"fighter"	5	2	"fighter"
//            5	"foo"	"fighter"	5	5	"fighter"
//            5	"hello"	"it is me"	5	2	"fighter"
//            5	"hello"	"it is me"	5	5	"fighter"
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousVarcharField("bigger", vc_size),
                    ObliviousField::makeObliviousVarcharField("otter", vc_size),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousVarcharField("mushroom", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousVarcharField("bigger", vc_size),
                    ObliviousField::makeObliviousVarcharField("otter", vc_size),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousVarcharField("mushroom", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousVarcharField("bigger", vc_size),
                    ObliviousField::makeObliviousVarcharField("otter", vc_size),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousVarcharField("tower", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousVarcharField("bigger", vc_size),
                    ObliviousField::makeObliviousVarcharField("otter", vc_size),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousVarcharField("tower", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousVarcharField("bigger", vc_size),
                    ObliviousField::makeObliviousVarcharField("otter", vc_size),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousVarcharField("otter", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousVarcharField("bigger", vc_size),
                    ObliviousField::makeObliviousVarcharField("otter", vc_size),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousVarcharField("otter", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousVarcharField("swift", vc_size),
                    ObliviousField::makeObliviousVarcharField("execution", vc_size),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousVarcharField("mushroom", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousVarcharField("swift", vc_size),
                    ObliviousField::makeObliviousVarcharField("execution", vc_size),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousVarcharField("tower", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousVarcharField("swift", vc_size),
                    ObliviousField::makeObliviousVarcharField("execution", vc_size),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousVarcharField("otter", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousVarcharField("foo", vc_size),
                    ObliviousField::makeObliviousVarcharField("fighter", vc_size),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousVarcharField("fighter", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousVarcharField("foo", vc_size),
                    ObliviousField::makeObliviousVarcharField("fighter", vc_size),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousVarcharField("fighter", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousVarcharField("hello", vc_size),
                    ObliviousField::makeObliviousVarcharField("it is me", vc_size),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousVarcharField("fighter", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousVarcharField("hello", vc_size),
                    ObliviousField::makeObliviousVarcharField("it is me", vc_size),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousVarcharField("fighter", vc_size),
            }),
    };

    MachineID firstMachine = 0;
    MachineID secondMachine = 1;
    MachineID thirdMachine = 2;
    utilities::InSGXController m1(firstMachine, 0);
    utilities::InSGXController m2(secondMachine, 0);
    utilities::InSGXController m3(thirdMachine, 0);

    std::vector<pos_vdb> orderBys;
    std::vector<SortOrder> sortOrder;
    for (pos_vdb i = 0; i < 6; i++) {
        orderBys.push_back(i);
        sortOrder.push_back(SortOrder::ASCEND);
    }

    KAnonymousSeqScan * scan01 = new KAnonymousSeqScan(0, k, oinput01);
    KAnonymousSeqScan * scan02 = new KAnonymousSeqScan(0, k, oinput02);
    ClusterKAnonymousHashJoin * join1 = new ClusterKAnonymousHashJoin(firstMachine, 3, k, scan01, scan02, std::vector<pos_vdb>{0}, std::vector<pos_vdb>{0}, &m1);
    KAnonymousBitonicSort * sort1 = new KAnonymousBitonicSort(4, k, join1, orderBys, sortOrder);

    KAnonymousSeqScan * scan11 = new KAnonymousSeqScan(0, k, oinput11);
    KAnonymousSeqScan * scan12 = new KAnonymousSeqScan(0, k, oinput12);
    ClusterKAnonymousHashJoin * join2 = new ClusterKAnonymousHashJoin(secondMachine, 3, k, scan11, scan12, std::vector<pos_vdb>{0}, std::vector<pos_vdb>{0}, &m2);
    KAnonymousBitonicSort * sort2 = new KAnonymousBitonicSort(4, k, join2, orderBys, sortOrder);

    KAnonymousSeqScan * scan21 = new KAnonymousSeqScan(0, k, oinput21);
    KAnonymousSeqScan * scan22 = new KAnonymousSeqScan(0, k, oinput22);
    ClusterKAnonymousHashJoin * join3 = new ClusterKAnonymousHashJoin(thirdMachine, 3, k, scan21, scan22, std::vector<pos_vdb>{0}, std::vector<pos_vdb>{0}, &m3);
    KAnonymousBitonicSort * sort3 = new KAnonymousBitonicSort(4, k, join3, orderBys, sortOrder);

    std::mutex print_lock;

    std::thread first(
            ClusterOperatorTestFixture::callKAnonymousOperatorAndVerifyOutput,
            &print_lock,
            sort1,
            firstMachine,
            &tuplesOfMachine0,
            expected_machine0_dummy
    );
    std::thread second(
            ClusterOperatorTestFixture::callKAnonymousOperatorAndVerifyOutput,
            &print_lock,
            sort2,
            secondMachine,
            &tuplesOfMachine1,
            expected_machine1_dummy
    );
    std::thread third(
            ClusterOperatorTestFixture::callKAnonymousOperatorAndVerifyOutput,
            &print_lock,
            sort3,
            thirdMachine,
            &tuplesOfMachine2,
            expected_machine2_dummy
    );

    first.join();
    second.join();
    third.join();
    delete sort1;
    delete sort2;
    delete sort3;
    utilities::DebugFakeLocalDataMover::clear();
}




TEST_F(ClusterOperatorTestFixture, ThreadedKATwoPartyOneEmpty) {
    size_vdb k = 2;
    size_vdb expected_machine0_dummy = 5;
    ObliviousTupleList tuplesOfMachine0 {
//            2	"bigger"	"execution" 2	5	"mint"
//            2	"bigger"	"execution" 2	5	"otter"
//            2	"swift"	"giraffe" 2	5	"mint"
//            2	"swift"	"giraffe" 2	5	"otter"
//            2	"swift"	"sloth" 2	5	"mint"
//            2	"swift"	"sloth" 2	5	"otter"
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousVarcharField("bigger", vc_size),
                    ObliviousField::makeObliviousVarcharField("execution", vc_size),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousVarcharField("mint", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousVarcharField("bigger", vc_size),
                    ObliviousField::makeObliviousVarcharField("execution", vc_size),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousVarcharField("otter", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousVarcharField("swift", vc_size),
                    ObliviousField::makeObliviousVarcharField("giraffe", vc_size),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousVarcharField("mint", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousVarcharField("swift", vc_size),
                    ObliviousField::makeObliviousVarcharField("giraffe", vc_size),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousVarcharField("otter", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousVarcharField("swift", vc_size),
                    ObliviousField::makeObliviousVarcharField("sloth", vc_size),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousVarcharField("mint", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousVarcharField("swift", vc_size),
                    ObliviousField::makeObliviousVarcharField("sloth", vc_size),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousVarcharField("otter", vc_size),
            }),
    };
    size_vdb expected_machine1_dummy = (k+k)*k;
    ObliviousTupleList tuplesOfMachine1 {
    };

    MachineID firstMachine = 0;
    MachineID secondMachine = 1;
    utilities::InSGXController m1(firstMachine, 0);
    utilities::InSGXController m2(secondMachine, 0);

    std::vector<pos_vdb> orderBys;
    std::vector<SortOrder> sortOrder;
    for (pos_vdb i = 0; i < 6; i++) {
        orderBys.push_back(i);
        sortOrder.push_back(SortOrder::ASCEND);
    }

    KAnonymousSeqScan * scan01 = new KAnonymousSeqScan(0, k, oinputshort01);
    KAnonymousSeqScan * scan02 = new KAnonymousSeqScan(0, k, oinputshort02);
    ClusterKAnonymousHashJoin * join1 = new ClusterKAnonymousHashJoin(firstMachine, 3, k, scan01, scan02, std::vector<pos_vdb>{0}, std::vector<pos_vdb>{0}, &m1);
    KAnonymousBitonicSort * sort1 = new KAnonymousBitonicSort(4, k, join1, orderBys, sortOrder);

    KAnonymousSeqScan * scan11 = new KAnonymousSeqScan(0, k, oinputshort11);
    KAnonymousSeqScan * scan12 = new KAnonymousSeqScan(0, k, oinputshort12);
    ClusterKAnonymousHashJoin * join2 = new ClusterKAnonymousHashJoin(secondMachine, 3, k, scan11, scan12, std::vector<pos_vdb>{0}, std::vector<pos_vdb>{0}, &m2);
    KAnonymousBitonicSort * sort2 = new KAnonymousBitonicSort(4, k, join2, orderBys, sortOrder);

    std::mutex print_lock;

    std::thread first(
            ClusterOperatorTestFixture::callKAnonymousOperatorAndVerifyOutput,
            &print_lock,
            sort1,
            firstMachine,
            &tuplesOfMachine0,
            expected_machine0_dummy
    );
    std::thread second(
            ClusterOperatorTestFixture::callKAnonymousOperatorAndVerifyOutput,
            &print_lock,
            sort2,
            secondMachine,
            &tuplesOfMachine1,
            expected_machine1_dummy
    );

    first.join();
    second.join();
    delete sort1;
    delete sort2;
    utilities::DebugFakeLocalDataMover::clear();
}

TEST_F(ClusterOperatorTestFixture, ThreadedKATwoPartyBothEmpty) {
    size_vdb k = 2;
    size_vdb expected_machine0_dummy = (k+k)*k;
    ObliviousTupleList tuplesOfMachine0{
    };
    size_vdb expected_machine1_dummy = (k+k)*k;
    ObliviousTupleList tuplesOfMachine1{
    };


    MachineID firstMachine = 0;
    MachineID secondMachine = 1;
    utilities::InSGXController m1(firstMachine, 0);
    utilities::InSGXController m2(secondMachine, 0);

    std::vector<pos_vdb> orderBys;
    std::vector<SortOrder> sortOrder;
    for (pos_vdb i = 0; i < 6; i++) {
        orderBys.push_back(i);
        sortOrder.push_back(SortOrder::ASCEND);
    }

    KAnonymousSeqScan * scan01 = new KAnonymousSeqScan(0, k, ObliviousTupleTable(s1, ObliviousTupleList()));
    KAnonymousSeqScan * scan02 = new KAnonymousSeqScan(0, k, ObliviousTupleTable(s2, ObliviousTupleList()));
    ClusterKAnonymousHashJoin * join1 = new ClusterKAnonymousHashJoin(firstMachine, 3, k, scan01, scan02, std::vector<pos_vdb>{0}, std::vector<pos_vdb>{0}, &m1);
    KAnonymousBitonicSort * sort1 = new KAnonymousBitonicSort(4, k, join1, orderBys, sortOrder);

    KAnonymousSeqScan * scan11 = new KAnonymousSeqScan(0, k, ObliviousTupleTable(s1, ObliviousTupleList()));
    KAnonymousSeqScan * scan12 = new KAnonymousSeqScan(0, k, ObliviousTupleTable(s2, ObliviousTupleList()));
    ClusterKAnonymousHashJoin * join2 = new ClusterKAnonymousHashJoin(secondMachine, 3, k, scan11, scan12, std::vector<pos_vdb>{0}, std::vector<pos_vdb>{0}, &m2);
    KAnonymousBitonicSort * sort2 = new KAnonymousBitonicSort(4, k, join2, orderBys, sortOrder);


    std::mutex print_lock;

    std::thread first(
            ClusterOperatorTestFixture::callKAnonymousOperatorAndVerifyOutput,
            &print_lock,
            sort1,
            firstMachine,
            &tuplesOfMachine0,
            expected_machine0_dummy
    );
    std::thread second(
            ClusterOperatorTestFixture::callKAnonymousOperatorAndVerifyOutput,
            &print_lock,
            sort2,
            secondMachine,
            &tuplesOfMachine1,
            expected_machine1_dummy
    );

    first.join();
    second.join();
    delete sort1;
    delete sort2;
    utilities::DebugFakeLocalDataMover::clear();
}
#else
#endif