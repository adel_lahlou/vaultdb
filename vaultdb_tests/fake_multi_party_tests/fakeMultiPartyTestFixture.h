#ifndef VAULTDB_PROJ_FAKEMULTIPARTYTESTFIXTURE_H
#define VAULTDB_PROJ_FAKEMULTIPARTYTESTFIXTURE_H

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <shared/type/RecordSchema.h>
#include <in_sgx/obdata/ObliviousTuple.h>
#include "in_sgx/obdata/ObliviousTupleTable.h"
#include "utilities/OutsideController.h"
#include "debug/DebugFakeLocalDataMover.h"

#ifndef PRINT_CLUSTER_OPERATOR_OUTPUT
//#define PRINT_CLUSTER_OPERATOR_OUTPUT
#endif

#ifndef VERIFY_CLUSTER_OPERATOR_OUTPUT
#define VERIFY_CLUSTER_OPERATOR_OUTPUT
#endif

using namespace type;
using namespace db::obdata;
using namespace db::obdata::obfield;

bool static recordschema_equal(const RecordSchema& s1, const RecordSchema& s2)
{
    if(s1.size() != s2.size())
        return false;
    for(int i = 0; i < s1.size(); ++i){
        if(s1[i].type != s2[i].type || s1[i].size != s2[i].size)
            return false;
    }
    return true;
}


bool static oblivious_tuples_equal(const ObliviousTuple& t1, const ObliviousTuple& t2)
{
    if(t1.size() != t2.size())
        return false;
    for(int i = 0; i < t1.size(); ++i){
        if(!(t1[i] == t2[i]))
            return false;
    }
    return true;
}


class FakeMultiPartyTestFixture : public ::testing::Test {
protected:
    static void callObliviousOperatorAndVerifyOutput(
            std::mutex *mtx,
            plan::oboperators::ObliviousOperator *op,
            MachineID machineID,
            ObliviousTupleList *expectedResults
    ) {
        int pos = 0;
        if (op->next() == OperatorStatus::NoMoreTuples) {
            return;
        };
        ObliviousTuple t;
        op->getCurrentTuple(t);
        std::unique_lock<std::mutex> guard2 (*mtx);
#ifdef PRINT_CLUSTER_OPERATOR_OUTPUT
        fprintf(stdout, "\nHost %d\n", machineID);
#endif
        if (t.isDummy()) {
#ifdef PRINT_CLUSTER_OPERATOR_OUTPUT
            fprintf(stdout, "DummyTuple: %s\n", t.toString().c_str());
#endif
        } else {
#ifdef PRINT_CLUSTER_OPERATOR_OUTPUT
            if (pos < expectedResults->size()){
                fprintf(stdout, "Expected / Actual \"%s\" / \"%s\"\n", (*expectedResults)[pos].toString().c_str(), t.toString().c_str());
            } else {
                fprintf(stdout, "Expected / Actual (none) / \"%s\"\n", t.toString().c_str());
            }
#endif
#ifdef VERIFY_CLUSTER_OPERATOR_OUTPUT
        ASSERT_TRUE(oblivious_tuples_equal(expectedResults->operator[](pos), t));
#endif
        }
        pos++;
        while (op->next() != OperatorStatus::NoMoreTuples) {
            op->getCurrentTuple(t);
            if (t.isDummy()) {
                continue;
            } else {
#ifdef PRINT_CLUSTER_OPERATOR_OUTPUT
                if (pos < expectedResults->size()) {
                    fprintf(stdout, "Expected / Actual \"%s\" / \"%s\"\n", (*expectedResults)[pos].toString().c_str(),
                            t.toString().c_str());
                } else {
                    fprintf(stdout, "Expected / Actual (none) / \"%s\"\n", t.toString().c_str());
                }
#endif
#ifdef VERIFY_CLUSTER_OPERATOR_OUTPUT
            ASSERT_TRUE(oblivious_tuples_equal(expectedResults->operator[](pos), t));
#endif
            }
            pos++;
        }
    };

    static void callKAnonymousOperatorAndVerifyOutput(
            std::mutex * mtx,
            plan::kaoperators::KAnonymousOperator * op,
            MachineID machineID,
            ObliviousTupleList * expectedResults,
            size_vdb expected_dummy_count
    ) {
        ObliviousTuple out;
        size_vdb i = 0, actual_dummy_count = 0;

        auto status = op->next();
        std::unique_lock<std::mutex> guard(*mtx);
#ifdef PRINT_CLUSTER_OPERATOR_OUTPUT
        fprintf(stdout, "\nHost %d\n", machineID);
#endif
        if (status == OperatorStatus::NoMoreTuples) {
#ifdef PRINT_CLUSTER_OPERATOR_OUTPUT
            fprintf(stdout, "... does not emit any tuple. Expected tuple count: %d\n", (size_vdb) expectedResults->size());
#endif
            return;
        }
        op->getCurrentTuple(out);
        if (out.isDummy()) {
#ifdef PRINT_CLUSTER_OPERATOR_OUTPUT
            fprintf(stdout, "DummyTuple: %s\n", out.toString().c_str());
#endif
            actual_dummy_count++;
        }
        else {
#ifdef PRINT_CLUSTER_OPERATOR_OUTPUT
            if (i < expectedResults->size()){
                fprintf(stdout, "Expected / Actual \"%s\" / \"%s\"\n", (*expectedResults)[i].toString().c_str(), out.toString().c_str());
            } else {
                fprintf(stdout, "Expected / Actual (none) / \"%s\"\n", out.toString().c_str());
            }
#endif
#ifdef VERIFY_CLUSTER_OPERATOR_OUTPUT
            ASSERT_TRUE(oblivious_tuples_equal(out,  (*expectedResults)[i]));
#endif
            ++i;
        }

        while (op->next() == OperatorStatus::Ok) {
            op->getCurrentTuple(out);
            if (out.isDummy()) {
#ifdef PRINT_CLUSTER_OPERATOR_OUTPUT
                fprintf(stdout, "DummyTuple: %s\n", out.toString().c_str());
#endif
                actual_dummy_count++;
                continue;
            } else {
#ifdef PRINT_CLUSTER_OPERATOR_OUTPUT
                if (i < expectedResults->size()){
                    fprintf(stdout, "Expected / Actual \"%s\" / \"%s\"\n", (*expectedResults)[i].toString().c_str(), out.toString().c_str());
                } else {
                    fprintf(stdout, "Expected / Actual (none) / \"%s\"\n", out.toString().c_str());
                    continue;
                }
#endif
#ifdef VERIFY_CLUSTER_OPERATOR_OUTPUT
                ASSERT_TRUE(oblivious_tuples_equal(out,  (*expectedResults)[i]));
#endif
                i++;
            }
        }
#ifdef VERIFY_CLUSTER_OPERATOR_OUTPUT
        ASSERT_TRUE(op->next() == OperatorStatus::NoMoreTuples);
        ASSERT_TRUE(i == expectedResults->size());
#endif
#ifdef PRINT_CLUSTER_OPERATOR_OUTPUT
        fprintf(stdout, "Expected dummy count: %d; Actual dummy count: %d\n", expected_dummy_count, actual_dummy_count);
#endif
#ifdef VERIFY_CLUSTER_OPERATOR_OUTPUT
        ASSERT_TRUE(expected_dummy_count <= actual_dummy_count);
#endif
    };

    static void callKAnonymousOperatorAndVerifyOutputWithTable(
            std::mutex * mtx,
            plan::kaoperators::KAnonymousOperator * op,
            MachineID machineID,
            ObliviousTupleTable * expectedResults,
            size_vdb expected_dummy_count
    ) {
        ObliviousTuple out;
        size_vdb i = 0, actual_dummy_count = 0;

        auto status = op->next();
        std::unique_lock<std::mutex> guard(*mtx);
#ifdef PRINT_CLUSTER_OPERATOR_OUTPUT
        fprintf(stdout, "\nHost %d\n", machineID);
#endif
        if (status == OperatorStatus::NoMoreTuples) {
#ifdef PRINT_CLUSTER_OPERATOR_OUTPUT
            fprintf(stdout, "... does not emit any tuple. Expected tuple count: %d\n", (size_vdb) expectedResults->size());
#endif
            return;
        }
        op->getCurrentTuple(out);
        if (out.isDummy()) {
#ifdef PRINT_CLUSTER_OPERATOR_OUTPUT
            fprintf(stdout, "DummyTuple: %s\n", out.toString().c_str());
#endif
            actual_dummy_count++;
        }
        else {
#ifdef PRINT_CLUSTER_OPERATOR_OUTPUT
            if (i < expectedResults->size()){
                fprintf(stdout, "Expected / Actual \"%s\" / \"%s\"\n", (*expectedResults)[i].toString().c_str(), out.toString().c_str());
            } else {
                fprintf(stdout, "Expected / Actual (none) / \"%s\"\n", out.toString().c_str());
            }
#endif
#ifdef VERIFY_CLUSTER_OPERATOR_OUTPUT
            ASSERT_TRUE(oblivious_tuples_equal(out,  (*expectedResults)[i]));
#endif
            ++i;
        }

        while (op->next() == OperatorStatus::Ok) {
            op->getCurrentTuple(out);
            if (out.isDummy()) {
#ifdef PRINT_CLUSTER_OPERATOR_OUTPUT
                fprintf(stdout, "DummyTuple: %s\n", out.toString().c_str());
#endif
                actual_dummy_count++;
                continue;
            } else {
#ifdef PRINT_CLUSTER_OPERATOR_OUTPUT
                if (i < expectedResults->size()){
                    fprintf(stdout, "Expected / Actual \"%s\" / \"%s\"\n", (*expectedResults)[i].toString().c_str(), out.toString().c_str());
                } else {
                    fprintf(stdout, "Expected / Actual (none) / \"%s\"\n", out.toString().c_str());
                    continue;
                }
#endif
#ifdef VERIFY_CLUSTER_OPERATOR_OUTPUT
                ASSERT_TRUE(oblivious_tuples_equal(out,  (*expectedResults)[i]));
#endif
                i++;
            }
        }
#ifdef VERIFY_CLUSTER_OPERATOR_OUTPUT
        ASSERT_TRUE(op->next() == OperatorStatus::NoMoreTuples);
        ASSERT_TRUE(i == expectedResults->size());
#endif
#ifdef PRINT_CLUSTER_OPERATOR_OUTPUT
        fprintf(stdout, "Expected dummy count: %d; Actual dummy count: %d\n", expected_dummy_count, actual_dummy_count);
#endif
#ifdef VERIFY_CLUSTER_OPERATOR_OUTPUT
        ASSERT_TRUE(expected_dummy_count <= actual_dummy_count);
#endif
    }


    static void runDebugQuery(MachineID machineID, TableID outputTableID, TableID input0, TableID input1, unsigned char * query, size_vdb encodingSize, int isFinalQuery) {
        utilities::DebugFakeLocalDataMover::instance()->deploySecureNewQuery(machineID, outputTableID, input0, input1, query, encodingSize, isFinalQuery);
    };

    FakeMultiPartyTestFixture()
            : username("test_user"),
              password("test"),
              healthlnk_site1("healthlnk_site1"),
              healthlnk_site2("healthlnk_site2"),
              healthlnk_site3("healthlnk_site3"),
              oinput01(s1, otuples01),
              oinput11(s1, otuples11),
              oinput02(s2, otuples02),
              oinput12(s2, otuples12),
              oinput21(s1, otuples21),
              oinput22(s2, otuples22),
              oinputshort01(s1, otuplesshort01),
              oinputshort02(s2, otuplesshort02),
              oinputshort11(s1, otuplesshort11),
              oinputshort12(s2, otuplesshort12),
              oinputka02(s2, otupleska02),
              oinputka12(s2, otupleska12),
              oinputka22(s2, otupleska22),
              oinputka32(s2, otupleska32),
              oinputka21(s1, otupleska21),
              oinputka31(s1, otupleska31),
              machineOneID(1),
              machineTwoID(2),
              machineThreeID(3),
              machineFourID(4),
              k(2)
    {
        tim.tv_sec = 0;
        tim.tv_nsec = 200000000L;
    };
    ~FakeMultiPartyTestFixture() {};

    size_vdb vc_size = 10;
    size_vdb diag_vc_size = 6;
    type::RecordSchema s1 {
            (TableID) 1, {
                    {type::FieldDataType::Int, sizeof(int)},
                    {type::FieldDataType::Fixchar, vc_size},
                    {type::FieldDataType::Fixchar, vc_size},
            }
    };
    type::RecordSchema s2{
            (TableID) 2, {
                    {type::FieldDataType::Int, sizeof(int)},
                    {type::FieldDataType::Int, sizeof(int)},
                    {type::FieldDataType::Fixchar, vc_size},
            }
    };
    type::RecordSchema s3{
            (TableID) 3, {
                    {type::FieldDataType::Int, sizeof(int)},
                    {type::FieldDataType::Int, sizeof(int)},
                    {type::FieldDataType::Int, sizeof(int)},
            }
    };

    type::RecordSchema diag { 5, {
            {type::FieldDataType::Int, INT_FIELD_SIZE},
            {type::FieldDataType::Fixchar, diag_vc_size},
            {type::FieldDataType::TimestampNoZone, TIMENOZONE_FIELD_SIZE}}};

    ObliviousTupleList otuples01 {
//            1	"hello"	"world"
//            4	"foo"	"bar"
//            1	"bigger"	"better"
//            4	"secure"	"execution"
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("     hello", vc_size),
                    ObliviousField::makeObliviousFixcharField("     world", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousFixcharField("       foo", vc_size),
                    ObliviousField::makeObliviousFixcharField("       bar", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("    bigger", vc_size),
                    ObliviousField::makeObliviousFixcharField("    better", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousFixcharField("    secure", vc_size),
                    ObliviousField::makeObliviousFixcharField(" execution", vc_size),
            }),
    };

    ObliviousTupleList otuples02 {
//            1	2	"world1"
//            4	2	"bar"
//            4	5	"bar"
//            1	2	"better2"
//            4	2	"execution1"
//            4	2	"execution2"
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("    world1", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("       bar", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousFixcharField("       bar", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("   better2", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("execution1", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("execution2", vc_size),
            }),
    };

    ObliviousTupleList otuples11{
//            2	"bigger"	"otter"
//            3	"secure"	"world"
//            1	"hard"	"bar"
//            2	"swift"	"execution"
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("    bigger", vc_size),
                    ObliviousField::makeObliviousFixcharField("     otter", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousFixcharField("    secure", vc_size),
                    ObliviousField::makeObliviousFixcharField("     world", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("      hard", vc_size),
                    ObliviousField::makeObliviousFixcharField("       bar", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("     swift", vc_size),
                    ObliviousField::makeObliviousFixcharField(" execution", vc_size),
            }),
    };

    ObliviousTupleList otuples12{
//            3	2	"world"
//            1	2	"bar"
//            2	5	"otter"
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("     world", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("       bar", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousFixcharField("     otter", vc_size),
            }),
    };

    ObliviousTupleList otuples21{
//            5	"foo"	"fighter"
//            5	"hello"	"it is me"
//            2	"bigger"	"otter"
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousFixcharField("       foo", vc_size),
                    ObliviousField::makeObliviousFixcharField("   fighter", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousFixcharField("     hello", vc_size),
                    ObliviousField::makeObliviousFixcharField("  it is me", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("    bigger", vc_size),
                    ObliviousField::makeObliviousFixcharField("     otter", vc_size),
            }),
    };

    ObliviousTupleList otuples22{
//            2	2	"mushroom"
//            2	2	"tower"
//            5	5	"fighter"
//            5	2	"fighter"
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("  mushroom", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("     tower", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousFixcharField("   fighter", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("   fighter", vc_size),
            }),
    };

    ObliviousTupleList otuplesshort01{
//            2	"swift"	"giraffe"
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("     swift", vc_size),
                    ObliviousField::makeObliviousFixcharField("   giraffe", vc_size),
            }),
    };

    ObliviousTupleList otuplesshort02{
//            2	5	"mint"
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousFixcharField("    mint", vc_size),
            }),
    };

    ObliviousTupleList otuplesshort11{
//            2	"bigger"	"execution"
//            2	"swift"	"sloth"
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("    bigger", vc_size),
                    ObliviousField::makeObliviousFixcharField(" execution", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("     swift", vc_size),
                    ObliviousField::makeObliviousFixcharField("     sloth", vc_size),
            }),
    };

    ObliviousTupleList otuplesshort12{
//            2	5	"otter"
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousFixcharField("     otter", vc_size),
            }),
    };

    ObliviousTupleList otupleska02{
//                    4,1,"fire pit"
//                    1,1,"world"
//                    4,1,"chimney"
//                    1,2,"lasting"
//                    4,1,"rogue"
//                    1,2,"better"
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("  fire pit", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("     world", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("   chimney", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("   lasting", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("     rogue", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("    better", vc_size),
            }),
    };

    ObliviousTupleList otupleska12{
//                    3,1,"tornado"
//                    1,3,"peace"
//                    1,3,"peace"
//                    2,1,"smart"
//                    3,2,"giant"
//                    1,3,"peace"
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("   tornado", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousFixcharField("     peace", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousFixcharField("     peace", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("     smart", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("     giant", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousFixcharField("     peace", vc_size),
            }),
    };


    ObliviousTupleList otupleska22 {
            // 1,1,"tornado"
            // 2,1,"tornado"
            // 14,3,"bar"
            // 13,2,"peace"
            // 19,1,"smart"
            // 4,2,"giant"
            // 2,3,"peace"
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("   tornado", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(10),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("   tornada", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(14),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousFixcharField("       bar", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(13),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("     peace", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(19),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("     smart", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("     giant", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousFixcharField("     peace", vc_size),
            }),
    };

    ObliviousTupleList otupleska32 {
            // 2,1,"trout"
            // 7,3,"bar bar a"
            // 5,2,"stereo"
            // 55,1,"smart"
            // 26,2,"giggles"
            // 10,1,"snooze"
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("     trout", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(7),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousFixcharField(" bar bar a", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("    stereo", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(55),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("     smart", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(26),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("   giggles", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(10),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("    snooze", vc_size),
            }),
    };

    ObliviousTupleList otupleska21 {
            // 2,"bar","wonder"
            // 4,"tornado","pillar"
            // 13,"one punch","man"
            // 19,"smart","bagel"
            // 14,"bar bar a","sleek"
            // 2,"friendly","giant"
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("bar", vc_size),
                    ObliviousField::makeObliviousFixcharField("wonder", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousFixcharField("tornado", vc_size),
                    ObliviousField::makeObliviousFixcharField("pillar", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(13),
                    ObliviousField::makeObliviousFixcharField("one punch", vc_size),
                    ObliviousField::makeObliviousFixcharField("man", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(19),
                    ObliviousField::makeObliviousFixcharField("smart", vc_size),
                    ObliviousField::makeObliviousFixcharField("bagel", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(14),
                    ObliviousField::makeObliviousFixcharField("bar bar a", vc_size),
                    ObliviousField::makeObliviousFixcharField("sleek", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("friendly", vc_size),
                    ObliviousField::makeObliviousFixcharField("giant", vc_size),
            },
    };

    ObliviousTupleList otupleska31{
            // 55,"trout","wombat"
            // 14,"peace","push"
            // 5,"stereo","yellow"
            // 14,"giant","tree"
            // 26,"jiggle","cake"
            // 10,"cake","forest"
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(55),
                    ObliviousField::makeObliviousFixcharField("trout", vc_size),
                    ObliviousField::makeObliviousFixcharField("wombat", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(14),
                    ObliviousField::makeObliviousFixcharField("peace", vc_size),
                    ObliviousField::makeObliviousFixcharField("push", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousFixcharField("stereo", vc_size),
                    ObliviousField::makeObliviousFixcharField("yellow", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(14),
                    ObliviousField::makeObliviousFixcharField("giant", vc_size),
                    ObliviousField::makeObliviousFixcharField("tree", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(26),
                    ObliviousField::makeObliviousFixcharField("jiggle", vc_size),
                    ObliviousField::makeObliviousFixcharField("cake", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(10),
                    ObliviousField::makeObliviousFixcharField("cake", vc_size),
                    ObliviousField::makeObliviousFixcharField("forest", vc_size),
            },
    };

    ObliviousTupleTable oinput01;
    ObliviousTupleTable oinput02;

    ObliviousTupleTable oinput11;
    ObliviousTupleTable oinput12;

    ObliviousTupleTable oinput21;
    ObliviousTupleTable oinput22;

    ObliviousTupleTable oinputshort01;
    ObliviousTupleTable oinputshort02;

    ObliviousTupleTable oinputshort11;
    ObliviousTupleTable oinputshort12;

    ObliviousTupleTable oinputka02;
    ObliviousTupleTable oinputka12;
    ObliviousTupleTable oinputka22;
    ObliviousTupleTable oinputka32;

    ObliviousTupleTable oinputka21;
    ObliviousTupleTable oinputka31;

    static size_vdb s3TuplesCount32Size;
    static size_vdb s3TuplesCount1024Size;

    static unsigned char randomS3TuplesCount32[];
    static unsigned char sortedS3TuplesCount32[];
    static unsigned char randomS3TuplesCount1024[];
    static unsigned char sortedS3TuplesCount1024[];

    ObliviousTupleTable uninitializedRandomS3TupleCount32;
    ObliviousTupleTable uninitializedSortedS3TupleCount32;
    ObliviousTupleTable uninitializedRandomS3TupleCount1024;
    ObliviousTupleTable uninitializedSortedS3TupleCount1024;

    const char * username;
    const char * password;
    const char * healthlnk_site1;
    const char * healthlnk_site2;
    const char * healthlnk_site3;

    MachineID machineOneID;
    MachineID machineTwoID;
    MachineID machineThreeID;
    MachineID machineFourID;
    size_vdb k;

    static std::mutex testLock;
    struct timespec tim, tim2;
};

#endif //VAULTDB_PROJ_FAKEMULTIPARTYTESTFIXTURE_H
