#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include "in_sgx/plan/kaoperators/KAnonymousSeqScan.h"
#include <in_sgx/plan/kaoperators/KAnonymousBitonicSort.h>
#include "in_sgx/plan/obexpressions/obexpressions.h"
#include "debug/DebugFakeLocalDataMover.h"
#include "fakeMultiPartyTestFixture.h"
#include <thread>
#include <debug/DebugInSGXController.h>
#include <in_sgx/plan/kaoperators/ClusterKAnonymousGenJoin.h>
#include <utilities/ConfigManager.h>


using namespace plan::kaoperators;
using namespace plan::obexpressions;

TEST_F(FakeMultiPartyTestFixture, ThreadedKATwoPartyBasicSimpleGenJoin)
{
    size_vdb expected_machine1_dummy = 0; // total 2
    ObliviousTupleList tuplesOfMachine1 {
            //            2,'bigger','otter',2,5,'otter'
            //            2,'swift','execution',2,5,'otter'
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("     swift", vc_size),
                    ObliviousField::makeObliviousFixcharField(" execution", vc_size),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousFixcharField("     otter", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("    bigger", vc_size),
                    ObliviousField::makeObliviousFixcharField("     otter", vc_size),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousFixcharField("     otter", vc_size),
            }),
    };

    size_vdb expected_machine2_dummy = 30; // total 48
    ObliviousTupleList tuplesOfMachine2 {
            //            1,'bigger','better',1,2,'bar'
            //            1,'bigger','better',1,2,'better2'
            //            1,'bigger','better',1,2,'world1'
            //            1,'hard','bar',1,2,'bar'
            //            1,'hard','bar',1,2,'better2'
            //            1,'hard','bar',1,2,'world1'
            //            1,'hello','world',1,2,'bar'
            //            1,'hello','world',1,2,'better2'
            //            1,'hello','world',1,2,'world1'
            //            3,'secure','world',3,2,'world'
            //            4,'foo','bar',4,2,'bar'
            //            4,'foo','bar',4,2,'execution1'
            //            4,'foo','bar',4,2,'execution2'
            //            4,'foo','bar',4,5,'bar'
            //            4,'secure','execution',4,2,'bar'
            //            4,'secure','execution',4,2,'execution1'
            //            4,'secure','execution',4,2,'execution2'
            //            4,'secure','execution',4,5,'bar'
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("      hard", vc_size),
                    ObliviousField::makeObliviousFixcharField("       bar", vc_size),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("       bar", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("      hard", vc_size),
                    ObliviousField::makeObliviousFixcharField("       bar", vc_size),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("    world1", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("      hard", vc_size),
                    ObliviousField::makeObliviousFixcharField("       bar", vc_size),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("   better2", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("     hello", vc_size),
                    ObliviousField::makeObliviousFixcharField("     world", vc_size),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("       bar", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("     hello", vc_size),
                    ObliviousField::makeObliviousFixcharField("     world", vc_size),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("    world1", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("     hello", vc_size),
                    ObliviousField::makeObliviousFixcharField("     world", vc_size),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("   better2", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("    bigger", vc_size),
                    ObliviousField::makeObliviousFixcharField("    better", vc_size),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("       bar", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("    bigger", vc_size),
                    ObliviousField::makeObliviousFixcharField("    better", vc_size),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("    world1", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("    bigger", vc_size),
                    ObliviousField::makeObliviousFixcharField("    better", vc_size),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("   better2", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousFixcharField("    secure", vc_size),
                    ObliviousField::makeObliviousFixcharField("     world", vc_size),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("     world", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousFixcharField("       foo", vc_size),
                    ObliviousField::makeObliviousFixcharField("       bar", vc_size),
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("       bar", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousFixcharField("       foo", vc_size),
                    ObliviousField::makeObliviousFixcharField("       bar", vc_size),
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("execution1", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousFixcharField("       foo", vc_size),
                    ObliviousField::makeObliviousFixcharField("       bar", vc_size),
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("execution2", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousFixcharField("       foo", vc_size),
                    ObliviousField::makeObliviousFixcharField("       bar", vc_size),
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousFixcharField("       bar", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousFixcharField("    secure", vc_size),
                    ObliviousField::makeObliviousFixcharField(" execution", vc_size),
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("       bar", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousFixcharField("    secure", vc_size),
                    ObliviousField::makeObliviousFixcharField(" execution", vc_size),
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("execution1", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousFixcharField("    secure", vc_size),
                    ObliviousField::makeObliviousFixcharField(" execution", vc_size),
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("execution2", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousFixcharField("    secure", vc_size),
                    ObliviousField::makeObliviousFixcharField(" execution", vc_size),
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousFixcharField("       bar", vc_size),
            }),
    };

    utilities::DebugInSGXController m1(machineOneID, 1);
    utilities::DebugInSGXController m2(machineTwoID, 1);

    std::vector<pos_vdb> orderBys;
    std::vector<SortOrder> sortOrder;
    for (pos_vdb i = 0; i < 6; i++) {
        orderBys.push_back(i);
        sortOrder.push_back(SortOrder::ASCEND);
    }

    TableID joinID = 3;
    TableID sortID = 180;
    TransmitterID tid1 = 45;
    TransmitterID tid2 = 55;

    KAnonymousSeqScan * scan01 = new KAnonymousSeqScan(0, k, & oinput01);
    KAnonymousSeqScan * scan02 = new KAnonymousSeqScan(0, k, & oinput02);
    ClusterKAnonymousGenJoin * join1 = new ClusterKAnonymousGenJoin(machineOneID, joinID, tid1, tid2, k, scan01, scan02,
                                                                    std::vector<pos_vdb>{0}, std::vector<pos_vdb>{0},
                                                                    std::vector<pos_vdb>{0}, std::vector<pos_vdb>{0},
                                                                    {}, {}, false, &m1);
    KAnonymousBitonicSort * sort1 = new KAnonymousBitonicSort(sortID, k, join1, orderBys, sortOrder, &m1);

    KAnonymousSeqScan * scan11 = new KAnonymousSeqScan(0, k, & oinput11);
    KAnonymousSeqScan * scan12 = new KAnonymousSeqScan(0, k, & oinput12);
    ClusterKAnonymousGenJoin * join2 = new ClusterKAnonymousGenJoin(machineTwoID, joinID, tid1, tid2, k, scan11, scan12,
                                                                    std::vector<pos_vdb>{0}, std::vector<pos_vdb>{0},
                                                                    std::vector<pos_vdb>{0}, std::vector<pos_vdb>{0},
                                                                    {}, {}, false, &m2);
    KAnonymousBitonicSort * sort2 = new KAnonymousBitonicSort(sortID, k, join2, orderBys, sortOrder, &m2);

    std::mutex print_lock;

    std::thread first(
            FakeMultiPartyTestFixture::callKAnonymousOperatorAndVerifyOutput,
            &print_lock,
            sort1,
            machineOneID,
            &tuplesOfMachine1,
            expected_machine1_dummy
    );
    std::thread second(
            FakeMultiPartyTestFixture::callKAnonymousOperatorAndVerifyOutput,
            &print_lock,
            sort2,
            machineTwoID,
            &tuplesOfMachine2,
            expected_machine2_dummy
    );

    first.join();
    second.join();
    delete sort1;
    delete sort2;
    utilities::DebugFakeLocalDataMover::instance()->clear();
}

TEST_F(FakeMultiPartyTestFixture, ThreadedKATwoPartyMultilevelGenSimpleGenJoin)
{
    size_vdb expected_machine1_dummy = 14; // total 20
    ObliviousTupleList tuplesOfMachine1 {
            // 2,"bar","wonder",2,1,"trout"
            // 2,"bar","wonder",2,3,"peace"
            // 2,"friendly","giant",2,1,"trout"
            // 2,"friendly","giant",2,3,"peace"
            // 26,"jiggle","cake",26,2,"giggles"
            // 55,"trout","wombat",55,1,"smart"
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("bar", vc_size),
                    ObliviousField::makeObliviousFixcharField("wonder", vc_size),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("trout", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("bar", vc_size),
                    ObliviousField::makeObliviousFixcharField("wonder", vc_size),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousFixcharField("peace", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("friendly", vc_size),
                    ObliviousField::makeObliviousFixcharField("giant", vc_size),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("trout", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("friendly", vc_size),
                    ObliviousField::makeObliviousFixcharField("giant", vc_size),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousFixcharField("peace", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(26),
                    ObliviousField::makeObliviousFixcharField("jiggle", vc_size),
                    ObliviousField::makeObliviousFixcharField("cake", vc_size),
                    ObliviousField::makeObliviousIntField(26),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("giggles", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(55),
                    ObliviousField::makeObliviousFixcharField("trout", vc_size),
                    ObliviousField::makeObliviousFixcharField("wombat", vc_size),
                    ObliviousField::makeObliviousIntField(55),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("smart", vc_size),
            },
    };

    size_vdb expected_machine2_dummy = 55; // total 64
    ObliviousTupleList tuplesOfMachine2 {
            // 4,"tornado","pillar",4,2,"giant"
            // 5,"stereo","yellow",5,2,"stereo"
            // 10,"cake","forest",10,1,"snooze"
            // 13,"one punch","man",13,2,"peace"
            // 14,"giant","tree",14,3,"bar"
            // 14,"peace","push",14,3,"bar"
            // 14,"bar bar a","sleek",14,3,"bar"
            // 19,"smart","bagel",19,1,"smart"
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousFixcharField("tornado", vc_size),
                    ObliviousField::makeObliviousFixcharField("pillar", vc_size),
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("giant", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousFixcharField("stereo", vc_size),
                    ObliviousField::makeObliviousFixcharField("yellow", vc_size),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("stereo", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(10),
                    ObliviousField::makeObliviousFixcharField("cake", vc_size),
                    ObliviousField::makeObliviousFixcharField("forest", vc_size),
                    ObliviousField::makeObliviousIntField(10),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("snooze", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(10),
                    ObliviousField::makeObliviousFixcharField("cake", vc_size),
                    ObliviousField::makeObliviousFixcharField("forest", vc_size),
                    ObliviousField::makeObliviousIntField(10),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("tornada", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(13),
                    ObliviousField::makeObliviousFixcharField("one punch", vc_size),
                    ObliviousField::makeObliviousFixcharField("man", vc_size),
                    ObliviousField::makeObliviousIntField(13),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("peace", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(14),
                    ObliviousField::makeObliviousFixcharField("giant", vc_size),
                    ObliviousField::makeObliviousFixcharField("tree", vc_size),
                    ObliviousField::makeObliviousIntField(14),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousFixcharField("bar", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(14),
                    ObliviousField::makeObliviousFixcharField("peace", vc_size),
                    ObliviousField::makeObliviousFixcharField("push", vc_size),
                    ObliviousField::makeObliviousIntField(14),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousFixcharField("bar", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(14),
                    ObliviousField::makeObliviousFixcharField("bar bar a", vc_size),
                    ObliviousField::makeObliviousFixcharField("sleek", vc_size),
                    ObliviousField::makeObliviousIntField(14),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousFixcharField("bar", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(19),
                    ObliviousField::makeObliviousFixcharField("smart", vc_size),
                    ObliviousField::makeObliviousFixcharField("bagel", vc_size),
                    ObliviousField::makeObliviousIntField(19),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("smart", vc_size),
            },
    };

    utilities::DebugInSGXController m1(machineOneID, 1);
    utilities::DebugInSGXController m2(machineTwoID, 1);

    std::vector<pos_vdb> orderBys;
    std::vector<SortOrder> sortOrder;
    for (pos_vdb i = 0; i < 6; i++) {
        orderBys.push_back(i);
        sortOrder.push_back(SortOrder::ASCEND);
    }

    TableID joinID = 3;
    TableID sortID = 178;
    TransmitterID tid1 = 45;
    TransmitterID tid2 = 55;

    KAnonymousSeqScan * scan21 = new KAnonymousSeqScan(0, k, & oinputka21);
    KAnonymousSeqScan * scan22 = new KAnonymousSeqScan(0, k, & oinputka22);
    ClusterKAnonymousGenJoin * join1 = new ClusterKAnonymousGenJoin(machineOneID, joinID, tid1, tid2, k, scan21, scan22,
                                                                    std::vector<pos_vdb>{0}, std::vector<pos_vdb>{0},
                                                                    std::vector<pos_vdb>{0}, std::vector<pos_vdb>{0},
                                                                    {}, {}, false, &m1);
    KAnonymousBitonicSort * sort1 = new KAnonymousBitonicSort(sortID, k, join1, orderBys, sortOrder, &m1);

    KAnonymousSeqScan * scan31 = new KAnonymousSeqScan(0, k, & oinputka31);
    KAnonymousSeqScan * scan32 = new KAnonymousSeqScan(0, k, & oinputka32);
    ClusterKAnonymousGenJoin * join2 = new ClusterKAnonymousGenJoin(machineTwoID, joinID, tid1, tid2, k, scan31, scan32,
                                                                    std::vector<pos_vdb>{0}, std::vector<pos_vdb>{0},
                                                                    std::vector<pos_vdb>{0}, std::vector<pos_vdb>{0},
                                                                    {}, {}, false, &m2);
    KAnonymousBitonicSort * sort2 = new KAnonymousBitonicSort(sortID, k, join2, orderBys, sortOrder, &m2);

    std::mutex print_lock;

    std::thread first(
            FakeMultiPartyTestFixture::callKAnonymousOperatorAndVerifyOutput,
            &print_lock,
            sort1,
            machineOneID,
            &tuplesOfMachine1,
            expected_machine1_dummy
    );
    std::thread second(
            FakeMultiPartyTestFixture::callKAnonymousOperatorAndVerifyOutput,
            &print_lock,
            sort2,
            machineTwoID,
            &tuplesOfMachine2,
            expected_machine2_dummy
    );

    first.join();
    second.join();
    delete sort1;
    delete sort2;
    utilities::DebugFakeLocalDataMover::instance()->clear();
}

static void sendPSQLQueryViaFakeLocal(MachineID dst_machine_id, type::RecordSchema psqlSchema, const char *sqlQuery, int isFinalQuery) {
    utilities::DebugFakeLocalDataMover::instance()->queryLocalPostgreAndSendIntoEnclave(dst_machine_id, psqlSchema, sqlQuery, isFinalQuery);
}

static void sendSecureQueryViaFakeLocal(MachineID dst_machine_id, TableID queryID, TableID loadInput1, TableID loadInput2, unsigned char *query, size_vdb size, int isFinalQuery) {
    utilities::DebugFakeLocalDataMover::instance()->deploySecureNewQuery(dst_machine_id, queryID, loadInput1, loadInput2, query, size, isFinalQuery);
}

#ifndef LOCAL_DEBUG_MODE
TEST_F(FakeMultiPartyTestFixture, shortAspirinProfileThree) {

    utilities::DebugInSGXController m1(machineOneID, 1);
    utilities::DebugInSGXController m2(machineTwoID, 1);
    utilities::PostgreSQLHandler handler(ConfigManager::instance()->getPgUser().get(),ConfigManager::instance()->getPgUserPass().get(),ConfigManager::instance()->getPgDB().get());

    //------------------------- left postgres query -----------------------------

    TableID psqlQuery1TableID = 5;
    TableID psqlQuery2TableID = 6;
    const char * psqlQuery1 = "select patient_id from diag";
    const char * psqlQuery2 = "select patient_id, pulse from vit";
    type::RecordSchema psqlQuery1Schema = {
            psqlQuery1TableID, {
                    {type::FieldDataType::Int, INT_FIELD_SIZE},
            }
    };
    type::RecordSchema psqlQuery2Schema = {
            psqlQuery2TableID, {
                    {type::FieldDataType::Int, INT_FIELD_SIZE},
                    {type::FieldDataType::Int, INT_FIELD_SIZE},
            }
    };

    std::thread t1_1(sendPSQLQueryViaFakeLocal, machineOneID, psqlQuery1Schema, psqlQuery1, STEP_QUERY);
    std::thread t1_2(sendPSQLQueryViaFakeLocal, machineOneID, psqlQuery2Schema, psqlQuery2, STEP_QUERY);
    std::thread t2_1(sendPSQLQueryViaFakeLocal, machineTwoID, psqlQuery1Schema, psqlQuery1, STEP_QUERY);
    std::thread t2_2(sendPSQLQueryViaFakeLocal, machineTwoID, psqlQuery2Schema, psqlQuery2, STEP_QUERY);
    t1_1.join();
    t1_2.join();
    t2_1.join();
    t2_2.join();

    //------------------------- left bottom join -------------------------------
    TableID startingTID = 100;

    TableID scanID = startingTID + 10;
    TransmitterID merge1TID = startingTID + 20;
    TransmitterID merge2TID = startingTID + 40;
    TableID join1ID = startingTID + 60; // * keep
    db::obdata::ObliviousTupleTable dummyTable1 (psqlQuery1Schema, {});
    db::obdata::ObliviousTupleTable dummyTable2 (psqlQuery2Schema, {});
    auto scan1 = new plan::kaoperators::KAnonymousSeqScan(scanID, k, &dummyTable1);
    auto scan2 = new plan::kaoperators::KAnonymousSeqScan(scanID, k, &dummyTable2);
    auto scan3 = new plan::kaoperators::KAnonymousSeqScan(scanID, k, &dummyTable1);
    auto scan4 = new plan::kaoperators::KAnonymousSeqScan(scanID, k, &dummyTable2);
    auto join1 = new plan::kaoperators::ClusterKAnonymousGenJoin (
            machineOneID, join1ID, merge1TID, merge2TID, k, scan1, scan2, {0}, {0}, {0}, {0}, {}, {}, true, nullptr);
    auto join2 = new plan::kaoperators::ClusterKAnonymousGenJoin(
            machineTwoID, join1ID, merge1TID, merge2TID, k, scan3, scan4, {0}, {0}, {0}, {0}, {}, {}, true, nullptr);

    size_vdb encodingSize1;
    size_vdb encodingSize2;
    std::shared_ptr<unsigned char> query1 (
            plan::kaoperators::KAnonymousOperator::EncodeQueryToNewArray(
                    join1, encodingSize1), [](unsigned char * p){delete[] p;});
    std::shared_ptr<unsigned char> query2 (
            plan::kaoperators::KAnonymousOperator::EncodeQueryToNewArray(
                    join2, encodingSize2), [](unsigned char * p){delete[] p;});

    auto leftJoinSchema = type::RecordSchema(join1->getSchema());
    delete join1;
    delete join2;

    std::thread t1(
            sendSecureQueryViaFakeLocal,
            machineOneID, join1ID, INPUT_TEST_TABLE_NOT_SPECIFIED, INPUT_TEST_TABLE_NOT_SPECIFIED, query1.get(), encodingSize1, STEP_QUERY);
    std::thread t2(
            sendSecureQueryViaFakeLocal,
            machineTwoID, join1ID, INPUT_TEST_TABLE_NOT_SPECIFIED, INPUT_TEST_TABLE_NOT_SPECIFIED, query2.get(), encodingSize2, STEP_QUERY);
    t1.join();
    t2.join();
    while (!utilities::DebugFakeLocalDataMover::instance()->isDone()) {
        sleep(1);
    }


    //------------------------- right postgres query -----------------------------
    TableID psqlQuery3TableID = 7;
    TableID psqlQuery4TableID = 8;
    type::RecordSchema psqlQuery3Schema = {
            psqlQuery3TableID, {
                    {type::FieldDataType::Int, INT_FIELD_SIZE},
            }
    };
    type::RecordSchema psqlQuery4Schema = {
            psqlQuery4TableID, {
                    {type::FieldDataType::Int, INT_FIELD_SIZE},
                    {type::FieldDataType::Int, INT_FIELD_SIZE},
                    {type::FieldDataType::Int, INT_FIELD_SIZE},
            }
    };

    const char * psqlQuery3 = "select patient_id from med";
    const char * psqlQuery4 = "select patient_id, gender, race from dem";

    std::thread t1_3(sendPSQLQueryViaFakeLocal, machineOneID, psqlQuery3Schema, psqlQuery3, STEP_QUERY);
    std::thread t1_4(sendPSQLQueryViaFakeLocal, machineOneID, psqlQuery4Schema, psqlQuery4, STEP_QUERY);
    std::thread t2_3(sendPSQLQueryViaFakeLocal, machineTwoID, psqlQuery3Schema, psqlQuery3, STEP_QUERY);
    std::thread t2_4(sendPSQLQueryViaFakeLocal, machineTwoID, psqlQuery4Schema, psqlQuery4, STEP_QUERY);
    t1_3.join();
    t1_4.join();
    t2_3.join();
    t2_4.join();

    //------------------------- scan repart and local sort -------------------------------
    startingTID = 200;

    scanID = startingTID + 10;
    merge1TID = startingTID + 20;
    merge2TID = startingTID + 40;
    TableID join2ID = startingTID + 60; // * keep
    db::obdata::ObliviousTupleTable dummyTable3 (psqlQuery3Schema, {});
    db::obdata::ObliviousTupleTable dummyTable4 (psqlQuery4Schema, {});
    scan1 = new plan::kaoperators::KAnonymousSeqScan(scanID, k, &dummyTable3);
    scan2 = new plan::kaoperators::KAnonymousSeqScan(scanID, k, &dummyTable4);
    scan3 = new plan::kaoperators::KAnonymousSeqScan(scanID, k, &dummyTable3);
    scan4 = new plan::kaoperators::KAnonymousSeqScan(scanID, k, &dummyTable4);
    join1 = new plan::kaoperators::ClusterKAnonymousGenJoin (
            machineOneID, join2ID, merge1TID, merge2TID, k, scan1, scan2, {0}, {0}, {0}, {0}, {}, {}, true, nullptr);
    join2 = new plan::kaoperators::ClusterKAnonymousGenJoin(
            machineTwoID, join2ID, merge1TID, merge2TID, k, scan3, scan4, {0}, {0}, {0}, {0}, {}, {}, true, nullptr);

    query1.reset(
            plan::kaoperators::KAnonymousOperator::EncodeQueryToNewArray(
                    join1, encodingSize1), [](unsigned char * p){delete[] p;});
    query2.reset(
            plan::kaoperators::KAnonymousOperator::EncodeQueryToNewArray(
                    join2, encodingSize2), [](unsigned char * p){delete[] p;});

    auto rightJoinSchema = type::RecordSchema(join1->getSchema());
    delete join1;
    delete join2;

    std::thread t3(
            sendSecureQueryViaFakeLocal,
            machineOneID, join2ID, INPUT_TEST_TABLE_NOT_SPECIFIED, INPUT_TEST_TABLE_NOT_SPECIFIED, query1.get(), encodingSize1, STEP_QUERY);
    std::thread t4(
            sendSecureQueryViaFakeLocal,
            machineTwoID, join2ID, INPUT_TEST_TABLE_NOT_SPECIFIED, INPUT_TEST_TABLE_NOT_SPECIFIED, query2.get(), encodingSize2, STEP_QUERY);
    t3.join();
    t4.join();
    while (!utilities::DebugFakeLocalDataMover::instance()->isDone()) {
        sleep(1);
    }


    //------------------------- top join -------------------------------
    startingTID = 300;

    scanID = startingTID + 10;
    merge1TID = startingTID + 20;
    merge2TID = startingTID + 40;
    TableID join3ID = startingTID + 60; // * keep
    db::obdata::ObliviousTupleTable dummyTable5 (leftJoinSchema, {});
    db::obdata::ObliviousTupleTable dummyTable6 (rightJoinSchema, {});
    scan1 = new plan::kaoperators::KAnonymousSeqScan(scanID, k, &dummyTable5);
    scan2 = new plan::kaoperators::KAnonymousSeqScan(scanID, k, &dummyTable6);
    scan3 = new plan::kaoperators::KAnonymousSeqScan(scanID, k, &dummyTable5);
    scan4 = new plan::kaoperators::KAnonymousSeqScan(scanID, k, &dummyTable6);
    join1 = new plan::kaoperators::ClusterKAnonymousGenJoin (
            machineOneID, join3ID, merge1TID, merge2TID, k, scan1, scan2, {0}, {0}, {0}, {0}, {}, {}, false, nullptr);
    join2 = new plan::kaoperators::ClusterKAnonymousGenJoin(
            machineTwoID, join3ID, merge1TID, merge2TID, k, scan3, scan4, {0}, {0}, {0}, {0}, {}, {}, false, nullptr);

    query1.reset(
            plan::kaoperators::KAnonymousOperator::EncodeQueryToNewArray(
                    join1, encodingSize1), [](unsigned char * p){delete[] p;});
    query2.reset(
            plan::kaoperators::KAnonymousOperator::EncodeQueryToNewArray(
                    join2, encodingSize2), [](unsigned char * p){delete[] p;});

    delete join1;
    delete join2;

    std::thread t5(
            sendSecureQueryViaFakeLocal,
            machineOneID, join3ID, join1ID, join2ID, query1.get(), encodingSize1, FINAL_QUERY);
    std::thread t6(
            sendSecureQueryViaFakeLocal,
            machineTwoID, join3ID, join1ID, join2ID, query2.get(), encodingSize2, FINAL_QUERY);
    t5.join();
    t6.join();
    while (!utilities::DebugFakeLocalDataMover::instance()->isDone()) {
        sleep(1);
    }

    //----------------------- result display -------------------

    ObliviousTupleTable resultTable = utilities::DebugFakeLocalDataMover::instance()->getFinalOutput();
#ifdef CONTROLLER_TEST_DEBUG_MSG
    printf("Table Output: \n");
#endif
    for (pos_vdb i = 0, size = resultTable.size(); i < size; i++) {
#ifdef CONTROLLER_TEST_DEBUG_MSG
        printf("%d: %s%s\n", i, resultTable[i].toString().c_str(), resultTable[i].isDummy() ? " (dummy)" : "");
#endif
    }
    utilities::DebugFakeLocalDataMover::instance()->clear();
}
#endif