#include "in_sgx/plan/kaoperators/KAnonymousSeqScan.h"
#include <in_sgx/plan/kaoperators/KAnonymousBitonicSort.h>
#include "debug/DebugFakeLocalDataMover.h"
#include "fakeMultiPartyTestFixture.h"
#include <debug/DebugInSGXController.h>
#include <in_sgx/plan/kaoperators/ClusterKAnonymousRepartition.h>
#include <in_sgx/plan/kaoperators/ClusterKAnonymousLimit.h>

using namespace plan::kaoperators;
using namespace plan::obexpressions;

TEST_F(FakeMultiPartyTestFixture, ThreadedKATwoPartyLimitOverLimit)
{

    size_vdb k = 2;
    size_vdb limit = 3;
    size_vdb expected_machine1_dummy = 0;
    ObliviousTupleList tuplesOfMachine1{
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(13),
                    ObliviousField::makeObliviousFixcharField("one punch", vc_size),
                    ObliviousField::makeObliviousFixcharField("man", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(26),
                    ObliviousField::makeObliviousFixcharField("jiggle", vc_size),
                    ObliviousField::makeObliviousFixcharField("cake", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(14),
                    ObliviousField::makeObliviousFixcharField("peace", vc_size),
                    ObliviousField::makeObliviousFixcharField("push", vc_size),
            },
    };

    size_vdb expected_machine2_dummy = 0;
    ObliviousTupleList tuplesOfMachine2 {
    };

    utilities::DebugInSGXController m1(machineOneID, 1);
    utilities::DebugInSGXController m2(machineTwoID, 1);

    std::vector<pos_vdb> orderBys;
    std::vector<SortOrder> sortOrder;

    TableID sortID = 25;
    TableID limitID = 45;
    TransmitterID transmitterID = 45;
    KAnonymousSeqScan * scan21 = new KAnonymousSeqScan(0, k, & oinputka21); // uses s1
    KAnonymousBitonicSort * sort1 = new KAnonymousBitonicSort(sortID, k ,scan21, {2}, {SortOrder ::ASCEND}, &m1);
    ClusterKAnonymousLimit limit1(machineOneID, limitID, transmitterID, k, sort1, limit, {2}, {SortOrder ::ASCEND}, machineOneID, &m1);

    KAnonymousSeqScan * scan31 = new KAnonymousSeqScan(0, k, & oinputka31);
    KAnonymousBitonicSort * sort2 = new KAnonymousBitonicSort(sortID, k ,scan31, {2}, {SortOrder ::ASCEND}, &m2);
    ClusterKAnonymousLimit limit2(machineTwoID, limitID, transmitterID, k, sort2, limit, {2}, {SortOrder ::ASCEND}, machineOneID, &m2);

    std::mutex print_lock;

    std::thread first(
            FakeMultiPartyTestFixture::callKAnonymousOperatorAndVerifyOutput,
            &print_lock,
            &limit1,
            machineOneID,
            &tuplesOfMachine1,
            expected_machine1_dummy
    );
    std::thread second(
            FakeMultiPartyTestFixture::callKAnonymousOperatorAndVerifyOutput,
            &print_lock,
            &limit2,
            machineTwoID,
            &tuplesOfMachine2,
            expected_machine2_dummy
    );

    first.join();
    second.join();
    utilities::DebugFakeLocalDataMover::instance()->clear();
}


TEST_F(FakeMultiPartyTestFixture, ThreadedKATwoPartyTooFewWithParse)
{

    size_vdb k = 2;
    size_vdb limit = 3;

    ObliviousTupleTable input1 (s1, {
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(13),
                    ObliviousField::makeObliviousFixcharField("one punch", vc_size),
                    ObliviousField::makeObliviousFixcharField("man", vc_size),
            },
    });
    ObliviousTupleTable input2 (s1, {
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(26),
                    ObliviousField::makeObliviousFixcharField("jiggle", vc_size),
                    ObliviousField::makeObliviousFixcharField("cake", vc_size),
            },
    });

    size_vdb expected_machine1_dummy = 1;
    ObliviousTupleList tuplesOfMachine1{
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(13),
                    ObliviousField::makeObliviousFixcharField("one punch", vc_size),
                    ObliviousField::makeObliviousFixcharField("man", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(26),
                    ObliviousField::makeObliviousFixcharField("jiggle", vc_size),
                    ObliviousField::makeObliviousFixcharField("cake", vc_size),
            },
    };

    size_vdb expected_machine2_dummy = 0;
    ObliviousTupleList tuplesOfMachine2 {
    };

    utilities::DebugInSGXController m1(machineOneID, 1);
    utilities::DebugInSGXController m2(machineTwoID, 1);

    m1.collectTablePointer(&input1);
    m2.collectTablePointer(&input2);

    std::vector<pos_vdb> orderBys;
    std::vector<SortOrder> sortOrder;

    TableID sortID = 25;
    TableID limitID = 45;
    TransmitterID transmitterID = 45;
    KAnonymousSeqScan * scan21 = new KAnonymousSeqScan(0, k, & input1); // uses s1
    KAnonymousBitonicSort * sort1 = new KAnonymousBitonicSort(sortID, k ,scan21, {2}, {SortOrder ::ASCEND}, &m1);
    ClusterKAnonymousLimit limit1(machineOneID, limitID, transmitterID, k, sort1, limit, {2}, {SortOrder ::ASCEND}, machineOneID, &m1);
    auto runPtr1 = m1.encodeKAnonymousOperatorAndParse(&limit1);

    KAnonymousSeqScan * scan31 = new KAnonymousSeqScan(0, k, & input2);
    KAnonymousBitonicSort * sort2 = new KAnonymousBitonicSort(sortID, k ,scan31, {2}, {SortOrder ::ASCEND}, &m2);
    ClusterKAnonymousLimit limit2(machineTwoID, limitID, transmitterID, k, sort2, limit, {2}, {SortOrder ::ASCEND}, machineOneID, &m2);
    auto runPtr2 = m2.encodeKAnonymousOperatorAndParse(&limit2);

    std::mutex print_lock;

    std::thread first(
            FakeMultiPartyTestFixture::callKAnonymousOperatorAndVerifyOutput,
            &print_lock,
            runPtr1,
            machineOneID,
            &tuplesOfMachine1,
            expected_machine1_dummy
    );
    std::thread second(
            FakeMultiPartyTestFixture::callKAnonymousOperatorAndVerifyOutput,
            &print_lock,
            runPtr2,
            machineTwoID,
            &tuplesOfMachine2,
            expected_machine2_dummy
    );

    first.join();
    second.join();
    // runPtr1 and runPtr2 will get deleted by decoder
    utilities::DebugFakeLocalDataMover::instance()->clear();
}