#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <in_sgx/plan/kaoperators/KAnonymousSeqScan.h>
#include <in_sgx/plan/kaoperators/ClusterKAnonymousBitonicMergeSort.h>
#include <in_sgx/plan/obexpressions/ObliviousExpressionFactory.h>
#include <debug/DebugFakeLocalDataMover.h>
#include <debug/DebugInSGXController.h>
#include <in_sgx/plan/kaoperators/ClusterKAnonymousRepartition.h>
#include <in_sgx/plan/kaoperators/ClusterKAnonymousLDAggregation.h>

#include "fakeMultiPartyTestFixture.h"

#ifndef PRINT_CLUSTER_OPERATOR_OUTPUT
//#define PRINT_CLUSTER_OPERATOR_OUTPUT
#endif

using namespace plan::obexpressions;

TEST_F(FakeMultiPartyTestFixture, ServerConstruction)
{
    TableID queryID = 9;

    //----------------------------- ENGINE SETUP ----------------------------
    utilities::DebugInSGXController m1(machineOneID, 1);

    //----------------------- QUERY ONE INPUT PREPARATION  ------------------
    size_vdb dataEncodingSize = oinput01.serializationSize();
    std::shared_ptr<unsigned char> tupleTableData (new unsigned char[dataEncodingSize], [](unsigned char * p){delete[] p;});
    oinput01.serializeTo(tupleTableData.get());

    //------------------------ QUERY ONE INPUT DELIVERY ---------------------
    utilities::DebugFakeLocalDataMover::instance()->ingestDebugTable(machineOneID, oinput01.getTableID(), dataEncodingSize, tupleTableData.get());

    //------------------------- QUERY ONE FORMULATION  ----------------------
    plan::kaoperators::KAnonymousSeqScan * scan =
            new plan::kaoperators::KAnonymousSeqScan(queryID, k, & oinput01);
    size_vdb encodingSize;
    std::shared_ptr<unsigned char> query (plan::kaoperators::KAnonymousOperator::EncodeQueryToNewArray(scan, encodingSize), [](unsigned char * p){delete[] p;});

    //-------------------------- QUERY ONE DELIVERY -------------------------
    utilities::DebugFakeLocalDataMover::instance()->deploySecureNewQuery(machineOneID, queryID, INPUT_TEST_TABLE_NOT_SPECIFIED, INPUT_TEST_TABLE_NOT_SPECIFIED, query.get(),encodingSize,FINAL_QUERY);

    //---------------------------- WAIT FOR RESULT --------------------------
    while (!utilities::DebugFakeLocalDataMover::instance()->isDone()) {
        nanosleep(&tim , &tim2);
    };
    ObliviousTupleTable * resultTable = &utilities::DebugFakeLocalDataMover::instance()->getFinalOutput();

    //---------------------------- INSPECT RESULT ---------------------------
#ifdef PRINT_CLUSTER_OPERATOR_OUTPUT
    printf("Table Output: \n");
#endif
    ASSERT_TRUE(oinput01.getSchema() == resultTable->getSchema());
    for (pos_vdb i = 0, size = resultTable->size(); i < size; i++) {
#ifdef PRINT_CLUSTER_OPERATOR_OUTPUT
        printf("Expected / Actual: %s / %s\n", oinput01[i].toString().c_str(), (*resultTable)[i].toString().c_str());
#endif
        ASSERT_TRUE(oblivious_tuples_equal(oinput01[i], (*resultTable)[i]));
    }

    //------------------------------- CLEAN UP -----------------------------
    utilities::DebugFakeLocalDataMover::instance()->clear();
}

TEST_F(FakeMultiPartyTestFixture, TwoPartySimpleQuery)
{
    //----------------------------- ENGINE SETUP ----------------------------
    utilities::DebugInSGXController m1(machineOneID, 1);
    utilities::DebugInSGXController m2(machineTwoID, 1);

    //----------------------- QUERY ONE INPUT PREPARATION  ------------------
    size_vdb dataEncodingSize1 = oinput01.serializationSize();
    size_vdb dataEncodingSize2 = oinput11.serializationSize();
    std::shared_ptr<unsigned char> tupleTableData1 (new unsigned char[dataEncodingSize1], [](unsigned char * p){delete[] p;});
    std::shared_ptr<unsigned char> tupleTableData2 (new unsigned char[dataEncodingSize2], [](unsigned char * p){delete[] p;});
    oinput01.serializeTo(tupleTableData1.get());
    oinput11.serializeTo(tupleTableData2.get());

    //------------------------ QUERY ONE INPUT DELIVERY ---------------------
    utilities::DebugFakeLocalDataMover::instance()->ingestDebugTable(machineOneID, oinput01.getTableID(), dataEncodingSize1, tupleTableData1.get());
    utilities::DebugFakeLocalDataMover::instance()->ingestDebugTable(machineTwoID, oinput11.getTableID(), dataEncodingSize2, tupleTableData2.get());

    //------------------------- QUERY ONE FORMULATION  ----------------------
    TableID queryID = 9;
    plan::kaoperators::KAnonymousSeqScan * scan1 =
            new plan::kaoperators::KAnonymousSeqScan(queryID, k, & oinput01);
    plan::kaoperators::KAnonymousSeqScan * scan2 =
            new plan::kaoperators::KAnonymousSeqScan(queryID, k, & oinput11);
    size_vdb encodingSize1, encodingSize2;
    std::shared_ptr<unsigned char> query1 (plan::kaoperators::KAnonymousOperator::EncodeQueryToNewArray(scan1, encodingSize1), [](unsigned char * p){delete[] p;});
    std::shared_ptr<unsigned char> query2 (plan::kaoperators::KAnonymousOperator::EncodeQueryToNewArray(scan2, encodingSize2), [](unsigned char * p){delete[] p;});

    //-------------------------- QUERY ONE DELIVERY -------------------------
    utilities::DebugFakeLocalDataMover::instance()->deploySecureNewQuery(machineOneID, queryID, INPUT_TEST_TABLE_NOT_SPECIFIED, INPUT_TEST_TABLE_NOT_SPECIFIED, query1.get(),encodingSize1, FINAL_QUERY);
    utilities::DebugFakeLocalDataMover::instance()->deploySecureNewQuery(machineTwoID, queryID, INPUT_TEST_TABLE_NOT_SPECIFIED, INPUT_TEST_TABLE_NOT_SPECIFIED, query2.get(),encodingSize2, FINAL_QUERY);

    //---------------------------- WAIT FOR RESULT --------------------------
    while (!utilities::DebugFakeLocalDataMover::instance()->isDone()) {
        nanosleep(&tim , &tim2);
    };
    ObliviousTupleTable * resultTable = &utilities::DebugFakeLocalDataMover::instance()->getFinalOutput();

    //---------------------------- INSPECT RESULT ---------------------------
#ifdef PRINT_CLUSTER_OPERATOR_OUTPUT
    printf("Table Output: \n");
#endif
    ASSERT_TRUE(oinput01.getSchema() == resultTable->getSchema());
    pos_vdb i = 0;
    for (size_vdb size = oinput01.size(); i < size; i++) {
#ifdef PRINT_CLUSTER_OPERATOR_OUTPUT
        printf("Expected / Actual: %s / %s\n", oinput01[i].toString().c_str(), (*resultTable)[i].toString().c_str());
#endif
        ASSERT_TRUE(oblivious_tuples_equal(oinput01[i], (*resultTable)[i]));
    }
    for (size_vdb size1 = oinput01.size(), size = size1 + oinput11.size(); i < size; i++) {
#ifdef PRINT_CLUSTER_OPERATOR_OUTPUT
        printf("Expected / Actual: %s / %s\n", oinput11[i - size1].toString().c_str(), (*resultTable)[i].toString().c_str());
#endif
        ASSERT_TRUE(oblivious_tuples_equal(oinput11[i - size1], (*resultTable)[i]));
    }

    //------------------------------- CLEAN UP -----------------------------
    utilities::DebugFakeLocalDataMover::instance()->clear();
}

TEST_F(FakeMultiPartyTestFixture, TwoPartySimpleQueryQueryArrivesFirst)
{
    //----------------------------- ENGINE SETUP ----------------------------
    utilities::DebugInSGXController m1(machineOneID, 1);
    utilities::DebugInSGXController m2(machineTwoID, 1);

    //------------------------- QUERY ONE FORMULATION  ----------------------
    TableID queryID = 9;
    plan::kaoperators::KAnonymousSeqScan * scan1 =
            new plan::kaoperators::KAnonymousSeqScan(queryID, k, & oinput01);
    plan::kaoperators::KAnonymousSeqScan * scan2 =
            new plan::kaoperators::KAnonymousSeqScan(queryID, k, & oinput11);
    size_vdb encodingSize1, encodingSize2;
    std::shared_ptr<unsigned char> query1 (plan::kaoperators::KAnonymousOperator::EncodeQueryToNewArray(scan1, encodingSize1), [](unsigned char * p){delete[] p;});
    std::shared_ptr<unsigned char> query2 (plan::kaoperators::KAnonymousOperator::EncodeQueryToNewArray(scan2, encodingSize2), [](unsigned char * p){delete[] p;});

    //-------------------------- QUERY ONE DELIVERY -------------------------
    std::thread t1 (runDebugQuery, machineOneID, queryID, INPUT_TEST_TABLE_NOT_SPECIFIED, INPUT_TEST_TABLE_NOT_SPECIFIED, query1.get(),encodingSize1, FINAL_QUERY);
    std::thread t2 (runDebugQuery, machineTwoID, queryID, INPUT_TEST_TABLE_NOT_SPECIFIED, INPUT_TEST_TABLE_NOT_SPECIFIED, query1.get(),encodingSize1, FINAL_QUERY);

    //----------------------- QUERY ONE INPUT PREPARATION  ------------------
    size_vdb dataEncodingSize1 = oinput01.serializationSize();
    size_vdb dataEncodingSize2 = oinput11.serializationSize();
    std::shared_ptr<unsigned char> tupleTableData1 (new unsigned char[dataEncodingSize1], [](unsigned char * p){delete[] p;});
    std::shared_ptr<unsigned char> tupleTableData2 (new unsigned char[dataEncodingSize2], [](unsigned char * p){delete[] p;});
    oinput01.serializeTo(tupleTableData1.get());
    oinput11.serializeTo(tupleTableData2.get());

    //------------------------ QUERY ONE INPUT DELIVERY ---------------------
    utilities::DebugFakeLocalDataMover::instance()->ingestDebugTable(machineOneID, oinput01.getTableID(), dataEncodingSize1, tupleTableData1.get());
    utilities::DebugFakeLocalDataMover::instance()->ingestDebugTable(machineTwoID, oinput11.getTableID(), dataEncodingSize2, tupleTableData2.get());

    t1.join();
    t2.join();

    //---------------------------- WAIT FOR RESULT --------------------------
    while (!utilities::DebugFakeLocalDataMover::instance()->isDone()) {
        nanosleep(&tim , &tim2);
    };
    ObliviousTupleTable * resultTable = &utilities::DebugFakeLocalDataMover::instance()->getFinalOutput();

    //---------------------------- INSPECT RESULT ---------------------------
#ifdef PRINT_CLUSTER_OPERATOR_OUTPUT
    printf("Table Output: \n");
#endif
    ASSERT_TRUE(oinput01.getSchema() == resultTable->getSchema());
    pos_vdb i = 0;

    auto table1 = oinput01;
    auto table2 = oinput11;
    if (oinput01[0].toString() != (*resultTable)[i].toString()) {
        table1 = oinput11;
        table2 = oinput01;
    }
    for (size_vdb size = table1.size(); i < size; i++) {
#ifdef PRINT_CLUSTER_OPERATOR_OUTPUT
        printf("Expected / Actual: %s / %s\n", table1[i].toString().c_str(), (*resultTable)[i].toString().c_str());
#endif
        ASSERT_TRUE(oblivious_tuples_equal(table1[i], (*resultTable)[i]));
    }
    for (size_vdb size1 = table1.size(), size = size1 + table2.size(); i < size; i++) {
#ifdef PRINT_CLUSTER_OPERATOR_OUTPUT
        printf("Expected / Actual: %s / %s\n", table2[i - size1].toString().c_str(), (*resultTable)[i].toString().c_str());
#endif
        ASSERT_TRUE(oblivious_tuples_equal(table2[i - size1], (*resultTable)[i]));
    }

    //------------------------------- CLEAN UP -----------------------------
    utilities::DebugFakeLocalDataMover::instance()->clear();
}

TEST_F(FakeMultiPartyTestFixture, TwoPartyPostgresRepeatMergeSortQuery)
{
    ObliviousTupleList expectedTuples{
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousFixcharField("008.45", diag_vc_size),
                    ObliviousField::makeObliviousIntField(3),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousFixcharField("414.01", diag_vc_size),
                    ObliviousField::makeObliviousIntField(3),
            }),
    };

    //----------------------------- ENGINE SETUP ----------------------------
    utilities::DebugInSGXController m1(machineOneID, 1);
    utilities::DebugInSGXController m2(machineTwoID, 1);


    //------------------------- POSTGRES QUERY SETUP ------------------------
    const char * machine1PostgreSQLQuery = "select patient_id, icd9, timestamp_ from diagnoses where patient_id = 1";
    const char * machine2PostgreSQLQuery = "select patient_id, icd9, timestamp_ from diagnoses where patient_id = 2";

    //----------------------- POSTGRES QUERY DELIVERY -----------------------
    utilities::DebugFakeLocalDataMover::instance()->queryLocalPostgreAndSendIntoEnclave(machineOneID, diag, machine1PostgreSQLQuery, STEP_QUERY);
    utilities::DebugFakeLocalDataMover::instance()->queryLocalPostgreAndSendIntoEnclave(machineTwoID, diag, machine2PostgreSQLQuery, STEP_QUERY);

    //------------------------- QUERY ONE FORMULATION  ----------------------
    TableID scanID1 = 10;
    TableID repartID = 40;
    TransmitterID repartTransmitterID = 55;
    TableID aggID = 70;
    TransmitterID aggTID = 100;
    std::vector<pos_vdb> groupBy {1};
    std::vector<ObliviousExpression> exp1 {
            ObliviousExpressionFactory::makeColumn(1, type::SchemaColumn{type::FieldDataType::Fixchar,diag_vc_size}),
            ObliviousExpressionFactory::makeSum(0, type::SchemaColumn{type::FieldDataType::Int,sizeof(int)}),
    };
    std::vector<ObliviousExpression> exp2 {
            ObliviousExpressionFactory::makeColumn(1, type::SchemaColumn{type::FieldDataType::Fixchar,diag_vc_size}),
            ObliviousExpressionFactory::makeSum(0, type::SchemaColumn{type::FieldDataType::Int,sizeof(int)}),
    };
    ObliviousTupleTable tempTable(diag, {});
    auto scan1 = new plan::kaoperators::KAnonymousSeqScan(scanID1, k, & tempTable);
    auto repart1 = new plan::kaoperators::ClusterKAnonymousRepartition(machineOneID, repartID, repartTransmitterID, k,
                                                                       scan1, {1}, {0},
                                                                       {}, true, nullptr);
    auto agg1 = new plan::kaoperators::ClusterKAnonymousLDAggregation(machineOneID, aggID, aggTID, k, repart1, exp1, groupBy, nullptr);

    auto scan2 = new plan::kaoperators::KAnonymousSeqScan(scanID1, k, & tempTable);
    auto repart2 = new plan::kaoperators::ClusterKAnonymousRepartition(machineTwoID, repartID, repartTransmitterID, k,
                                                                       scan2, {1}, {0},
                                                                       {}, true, nullptr);
    auto agg2 = new plan::kaoperators::ClusterKAnonymousLDAggregation(machineTwoID, aggID, aggTID, k, repart2, exp2, groupBy, nullptr);

    size_vdb encodingSize1, encodingSize2;
    std::shared_ptr<unsigned char> query1 (plan::kaoperators::KAnonymousOperator::EncodeQueryToNewArray(agg1, encodingSize1), [](unsigned char * p){delete[] p;});
    std::shared_ptr<unsigned char> query2 (plan::kaoperators::KAnonymousOperator::EncodeQueryToNewArray(agg2, encodingSize2), [](unsigned char * p){delete[] p;});

    //-------------------------- QUERY ONE DELIVERY -------------------------
    std::thread t1(runDebugQuery, machineOneID, aggID, INPUT_TEST_TABLE_NOT_SPECIFIED, INPUT_TEST_TABLE_NOT_SPECIFIED, query1.get(),encodingSize1, STEP_QUERY);
    std::thread t2(runDebugQuery, machineTwoID, aggID, INPUT_TEST_TABLE_NOT_SPECIFIED, INPUT_TEST_TABLE_NOT_SPECIFIED, query2.get(),encodingSize2, STEP_QUERY);
    t1.join();
    t2.join();
    while (!utilities::DebugFakeLocalDataMover::instance()->isDone()) {
        nanosleep(&tim , &tim2);
    };

    //------------------------- QUERY TWO FORMULATION  ----------------------
    TableID scanID2 = 140;
    TableID sortID = 170;
    TransmitterID sortTID = 200;
    ObliviousTupleTable tempTable21(agg1->getSchema(), {});
    ObliviousTupleTable tempTable22(agg2->getSchema(), {});
    plan::kaoperators::KAnonymousSeqScan * scan3 =
            new plan::kaoperators::KAnonymousSeqScan(scanID2, k, & tempTable21);
    plan::kaoperators::KAnonymousSeqScan * scan4 =
            new plan::kaoperators::KAnonymousSeqScan(scanID2, k, & tempTable22);
    plan::kaoperators::ClusterKAnonymousBitonicMergeSort sort1(machineOneID, sortID, sortTID, k, scan3, {0, 1},
                                                                     {SortOrder::ASCEND, SortOrder::ASCEND}, nullptr,
                                                                     machineOneID);
    plan::kaoperators::ClusterKAnonymousBitonicMergeSort sort2(machineTwoID, sortID, sortTID, k, scan4, {0, 1},
                                                                     {SortOrder::ASCEND, SortOrder::ASCEND}, nullptr,
                                                                     machineOneID);

    size_vdb encodingSize3, encodingSize4;
    std::shared_ptr<unsigned char> query3 (plan::kaoperators::KAnonymousOperator::EncodeQueryToNewArray(&sort1, encodingSize3), [](unsigned char * p){delete[] p;});
    std::shared_ptr<unsigned char> query4 (plan::kaoperators::KAnonymousOperator::EncodeQueryToNewArray(&sort2, encodingSize4), [](unsigned char * p){delete[] p;});


    //-------------------------- QUERY ONE DELIVERY -------------------------
    std::thread t3(runDebugQuery, machineOneID, sortID, aggID, INPUT_TEST_TABLE_NOT_SPECIFIED, query3.get(),encodingSize3, FINAL_QUERY);
    std::thread t4(runDebugQuery, machineTwoID, sortID, aggID, INPUT_TEST_TABLE_NOT_SPECIFIED, query4.get(),encodingSize4, FINAL_QUERY);
    t3.join();
    t4.join();

    //---------------------------- WAIT FOR RESULT --------------------------
    while (!utilities::DebugFakeLocalDataMover::instance()->isDone()) {
        nanosleep(&tim , &tim2);
    };
    ObliviousTupleTable * resultTable = &utilities::DebugFakeLocalDataMover::instance()->getFinalOutput();

    //---------------------------- INSPECT RESULT ---------------------------
#ifdef PRINT_CLUSTER_OPERATOR_OUTPUT
    printf("Table Output: \n");
#endif
#ifdef VERIFY_CLUSTER_OPERATOR_OUTPUT
    ASSERT_TRUE(agg1->getSchema() == resultTable->getSchema());
#endif
    pos_vdb e = 0, r = 0;
    for (size_vdb size = (size_vdb)resultTable->size(); r < size; r++) {
        if (resultTable->operator[](r).isDummy()) {
            continue;
        }
        if (e < expectedTuples.size()) {

#ifdef PRINT_CLUSTER_OPERATOR_OUTPUT
            printf("Expected / Actual: %s / %s\n", expectedTuples[e].toString().c_str(),
                   (*resultTable)[r].toString().c_str());
#endif
#ifdef VERIFY_CLUSTER_OPERATOR_OUTPUT
            ASSERT_TRUE(oblivious_tuples_equal(expectedTuples[e], (*resultTable)[r]));
#endif
            e++;
        } else {
#ifdef PRINT_CLUSTER_OPERATOR_OUTPUT
            printf("Expected / Actual: (none) / %s\n", (*resultTable)[r].toString().c_str());
#endif
#ifdef VERIFY_CLUSTER_OPERATOR_OUTPUT
            FAIL();
#endif
        }
    }
#ifdef VERIFY_CLUSTER_OPERATOR_OUTPUT
    ASSERT_TRUE(e == expectedTuples.size() && r == resultTable->size());
#endif

    //------------------------------- CLEAN UP -----------------------------
    utilities::DebugFakeLocalDataMover::instance()->clear();
}
