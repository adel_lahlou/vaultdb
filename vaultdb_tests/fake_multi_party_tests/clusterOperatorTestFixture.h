#ifndef VAULTDB_CLUSTEROPERATORTESTFIXTURE_H
#define VAULTDB_CLUSTEROPERATORTESTFIXTURE_H

#include <gtest/gtest.h>
#include "in_sgx/obdata/ObliviousTuple.h"
#include "in_sgx/obdata/ObliviousTupleTable.h"
#include "shared/type/RecordSchema.h"
#include "in_sgx/plan/oboperators/ObliviousOperator.h"
#include "in_sgx/plan/kaoperators/KAnonymousOperator.h"
#include <mutex>

using namespace db::obdata;
using namespace db::obdata::obfield;
using namespace type;

#ifndef PRINT_CLUSTER_OPERATOR_OUTPUT
//#define PRINT_CLUSTER_OPERATOR_OUTPUT
#endif

bool static recordschema_equal(const RecordSchema& s1, const RecordSchema& s2)
{
    if(s1.size() != s2.size())
        return false;

    for(int i = 0; i < s1.size(); ++i){
        if(s1[i].type != s2[i].type)
            return false;
    }

    return true;
}

bool static oblivious_tuples_equal(const ObliviousTuple& t1, const ObliviousTuple& t2)
{
    if(t1.size() != t2.size())
        return false;

    for(int i = 0; i < t1.size(); ++i){
        if(!(t1[i] == t2[i]))
            return false;
    }

    return true;
}


class ClusterOperatorTestFixture : public ::testing::Test {

    friend class ObliviousTuple;

public:
    static void callJoinAndPrintTuple(
            std::mutex * mtx,
            plan::oboperators::ObliviousOperator * op,
            MachineID machineID,
            ObliviousTupleList * expectedResults
    ) {
        int pos = 0;
        if (op->next() == OperatorStatus::NoMoreTuples) {
            return;
        };
        ObliviousTuple t;
        op->getCurrentTuple(t);
        std::unique_lock<std::mutex> guard2 (*mtx);
#ifdef PRINT_CLUSTER_OPERATOR_OUTPUT
        std::cout << std::endl << "Machine " << machineID << " printing: " << std::endl;
        std::cout << t << std::endl;
#endif
        ASSERT_TRUE(oblivious_tuples_equal(expectedResults->operator[](pos), t));
        pos++;
        while (op->next() != OperatorStatus::NoMoreTuples) {
            op->getCurrentTuple(t);
            if (t.isDummy()) {
                continue;
            }
#ifdef PRINT_CLUSTER_OPERATOR_OUTPUT
            std::cout << t << std::endl;
#endif
            ASSERT_TRUE(oblivious_tuples_equal(expectedResults->operator[](pos), t));
            pos++;
        }
    };

    static void callKAnonymousOperatorAndVerifyOutput(
            std::mutex * mtx,
            plan::kaoperators::KAnonymousOperator * op,
            MachineID machineID,
            ObliviousTupleList * expectedResults,
            size_vdb expected_dummy_count
    ) {
        ObliviousTuple out;
        size_vdb i = 0, actual_dummy_count = 0;

        op->next();
        op->getCurrentTuple(out);

        std::unique_lock<std::mutex> guard(*mtx);
#ifdef PRINT_CLUSTER_OPERATOR_OUTPUT
        std::cout << std::endl << "Machine ID " << machineID << std::endl;
#endif
        if (out.isDummy()) {
#ifdef PRINT_CLUSTER_OPERATOR_OUTPUT
            std::cout << "Dummy tuple: \"" << out << "\"" <<std::endl;
#endif
            actual_dummy_count++;
        }
        else {
#ifdef PRINT_CLUSTER_OPERATOR_OUTPUT
            if (i < expectedResults->size()){
                std::cout << "Expected / Actual: \"" << (*expectedResults)[i] << "\" / \"" << out << "\"" << std::endl;
            } else {
                std::cout << "Expected / Actual: " << "(none)" << " / \"" << out << "\"" << std::endl;
            }
#endif
            ASSERT_TRUE(oblivious_tuples_equal(out,  (*expectedResults)[i]));
            ++i;
        }

        while (op->next() == OperatorStatus::Ok) {
            op->getCurrentTuple(out);
            if (out.isDummy()) {
#ifdef PRINT_CLUSTER_OPERATOR_OUTPUT
                std::cout << "Dummy tuple: \"" << out << "\"" <<std::endl;
#endif
                actual_dummy_count++;
                continue;
            } else {
#ifdef PRINT_CLUSTER_OPERATOR_OUTPUT
                if (i < expectedResults->size()){
                    std::cout << "Expected / Actual: \"" << (*expectedResults)[i] << "\" / \"" << out << "\"" << std::endl;
                } else {
                    std::cout << "Expected / Actual: " << "(none)" << " / \"" << out << "\"" << std::endl;
                }
#endif
                ASSERT_TRUE(oblivious_tuples_equal(out,  (*expectedResults)[i]));
                i++;
            }
        }
        ASSERT_TRUE(op->next() == OperatorStatus::NoMoreTuples);
        ASSERT_TRUE(i == expectedResults->size());
#ifdef PRINT_CLUSTER_OPERATOR_OUTPUT
        std::cout << "Expected dummy count: " << expected_dummy_count << "; Actual dummy count: " << actual_dummy_count << std::endl;
#endif
        ASSERT_TRUE(expected_dummy_count <= actual_dummy_count);
    };
protected:
    ClusterOperatorTestFixture()
            : oinput01(s1, otuples01),
              oinput02(s2, otuples02),
              oinput11(s1, otuples11),
              oinput12(s2, otuples12),
              oinput21(s1, otuples21),
              oinput22(s2, otuples22),
              oinputshort01(s1, otuplesshort01),
              oinputshort02(s2, otuplesshort02),
              oinputshort11(s1, otuplesshort11),
              oinputshort12(s2, otuplesshort12),
              oinputka02(s2, otupleska02),
              oinputka12(s2, otupleska12)
    {}

    ~ClusterOperatorTestFixture() {}

    size_vdb vc_size = 10;

    type::RecordSchema s1{
            (TableID) 1, {
                    {type::FieldDataType::Int, sizeof(int)},
                    {type::FieldDataType::Fixchar, vc_size},
                    {type::FieldDataType::Fixchar, vc_size},
            }
    };

    type::RecordSchema s2{
            (TableID) 2, {
                    {type::FieldDataType::Int, sizeof(int)},
                    {type::FieldDataType::Int, sizeof(int)},
                    {type::FieldDataType::Fixchar, vc_size},
            }
    };

    ObliviousTupleList otuples01 {
//            1	"hello"	"world"
//            4	"foo"	"bar"
//            1	"bigger"	"better"
//            4	"secure"	"execution"
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("     hello", vc_size),
                    ObliviousField::makeObliviousFixcharField("     world", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousFixcharField("       foo", vc_size),
                    ObliviousField::makeObliviousFixcharField("       bar", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("    bigger", vc_size),
                    ObliviousField::makeObliviousFixcharField("    better", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousFixcharField("    secure", vc_size),
                    ObliviousField::makeObliviousFixcharField(" execution", vc_size),
            }),
    };

    ObliviousTupleList otuples02 {
//            1	2	"world1"
//            4	2	"bar"
//            4	5	"bar"
//            1	2	"better2"
//            4	2	"execution1"
//            4	2	"execution2"
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("    world1", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("       bar", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousFixcharField("       bar", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("   better2", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("execution1", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("execution2", vc_size),
            }),
    };

    ObliviousTupleList otuples11{
//            2	"bigger"	"otter"
//            3	"secure"	"world"
//            1	"hard"	"bar"
//            2	"swift"	"execution"
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("    bigger", vc_size),
                    ObliviousField::makeObliviousFixcharField("     otter", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousFixcharField("    secure", vc_size),
                    ObliviousField::makeObliviousFixcharField("     world", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("      hard", vc_size),
                    ObliviousField::makeObliviousFixcharField("       bar", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("     swift", vc_size),
                    ObliviousField::makeObliviousFixcharField(" execution", vc_size),
            }),
    };

    ObliviousTupleList otuples12{
//            3	2	"world"
//            1	2	"bar"
//            2	5	"otter"
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("     world", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("       bar", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousFixcharField("     otter", vc_size),
            }),
    };

    ObliviousTupleList otuples21{
//            5	"foo"	"fighter"
//            5	"hello"	"it is me"
//            2	"bigger"	"otter"
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousFixcharField("       foo", vc_size),
                    ObliviousField::makeObliviousFixcharField("   fighter", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousFixcharField("     hello", vc_size),
                    ObliviousField::makeObliviousFixcharField("  it is me", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("    bigger", vc_size),
                    ObliviousField::makeObliviousFixcharField("     otter", vc_size),
            }),
    };

    ObliviousTupleList otuples22{
//            2	2	"mushroom"
//            2	2	"tower"
//            5	5	"fighter"
//            5	2	"fighter"
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("  mushroom", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("     tower", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousFixcharField("   fighter", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("   fighter", vc_size),
            }),
    };

    ObliviousTupleList otuplesshort01{
//            2	"swift"	"giraffe"
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("     swift", vc_size),
                    ObliviousField::makeObliviousFixcharField("   giraffe", vc_size),
            }),
    };

    ObliviousTupleList otuplesshort02{
//            2	5	"mint"
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousFixcharField("    mint", vc_size),
            }),
    };

    ObliviousTupleList otuplesshort11{
//            2	"bigger"	"execution"
//            2	"swift"	"sloth"
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("    bigger", vc_size),
                    ObliviousField::makeObliviousFixcharField(" execution", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("     swift", vc_size),
                    ObliviousField::makeObliviousFixcharField("     sloth", vc_size),
            }),
    };

    ObliviousTupleList otuplesshort12{
//            2	5	"otter"
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousFixcharField("     otter", vc_size),
            }),
    };

    ObliviousTupleList otupleska02{
//                    4,1,"fire pit"
//                    1,1,"world"
//                    4,1,"chimney"
//                    1,2,"lasting"
//                    4,1,"rogue"
//                    1,2,"better"
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("  fire pit", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("     world", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("   chimney", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("   lasting", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("     rogue", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("    better", vc_size),
            }),
    };

    ObliviousTupleList otupleska12{
//                    3,1,"tornado"
//                    1,3,"peace"
//                    1,3,"peace"
//                    2,1,"smart"
//                    3,2,"giant"
//                    1,3,"peace"
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("   tornado", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousFixcharField("     peace", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousFixcharField("     peace", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("     smart", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("     giant", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousFixcharField("     peace", vc_size),
            }),
    };

    ObliviousTupleTable oinput01;
    ObliviousTupleTable oinput02;

    ObliviousTupleTable oinput11;
    ObliviousTupleTable oinput12;

    ObliviousTupleTable oinput21;
    ObliviousTupleTable oinput22;

    ObliviousTupleTable oinputshort01;
    ObliviousTupleTable oinputshort02;

    ObliviousTupleTable oinputshort11;
    ObliviousTupleTable oinputshort12;

    ObliviousTupleTable oinputka02;
    ObliviousTupleTable oinputka12;
};

#endif //VAULTDB_CLUSTEROPERATORTESTFIXTURE_H
