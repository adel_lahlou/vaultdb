#include <gtest/gtest.h>
#include <gmock/gmock.h>

#ifndef PRINT_CLUSTER_OPERATOR_OUTPUT
//#define PRINT_CLUSTER_OPERATOR_OUTPUT
#endif

#include "clusterOperatorTestFixture.h"
#include <in_sgx/plan/oboperators/ClusterObliviousBitonicMergeSort.h>
#include "in_sgx/plan/oboperators/ObliviousSeqScan.h"
#include "in_sgx/plan/obexpressions/obexpressions.h"
#include <thread>
#include "debug/DebugFakeLocalDataMover.h"

using namespace plan::oboperators;
using namespace plan::obexpressions;
#ifdef VAULTDB_INSGXCONTROLLER_TEST
TEST_F(ClusterOperatorTestFixture, ThreadedTwoPartyObliviousMergeSort) {
    ObliviousTupleList tuplesOfMachine0{
//            1	2	"bar"
//            1	2	"better2"
//            1	2	"world1"
//            2	5	"otter"
//            3	2	"world"
//            4	2	"bar"
//            4	2	"execution"
//            4	2	"execution"
//            4	5	"bar"
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousVarcharField("bar", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousVarcharField("better2", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousVarcharField("world1", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousVarcharField("otter", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousVarcharField("world", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousVarcharField("bar", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousVarcharField("execution1", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousVarcharField("execution2", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousVarcharField("bar", vc_size),
            },
    };

    ObliviousTupleList tuplesOfMachine1{
    };


    MachineID firstMachine = 0;
    MachineID secondMachine = 1;
    utilities::InSGXController m1(firstMachine, 0);
    utilities::InSGXController m2(secondMachine, 0);

    std::vector<pos_vdb> orderBys;
    std::vector<SortOrder> sortOrder;
    for (pos_vdb i = 0; i < 3; i++) {
        orderBys.push_back(i);
        sortOrder.push_back(SortOrder::ASCEND);
    }

    ObliviousSeqScan * scan01 = new ObliviousSeqScan(0, oinput02);
    ClusterObliviousBitonicMergeSort * sort1 = new ClusterObliviousBitonicMergeSort(
            firstMachine, 3, scan01, orderBys, sortOrder, &m1);

    ObliviousSeqScan * scan11 = new ObliviousSeqScan(0, oinput12);
    ClusterObliviousBitonicMergeSort * sort2 = new ClusterObliviousBitonicMergeSort(
            secondMachine, 3, scan11, orderBys, sortOrder, &m2);


    std::mutex print_lock;

    std::thread first(
            ClusterOperatorTestFixture::callJoinAndPrintTuple,
            &print_lock,
            sort1,
            firstMachine,
            &tuplesOfMachine0
    );
    std::thread second(
            ClusterOperatorTestFixture::callJoinAndPrintTuple,
            &print_lock,
            sort2,
            secondMachine,
            &tuplesOfMachine1
    );

    first.join();
    second.join();
    delete sort1;
    delete sort2;
    utilities::DebugFakeLocalDataMover::clear();
}

TEST_F(ClusterOperatorTestFixture, ThreadedThreePartyObliviousMergeSort) {
    ObliviousTupleList tuplesOfMachine0{
//            1	2	"bar"
//            1	2	"better2"
//            1	2	"world1"
//            2	2	"mushroom"
//            2	2	"tower"
//            2	5	"otter"
//            3	2	"world"
//            4	2	"bar"
//            4	2	"execution"
//            4	2	"execution"
//            4	5	"bar"
//            5	2	"fighter"
//            5	5	"fighter"
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousVarcharField("bar", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousVarcharField("better2", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousVarcharField("world1", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousVarcharField("mushroom", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousVarcharField("tower", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousVarcharField("otter", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousVarcharField("world", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousVarcharField("bar", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousVarcharField("execution1", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousVarcharField("execution2", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousVarcharField("bar", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousVarcharField("fighter", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousVarcharField("fighter", vc_size),
            },
    };

    ObliviousTupleList tuplesOfMachine1{
    };

    ObliviousTupleList tuplesOfMachine2{
    };


    MachineID firstMachine = 0;
    MachineID secondMachine = 1;
    MachineID thirdMachine = 2;
    utilities::InSGXController m1(firstMachine, 0);
    utilities::InSGXController m2(secondMachine, 0);
    utilities::InSGXController m3(thirdMachine, 0);

    std::vector<pos_vdb> orderBys;
    std::vector<SortOrder> sortOrder;
    for (pos_vdb i = 0; i < 3; i++) {
        orderBys.push_back(i);
        sortOrder.push_back(SortOrder::ASCEND);
    }

    ObliviousSeqScan * scan02 = new ObliviousSeqScan(0, oinput02);
    ClusterObliviousBitonicMergeSort * sort1 = new ClusterObliviousBitonicMergeSort(
            firstMachine, 3, scan02, orderBys, sortOrder, &m1);

    ObliviousSeqScan * scan12 = new ObliviousSeqScan(0, oinput12);
    ClusterObliviousBitonicMergeSort * sort2 = new ClusterObliviousBitonicMergeSort(
            secondMachine, 3, scan12, orderBys, sortOrder, &m2);

    ObliviousSeqScan * scan22 = new ObliviousSeqScan(0, oinput22);
    ClusterObliviousBitonicMergeSort * sort3 = new ClusterObliviousBitonicMergeSort(
            thirdMachine, 3, scan22, orderBys, sortOrder, &m3);

    std::mutex print_lock;

    std::thread first(
            ClusterOperatorTestFixture::callJoinAndPrintTuple,
            &print_lock,
            sort1,
            firstMachine,
            &tuplesOfMachine0
    );
    std::thread second(
            ClusterOperatorTestFixture::callJoinAndPrintTuple,
            &print_lock,
            sort2,
            secondMachine,
            &tuplesOfMachine1
    );
    std::thread third(
            ClusterOperatorTestFixture::callJoinAndPrintTuple,
            &print_lock,
            sort3,
            thirdMachine,
            &tuplesOfMachine2
    );

    first.join();
    second.join();
    third.join();
    delete sort1;
    delete sort2;
    delete sort3;
    utilities::DebugFakeLocalDataMover::clear();
}

TEST_F(ClusterOperatorTestFixture, ThreadedTwoPartyEmptyObliviousMergeSort) {
    ObliviousTupleList tuplesOfMachine0{
    };

    ObliviousTupleList tuplesOfMachine1{
    };


    MachineID firstMachine = 0;
    MachineID secondMachine = 1;
    utilities::InSGXController m1(firstMachine, 0);
    utilities::InSGXController m2(secondMachine, 0);

    std::vector<pos_vdb> orderBys;
    std::vector<SortOrder> sortOrder;
    for (pos_vdb i = 0; i < 3; i++) {
        orderBys.push_back(i);
        sortOrder.push_back(SortOrder::ASCEND);
    }

    ObliviousSeqScan * scan01 = new ObliviousSeqScan(0, ObliviousTupleTable(s2, ObliviousTupleList()));
    ClusterObliviousBitonicMergeSort * sort1 = new ClusterObliviousBitonicMergeSort(
            firstMachine, 3, scan01, orderBys, sortOrder, &m1);

    ObliviousSeqScan * scan11 = new ObliviousSeqScan(0, ObliviousTupleTable(s2, ObliviousTupleList()));
    ClusterObliviousBitonicMergeSort * sort2 = new ClusterObliviousBitonicMergeSort(
            secondMachine, 3, scan11, orderBys, sortOrder, &m2);


    std::mutex print_lock;

    std::thread first(
            ClusterOperatorTestFixture::callJoinAndPrintTuple,
            &print_lock,
            sort1,
            firstMachine,
            &tuplesOfMachine0
    );
    std::thread second(
            ClusterOperatorTestFixture::callJoinAndPrintTuple,
            &print_lock,
            sort2,
            secondMachine,
            &tuplesOfMachine1
    );

    first.join();
    second.join();
    delete sort1;
    delete sort2;
    utilities::DebugFakeLocalDataMover::clear();
}
#else
#endif