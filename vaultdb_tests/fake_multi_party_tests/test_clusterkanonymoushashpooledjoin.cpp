#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include "in_sgx/plan/kaoperators/KAnonymousSeqScan.h"
#include <in_sgx/plan/kaoperators/KAnonymousBitonicSort.h>
#include "in_sgx/plan/obexpressions/obexpressions.h"
#include "debug/DebugFakeLocalDataMover.h"
#include "in_sgx/plan/kaoperators/ClusterKAnonymousPooledJoin.h"
#include "fakeMultiPartyTestFixture.h"
#include <thread>
#include <debug/DebugInSGXController.h>

using namespace plan::kaoperators;
using namespace plan::obexpressions;

TEST_F(FakeMultiPartyTestFixture, ThreadedKAPooledTwoPartyPooledJoin)
{
    size_vdb expected_machine0_dummy = 12;
    ObliviousTupleList tuplesOfMachine0 {
//            2,'bigger','otter',2,5,'otter'
//            2,'swift','execution',2,5,'otter'
//            4,'foo','bar',4,2,'bar'
//            4,'foo','bar',4,2,'execution1'
//            4,'foo','bar',4,2,'execution2'
//            4,'foo','bar',4,5,'bar'
//            4,'secure','execution',4,2,'bar'
//            4,'secure','execution',4,2,'execution1'
//            4,'secure','execution',4,2,'execution2'
//            4,'secure','execution',4,5,'bar'
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("     swift", vc_size),
                    ObliviousField::makeObliviousFixcharField(" execution", vc_size),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousFixcharField("     otter", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("    bigger", vc_size),
                    ObliviousField::makeObliviousFixcharField("     otter", vc_size),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousFixcharField("     otter", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousFixcharField("       foo", vc_size),
                    ObliviousField::makeObliviousFixcharField("       bar", vc_size),
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("       bar", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousFixcharField("       foo", vc_size),
                    ObliviousField::makeObliviousFixcharField("       bar", vc_size),
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("execution1", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousFixcharField("       foo", vc_size),
                    ObliviousField::makeObliviousFixcharField("       bar", vc_size),
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("execution2", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousFixcharField("       foo", vc_size),
                    ObliviousField::makeObliviousFixcharField("       bar", vc_size),
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousFixcharField("       bar", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousFixcharField("    secure", vc_size),
                    ObliviousField::makeObliviousFixcharField(" execution", vc_size),
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("       bar", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousFixcharField("    secure", vc_size),
                    ObliviousField::makeObliviousFixcharField(" execution", vc_size),
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("execution1", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousFixcharField("    secure", vc_size),
                    ObliviousField::makeObliviousFixcharField(" execution", vc_size),
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("execution2", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousFixcharField("    secure", vc_size),
                    ObliviousField::makeObliviousFixcharField(" execution", vc_size),
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousFixcharField("       bar", vc_size),
            }),
    };

    size_vdb expected_machine1_dummy = 0;
    ObliviousTupleList tuplesOfMachine1 {
//            1,'bigger','better',1,2,'bar'
//            1,'bigger','better',1,2,'better2'
//            1,'bigger','better',1,2,'world1'
//            1,'hard','bar',1,2,'bar'
//            1,'hard','bar',1,2,'better2'
//            1,'hard','bar',1,2,'world1'
//            1,'hello','world',1,2,'bar'
//            1,'hello','world',1,2,'better2'
//            1,'hello','world',1,2,'world1'
//            3,'secure','world',3,2,'world'
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("      hard", vc_size),
                    ObliviousField::makeObliviousFixcharField("       bar", vc_size),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("       bar", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("      hard", vc_size),
                    ObliviousField::makeObliviousFixcharField("       bar", vc_size),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("    world1", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("      hard", vc_size),
                    ObliviousField::makeObliviousFixcharField("       bar", vc_size),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("   better2", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("     hello", vc_size),
                    ObliviousField::makeObliviousFixcharField("     world", vc_size),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("       bar", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("     hello", vc_size),
                    ObliviousField::makeObliviousFixcharField("     world", vc_size),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("    world1", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("     hello", vc_size),
                    ObliviousField::makeObliviousFixcharField("     world", vc_size),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("   better2", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("    bigger", vc_size),
                    ObliviousField::makeObliviousFixcharField("    better", vc_size),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("       bar", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("    bigger", vc_size),
                    ObliviousField::makeObliviousFixcharField("    better", vc_size),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("    world1", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("    bigger", vc_size),
                    ObliviousField::makeObliviousFixcharField("    better", vc_size),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("   better2", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousFixcharField("    secure", vc_size),
                    ObliviousField::makeObliviousFixcharField("     world", vc_size),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("     world", vc_size),
            }),
    };

    utilities::DebugInSGXController m1(machineOneID, 1);
    utilities::DebugInSGXController m2(machineTwoID, 1);

    std::vector<pos_vdb> orderBys;
    std::vector<SortOrder> sortOrder;
    for (pos_vdb i = 0; i < 6; i++) {
        orderBys.push_back(i);
        sortOrder.push_back(SortOrder::ASCEND);
    }

    TableID joinID = 3;
    TransmitterID tid1 = 45;
    TransmitterID tid2 = 55;

    KAnonymousSeqScan * scan01 = new KAnonymousSeqScan(0, k, & oinput01);
    KAnonymousSeqScan * scan02 = new KAnonymousSeqScan(0, k, & oinput02);
    ClusterKAnonymousPooledJoin * join1 = new ClusterKAnonymousPooledJoin(
            machineOneID, joinID, tid1, tid2, k, scan01, scan02,
            std::vector<pos_vdb>{0}, std::vector<pos_vdb>{0},
            std::vector<pos_vdb>{2}, std::vector<pos_vdb>{1},
            &m1);
    KAnonymousBitonicSort * sort1 = new KAnonymousBitonicSort(4, k, join1, orderBys, sortOrder, &m1);

    KAnonymousSeqScan * scan11 = new KAnonymousSeqScan(0, k, & oinput11);
    KAnonymousSeqScan * scan12 = new KAnonymousSeqScan(0, k, & oinput12);
    ClusterKAnonymousPooledJoin * join2 = new ClusterKAnonymousPooledJoin(
            machineTwoID, joinID, tid1, tid2, k, scan11, scan12,
            std::vector<pos_vdb>{0}, std::vector<pos_vdb>{0},
            std::vector<pos_vdb>{2}, std::vector<pos_vdb>{1},
            &m2);
    KAnonymousBitonicSort * sort2 = new KAnonymousBitonicSort(4, k, join2, orderBys, sortOrder, &m2);

    std::mutex print_lock;

    std::thread first(
            FakeMultiPartyTestFixture::callKAnonymousOperatorAndVerifyOutput,
            &print_lock,
            sort1,
            machineOneID,
            &tuplesOfMachine0,
            expected_machine0_dummy
    );
    std::thread second(
            FakeMultiPartyTestFixture::callKAnonymousOperatorAndVerifyOutput,
            &print_lock,
            sort2,
            machineTwoID,
            &tuplesOfMachine1,
            expected_machine1_dummy
    );

    first.join();
    second.join();
    delete sort1;
    delete sort2;
    utilities::DebugFakeLocalDataMover::instance()->clear();
}

TEST_F(FakeMultiPartyTestFixture, ThreadedKAThreePartyPooledJoin) {
    size_vdb k = 2;
    size_vdb expected_machine0_dummy = 7;
    ObliviousTupleList tuplesOfMachine0 {
//            3	"secure"	"world"	3	2	"world"
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousFixcharField("    secure", vc_size),
                    ObliviousField::makeObliviousFixcharField("     world", vc_size),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("     world", vc_size),
            }),
    };

    size_vdb expected_machine1_dummy = 23;
    ObliviousTupleList tuplesOfMachine1 {
//            1	"bigger"	"better"	1	2	"bar"
//            1	"bigger"	"better"	1	2	"better2"
//            1	"bigger"	"better"	1	2	"world1"
//            1	"hard"	"bar"	1	2	"bar"
//            1	"hard"	"bar"	1	2	"better2"
//            1	"hard"	"bar"	1	2	"world1"
//            1	"hello"	"world"	1	2	"bar"
//            1	"hello"	"world"	1	2	"better2"
//            1	"hello"	"world"	1	2	"world1"
//            4	"foo"	"bar"	4	2	"bar"
//            4	"foo"	"bar"	4	2	"execution1"
//            4	"foo"	"bar"	4	2	"execution2"
//            4	"foo"	"bar"	4	5	"bar"
//            4	"secure"	"execution"	4	2	"bar"
//            4	"secure"	"execution"	4	2	"execution1"
//            4	"secure"	"execution"	4	2	"execution2"
//            4	"secure"	"execution"	4	5	"bar"
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("      hard", vc_size),
                    ObliviousField::makeObliviousFixcharField("       bar", vc_size),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("       bar", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("      hard", vc_size),
                    ObliviousField::makeObliviousFixcharField("       bar", vc_size),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("    world1", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("      hard", vc_size),
                    ObliviousField::makeObliviousFixcharField("       bar", vc_size),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("   better2", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("     hello", vc_size),
                    ObliviousField::makeObliviousFixcharField("     world", vc_size),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("       bar", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("     hello", vc_size),
                    ObliviousField::makeObliviousFixcharField("     world", vc_size),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("    world1", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("     hello", vc_size),
                    ObliviousField::makeObliviousFixcharField("     world", vc_size),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("   better2", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("    bigger", vc_size),
                    ObliviousField::makeObliviousFixcharField("    better", vc_size),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("       bar", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("    bigger", vc_size),
                    ObliviousField::makeObliviousFixcharField("    better", vc_size),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("    world1", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("    bigger", vc_size),
                    ObliviousField::makeObliviousFixcharField("    better", vc_size),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("   better2", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousFixcharField("       foo", vc_size),
                    ObliviousField::makeObliviousFixcharField("       bar", vc_size),
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("       bar", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousFixcharField("       foo", vc_size),
                    ObliviousField::makeObliviousFixcharField("       bar", vc_size),
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("execution1", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousFixcharField("       foo", vc_size),
                    ObliviousField::makeObliviousFixcharField("       bar", vc_size),
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("execution2", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousFixcharField("       foo", vc_size),
                    ObliviousField::makeObliviousFixcharField("       bar", vc_size),
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousFixcharField("       bar", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousFixcharField("    secure", vc_size),
                    ObliviousField::makeObliviousFixcharField(" execution", vc_size),
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("       bar", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousFixcharField("    secure", vc_size),
                    ObliviousField::makeObliviousFixcharField(" execution", vc_size),
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("execution1", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousFixcharField("    secure", vc_size),
                    ObliviousField::makeObliviousFixcharField(" execution", vc_size),
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("execution2", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousFixcharField("    secure", vc_size),
                    ObliviousField::makeObliviousFixcharField(" execution", vc_size),
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousFixcharField("       bar", vc_size),
            }),
    };

    size_vdb expected_machine2_dummy = 21;
    ObliviousTupleList tuplesOfMachine2 {
//            2	"bigger"	"otter"	2	2	"mushroom"
//            2	"bigger"	"otter"	2	2	"mushroom"
//            2	"bigger"	"otter"	2	2	"tower"
//            2	"bigger"	"otter"	2	2	"tower"
//            2	"bigger"	"otter"	2	5	"otter"
//            2	"bigger"	"otter"	2	5	"otter"
//            2	"swift"	"execution"	2	2	"mushroom"
//            2	"swift"	"execution"	2	2	"tower"
//            2	"swift"	"execution"	2	5	"otter"
//            5	"foo"	"fighter"	5	2	"fighter"
//            5	"foo"	"fighter"	5	5	"fighter"
//            5	"hello"	"it is me"	5	2	"fighter"
//            5	"hello"	"it is me"	5	5	"fighter"
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("     swift", vc_size),
                    ObliviousField::makeObliviousFixcharField(" execution", vc_size),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("     tower", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("     swift", vc_size),
                    ObliviousField::makeObliviousFixcharField(" execution", vc_size),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("  mushroom", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("     swift", vc_size),
                    ObliviousField::makeObliviousFixcharField(" execution", vc_size),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousFixcharField("     otter", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("    bigger", vc_size),
                    ObliviousField::makeObliviousFixcharField("     otter", vc_size),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("     tower", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("    bigger", vc_size),
                    ObliviousField::makeObliviousFixcharField("     otter", vc_size),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("     tower", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("    bigger", vc_size),
                    ObliviousField::makeObliviousFixcharField("     otter", vc_size),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("  mushroom", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("    bigger", vc_size),
                    ObliviousField::makeObliviousFixcharField("     otter", vc_size),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("  mushroom", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("    bigger", vc_size),
                    ObliviousField::makeObliviousFixcharField("     otter", vc_size),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousFixcharField("     otter", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("    bigger", vc_size),
                    ObliviousField::makeObliviousFixcharField("     otter", vc_size),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousFixcharField("     otter", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousFixcharField("       foo", vc_size),
                    ObliviousField::makeObliviousFixcharField("   fighter", vc_size),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("   fighter", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousFixcharField("       foo", vc_size),
                    ObliviousField::makeObliviousFixcharField("   fighter", vc_size),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousFixcharField("   fighter", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousFixcharField("     hello", vc_size),
                    ObliviousField::makeObliviousFixcharField("  it is me", vc_size),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("   fighter", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousFixcharField("     hello", vc_size),
                    ObliviousField::makeObliviousFixcharField("  it is me", vc_size),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousFixcharField("   fighter", vc_size),
            }),
    };

    utilities::DebugInSGXController m1(machineOneID, 1);
    utilities::DebugInSGXController m2(machineTwoID, 1);
    utilities::DebugInSGXController m3(machineThreeID, 1);


    std::vector<pos_vdb> orderBys;
    std::vector<SortOrder> sortOrder;
    for (pos_vdb i = 0; i < 6; i++) {
        orderBys.push_back(i);
        sortOrder.push_back(SortOrder::ASCEND);
    }

    TableID joinID = 8;
    TransmitterID tid1 = 35;
    TransmitterID tid2 = 45;
    KAnonymousSeqScan * scan01 = new KAnonymousSeqScan(0, k, & oinput01);
    KAnonymousSeqScan * scan02 = new KAnonymousSeqScan(0, k, & oinput02);
    ClusterKAnonymousPooledJoin * join1 = new ClusterKAnonymousPooledJoin(
            machineOneID, joinID, tid1, tid2, k, scan01, scan02,
            std::vector<pos_vdb>{0}, std::vector<pos_vdb>{0},
            std::vector<pos_vdb>{2}, std::vector<pos_vdb>{1},
            &m1);
    KAnonymousBitonicSort * sort1 = new KAnonymousBitonicSort(4, k, join1, orderBys, sortOrder, &m1);

    KAnonymousSeqScan * scan11 = new KAnonymousSeqScan(0, k, & oinput11);
    KAnonymousSeqScan * scan12 = new KAnonymousSeqScan(0, k, & oinput12);
    ClusterKAnonymousPooledJoin * join2 = new ClusterKAnonymousPooledJoin(
            machineTwoID, joinID, tid1, tid2, k, scan11, scan12,
            std::vector<pos_vdb>{0}, std::vector<pos_vdb>{0},
            std::vector<pos_vdb>{2}, std::vector<pos_vdb>{1},
            &m2);
    KAnonymousBitonicSort * sort2 = new KAnonymousBitonicSort(4, k, join2, orderBys, sortOrder, &m2);

    KAnonymousSeqScan * scan21 = new KAnonymousSeqScan(0, k, & oinput21);
    KAnonymousSeqScan * scan22 = new KAnonymousSeqScan(0, k, & oinput22);
    ClusterKAnonymousPooledJoin * join3 = new ClusterKAnonymousPooledJoin(
            machineThreeID, joinID, tid1, tid2, k, scan21, scan22,
            std::vector<pos_vdb>{0}, std::vector<pos_vdb>{0},
            std::vector<pos_vdb>{2}, std::vector<pos_vdb>{1},
            &m3);
    KAnonymousBitonicSort * sort3 = new KAnonymousBitonicSort(4, k, join3, orderBys, sortOrder, &m3);

    std::mutex print_lock;

    std::thread first(
            FakeMultiPartyTestFixture::callKAnonymousOperatorAndVerifyOutput,
            &print_lock,
            sort1,
            machineOneID,
            &tuplesOfMachine0,
            expected_machine0_dummy
    );
    std::thread second(
            FakeMultiPartyTestFixture::callKAnonymousOperatorAndVerifyOutput,
            &print_lock,
            sort2,
            machineTwoID,
            &tuplesOfMachine1,
            expected_machine1_dummy
    );
    std::thread third(
            FakeMultiPartyTestFixture::callKAnonymousOperatorAndVerifyOutput,
            &print_lock,
            sort3,
            machineThreeID,
            &tuplesOfMachine2,
            expected_machine2_dummy
    );

    first.join();
    second.join();
    third.join();
    delete sort1;
    delete sort2;
    delete sort3;
    utilities::DebugFakeLocalDataMover::instance()->clear();
}




TEST_F(FakeMultiPartyTestFixture, ThreadedKATwoPartyOneEmptyPooled) {

    size_vdb expected_machine0_dummy = 10;
    ObliviousTupleList tuplesOfMachine0 {
//            2	"bigger"	"execution" 2	5	"mint"
//            2	"bigger"	"execution" 2	5	"otter"
//            2	"swift"	"giraffe" 2	5	"mint"
//            2	"swift"	"giraffe" 2	5	"otter"
//            2	"swift"	"sloth" 2	5	"mint"
//            2	"swift"	"sloth" 2	5	"otter"
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("     swift", vc_size),
                    ObliviousField::makeObliviousFixcharField("     sloth", vc_size),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousFixcharField("      mint", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("     swift", vc_size),
                    ObliviousField::makeObliviousFixcharField("     sloth", vc_size),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousFixcharField("     otter", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("     swift", vc_size),
                    ObliviousField::makeObliviousFixcharField("   giraffe", vc_size),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousFixcharField("      mint", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("     swift", vc_size),
                    ObliviousField::makeObliviousFixcharField("   giraffe", vc_size),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousFixcharField("     otter", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("    bigger", vc_size),
                    ObliviousField::makeObliviousFixcharField(" execution", vc_size),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousFixcharField("      mint", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("    bigger", vc_size),
                    ObliviousField::makeObliviousFixcharField(" execution", vc_size),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousFixcharField("     otter", vc_size),
            }),
    };

    size_vdb expected_machine1_dummy = 2;
    ObliviousTupleList tuplesOfMachine1 {
    };

    utilities::DebugInSGXController m1(machineOneID, 1);
    utilities::DebugInSGXController m2(machineTwoID, 1);

    std::vector<pos_vdb> orderBys;
    std::vector<SortOrder> sortOrder;
    for (pos_vdb i = 0; i < 6; i++) {
        orderBys.push_back(i);
        sortOrder.push_back(SortOrder::ASCEND);
    }

    TableID joinID = 10;
    TransmitterID tid1 = 35;
    TransmitterID tid2 = 45;
    KAnonymousSeqScan * scan01 = new KAnonymousSeqScan(0, k, & oinputshort01);
    KAnonymousSeqScan * scan02 = new KAnonymousSeqScan(0, k, & oinputshort02);
    ClusterKAnonymousPooledJoin * join1 = new ClusterKAnonymousPooledJoin(
            machineOneID, joinID, tid1, tid2, k, scan01, scan02,
            std::vector<pos_vdb>{0}, std::vector<pos_vdb>{0},
            std::vector<pos_vdb>{2}, std::vector<pos_vdb>{1},
            &m1);
    KAnonymousBitonicSort * sort1 = new KAnonymousBitonicSort(4, k, join1, orderBys, sortOrder, &m1);

    KAnonymousSeqScan * scan11 = new KAnonymousSeqScan(0, k, & oinputshort11);
    KAnonymousSeqScan * scan12 = new KAnonymousSeqScan(0, k, & oinputshort12);
    ClusterKAnonymousPooledJoin * join2 = new ClusterKAnonymousPooledJoin(
            machineTwoID, joinID, tid1, tid2, k, scan11, scan12,
            std::vector<pos_vdb>{0}, std::vector<pos_vdb>{0},
            std::vector<pos_vdb>{2}, std::vector<pos_vdb>{1},
            &m2);
    KAnonymousBitonicSort * sort2 = new KAnonymousBitonicSort(4, k, join2, orderBys, sortOrder, &m2);

    std::mutex print_lock;

    std::thread first(
            FakeMultiPartyTestFixture::callKAnonymousOperatorAndVerifyOutput,
            &print_lock,
            sort1,
            machineOneID,
            &tuplesOfMachine0,
            expected_machine0_dummy
    );
    std::thread second(
            FakeMultiPartyTestFixture::callKAnonymousOperatorAndVerifyOutput,
            &print_lock,
            sort2,
            machineTwoID,
            &tuplesOfMachine1,
            expected_machine1_dummy
    );

    first.join();
    second.join();
    delete sort1;
    delete sort2;
    utilities::DebugFakeLocalDataMover::instance()->clear();
}

TEST_F(FakeMultiPartyTestFixture, ThreadedKATwoPartyBothEmptyPooled) {
    size_vdb expected_machine0_dummy = k;
    ObliviousTupleList tuplesOfMachine0{
    };
    size_vdb expected_machine1_dummy = k;
    ObliviousTupleList tuplesOfMachine1{
    };


    utilities::DebugInSGXController m1(machineOneID, 1);
    utilities::DebugInSGXController m2(machineTwoID, 1);

    std::vector<pos_vdb> orderBys;
    std::vector<SortOrder> sortOrder;
    for (pos_vdb i = 0; i < 6; i++) {
        orderBys.push_back(i);
        sortOrder.push_back(SortOrder::ASCEND);
    }

    TableID joinID = 12;
    TransmitterID tid1 = 35;
    TransmitterID tid2 = 45;
    ObliviousTupleTable t1(s1, ObliviousTupleList());
    ObliviousTupleTable t2(s2, ObliviousTupleList());
    KAnonymousSeqScan * scan01 = new KAnonymousSeqScan(0, k, & t1);
    KAnonymousSeqScan * scan02 = new KAnonymousSeqScan(0, k, & t2);
    ClusterKAnonymousPooledJoin * join1 = new ClusterKAnonymousPooledJoin(
            machineOneID, joinID, tid1, tid2, k, scan01, scan02,
            std::vector<pos_vdb>{0}, std::vector<pos_vdb>{0},
            std::vector<pos_vdb>{2}, std::vector<pos_vdb>{1},
            &m1);
    KAnonymousBitonicSort * sort1 = new KAnonymousBitonicSort(4, k, join1, orderBys, sortOrder, &m1);

    KAnonymousSeqScan * scan11 = new KAnonymousSeqScan(0, k, & t1);
    KAnonymousSeqScan * scan12 = new KAnonymousSeqScan(0, k, & t2);
    ClusterKAnonymousPooledJoin * join2 = new ClusterKAnonymousPooledJoin(
            machineTwoID, joinID, tid1, tid2, k, scan11, scan12,
            std::vector<pos_vdb>{0}, std::vector<pos_vdb>{0},
            std::vector<pos_vdb>{2}, std::vector<pos_vdb>{1},
            &m2);
    KAnonymousBitonicSort * sort2 = new KAnonymousBitonicSort(4, k, join2, orderBys, sortOrder, &m2);


    std::mutex print_lock;

    std::thread first(
            FakeMultiPartyTestFixture::callKAnonymousOperatorAndVerifyOutput,
            &print_lock,
            sort1,
            machineOneID,
            &tuplesOfMachine0,
            expected_machine0_dummy
    );
    std::thread second(
            FakeMultiPartyTestFixture::callKAnonymousOperatorAndVerifyOutput,
            &print_lock,
            sort2,
            machineTwoID,
            &tuplesOfMachine1,
            expected_machine1_dummy
    );

    first.join();
    second.join();
    delete sort1;
    delete sort2;
    utilities::DebugFakeLocalDataMover::instance()->clear();
}
