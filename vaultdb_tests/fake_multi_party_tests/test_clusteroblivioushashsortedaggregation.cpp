#include <gtest/gtest.h>
#include <gmock/gmock.h>

#ifndef PRINT_CLUSTER_OPERATOR_OUTPUT
//#define PRINT_CLUSTER_OPERATOR_OUTPUT
#endif

#include "fakeMultiPartyTestFixture.h"
#include <in_sgx/plan/oboperators/ClusterObliviousBitonicMergeSort.h>
#include "in_sgx/plan/oboperators/ObliviousSeqScan.h"
#include "in_sgx/plan/obexpressions/obexpressions.h"
#include "debug/DebugFakeLocalDataMover.h"
#include <thread>
#include <in_sgx/plan/oboperators/ClusterObliviousHashSortedAggregation.h>
#include <in_sgx/plan/oboperators/BitonicSort.h>
#include <in_sgx/plan/oboperators/ObliviousSortedGroup.h>
#include <in_sgx/plan/kaoperators/KAnonymousSeqScan.h>
#include <in_sgx/plan/obexpressions/ObliviousExpressionFactory.h>
#include <debug/DebugInSGXController.h>
#include "in_sgx/plan/kaoperators/ClusterKAnonymousWindow.h"


using namespace plan::oboperators;
using namespace plan::kaoperators;
using namespace plan::obexpressions;

TEST_F(FakeMultiPartyTestFixture, ThreadedTwoPartyObliviousHashSortedAggregation) {
    ObliviousTupleList tuplesOfMachine0{
//            2	5
//            4	11
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(5),
            },

            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(11),
            },

    };
    ObliviousTupleList tuplesOfMachine1{
//            1	6
//            3	2
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(6),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousIntField(2),
            },
    };


    utilities::DebugInSGXController m1(machineOneID, 1);
    utilities::DebugInSGXController m2(machineTwoID, 1);

    std::vector<pos_vdb> groupBys{0};
    std::vector<ObliviousExpression> expressions1{
            ObliviousExpressionFactory::makeColumn(0, type::SchemaColumn{type::FieldDataType::Int, sizeof(int)}),
            ObliviousExpressionFactory::makeSum(1, type::SchemaColumn{type::FieldDataType::Int, sizeof(int)}),
    };

    std::vector<ObliviousExpression> expressions2{
            ObliviousExpressionFactory::makeColumn(0, type::SchemaColumn{type::FieldDataType::Int, sizeof(int)}),
            ObliviousExpressionFactory::makeSum(1, type::SchemaColumn{type::FieldDataType::Int, sizeof(int)}),
    };

    TableID aggID = 3;
    TransmitterID tid = 45;
    ObliviousSeqScan *scan02 = new ObliviousSeqScan(0, & oinput02);
    ClusterObliviousHashSortedAggregation *agg1 = new ClusterObliviousHashSortedAggregation(machineOneID, aggID, tid, scan02,
                                                                                            expressions1, groupBys, &m1);
    BitonicSort * sort1 = new BitonicSort(4, agg1, groupBys, std::vector<SortOrder> {SortOrder ::ASCEND});

    ObliviousSeqScan *scan12 = new ObliviousSeqScan(0, & oinput12);
    ClusterObliviousHashSortedAggregation *agg2 = new ClusterObliviousHashSortedAggregation(machineTwoID, aggID, tid, scan12,
                                                                                            expressions2, groupBys, &m2);
    BitonicSort * sort2 = new BitonicSort(4, agg2, groupBys, std::vector<SortOrder> {SortOrder ::ASCEND});


    std::mutex print_lock;

    std::thread first(
            FakeMultiPartyTestFixture::callObliviousOperatorAndVerifyOutput,
            &print_lock,
            sort1,
            machineOneID,
            &tuplesOfMachine0
    );
    std::thread second(
            FakeMultiPartyTestFixture::callObliviousOperatorAndVerifyOutput,
            &print_lock,
            sort2,
            machineTwoID,
            &tuplesOfMachine1
    );

    first.join();
    second.join();
    delete sort1;
    delete sort2;
    utilities::DebugFakeLocalDataMover::instance()->clear();
}
