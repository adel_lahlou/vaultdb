include_directories(${gtest_SOURCE_DIR}/include ${gtest_SOURCE_DIR})
include_directories(${gmock_SOURCE_DIR}/include ${gmock_SOURCE_DIR})


set(FAKEMPTESTS_SOURCE_FILES
        fakeMultiPartyTestFixture.h
        clusterOperatorTestFixture.h
        test_basicEndToEnd.cpp
        test_clusteroblivioushashjoin.cpp
        test_clusterobliviousbitonicmergesort.cpp
        test_clusteroblivioushashsortedaggregation.cpp
        test_clusterkanonymousaggregations.cpp
        test_clusterkanonymoushashjoin.cpp
        test_clusterkanonymoushashpooledjoin.cpp
        test_clusterkanonymousrepartition.cpp
        test_clusterkanonymousgenjoin.cpp
        test_clusterkanonymouscolumnsort.cpp
        fakeMultiPartyTestConstants.cpp
        test_clusterkanonymouslimit.cpp
        )

add_executable(runFakeMPTests ${FAKEMPTESTS_SOURCE_FILES})

target_link_libraries(runFakeMPTests gtest gtest_main)
target_link_libraries(runFakeMPTests vaultdb_plain vaultdb_in_sgx pq ssl crypto)