#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <debug/DebugInSGXController.h>
#include <in_sgx/serialization/ObliviousSerializer.h>
#include <in_sgx/plan/kaoperators/ClusterKAnonymousColumnSort.h>
#include <in_sgx/plan/kaoperators/KAnonymousSeqScan.h>
#include "fakeMultiPartyTestFixture.h"

using namespace plan::kaoperators;

TEST_F(FakeMultiPartyTestFixture, columnsort2MPerfectCondition32TuplesSimple) {
    /**
     * Given there are 1024 input tuples, and we can split the blocks perfectly
     * Suppose there are 2 machines, then s = 2;
     * Therefore minimum r >= 2 * s * s = 8
     * We will need 16 blocks
     * Each block is therefore 1024 / 16 = 64 tuples
     *
     * Here we'll be using the 32 tuple case
     */

    TableID inputTID = 5;
    RecordSchema singleColumn(inputTID, { {FieldDataType::Int, sizeof(int)} });
    ObliviousTupleTable input1(singleColumn, {
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(4)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(5)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(12)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(3)}),

            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(15)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(9)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(11)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(16)}),
    });

    ObliviousTupleTable input2(singleColumn, {
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(6)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(2)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(14)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(10)}),

            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(1)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(7)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(8)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(13)}),
    });

    ObliviousTupleTable output1(singleColumn, {
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(1)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(2)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(3)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(4)}),

            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(5)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(6)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(7)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(8)}),
    });

    ObliviousTupleTable output2(singleColumn, {
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(9)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(10)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(11)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(12)}),

            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(13)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(14)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(15)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(16)}),
    });

    utilities::DebugInSGXController m1(machineOneID, 1);
    utilities::DebugInSGXController m2(machineTwoID, 1);

    TableID scanID = 10;
    TableID sortID = 30;
    TransmitterID transmitterID = 40;

    auto scan1 = new KAnonymousSeqScan(scanID, k, &input1);
    ClusterKAnonymousColumnSort sort1(machineOneID, sortID, transmitterID, k, scan1, {0}, {SortOrder::ASCEND}, 1, &m1);

    auto scan2 = new KAnonymousSeqScan(scanID, k, &input2);
    ClusterKAnonymousColumnSort sort2(machineTwoID, sortID, transmitterID, k, scan2, {0}, {SortOrder::ASCEND}, 1, &m2);

    std::mutex printMutex;

    std::thread thread1(callKAnonymousOperatorAndVerifyOutputWithTable, &printMutex, &sort1, machineOneID, &output1,0);
    std::thread thread2(callKAnonymousOperatorAndVerifyOutputWithTable, &printMutex, &sort2, machineTwoID, &output2,0);

    thread1.join();
    thread2.join();
    utilities::DebugFakeLocalDataMover::instance()->clear();
}

TEST_F(FakeMultiPartyTestFixture, columnsort2MPerfectCondition32Tuples) {
    /**
     * Given there are 1024 input tuples, and we can split the blocks perfectly
     * Suppose there are 2 machines, then s = 2;
     * Therefore minimum r >= 2 * s * s = 8
     * We will need 16 blocks
     * Each block is therefore 1024 / 16 = 64 tuples
     *
     * Here we'll be using the 32 total tuples with 2 tuples per table
     */

    uninitializedRandomS3TupleCount32 = ObliviousSerializer::deserializeObliviousTupleTable(randomS3TuplesCount32, s3TuplesCount32Size);
    uninitializedSortedS3TupleCount32 = ObliviousSerializer::deserializeObliviousTupleTable(sortedS3TuplesCount32, s3TuplesCount32Size);

    utilities::DebugInSGXController m1(machineOneID, 1);
    utilities::DebugInSGXController m2(machineTwoID, 1);

    TableID scanID = 10;
    TableID sortID = 30;
    TransmitterID transmitterID = 40;

    ObliviousTupleTable m1Input(uninitializedRandomS3TupleCount32);
    ObliviousTupleTable m2Input(uninitializedRandomS3TupleCount32.getSchema(), {});
    ObliviousTupleTable::CutOffLastN(16, m1Input, m2Input);

    ASSERT_EQ(m1Input.size(), 16);
    ASSERT_EQ(m2Input.size(), 16);

    auto scan1 = new KAnonymousSeqScan(scanID, k, &m1Input);
    ClusterKAnonymousColumnSort sort1(machineOneID, sortID, transmitterID, k, scan1, {1, 0, 2},
                                      {SortOrder::ASCEND, SortOrder::ASCEND, SortOrder::ASCEND}, 2, &m1);

    auto scan2 = new KAnonymousSeqScan(scanID, k, &m2Input);
    ClusterKAnonymousColumnSort sort2(machineTwoID, sortID, transmitterID, k, scan2, {1, 0, 2},
                                      {SortOrder::ASCEND, SortOrder::ASCEND, SortOrder::ASCEND}, 2, &m2);

    ObliviousTupleTable m1ExpectedOutput(uninitializedSortedS3TupleCount32);
    ObliviousTupleTable m2ExpectedOutput(uninitializedSortedS3TupleCount32.getSchema(), {});
    ObliviousTupleTable::CutOffLastN(16, m1ExpectedOutput, m2ExpectedOutput);

    std::mutex printMutex;

    std::thread thread1(callKAnonymousOperatorAndVerifyOutputWithTable, &printMutex, &sort1, machineOneID, &m1ExpectedOutput,0);
    std::thread thread2(callKAnonymousOperatorAndVerifyOutputWithTable, &printMutex, &sort2, machineTwoID, &m2ExpectedOutput,0);

    thread1.join();
    thread2.join();
    utilities::DebugFakeLocalDataMover::instance()->clear();
}

TEST_F(FakeMultiPartyTestFixture, columnsort2MPerfectCondition32TuplesWithParsing) {
    /**
     * Given there are 1024 input tuples, and we can split the blocks perfectly
     * Suppose there are 2 machines, then s = 2;
     * Therefore minimum r >= 2 * s * s = 8
     * We will need 16 blocks
     * Each block is therefore 1024 / 16 = 64 tuples
     *
     * Here we'll be using the 32 total tuples with 2 tuples per table
     */

    uninitializedRandomS3TupleCount32 = ObliviousSerializer::deserializeObliviousTupleTable(randomS3TuplesCount32, s3TuplesCount32Size);
    uninitializedSortedS3TupleCount32 = ObliviousSerializer::deserializeObliviousTupleTable(sortedS3TuplesCount32, s3TuplesCount32Size);

    utilities::DebugInSGXController m1(machineOneID, 1);
    utilities::DebugInSGXController m2(machineTwoID, 1);

    TableID scanID = 10;
    TableID sortID = 30;
    TransmitterID transmitterID = 40;

    ObliviousTupleTable m1Input(uninitializedRandomS3TupleCount32);
    ObliviousTupleTable m2Input(uninitializedRandomS3TupleCount32.getSchema(), {});
    ObliviousTupleTable::CutOffLastN(16, m1Input, m2Input);

    ASSERT_EQ(m1Input.size(), 16);
    ASSERT_EQ(m2Input.size(), 16);

    m1.collectTablePointer(&m1Input);
    m2.collectTablePointer(&m2Input);

    auto scan1 = new KAnonymousSeqScan(scanID, k, &m1Input);
    ClusterKAnonymousColumnSort sort1(machineOneID, sortID, transmitterID, k, scan1, {1, 0, 2},
                                      {SortOrder::ASCEND, SortOrder::ASCEND, SortOrder::ASCEND}, 2, &m1);
    auto runPtr1 = m1.encodeKAnonymousOperatorAndParse(&sort1);

    auto scan2 = new KAnonymousSeqScan(scanID, k, &m2Input);
    ClusterKAnonymousColumnSort sort2(machineTwoID, sortID, transmitterID, k, scan2, {1, 0, 2},
                                      {SortOrder::ASCEND, SortOrder::ASCEND, SortOrder::ASCEND}, 2, &m2);
    auto runPtr2 = m2.encodeKAnonymousOperatorAndParse(&sort2);

    ObliviousTupleTable m1ExpectedOutput(uninitializedSortedS3TupleCount32);
    ObliviousTupleTable m2ExpectedOutput(uninitializedSortedS3TupleCount32.getSchema(), {});
    ObliviousTupleTable::CutOffLastN(16, m1ExpectedOutput, m2ExpectedOutput);

    std::mutex printMutex;

    std::thread thread1(callKAnonymousOperatorAndVerifyOutputWithTable, &printMutex, runPtr1, machineOneID, &m1ExpectedOutput,0);
    std::thread thread2(callKAnonymousOperatorAndVerifyOutputWithTable, &printMutex, runPtr2, machineTwoID, &m2ExpectedOutput,0);

    thread1.join();
    thread2.join();
    utilities::DebugFakeLocalDataMover::instance()->clear();
}

TEST_F(FakeMultiPartyTestFixture, columnsort4MPerfectCondition32TuplesSimple) {
    /**
     * Given there are 128 input tuples, and we can split the blocks perfectly
     * Suppose there are 2 machines, then s = 2;
     * Therefore minimum r >= 2 * s * s =` 8
     * We will need 16 blocks
     * Each block is therefore 1024 / 16 = 64 tuples
     *
     * Here we'll be using the 32 tuple case
     */

    TableID inputTID = 5;
    RecordSchema singleColumn(inputTID, { {FieldDataType::Int, sizeof(int)} });

    ObliviousTupleTable input1(singleColumn, {
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(28)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(111)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(58)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(20)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(63)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(118)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(5)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(120)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(74)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(43)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(42)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(23)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(64)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(80)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(71)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(114)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(16)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(116)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(13)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(37)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(83)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(93)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(48)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(109)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(72)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(70)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(44)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(29)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(85)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(110)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(22)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(65)}),
    });
    ObliviousTupleTable input2(singleColumn, {
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(26)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(21)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(4)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(92)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(79)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(105)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(47)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(36)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(126)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(90)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(124)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(104)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(97)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(40)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(17)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(122)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(46)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(75)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(18)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(81)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(2)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(84)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(38)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(25)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(66)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(121)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(8)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(60)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(106)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(123)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(94)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(45)}),
    });

    ObliviousTupleTable input3(singleColumn, {
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(117)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(11)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(128)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(31)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(100)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(33)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(57)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(102)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(89)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(87)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(67)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(86)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(103)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(88)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(125)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(68)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(115)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(91)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(10)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(30)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(82)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(101)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(113)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(112)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(39)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(41)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(19)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(119)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(24)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(9)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(107)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(27)}),
    });
    ObliviousTupleTable input4(singleColumn, {
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(15)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(51)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(32)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(52)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(56)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(99)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(59)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(69)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(7)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(77)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(34)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(62)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(14)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(12)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(96)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(108)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(49)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(6)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(54)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(61)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(76)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(53)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(95)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(55)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(1)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(78)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(98)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(127)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(50)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(35)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(3)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(73)}),
    });

    ObliviousTupleTable output1(singleColumn, {
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(1)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(2)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(3)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(4)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(5)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(6)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(7)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(8)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(9)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(10)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(11)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(12)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(13)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(14)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(15)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(16)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(17)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(18)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(19)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(20)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(21)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(22)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(23)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(24)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(25)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(26)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(27)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(28)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(29)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(30)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(31)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(32)}),
    });
    ObliviousTupleTable output2(singleColumn, {
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(33)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(34)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(35)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(36)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(37)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(38)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(39)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(40)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(41)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(42)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(43)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(44)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(45)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(46)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(47)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(48)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(49)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(50)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(51)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(52)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(53)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(54)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(55)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(56)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(57)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(58)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(59)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(60)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(61)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(62)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(63)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(64)}),
    });

    ObliviousTupleTable output3(singleColumn, {
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(65)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(66)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(67)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(68)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(69)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(70)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(71)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(72)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(73)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(74)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(75)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(76)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(77)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(78)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(79)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(80)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(81)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(82)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(83)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(84)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(85)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(86)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(87)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(88)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(89)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(90)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(91)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(92)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(93)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(94)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(95)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(96)}),
    });
    ObliviousTupleTable output4(singleColumn, {
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(97)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(98)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(99)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(100)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(101)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(102)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(103)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(104)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(105)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(106)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(107)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(108)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(109)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(110)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(111)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(112)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(113)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(114)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(115)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(116)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(117)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(118)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(119)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(120)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(121)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(122)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(123)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(124)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(125)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(126)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(127)}),
            ObliviousTuple(false, {ObliviousField::makeObliviousIntField(128)}),
    });

    utilities::DebugInSGXController m1(machineOneID, 1);
    utilities::DebugInSGXController m2(machineTwoID, 1);
    utilities::DebugInSGXController m3(machineThreeID, 1);
    utilities::DebugInSGXController m4(machineFourID, 1);

    TableID scanID = 10;
    TableID sortID = 30;
    TransmitterID transmitterID = 40;

    auto scan1 = new KAnonymousSeqScan(scanID, k, &input1);
    ClusterKAnonymousColumnSort sort1(machineOneID, sortID, transmitterID, k, scan1, {0}, {SortOrder::ASCEND}, 1, &m1);
    auto scan2 = new KAnonymousSeqScan(scanID, k, &input2);
    ClusterKAnonymousColumnSort sort2(machineTwoID, sortID, transmitterID, k, scan2, {0}, {SortOrder::ASCEND}, 1, &m2);
    auto scan3 = new KAnonymousSeqScan(scanID, k, &input3);
    ClusterKAnonymousColumnSort sort3(machineThreeID, sortID, transmitterID, k, scan3, {0}, {SortOrder::ASCEND}, 1, &m3);
    auto scan4 = new KAnonymousSeqScan(scanID, k, &input4);
    ClusterKAnonymousColumnSort sort4(machineFourID, sortID, transmitterID, k, scan4, {0}, {SortOrder::ASCEND}, 1, &m4);

    std::mutex printMutex;

    std::thread thread1(callKAnonymousOperatorAndVerifyOutputWithTable, &printMutex, &sort1, machineOneID, &output1,0);
    std::thread thread2(callKAnonymousOperatorAndVerifyOutputWithTable, &printMutex, &sort2, machineTwoID, &output2,0);
    std::thread thread3(callKAnonymousOperatorAndVerifyOutputWithTable, &printMutex, &sort3, machineThreeID, &output3,0);
    std::thread thread4(callKAnonymousOperatorAndVerifyOutputWithTable, &printMutex, &sort4, machineFourID, &output4,0);

    thread1.join();
    thread2.join();
    thread3.join();
    thread4.join();
    utilities::DebugFakeLocalDataMover::instance()->clear();
}

TEST_F(FakeMultiPartyTestFixture, columnsort4MPerfectCondition1024Tuples) {
    /**
     * Given there are 1024 input tuples, and we can split the blocks perfectly
     * Suppose there are 4 machines, then s = 4;
     * Therefore minimum r >= 2 * s * s =` 32
     * We will need 16 blocks
     * Each block is therefore 1024 / 32 = 32 tuples
     */
    utilities::DebugInSGXController m1(machineOneID, 1);
    utilities::DebugInSGXController m2(machineTwoID, 1);
    utilities::DebugInSGXController m3(machineThreeID, 1);
    utilities::DebugInSGXController m4(machineFourID, 1);

    uninitializedRandomS3TupleCount1024 = ObliviousSerializer::deserializeObliviousTupleTable(randomS3TuplesCount1024, s3TuplesCount1024Size);
    uninitializedSortedS3TupleCount1024 = ObliviousSerializer::deserializeObliviousTupleTable(sortedS3TuplesCount1024, s3TuplesCount1024Size);

    TableID scanID = 10;
    TableID sortID = 30;
    TransmitterID transmitterID = 40;

    ObliviousTupleTable m1Input(uninitializedRandomS3TupleCount1024);
    ObliviousTupleTable m2Input(uninitializedRandomS3TupleCount1024.getSchema(), {});
    ObliviousTupleTable m3Input(uninitializedRandomS3TupleCount1024.getSchema(), {});
    ObliviousTupleTable m4Input(uninitializedRandomS3TupleCount1024.getSchema(), {});
    ObliviousTupleTable::CutOffLastN(256, m1Input, m4Input);
    ObliviousTupleTable::CutOffLastN(256, m1Input, m3Input);
    ObliviousTupleTable::CutOffLastN(256, m1Input, m2Input);

    auto scan1 = new KAnonymousSeqScan(scanID, k, &m1Input);
    auto scan2 = new KAnonymousSeqScan(scanID, k, &m2Input);
    auto scan3 = new KAnonymousSeqScan(scanID, k, &m3Input);
    auto scan4 = new KAnonymousSeqScan(scanID, k, &m4Input);
    ClusterKAnonymousColumnSort sort1(machineOneID, sortID, transmitterID, k, scan1, {1, 0, 2},
                                      {SortOrder::ASCEND, SortOrder::ASCEND, SortOrder::ASCEND}, 8, &m1);
    ClusterKAnonymousColumnSort sort2(machineTwoID, sortID, transmitterID, k, scan2, {1, 0, 2},
                                      {SortOrder::ASCEND, SortOrder::ASCEND, SortOrder::ASCEND}, 8, &m2);
    ClusterKAnonymousColumnSort sort3(machineThreeID, sortID, transmitterID, k, scan3, {1, 0, 2},
                                      {SortOrder::ASCEND, SortOrder::ASCEND, SortOrder::ASCEND}, 8, &m3);
    ClusterKAnonymousColumnSort sort4(machineFourID, sortID, transmitterID, k, scan4, {1, 0, 2},
                                      {SortOrder::ASCEND, SortOrder::ASCEND, SortOrder::ASCEND}, 8, &m4);

    ObliviousTupleTable m1Output(uninitializedSortedS3TupleCount1024);
    ObliviousTupleTable m2Output(uninitializedSortedS3TupleCount1024.getSchema(), {});
    ObliviousTupleTable m3Output(uninitializedSortedS3TupleCount1024.getSchema(), {});
    ObliviousTupleTable m4Output(uninitializedSortedS3TupleCount1024.getSchema(), {});
    ObliviousTupleTable::CutOffLastN(256, m1Output, m4Output);
    ObliviousTupleTable::CutOffLastN(256, m1Output, m3Output);
    ObliviousTupleTable::CutOffLastN(256, m1Output, m2Output);

    std::mutex printMutex;

    std::thread thread1(callKAnonymousOperatorAndVerifyOutputWithTable, &printMutex, &sort1, machineOneID, &m1Output,0);
    std::thread thread2(callKAnonymousOperatorAndVerifyOutputWithTable, &printMutex, &sort2, machineTwoID, &m2Output,0);
    std::thread thread3(callKAnonymousOperatorAndVerifyOutputWithTable, &printMutex, &sort3, machineThreeID, &m3Output,0);
    std::thread thread4(callKAnonymousOperatorAndVerifyOutputWithTable, &printMutex, &sort4, machineFourID, &m4Output,0);

    thread1.join();
    thread2.join();
    thread3.join();
    thread4.join();
    utilities::DebugFakeLocalDataMover::instance()->clear();
}

TEST_F(FakeMultiPartyTestFixture, columnsort4MSkewed1024Tuples) {
    /**
     * Given there are 1024 input tuples, and we can split them into 512, 256, 129, 127 tuples
     * Suppose there are 4 machines, then s = 4;
     * Therefore minimum r >= 2 * s * s = 32
     * Suppose we ask that each block is 8 tuples long, then each machine starts with 64, 32, 17, 16 partitions
     * We therefore pad them to 64 each, using 0, 256, 376 + 8, 384, totaling 1024 dummy tuples
     * Thus in the final result, they each has 512, 512, 0, 0 dummy tuples
     */
    utilities::DebugInSGXController m1(machineOneID, 1);
    utilities::DebugInSGXController m2(machineTwoID, 1);
    utilities::DebugInSGXController m3(machineThreeID, 1);
    utilities::DebugInSGXController m4(machineFourID, 1);

    uninitializedRandomS3TupleCount1024 = ObliviousSerializer::deserializeObliviousTupleTable(randomS3TuplesCount1024, s3TuplesCount1024Size);
    uninitializedSortedS3TupleCount1024 = ObliviousSerializer::deserializeObliviousTupleTable(sortedS3TuplesCount1024, s3TuplesCount1024Size);

    TableID scanID = 10;
    TableID sortID = 30;
    TransmitterID transmitterID = 40;

    ObliviousTupleTable m1Input(uninitializedRandomS3TupleCount1024);
    ObliviousTupleTable m2Input(uninitializedRandomS3TupleCount1024.getSchema(), {});
    ObliviousTupleTable m3Input(uninitializedRandomS3TupleCount1024.getSchema(), {});
    ObliviousTupleTable m4Input(uninitializedRandomS3TupleCount1024.getSchema(), {});
    ObliviousTupleTable::CutOffLastN(127, m1Input, m4Input);
    ObliviousTupleTable::CutOffLastN(129, m1Input, m3Input);
    ObliviousTupleTable::CutOffLastN(256, m1Input, m2Input);

    auto scan1 = new KAnonymousSeqScan(scanID, k, &m1Input);
    auto scan2 = new KAnonymousSeqScan(scanID, k, &m2Input);
    auto scan3 = new KAnonymousSeqScan(scanID, k, &m3Input);
    auto scan4 = new KAnonymousSeqScan(scanID, k, &m4Input);
    ClusterKAnonymousColumnSort sort1(machineOneID, sortID, transmitterID, k, scan1, {1, 0, 2},
                                      {SortOrder::ASCEND, SortOrder::ASCEND, SortOrder::ASCEND}, 8, &m1);
    ClusterKAnonymousColumnSort sort2(machineTwoID, sortID, transmitterID, k, scan2, {1, 0, 2},
                                      {SortOrder::ASCEND, SortOrder::ASCEND, SortOrder::ASCEND}, 8, &m2);
    ClusterKAnonymousColumnSort sort3(machineThreeID, sortID, transmitterID, k, scan3, {1, 0, 2},
                                      {SortOrder::ASCEND, SortOrder::ASCEND, SortOrder::ASCEND}, 8, &m3);
    ClusterKAnonymousColumnSort sort4(machineFourID, sortID, transmitterID, k, scan4, {1, 0, 2},
                                      {SortOrder::ASCEND, SortOrder::ASCEND, SortOrder::ASCEND}, 8, &m4);

    ObliviousTupleTable m1Output(uninitializedSortedS3TupleCount1024.getSchema(), {});
    ObliviousTupleTable m2Output(uninitializedSortedS3TupleCount1024.getSchema(), {});
    ObliviousTupleTable m3Output(uninitializedSortedS3TupleCount1024);
    ObliviousTupleTable m4Output(uninitializedSortedS3TupleCount1024.getSchema(), {});
    ObliviousTupleTable::CutOffLastN(512, m3Output, m4Output);

    std::mutex printMutex;

    std::thread thread1(callKAnonymousOperatorAndVerifyOutputWithTable, &printMutex, &sort1, machineOneID, &m1Output,512);
    std::thread thread2(callKAnonymousOperatorAndVerifyOutputWithTable, &printMutex, &sort2, machineTwoID, &m2Output,512);
    std::thread thread3(callKAnonymousOperatorAndVerifyOutputWithTable, &printMutex, &sort3, machineThreeID, &m3Output,0);
    std::thread thread4(callKAnonymousOperatorAndVerifyOutputWithTable, &printMutex, &sort4, machineFourID, &m4Output,0);

    thread1.join();
    thread2.join();
    thread3.join();
    thread4.join();
    utilities::DebugFakeLocalDataMover::instance()->clear();
}