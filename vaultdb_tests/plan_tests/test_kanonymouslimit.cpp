#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <in_sgx/plan/kaoperators/KAnonymousSeqScan.h>
#include <in_sgx/plan/kaoperators/KAnonymousLimit.h>

#include "kAnonymousOperatorTestFixture.h"

//#define KANO_LIMIT_PRINT_TUPLES

using namespace plan::kaoperators;
using namespace plan::obexpressions;

TEST_F(KAnonymousOperatorTestFixture, KAnonymousLimitUnderFill) {

    size_vdb k = 2;
    size_vdb tupleLimit = 5;
    ObliviousTupleList expectedTuples {
//            R - 4,3,"rogue"
//            R - 1,3,"peace3"
//            R - 3,1,"tornado"
//            R - 3,2,"giant"
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousFixcharField("rogue", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousFixcharField("peace3", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("tornado", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("giant", vc_size),
            }),
    };


    KAnonymousSeqScan * scan = new KAnonymousSeqScan(0, k, & oinput4);
    KAnonymousLimit limit(1, k, tupleLimit, {scan});

    ObliviousTuple out;
#ifdef KANO_LIMIT_PRINT_TUPLES
    std::cout << std::endl;
#endif
    for (ObliviousTuple t : expectedTuples) {
        limit.next();
        limit.getCurrentTuple(out);
#ifdef KANO_LIMIT_PRINT_TUPLES
            std::cout << "Real / actual: " << t << " / " << out << std::endl;
#endif
        ASSERT_TRUE(oblivious_tuples_equal(out, t));
    }
    ASSERT_EQ(limit.next(), OperatorStatus::NoMoreTuples);
    limit.reset();
}

TEST_F(KAnonymousOperatorTestFixture, KAnonymousLimitOverFill) {

    size_vdb k = 2;
    size_vdb tupleLimit = 5;
    ObliviousTupleList expectedTuples {
//            1,3,"peace"
//            4,1,"fire pit"
//            4,1,"rogue"
//            1,2,"lasting"
//            1,3,"peace"
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousFixcharField("peace", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("fire pit", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("rogue", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("lasting", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousFixcharField("peace", vc_size),
            }),
    };


    KAnonymousSeqScan * scan = new KAnonymousSeqScan(0, k, & oinput3);
    KAnonymousLimit limit(1, k, tupleLimit, {scan});

    ObliviousTuple out;
#ifdef KANO_LIMIT_PRINT_TUPLES
    std::cout << std::endl;
#endif
    for (ObliviousTuple t : expectedTuples) {
        limit.next();
        limit.getCurrentTuple(out);
#ifdef KANO_LIMIT_PRINT_TUPLES
        std::cout << "Real / actual: " << t << " / " << out << std::endl;
#endif
        ASSERT_TRUE(oblivious_tuples_equal(out, t));
    }
    ASSERT_EQ(limit.next(), OperatorStatus::NoMoreTuples);
    limit.reset();
}

TEST_F(KAnonymousOperatorTestFixture, KAnonymousLimitEmptyInput) {

    size_vdb k = 2;
    size_vdb tupleLimit = 5;
    ObliviousTupleList expectedTuples {
    };

    KAnonymousSeqScan * scan = new KAnonymousSeqScan(0, k, & oinputEmpty);
    KAnonymousLimit limit(1, k, tupleLimit, {scan});

    ObliviousTuple out;
#ifdef KANO_LIMIT_PRINT_TUPLES
    std::cout << std::endl;
#endif
    for (ObliviousTuple t : expectedTuples) {
        limit.next();
        limit.getCurrentTuple(out);
#ifdef KANO_LIMIT_PRINT_TUPLES
        std::cout << "Real / actual: " << t << " / " << out << std::endl;
#endif
        ASSERT_TRUE(oblivious_tuples_equal(out, t));
    }
    ASSERT_EQ(limit.next(), OperatorStatus::NoMoreTuples);
    limit.reset();
}