#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include "obliviousOperatorTestFixture.h"
#include "in_sgx/plan/obexpressions/ObliviousExpressionFactory.h"
#include <in_sgx/plan/oboperators/ObliviousOperator.h>
#include <in_sgx/plan/oboperators/ObliviousSeqScan.h>
#include <in_sgx/plan/oboperators/ObliviousFilter.h>

using namespace plan::oboperators;
using namespace plan::obexpressions;

TEST_F(ObliviousOperatorTestFixture, ObliviousFilterConstruction)
{
    ObliviousExpression filterPred =
            ObliviousExpressionFactory::makeEq(
                    ObliviousExpressionFactory::makeAdd(
                            ObliviousExpressionFactory::makeColumn(0, type::SchemaColumn{type::FieldDataType::Int, sizeof(int)}),
                            ObliviousExpressionFactory::makeLiteral(ObliviousField::makeObliviousIntField(1), sizeof(int))),
                    ObliviousExpressionFactory::makeLiteral(ObliviousField::makeObliviousIntField(5), sizeof(int)));
    ObliviousSeqScan * scan = new ObliviousSeqScan(0, & oinput1);
    ObliviousFilter filter(2, scan, filterPred);

    ASSERT_EQ(filter.getStatus(), OperatorStatus::NotInitialized);
    ASSERT_EQ(filter.getChildren().size(), 1);
    ASSERT_TRUE(recordschema_equal(filter.getSchema(), s1));
    ASSERT_TRUE(recordschema_equal(filter.getChildSchema(0), s1));
    ASSERT_EQ(scan->getParent(), &filter);
    ASSERT_EQ(filter.getParent(), nullptr);
    ASSERT_FALSE(filter.isBlocking());
}

TEST_F(ObliviousOperatorTestFixture, ObliviousFilterNextAndGet)
{
    ObliviousExpression filterPred =
            ObliviousExpressionFactory::makeEq(
                    ObliviousExpressionFactory::makeAdd(
                            ObliviousExpressionFactory::makeColumn(0, type::SchemaColumn{type::FieldDataType::Int, sizeof(int)}),
                            ObliviousExpressionFactory::makeLiteral(ObliviousField::makeObliviousIntField(1), sizeof(int))),
                    ObliviousExpressionFactory::makeLiteral(ObliviousField::makeObliviousIntField(5), sizeof(int)));
    ObliviousSeqScan * scan = new ObliviousSeqScan(0, & oinput1);
    ObliviousFilter filter(2, scan, filterPred);

    ASSERT_EQ(filter.getStatus(), OperatorStatus::NotInitialized);

    ObliviousTuple out;

    ASSERT_EQ(filter.next(), OperatorStatus::Ok);
    ASSERT_EQ(filter.getCurrentTuple(out), OperatorStatus::Ok);
    ASSERT_TRUE(oblivious_tuples_equal(otuples[0], out));
    ASSERT_TRUE(out.isDummy());

    ASSERT_EQ(filter.next(), OperatorStatus::Ok);
    ASSERT_EQ(filter.getCurrentTuple(out), OperatorStatus::Ok);
    ASSERT_TRUE(oblivious_tuples_equal(otuples[1], out));
    ASSERT_FALSE(out.isDummy());

    ASSERT_EQ(filter.next(), OperatorStatus::Ok);
    ASSERT_EQ(filter.getCurrentTuple(out), OperatorStatus::Ok);
    ASSERT_TRUE(oblivious_tuples_equal(otuples[2], out));
    ASSERT_TRUE(out.isDummy());

    ASSERT_EQ(filter.next(), OperatorStatus::Ok);
    ASSERT_EQ(filter.getCurrentTuple(out), OperatorStatus::Ok);
    ASSERT_TRUE(oblivious_tuples_equal(otuples[3], out));
    ASSERT_FALSE(out.isDummy());

    ASSERT_EQ(filter.next(), OperatorStatus::NoMoreTuples);
    ASSERT_EQ(filter.getCurrentTuple(out), OperatorStatus::NoMoreTuples);
    ASSERT_TRUE(oblivious_tuples_equal(otuples[3], out));
    ASSERT_FALSE(out.isDummy());

    ASSERT_EQ(filter.next(), OperatorStatus::NoMoreTuples);
    ASSERT_EQ(filter.getCurrentTuple(out), OperatorStatus::NoMoreTuples);
    ASSERT_TRUE(oblivious_tuples_equal(otuples[3], out));
    ASSERT_FALSE(out.isDummy());

    filter.reset();

    ASSERT_EQ(filter.next(), OperatorStatus::Ok);
    ASSERT_EQ(filter.getCurrentTuple(out), OperatorStatus::Ok);
    ASSERT_TRUE(oblivious_tuples_equal(otuples[0], out));
    ASSERT_TRUE(out.isDummy());

    ASSERT_EQ(filter.next(), OperatorStatus::Ok);
    ASSERT_EQ(filter.getCurrentTuple(out), OperatorStatus::Ok);
    ASSERT_TRUE(oblivious_tuples_equal(otuples[1], out));
    ASSERT_FALSE(out.isDummy());

    ASSERT_EQ(filter.next(), OperatorStatus::Ok);
    ASSERT_EQ(filter.getCurrentTuple(out), OperatorStatus::Ok);
    ASSERT_TRUE(oblivious_tuples_equal(otuples[2], out));
    ASSERT_TRUE(out.isDummy());

    ASSERT_EQ(filter.next(), OperatorStatus::Ok);
    ASSERT_EQ(filter.getCurrentTuple(out), OperatorStatus::Ok);
    ASSERT_TRUE(oblivious_tuples_equal(otuples[3], out));
    ASSERT_FALSE(out.isDummy());

    ASSERT_EQ(filter.next(), OperatorStatus::NoMoreTuples);
    ASSERT_EQ(filter.getCurrentTuple(out), OperatorStatus::NoMoreTuples);
    ASSERT_TRUE(oblivious_tuples_equal(otuples[3], out));
    ASSERT_FALSE(out.isDummy());

    ASSERT_EQ(filter.next(), OperatorStatus::NoMoreTuples);
    ASSERT_EQ(filter.getCurrentTuple(out), OperatorStatus::NoMoreTuples);
    ASSERT_TRUE(oblivious_tuples_equal(otuples[3], out));
    ASSERT_FALSE(out.isDummy());
}
