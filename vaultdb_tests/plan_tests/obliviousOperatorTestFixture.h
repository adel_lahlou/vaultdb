#ifndef VAULTDB_OBLIVIOUSOPERATORTESTFIXTURE_H
#define VAULTDB_OBLIVIOUSOPERATORTESTFIXTURE_H

#include <gtest/gtest.h>
#include <in_sgx/obdata/ObliviousTuple.h>
#include <in_sgx/obdata/ObliviousTupleTable.h>

#include <plaintext/data/Tuple.h>
#include <plaintext/data/TupleTable.h>

#include <shared/type/RecordSchema.h>

using namespace db::data;
using namespace db::data::field;
using namespace db::obdata;
using namespace db::obdata::obfield;
using namespace type;

bool static recordschema_equal(const RecordSchema& s1, const RecordSchema& s2)
{
    if(s1.size() != s2.size())
        return false;

    for(int i = 0; i < s1.size(); ++i){
        if(s1[i].type != s2[i].type || s1[i].size != s2[i].size)
            return false;
    }

    return true;
}


bool static oblivious_tuples_equal(const ObliviousTuple& t1, const ObliviousTuple& t2)
{
    if(t1.size() != t2.size())
        return false;

    for(int i = 0; i < t1.size(); ++i){
        if(!(t1[i] == t2[i]))
            return false;
    }

    return true;
}

class ObliviousOperatorTestFixture : public ::testing::Test {
protected:

    ObliviousOperatorTestFixture()
            : oinput1(s1, otuples),
              oinput2(s1, otuples2)
    {}

    ~ObliviousOperatorTestFixture() {}

    size_vdb vc_size = 10;

    type::RecordSchema s1{
            (TableID) 3, {
                    {type::FieldDataType::Int, sizeof(int)},
                    {type::FieldDataType::Int, sizeof(int)},
                    {type::FieldDataType::Int, sizeof(int)},
                    {type::FieldDataType::Fixchar, vc_size},
                    {type::FieldDataType::Fixchar, vc_size},
            }
    };

    ObliviousTupleList otuples {
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousFixcharField("hello     ", vc_size),
                    ObliviousField::makeObliviousFixcharField("world     ", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousIntField(6),
                    ObliviousField::makeObliviousFixcharField("foo       ", vc_size),
                    ObliviousField::makeObliviousFixcharField("bar       ", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousFixcharField("bigger    ", vc_size),
                    ObliviousField::makeObliviousFixcharField("better    ", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(6),
                    ObliviousField::makeObliviousFixcharField("secure    ", vc_size),
                    ObliviousField::makeObliviousFixcharField("execution ", vc_size),
            },
    };

    ObliviousTupleList otuples2 {
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousFixcharField("hello     ", vc_size),
                    ObliviousField::makeObliviousFixcharField("world     ", vc_size),
            },

            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousFixcharField("hello     ", vc_size),
                    ObliviousField::makeObliviousFixcharField("bar       ", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousIntField(6),
                    ObliviousField::makeObliviousFixcharField("foo       ", vc_size),
                    ObliviousField::makeObliviousFixcharField("bar       ", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousFixcharField("bigger    ", vc_size),
                    ObliviousField::makeObliviousFixcharField("better    ", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(6),
                    ObliviousField::makeObliviousFixcharField("secure    ", vc_size),
                    ObliviousField::makeObliviousFixcharField("execution ", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousFixcharField("hello     ", vc_size),
                    ObliviousField::makeObliviousFixcharField("world     ", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(6),
                    ObliviousField::makeObliviousFixcharField("secure    ", vc_size),
                    ObliviousField::makeObliviousFixcharField("execution ", vc_size),
            },
    };


    ObliviousTupleTable oinput1;
    ObliviousTupleTable oinput2;
};

#endif //VAULTDB_OBLIVIOUSOPERATORTESTFIXTURE_H
