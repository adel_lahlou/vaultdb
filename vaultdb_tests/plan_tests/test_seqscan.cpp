#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include "operatorTestFixture.h"
#include <plaintext/plan/operators/Operator.h>
#include <plaintext/plan/operators/SeqScan.h>

using namespace plan::operators;
using namespace plan::expressions;

TEST_F(OperatorTestFixture, SeqScanConstruction)
{
    SeqScan scan(1, s1, nullptr, input1);

    ASSERT_EQ(scan.getStatus(), OperatorStatus::NotInitialized);
    ASSERT_EQ(scan.getChildren().size(), 0);
    ASSERT_TRUE(recordschema_equal(scan.getSchema(), s1));
    ASSERT_EQ(scan.getParent(), nullptr);
    ASSERT_FALSE(scan.isBlocking());
}

TEST_F(OperatorTestFixture, SeqScanNextAndGet)
{
    SeqScan scan(1, s1, nullptr, input1);

    ASSERT_EQ(scan.getStatus(), OperatorStatus::NotInitialized);

    Tuple out;

    ASSERT_EQ(scan.next(), OperatorStatus::Ok);
    ASSERT_EQ(scan.getCurrentTuple(out), OperatorStatus::Ok);
    ASSERT_TRUE(tuples_equal(tuples[0], out));

    ASSERT_EQ(scan.next(), OperatorStatus::Ok);
    ASSERT_EQ(scan.getCurrentTuple(out), OperatorStatus::Ok);
    ASSERT_TRUE(tuples_equal(tuples[1], out));

    ASSERT_EQ(scan.next(), OperatorStatus::Ok);
    ASSERT_EQ(scan.getCurrentTuple(out), OperatorStatus::Ok);
    ASSERT_TRUE(tuples_equal(tuples[2], out));

    ASSERT_EQ(scan.next(), OperatorStatus::Ok);
    ASSERT_EQ(scan.getCurrentTuple(out), OperatorStatus::Ok);
    ASSERT_TRUE(tuples_equal(tuples[3], out));

    ASSERT_EQ(scan.next(), OperatorStatus::NoMoreTuples);
    ASSERT_EQ(scan.getCurrentTuple(out), OperatorStatus::NoMoreTuples);
    ASSERT_TRUE(tuples_equal(tuples[3], out));

    ASSERT_EQ(scan.next(), OperatorStatus::NoMoreTuples);
    ASSERT_EQ(scan.getCurrentTuple(out), OperatorStatus::NoMoreTuples);
    ASSERT_TRUE(tuples_equal(tuples[3], out));

    scan.reset();

    ASSERT_EQ(scan.getStatus(), OperatorStatus::NotInitialized);

    ASSERT_EQ(scan.next(), OperatorStatus::Ok);
    ASSERT_EQ(scan.getCurrentTuple(out), OperatorStatus::Ok);
    ASSERT_TRUE(tuples_equal(tuples[0], out));

    ASSERT_EQ(scan.next(), OperatorStatus::Ok);
    ASSERT_EQ(scan.getCurrentTuple(out), OperatorStatus::Ok);
    ASSERT_TRUE(tuples_equal(tuples[1], out));

    ASSERT_EQ(scan.next(), OperatorStatus::Ok);
    ASSERT_EQ(scan.getCurrentTuple(out), OperatorStatus::Ok);
    ASSERT_TRUE(tuples_equal(tuples[2], out));

    ASSERT_EQ(scan.next(), OperatorStatus::Ok);
    ASSERT_EQ(scan.getCurrentTuple(out), OperatorStatus::Ok);
    ASSERT_TRUE(tuples_equal(tuples[3], out));

    ASSERT_EQ(scan.next(), OperatorStatus::NoMoreTuples);
    ASSERT_EQ(scan.getCurrentTuple(out), OperatorStatus::NoMoreTuples);
    ASSERT_TRUE(tuples_equal(tuples[3], out));

    ASSERT_EQ(scan.next(), OperatorStatus::NoMoreTuples);
    ASSERT_EQ(scan.getCurrentTuple(out), OperatorStatus::NoMoreTuples);
    ASSERT_TRUE(tuples_equal(tuples[3], out));
}


