#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include "operatorTestFixture.h"
#include <plaintext/plan/operators/Operator.h>
#include <plaintext/plan/operators/SeqScan.h>
#include <plaintext/plan/operators/Filter.h>
#include <plaintext/plan/operators/SortedGroup.h>

using namespace plan::operators;
using namespace plan::expressions;

TEST_F(OperatorTestFixture, SortedGroupConstruction)
{
    RecordSchema expectedOutSchema{s1.getTableID(), {
        {FieldDataType::Int, sizeof(int)},
        {FieldDataType::Varchar, 20},
        {FieldDataType::Int, sizeof(int)},
        {FieldDataType::Int, sizeof(int)},
        {FieldDataType::Int, sizeof(int)},
        {FieldDataType::Int, sizeof(int)},
    }};

    ExpressionList aggs = {
            Expression::makeLiteral(tuples2[0][0]),
            Expression::makeColumn(4),
            Expression::makeSum(0),
            Expression::makeSum(1),
            Expression::makeSum(2),
            Expression::makeCount(4),
    };
    std::vector<size_vdb> groupBys = {0, 4};
    SeqScan * scan = new SeqScan(1, s1, nullptr, input1);
    SortedGroup sgroup(2, scan, aggs, groupBys);

    ASSERT_EQ(sgroup.getStatus(), OperatorStatus::NotInitialized);
    ASSERT_EQ(sgroup.getChildren().size(), 1);
    ASSERT_TRUE(recordschema_equal(sgroup.getSchema(), expectedOutSchema));
    ASSERT_TRUE(recordschema_equal(sgroup.getChildSchema(0), s1));
    ASSERT_EQ(scan->getParent(), sgroup.getChild(0));
    ASSERT_EQ(sgroup.getParent(), nullptr);
    ASSERT_FALSE(sgroup.isBlocking());
}

TEST_F(OperatorTestFixture, SortedGroupNextAndGet) {
    ExpressionList aggs = {
            Expression::makeLiteral(tuples2[0][0]),
            Expression::makeColumn(4),
            Expression::makeSum(0),
            Expression::makeSum(1),
            Expression::makeSum(2),
            Expression::makeCount(4),
    };
    std::vector<size_vdb> groupBys = {0, 4};
    SeqScan *scan = new SeqScan(1, s1, nullptr, input2);
    SortedGroup sgroup(2, scan, aggs, groupBys);

    Tuple out;
    for (int i = 0; i < 3; ++i) {

        ASSERT_EQ(sgroup.getStatus(), OperatorStatus::NotInitialized);

        ASSERT_EQ(sgroup.next(), OperatorStatus::Ok);
        ASSERT_EQ(sgroup.getCurrentTuple(out), OperatorStatus::Ok);
        ASSERT_TRUE(tuples_equal(out, Tuple {
                tuples2[0][0],
                tuples2[3][4],
                Field::makeIntField(1),
                Field::makeIntField(2),
                Field::makeIntField(4),
                Field::makeIntField(1),
        }));

        ASSERT_EQ(sgroup.next(), OperatorStatus::Ok);
        ASSERT_EQ(sgroup.getCurrentTuple(out), OperatorStatus::Ok);
        ASSERT_TRUE(tuples_equal(out, Tuple {
                tuples2[0][0],
                tuples2[0][4],
                Field::makeIntField(2),
                Field::makeIntField(4),
                Field::makeIntField(6),
                Field::makeIntField(2),
        }));


        ASSERT_EQ(sgroup.next(), OperatorStatus::Ok);
        ASSERT_EQ(sgroup.getCurrentTuple(out), OperatorStatus::Ok);
        ASSERT_TRUE(tuples_equal(out, Tuple {
                tuples2[0][0],
                tuples2[1][4],
                Field::makeIntField(8),
                Field::makeIntField(7),
                Field::makeIntField(9),
                Field::makeIntField(2),
        }));

        ASSERT_EQ(sgroup.next(), OperatorStatus::Ok);
        ASSERT_EQ(sgroup.getCurrentTuple(out), OperatorStatus::Ok);
        ASSERT_TRUE(tuples_equal(out, Tuple {
                tuples2[0][0],
                tuples2[4][4],
                Field::makeIntField(8),
                Field::makeIntField(4),
                Field::makeIntField(12),
                Field::makeIntField(2),
        }));

        ASSERT_EQ(sgroup.next(), OperatorStatus::NoMoreTuples);
        ASSERT_EQ(sgroup.getCurrentTuple(out), OperatorStatus::NoMoreTuples);
        ASSERT_TRUE(tuples_equal(out, Tuple {
                tuples2[0][0],
                tuples2[4][4],
                Field::makeIntField(8),
                Field::makeIntField(4),
                Field::makeIntField(12),
                Field::makeIntField(2),
        }));

        ASSERT_EQ(sgroup.next(), OperatorStatus::NoMoreTuples);
        ASSERT_EQ(sgroup.getCurrentTuple(out), OperatorStatus::NoMoreTuples);
        ASSERT_TRUE(tuples_equal(out, Tuple {
                tuples2[0][0],
                tuples2[4][4],
                Field::makeIntField(8),
                Field::makeIntField(4),
                Field::makeIntField(12),
                Field::makeIntField(2),
        }));

        sgroup.reset();
    }
}
