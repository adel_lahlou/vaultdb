#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include "obliviousOperatorTestFixture.h"
#include <in_sgx/plan/oboperators/ObliviousOperator.h>
#include <in_sgx/plan/oboperators/ObliviousSeqScan.h>
#include <in_sgx/plan/oboperators/BitonicSort.h>

using namespace plan::oboperators;

TEST_F(ObliviousOperatorTestFixture, BitonicConstruction)
{
    ObliviousSeqScan * scan = new ObliviousSeqScan(0, & oinput1);
    std::vector<pos_vdb> orderByEntries;
    std::vector<SortOrder> sortOrders;
    orderByEntries.push_back(4);
    sortOrders.push_back(SortOrder::ASCEND);
    BitonicSort * sort = new BitonicSort(2, scan, orderByEntries, sortOrders);

    ASSERT_EQ(sort->getChildren().size(), 1);
    ASSERT_TRUE(recordschema_equal(sort->getSchema(), s1));
    ASSERT_TRUE(recordschema_equal(sort->getChildSchema(0), s1));
    ASSERT_EQ(scan->getParent(), sort);
    ASSERT_EQ(sort->getParent(), nullptr);
    ASSERT_TRUE(sort->isBlocking());

    delete sort;
}

TEST_F(ObliviousOperatorTestFixture, BitonicNextAndGet)
{
    ObliviousSeqScan * scan = new ObliviousSeqScan(0, & oinput1);
    std::vector<pos_vdb> orderByEntries;
    std::vector<SortOrder> sortOrders;
    orderByEntries.push_back(4);
    sortOrders.push_back(SortOrder::ASCEND);
    BitonicSort * sort = new BitonicSort(2, scan, orderByEntries, sortOrders);

    ObliviousTuple out;

    ASSERT_EQ(sort->next(), OperatorStatus::Ok);
    ASSERT_EQ(sort->getCurrentTuple(out), OperatorStatus::Ok);
    ASSERT_TRUE(oblivious_tuples_equal(otuples[1], out));

    ASSERT_EQ(sort->next(), OperatorStatus::Ok);
    ASSERT_EQ(sort->getCurrentTuple(out), OperatorStatus::Ok);
    ASSERT_TRUE(oblivious_tuples_equal(otuples[2], out));

    ASSERT_EQ(sort->next(), OperatorStatus::Ok);
    ASSERT_EQ(sort->getCurrentTuple(out), OperatorStatus::Ok);
    ASSERT_TRUE(oblivious_tuples_equal(otuples[3], out));

    ASSERT_EQ(sort->next(), OperatorStatus::Ok);
    ASSERT_EQ(sort->getCurrentTuple(out), OperatorStatus::Ok);
    ASSERT_TRUE(oblivious_tuples_equal(otuples[0], out));

    ASSERT_EQ(sort->next(), OperatorStatus::NoMoreTuples);
    ASSERT_EQ(sort->getCurrentTuple(out), OperatorStatus::NoMoreTuples);
    ASSERT_TRUE(oblivious_tuples_equal(otuples[0], out));

    delete sort;
}
