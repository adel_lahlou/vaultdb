#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include "operatorTestFixture.h"
#include <plaintext/plan/operators/Operator.h>
#include <plaintext/plan/operators/SeqScan.h>
#include <plaintext/plan/operators/Filter.h>
#include <plaintext/plan/operators/NestedJoin.h>

using namespace plan::operators;
using namespace plan::expressions;

TEST_F(OperatorTestFixture, JoinConstruction)
{
    JoinExpression joinPred(Expression::makeColumn(0), Expression::makeColumn(2));
    SeqScan * scan1 = new SeqScan(1, s1, nullptr, input1);
    SeqScan * scan2 = new SeqScan(2, s1, nullptr, input1);
    NestedJoin njoin(3, scan1, scan2, joinPred);

    ASSERT_EQ(njoin.getStatus(), OperatorStatus::NotInitialized);
    ASSERT_EQ(njoin.getChildren().size(), 2);
    ASSERT_TRUE(recordschema_equal(njoin.getSchema(), RecordSchema::join(s1, s1)));
    ASSERT_TRUE(recordschema_equal(njoin.getChildSchema(0), s1));
    ASSERT_TRUE(recordschema_equal(njoin.getChildSchema(1), s1));
    ASSERT_EQ(scan1->getParent(), &njoin);
    ASSERT_EQ(scan2->getParent(), &njoin);
    ASSERT_EQ(njoin.getParent(), nullptr);
    ASSERT_FALSE(njoin.isBlocking());
}

TEST_F(OperatorTestFixture, JoinNextAndGet)
{
    JoinExpression joinPred(Expression::makeColumn(0), Expression::makeColumn(2));
    SeqScan * scan1 = new SeqScan(1, s1, nullptr, input1);
    SeqScan * scan2 = new SeqScan(2, s1, nullptr, input1);
    NestedJoin njoin(3, scan1, scan2, joinPred);

    ASSERT_EQ(njoin.getStatus(), OperatorStatus::NotInitialized);

    Tuple out;

    ASSERT_EQ(njoin.next(), OperatorStatus::Ok);
    ASSERT_EQ(njoin.getCurrentTuple(out), OperatorStatus::Ok);
    ASSERT_TRUE(tuples_equal(out, Tuple::join(tuples[1], tuples[2])));

    ASSERT_EQ(njoin.next(), OperatorStatus::Ok);
    ASSERT_EQ(njoin.getCurrentTuple(out), OperatorStatus::Ok);
    ASSERT_TRUE(tuples_equal(out, Tuple::join(tuples[3], tuples[2])));

    ASSERT_EQ(njoin.next(), OperatorStatus::NoMoreTuples);
    ASSERT_EQ(njoin.getCurrentTuple(out), OperatorStatus::NoMoreTuples);
    ASSERT_TRUE(tuples_equal(out, Tuple::join(tuples[3], tuples[2])));

    ASSERT_EQ(njoin.next(), OperatorStatus::NoMoreTuples);
    ASSERT_EQ(njoin.getCurrentTuple(out), OperatorStatus::NoMoreTuples);
    ASSERT_TRUE(tuples_equal(out, Tuple::join(tuples[3], tuples[2])));

    njoin.reset();

    ASSERT_EQ(njoin.getStatus(), OperatorStatus::NotInitialized);

    ASSERT_EQ(njoin.next(), OperatorStatus::Ok);
    ASSERT_EQ(njoin.getCurrentTuple(out), OperatorStatus::Ok);
    ASSERT_TRUE(tuples_equal(out, Tuple::join(tuples[1], tuples[2])));

    ASSERT_EQ(njoin.next(), OperatorStatus::Ok);
    ASSERT_EQ(njoin.getCurrentTuple(out), OperatorStatus::Ok);
    ASSERT_TRUE(tuples_equal(out, Tuple::join(tuples[3], tuples[2])));

    ASSERT_EQ(njoin.next(), OperatorStatus::NoMoreTuples);
    ASSERT_EQ(njoin.getCurrentTuple(out), OperatorStatus::NoMoreTuples);
    ASSERT_TRUE(tuples_equal(out, Tuple::join(tuples[3], tuples[2])));

    ASSERT_EQ(njoin.next(), OperatorStatus::NoMoreTuples);
    ASSERT_EQ(njoin.getCurrentTuple(out), OperatorStatus::NoMoreTuples);
    ASSERT_TRUE(tuples_equal(out, Tuple::join(tuples[3], tuples[2])));
}
