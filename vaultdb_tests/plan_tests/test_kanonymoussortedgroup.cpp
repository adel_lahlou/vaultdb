#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include "kAnonymousOperatorTestFixture.h"
#include "in_sgx/plan/obexpressions/ObliviousExpressionFactory.h"
#include <in_sgx/plan/kaoperators/KAnonymousOperator.h>
#include <in_sgx/plan/kaoperators/KAnonymousSeqScan.h>
#include <in_sgx/plan/kaoperators/KAnonymousFilter.h>
#include <in_sgx/plan/kaoperators/KAnonymousSortedGroup.h>
#include <in_sgx/plan/kaoperators/KAnonymousWindow.h>
#include <debug/DebugInSGXController.h>
#include <debug/DebugFakeLocalDataMover.h>

#ifndef KANO_SORTED_GROUP_PRINT_TUPLES
//#define KANO_SORTED_GROUP_PRINT_TUPLES
#endif

using namespace plan::kaoperators;
using namespace plan::obexpressions;

TEST_F(KAnonymousOperatorTestFixture, KAnonymousSortedGroupConstruction)
{
    RecordSchema expectedOutSchema{s1.getTableID(), {
            {FieldDataType::Int, sizeof(int)},
            {FieldDataType::Fixchar, vc_size},
            {FieldDataType::Int, sizeof(int)},
            {FieldDataType::Int, sizeof(int)},
            {FieldDataType::Int, sizeof(int)},
            {FieldDataType::Int, sizeof(int)},
    }};

    ObliviousExpressionList exps = {
            ObliviousExpressionFactory::makeLiteral(otuples2[0][0], sizeof(int)),
            ObliviousExpressionFactory::makeColumn(4, type::SchemaColumn{type::FieldDataType::Fixchar, vc_size}),
            ObliviousExpressionFactory::makeSum(0, type::SchemaColumn{type::FieldDataType::Int, sizeof(int)}),
            ObliviousExpressionFactory::makeSum(1, type::SchemaColumn{type::FieldDataType::Int, sizeof(int)}),
            ObliviousExpressionFactory::makeSum(2, type::SchemaColumn{type::FieldDataType::Int, sizeof(int)}),
            ObliviousExpressionFactory::makeCount(4),
    };

    size_vdb k = 2;
    utilities::DebugInSGXController m1(1, 0);
    std::vector<pos_vdb> groupBys = {0, 4};
    KAnonymousSeqScan * scan = new KAnonymousSeqScan(0, k, & oinput1);
    KAnonymousSortedGroup sgroup(1, k, scan, exps, groupBys, &m1);

    ASSERT_EQ(sgroup.getStatus(), OperatorStatus::NotInitialized);
    ASSERT_EQ(sgroup.getChildren().size(), 1);
    ASSERT_TRUE(recordschema_equal(sgroup.getSchema(), expectedOutSchema));
    ASSERT_TRUE(recordschema_equal(sgroup.getChildSchema(0), s1));
    ASSERT_EQ(scan->getParent(), sgroup.getChild(0));
    ASSERT_EQ(sgroup.getParent(), nullptr);
    ASSERT_FALSE(sgroup.isBlocking());
    utilities::DebugFakeLocalDataMover::instance()->clear();
}

TEST_F(KAnonymousOperatorTestFixture, KAnonymousSortedGroupNextAndGet) {

    size_vdb k = 2;

    ObliviousExpressionList exps = {
            ObliviousExpressionFactory::makeColumn(0, type::SchemaColumn{type::FieldDataType::Int, sizeof(int)}),
            ObliviousExpressionFactory::makeSum(1, type::SchemaColumn{type::FieldDataType::Int, sizeof(int)}),
            ObliviousExpressionFactory::makeCount(1),
            ObliviousExpressionFactory::makeCountDistinct(1, type::SchemaColumn{type::FieldDataType::Int, sizeof(int)}),
    };

    ObliviousTupleList expectedTuples {
//            k = 2
//            Query: col_0, sum(col_1), count(col_1)
//            D - 1,1,1
//            D - 1,3,2
//             X - 1,5,3
//             X - 1,8,4
//             X - 1,11,5
//            R - 1,14,6
//            R - 2,1,1
//            D - 3,1,1
//            R - 3,3,2
//            D - 4,1,1
//            D - 4,2,2
//            R - 4,3,3
//            ObliviousTuple(true, {
//                    ObliviousField::makeObliviousIntField(1),
//                    ObliviousField::makeObliviousIntField(1),
//                    ObliviousField::makeObliviousIntField(1),
//            }),
//            ObliviousTuple(true, {
//                    ObliviousField::makeObliviousIntField(1),
//                    ObliviousField::makeObliviousIntField(3),
//                    ObliviousField::makeObliviousIntField(2),
//            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(14),
                    ObliviousField::makeObliviousIntField(6),
                    ObliviousField::makeObliviousIntField(3),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(1),
            }),
//            ObliviousTuple(true, {
//                    ObliviousField::makeObliviousIntField(3),
//                    ObliviousField::makeObliviousIntField(1),
//                    ObliviousField::makeObliviousIntField(1),
//            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(2),
            }),
//            ObliviousTuple(true, {
//                    ObliviousField::makeObliviousIntField(4),
//                    ObliviousField::makeObliviousIntField(1),
//                    ObliviousField::makeObliviousIntField(1),
//            }),
//            ObliviousTuple(true, {
//                    ObliviousField::makeObliviousIntField(4),
//                    ObliviousField::makeObliviousIntField(2),
//                    ObliviousField::makeObliviousIntField(2),
//            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousIntField(1),
            }),
    };

    utilities::DebugInSGXController m1(1, 0);

    std::vector<pos_vdb> groupBys = {0};
    KAnonymousSeqScan *scan = new KAnonymousSeqScan(0, k, & oinput3);
    KAnonymousSortedGroup sgroup(1, k, scan, exps, groupBys, &m1);

    ObliviousTuple out;
    pos_vdb i = 0;
#ifdef KANO_SORTED_GROUP_PRINT_TUPLES
    fprintf(stdout, "\n");
#endif
    while (sgroup.next() == OperatorStatus::Ok) {
        sgroup.getCurrentTuple(out);
        if (out.isDummy()) {
#ifdef KANO_SORTED_GROUP_PRINT_TUPLES
            fprintf(stdout, "Produced a dummy tuple; %s\n", out.toString().c_str());
#endif
            continue;
        } else {
            if (i < expectedTuples.size()) {
                ObliviousTuple t = expectedTuples[i];
#ifdef KANO_SORTED_GROUP_PRINT_TUPLES
                fprintf(stdout, "Expected / actual: %s / %s\n", t.toString().c_str(), out.toString().c_str());
#endif
                ASSERT_TRUE(oblivious_tuples_equal(out, t));
            } else {
#ifdef KANO_SORTED_GROUP_PRINT_TUPLES
                fprintf(stdout, "Expected / actual: (none) / %s\n", out.toString().c_str());
#endif
            }
            i++;
        }
    }
    ASSERT_EQ(i, expectedTuples.size());
    ASSERT_EQ(sgroup.getStatus(), OperatorStatus::NoMoreTuples);
    utilities::DebugFakeLocalDataMover::instance()->clear();
}


TEST_F(KAnonymousOperatorTestFixture, KAnonymousSortedGroupWithDummy) {

    size_vdb k = 2;

    ObliviousExpressionList exps = {
            ObliviousExpressionFactory::makeColumn(0, type::SchemaColumn{type::FieldDataType::Int, sizeof(int)}),
            ObliviousExpressionFactory::makeSum(1, type::SchemaColumn{type::FieldDataType::Int, sizeof(int)}),
            ObliviousExpressionFactory::makeCount(1),
    };

    ObliviousTupleList expectedTuples {
//            k = 2
//            Query: col_0, sum(col_1), count(col_1)
//            D - 1,2,1
//            D - 1,5,2
//              X - 1,8,3
//            R - 1,3,1
//            D - 2,1,1
//            D - 3,1,1 <- R
//            R - 3,3,2
//            D - 4,6,1
//            D - 4,3,2
//            R - 4,2,1
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(1),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousIntField(2),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousIntField(1),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(1),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(1),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousIntField(2),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(6),
                    ObliviousField::makeObliviousIntField(1),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(2),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousIntField(1),
            }),
    };
    utilities::DebugInSGXController m1(1, 0);
    std::vector<pos_vdb> groupBys = {0};
    KAnonymousSeqScan *scan = new KAnonymousSeqScan(0, k, & oinput4);
    KAnonymousSortedGroup sgroup(1, k, scan, exps, groupBys, &m1);

    ObliviousTuple out;
#ifdef KANO_SORTED_GROUP_PRINT_TUPLES
    std::cout << std::endl;
#endif
    for (ObliviousTuple t : expectedTuples) {
        sgroup.next();
        sgroup.getCurrentTuple(out);
        ASSERT_EQ(out.isDummy(), t.isDummy());
        if (out.isDummy()) {
#ifdef KANO_SORTED_GROUP_PRINT_TUPLES
            printf("Produced a dummy tuple; %s\n", out.toString().c_str());
#endif
            continue;
        } else {
#ifdef KANO_SORTED_GROUP_PRINT_TUPLES
            printf("Real / actual: %s / %s\n", t.toString().c_str(), out.toString().c_str());
#endif
        }
        ASSERT_TRUE(oblivious_tuples_equal(out, t));
    }
    ASSERT_EQ(sgroup.next(), OperatorStatus::NoMoreTuples);
    utilities::DebugFakeLocalDataMover::instance()->clear();
}


TEST_F(KAnonymousOperatorTestFixture, KAnonymousDistinctWithDummy) {

    size_vdb k = 2;

    ObliviousExpressionList exps = {
            ObliviousExpressionFactory::makeColumn(0, type::SchemaColumn{type::FieldDataType::Int, sizeof(int)}),
    };

    ObliviousTupleList expectedTuples {
//            k = 2
//            Query: col_0
//            D - 1
//            D - 1
//              X - 1
//            R - 1
//            D - 2
//            D - 3
//            R - 3
//            D - 4
//            D - 4
//            R - 4
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(1),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(1),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(2),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(3),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(3),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(4),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(4),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(4),
            }),
    };
    utilities::DebugInSGXController m1(1, 0);
    std::vector<pos_vdb> groupBys = {0};
    KAnonymousSeqScan *scan = new KAnonymousSeqScan(0, k, & oinput4);
    KAnonymousSortedGroup sgroup(1, k, scan, exps, groupBys, &m1);

    ObliviousTuple out;
#ifdef KANO_SORTED_GROUP_PRINT_TUPLES
    std::cout << std::endl;
#endif
    for (ObliviousTuple t : expectedTuples) {
        sgroup.next();
        sgroup.getCurrentTuple(out);
        ASSERT_EQ(out.isDummy(), t.isDummy());
        if (out.isDummy()) {
#ifdef KANO_SORTED_GROUP_PRINT_TUPLES
            printf("Produced a dummy tuple; %s\n", out.toString().c_str());
#endif
            continue;
        } else {
#ifdef KANO_SORTED_GROUP_PRINT_TUPLES
            printf("Real / actual: %s / %s\n", t.toString().c_str(), out.toString().c_str());
#endif
        }
        ASSERT_TRUE(oblivious_tuples_equal(out, t));
    }
    ASSERT_EQ(sgroup.next(), OperatorStatus::NoMoreTuples);
    utilities::DebugFakeLocalDataMover::instance()->clear();
}


TEST_F(KAnonymousOperatorTestFixture, KAnonymousWindowRowNumber) {

    size_vdb k = 2;

    ObliviousExpressionList exps = {
            ObliviousExpressionFactory::makeColumn(0, type::SchemaColumn{type::FieldDataType::Int, sizeof(int)}),
            ObliviousExpressionFactory::makeColumn(1, type::SchemaColumn{type::FieldDataType::Int, sizeof(int)}),
            ObliviousExpressionFactory::makeColumn(2, type::SchemaColumn{type::FieldDataType::Fixchar, vc_size}),
            ObliviousExpressionFactory::makeRowNumber(),
    };

    ObliviousTupleList expectedTuples {
//            D - 1,3,"peace1",0
//            D - 1,3,"peace2",0
//            R - 1,3,"peace3",1
//            D - 1,2,"lasting",0
//            D - 2,1,"smart",0
//            R - 3,2,"giant",1
//            D - 3,1,"tornado",2
//            R - 4,2,"chimney",1
//            D - 4,6,"fire pit",1
//            R - 4,3,"rogue",2


            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousFixcharField("    peace1", vc_size),
                    ObliviousField::makeObliviousIntField(0),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousFixcharField("    peace2", vc_size),
                    ObliviousField::makeObliviousIntField(0),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousFixcharField("    peace3", vc_size),
                    ObliviousField::makeObliviousIntField(1),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("   lasting", vc_size),
                    ObliviousField::makeObliviousIntField(0),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("     smart", vc_size),
                    ObliviousField::makeObliviousIntField(0),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("     giant", vc_size),
                    ObliviousField::makeObliviousIntField(1),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("   tornado", vc_size),
                    ObliviousField::makeObliviousIntField(2),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousFixcharField("     rogue", vc_size),
                    ObliviousField::makeObliviousIntField(1),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("   chimney", vc_size),
                    ObliviousField::makeObliviousIntField(0),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(6),
                    ObliviousField::makeObliviousFixcharField("  fire pit", vc_size),
                    ObliviousField::makeObliviousIntField(0),
            }),

    };
    utilities::DebugInSGXController m1(1, 0);
    std::vector<pos_vdb> partition = {0};
    std::vector<pos_vdb> orderBys = {2};
    std::vector<SortOrder> sortOrder = {SortOrder :: ASCEND};
    KAnonymousSeqScan *scan = new KAnonymousSeqScan(0, k, & oinput4);
    KAnonymousWindow window(1, k, scan, exps, partition, orderBys,
                            sortOrder, &m1);

    ObliviousTuple out;
#ifdef KANO_SORTED_GROUP_PRINT_TUPLES
    std::cout << std::endl;
#endif
    for (ObliviousTuple t : expectedTuples) {
        window.next();
        window.getCurrentTuple(out);
        ASSERT_EQ(out.isDummy(), t.isDummy());
        if (out.isDummy()) {
#ifdef KANO_SORTED_GROUP_PRINT_TUPLES
            printf("Dummy: %s\n", out.toString().c_str());
#endif
            continue;
        } else {
#ifdef KANO_SORTED_GROUP_PRINT_TUPLES
            printf("Expected / Actual: %s / %s\n", t.toString().c_str(), out.toString().c_str());
#endif
        }
        ASSERT_TRUE(oblivious_tuples_equal(out, t));
    }
    ASSERT_EQ(window.next(), OperatorStatus::NoMoreTuples);
    utilities::DebugFakeLocalDataMover::instance()->clear();
}