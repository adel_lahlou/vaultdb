#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include "obliviousOperatorTestFixture.h"
#include "in_sgx/plan/obexpressions/ObliviousExpressionFactory.h"
#include <in_sgx/plan/oboperators/ObliviousOperator.h>
#include <in_sgx/plan/oboperators/ObliviousSeqScan.h>
#include <in_sgx/plan/oboperators/ObliviousFilter.h>
#include <in_sgx/plan/oboperators/ObliviousSortedGroup.h>


using namespace plan::oboperators;
using namespace plan::obexpressions;

TEST_F(ObliviousOperatorTestFixture, ObliviousSortedGroupConstruction)
{
    RecordSchema expectedOutSchema{s1.getTableID(), {
            {FieldDataType::Int, sizeof(int)},
            {FieldDataType::Fixchar, vc_size},
            {FieldDataType::Int, sizeof(int)},
            {FieldDataType::Int, sizeof(int)},
            {FieldDataType::Int, sizeof(int)},
            {FieldDataType::Int, sizeof(int)},
    }};

    ObliviousExpressionList exps = {
            ObliviousExpressionFactory::makeLiteral(otuples2[0][0], s1[0].size),
            ObliviousExpressionFactory::makeColumn(4, s1[4]),
            ObliviousExpressionFactory::makeSum(0, s1[0]),
            ObliviousExpressionFactory::makeSum(1, s1[1]),
            ObliviousExpressionFactory::makeSum(2, s1[2]),
            ObliviousExpressionFactory::makeCount(4),
    };
    std::vector<pos_vdb> groupBys = {0, 4};
    ObliviousSeqScan * scan = new ObliviousSeqScan(0,& oinput1);
    ObliviousSortedGroup sgroup(1, scan, exps, groupBys);

    ASSERT_EQ(sgroup.getStatus(), OperatorStatus::NotInitialized);
    ASSERT_EQ(sgroup.getChildren().size(), 1);
    ASSERT_TRUE(recordschema_equal(sgroup.getSchema(), expectedOutSchema));
    ASSERT_TRUE(recordschema_equal(sgroup.getChildSchema(0), s1));
    ASSERT_EQ(scan->getParent(), sgroup.getChild(0));
    ASSERT_EQ(sgroup.getParent(), nullptr);
    ASSERT_FALSE(sgroup.isBlocking());
}

TEST_F(ObliviousOperatorTestFixture, ObliviousSortedGroupNextAndGet) {
    ObliviousExpressionList exps = {
            ObliviousExpressionFactory::makeLiteral(otuples2[0][0], s1[0].size),
            ObliviousExpressionFactory::makeColumn(4, s1[4]),
            ObliviousExpressionFactory::makeSum(0, s1[0]),
            ObliviousExpressionFactory::makeSum(1, s1[1]),
            ObliviousExpressionFactory::makeSum(2, s1[2]),
            ObliviousExpressionFactory::makeCount(4),
    };
    std::vector<pos_vdb> groupBys = {0, 4};
    ObliviousSeqScan *scan = new ObliviousSeqScan(0, &oinput2);
    ObliviousSortedGroup sgroup(1, scan, exps, groupBys);

    ObliviousTuple out;

    for(int i =0; i < 1; ++i) {

        ASSERT_EQ(sgroup.getStatus(), OperatorStatus::NotInitialized);

        ASSERT_EQ(sgroup.next(), OperatorStatus::Ok);
        ASSERT_EQ(sgroup.getCurrentTuple(out), OperatorStatus::Ok);
        ASSERT_TRUE(oblivious_tuples_equal(out, ObliviousTuple {
                otuples2[0][0],
                otuples2[3][4],
                ObliviousField::makeObliviousIntField(1),
                ObliviousField::makeObliviousIntField(2),
                ObliviousField::makeObliviousIntField(4),
                ObliviousField::makeObliviousIntField(1),
        }));
        ASSERT_FALSE(out.isDummy());

        ASSERT_EQ(sgroup.next(), OperatorStatus::Ok);
        ASSERT_EQ(sgroup.getCurrentTuple(out), OperatorStatus::Ok);
        ASSERT_TRUE(oblivious_tuples_equal(out, ObliviousTuple {
                otuples2[0][0],
                otuples2[0][4],
                ObliviousField::makeObliviousIntField(1),
                ObliviousField::makeObliviousIntField(2),
                ObliviousField::makeObliviousIntField(3),
                ObliviousField::makeObliviousIntField(1),
        }));
        ASSERT_TRUE(out.isDummy());

        ASSERT_EQ(sgroup.next(), OperatorStatus::Ok);
        ASSERT_EQ(sgroup.getCurrentTuple(out), OperatorStatus::Ok);
        ASSERT_TRUE(oblivious_tuples_equal(out, ObliviousTuple {
                otuples2[0][0],
                otuples2[0][4],
                ObliviousField::makeObliviousIntField(2),
                ObliviousField::makeObliviousIntField(4),
                ObliviousField::makeObliviousIntField(6),
                ObliviousField::makeObliviousIntField(2),
        }));
        ASSERT_FALSE(out.isDummy());

        ASSERT_EQ(sgroup.next(), OperatorStatus::Ok);
        ASSERT_EQ(sgroup.getCurrentTuple(out), OperatorStatus::Ok);
        ASSERT_TRUE(oblivious_tuples_equal(out, ObliviousTuple {
                otuples2[0][0],
                otuples2[1][4],
                ObliviousField::makeObliviousIntField(4),
                ObliviousField::makeObliviousIntField(5),
                ObliviousField::makeObliviousIntField(6),
                ObliviousField::makeObliviousIntField(1),
        }));
        ASSERT_TRUE(out.isDummy());

        ASSERT_EQ(sgroup.next(), OperatorStatus::Ok);
        ASSERT_EQ(sgroup.getCurrentTuple(out), OperatorStatus::Ok);
        ASSERT_TRUE(oblivious_tuples_equal(out, ObliviousTuple {
                otuples2[0][0],
                otuples2[1][4],
                ObliviousField::makeObliviousIntField(8),
                ObliviousField::makeObliviousIntField(7),
                ObliviousField::makeObliviousIntField(9),
                ObliviousField::makeObliviousIntField(2),
        }));
        ASSERT_FALSE(out.isDummy());

        ASSERT_EQ(sgroup.next(), OperatorStatus::Ok);
        ASSERT_EQ(sgroup.getCurrentTuple(out), OperatorStatus::Ok);
        ASSERT_TRUE(oblivious_tuples_equal(out, ObliviousTuple {
                otuples2[0][0],
                otuples2[4][4],
                ObliviousField::makeObliviousIntField(4),
                ObliviousField::makeObliviousIntField(2),
                ObliviousField::makeObliviousIntField(6),
                ObliviousField::makeObliviousIntField(1),
        }));
        ASSERT_TRUE(out.isDummy());

        ASSERT_EQ(sgroup.next(), OperatorStatus::Ok);
        ASSERT_EQ(sgroup.getCurrentTuple(out), OperatorStatus::Ok);
        ASSERT_TRUE(oblivious_tuples_equal(out, ObliviousTuple {
                otuples2[0][0],
                otuples2[4][4],
                ObliviousField::makeObliviousIntField(8),
                ObliviousField::makeObliviousIntField(4),
                ObliviousField::makeObliviousIntField(12),
                ObliviousField::makeObliviousIntField(2),
        }));
        ASSERT_FALSE(out.isDummy());

        ASSERT_EQ(sgroup.next(), OperatorStatus::NoMoreTuples);
        ASSERT_EQ(sgroup.getCurrentTuple(out), OperatorStatus::NoMoreTuples);
        ASSERT_TRUE(oblivious_tuples_equal(out, ObliviousTuple {
                otuples2[0][0],
                otuples2[4][4],
                ObliviousField::makeObliviousIntField(8),
                ObliviousField::makeObliviousIntField(4),
                ObliviousField::makeObliviousIntField(12),
                ObliviousField::makeObliviousIntField(2),
        }));
        ASSERT_FALSE(out.isDummy());

        ASSERT_EQ(sgroup.next(), OperatorStatus::NoMoreTuples);
        ASSERT_EQ(sgroup.getCurrentTuple(out), OperatorStatus::NoMoreTuples);
        ASSERT_TRUE(oblivious_tuples_equal(out, ObliviousTuple {
                otuples2[0][0],
                otuples2[4][4],
                ObliviousField::makeObliviousIntField(8),
                ObliviousField::makeObliviousIntField(4),
                ObliviousField::makeObliviousIntField(12),
                ObliviousField::makeObliviousIntField(2),
        }));
        ASSERT_FALSE(out.isDummy());

        sgroup.reset();
    }
}
