#ifndef VAULTDB_OPERATORTESTFIXTURE_H
#define VAULTDB_OPERATORTESTFIXTURE_H

#include <gtest/gtest.h>
#include <plaintext/data/Tuple.h>
#include <plaintext/data/TupleTable.h>
#include <shared/type/RecordSchema.h>

using namespace db::data;
using namespace db::data::field;
using namespace type;

bool static recordschema_equal(const RecordSchema& s1, const RecordSchema& s2)
{
    if(s1.size() != s2.size())
        return false;

    for(int i = 0; i < s1.size(); ++i){
        if(s1[i].type != s2[i].type)
            return false;
    }

    return true;
}

bool static tuples_equal(const Tuple& t1, const Tuple& t2)
{
    if(t1.size() != t2.size())
        return false;

    for(int i = 0; i < t1.size(); ++i){
        if(t1[i] != t2[i])
            return false;
    }

    return true;
}

class OperatorTestFixture : public ::testing::Test {
protected:

    OperatorTestFixture()
            : input1(s1, tuples),
              input2(s1, tuples2)
    {}

    ~OperatorTestFixture() {}

    type::RecordSchema s1{
            (TableID) 4, {
                    {type::FieldDataType::Int, sizeof(int)},
                    {type::FieldDataType::Int, sizeof(int)},
                    {type::FieldDataType::Int, sizeof(int)},
                    {type::FieldDataType::Varchar, 20},
                    {type::FieldDataType::Varchar, 20},
            }
    };

    TupleList tuples {
        Tuple{
                Field::makeIntField(1),
                Field::makeIntField(2),
                Field::makeIntField(3),
                Field::makeVarcharField("hello"),
                Field::makeVarcharField("world"),
        },
        Tuple{
                Field::makeIntField(4),
                Field::makeIntField(5),
                Field::makeIntField(6),
                Field::makeVarcharField("foo"),
                Field::makeVarcharField("bar"),
        },
        Tuple{
                Field::makeIntField(1),
                Field::makeIntField(2),
                Field::makeIntField(4),
                Field::makeVarcharField("bigger"),
                Field::makeVarcharField("better"),
        },
        Tuple{
                Field::makeIntField(4),
                Field::makeIntField(2),
                Field::makeIntField(6),
                Field::makeVarcharField("secure"),
                Field::makeVarcharField("execution"),
        },
    };

    TupleList tuples2 {
            Tuple{
                    Field::makeIntField(1),
                    Field::makeIntField(2),
                    Field::makeIntField(3),
                    Field::makeVarcharField("hello"),
                    Field::makeVarcharField("world"),
            },

            Tuple{
                    Field::makeIntField(4),
                    Field::makeIntField(2),
                    Field::makeIntField(3),
                    Field::makeVarcharField("hello"),
                    Field::makeVarcharField("bar"),
            },
            Tuple{
                    Field::makeIntField(4),
                    Field::makeIntField(5),
                    Field::makeIntField(6),
                    Field::makeVarcharField("foo"),
                    Field::makeVarcharField("bar"),
            },
            Tuple{
                    Field::makeIntField(1),
                    Field::makeIntField(2),
                    Field::makeIntField(4),
                    Field::makeVarcharField("bigger"),
                    Field::makeVarcharField("better"),
            },
            Tuple{
                    Field::makeIntField(4),
                    Field::makeIntField(2),
                    Field::makeIntField(6),
                    Field::makeVarcharField("secure"),
                    Field::makeVarcharField("execution"),
            },
            Tuple{
                    Field::makeIntField(1),
                    Field::makeIntField(2),
                    Field::makeIntField(3),
                    Field::makeVarcharField("hello"),
                    Field::makeVarcharField("world"),
            },
            Tuple{
                    Field::makeIntField(4),
                    Field::makeIntField(2),
                    Field::makeIntField(6),
                    Field::makeVarcharField("secure"),
                    Field::makeVarcharField("execution"),
            },
    };

    TupleTable input1;
    TupleTable input2;
};

#endif //VAULTDB_OPERATORTESTFIXTURE_H
