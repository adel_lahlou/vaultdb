#ifndef VAULTDB_EXPRESSIONSTESTFIXTURE_H
#define VAULTDB_EXPRESSIONSTESTFIXTURE_H

#include <gtest/gtest.h>
#include <plaintext/plan/expressions/expressions.h>
#include <plaintext/data/field/IntField.h>
#include <plaintext/data/field/BoolField.h>
#include <plaintext/data/field/VarcharField.h>
#include <plaintext/data/Tuple.h>

// TODO: decide whether these using are worth it
using namespace plan::expressions;
using namespace db::data;
using namespace db::data::field;

class ExpressionsTestFixture : public ::testing::Test {
protected:
    ExpressionsTestFixture() {
        c_10.setTuple(&t1);
        c_11.setTuple(&t1);
        c_12.setTuple(&t1);
        c_13.setTuple(&t1);

        c_20.setTuple(&t2);
        c_21.setTuple(&t2);
        c_22.setTuple(&t2);
        c_23.setTuple(&t2);
    }

    ~ExpressionsTestFixture() {};

    db::data::Tuple t1{
            Field::makeIntField(1),
            Field::makeIntField(2),
            Field::makeBoolField(true),
            Field::makeVarcharField("hello"),
            Field::makeVarcharField("hello")
    };

    db::data::Tuple t2{
            Field::makeIntField(3),
            Field::makeIntField(4),
            Field::makeBoolField(true),
            Field::makeVarcharField("foo"),
            Field::makeVarcharField("bar")
    };

    Expression i_1 = Expression::makeLiteral(Field::makeIntField(1));
    Expression i_2 = Expression::makeLiteral(Field::makeIntField(2));
    Expression i_3 = Expression::makeLiteral(Field::makeIntField(3));

    Expression b_true = Expression::makeLiteral(Field::makeBoolField(true));
    Expression b_false = Expression::makeLiteral(Field::makeBoolField(false));

    Expression s_hello = Expression::makeLiteral(Field::makeVarcharField("hello"));
    Expression s_world = Expression::makeLiteral(Field::makeVarcharField("world"));

    Expression c_10 = Expression::makeColumn(0);
    Expression c_11 = Expression::makeColumn(1);
    Expression c_12 = Expression::makeColumn(2);
    Expression c_13 = Expression::makeColumn(3);

    Expression c_20 = Expression::makeColumn(0);
    Expression c_21 = Expression::makeColumn(1);
    Expression c_22 = Expression::makeColumn(2);
    Expression c_23 = Expression::makeColumn(3);
};

#endif
