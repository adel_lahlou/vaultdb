#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <in_sgx/plan/obexpressions/obexpressions.h>
#include "obliviousExpressionsTestFixture.h"
#include "in_sgx/plan/obexpressions/ObliviousExpressionFactory.h"


TEST_F(ObliviousExpressionsTestFixture, ConstructLeaf)
{
    ASSERT_EQ(i_1.evaluate(), ObliviousField::makeObliviousIntField(1));
    ASSERT_EQ(i_2.evaluate(), ObliviousField::makeObliviousIntField(2));

    ASSERT_EQ(b_true.evaluate(), ObliviousField::makeObliviousBoolField(true));
    ASSERT_EQ(b_false.evaluate(), ObliviousField::makeObliviousBoolField(false));
    ASSERT_TRUE(b_true.evaluate().isTruthy());
    ASSERT_FALSE(b_false.evaluate().isTruthy());

    ASSERT_EQ(s_hello.evaluate(), ObliviousField::makeObliviousFixcharField("     hello", vc_size));
    ASSERT_EQ(s_world.evaluate(), ObliviousField::makeObliviousFixcharField("     world", vc_size));

    ASSERT_EQ(c_10.evaluate(), t1[0]);
    ASSERT_EQ(c_11.evaluate(), t1[1]);
    ASSERT_EQ(c_vc_size.evaluate(), t1[2]);
    ASSERT_EQ(c_13.evaluate(), t1[3]);

    ASSERT_EQ(c_20.evaluate(), t2[0]);
    ASSERT_EQ(c_21.evaluate(), t2[1]);
    ASSERT_EQ(c_22.evaluate(), t2[2]);
    ASSERT_EQ(c_23.evaluate(), t2[3]);
}


TEST_F(ObliviousExpressionsTestFixture, Arithmetic)
{
    ObliviousExpression a_3 = ObliviousExpressionFactory::makeAdd(i_1, i_2);
    ASSERT_EQ(a_3.evaluate(), ObliviousField::makeObliviousIntField(3));

    ObliviousExpression a_4 = ObliviousExpressionFactory::makeAdd(i_2, i_2);
    ASSERT_EQ(a_4.evaluate(), ObliviousField::makeObliviousIntField(4));

    ObliviousExpression s_1 = ObliviousExpressionFactory::makeSub(i_2, i_1);
    ASSERT_EQ(s_1.evaluate(), ObliviousField::makeObliviousIntField(1));

    ObliviousExpression s_neg1 = ObliviousExpressionFactory::makeSub(i_1, i_2);
    ASSERT_EQ(s_neg1.evaluate(), ObliviousField::makeObliviousIntField(-1));

    ObliviousExpression m_2 = ObliviousExpressionFactory::makeMul(i_1, i_2);
    ASSERT_EQ(m_2.evaluate(), ObliviousField::makeObliviousIntField(2));

    ObliviousExpression m_1 = ObliviousExpressionFactory::makeMul(i_1, i_1);
    ASSERT_EQ(m_1.evaluate(), ObliviousField::makeObliviousIntField(1));

    ObliviousExpression d_1 = ObliviousExpressionFactory::makeMul(i_1, i_1);
    ASSERT_EQ(d_1.evaluate(), ObliviousField::makeObliviousIntField(1));

    ObliviousExpression a_cs_t1 = ObliviousExpressionFactory::makeAdd(
            ObliviousExpressionFactory::makeColumn(1, type::SchemaColumn{type::FieldDataType::Int, sizeof(int)}),
            ObliviousExpressionFactory::makeColumn(0, type::SchemaColumn{type::FieldDataType::Int, sizeof(int)}));
    a_cs_t1.setObliviousTuple(&t1);
    ASSERT_EQ(a_cs_t1.evaluate(), t1[1] + t1[0]);

    ObliviousExpression s_cs_t1 = ObliviousExpressionFactory::makeSub(
            ObliviousExpressionFactory::makeColumn(1, type::SchemaColumn{type::FieldDataType::Int, sizeof(int)}),
            ObliviousExpressionFactory::makeColumn(0, type::SchemaColumn{type::FieldDataType::Int, sizeof(int)}));
    s_cs_t1.setObliviousTuple(&t1);
    ASSERT_EQ(s_cs_t1.evaluate(), t1[1] - t1[0]);

    ObliviousExpression a_cs_t2 = ObliviousExpressionFactory::makeAdd(
            ObliviousExpressionFactory::makeColumn(1, type::SchemaColumn{type::FieldDataType::Int, sizeof(int)}),
            ObliviousExpressionFactory::makeColumn(0, type::SchemaColumn{type::FieldDataType::Int, sizeof(int)}));
    a_cs_t2.setObliviousTuple(&t2);
    ASSERT_EQ(a_cs_t2.evaluate(), t2[1] + t2[0]);

    ObliviousExpression s_cs_t2 = ObliviousExpressionFactory::makeSub(
            ObliviousExpressionFactory::makeColumn(1, type::SchemaColumn{type::FieldDataType::Int, sizeof(int)}),
            ObliviousExpressionFactory::makeColumn(0, type::SchemaColumn{type::FieldDataType::Int, sizeof(int)}));
    s_cs_t2.setObliviousTuple(&t2);
    ASSERT_EQ(s_cs_t2.evaluate(), t2[1] - t2[0]);
}


TEST_F(ObliviousExpressionsTestFixture, Comparison)
{
    ObliviousExpression eq_i_true = ObliviousExpressionFactory::makeEq(i_1, ObliviousExpressionFactory::makeLiteral(ObliviousField::makeObliviousIntField(1), sizeof(int)));
    ObliviousExpression eq_i_false = ObliviousExpressionFactory::makeEq(i_1, i_2);

    ASSERT_TRUE(eq_i_true.evaluate().isTruthy());
    ASSERT_FALSE(eq_i_false.evaluate().isTruthy());

    ObliviousExpression eq_b_true = ObliviousExpressionFactory::makeEq(b_false, ObliviousExpressionFactory::makeLiteral(ObliviousField::makeObliviousBoolField(false), sizeof(int)));
    ObliviousExpression eq_b_false = ObliviousExpressionFactory::makeEq(b_true, b_false);

    ASSERT_TRUE(eq_b_true.evaluate().isTruthy());
    ASSERT_FALSE(eq_b_false.evaluate().isTruthy());

    ObliviousExpression eq_vc_true = ObliviousExpressionFactory::makeEq(s_hello, ObliviousExpressionFactory::makeLiteral(
            ObliviousField::makeObliviousFixcharField("hello", vc_size), vc_size));
    ObliviousExpression eq_vc_false = ObliviousExpressionFactory::makeEq(s_hello, s_world);

    ASSERT_TRUE(eq_vc_true.evaluate().isTruthy());
    ASSERT_FALSE(eq_vc_false.evaluate().isTruthy());

    ObliviousExpression neq_s_true = ObliviousExpressionFactory::makeNEq(s_hello, s_world);
    ObliviousExpression neq_s_false = ObliviousExpressionFactory::makeNEq(s_hello, ObliviousExpressionFactory::makeLiteral(
            ObliviousField::makeObliviousFixcharField("hello", vc_size), vc_size));

    ASSERT_TRUE(neq_s_true.evaluate().isTruthy());
    ASSERT_FALSE(neq_s_false.evaluate().isTruthy());

    ObliviousExpression lt_i_true = ObliviousExpressionFactory::makeLT(i_1, i_3);
    ObliviousExpression lt_i_false1 = ObliviousExpressionFactory::makeLT(i_1, i_1);
    ObliviousExpression lt_i_false2 = ObliviousExpressionFactory::makeLT(i_3, i_2);

    ASSERT_TRUE(lt_i_true.evaluate().isTruthy());
    ASSERT_FALSE(lt_i_false1.evaluate().isTruthy());
    ASSERT_FALSE(lt_i_false2.evaluate().isTruthy());

    ObliviousExpression gt_i_true = ObliviousExpressionFactory::makeGT(i_3, i_2);
    ObliviousExpression gt_i_false1 = ObliviousExpressionFactory::makeGT(i_3, i_3);
    ObliviousExpression gt_i_false2 = ObliviousExpressionFactory::makeGT(i_2, i_3);

    ASSERT_TRUE(gt_i_true.evaluate().isTruthy());
    ASSERT_FALSE(gt_i_false1.evaluate().isTruthy());
    ASSERT_FALSE(gt_i_false2.evaluate().isTruthy());

    ObliviousExpression lte_i_true1 = ObliviousExpressionFactory::makeLTE(i_1, i_3);
    ObliviousExpression lte_i_true2 = ObliviousExpressionFactory::makeLTE(i_1, i_1);
    ObliviousExpression lte_i_false2 = ObliviousExpressionFactory::makeLTE(i_3, i_2);

    ASSERT_TRUE(lte_i_true1.evaluate().isTruthy());
    ASSERT_TRUE(lte_i_true2.evaluate().isTruthy());
    ASSERT_FALSE(lte_i_false2.evaluate().isTruthy());

    ObliviousExpression gte_i_true1 = ObliviousExpressionFactory::makeGTE(i_3, i_1);
    ObliviousExpression gte_i_true2 = ObliviousExpressionFactory::makeGTE(i_3, i_3);
    ObliviousExpression gte_i_false2 = ObliviousExpressionFactory::makeGTE(i_1, i_3);

    ASSERT_TRUE(gte_i_true1.evaluate().isTruthy());
    ASSERT_TRUE(gte_i_true2.evaluate().isTruthy());
    ASSERT_FALSE(gte_i_false2.evaluate().isTruthy());

}


TEST_F(ObliviousExpressionsTestFixture, Logical)
{
    ObliviousExpression and_true  = ObliviousExpressionFactory::makeAnd(ObliviousExpressionFactory::makeGTE(i_3, i_1), b_true);
    ObliviousExpression and_false1 = ObliviousExpressionFactory::makeAnd(ObliviousExpressionFactory::makeGTE(i_3, i_1), b_false);
    ObliviousExpression and_false2 = ObliviousExpressionFactory::makeAnd(b_false, ObliviousExpressionFactory::makeLTE(i_3, i_2));

    ASSERT_TRUE(and_true.evaluate().isTruthy());
    ASSERT_FALSE(and_false1.evaluate().isTruthy());
    ASSERT_FALSE(and_false2.evaluate().isTruthy());

    ObliviousExpression or_true1 = ObliviousExpressionFactory::makeOr(ObliviousExpressionFactory::makeGTE(i_3, i_1), b_true);
    ObliviousExpression or_true2 = ObliviousExpressionFactory::makeOr(ObliviousExpressionFactory::makeGTE(i_3, i_1), b_false);
    ObliviousExpression or_false = ObliviousExpressionFactory::makeOr(b_false, ObliviousExpressionFactory::makeLTE(i_3, i_2));

    ASSERT_TRUE(or_true1.evaluate().isTruthy());
    ASSERT_TRUE(or_true2.evaluate().isTruthy());
    ASSERT_FALSE(or_false.evaluate().isTruthy());

    ObliviousExpression not_true = ObliviousExpressionFactory::makeNot(and_false1);
    ObliviousExpression not_false = ObliviousExpressionFactory::makeNot(and_true);

    ASSERT_TRUE(not_true.evaluate().isTruthy());
    ASSERT_FALSE(not_false.evaluate().isTruthy());
}
