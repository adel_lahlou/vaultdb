#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include "operatorTestFixture.h"
#include <plaintext/plan/operators/Operator.h>
#include <plaintext/plan/operators/SeqScan.h>
#include <plaintext/plan/operators/Filter.h>
#include <plaintext/plan/operators/QuickSort.h>

using namespace plan::operators;
using namespace plan::expressions;

TEST_F(OperatorTestFixture, QuickSortConstruction)
{
    SeqScan * scan = new SeqScan(1, s1, nullptr, input1);
    QuickSort qsort(2, scan, std::vector<size_vdb>{0, 2}, SortDirection::Descending);

    ASSERT_EQ(qsort.getStatus(), OperatorStatus::NotInitialized);
    ASSERT_EQ(qsort.getChildren().size(), 1);
    ASSERT_TRUE(recordschema_equal(qsort.getSchema(), s1));
    ASSERT_TRUE(recordschema_equal(qsort.getChildSchema(0), s1));
    ASSERT_EQ(scan->getParent(), &qsort);
    ASSERT_EQ(qsort.getParent(), nullptr);
    ASSERT_TRUE(qsort.isBlocking());
}

TEST_F(OperatorTestFixture, QuickSortNextAndGet)
{
    SeqScan * scan1 = new SeqScan(1, s1, nullptr, input1);
    QuickSort qsort1(2, scan1, std::vector<size_vdb>{0, 2}, SortDirection::Descending);

    ASSERT_EQ(qsort1.getStatus(), OperatorStatus::NotInitialized);

    Tuple out;

    ASSERT_EQ(qsort1.next(), OperatorStatus::Ok);
    ASSERT_EQ(qsort1.getCurrentTuple(out), OperatorStatus::Ok);
    ASSERT_TRUE(tuples_equal(tuples[1], out));

    ASSERT_EQ(qsort1.next(), OperatorStatus::Ok);
    ASSERT_EQ(qsort1.getCurrentTuple(out), OperatorStatus::Ok);
    ASSERT_TRUE(tuples_equal(tuples[3], out));

    ASSERT_EQ(qsort1.next(), OperatorStatus::Ok);
    ASSERT_EQ(qsort1.getCurrentTuple(out), OperatorStatus::Ok);
    ASSERT_TRUE(tuples_equal(tuples[2], out));

    ASSERT_EQ(qsort1.next(), OperatorStatus::Ok);
    ASSERT_EQ(qsort1.getCurrentTuple(out), OperatorStatus::Ok);
    ASSERT_TRUE(tuples_equal(tuples[0], out));

    qsort1.reset();
    ASSERT_EQ(qsort1.next(), OperatorStatus::Ok);
    ASSERT_EQ(qsort1.getCurrentTuple(out), OperatorStatus::Ok);
    ASSERT_TRUE(tuples_equal(tuples[1], out));

    ASSERT_EQ(qsort1.next(), OperatorStatus::Ok);
    ASSERT_EQ(qsort1.getCurrentTuple(out), OperatorStatus::Ok);
    ASSERT_TRUE(tuples_equal(tuples[3], out));

    ASSERT_EQ(qsort1.next(), OperatorStatus::Ok);
    ASSERT_EQ(qsort1.getCurrentTuple(out), OperatorStatus::Ok);
    ASSERT_TRUE(tuples_equal(tuples[2], out));

    ASSERT_EQ(qsort1.next(), OperatorStatus::Ok);
    ASSERT_EQ(qsort1.getCurrentTuple(out), OperatorStatus::Ok);
    ASSERT_TRUE(tuples_equal(tuples[0], out));

    SeqScan * scan2 = new SeqScan(1, s1, nullptr, input1);
    QuickSort qsort2(2, scan2, std::vector<size_vdb>{0, 2}, SortDirection::Ascending);

    ASSERT_EQ(qsort2.getStatus(), OperatorStatus::NotInitialized);

    ASSERT_EQ(qsort2.next(), OperatorStatus::Ok);
    ASSERT_EQ(qsort2.getCurrentTuple(out), OperatorStatus::Ok);
    ASSERT_TRUE(tuples_equal(tuples[0], out));

    ASSERT_EQ(qsort2.next(), OperatorStatus::Ok);
    ASSERT_EQ(qsort2.getCurrentTuple(out), OperatorStatus::Ok);
    ASSERT_TRUE(tuples_equal(tuples[2], out));

    ASSERT_EQ(qsort2.next(), OperatorStatus::Ok);
    ASSERT_EQ(qsort2.getCurrentTuple(out), OperatorStatus::Ok);
    ASSERT_TRUE(tuples_equal(tuples[3], out));

    ASSERT_EQ(qsort2.next(), OperatorStatus::Ok);
    ASSERT_EQ(qsort2.getCurrentTuple(out), OperatorStatus::Ok);
    ASSERT_TRUE(tuples_equal(tuples[1], out));

    qsort2.reset();
    ASSERT_EQ(qsort2.next(), OperatorStatus::Ok);
    ASSERT_EQ(qsort2.getCurrentTuple(out), OperatorStatus::Ok);
    ASSERT_TRUE(tuples_equal(tuples[0], out));

    ASSERT_EQ(qsort2.next(), OperatorStatus::Ok);
    ASSERT_EQ(qsort2.getCurrentTuple(out), OperatorStatus::Ok);
    ASSERT_TRUE(tuples_equal(tuples[2], out));

    ASSERT_EQ(qsort2.next(), OperatorStatus::Ok);
    ASSERT_EQ(qsort2.getCurrentTuple(out), OperatorStatus::Ok);
    ASSERT_TRUE(tuples_equal(tuples[3], out));

    ASSERT_EQ(qsort2.next(), OperatorStatus::Ok);
    ASSERT_EQ(qsort2.getCurrentTuple(out), OperatorStatus::Ok);
    ASSERT_TRUE(tuples_equal(tuples[1], out));


    SeqScan * scan3 = new SeqScan(1, s1, nullptr, input2);
    QuickSort qsort3(2, scan3, std::vector<size_vdb>{0, 4}, SortDirection::Ascending);

    ASSERT_EQ(qsort3.getStatus(), OperatorStatus::NotInitialized);

    ASSERT_EQ(qsort3.next(), OperatorStatus::Ok);
    ASSERT_EQ(qsort3.getCurrentTuple(out), OperatorStatus::Ok);
    ASSERT_TRUE(tuples_equal(tuples2[3], out));

    ASSERT_EQ(qsort3.next(), OperatorStatus::Ok);
    ASSERT_EQ(qsort3.getCurrentTuple(out), OperatorStatus::Ok);
    ASSERT_TRUE(tuples_equal(tuples2[0], out) || tuples_equal(tuples2[5], out));
    // TODO: not Clear whether tests would catch doing the same one twice

    ASSERT_EQ(qsort3.next(), OperatorStatus::Ok);
    ASSERT_EQ(qsort3.getCurrentTuple(out), OperatorStatus::Ok);
    ASSERT_TRUE(tuples_equal(tuples2[0], out) || tuples_equal(tuples2[5], out));

    ASSERT_EQ(qsort3.next(), OperatorStatus::Ok);
    ASSERT_EQ(qsort3.getCurrentTuple(out), OperatorStatus::Ok);
    ASSERT_TRUE(tuples_equal(tuples2[1], out) || tuples_equal(tuples2[2], out));

    ASSERT_EQ(qsort3.next(), OperatorStatus::Ok);
    ASSERT_EQ(qsort3.getCurrentTuple(out), OperatorStatus::Ok);
    ASSERT_TRUE(tuples_equal(tuples2[1], out) || tuples_equal(tuples2[2], out));

    ASSERT_EQ(qsort3.next(), OperatorStatus::Ok);
    ASSERT_EQ(qsort3.getCurrentTuple(out), OperatorStatus::Ok);
    ASSERT_TRUE(tuples_equal(tuples2[4], out) || tuples_equal(tuples2[6], out));

    ASSERT_EQ(qsort3.next(), OperatorStatus::Ok);
    ASSERT_EQ(qsort3.getCurrentTuple(out), OperatorStatus::Ok);
    ASSERT_TRUE(tuples_equal(tuples2[4], out) || tuples_equal(tuples2[6], out));

    ASSERT_EQ(qsort3.next(), OperatorStatus::NoMoreTuples);
    ASSERT_EQ(qsort3.getCurrentTuple(out), OperatorStatus::NoMoreTuples);
    ASSERT_TRUE(tuples_equal(tuples2[4], out) || tuples_equal(tuples2[6], out));

    qsort3.reset();

    ASSERT_EQ(qsort3.getStatus(), OperatorStatus::NotInitialized);

    ASSERT_EQ(qsort3.next(), OperatorStatus::Ok);
    ASSERT_EQ(qsort3.getCurrentTuple(out), OperatorStatus::Ok);
    ASSERT_TRUE(tuples_equal(tuples2[3], out));

    ASSERT_EQ(qsort3.next(), OperatorStatus::Ok);
    ASSERT_EQ(qsort3.getCurrentTuple(out), OperatorStatus::Ok);
    ASSERT_TRUE(tuples_equal(tuples2[0], out) || tuples_equal(tuples2[5], out));
    // TODO: not Clear whether tests would catch doing the same one twice

    ASSERT_EQ(qsort3.next(), OperatorStatus::Ok);
    ASSERT_EQ(qsort3.getCurrentTuple(out), OperatorStatus::Ok);
    ASSERT_TRUE(tuples_equal(tuples2[0], out) || tuples_equal(tuples2[5], out));

    ASSERT_EQ(qsort3.next(), OperatorStatus::Ok);
    ASSERT_EQ(qsort3.getCurrentTuple(out), OperatorStatus::Ok);
    ASSERT_TRUE(tuples_equal(tuples2[1], out) || tuples_equal(tuples2[2], out));

    ASSERT_EQ(qsort3.next(), OperatorStatus::Ok);
    ASSERT_EQ(qsort3.getCurrentTuple(out), OperatorStatus::Ok);
    ASSERT_TRUE(tuples_equal(tuples2[1], out) || tuples_equal(tuples2[2], out));

    ASSERT_EQ(qsort3.next(), OperatorStatus::Ok);
    ASSERT_EQ(qsort3.getCurrentTuple(out), OperatorStatus::Ok);
    ASSERT_TRUE(tuples_equal(tuples2[4], out) || tuples_equal(tuples2[6], out));

    ASSERT_EQ(qsort3.next(), OperatorStatus::Ok);
    ASSERT_EQ(qsort3.getCurrentTuple(out), OperatorStatus::Ok);
    ASSERT_TRUE(tuples_equal(tuples2[4], out) || tuples_equal(tuples2[6], out));

    ASSERT_EQ(qsort3.next(), OperatorStatus::NoMoreTuples);
    ASSERT_EQ(qsort3.getCurrentTuple(out), OperatorStatus::NoMoreTuples);
    ASSERT_TRUE(tuples_equal(tuples2[4], out) || tuples_equal(tuples2[6], out));
}
