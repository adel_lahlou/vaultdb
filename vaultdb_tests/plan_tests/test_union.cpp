#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include "operatorTestFixture.h"
#include "plaintext/plan/operators/Operator.h"
#include "plaintext/plan/operators/SeqScan.h"
#include "plaintext/plan/operators/Union.h"

using namespace plan::operators;
using namespace plan::expressions;

TEST_F(OperatorTestFixture, UnionConstruction)
{
    SeqScan *scan1 = new SeqScan(1, s1, nullptr, input1);
    SeqScan *scan2 = new SeqScan(2, s1, nullptr, input2);
    Union aunion(3, OperatorPtrList {scan1, scan2});

    ASSERT_EQ(aunion.getStatus(), OperatorStatus::NotInitialized);
    ASSERT_EQ(aunion.getChildren().size(), 2);
    ASSERT_TRUE(recordschema_equal(aunion.getSchema(), s1));
    ASSERT_EQ(aunion.getParent(), nullptr);
    ASSERT_FALSE(aunion.isBlocking());
}

TEST_F(OperatorTestFixture, UnionNextAndGet)
{
    SeqScan *scan1 = new SeqScan(1, s1, nullptr, input1);
    SeqScan *scan2 = new SeqScan(2, s1, nullptr, input2);
    Union aunion(3, OperatorPtrList {scan1, scan2});

    ASSERT_EQ(aunion.getStatus(), OperatorStatus::NotInitialized);

    Tuple out;

    for(int i = 0; i < 3; ++i) {
        ASSERT_EQ(aunion.next(), OperatorStatus::Ok);
        ASSERT_EQ(aunion.getCurrentTuple(out), OperatorStatus::Ok);
        ASSERT_TRUE(tuples_equal(tuples[0], out));

        ASSERT_EQ(aunion.next(), OperatorStatus::Ok);
        ASSERT_EQ(aunion.getCurrentTuple(out), OperatorStatus::Ok);
        ASSERT_TRUE(tuples_equal(tuples[1], out));

        ASSERT_EQ(aunion.next(), OperatorStatus::Ok);
        ASSERT_EQ(aunion.getCurrentTuple(out), OperatorStatus::Ok);
        ASSERT_TRUE(tuples_equal(tuples[2], out));

        ASSERT_EQ(aunion.next(), OperatorStatus::Ok);
        ASSERT_EQ(aunion.getCurrentTuple(out), OperatorStatus::Ok);
        ASSERT_TRUE(tuples_equal(tuples[3], out));

        ASSERT_EQ(aunion.next(), OperatorStatus::Ok);
        ASSERT_EQ(aunion.getCurrentTuple(out), OperatorStatus::Ok);
        ASSERT_TRUE(tuples_equal(tuples2[0], out));

        ASSERT_EQ(aunion.next(), OperatorStatus::Ok);
        ASSERT_EQ(aunion.getCurrentTuple(out), OperatorStatus::Ok);
        ASSERT_TRUE(tuples_equal(tuples2[1], out));

        ASSERT_EQ(aunion.next(), OperatorStatus::Ok);
        ASSERT_EQ(aunion.getCurrentTuple(out), OperatorStatus::Ok);
        ASSERT_TRUE(tuples_equal(tuples2[2], out));

        ASSERT_EQ(aunion.next(), OperatorStatus::Ok);
        ASSERT_EQ(aunion.getCurrentTuple(out), OperatorStatus::Ok);
        ASSERT_TRUE(tuples_equal(tuples2[3], out));

        ASSERT_EQ(aunion.next(), OperatorStatus::Ok);
        ASSERT_EQ(aunion.getCurrentTuple(out), OperatorStatus::Ok);
        ASSERT_TRUE(tuples_equal(tuples2[4], out));

        ASSERT_EQ(aunion.next(), OperatorStatus::Ok);
        ASSERT_EQ(aunion.getCurrentTuple(out), OperatorStatus::Ok);
        ASSERT_TRUE(tuples_equal(tuples2[5], out));

        ASSERT_EQ(aunion.next(), OperatorStatus::Ok);
        ASSERT_EQ(aunion.getCurrentTuple(out), OperatorStatus::Ok);
        ASSERT_TRUE(tuples_equal(tuples2[6], out));

        ASSERT_EQ(aunion.next(), OperatorStatus::NoMoreTuples);
        ASSERT_EQ(aunion.getCurrentTuple(out), OperatorStatus::NoMoreTuples);
        ASSERT_TRUE(tuples_equal(tuples2[6], out));

        ASSERT_EQ(aunion.next(), OperatorStatus::NoMoreTuples);
        ASSERT_EQ(aunion.getCurrentTuple(out), OperatorStatus::NoMoreTuples);
        ASSERT_TRUE(tuples_equal(tuples2[6], out));

        aunion.reset();
    }
}