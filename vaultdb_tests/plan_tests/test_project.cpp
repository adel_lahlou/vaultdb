#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "operatorTestFixture.h"
#include "plaintext/plan/operators/Project.h"
#include "plaintext/plan/operators/SeqScan.h"

using namespace plan::operators;
using namespace plan::expressions;
using namespace db::data::field;
using namespace db::data;

TEST_F(OperatorTestFixture, ProjectConstruction){
    ExpressionList exps = {
            Expression::makeColumn(3),
            Expression::makeColumn(0),
            Expression::makeColumn(1),
            Expression::makeAdd(
                    Expression::makeLiteral(Field::makeIntField(15)),
                    Expression::makeColumn(1)
            )
    };

    SeqScan *scan = new SeqScan(1, s1, nullptr, input1);
    RecordSchema outSchema(
            1, {
                    s1[3], s1[0], s1[1], s1[1]
            });
    Project project(2, scan, exps);

    ASSERT_EQ(project.getStatus(), OperatorStatus::NotInitialized);
    EXPECT_EQ(project.getChildren().size(), 1);
    ASSERT_TRUE(recordschema_equal(project.getSchema(), outSchema));
    ASSERT_EQ(project.getParent(), nullptr);
    ASSERT_FALSE(project.isBlocking());
}


TEST_F(OperatorTestFixture, ProjectNextAndGet) {
    ExpressionList exps = {
            Expression::makeColumn(3),
            Expression::makeColumn(0),
            Expression::makeColumn(1),
            Expression::makeAdd(
                    Expression::makeLiteral(Field::makeIntField(15)),
                    Expression::makeColumn(1)
            )
    };

    SeqScan *scan = new SeqScan(1, s1, nullptr, input1);
    RecordSchema outSchema(
            1, {
                    s1[3], s1[0], s1[1], s1[1]
            });
    Project project(2, scan, exps);

    ASSERT_EQ(project.getStatus(), OperatorStatus::NotInitialized);

    // Field added = Field::makeIntField(15);

    Tuple out;

    for(int i = 0; i < 3; ++i) {
        ASSERT_EQ(project.next(), OperatorStatus::Ok);
        ASSERT_EQ(project.getCurrentTuple(out), OperatorStatus::Ok);
        ASSERT_TRUE(tuples_equal(out, Tuple {
                tuples[0][3],
                tuples[0][0],
                tuples[0][1],
                tuples[0][1] + Field::makeIntField(15)
        }));

        ASSERT_EQ(project.next(), OperatorStatus::Ok);
        ASSERT_EQ(project.getCurrentTuple(out), OperatorStatus::Ok);
        ASSERT_TRUE(tuples_equal(out, Tuple {
                tuples[1][3],
                tuples[1][0],
                tuples[1][1],
                tuples[1][1] + Field::makeIntField(15)
        }));


        ASSERT_EQ(project.next(), OperatorStatus::Ok);
        ASSERT_EQ(project.getCurrentTuple(out), OperatorStatus::Ok);
        ASSERT_TRUE(tuples_equal(out, Tuple {
                tuples[2][3],
                tuples[2][0],
                tuples[2][1],
                tuples[2][1] + Field::makeIntField(15)
        }));


        ASSERT_EQ(project.next(), OperatorStatus::Ok);
        ASSERT_EQ(project.getCurrentTuple(out), OperatorStatus::Ok);
        ASSERT_TRUE(tuples_equal(out, Tuple {
                tuples[3][3],
                tuples[3][0],
                tuples[3][1],
                tuples[3][1] + Field::makeIntField(15)
        }));

        project.reset();
    }
}