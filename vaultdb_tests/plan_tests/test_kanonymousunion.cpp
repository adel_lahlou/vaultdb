#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <in_sgx/plan/kaoperators/KAnonymousSeqScan.h>
#include <in_sgx/plan/kaoperators/KAnonymousUnion.h>
#include <debug/DebugInSGXController.h>
#include <debug/DebugFakeLocalDataMover.h>

#include "kAnonymousOperatorTestFixture.h"

//#define KANO_UNION_PRINT_TUPLES

using namespace plan::kaoperators;
using namespace plan::obexpressions;

TEST_F(KAnonymousOperatorTestFixture, KAnonymousUnionParseAndRun) {
    size_vdb k = 2;
    size_vdb expectedDummyCount = 0;
    ObliviousTupleList expectedTuples {
//            R - 4,3,"rogue"
//            R - 1,3,"peace3"
//            R - 3,1,"tornado"
//            R - 3,2,"giant"
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousFixcharField("rogue", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousFixcharField("peace3", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("tornado", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("giant", vc_size),
            }),
    };

    ObliviousTupleTable t1 (
            type::RecordSchema((TableID) 11, {
                    {type::FieldDataType::Int, sizeof(int)},
                    {type::FieldDataType::Int, sizeof(int)},
                    {type::FieldDataType::Fixchar, vc_size},
            }), {
                    ObliviousTuple(false, {
                            ObliviousField::makeObliviousIntField(4),
                            ObliviousField::makeObliviousIntField(3),
                            ObliviousField::makeObliviousFixcharField("rogue", vc_size),
                    }),
                    ObliviousTuple(false, {
                            ObliviousField::makeObliviousIntField(1),
                            ObliviousField::makeObliviousIntField(3),
                            ObliviousField::makeObliviousFixcharField("peace3", vc_size),
                    }),
            }
    );

    ObliviousTupleTable t2 (
            type::RecordSchema((TableID) 12, {
                    {type::FieldDataType::Int, sizeof(int)},
                    {type::FieldDataType::Int, sizeof(int)},
                    {type::FieldDataType::Fixchar, vc_size},
            }), {
                    ObliviousTuple(false, {
                            ObliviousField::makeObliviousIntField(3),
                            ObliviousField::makeObliviousIntField(1),
                            ObliviousField::makeObliviousFixcharField("tornado", vc_size),
                    }),
            }
    );

    ObliviousTupleTable t3 (
            type::RecordSchema((TableID) 13, {
                    {type::FieldDataType::Int, sizeof(int)},
                    {type::FieldDataType::Int, sizeof(int)},
                    {type::FieldDataType::Fixchar, vc_size},
            }), {
                    ObliviousTuple(false, {
                            ObliviousField::makeObliviousIntField(3),
                            ObliviousField::makeObliviousIntField(2),
                            ObliviousField::makeObliviousFixcharField("giant", vc_size),
                    }),
            }
    );


    KAnonymousSeqScan * scan1 = new KAnonymousSeqScan(0, k, & t1);
    KAnonymousSeqScan * scan2 = new KAnonymousSeqScan(1, k, & t2);
    KAnonymousSeqScan * scan3 = new KAnonymousSeqScan(2, k, & t3);
    KAnonymousUnion union_(3, k, {scan1, scan2, scan3});

    utilities::DebugInSGXController m(1, 1);
    m.collectTablePointer(&t1);
    m.collectTablePointer(&t2);
    m.collectTablePointer(&t3);

    auto parsedUnion = m.encodeKAnonymousOperatorAndParse(&union_);

    ObliviousTuple out;
    int pos = 0;
    size_vdb dummyCount = 0;
#ifdef KANO_UNION_PRINT_TUPLES
    printf("\n");
#endif
    while (parsedUnion->next() == OperatorStatus::Ok) {
        parsedUnion->getCurrentTuple(out);
        if (out.isDummy()) {
#ifdef KANO_UNION_PRINT_TUPLES
            printf("Dummy: %s\n", out.toString().c_str());
#endif
            dummyCount++;
        } else if (pos >= expectedTuples.size()) {
#ifdef KANO_UNION_PRINT_TUPLES
            printf("Expected / actual: (none) / %s\n", out.toString().c_str());
#endif
        } else {
            ObliviousTuple t  = expectedTuples[pos];
#ifdef KANO_UNION_PRINT_TUPLES
            printf("Expected / actual: %s / %s\n", t.toString().c_str(), out.toString().c_str());
#endif
            ASSERT_TRUE(oblivious_tuples_equal(out, t));
        }
        pos++;
    }
    ASSERT_EQ(parsedUnion->next(), OperatorStatus::NoMoreTuples);
    ASSERT_EQ(expectedDummyCount, dummyCount);
    utilities::DebugFakeLocalDataMover::instance()->clear();
}

TEST_F(KAnonymousOperatorTestFixture, KAnonymousUnionEmpty) {
    size_vdb k = 2;
    size_vdb expectedDummyCount = 2;
    ObliviousTupleList expectedTuples {
    };

    ObliviousTupleTable t1 (
            type::RecordSchema((TableID) 11, {
                    {type::FieldDataType::Int, sizeof(int)},
                    {type::FieldDataType::Int, sizeof(int)},
                    {type::FieldDataType::Fixchar, vc_size},
            }), {
            }
    );

    ObliviousTupleTable t2 (
            type::RecordSchema((TableID) 12, {
                    {type::FieldDataType::Int, sizeof(int)},
                    {type::FieldDataType::Int, sizeof(int)},
                    {type::FieldDataType::Fixchar, vc_size},
            }), {
            }
    );

    ObliviousTupleTable t3 (
            type::RecordSchema((TableID) 13, {
                    {type::FieldDataType::Int, sizeof(int)},
                    {type::FieldDataType::Int, sizeof(int)},
                    {type::FieldDataType::Fixchar, vc_size},
            }), {
            }
    );


    KAnonymousSeqScan * scan1 = new KAnonymousSeqScan(0, k, & t1);
    KAnonymousSeqScan * scan2 = new KAnonymousSeqScan(1, k, & t2);
    KAnonymousSeqScan * scan3 = new KAnonymousSeqScan(2, k, & t3);
    KAnonymousUnion union_(3, k, {scan1, scan2, scan3});

    utilities::DebugInSGXController m(1, 1);
    m.collectTablePointer(&t1);
    m.collectTablePointer(&t2);
    m.collectTablePointer(&t3);

    auto parsedUnion = m.encodeKAnonymousOperatorAndParse(&union_);

    ObliviousTuple out;
    int pos = 0;
    size_vdb dummyCount = 0;
#ifdef KANO_UNION_PRINT_TUPLES
    printf("\n");
#endif
    while (parsedUnion->next() == OperatorStatus::Ok) {
        parsedUnion->getCurrentTuple(out);
        if (out.isDummy()) {
#ifdef KANO_UNION_PRINT_TUPLES
            printf("Dummy: %s\n", out.toString().c_str());
#endif
            dummyCount++;
        } else if (pos >= expectedTuples.size()) {
#ifdef KANO_UNION_PRINT_TUPLES
            printf("Expected / actual: (none) / %s\n", out.toString().c_str());
#endif
        } else {
            ObliviousTuple t  = expectedTuples[pos];
#ifdef KANO_UNION_PRINT_TUPLES
            printf("Expected / actual: %s / %s\n", t.toString().c_str(), out.toString().c_str());
#endif
            ASSERT_TRUE(oblivious_tuples_equal(out, t));
        }
        pos++;
    }
    ASSERT_EQ(parsedUnion->next(), OperatorStatus::NoMoreTuples);
    ASSERT_EQ(expectedDummyCount, dummyCount);
    utilities::DebugFakeLocalDataMover::instance()->clear();
}