#ifndef VAULTDB_KANONYMOUSOPERATORTESTFIXTURE_H
#define VAULTDB_KANONYMOUSOPERATORTESTFIXTURE_H

#ifndef PRINT_KANO_OPERATOR_OUTPUT
//#define PRINT_KANO_OPERATOR_OUTPUT
#endif

#ifndef VERIFY_KANO_OPERATOR_OUTPUT
#define VERIFY_KANO_OPERATOR_OUTPUT
#endif

#include <gtest/gtest.h>
#include <in_sgx/obdata/ObliviousTuple.h>
#include <in_sgx/obdata/ObliviousTupleTable.h>

#include <plaintext/data/Tuple.h>
#include <plaintext/data/TupleTable.h>

#include <shared/type/RecordSchema.h>
#include "in_sgx/plan/kaoperators/KAnonymousOperator.h"

using namespace db::data;
using namespace db::data::field;
using namespace db::obdata;
using namespace db::obdata::obfield;
using namespace type;
using namespace plan::kaoperators;

bool static recordschema_equal(const RecordSchema& s1, const RecordSchema& s2)
{
    if(s1.size() != s2.size())
        return false;

    for(int i = 0; i < s1.size(); ++i){
        if(s1[i].type != s2[i].type || s1[i].size != s2[i].size)
            return false;
    }

    return true;
}


bool static oblivious_tuples_equal(const ObliviousTuple& t1, const ObliviousTuple& t2)
{
    if(t1.size() != t2.size())
        return false;

    for(int i = 0; i < t1.size(); ++i){
        if(!(t1[i] == t2[i]))
            return false;
    }

    return true;
}

class KAnonymousOperatorTestFixture : public ::testing::Test {
protected:
    static db::obdata::obfield::ObliviousField makeObliviousTimestampNoZoneFieldFromString(const char * data) {
        if (strlen(data) < 19) {
            throw std::length_error("Input char array too short");
        }
        tm tm1;
        sscanf(data,"%04d-%02d-%2d %02d:%02d:%02d",&tm1.tm_year,&tm1.tm_mon,&tm1.tm_mday,
               &tm1.tm_hour,&tm1.tm_min,&tm1.tm_sec);
        tm1.tm_year -= 1900;
        tm1.tm_mon -= 1;
        tm1.tm_isdst = 0;
        return db::obdata::obfield::ObliviousField::makeObliviousTimestampNoZoneField(timegm(&tm1));
    };

    static void verify_output(KAnonymousOperator * op, ObliviousTupleList * expectedTuples) {
        ObliviousTuple out;
#ifdef PRINT_KANO_OPERATOR_OUTPUT
        printf("\n");
#endif
        pos_vdb i = 0;
        size_vdb esize = (size_vdb)expectedTuples->size();
        while (op->next() == OperatorStatus::Ok) {
            op->getCurrentTuple(out);
            if (out.isDummy()) {
#ifdef PRINT_KANO_OPERATOR_OUTPUT
                printf("Produced a dummy tuple: %s\n", out.toString().c_str());
#endif
            } else if (i >= esize) {
#ifdef PRINT_KANO_OPERATOR_OUTPUT
                printf("Expected / Actual: (none) / %s\n", out.toString().c_str());
#endif
            } else {
#ifdef PRINT_KANO_OPERATOR_OUTPUT
                printf("Expected / Actual: %s / %s\n", (*expectedTuples)[i].toString().c_str(), out.toString().c_str());
#endif
#ifdef VERIFY_KANO_OPERATOR_OUTPUT
                ASSERT_TRUE(oblivious_tuples_equal((*expectedTuples)[i], out));
#endif
                i++;
            }
        }
#ifdef VERIFY_KANO_OPERATOR_OUTPUT
        ASSERT_TRUE(op->next() == OperatorStatus::NoMoreTuples);
#endif
    }
    KAnonymousOperatorTestFixture()
            : oinput1(s1, otuples1),
              oinput2(s1, otuples2),
              oinput3(s3, otuples3),
              oinput4(s3, otuples4),
              oinput5(s3, otuples5),
              oinputEmpty(s3, otuplesEmpty)
    {}

    ~KAnonymousOperatorTestFixture() {}

    size_vdb vc_size = 10;

    type::RecordSchema s1{
            (TableID) 1, {
                    {type::FieldDataType::Int, sizeof(int)},
                    {type::FieldDataType::Int, sizeof(int)},
                    {type::FieldDataType::Int, sizeof(int)},
                    {type::FieldDataType::Fixchar, vc_size},
                    {type::FieldDataType::Fixchar, vc_size},
            }
    };

    type::RecordSchema s3{
            (TableID) 3, {
                    {type::FieldDataType::Int, sizeof(int)},
                    {type::FieldDataType::Int, sizeof(int)},
                    {type::FieldDataType::Fixchar, vc_size},
            }
    };

    ObliviousTupleList otuples1 {
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousFixcharField("hello", vc_size),
                    ObliviousField::makeObliviousFixcharField("world", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousIntField(6),
                    ObliviousField::makeObliviousFixcharField("foo", vc_size),
                    ObliviousField::makeObliviousFixcharField("bar", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousFixcharField("bigger", vc_size),
                    ObliviousField::makeObliviousFixcharField("better", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(6),
                    ObliviousField::makeObliviousFixcharField("secure", vc_size),
                    ObliviousField::makeObliviousFixcharField("execution", vc_size),
            }),
    };

    ObliviousTupleList otuples2 {
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousFixcharField("hello", vc_size),
                    ObliviousField::makeObliviousFixcharField("world", vc_size),
            }),

            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousFixcharField("hello", vc_size),
                    ObliviousField::makeObliviousFixcharField("bar", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousIntField(6),
                    ObliviousField::makeObliviousFixcharField("foo", vc_size),
                    ObliviousField::makeObliviousFixcharField("bar", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousFixcharField("bigger", vc_size),
                    ObliviousField::makeObliviousFixcharField("better", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(6),
                    ObliviousField::makeObliviousFixcharField("secure", vc_size),
                    ObliviousField::makeObliviousFixcharField("execution", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousFixcharField("hello", vc_size),
                    ObliviousField::makeObliviousFixcharField("world", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(6),
                    ObliviousField::makeObliviousFixcharField("secure", vc_size),
                    ObliviousField::makeObliviousFixcharField("execution", vc_size),
            }),
    };

    ObliviousTupleList otuples3 {
//            1,3,"peace"
//            4,1,"fire pit"
//            4,1,"rogue"
//            1,2,"lasting"
//            1,3,"peace"
//            1,3,"peace"
//            2,1,"smart"
//            3,1,"tornado"
//            3,2,"giant"
//            4,1,"chimney"
//            1,1,"world"
//            1,2,"better"
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousFixcharField("peace", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("fire pit", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("rogue", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("lasting", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousFixcharField("peace", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousFixcharField("peace", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("smart", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("tornado", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("giant", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("chimney", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("world", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("better", vc_size),
            }),
    };


    ObliviousTupleList otuples4 {
//            D - 1,3,"peace1"
//            D - 4,6,"fire pit"
//            R - 4,3,"rogue"
//            D - 1,2,"lasting"
//            D - 1,3,"peace2"
//            R - 1,3,"peace3"
//            D - 2,1,"smart"
//            R - 3,1,"tornado"
//            R - 3,2,"giant"
//            D - 4,2,"chimney"
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousFixcharField("peace1", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(6),
                    ObliviousField::makeObliviousFixcharField("fire pit", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousFixcharField("rogue", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("lasting", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousFixcharField("peace2", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousFixcharField("peace3", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("smart", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("tornado", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("giant", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("chimney", vc_size),
            }),
    };

    ObliviousTupleList otuples5 {
//            D - 1,3,"peace1"
//            D - 4,6,"fire pit"
//            R - 4,3,"rogue"
//            D - 1,2,"lasting"
//            D - 1,3,"peace2"
//            R - 1,3,"peace3"
//            D - 2,1,"smart"
//            R - 3,1,"tornado"
//            R - 3,2,"giant"
//            D - 4,2,"chimney"
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousFixcharField("peace1", vc_size),
            }),
//            ObliviousTuple(true, {
//                    ObliviousField::makeObliviousIntField(4),
//                    ObliviousField::makeObliviousIntField(6),
//                    ObliviousField::makeObliviousFixcharField("fire pit", vc_size),
//            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousFixcharField("peass2", vc_size),
            }),
//            ObliviousTuple(true, {
//                    ObliviousField::makeObliviousIntField(1),
//                    ObliviousField::makeObliviousIntField(2),
//                    ObliviousField::makeObliviousFixcharField("lasting", vc_size),
//            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousFixcharField("peace2", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousFixcharField("peace3", vc_size),
            }),
            ObliviousTuple(true, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("peass3", vc_size),
            }),
//            ObliviousTuple(true, {
//                    ObliviousField::makeObliviousIntField(3),
//                    ObliviousField::makeObliviousIntField(1),
//                    ObliviousField::makeObliviousFixcharField("tornado", vc_size),
//            }),
//            ObliviousTuple(true, {
//                    ObliviousField::makeObliviousIntField(3),
//                    ObliviousField::makeObliviousIntField(2),
//                    ObliviousField::makeObliviousFixcharField("giant", vc_size),
//            }),
//            ObliviousTuple(true, {
//                    ObliviousField::makeObliviousIntField(4),
//                    ObliviousField::makeObliviousIntField(2),
//                    ObliviousField::makeObliviousFixcharField("chimney", vc_size),
//            }),
    };

    ObliviousTupleList otuplesEmpty {
    };

    ObliviousTupleTable oinput1;
    ObliviousTupleTable oinput2;
    ObliviousTupleTable oinput3;
    ObliviousTupleTable oinput4;
    ObliviousTupleTable oinput5;
    ObliviousTupleTable oinputEmpty;
};

#endif //VAULTDB_KANONYMOUSOPERATORTESTFIXTURE_H
