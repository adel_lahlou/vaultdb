#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <in_sgx/plan/kaoperators/KAnonymousSeqScan.h>
#include <in_sgx/plan/kaoperators/KAnonymousLocalGenFilter.h>
#include <in_sgx/plan/obexpressions/ObliviousExpressionFactory.h>
#include <debug/DebugInSGXController.h>
#include <debug/DebugFakeLocalDataMover.h>
#include <utilities/TuplePrinter.h>

#include "kAnonymousOperatorTestFixture.h"

#ifndef KANO_GEN_FILTER_PRINT_TUPLES
//#define KANO_GEN_FILTER_PRINT_TUPLES
#endif

#ifndef KANO_GEN_FILTER_VERIFY
#define KANO_GEN_FILTER_VERIFY
#endif

using namespace plan::kaoperators;
using namespace plan::obexpressions;


TEST_F(KAnonymousOperatorTestFixture, KAnonymousGenFilterHigherLevel) {

    size_vdb k = 2;
    size_vdb expectedDummyCount = 2;
    ObliviousTupleList expectedTuples {
//            D - 1,3,"peace1"
//            D - 1,3,"peace2"
//            R - 1,3,"peace3"
//            ObliviousTuple(true, {
//                    ObliviousField::makeObliviousIntField(1),
//                    ObliviousField::makeObliviousIntField(3),
//                    ObliviousField::makeObliviousFixcharField("peace1", vc_size),
//            }),
//            ObliviousTuple(true, {
//                    ObliviousField::makeObliviousIntField(1),
//                    ObliviousField::makeObliviousIntField(3),
//                    ObliviousField::makeObliviousFixcharField("peace2", vc_size),
//            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousFixcharField("peace3", vc_size),
            }),
    };

    ObliviousExpression ob1 = ObliviousExpressionFactory::makeEq(
            ObliviousExpressionFactory::makeColumn(2, type::SchemaColumn{type::FieldDataType::Fixchar, vc_size}),
            ObliviousExpressionFactory::makeLiteral(ObliviousField::makeObliviousFixcharField("peace3", vc_size), vc_size)
    );
    ObliviousExpression ob2 = ObliviousExpressionFactory::makeEq(
            ObliviousExpressionFactory::makeColumn(2, type::SchemaColumn{type::FieldDataType::Fixchar, vc_size}),
            ObliviousExpressionFactory::makeLiteral(ObliviousField::makeObliviousFixcharField("peace3", vc_size), vc_size)
    );

    utilities::DebugInSGXController m(1, 1);

    KAnonymousSeqScan * scan = new KAnonymousSeqScan(0, k, & oinput5);
    KAnonymousLocalGenFilter genFilterLevel(1, k, scan, ob1,
                                            ob2, {0}, {}, &m);


#ifdef KANO_GEN_FILTER_PRINT_TUPLES
    printf("\n");
#endif
    pos_vdb expectedPos = 0;
    size_vdb expectedSize = (size_vdb)expectedTuples.size();
    ObliviousTuple out;
    size_vdb dummyCount = 0;
    while (genFilterLevel.next() == OperatorStatus::Ok) {
        genFilterLevel.getCurrentTuple(out);
        if(out.isDummy()) {
#ifdef KANO_GEN_FILTER_PRINT_TUPLES
            printf("Dummy tuple: %s\n", out.toString().c_str());
#endif
            dummyCount++;
        } else if (expectedPos < expectedSize) {
#ifdef KANO_GEN_FILTER_PRINT_TUPLES
            printf("Expected / Actual: %s / %s\n", expectedTuples[expectedPos].toString().c_str(), out.toString().c_str());
#endif
#ifdef KANO_GEN_FILTER_VERIFY
            ASSERT_TRUE(oblivious_tuples_equal(expectedTuples[expectedPos], out));
#endif
            expectedPos++;
        } else {
#ifdef KANO_GEN_FILTER_PRINT_TUPLES
            printf("Expected / Actual: (none) / %s\n", out.toString().c_str());
#endif
        }
    }
#ifdef KANO_GEN_FILTER_VERIFY
    ASSERT_EQ(genFilterLevel.next(), OperatorStatus::NoMoreTuples);
    ASSERT_EQ(expectedDummyCount, dummyCount);
#endif
    genFilterLevel.reset();
    utilities::DebugFakeLocalDataMover::instance()->clear();
}


TEST_F(KAnonymousOperatorTestFixture, NestedArithmics) {

    size_vdb k = 2;
    auto filterPred = plan::obexpressions::ObliviousExpressionFactory::makeGTE(
                                plan::obexpressions::ObliviousExpressionFactory::makeSub(
                                        plan::obexpressions::ObliviousExpressionFactory::makeColumn(4, {type::FieldDataType::TimestampNoZone, sizeof(time_t)}),
                                        plan::obexpressions::ObliviousExpressionFactory::makeColumn(1, {type::FieldDataType::TimestampNoZone, sizeof(time_t)})
                                ),
                                plan::obexpressions::ObliviousExpressionFactory::makeLiteral(
                                        db::obdata::obfield::ObliviousField::makeObliviousIntField(15 * 3600 * 24), sizeof(int))
                        );

    type::RecordSchema schema (1, {
            {type::FieldDataType::Int, sizeof(int)},
            {type::FieldDataType::TimestampNoZone, sizeof(time_t)},
            {type::FieldDataType::Int, sizeof(int)},
            {type::FieldDataType::Int, sizeof(int)},
            {type::FieldDataType::TimestampNoZone, sizeof(time_t)},
            {type::FieldDataType::Int, sizeof(int)},
    });

    db::obdata::ObliviousTupleTable t1 (
            schema,
            {
                db::obdata::ObliviousTuple(false, {
                      ObliviousField::makeObliviousIntField(123),
                      makeObliviousTimestampNoZoneFieldFromString("2004-06-24 00:00:00"),
                      ObliviousField::makeObliviousIntField(1),
                      ObliviousField::makeObliviousIntField(123),
                      makeObliviousTimestampNoZoneFieldFromString("2004-06-25 00:00:00"),
                      ObliviousField::makeObliviousIntField(0),
                }),
                db::obdata::ObliviousTuple(false, {
                      ObliviousField::makeObliviousIntField(123),
                      makeObliviousTimestampNoZoneFieldFromString("2004-06-24 00:00:00"),
                      ObliviousField::makeObliviousIntField(1),
                      ObliviousField::makeObliviousIntField(123),
                      makeObliviousTimestampNoZoneFieldFromString("2006-09-24 00:00:00"),
                      ObliviousField::makeObliviousIntField(0),
                }),
            });

    ObliviousTupleList expectedTuples {
            db::obdata::ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(123),
                    makeObliviousTimestampNoZoneFieldFromString("2004-06-24 00:00:00"),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(123),
                    makeObliviousTimestampNoZoneFieldFromString("2006-09-24 00:00:00"),
                    ObliviousField::makeObliviousIntField(0),
            }),
    };

    utilities::DebugInSGXController m(1, 1);

    m.collectTablePointer(&t1);

    KAnonymousSeqScan * scan = new KAnonymousSeqScan(0, k, & t1);
    KAnonymousLocalGenFilter genFilterLevel(1, k, scan, filterPred, filterPred, {0}, {}, nullptr);

    size_vdb expectedDummyCount = 1;
    auto filter = m.encodeKAnonymousOperatorAndParse(&genFilterLevel);


#ifdef KANO_GEN_FILTER_PRINT_TUPLES
    printf("\n");
#endif
    pos_vdb expectedPos = 0;
    size_vdb expectedSize = (size_vdb)expectedTuples.size();
    ObliviousTuple out;
    size_vdb dummyCount = 0;
    while (filter->next() == OperatorStatus::Ok) {
        filter->getCurrentTuple(out);
        if(out.isDummy()) {
#ifdef KANO_GEN_FILTER_PRINT_TUPLES
            printf("Dummy tuple: '%s'\n", TuplePrinter::convert(&schema, out).c_str());
#endif
            dummyCount++;
        } else if (expectedPos < expectedSize) {
#ifdef KANO_GEN_FILTER_PRINT_TUPLES
            printf("Expected / Actual: '%s' / '%s'\n", TuplePrinter::convert(&schema, expectedTuples[expectedPos]).c_str(),
                   TuplePrinter::convert(&schema, out).c_str());
#endif
#ifdef KANO_GEN_FILTER_VERIFY
            ASSERT_TRUE(oblivious_tuples_equal(expectedTuples[expectedPos], out));
#endif
            expectedPos++;
        } else {
#ifdef KANO_GEN_FILTER_PRINT_TUPLES
            printf("Expected / Actual: (none) / '%s'\n", TuplePrinter::convert(&schema, out).c_str());
#endif
        }
    }
#ifdef KANO_GEN_FILTER_VERIFY
    ASSERT_EQ(expectedPos, expectedTuples.size());
    ASSERT_EQ(filter->next(), OperatorStatus::NoMoreTuples);
    ASSERT_EQ(expectedDummyCount, dummyCount);
#endif
    utilities::DebugFakeLocalDataMover::instance()->clear();
}