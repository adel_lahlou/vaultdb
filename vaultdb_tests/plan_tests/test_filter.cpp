#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include "operatorTestFixture.h"
#include <plaintext/plan/operators/Operator.h>
#include <plaintext/plan/operators/SeqScan.h>
#include <plaintext/plan/operators/Filter.h>

using namespace plan::operators;
using namespace plan::expressions;

TEST_F(OperatorTestFixture, FilterConstruction)
{
    Expression filterPred =
            Expression::makeEq(
                    Expression::makeAdd(
                            Expression::makeColumn(0),
                            Expression::makeLiteral(Field::makeIntField(1))),
                    Expression::makeLiteral(Field::makeIntField(5)));
    SeqScan * scan = new SeqScan(1, s1, nullptr, input1);
    Filter filter(2, s1, nullptr, scan, filterPred);

    ASSERT_EQ(filter.getStatus(), OperatorStatus::NotInitialized);
    ASSERT_EQ(filter.getChildren().size(), 1);
    ASSERT_TRUE(recordschema_equal(filter.getSchema(), s1));
    ASSERT_TRUE(recordschema_equal(filter.getChildSchema(0), s1));
    ASSERT_EQ(scan->getParent(), &filter);
    ASSERT_EQ(filter.getParent(), nullptr);
    ASSERT_FALSE(filter.isBlocking());
}

TEST_F(OperatorTestFixture, FilterNextAndGet)
{
    Expression filterPred =
            Expression::makeEq(
                    Expression::makeAdd(
                            Expression::makeColumn(0),
                            Expression::makeLiteral(Field::makeIntField(1))),
                    Expression::makeLiteral(Field::makeIntField(5)));
    SeqScan * scan = new SeqScan(1, s1, nullptr, input1);
    Filter filter(2, s1, nullptr, scan, filterPred);

    ASSERT_EQ(filter.getStatus(), OperatorStatus::NotInitialized);

    Tuple out;

    ASSERT_EQ(filter.next(), OperatorStatus::Ok);
    ASSERT_EQ(filter.getCurrentTuple(out), OperatorStatus::Ok);
    ASSERT_TRUE(tuples_equal(tuples[1], out));

    ASSERT_EQ(filter.next(), OperatorStatus::Ok);
    ASSERT_EQ(filter.getCurrentTuple(out), OperatorStatus::Ok);
    ASSERT_TRUE(tuples_equal(tuples[3], out));

    ASSERT_EQ(filter.next(), OperatorStatus::NoMoreTuples);
    ASSERT_EQ(filter.getCurrentTuple(out), OperatorStatus::NoMoreTuples);
    ASSERT_TRUE(tuples_equal(tuples[3], out));

    ASSERT_EQ(filter.next(), OperatorStatus::NoMoreTuples);
    ASSERT_EQ(filter.getCurrentTuple(out), OperatorStatus::NoMoreTuples);
    ASSERT_TRUE(tuples_equal(tuples[3], out));

    filter.reset();
    ASSERT_EQ(filter.getStatus(), OperatorStatus::NotInitialized);

    ASSERT_EQ(filter.next(), OperatorStatus::Ok);
    ASSERT_EQ(filter.getCurrentTuple(out), OperatorStatus::Ok);
    ASSERT_TRUE(tuples_equal(tuples[1], out));

    ASSERT_EQ(filter.next(), OperatorStatus::Ok);
    ASSERT_EQ(filter.getCurrentTuple(out), OperatorStatus::Ok);
    ASSERT_TRUE(tuples_equal(tuples[3], out));

    ASSERT_EQ(filter.next(), OperatorStatus::NoMoreTuples);
    ASSERT_EQ(filter.getCurrentTuple(out), OperatorStatus::NoMoreTuples);
    ASSERT_TRUE(tuples_equal(tuples[3], out));

    ASSERT_EQ(filter.next(), OperatorStatus::NoMoreTuples);
    ASSERT_EQ(filter.getCurrentTuple(out), OperatorStatus::NoMoreTuples);
    ASSERT_TRUE(tuples_equal(tuples[3], out));
}
