#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "obliviousOperatorTestFixture.h"
#include "in_sgx/plan/oboperators/ObliviousProject.h"
#include "in_sgx/plan/oboperators/ObliviousSeqScan.h"
#include "in_sgx/plan/obexpressions/ObliviousExpressionFactory.h"

using namespace plan::oboperators;
using namespace plan::obexpressions;
using namespace db::obdata::obfield;
using namespace db::obdata;

TEST_F(ObliviousOperatorTestFixture, ObliviousProjectConstruction){
    ObliviousExpressionList exps = {
            ObliviousExpressionFactory::makeColumn(3, type::SchemaColumn{type::FieldDataType::Fixchar, vc_size}),
            ObliviousExpressionFactory::makeColumn(0, type::SchemaColumn{type::FieldDataType::Int, sizeof(int)}),
            ObliviousExpressionFactory::makeColumn(1, type::SchemaColumn{type::FieldDataType::Int, sizeof(int)}),
            ObliviousExpressionFactory::makeAdd(
                    ObliviousExpressionFactory::makeLiteral(ObliviousField::makeObliviousIntField(15), sizeof(int)),
                    ObliviousExpressionFactory::makeColumn(1, type::SchemaColumn{type::FieldDataType::Int, sizeof(int)})
            )
    };

    ObliviousSeqScan *scan = new ObliviousSeqScan(0, & oinput1);
    RecordSchema outSchema(
            1, {
                    s1[3], s1[0], s1[1], s1[1]
            });
    ObliviousProject project(1, scan, exps);

    ASSERT_EQ(project.getStatus(), OperatorStatus::NotInitialized);
    EXPECT_EQ(project.getChildren().size(), 1);
    ASSERT_TRUE(recordschema_equal(project.getSchema(), outSchema));
    ASSERT_EQ(project.getParent(), nullptr);
    ASSERT_FALSE(project.isBlocking());
}


TEST_F(ObliviousOperatorTestFixture, ObliviousProjectNextAndGet) {
    ObliviousExpressionList exps = {
            ObliviousExpressionFactory::makeColumn(3, type::SchemaColumn{type::FieldDataType::Fixchar, vc_size}),
            ObliviousExpressionFactory::makeColumn(0, type::SchemaColumn{type::FieldDataType::Int, sizeof(int)}),
            ObliviousExpressionFactory::makeColumn(1, type::SchemaColumn{type::FieldDataType::Int, sizeof(int)}),
            ObliviousExpressionFactory::makeAdd(
                    ObliviousExpressionFactory::makeLiteral(ObliviousField::makeObliviousIntField(15), sizeof(int)),
                    ObliviousExpressionFactory::makeColumn(1, type::SchemaColumn{type::FieldDataType::Int, sizeof(int)})
            )
    };

    ObliviousSeqScan *scan = new ObliviousSeqScan(0, & oinput1);
    RecordSchema outSchema(
            1, {
                    s1[3], s1[0], s1[1], s1[1]
            });
    ObliviousProject project(1, scan, exps);

    ASSERT_EQ(project.getStatus(), OperatorStatus::NotInitialized);

    // ObliviousField added = ObliviousField::makeObliviousIntField(15);

    ObliviousTuple out;

    for(int i = 0; i < 3; ++i) {
        ASSERT_EQ(project.next(), OperatorStatus::Ok);
        ASSERT_EQ(project.getCurrentTuple(out), OperatorStatus::Ok);
        ASSERT_TRUE(oblivious_tuples_equal(out, ObliviousTuple {
                otuples[0][3],
                otuples[0][0],
                otuples[0][1],
                otuples[0][1] + ObliviousField::makeObliviousIntField(15)
        }));

        ASSERT_EQ(project.next(), OperatorStatus::Ok);
        ASSERT_EQ(project.getCurrentTuple(out), OperatorStatus::Ok);
        ASSERT_TRUE(oblivious_tuples_equal(out, ObliviousTuple {
                otuples[1][3],
                otuples[1][0],
                otuples[1][1],
                otuples[1][1] + ObliviousField::makeObliviousIntField(15)
        }));


        ASSERT_EQ(project.next(), OperatorStatus::Ok);
        ASSERT_EQ(project.getCurrentTuple(out), OperatorStatus::Ok);
        ASSERT_TRUE(oblivious_tuples_equal(out, ObliviousTuple {
                otuples[2][3],
                otuples[2][0],
                otuples[2][1],
                otuples[2][1] + ObliviousField::makeObliviousIntField(15)
        }));


        ASSERT_EQ(project.next(), OperatorStatus::Ok);
        ASSERT_EQ(project.getCurrentTuple(out), OperatorStatus::Ok);
        ASSERT_TRUE(oblivious_tuples_equal(out, ObliviousTuple {
                otuples[3][3],
                otuples[3][0],
                otuples[3][1],
                otuples[3][1] + ObliviousField::makeObliviousIntField(15)
        }));

        project.reset();
    }
}