#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <plaintext/plan/expressions/expressions.h>
#include "expressionsTestFixture.h"




TEST_F(ExpressionsTestFixture, ConstructLeaf)
{
    ASSERT_EQ(i_1.evaluate(), Field::makeIntField(1));
    ASSERT_EQ(i_2.evaluate(), Field::makeIntField(2));

    ASSERT_EQ(b_true.evaluate(), Field::makeBoolField(true));
    ASSERT_EQ(b_false.evaluate(), Field::makeBoolField(false));
    ASSERT_TRUE(b_true.evaluate().isTruthy());
    ASSERT_FALSE(b_false.evaluate().isTruthy());

    ASSERT_EQ(s_hello.evaluate(), Field::makeVarcharField("hello"));
    ASSERT_EQ(s_world.evaluate(), Field::makeVarcharField("world"));

    ASSERT_EQ(c_10.evaluate(), t1[0]);
    ASSERT_EQ(c_11.evaluate(), t1[1]);
    ASSERT_EQ(c_12.evaluate(), t1[2]);
    ASSERT_EQ(c_13.evaluate(), t1[3]);

    ASSERT_EQ(c_20.evaluate(), t2[0]);
    ASSERT_EQ(c_21.evaluate(), t2[1]);
    ASSERT_EQ(c_22.evaluate(), t2[2]);
    ASSERT_EQ(c_23.evaluate(), t2[3]);
}


TEST_F(ExpressionsTestFixture, Arithmetic)
{
    Expression a_3 = Expression::makeAdd(i_1, i_2);
    ASSERT_EQ(a_3.evaluate(), Field::makeIntField(3));

    Expression a_4 = Expression::makeAdd(i_2, i_2);
    ASSERT_EQ(a_4.evaluate(), Field::makeIntField(4));

    Expression s_1 = Expression::makeSub(i_2, i_1);
    ASSERT_EQ(s_1.evaluate(), Field::makeIntField(1));

    Expression s_neg1 = Expression::makeSub(i_1, i_2);
    ASSERT_EQ(s_neg1.evaluate(), Field::makeIntField(-1));

    Expression m_2 = Expression::makeMul(i_1, i_2);
    ASSERT_EQ(m_2.evaluate(), Field::makeIntField(2));

    Expression m_1 = Expression::makeMul(i_1, i_1);
    ASSERT_EQ(m_1.evaluate(), Field::makeIntField(1));

    Expression d_1 = Expression::makeMul(i_1, i_1);
    ASSERT_EQ(d_1.evaluate(), Field::makeIntField(1));

    Expression a_cs_t1 = Expression::makeAdd(Expression::makeColumn(1), Expression::makeColumn(0));
    a_cs_t1.setTuple(&t1);
    ASSERT_EQ(a_cs_t1.evaluate(), t1[1] + t1[0]);

    Expression s_cs_t1 = Expression::makeSub(Expression::makeColumn(1), Expression::makeColumn(0));
    s_cs_t1.setTuple(&t1);
    ASSERT_EQ(s_cs_t1.evaluate(), t1[1] - t1[0]);

    Expression a_cs_t2 = Expression::makeAdd(Expression::makeColumn(1), Expression::makeColumn(0));
    a_cs_t2.setTuple(&t2);
    ASSERT_EQ(a_cs_t2.evaluate(), t2[1] + t2[0]);

    Expression s_cs_t2 = Expression::makeSub(Expression::makeColumn(1), Expression::makeColumn(0));
    s_cs_t2.setTuple(&t2);
    ASSERT_EQ(s_cs_t2.evaluate(), t2[1] - t2[0]);
}


TEST_F(ExpressionsTestFixture, Comparison)
{
    Expression eq_i_true = Expression::makeEq(i_1, Expression::makeLiteral(Field::makeIntField(1)));
    Expression eq_i_false = Expression::makeEq(i_1, i_2);

    ASSERT_TRUE(eq_i_true.evaluate().isTruthy());
    ASSERT_FALSE(eq_i_false.evaluate().isTruthy());

    Expression eq_b_true = Expression::makeEq(b_false, Expression::makeLiteral(Field::makeBoolField(false)));
    Expression eq_b_false = Expression::makeEq(b_true, b_false);

    ASSERT_TRUE(eq_b_true.evaluate().isTruthy());
    ASSERT_FALSE(eq_b_false.evaluate().isTruthy());

    Expression eq_vc_true = Expression::makeEq(s_hello, Expression::makeLiteral(Field::makeVarcharField("hello")));
    Expression eq_vc_false = Expression::makeEq(s_hello, s_world);

    ASSERT_TRUE(eq_vc_true.evaluate().isTruthy());
    ASSERT_FALSE(eq_vc_false.evaluate().isTruthy());

    Expression neq_s_true = Expression::makeNEq(s_hello, s_world);
    Expression neq_s_false = Expression::makeNEq(s_hello, Expression::makeLiteral(Field::makeVarcharField("hello")));

    ASSERT_TRUE(neq_s_true.evaluate().isTruthy());
    ASSERT_FALSE(neq_s_false.evaluate().isTruthy());

    Expression lt_i_true = Expression::makeLT(i_1, i_3);
    Expression lt_i_false1 = Expression::makeLT(i_1, i_1);
    Expression lt_i_false2 = Expression::makeLT(i_3, i_2);

    ASSERT_TRUE(lt_i_true.evaluate().isTruthy());
    ASSERT_FALSE(lt_i_false1.evaluate().isTruthy());
    ASSERT_FALSE(lt_i_false2.evaluate().isTruthy());

    Expression gt_i_true = Expression::makeGT(i_3, i_2);
    Expression gt_i_false1 = Expression::makeGT(i_3, i_3);
    Expression gt_i_false2 = Expression::makeGT(i_2, i_3);

    ASSERT_TRUE(gt_i_true.evaluate().isTruthy());
    ASSERT_FALSE(gt_i_false1.evaluate().isTruthy());
    ASSERT_FALSE(gt_i_false2.evaluate().isTruthy());

    Expression lte_i_true1 = Expression::makeLTE(i_1, i_3);
    Expression lte_i_true2 = Expression::makeLTE(i_1, i_1);
    Expression lte_i_false2 = Expression::makeLTE(i_3, i_2);
    
    ASSERT_TRUE(lte_i_true1.evaluate().isTruthy());
    ASSERT_TRUE(lte_i_true2.evaluate().isTruthy());
    ASSERT_FALSE(lte_i_false2.evaluate().isTruthy());

    Expression gte_i_true1 = Expression::makeGTE(i_3, i_1);
    Expression gte_i_true2 = Expression::makeGTE(i_3, i_3);
    Expression gte_i_false2 = Expression::makeGTE(i_1, i_3);

    ASSERT_TRUE(gte_i_true1.evaluate().isTruthy());
    ASSERT_TRUE(gte_i_true2.evaluate().isTruthy());
    ASSERT_FALSE(gte_i_false2.evaluate().isTruthy());

}


TEST_F(ExpressionsTestFixture, Logical)
{
    Expression and_true  = Expression::makeAnd(Expression::makeGTE(i_3, i_1), b_true);
    Expression and_false1 = Expression::makeAnd(Expression::makeGTE(i_3, i_1), b_false);
    Expression and_false2 = Expression::makeAnd(b_false, Expression::makeLTE(i_3, i_2));

    ASSERT_TRUE(and_true.evaluate().isTruthy());
    ASSERT_FALSE(and_false1.evaluate().isTruthy());
    ASSERT_FALSE(and_false2.evaluate().isTruthy());

    Expression or_true1 = Expression::makeOr(Expression::makeGTE(i_3, i_1), b_true);
    Expression or_true2 = Expression::makeOr(Expression::makeGTE(i_3, i_1), b_false);
    Expression or_false = Expression::makeOr(b_false, Expression::makeLTE(i_3, i_2));

    ASSERT_TRUE(or_true1.evaluate().isTruthy());
    ASSERT_TRUE(or_true2.evaluate().isTruthy());
    ASSERT_FALSE(or_false.evaluate().isTruthy());

    Expression not_true = Expression::makeNot(and_false1);
    Expression not_false = Expression::makeNot(and_true);

    ASSERT_TRUE(not_true.evaluate().isTruthy());
    ASSERT_FALSE(not_false.evaluate().isTruthy());
}
