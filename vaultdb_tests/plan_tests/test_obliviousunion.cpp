#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include "obliviousOperatorTestFixture.h"
#include "in_sgx/plan/oboperators/ObliviousOperator.h"
#include "in_sgx/plan/oboperators/ObliviousSeqScan.h"
#include "in_sgx/plan/oboperators/ObliviousUnion.h"

using namespace plan::oboperators;
using namespace plan::obexpressions;

TEST_F(ObliviousOperatorTestFixture, ObliviousUnionConstruction)
{
    ObliviousSeqScan *scan1 = new ObliviousSeqScan(0, & oinput1);
    ObliviousSeqScan *scan2 = new ObliviousSeqScan(0, & oinput2);
    ObliviousUnion aunion(3, ObliviousOperatorPtrList {scan1, scan2});

    ASSERT_EQ(aunion.getStatus(), OperatorStatus::NotInitialized);
    ASSERT_EQ(aunion.getChildren().size(), 2);
    ASSERT_TRUE(recordschema_equal(aunion.getSchema(), s1));
    ASSERT_EQ(aunion.getParent(), nullptr);
    ASSERT_FALSE(aunion.isBlocking());
}

TEST_F(ObliviousOperatorTestFixture, ObliviousUnionNextAndGet)
{
    ObliviousSeqScan *scan1 = new ObliviousSeqScan(0, & oinput1);
    ObliviousSeqScan *scan2 = new ObliviousSeqScan(0, & oinput2);
    ObliviousUnion aunion(3, ObliviousOperatorPtrList {scan1, scan2});

    ASSERT_EQ(aunion.getStatus(), OperatorStatus::NotInitialized);

    ObliviousTuple out;

    for(int i = 0; i < 3; ++i) {
        ASSERT_EQ(aunion.next(), OperatorStatus::Ok);
        ASSERT_EQ(aunion.getCurrentTuple(out), OperatorStatus::Ok);
        ASSERT_TRUE(oblivious_tuples_equal(otuples[0], out));

        ASSERT_EQ(aunion.next(), OperatorStatus::Ok);
        ASSERT_EQ(aunion.getCurrentTuple(out), OperatorStatus::Ok);
        ASSERT_TRUE(oblivious_tuples_equal(otuples[1], out));

        ASSERT_EQ(aunion.next(), OperatorStatus::Ok);
        ASSERT_EQ(aunion.getCurrentTuple(out), OperatorStatus::Ok);
        ASSERT_TRUE(oblivious_tuples_equal(otuples[2], out));

        ASSERT_EQ(aunion.next(), OperatorStatus::Ok);
        ASSERT_EQ(aunion.getCurrentTuple(out), OperatorStatus::Ok);
        ASSERT_TRUE(oblivious_tuples_equal(otuples[3], out));

        ASSERT_EQ(aunion.next(), OperatorStatus::Ok);
        ASSERT_EQ(aunion.getCurrentTuple(out), OperatorStatus::Ok);
        ASSERT_TRUE(oblivious_tuples_equal(otuples2[0], out));

        ASSERT_EQ(aunion.next(), OperatorStatus::Ok);
        ASSERT_EQ(aunion.getCurrentTuple(out), OperatorStatus::Ok);
        ASSERT_TRUE(oblivious_tuples_equal(otuples2[1], out));

        ASSERT_EQ(aunion.next(), OperatorStatus::Ok);
        ASSERT_EQ(aunion.getCurrentTuple(out), OperatorStatus::Ok);
        ASSERT_TRUE(oblivious_tuples_equal(otuples2[2], out));

        ASSERT_EQ(aunion.next(), OperatorStatus::Ok);
        ASSERT_EQ(aunion.getCurrentTuple(out), OperatorStatus::Ok);
        ASSERT_TRUE(oblivious_tuples_equal(otuples2[3], out));

        ASSERT_EQ(aunion.next(), OperatorStatus::Ok);
        ASSERT_EQ(aunion.getCurrentTuple(out), OperatorStatus::Ok);
        ASSERT_TRUE(oblivious_tuples_equal(otuples2[4], out));

        ASSERT_EQ(aunion.next(), OperatorStatus::Ok);
        ASSERT_EQ(aunion.getCurrentTuple(out), OperatorStatus::Ok);
        ASSERT_TRUE(oblivious_tuples_equal(otuples2[5], out));

        ASSERT_EQ(aunion.next(), OperatorStatus::Ok);
        ASSERT_EQ(aunion.getCurrentTuple(out), OperatorStatus::Ok);
        ASSERT_TRUE(oblivious_tuples_equal(otuples2[6], out));

        ASSERT_EQ(aunion.next(), OperatorStatus::NoMoreTuples);
        ASSERT_EQ(aunion.getCurrentTuple(out), OperatorStatus::NoMoreTuples);
        ASSERT_TRUE(oblivious_tuples_equal(otuples2[6], out));

        ASSERT_EQ(aunion.next(), OperatorStatus::NoMoreTuples);
        ASSERT_EQ(aunion.getCurrentTuple(out), OperatorStatus::NoMoreTuples);
        ASSERT_TRUE(oblivious_tuples_equal(otuples2[6], out));

        aunion.reset();
    }
}