#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include "obliviousOperatorTestFixture.h"
#include "in_sgx/plan/obexpressions/ObliviousExpressionFactory.h"
#include <in_sgx/plan/oboperators/ObliviousOperator.h>
#include <in_sgx/plan/oboperators/ObliviousSeqScan.h>
#include <in_sgx/plan/oboperators/ObliviousFilter.h>
#include <in_sgx/plan/oboperators/ObliviousNestedJoin.h>
#include <in_sgx/plan/obexpressions/obexpressions.h>

using namespace plan::oboperators;
using namespace plan::obexpressions;

TEST_F(ObliviousOperatorTestFixture, JoinConstruction)
{
    JoinObliviousExpression joinPred(ObliviousExpressionFactory::makeColumn(0, type::SchemaColumn{type::FieldDataType::Int, sizeof(int)}), ObliviousExpressionFactory::makeColumn(2, type::SchemaColumn{type::FieldDataType::Int, sizeof(int)}));
    ObliviousSeqScan * scan1 = new ObliviousSeqScan(0, & oinput1);
    ObliviousSeqScan * scan2 = new ObliviousSeqScan(0, & oinput1);
    ObliviousNestedJoin njoin(3, scan1, scan2, joinPred);

    ASSERT_EQ(njoin.getStatus(), OperatorStatus::NotInitialized);
    ASSERT_EQ(njoin.getChildren().size(), 2);
    ASSERT_TRUE(recordschema_equal(njoin.getSchema(), RecordSchema::join(s1, s1)));
    ASSERT_TRUE(recordschema_equal(njoin.getChildSchema(0), s1));
    ASSERT_TRUE(recordschema_equal(njoin.getChildSchema(1), s1));
    ASSERT_EQ(scan1->getParent(), &njoin);
    ASSERT_EQ(scan2->getParent(), &njoin);
    ASSERT_EQ(njoin.getParent(), nullptr);
    ASSERT_FALSE(njoin.isBlocking());
}

TEST_F(ObliviousOperatorTestFixture, JoinNextAndGet) {
    JoinObliviousExpression joinPred(ObliviousExpressionFactory::makeColumn(0, type::SchemaColumn{type::FieldDataType::Int, sizeof(int)}), ObliviousExpressionFactory::makeColumn(2, type::SchemaColumn{type::FieldDataType::Int, sizeof(int)}));
    ObliviousSeqScan *scan1 = new ObliviousSeqScan(0, & oinput1);
    ObliviousSeqScan *scan2 = new ObliviousSeqScan(0, & oinput1);
    ObliviousNestedJoin njoin(3, scan1, scan2, joinPred);

    ASSERT_EQ(njoin.getStatus(), OperatorStatus::NotInitialized);

    ObliviousTuple out;

    for (int i = 0; i < 3; ++i) {
        ASSERT_EQ(njoin.getStatus(), OperatorStatus::NotInitialized);

        ASSERT_EQ(njoin.next(), OperatorStatus::Ok);
        ASSERT_EQ(njoin.getCurrentTuple(out), OperatorStatus::Ok);
        ASSERT_TRUE(oblivious_tuples_equal(out, ObliviousTuple::join(otuples[0], otuples[0])));
        ASSERT_TRUE(out.isDummy());

        ASSERT_EQ(njoin.next(), OperatorStatus::Ok);
        ASSERT_EQ(njoin.getCurrentTuple(out), OperatorStatus::Ok);
        ASSERT_TRUE(oblivious_tuples_equal(out, ObliviousTuple::join(otuples[0], otuples[1])));
        ASSERT_TRUE(out.isDummy());

        ASSERT_EQ(njoin.next(), OperatorStatus::Ok);
        ASSERT_EQ(njoin.getCurrentTuple(out), OperatorStatus::Ok);
        ASSERT_TRUE(oblivious_tuples_equal(out, ObliviousTuple::join(otuples[0], otuples[2])));
        ASSERT_TRUE(out.isDummy());

        ASSERT_EQ(njoin.next(), OperatorStatus::Ok);
        ASSERT_EQ(njoin.getCurrentTuple(out), OperatorStatus::Ok);
        ASSERT_TRUE(oblivious_tuples_equal(out, ObliviousTuple::join(otuples[0], otuples[3])));
        ASSERT_TRUE(out.isDummy());


        ASSERT_EQ(njoin.next(), OperatorStatus::Ok);
        ASSERT_EQ(njoin.getCurrentTuple(out), OperatorStatus::Ok);
        ASSERT_TRUE(oblivious_tuples_equal(out, ObliviousTuple::join(otuples[1], otuples[0])));
        ASSERT_TRUE(out.isDummy());

        ASSERT_EQ(njoin.next(), OperatorStatus::Ok);
        ASSERT_EQ(njoin.getCurrentTuple(out), OperatorStatus::Ok);
        ASSERT_TRUE(oblivious_tuples_equal(out, ObliviousTuple::join(otuples[1], otuples[1])));
        ASSERT_TRUE(out.isDummy());

        ASSERT_EQ(njoin.next(), OperatorStatus::Ok);
        ASSERT_EQ(njoin.getCurrentTuple(out), OperatorStatus::Ok);
        ASSERT_TRUE(oblivious_tuples_equal(out, ObliviousTuple::join(otuples[1], otuples[2])));
        ASSERT_FALSE(out.isDummy());

        ASSERT_EQ(njoin.next(), OperatorStatus::Ok);
        ASSERT_EQ(njoin.getCurrentTuple(out), OperatorStatus::Ok);
        ASSERT_TRUE(oblivious_tuples_equal(out, ObliviousTuple::join(otuples[1], otuples[3])));
        ASSERT_TRUE(out.isDummy());


        ASSERT_EQ(njoin.next(), OperatorStatus::Ok);
        ASSERT_EQ(njoin.getCurrentTuple(out), OperatorStatus::Ok);
        ASSERT_TRUE(oblivious_tuples_equal(out, ObliviousTuple::join(otuples[2], otuples[0])));
        ASSERT_TRUE(out.isDummy());

        ASSERT_EQ(njoin.next(), OperatorStatus::Ok);
        ASSERT_EQ(njoin.getCurrentTuple(out), OperatorStatus::Ok);
        ASSERT_TRUE(oblivious_tuples_equal(out, ObliviousTuple::join(otuples[2], otuples[1])));
        ASSERT_TRUE(out.isDummy());

        ASSERT_EQ(njoin.next(), OperatorStatus::Ok);
        ASSERT_EQ(njoin.getCurrentTuple(out), OperatorStatus::Ok);
        ASSERT_TRUE(oblivious_tuples_equal(out, ObliviousTuple::join(otuples[2], otuples[2])));
        ASSERT_TRUE(out.isDummy());

        ASSERT_EQ(njoin.next(), OperatorStatus::Ok);
        ASSERT_EQ(njoin.getCurrentTuple(out), OperatorStatus::Ok);
        ASSERT_TRUE(oblivious_tuples_equal(out, ObliviousTuple::join(otuples[2], otuples[3])));
        ASSERT_TRUE(out.isDummy());


        ASSERT_EQ(njoin.next(), OperatorStatus::Ok);
        ASSERT_EQ(njoin.getCurrentTuple(out), OperatorStatus::Ok);
        ASSERT_TRUE(oblivious_tuples_equal(out, ObliviousTuple::join(otuples[3], otuples[0])));
        ASSERT_TRUE(out.isDummy());

        ASSERT_EQ(njoin.next(), OperatorStatus::Ok);
        ASSERT_EQ(njoin.getCurrentTuple(out), OperatorStatus::Ok);
        ASSERT_TRUE(oblivious_tuples_equal(out, ObliviousTuple::join(otuples[3], otuples[1])));
        ASSERT_TRUE(out.isDummy());

        ASSERT_EQ(njoin.next(), OperatorStatus::Ok);
        ASSERT_EQ(njoin.getCurrentTuple(out), OperatorStatus::Ok);
        ASSERT_TRUE(oblivious_tuples_equal(out, ObliviousTuple::join(otuples[3], otuples[2])));
        ASSERT_FALSE(out.isDummy());

        ASSERT_EQ(njoin.next(), OperatorStatus::Ok);
        ASSERT_EQ(njoin.getCurrentTuple(out), OperatorStatus::Ok);
        ASSERT_TRUE(oblivious_tuples_equal(out, ObliviousTuple::join(otuples[3], otuples[3])));
        ASSERT_TRUE(out.isDummy());

        njoin.reset();
    }
}
