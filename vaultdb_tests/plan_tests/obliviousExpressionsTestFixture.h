#ifndef VAULTDB_OBLIVIOUSEXPRESSIONSTESTFIXTURE_H
#define VAULTDB_OBLIVIOUSEXPRESSIONSTESTFIXTURE_H

#include <gtest/gtest.h>
#include <in_sgx/plan/obexpressions/obexpressions.h>
#include <in_sgx/obdata/obfield/ObliviousIntField.h>
#include <in_sgx/obdata/obfield/ObliviousBoolField.h>
#include <in_sgx/obdata/obfield/ObliviousFixcharField.h>
#include <in_sgx/obdata/ObliviousTuple.h>
#include "in_sgx/plan/obexpressions/ObliviousExpressionFactory.h"

// TODO: decide whether these using are worth it
using namespace plan::obexpressions;
using namespace db::obdata;
using namespace db::obdata::obfield;

class ObliviousExpressionsTestFixture : public ::testing::Test {
protected:
    ObliviousExpressionsTestFixture() {
        c_10.setObliviousTuple(&t1);
        c_11.setObliviousTuple(&t1);
        c_vc_size.setObliviousTuple(&t1);
        c_13.setObliviousTuple(&t1);

        c_20.setObliviousTuple(&t2);
        c_21.setObliviousTuple(&t2);
        c_22.setObliviousTuple(&t2);
        c_23.setObliviousTuple(&t2);
    }

    ~ObliviousExpressionsTestFixture() {};

    size_vdb vc_size = 10;

    db::obdata::ObliviousTuple t1{
            ObliviousField::makeObliviousIntField(1),
            ObliviousField::makeObliviousIntField(2),
            ObliviousField::makeObliviousBoolField(true),
            ObliviousField::makeObliviousFixcharField("     hello", vc_size),
            ObliviousField::makeObliviousFixcharField("     hello", vc_size)
    };

    db::obdata::ObliviousTuple t2{
            ObliviousField::makeObliviousIntField(3),
            ObliviousField::makeObliviousIntField(4),
            ObliviousField::makeObliviousBoolField(true),
            ObliviousField::makeObliviousFixcharField("       foo", vc_size),
            ObliviousField::makeObliviousFixcharField("       bar", vc_size)
    };

    ObliviousExpression i_1 = ObliviousExpressionFactory::makeLiteral(ObliviousField::makeObliviousIntField(1), sizeof(int));
    ObliviousExpression i_2 = ObliviousExpressionFactory::makeLiteral(ObliviousField::makeObliviousIntField(2), sizeof(int));
    ObliviousExpression i_3 = ObliviousExpressionFactory::makeLiteral(ObliviousField::makeObliviousIntField(3), sizeof(int));

    ObliviousExpression b_true = ObliviousExpressionFactory::makeLiteral(ObliviousField::makeObliviousBoolField(true), sizeof(int));
    ObliviousExpression b_false = ObliviousExpressionFactory::makeLiteral(ObliviousField::makeObliviousBoolField(false), sizeof(int));

    ObliviousExpression s_hello = ObliviousExpressionFactory::makeLiteral(
            ObliviousField::makeObliviousFixcharField("     hello", vc_size), vc_size);
    ObliviousExpression s_world = ObliviousExpressionFactory::makeLiteral(
            ObliviousField::makeObliviousFixcharField("     world", vc_size), vc_size);

    ObliviousExpression c_10 = ObliviousExpressionFactory::makeColumn(0, type::SchemaColumn{type::FieldDataType::Int, sizeof(int)});
    ObliviousExpression c_11 = ObliviousExpressionFactory::makeColumn(1, type::SchemaColumn{type::FieldDataType::Int, sizeof(int)});
    ObliviousExpression c_vc_size = ObliviousExpressionFactory::makeColumn(2, type::SchemaColumn{type::FieldDataType::Int, sizeof(int)});
    ObliviousExpression c_13 = ObliviousExpressionFactory::makeColumn(3, type::SchemaColumn{type::FieldDataType::Fixchar, vc_size});

    ObliviousExpression c_20 = ObliviousExpressionFactory::makeColumn(0, type::SchemaColumn{type::FieldDataType::Int, sizeof(int)});
    ObliviousExpression c_21 = ObliviousExpressionFactory::makeColumn(1, type::SchemaColumn{type::FieldDataType::Int, sizeof(int)});
    ObliviousExpression c_22 = ObliviousExpressionFactory::makeColumn(2, type::SchemaColumn{type::FieldDataType::Int, sizeof(int)});
    ObliviousExpression c_23 = ObliviousExpressionFactory::makeColumn(3, type::SchemaColumn{type::FieldDataType::Fixchar, vc_size});
};

#endif //VAULTDB_OBLIVIOUSEXPRESSIONSTESTFIXTURE_H
