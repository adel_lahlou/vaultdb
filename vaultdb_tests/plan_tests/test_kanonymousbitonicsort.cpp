#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <in_sgx/plan/kaoperators/KAnonymousSeqScan.h>
#include <in_sgx/plan/kaoperators/KAnonymousBitonicSort.h>
#include <debug/DebugInSGXController.h>
#include <in_sgx/obdata/BitonicSorter.h>
#include <in_sgx/utilities/SecureMove.h>
#include <in_sgx/serialization/ObliviousSerializer.h>
#include <debug/DebugFakeLocalDataMover.h>
#include "kAnonymousOperatorTestFixture.h"

using namespace plan::kaoperators;

TEST_F(KAnonymousOperatorTestFixture, BitonicConstruction)
{
    utilities::DebugInSGXController m1(1, 1);

    size_vdb k = 2;
    KAnonymousSeqScan * scan = new KAnonymousSeqScan(0, k, & oinput1);
    std::vector<pos_vdb> orderByEntries;
    std::vector<SortOrder> sortOrders;
    orderByEntries.push_back(4);
    sortOrders.push_back(SortOrder::ASCEND);
    KAnonymousBitonicSort sort(2, k, scan, orderByEntries, sortOrders, &m1);
#ifdef VERIFY_KANO_OPERATOR_OUTPUT
    ASSERT_EQ(sort.getChildren().size(), 1);
    ASSERT_TRUE(recordschema_equal(sort.getSchema(), s1));
    ASSERT_TRUE(recordschema_equal(sort.getChildSchema(0), s1));
    ASSERT_EQ(scan->getParent(), &sort);
    ASSERT_EQ(sort.getParent(), nullptr);
    ASSERT_TRUE(sort.isBlocking());
#endif
    utilities::DebugFakeLocalDataMover::instance()->clear();
}

TEST_F(KAnonymousOperatorTestFixture, BitonicNextAndGet)
{
    utilities::DebugInSGXController m1(1, 1);
    size_vdb k = 2;
    ObliviousTupleList expectedTuples {
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousIntField(6),
                    ObliviousField::makeObliviousFixcharField("foo", vc_size),
                    ObliviousField::makeObliviousFixcharField("bar", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousFixcharField("hello", vc_size),
                    ObliviousField::makeObliviousFixcharField("world", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousFixcharField("bigger", vc_size),
                    ObliviousField::makeObliviousFixcharField("better", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(6),
                    ObliviousField::makeObliviousFixcharField("secure", vc_size),
                    ObliviousField::makeObliviousFixcharField("execution", vc_size),
            }),
    };

    KAnonymousSeqScan * scan = new KAnonymousSeqScan(0, k, & oinput1);
    KAnonymousBitonicSort sort(2, k, scan, {4}, {SortOrder::ASCEND}, &m1);
    verify_output(&sort, &expectedTuples);
    utilities::DebugFakeLocalDataMover::instance()->clear();
}

TEST_F(KAnonymousOperatorTestFixture, BitonicSortUnevenCase)
{
    utilities::DebugInSGXController m1(1, 1);
    size_vdb k = 2;
    ObliviousTupleList expectedTuples {
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("world", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("smart", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("tornado", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("rogue", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("chimney", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("fire pit", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("better", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("lasting", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("giant", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousFixcharField("peace", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousFixcharField("peace", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousFixcharField("peace", vc_size),
            }),
    };

    KAnonymousSeqScan * scan = new KAnonymousSeqScan(0, k, & oinput3);
    KAnonymousBitonicSort sort(2, k, scan, {1, 0, 2}, {SortOrder::ASCEND, SortOrder::ASCEND, SortOrder::ASCEND}, &m1);
    verify_output(&sort, &expectedTuples);
    utilities::DebugFakeLocalDataMover::instance()->clear();
}