#include <gtest/gtest.h>
#include <gmock/gmock.h>


#include <shared/type/RecordSchema.h>

using namespace type;

TEST(RecordSchema, Construction)
{
    RecordSchema empty;

    EXPECT_EQ(empty.size(), 0);

    SchemaColumnHolder ts{
        {FieldDataType::Invalid, sizeof(int)},
        {FieldDataType::Int, sizeof(int)},
        {FieldDataType::Fixchar, 20},
        {FieldDataType::Bool, sizeof(int)},
        {FieldDataType::TimestampNoZone, sizeof(time_t)}
    };

    RecordSchema w_vec(0, ts);

    EXPECT_EQ(w_vec.size(), 5);
    EXPECT_EQ(w_vec[0].type, FieldDataType::Invalid);
    EXPECT_EQ(w_vec[1].type, FieldDataType::Int);
    EXPECT_EQ(w_vec[2].type, FieldDataType::Fixchar);
    EXPECT_EQ(w_vec[3].type, FieldDataType::Bool);
    EXPECT_EQ(w_vec[4].type, FieldDataType::TimestampNoZone);

    RecordSchema w_list{
            (TableID) 1, (SchemaColumnHolder) {
                    {FieldDataType::Invalid, sizeof(int)},
                    {FieldDataType::Int, sizeof(int)},
                    {FieldDataType::Fixchar, 20},
                    {FieldDataType::Bool, sizeof(int)},
                    {FieldDataType::TimestampNoZone, sizeof(time_t)}
            }
    };//(0, <#initializer#>);

    EXPECT_EQ(w_list.size(), 5);
    EXPECT_EQ(w_list[0].type, FieldDataType::Invalid);
    EXPECT_EQ(w_list[1].type, FieldDataType::Int);
    EXPECT_EQ(w_list[2].type, FieldDataType::Fixchar);
    EXPECT_EQ(w_list[3].type, FieldDataType::Bool);
    EXPECT_EQ(w_list[4].type, FieldDataType::TimestampNoZone);
}


TEST(RecordSchema, MutationAndAccess)
{
    RecordSchema notEmpty;

    notEmpty.addField(SchemaColumn{FieldDataType::Invalid, sizeof(int)});
    notEmpty.addField(SchemaColumn{FieldDataType::Int, sizeof(int)});
    notEmpty.addField(SchemaColumn{FieldDataType::Fixchar, 10});
    notEmpty.addField(SchemaColumn{FieldDataType::Bool, sizeof(int)});
    notEmpty.addField(SchemaColumn{FieldDataType::TimestampNoZone, sizeof(time_t)});

    EXPECT_EQ(notEmpty[0].type, FieldDataType::Invalid);
    EXPECT_EQ(notEmpty[1].type, FieldDataType::Int);
    EXPECT_EQ(notEmpty[2].type, FieldDataType::Fixchar);
    EXPECT_EQ(notEmpty[3].type, FieldDataType::Bool);
    EXPECT_EQ(notEmpty[4].type, FieldDataType::TimestampNoZone);
    EXPECT_EQ(notEmpty.size(), 5);

    SchemaColumnHolder ts{
            {FieldDataType::Invalid, sizeof(int)},
            {FieldDataType::Int, sizeof(int)},
            {FieldDataType::Fixchar, 20},
            {FieldDataType::Bool, sizeof(int)},
            {FieldDataType::TimestampNoZone, sizeof(time_t)}
    };

    RecordSchema w_vec(0, ts);
    w_vec.erase(0);
    w_vec.erase(2);

    EXPECT_EQ(w_vec[0].type, FieldDataType::Int);
    EXPECT_EQ(w_vec[1].type, FieldDataType::Fixchar);
    EXPECT_EQ(w_vec[2].type, FieldDataType::TimestampNoZone);
    EXPECT_EQ(w_vec.size(), 3);


    RecordSchema w_list{
            (TableID) 2, (SchemaColumnHolder) {
                    {FieldDataType::Invalid, sizeof(int)},
                    {FieldDataType::Int, sizeof(int)},
                    {FieldDataType::Fixchar, 20},
                    {FieldDataType::Bool, sizeof(int)},
                    {FieldDataType::TimestampNoZone, sizeof(time_t)}
            }
    };//(0, <#initializer#>);

    w_list.project(std::vector<size_vdb>{0, 2});
    EXPECT_EQ(w_list.size(), 3);
    EXPECT_EQ(w_list[0].type, FieldDataType::Int);
    EXPECT_EQ(w_list[1].type, FieldDataType::Bool);
    EXPECT_EQ(w_list[2].type, FieldDataType::TimestampNoZone);

    RecordSchema w_join = RecordSchema::join(w_vec, w_list);
    EXPECT_EQ(w_join.size(), 6);
    EXPECT_EQ(w_join[0].type, FieldDataType::Int);
    EXPECT_EQ(w_join[1].type, FieldDataType::Fixchar);
    EXPECT_EQ(w_join[2].type, FieldDataType::TimestampNoZone);
    EXPECT_EQ(w_join[3].type, FieldDataType::Int);
    EXPECT_EQ(w_join[4].type, FieldDataType::Bool);
    EXPECT_EQ(w_join[5].type, FieldDataType::TimestampNoZone);
}