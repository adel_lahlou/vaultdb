include_directories(${gtest_SOURCE_DIR}/include ${gtest_SOURCE_DIR})
include_directories(${gmock_SOURCE_DIR}/include ${gmock_SOURCE_DIR})


set(TYPETESTS_SOURCE_FILES
        test_schema.cpp)


add_executable(runTypeTests ${TYPETESTS_SOURCE_FILES})

target_link_libraries(runTypeTests gtest gtest_main)
target_link_libraries(runTypeTests vaultdb_plain)