#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include "utilityTestFixture.h"
#include "in_sgx/utilities/InSGXController.h"
#include <in_sgx/obdata/ObliviousTupleTable.h>
#include <debug/DebugFakeLocalDataMover.h>

#include "in_sgx/serialization/ObliviousSerializer.h"

using namespace utilities;
using namespace db::obdata;
using namespace db::obdata::obfield;
using namespace type;
#ifdef VAULTDB_INSGXCONTROLLER_TEST
TEST_F(UtilityTestFixture, Construction)
{
    InSGXController m1(1, 0);
    InSGXController m2(2, 0);

    m1.registerMachine(m2.getMachineID());
    m2.registerMachine(m1.getMachineID());

    const char * input = "random string";
    size_vdb input_size = 13;
    m1.dispatch(2,9,input_size, (unsigned char *)input);
    DataSizePair output_pair = m2.get(1,9);
    char* output = (char*)output_pair.first.get();
    size_vdb outputSize = output_pair.second;

    ASSERT_TRUE(std::string(input, input_size) == std::string(output, outputSize));
    DebugFakeLocalDataMover::eraseMachineRecord(1);
    DebugFakeLocalDataMover::eraseMachineRecord(2);
}

TEST_F(UtilityTestFixture, TablePassing)
{
    size_vdb vc_size = 20;
    
    type::RecordSchema schema{
            (TableID) 3, {
                    {type::FieldDataType::Int, sizeof(int)},
                    {type::FieldDataType::Int, sizeof(int)},
                    {type::FieldDataType::Int, sizeof(int)},
                    {type::FieldDataType::Varchar, vc_size},
                    {type::FieldDataType::Varchar, vc_size},
            }
    };
    ObliviousTupleList otuples {
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousVarcharField("hello", vc_size),
                    ObliviousField::makeObliviousVarcharField("world", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(5),
                    ObliviousField::makeObliviousIntField(6),
                    ObliviousField::makeObliviousVarcharField("foo", vc_size),
                    ObliviousField::makeObliviousVarcharField("bar", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousVarcharField("bigger", vc_size),
                    ObliviousField::makeObliviousVarcharField("better", vc_size),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousIntField(6),
                    ObliviousField::makeObliviousVarcharField("secure", vc_size),
                    ObliviousField::makeObliviousVarcharField("execution", vc_size),
            },
    };

    InSGXController m1(3, 0);
    InSGXController m2(4, 0);

    ObliviousTupleTable ott(schema, otuples);

    m1.registerMachine(m2.getMachineID());
    m2.registerMachine(m1.getMachineID());

    size_vdb size = ott.serializationSize();
    unsigned char input[size];
    ott.serializeTo(input);
    m1.dispatch(4,109, size, input);
    DataSizePair output_pair = m2.get(3,109);
    unsigned char* output = output_pair.first.get();
    size_vdb outputSize = output_pair.second;

    ObliviousTupleTable tab = ObliviousSerializer::deserializeObliviousTupleTable(output, sizeof(output));
    ASSERT_TRUE(recordschema_equal(tab.getSchema(), schema));

    ASSERT_TRUE(oblivious_tuples_equal(otuples[0], tab[0]));
    ASSERT_TRUE(oblivious_tuples_equal(otuples[1], tab[1]));
    ASSERT_TRUE(oblivious_tuples_equal(otuples[2], tab[2]));
    ASSERT_TRUE(oblivious_tuples_equal(otuples[3], tab[3]));

    DebugFakeLocalDataMover::eraseMachineRecord(1);
    DebugFakeLocalDataMover::eraseMachineRecord(2);
}
#else
#endif