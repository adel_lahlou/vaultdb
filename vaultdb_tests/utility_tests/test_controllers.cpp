#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <utilities/OutsideController.h>
#include <gateway/GatewayBetweenTwoWorlds.h>
#include <gflags/gflags.h>
#include <in_sgx/plan/kaoperators/KAnonymousSeqScan.h>
#include <in_sgx/plan/kaoperators/KAnonymousFilter.h>
#include <in_sgx/plan/kaoperators/ClusterKAnonymousRepartition.h>
#include <in_sgx/plan/obexpressions/ObliviousExpression.h>
#include <in_sgx/plan/obexpressions/ObliviousExpressionFactory.h>
#include <cstdio>
#include <in_sgx/plan/kaoperators/KAnonymousLocalGenFilter.h>
#include <in_sgx/plan/kaoperators/ClusterKAnonymousBinnedAggregation.h>
#include <in_sgx/plan/kaoperators/ClusterKAnonymousGenJoin.h>
#include <utilities/TuplePrinter.h>
#include <in_sgx/plan/kaoperators/KAnonymousBitonicSort.h>
#include <in_sgx/plan/kaoperators/KAnonymousProject.h>
#include <in_sgx/plan/kaoperators/ClusterKAnonymousSortedAggregation.h>
#include <in_sgx/plan/kaoperators/ClusterKAnonymousLimit.h>
#include <ctime>
#include <in_sgx/plan/kaoperators/ClusterKAnonymousWindow.h>
#include <in_sgx/obdata/ObliviousDataFactory.h>

#include "utilityTestFixture.h"
#include <sys/time.h>
#include <in_sgx/plan/kaoperators/KAnonymousUnion.h>

typedef unsigned long long timestamp_t;

static timestamp_t getTimestamp() {
    struct timeval now;
    gettimeofday (&now, NULL);
    return  now.tv_usec + (timestamp_t)now.tv_sec * 1000000;
}

#define TS_T_PER_SEC 1000000L // TIMESTAMP_T_UNITS_PER_SEC

#ifndef CONTROLLER_TEST_DEBUG_MSG
#define CONTROLLER_TEST_DEBUG_MSG
#endif

DEFINE_string(local_address_port, "localhost:4000", "address and port for local server to run on");
DEFINE_int32(machine_id, 0, "machine id");
DEFINE_int32(queryID, 1, "query id");
DEFINE_int32(k, 2, "k, as in k-anonymity");
DEFINE_int32(year, 2006, "using data of this year; default 2006");

void sendPSQLQuery(MachineID dst_machine_id, TableID queryOutputTableID, type::RecordSchema psqlSchema, const char *sqlQuery, int isFinalQuery);
void sendSecureQuery(MachineID dst_machine_id, TableID queryID, unsigned char *query, size_vdb size, int isFinalQuery);

static void registerAllMachines(MachineID machineID, size_vdb hostCount, size_vdb instancePerMachine) {
    for (pos_vdb i = 0; i < hostCount; i++) {

        if (machineID == i + 1) {
            continue;
        }

        pos_vdb vdb_machine_serial = i / instancePerMachine + 1;
        std::string regString = std::string("vaultdb0") + std::to_string(vdb_machine_serial) + std::string(":");

        if (i < 10 - 1) {
            pos_vdb i_ = i + 1;
            regString += std::to_string(4000 + i_ * 100 + i_ * 10 + i_);
        } else {
            pos_vdb i_ = i - 10 + 1;
            regString += std::to_string(5000 + i_ * 100 + i_ * 10 + i_);
        }
        OutsideController::RegisterMachine(i + 1, regString);
        printf("Machine %d registers %s\n", machineID, regString.c_str());
    }
}


static type::RecordSchema deployGenJoin(TableID startingTID, size_vdb totalMachineCount, size_vdb k, type::RecordSchema leftInputSchema,
                                 type::RecordSchema rightInputSchema, std::vector<pos_vdb> leftAttr, std::vector<pos_vdb> rightAttr,
                                 std::vector<pos_vdb> leftEntities, std::vector<pos_vdb> rightEntities, bool isRepartitionSorting,
                                 int isFinalQuery) {

    TableID scanID = startingTID + 10;
    TransmitterID merge1TID = startingTID + 20;
    TransmitterID merge2TID = startingTID + 40;
    TableID joinID = startingTID + 60;
    db::obdata::ObliviousTupleTable dummyTable1 (leftInputSchema, {});
    db::obdata::ObliviousTupleTable dummyTable2 (rightInputSchema, {});
    auto scan1 = new plan::kaoperators::KAnonymousSeqScan(scanID, k, &dummyTable1);
    auto scan2 = new plan::kaoperators::KAnonymousSeqScan(scanID, k, &dummyTable2);

    auto scan3 = new plan::kaoperators::KAnonymousSeqScan(scanID, k, &dummyTable1);
    auto scan4 = new plan::kaoperators::KAnonymousSeqScan(scanID, k, &dummyTable2);
    plan::kaoperators::ClusterKAnonymousGenJoin join1(
            1, joinID, merge1TID, merge2TID, k, scan1, scan2, leftAttr, rightAttr, leftEntities, rightEntities, {}, {}, isRepartitionSorting, nullptr);
    plan::kaoperators::ClusterKAnonymousGenJoin join2(
            2, joinID, merge1TID, merge2TID, k, scan3, scan4, leftAttr, rightAttr, leftEntities, rightEntities, {}, {}, isRepartitionSorting, nullptr);

    size_vdb encodingSize1;
    size_vdb encodingSize2;
    std::shared_ptr<unsigned char> query1 (
            plan::kaoperators::KAnonymousOperator::EncodeQueryToNewArray(
                    &join1, encodingSize1), [](unsigned char * p){delete[] p;});
    std::shared_ptr<unsigned char> query2 (
            plan::kaoperators::KAnonymousOperator::EncodeQueryToNewArray(
                    &join2, encodingSize2), [](unsigned char * p){delete[] p;});

    std::thread t3(sendSecureQuery, 1, joinID, query1.get(), encodingSize1, isFinalQuery);
    std::thread t4(sendSecureQuery, 2, joinID, query2.get(), encodingSize2, isFinalQuery);

    t3.join();
    t4.join();

    while (!OutsideController::IsDone()) {
        sleep(1);
    }

    return type::RecordSchema(join1.getSchema());
};




/**
 * Query 1
 * Low card two party test query
 * Simple scan
 */
static void query1(size_vdb fixchar_size) {

    printf("[INFO] Query 1 initiated.\n");

    // we're honest broker; send queries
    OutsideController::RegisterMachine(1, "localhost:4111");
    OutsideController::RegisterMachine(2, "localhost:4222");
    
    MachineID machineOneID = 1;
    MachineID machineTwoID = 2;

    size_vdb k = (size_vdb) FLAGS_k;

    //------------------------- postgres query -----------------------------

    TableID psqlTableID = 5;
    const char * postgresQuery = "select cast(patient_id % 17 as integer), cast(substring(icd9, 0, 4) as varchar) from diagnoses where patient_id > 1500 and patient_id < 1700 and icd9 ilike 'v2%'";
    type::RecordSchema psqlSchema = {
            psqlTableID, {
                    {type::FieldDataType::Int, INT_FIELD_SIZE},
                    {type::FieldDataType::Fixchar, fixchar_size},
            }
    };

    std::thread t1(sendPSQLQuery, machineOneID, psqlTableID, psqlSchema, postgresQuery, STEP_QUERY);
    std::thread t2(sendPSQLQuery, machineTwoID, psqlTableID, psqlSchema, postgresQuery, STEP_QUERY);

    t1.join();
    t2.join();

    while (!OutsideController::IsDone()) {
        sleep(1);
    }

    //------------------------- scan -------------------------------

    TableID scanID = 10;
    TableID mergeID = 40;
    TransmitterID repartTID = 50;
    db::obdata::ObliviousTupleTable dummyTable (psqlSchema, {});
    auto scan = new plan::kaoperators::KAnonymousSeqScan(scanID, k, &dummyTable);
    auto repartition1 = new plan::kaoperators::ClusterKAnonymousRepartition(machineOneID, mergeID, repartTID, k, scan,
                                                                            {0}, {0},
                                                                            std::vector<size_vdb>(), true, nullptr);
    auto repartition2 = new plan::kaoperators::ClusterKAnonymousRepartition(machineTwoID, mergeID, repartTID, k, scan,
                                                                            {0}, {0},
                                                                            std::vector<size_vdb>(), true, nullptr);

    size_vdb encodingSize1;
    size_vdb encodingSize2;
    std::shared_ptr<unsigned char> query1 (
            plan::kaoperators::KAnonymousOperator::EncodeQueryToNewArray(
                    repartition1, encodingSize1), [](unsigned char * p){delete[] p;});
    std::shared_ptr<unsigned char> query2 (
            plan::kaoperators::KAnonymousOperator::EncodeQueryToNewArray(
                    repartition2, encodingSize2), [](unsigned char * p){delete[] p;});

    std::thread t3(sendSecureQuery, machineOneID, mergeID, query1.get(), encodingSize1, FINAL_QUERY);
    std::thread t4(sendSecureQuery, machineTwoID, mergeID, query2.get(), encodingSize2, FINAL_QUERY);

    t3.join();
    t4.join();

    while (!OutsideController::IsDone()) {
        sleep(1);
    }

    //----------------------- result display -------------------

    ObliviousTupleTable * resultTable = &OutsideController::GetResultObliviousTupleTable();
#ifdef CONTROLLER_TEST_DEBUG_MSG
    printf("Table Output: \n");
#endif
    for (pos_vdb i = 0, size = resultTable->size(); i < size; i++) {
#ifdef CONTROLLER_TEST_DEBUG_MSG
        printf("%d: %s -- isDummy? %d\n", i, (*resultTable)[i].toString().c_str(), (*resultTable)[i].isDummy());
#endif
    }
};

/**
 * Query 2
 * Low card two party test query
 * Scan Repart Filter
 */
static void query2(size_vdb fixchar_size) {

    printf("[INFO] Query 2 initiated.\n");

    // we're honest broker; send queries
    OutsideController::RegisterMachine(1, "localhost:4111");
    OutsideController::RegisterMachine(2, "localhost:4222");

    size_vdb k = (size_vdb) FLAGS_k;

    MachineID machineOneID = 1;
    MachineID machineTwoID = 2;

    //------------------------- postgres query -----------------------------

    TableID psqlTableID = 5;
    const char * postgresQuery = "select cast(patient_id % 17 as integer), cast(substring(icd9, 0, 4) as varchar) from diagnoses where patient_id > 1500 and patient_id < 1700 and icd9 ilike 'v2%'";
    type::RecordSchema psqlSchema = {
            psqlTableID, {
                    {type::FieldDataType::Int, INT_FIELD_SIZE},
                    {type::FieldDataType::Fixchar, fixchar_size},
            }
    };

    std::thread t1(sendPSQLQuery, machineOneID, psqlTableID, psqlSchema, postgresQuery, STEP_QUERY);
    std::thread t2(sendPSQLQuery, machineTwoID, psqlTableID, psqlSchema, postgresQuery, STEP_QUERY);

    t1.join();
    t2.join();

    while (!OutsideController::IsDone()) {
        sleep(1);
    }

    //------------------------- scan repart filter -------------------------------

    TableID scanID = 10;
    TableID mergeID = 40;
    TransmitterID repartTID = 50;
    TableID filterID = 60;
    db::obdata::ObliviousTupleTable dummyTable (psqlSchema, {});
    auto scan = new plan::kaoperators::KAnonymousSeqScan(scanID, k, &dummyTable);
    auto repartition1 = new plan::kaoperators::ClusterKAnonymousRepartition(machineOneID, mergeID, repartTID, k, scan,
                                                                            {0}, {0},
                                                                            std::vector<size_vdb>(), true, nullptr);
    auto repartition2 = new plan::kaoperators::ClusterKAnonymousRepartition(machineTwoID, mergeID, repartTID, k, scan,
                                                                            {0}, {0},
                                                                            std::vector<size_vdb>(), true, nullptr);

    auto filterPred = plan::obexpressions::ObliviousExpressionFactory::makeEq(
            plan::obexpressions::ObliviousExpressionFactory::makeColumn(0, {type::FieldDataType::Int, sizeof(int)}),
            plan::obexpressions::ObliviousExpressionFactory::makeLiteral(ObliviousField::makeObliviousIntField(11), sizeof(int))
    );
    auto filter1 = new plan::kaoperators::KAnonymousLocalGenFilter(filterID, k, repartition1, filterPred, filterPred, {0}, {}, nullptr);
    auto filter2 = new plan::kaoperators::KAnonymousLocalGenFilter(filterID, k, repartition2, filterPred, filterPred, {0}, {}, nullptr);

    size_vdb encodingSize1;
    size_vdb encodingSize2;
    std::shared_ptr<unsigned char> query1 (
            plan::kaoperators::KAnonymousOperator::EncodeQueryToNewArray(
                    filter1, encodingSize1), [](unsigned char * p){delete[] p;});
    std::shared_ptr<unsigned char> query2 (
            plan::kaoperators::KAnonymousOperator::EncodeQueryToNewArray(
                    filter2, encodingSize2), [](unsigned char * p){delete[] p;});

    std::thread t3(sendSecureQuery, machineOneID, filterID, query1.get(), encodingSize1, FINAL_QUERY);
    std::thread t4(sendSecureQuery, machineTwoID, filterID, query2.get(), encodingSize2, FINAL_QUERY);

    t3.join();
    t4.join();

    while (!OutsideController::IsDone()) {
        sleep(1);
    }

    //----------------------- result display -------------------

    ObliviousTupleTable * resultTable = &OutsideController::GetResultObliviousTupleTable();
#ifdef CONTROLLER_TEST_DEBUG_MSG
    printf("Table Output: \n");
#endif
    for (pos_vdb i = 0, size = resultTable->size(); i < size; i++) {
#ifdef CONTROLLER_TEST_DEBUG_MSG
        printf("%d: %s -- isDummy? %d\n", i, (*resultTable)[i].toString().c_str(), (*resultTable)[i].isDummy());
#endif
    }
};

/**
 * Query 3
 * Low card two party test query
 * Scan Repart Filter -> agg
 */
static void query3(size_vdb fixchar_size) {

    printf("[INFO] Query 3 initiated.\n");

    // we're honest broker; send queries
    OutsideController::RegisterMachine(1, "localhost:4111");
    OutsideController::RegisterMachine(2, "localhost:4222");

    size_vdb k = (size_vdb) FLAGS_k;

    MachineID machineOneID = 1;
    MachineID machineTwoID = 2;
    
    //------------------------- postgres query -----------------------------

    TableID psqlTableID = 5;
    const char * postgresQuery = "select cast(patient_id % 17 as integer), cast(substring(icd9, 0, 4) as varchar) from diagnoses where patient_id > 1500 and patient_id < 1700 and icd9 ilike 'v2%'";
    type::RecordSchema psqlSchema = {
            psqlTableID, {
                    {type::FieldDataType::Int, INT_FIELD_SIZE},
                    {type::FieldDataType::Fixchar, fixchar_size},
            }
    };

    std::thread t1(sendPSQLQuery, machineOneID, psqlTableID, psqlSchema, postgresQuery, STEP_QUERY);
    std::thread t2(sendPSQLQuery, machineTwoID, psqlTableID, psqlSchema, postgresQuery, STEP_QUERY);

    t1.join();
    t2.join();

    while (!OutsideController::IsDone()) {
        sleep(1);
    }

    //------------------------- scan merge and filter -------------------------------

    TableID scanID = 10;
    TableID mergeID = 40;
    TransmitterID repartTID = 50;
    TableID filterID = 60;
    db::obdata::ObliviousTupleTable dummyTable (psqlSchema, {});
    auto scan = new plan::kaoperators::KAnonymousSeqScan(scanID, k, &dummyTable);
    auto repartition1 = new plan::kaoperators::ClusterKAnonymousRepartition(machineOneID, mergeID, repartTID, k, scan,
                                                                            {0}, {0},
                                                                            std::vector<size_vdb>(), true, nullptr);
    auto repartition2 = new plan::kaoperators::ClusterKAnonymousRepartition(machineTwoID, mergeID, repartTID, k, scan,
                                                                            {0}, {0},
                                                                            std::vector<size_vdb>(), true, nullptr);

    auto filterPred = plan::obexpressions::ObliviousExpressionFactory::makeEq(
            plan::obexpressions::ObliviousExpressionFactory::makeColumn(0, {type::FieldDataType::Int, sizeof(int)}),
            plan::obexpressions::ObliviousExpressionFactory::makeLiteral(ObliviousField::makeObliviousIntField(11), sizeof(int))
    );
    auto filter1 = new plan::kaoperators::KAnonymousLocalGenFilter(filterID, k, repartition1, filterPred, filterPred, {0}, {}, nullptr);
    auto filter2 = new plan::kaoperators::KAnonymousLocalGenFilter(filterID, k, repartition2, filterPred, filterPred, {0}, {}, nullptr);

    size_vdb encodingSize1;
    size_vdb encodingSize2;
    std::shared_ptr<unsigned char> query1 (
            plan::kaoperators::KAnonymousOperator::EncodeQueryToNewArray(
                    filter1, encodingSize1), [](unsigned char * p){delete[] p;});
    std::shared_ptr<unsigned char> query2 (
            plan::kaoperators::KAnonymousOperator::EncodeQueryToNewArray(
                    filter2, encodingSize2), [](unsigned char * p){delete[] p;});

    std::thread t3(sendSecureQuery, machineOneID, filterID, query1.get(), encodingSize1, STEP_QUERY);
    std::thread t4(sendSecureQuery, machineTwoID, filterID, query2.get(), encodingSize2, STEP_QUERY);

    t3.join();
    t4.join();

    while (!OutsideController::IsDone()) {
        sleep(1);
    }

    //------------------------- aggregation -------------------------------

    TableID scanStage2ID = 80;
    TableID aggID = 85;
    TransmitterID tid = 95;
    db::obdata::ObliviousTupleTable dummyTable2 (filter1->getSchema(), {});
    std::vector<plan::obexpressions::ObliviousExpression> expr {
            plan::obexpressions::ObliviousExpressionFactory::makeColumn(1, type::SchemaColumn{type::FieldDataType::Fixchar, fixchar_size}),
            plan::obexpressions::ObliviousExpressionFactory::makeSum(0, type::SchemaColumn{type::FieldDataType::Int, sizeof(int)}),
    };
    auto scanStage2 = new plan::kaoperators::KAnonymousSeqScan(scanStage2ID, k, &dummyTable2);
    auto agg1 = new plan::kaoperators::ClusterKAnonymousBinnedAggregation(machineOneID, aggID, tid, k, scanStage2, expr,
                                                                          {1}, false, false, nullptr);
    auto agg2 = new plan::kaoperators::ClusterKAnonymousBinnedAggregation(machineTwoID, aggID, tid, k, scanStage2, expr,
                                                                          {1}, false, false, nullptr);

    size_vdb encodingSize3;
    size_vdb encodingSize4;
    std::shared_ptr<unsigned char> query3 (
            plan::kaoperators::KAnonymousOperator::EncodeQueryToNewArray(
                    agg1, encodingSize3), [](unsigned char * p){delete[] p;});
    std::shared_ptr<unsigned char> query4 (
            plan::kaoperators::KAnonymousOperator::EncodeQueryToNewArray(
                    agg2, encodingSize4), [](unsigned char * p){delete[] p;});

    std::thread t5(sendSecureQuery, machineOneID, aggID, query3.get(), encodingSize3, FINAL_QUERY);
    std::thread t6(sendSecureQuery, machineTwoID, aggID, query4.get(), encodingSize4, FINAL_QUERY);

    t5.join();
    t6.join();

    while (!OutsideController::IsDone()) {
        sleep(1);
    }

    //----------------------- result display -------------------

    ObliviousTupleTable * resultTable = &OutsideController::GetResultObliviousTupleTable();
#ifdef CONTROLLER_TEST_DEBUG_MSG
    printf("Table Output: \n");
#endif
    for (pos_vdb i = 0, size = resultTable->size(); i < size; i++) {
#ifdef CONTROLLER_TEST_DEBUG_MSG
        printf("%d: %s -- isDummy? %d\n", i, (*resultTable)[i].toString().c_str(), (*resultTable)[i].isDummy());
#endif
    }
};

/**
 * Query 4
 * Low card two party test query
 * Bottom left join in aspirin profile
 */
static void query4(size_vdb fixchar_size) {

    printf("[INFO] Query 4 initiated.\n");

    struct timespec tim, tim2;
    tim.tv_sec = 0;
    tim.tv_nsec = 500000000L;

    // we're honest broker; send queries
    OutsideController::RegisterMachine(1, "localhost:4111");
    OutsideController::RegisterMachine(2, "localhost:4222");

    size_vdb k = (size_vdb) FLAGS_k;

    MachineID machineOneID = 1;
    MachineID machineTwoID = 2;

    //------------------------- postgres query -----------------------------

    TableID psqlQuery1TableID = 5;
    TableID psqlQuery2TableID = 6;
    const char * psqlQuery1 = "select cast(patient_id % 17 as integer), cast(substring(icd9, 0, 4) as varchar) from diagnoses where patient_id > 1500 and patient_id < 1700 and icd9 ilike 'v2%'";
    const char * psqlQuery2 = "select cast(patient_id % 17 as integer), visit_no, cast(pulse as integer) from vitals where patient_id > 1500 and patient_id < 1700 and pulse > 100";
    type::RecordSchema psqlQuery1Schema = {
            psqlQuery1TableID, {
                    {type::FieldDataType::Int, INT_FIELD_SIZE},
                    {type::FieldDataType::Fixchar, fixchar_size},
            }
    };
    type::RecordSchema psqlQuery2Schema = {
            psqlQuery2TableID, {
                    {type::FieldDataType::Int, INT_FIELD_SIZE},
                    {type::FieldDataType::Int, INT_FIELD_SIZE},
            }
    };

    std::thread t1_1(sendPSQLQuery, machineOneID, psqlQuery1TableID, psqlQuery1Schema, psqlQuery1, STEP_QUERY);
    nanosleep(&tim , &tim2);
    std::thread t2_1(sendPSQLQuery, machineTwoID, psqlQuery1TableID, psqlQuery1Schema, psqlQuery1, STEP_QUERY);
    nanosleep(&tim , &tim2);
    std::thread t1_2(sendPSQLQuery, machineOneID, psqlQuery2TableID, psqlQuery2Schema, psqlQuery2, STEP_QUERY);
    nanosleep(&tim , &tim2);
    std::thread t2_2(sendPSQLQuery, machineTwoID, psqlQuery2TableID, psqlQuery2Schema, psqlQuery2, STEP_QUERY);
    nanosleep(&tim , &tim2);

    t1_1.join();
    t2_1.join();
    t1_2.join();
    t2_2.join();

    while (!OutsideController::IsDone()) {
        sleep(1);
    }

    //------------------------- scan merge and filter -------------------------------

    TableID scan1ID = 10;
    TableID scan2ID = 11;
    TableID joinID = 30;
    TransmitterID merge1TID = 40;
    TransmitterID merge2TID = 50;
    db::obdata::ObliviousTupleTable dummyTable1 (psqlQuery1Schema, {});
    db::obdata::ObliviousTupleTable dummyTable2 (psqlQuery2Schema, {});
    auto scan1 = new plan::kaoperators::KAnonymousSeqScan(scan1ID, k, &dummyTable1);
    auto scan2 = new plan::kaoperators::KAnonymousSeqScan(scan2ID, k, &dummyTable2);

    auto join1 = new plan::kaoperators::ClusterKAnonymousGenJoin(
            machineOneID, joinID, merge1TID, merge2TID, k, scan1, scan2, {0}, {0}, {0}, {0}, {}, {}, true, nullptr);
    auto join2 = new plan::kaoperators::ClusterKAnonymousGenJoin(
            machineTwoID, joinID, merge1TID, merge2TID, k, scan1, scan2, {0}, {0}, {0}, {0}, {}, {}, true, nullptr);

    size_vdb encodingSize1;
    size_vdb encodingSize2;
    std::shared_ptr<unsigned char> query1 (
            plan::kaoperators::KAnonymousOperator::EncodeQueryToNewArray(
                    join1, encodingSize1), [](unsigned char * p){delete[] p;});
    std::shared_ptr<unsigned char> query2 (
            plan::kaoperators::KAnonymousOperator::EncodeQueryToNewArray(
                    join2, encodingSize2), [](unsigned char * p){delete[] p;});

    std::thread t3(sendSecureQuery, machineOneID, joinID, query1.get(), encodingSize1, FINAL_QUERY);
    std::thread t4(sendSecureQuery, machineTwoID, joinID, query2.get(), encodingSize2, FINAL_QUERY);

    t3.join();
    t4.join();

    while (!OutsideController::IsDone()) {
        sleep(1);
    }

    //----------------------- result display -------------------

    ObliviousTupleTable * resultTable = &OutsideController::GetResultObliviousTupleTable();
#ifdef CONTROLLER_TEST_DEBUG_MSG
    printf("Table Output: \n");
#endif
    for (pos_vdb i = 0, size = resultTable->size(); i < size; i++) {
#ifdef CONTROLLER_TEST_DEBUG_MSG
        printf("%d: %s -- isDummy? %d\n", i, (*resultTable)[i].toString().c_str(), (*resultTable)[i].isDummy());
#endif
    }
};

/**
 * Query 5
 * Right join of Aspirin profile
 * Simple scan
 */
static void query5(size_vdb fixchar_size) {

    printf("[INFO] Query 5 initiated.\n");

    // we're honest broker; send queries
    OutsideController::RegisterMachine(1, "localhost:4111");
    OutsideController::RegisterMachine(2, "localhost:4222");

    size_vdb k = (size_vdb) FLAGS_k;

    MachineID machineOneID = 1;
    MachineID machineTwoID = 2;

    TableID psqlQuery3TableID = 7;
    TableID psqlQuery4TableID = 8;
    type::RecordSchema psqlQuery3Schema = {
            psqlQuery3TableID, {
                    {type::FieldDataType::Int, INT_FIELD_SIZE},
            }
    };
    type::RecordSchema psqlQuery4Schema = {
            psqlQuery4TableID, {
                    {type::FieldDataType::Int, INT_FIELD_SIZE},
                    {type::FieldDataType::Int, INT_FIELD_SIZE},
                    {type::FieldDataType::Int, INT_FIELD_SIZE},
            }
    };

    struct timespec tim, tim2;
    tim.tv_sec = 0;
    tim.tv_nsec = 500000000L;

    const char * psqlQuery3 = "select cast(patient_id % 17 as integer) from medications where patient_id > 1500 and patient_id < 1700 and medication ilike 'd%'";
    const char * psqlQuery4 = "select cast(patient_id % 17 as integer) id, max(race), max(gender) from demographics where patient_id > 1500 and patient_id < 1700 group by id";
    std::thread t1_3(sendPSQLQuery, machineOneID, psqlQuery3TableID, psqlQuery3Schema, psqlQuery3, STEP_QUERY);
    nanosleep(&tim , &tim2);
    std::thread t2_3(sendPSQLQuery, machineTwoID, psqlQuery3TableID, psqlQuery3Schema, psqlQuery3, STEP_QUERY);
    nanosleep(&tim , &tim2);
    std::thread t1_4(sendPSQLQuery, machineOneID, psqlQuery4TableID, psqlQuery4Schema, psqlQuery4, STEP_QUERY);
    nanosleep(&tim , &tim2);
    std::thread t2_4(sendPSQLQuery, machineTwoID, psqlQuery4TableID, psqlQuery4Schema, psqlQuery4, STEP_QUERY);
    nanosleep(&tim , &tim2);
    t1_3.join();
    t2_3.join();
    t1_4.join();
    t2_4.join();

    while (!OutsideController::IsDone()) {
        sleep(1);
    }

    //------------------------- scan repart and local sort -------------------------------

    auto schema = deployGenJoin(200, 2, k, psqlQuery3Schema, psqlQuery4Schema, {0}, {0}, {0}, {0}, true, FINAL_QUERY);

    printf("\n\n");
    for (pos_vdb i = 0, size = schema.size(); i < size; i ++ ) {
        printf("<><><><><><><> Schema item %d type: %d <><><><><><><>\n", i, (int)schema[i].type);
    }
    printf("\n\n");

    //----------------------- result display -------------------

    ObliviousTupleTable * resultTable = &OutsideController::GetResultObliviousTupleTable();
#ifdef CONTROLLER_TEST_DEBUG_MSG
    printf("Table Output: \n");
#endif
    for (pos_vdb i = 0, size = resultTable->size(); i < size; i++) {
#ifdef CONTROLLER_TEST_DEBUG_MSG
        printf("%d: %s -- isDummy? %d\n", i, (*resultTable)[i].toString().c_str(), (*resultTable)[i].isDummy());
#endif
    }
};

/**
 * Query 6
 * Low card two party test query
 * All three joins in Aspirin Profile
 */
static void query6(size_vdb fixchar_size) {

    printf("[INFO] Query 6 initiated.\n");

    // we're honest broker; send queries
    OutsideController::RegisterMachine(1, "localhost:4111");
    OutsideController::RegisterMachine(2, "localhost:4222");

    size_vdb k = (size_vdb) FLAGS_k;

    MachineID machineOneID = 1;
    MachineID machineTwoID = 2;

    //------------------------- left postgres query -----------------------------

    TableID psqlQuery1TableID = 5;
    TableID psqlQuery2TableID = 6;
    const char * psqlQuery1 = "select cast(patient_id % 17 + 100 as integer) id, cast(substring(icd9, 0, 4) as varchar) from diagnoses where patient_id > 1500 and patient_id < 1700 and icd9 ilike 'v2%' order by id limit 12";
    const char * psqlQuery2 = "select cast(patient_id % 17 + 100 as integer) id, cast(pulse as integer) from vitals where patient_id > 1500 and patient_id < 1700 and pulse > 100 order by id limit 20";
    type::RecordSchema psqlQuery1Schema = {
            psqlQuery1TableID, {
                    {type::FieldDataType::Int, INT_FIELD_SIZE},
                    {type::FieldDataType::Fixchar, fixchar_size},
            }
    };
    type::RecordSchema psqlQuery2Schema = {
            psqlQuery2TableID, {
                    {type::FieldDataType::Int, INT_FIELD_SIZE},
                    {type::FieldDataType::Int, INT_FIELD_SIZE},
            }
    };

    std::thread t1_1(sendPSQLQuery, machineOneID, psqlQuery1TableID, psqlQuery1Schema, psqlQuery1, STEP_QUERY);
    std::thread t1_2(sendPSQLQuery, machineOneID, psqlQuery2TableID, psqlQuery2Schema, psqlQuery2, STEP_QUERY);
    std::thread t2_1(sendPSQLQuery, machineTwoID, psqlQuery1TableID, psqlQuery1Schema, psqlQuery1, STEP_QUERY);
    std::thread t2_2(sendPSQLQuery, machineTwoID, psqlQuery2TableID, psqlQuery2Schema, psqlQuery2, STEP_QUERY);
    t1_1.join();
    t1_2.join();
    t2_1.join();
    t2_2.join();

    while (!OutsideController::IsDone()) {
        sleep(1);
    }

    //------------------------- left bottom join -------------------------------
    auto leftJoinSchema  = deployGenJoin(100, 2, k, psqlQuery1Schema, psqlQuery2Schema, {0}, {0}, {0}, {0}, true, STEP_QUERY);


    //------------------------- right postgres query -----------------------------
    TableID psqlQuery3TableID = 7;
    TableID psqlQuery4TableID = 8;
    type::RecordSchema psqlQuery3Schema = {
            psqlQuery3TableID, {
                    {type::FieldDataType::Int, INT_FIELD_SIZE},
            }
    };
    type::RecordSchema psqlQuery4Schema = {
            psqlQuery4TableID, {
                    {type::FieldDataType::Int, INT_FIELD_SIZE},
                    {type::FieldDataType::Int, INT_FIELD_SIZE},
                    {type::FieldDataType::Int, INT_FIELD_SIZE},
            }
    };

    const char * psqlQuery3 = "select cast(patient_id % 17 + 101 as integer) id from medications where patient_id > 1500 and patient_id < 1700 and medication ilike 'd%' order by id limit 10";
    const char * psqlQuery4 = "select cast(patient_id % 17 + 102 as integer) id, max(race), max(gender) from demographics where patient_id > 1500 and patient_id < 1700 group by id order by id limit 17";
    std::thread t1_3(sendPSQLQuery, machineOneID, psqlQuery3TableID, psqlQuery3Schema, psqlQuery3, STEP_QUERY);
    std::thread t1_4(sendPSQLQuery, machineOneID, psqlQuery4TableID, psqlQuery4Schema, psqlQuery4, STEP_QUERY);
    std::thread t2_3(sendPSQLQuery, machineTwoID, psqlQuery3TableID, psqlQuery3Schema, psqlQuery3, STEP_QUERY);
    std::thread t2_4(sendPSQLQuery, machineTwoID, psqlQuery4TableID, psqlQuery4Schema, psqlQuery4, STEP_QUERY);
    t1_3.join();
    t1_4.join();
    t2_3.join();
    t2_4.join();

    while (!OutsideController::IsDone()) {
        sleep(1);
    }

    //------------------------- scan repart and local sort -------------------------------
    auto rightJoinSchema = deployGenJoin(200, 2, k, psqlQuery3Schema, psqlQuery4Schema, {0}, {0}, {0}, {0}, true, STEP_QUERY);

    //------------------------- top join -------------------------------
    deployGenJoin(300, 2, k, leftJoinSchema, rightJoinSchema, {0}, {0}, {0}, {0}, false, FINAL_QUERY);

    //----------------------- result display -------------------

    ObliviousTupleTable * resultTable = &OutsideController::GetResultObliviousTupleTable();
#ifdef CONTROLLER_TEST_DEBUG_MSG
    printf("Table Output: \n");
#endif
    for (pos_vdb i = 0, size = resultTable->size(); i < size; i++) {
#ifdef CONTROLLER_TEST_DEBUG_MSG
//        printf("%03d: %s\n", i, TuplePrinter::convert(&resultTable->getSchema(), (*resultTable)[i]).c_str());
        printf("%d: %s -- isDummy? %d\n", i, (*resultTable)[i].toString().c_str(), (*resultTable)[i].isDummy());
#endif
    }
};

/**
 * Query 7
 * Low card two party test query
 * joining two sets of locally produced tables
 */
static void query7(size_vdb fixchar_size) {

    printf("[INFO] Query 7 initiated.\n");

    // we're honest broker; send queries
    OutsideController::RegisterMachine(1, "localhost:4111");
    OutsideController::RegisterMachine(2, "localhost:4222");

    size_vdb k = (size_vdb) FLAGS_k;

    MachineID machineOneID = 1;
    MachineID machineTwoID = 2;

    struct timespec tim, tim2;
    tim.tv_sec = 0;
    tim.tv_nsec = 500000000L;

    //------------------------- left postgres query -----------------------------

    TableID psqlQuery1TableID = 5;
    TableID psqlQuery2TableID = 6;
    const char * psqlQuery1 = "select * from oinput01";
    const char * psqlQuery2 = "select * from oinput02";
    const char * psqlQuery3 = "select * from oinput11";
    const char * psqlQuery4 = "select * from oinput12";
    type::RecordSchema psqlQuery1Schema = {
            psqlQuery1TableID, {
                    {type::FieldDataType::Int, INT_FIELD_SIZE},
                    {type::FieldDataType::Fixchar, fixchar_size},
                    {type::FieldDataType::Fixchar, fixchar_size},
            }
    };
    type::RecordSchema psqlQuery2Schema = {
            psqlQuery2TableID, {
                    {type::FieldDataType::Int, INT_FIELD_SIZE},
                    {type::FieldDataType::Int, INT_FIELD_SIZE},
                    {type::FieldDataType::Fixchar, fixchar_size},
            }
    };

    std::thread t1_1(sendPSQLQuery, machineOneID, psqlQuery1TableID, psqlQuery1Schema, psqlQuery1, STEP_QUERY);
    nanosleep(&tim , &tim2);
    std::thread t1_2(sendPSQLQuery, machineOneID, psqlQuery2TableID, psqlQuery2Schema, psqlQuery2, STEP_QUERY);
    nanosleep(&tim , &tim2);
    std::thread t2_1(sendPSQLQuery, machineTwoID, psqlQuery1TableID, psqlQuery1Schema, psqlQuery3, STEP_QUERY);
    nanosleep(&tim , &tim2);
    std::thread t2_2(sendPSQLQuery, machineTwoID, psqlQuery2TableID, psqlQuery2Schema, psqlQuery4, STEP_QUERY);
    nanosleep(&tim , &tim2);
    t1_1.join();
    t1_2.join();
    t2_1.join();
    t2_2.join();

    while (!OutsideController::IsDone()) {
        sleep(1);
    }

    //------------------------- top join -------------------------------
    deployGenJoin(100, 2, k, psqlQuery1Schema, psqlQuery2Schema, {0}, {0}, {0}, {0}, false, FINAL_QUERY);

    //----------------------- result display -------------------

    ObliviousTupleTable * resultTable = &OutsideController::GetResultObliviousTupleTable();
#ifdef CONTROLLER_TEST_DEBUG_MSG
    printf("Table Output: \n");
#endif
    for (pos_vdb i = 0, size = resultTable->size(); i < size; i++) {
#ifdef CONTROLLER_TEST_DEBUG_MSG
//        printf("%03d: %s\n", i, TuplePrinter::convert(&resultTable->getSchema(), (*resultTable)[i]).c_str());
        printf("%d: %s -- isDummy? %d\n", i, (*resultTable)[i].toString().c_str(), (*resultTable)[i].isDummy());
#endif
    }
};

/**
 * Query 8
 * High card two party test query, 2007
 * All three joins in Aspirin Profile
 */
static void query8(size_vdb fixchar_size) {

    printf("[INFO] Query 8 initiated.\n");

    // we're honest broker; send queries
    OutsideController::RegisterMachine(1, "localhost:4111");
    OutsideController::RegisterMachine(2, "localhost:4222");

    size_vdb k = (size_vdb) FLAGS_k;

    MachineID machineOneID = 1;
    MachineID machineTwoID = 2;

    //------------------------- left postgres query -----------------------------

    TableID psqlQuery1TableID = 5;
    TableID psqlQuery2TableID = 6;
    const char * psqlQuery1 = "select pid from diagnoses d, pid_matcher m where d.patient_id = m.patient_id and icd9 ilike '414%' and year = 2007 limit 1000";
    const char * psqlQuery2 = "select pid, cast(pulse as integer) from vitals v, pid_matcher m where v.patient_id = m.patient_id and pulse is not null and year = 2007 limit 1000";
    type::RecordSchema psqlQuery1Schema = {
            psqlQuery1TableID, {
                    {type::FieldDataType::Int, INT_FIELD_SIZE},
            }
    };
    type::RecordSchema psqlQuery2Schema = {
            psqlQuery2TableID, {
                    {type::FieldDataType::Int, INT_FIELD_SIZE},
                    {type::FieldDataType::Int, INT_FIELD_SIZE},
            }
    };

    std::thread t1_1(sendPSQLQuery, machineOneID, psqlQuery1TableID, psqlQuery1Schema, psqlQuery1, STEP_QUERY);
    std::thread t1_2(sendPSQLQuery, machineOneID, psqlQuery2TableID, psqlQuery2Schema, psqlQuery2, STEP_QUERY);
    std::thread t2_1(sendPSQLQuery, machineTwoID, psqlQuery1TableID, psqlQuery1Schema, psqlQuery1, STEP_QUERY);
    std::thread t2_2(sendPSQLQuery, machineTwoID, psqlQuery2TableID, psqlQuery2Schema, psqlQuery2, STEP_QUERY);
    t1_1.join();
    t1_2.join();
    t2_1.join();
    t2_2.join();

    while (!OutsideController::IsDone()) {
        sleep(1);
    }

    //------------------------- left bottom join -------------------------------
//    auto leftJoinSchema = deployGenJoin(100, 2, k, psqlQuery1Schema, psqlQuery2Schema, {0}, {0}, {0}, {0}, true, STEP_QUERY);

    TableID startingTID = 100;

    TableID scanID = startingTID + 10;
    TransmitterID merge1TID = startingTID + 20;
    TransmitterID merge2TID = startingTID + 40;
    TableID joinID = startingTID + 60;
    TableID projectID = startingTID + 80;
    db::obdata::ObliviousTupleTable dummyTable1 (psqlQuery1Schema, {});
    db::obdata::ObliviousTupleTable dummyTable2 (psqlQuery2Schema, {});
    auto scan1 = new plan::kaoperators::KAnonymousSeqScan(scanID, k, &dummyTable1);
    auto scan2 = new plan::kaoperators::KAnonymousSeqScan(scanID, k, &dummyTable2);

    auto scan3 = new plan::kaoperators::KAnonymousSeqScan(scanID, k, &dummyTable1);
    auto scan4 = new plan::kaoperators::KAnonymousSeqScan(scanID, k, &dummyTable2);
    auto join1 = new plan::kaoperators::ClusterKAnonymousGenJoin (
            machineOneID, joinID, merge1TID, merge2TID, k, scan1, scan2, {0}, {0}, {0}, {0}, {}, {0, 0}, true, nullptr);
    auto join2 = new plan::kaoperators::ClusterKAnonymousGenJoin(
            machineTwoID, joinID, merge1TID, merge2TID, k, scan3, scan4, {0}, {0}, {0}, {0}, {}, {0, 0}, true, nullptr);

    plan::obexpressions::ObliviousExpressionList expr1 = {
            plan::obexpressions::ObliviousExpressionFactory::makeColumn(0, type::SchemaColumn{type::FieldDataType::Int, sizeof(int)}),
            plan::obexpressions::ObliviousExpressionFactory::makeColumn(2, type::SchemaColumn{type::FieldDataType::Int, sizeof(int)}),
    };
    auto project1 = new plan::kaoperators::KAnonymousProject(projectID, k, join1, expr1);
    auto project2 = new plan::kaoperators::KAnonymousProject(projectID, k, join2, expr1);

    size_vdb encodingSize1;
    size_vdb encodingSize2;
    std::shared_ptr<unsigned char> query1 (
            plan::kaoperators::KAnonymousOperator::EncodeQueryToNewArray(
                    project1, encodingSize1), [](unsigned char * p){delete[] p;});
    std::shared_ptr<unsigned char> query2 (
            plan::kaoperators::KAnonymousOperator::EncodeQueryToNewArray(
                    project2, encodingSize2), [](unsigned char * p){delete[] p;});

    auto leftJoinSchema = type::RecordSchema(project1->getSchema());
    delete project1;
    delete project2;

    std::thread t3(sendSecureQuery, 1, joinID, query1.get(), encodingSize1, STEP_QUERY);
    std::thread t4(sendSecureQuery, 2, joinID, query2.get(), encodingSize2, STEP_QUERY);
    t3.join();
    t4.join();

    while (!OutsideController::IsDone()) {
        sleep(1);
    }




    //------------------------- right postgres query -----------------------------
    TableID psqlQuery3TableID = 7;
    TableID psqlQuery4TableID = 8;
    type::RecordSchema psqlQuery3Schema = {
            psqlQuery3TableID, {
                    {type::FieldDataType::Int, INT_FIELD_SIZE},
            }
    };
    type::RecordSchema psqlQuery4Schema = {
            psqlQuery4TableID, {
                    {type::FieldDataType::Int, INT_FIELD_SIZE},
                    {type::FieldDataType::Int, INT_FIELD_SIZE},
                    {type::FieldDataType::Int, INT_FIELD_SIZE},
            }
    };

    const char * psqlQuery3 = "select pid from medications m, pid_matcher p where m.patient_id = p.patient_id and medication ilike 'aspirin%' and year = 2007 limit 1000";
    const char * psqlQuery4 = "select distinct pid, race, gender from demographics dem, medications med, pid_matcher p where dem.patient_id = med.patient_id and p.patient_id = med.patient_id and year = 2007 limit 1000";
    std::thread t1_3(sendPSQLQuery, machineOneID, psqlQuery3TableID, psqlQuery3Schema, psqlQuery3, STEP_QUERY);
    std::thread t1_4(sendPSQLQuery, machineOneID, psqlQuery4TableID, psqlQuery4Schema, psqlQuery4, STEP_QUERY);
    std::thread t2_3(sendPSQLQuery, machineTwoID, psqlQuery3TableID, psqlQuery3Schema, psqlQuery3, STEP_QUERY);
    std::thread t2_4(sendPSQLQuery, machineTwoID, psqlQuery4TableID, psqlQuery4Schema, psqlQuery4, STEP_QUERY);
    t1_3.join();
    t1_4.join();
    t2_3.join();
    t2_4.join();

    while (!OutsideController::IsDone()) {
        sleep(1);
    }

    //------------------------- scan repart and local sort -------------------------------
//    auto rightJoinSchema = deployGenJoin(200, 2, k, psqlQuery3Schema, psqlQuery4Schema, {0}, {0}, {0}, {0}, true, STEP_QUERY);

    startingTID = 200;

    scanID = startingTID + 10;
    merge1TID = startingTID + 20;
    merge2TID = startingTID + 40;
    joinID = startingTID + 60;
    projectID = startingTID + 80;
    db::obdata::ObliviousTupleTable dummyTable3 (psqlQuery3Schema, {});
    db::obdata::ObliviousTupleTable dummyTable4 (psqlQuery4Schema, {});
    scan1 = new plan::kaoperators::KAnonymousSeqScan(scanID, k, &dummyTable3);
    scan2 = new plan::kaoperators::KAnonymousSeqScan(scanID, k, &dummyTable4);

    scan3 = new plan::kaoperators::KAnonymousSeqScan(scanID, k, &dummyTable3);
    scan4 = new plan::kaoperators::KAnonymousSeqScan(scanID, k, &dummyTable4);
    join1 = new plan::kaoperators::ClusterKAnonymousGenJoin (
            machineOneID, joinID, merge1TID, merge2TID, k, scan1, scan2, {0}, {0}, {0}, {0}, {}, {}, true, nullptr);
    join2 = new plan::kaoperators::ClusterKAnonymousGenJoin(
            machineTwoID, joinID, merge1TID, merge2TID, k, scan3, scan4, {0}, {0}, {0}, {0}, {}, {}, true, nullptr);

    plan::obexpressions::ObliviousExpressionList expr2 = {
            plan::obexpressions::ObliviousExpressionFactory::makeColumn(1, type::SchemaColumn{type::FieldDataType::Int, sizeof(int)}),
            plan::obexpressions::ObliviousExpressionFactory::makeColumn(2, type::SchemaColumn{type::FieldDataType::Int, sizeof(int)}),
            plan::obexpressions::ObliviousExpressionFactory::makeColumn(3, type::SchemaColumn{type::FieldDataType::Int, sizeof(int)}),
    };
    project1 = new plan::kaoperators::KAnonymousProject(projectID, k, join1, expr2);
    project2 = new plan::kaoperators::KAnonymousProject(projectID, k, join2, expr2);

    encodingSize1;
    encodingSize2;
    query1.reset (plan::kaoperators::KAnonymousOperator::EncodeQueryToNewArray(
                    project1, encodingSize1), [](unsigned char * p){delete[] p;});
    query2.reset (plan::kaoperators::KAnonymousOperator::EncodeQueryToNewArray(
                    project2, encodingSize2), [](unsigned char * p){delete[] p;});

    auto rightJoinSchema = type::RecordSchema(project1->getSchema());
    delete project1;
    delete project2;

    std::thread t5(sendSecureQuery, 1, joinID, query1.get(), encodingSize1, STEP_QUERY);
    std::thread t6(sendSecureQuery, 2, joinID, query2.get(), encodingSize2, STEP_QUERY);
    t5.join();
    t6.join();

    while (!OutsideController::IsDone()) {
        sleep(1);
    }




    //------------------------- top join -------------------------------
//    deployGenJoin(300, 2, k, leftJoinSchema, rightJoinSchema, {0}, {0}, {0}, {0}, false, FINAL_QUERY);

    startingTID = 300;

    scanID = startingTID + 10;
    merge1TID = startingTID + 20;
    merge2TID = startingTID + 40;
    joinID = startingTID + 60;
    projectID = startingTID + 80;
    db::obdata::ObliviousTupleTable dummyTable5 (leftJoinSchema, {});
    db::obdata::ObliviousTupleTable dummyTable6 (rightJoinSchema, {});
    scan1 = new plan::kaoperators::KAnonymousSeqScan(scanID, k, &dummyTable5);
    scan2 = new plan::kaoperators::KAnonymousSeqScan(scanID, k, &dummyTable6);

    scan3 = new plan::kaoperators::KAnonymousSeqScan(scanID, k, &dummyTable5);
    scan4 = new plan::kaoperators::KAnonymousSeqScan(scanID, k, &dummyTable6);
    join1 = new plan::kaoperators::ClusterKAnonymousGenJoin (
            machineOneID, joinID, merge1TID, merge2TID, k, scan1, scan2, {0}, {0}, {0}, {0}, {}, {}, true, nullptr);
    join2 = new plan::kaoperators::ClusterKAnonymousGenJoin(
            machineTwoID, joinID, merge1TID, merge2TID, k, scan3, scan4, {0}, {0}, {0}, {0}, {}, {}, true, nullptr);

    plan::obexpressions::ObliviousExpressionList expr3 = {
            plan::obexpressions::ObliviousExpressionFactory::makeColumn(3, type::SchemaColumn{type::FieldDataType::Int, sizeof(int)}),
            plan::obexpressions::ObliviousExpressionFactory::makeColumn(4, type::SchemaColumn{type::FieldDataType::Int, sizeof(int)}),
            plan::obexpressions::ObliviousExpressionFactory::makeColumn(1, type::SchemaColumn{type::FieldDataType::Int, sizeof(int)}),
    };
    project1 = new plan::kaoperators::KAnonymousProject(projectID, k, join1, expr3);
    project2 = new plan::kaoperators::KAnonymousProject(projectID, k, join2, expr3);

    encodingSize1;
    encodingSize2;
    query1.reset (plan::kaoperators::KAnonymousOperator::EncodeQueryToNewArray(
            project1, encodingSize1), [](unsigned char * p){delete[] p;});
    query2.reset (plan::kaoperators::KAnonymousOperator::EncodeQueryToNewArray(
            project2, encodingSize2), [](unsigned char * p){delete[] p;});

    delete project1;
    delete project2;

    std::thread t7(sendSecureQuery, 1, joinID, query1.get(), encodingSize1, FINAL_QUERY);
    std::thread t8(sendSecureQuery, 2, joinID, query2.get(), encodingSize2, FINAL_QUERY);
    t7.join();
    t8.join();

    while (!OutsideController::IsDone()) {
        sleep(1);
    }




    //----------------------- result display -------------------

    ObliviousTupleTable * resultTable = &OutsideController::GetResultObliviousTupleTable();
    size_vdb dummyCount = 0;
    size_vdb realCount = 0;
#ifdef CONTROLLER_TEST_DEBUG_MSG
    printf("Table Output contains %d tuples; including ", resultTable->size());
#endif
    for (pos_vdb i = 0, size = resultTable->size(); i < size; i++) {
        if ((*resultTable)[i].isDummy()) {
            dummyCount ++;
        } else {
            realCount ++;
        }
#ifdef CONTROLLER_TEST_DEBUG_MSG
//        printf("%03d: %s\n", i, TuplePrinter::convert(&resultTable->getSchema(), (*resultTable)[i]).c_str());
//        printf("%d: %s -- isDummy? %d\n", i, (*resultTable)[i].toString().c_str(), (*resultTable)[i].isDummy());
#endif
    }
#ifdef CONTROLLER_TEST_DEBUG_MSG
    printf("%d dummies and %d real tuples; such as: \n", dummyCount, realCount);
    for (pos_vdb i = 0, size = resultTable->size() < 20 ? resultTable->size() : 20; i < size; i++) {
        printf("%s%s\n", TuplePrinter::convert(&resultTable->getSchema(), (*resultTable)[i]).c_str(), (*resultTable)[i].isDummy() ? " (Dummy)" : "");
    }
    if (resultTable->size() < 20) {
        printf("...\n");
    }
#endif
};



/**
 * Query 9
 * Medium card two party test query
 * Bottom left join in aspirin profile
 */
static void query9(size_vdb fixchar_size) {

    printf("[INFO] Query 9 initiated.\n");

    struct timespec tim, tim2;
    tim.tv_sec = 0;
    tim.tv_nsec = 500000000L;

    // we're honest broker; send queries
    OutsideController::RegisterMachine(1, "localhost:4111");
    OutsideController::RegisterMachine(2, "localhost:4222");

    size_vdb k = (size_vdb) FLAGS_k;

    MachineID machineOneID = 1;
    MachineID machineTwoID = 2;

    //------------------------- postgres query -----------------------------

    TableID psqlQuery1TableID = 5;
    TableID psqlQuery2TableID = 6;
    const char * psqlQuery1 = "select distinct pid from diagnoses d, pid_matcher m where d.patient_id = m.patient_id and icd9 ilike '414%' limit 1000";
    const char * psqlQuery2 = "select distinct pid, cast(pulse as integer) from vitals v, pid_matcher m where v.patient_id = m.patient_id limit 1000";

    type::RecordSchema psqlQuery1Schema = {
            psqlQuery1TableID, {
                    {type::FieldDataType::Int, INT_FIELD_SIZE},
            }
    };
    type::RecordSchema psqlQuery2Schema = {
            psqlQuery2TableID, {
                    {type::FieldDataType::Int, INT_FIELD_SIZE},
                    {type::FieldDataType::Int, INT_FIELD_SIZE},
            }
    };

    std::thread t1_1(sendPSQLQuery, machineOneID, psqlQuery1TableID, psqlQuery1Schema, psqlQuery1, STEP_QUERY);
    nanosleep(&tim , &tim2);
    std::thread t2_1(sendPSQLQuery, machineTwoID, psqlQuery1TableID, psqlQuery1Schema, psqlQuery1, STEP_QUERY);
    nanosleep(&tim , &tim2);
    std::thread t1_2(sendPSQLQuery, machineOneID, psqlQuery2TableID, psqlQuery2Schema, psqlQuery2, STEP_QUERY);
    nanosleep(&tim , &tim2);
    std::thread t2_2(sendPSQLQuery, machineTwoID, psqlQuery2TableID, psqlQuery2Schema, psqlQuery2, STEP_QUERY);
    nanosleep(&tim , &tim2);

    t1_1.join();
    t2_1.join();
    t1_2.join();
    t2_2.join();

    while (!OutsideController::IsDone()) {
        sleep(1);
    }

    //------------------------- scan merge and filter -------------------------------

    TableID scan1ID = 10;
    TableID scan2ID = 11;
    TableID joinID = 30;
    TransmitterID merge1TID = 40;
    TransmitterID merge2TID = 50;
    db::obdata::ObliviousTupleTable dummyTable1 (psqlQuery1Schema, {});
    db::obdata::ObliviousTupleTable dummyTable2 (psqlQuery2Schema, {});
    auto scan1 = new plan::kaoperators::KAnonymousSeqScan(scan1ID, k, &dummyTable1);
    auto scan2 = new plan::kaoperators::KAnonymousSeqScan(scan2ID, k, &dummyTable2);

    auto join1 = new plan::kaoperators::ClusterKAnonymousGenJoin(
            machineOneID, joinID, merge1TID, merge2TID, k, scan1, scan2, {0}, {0}, {0}, {0}, {}, {0, 0}, true, nullptr);
    auto join2 = new plan::kaoperators::ClusterKAnonymousGenJoin(
            machineTwoID, joinID, merge1TID, merge2TID, k, scan1, scan2, {0}, {0}, {0}, {0}, {}, {0, 0}, true, nullptr);

    size_vdb encodingSize1;
    size_vdb encodingSize2;
    std::shared_ptr<unsigned char> query1 (
            plan::kaoperators::KAnonymousOperator::EncodeQueryToNewArray(
                    join1, encodingSize1), [](unsigned char * p){delete[] p;});
    std::shared_ptr<unsigned char> query2 (
            plan::kaoperators::KAnonymousOperator::EncodeQueryToNewArray(
                    join2, encodingSize2), [](unsigned char * p){delete[] p;});

    std::thread t3(sendSecureQuery, machineOneID, joinID, query1.get(), encodingSize1, FINAL_QUERY);
    std::thread t4(sendSecureQuery, machineTwoID, joinID, query2.get(), encodingSize2, FINAL_QUERY);

    t3.join();
    t4.join();

    while (!OutsideController::IsDone()) {
        sleep(1);
    }

    //----------------------- result display -------------------

    ObliviousTupleTable * resultTable = &OutsideController::GetResultObliviousTupleTable();
#ifdef CONTROLLER_TEST_DEBUG_MSG
    printf("Table Output: \n");
#endif
    for (pos_vdb i = 0, size = resultTable->size(); i < size; i++) {
#ifdef CONTROLLER_TEST_DEBUG_MSG
        printf("%d: %s -- isDummy? %d\n", i, (*resultTable)[i].toString().c_str(), (*resultTable)[i].isDummy());
#endif
    }
};

/**
 * Query 10
 * Full cardinality Co-morbidity 2006 or another year
 */
static void query10(size_vdb fixchar_size) {

    /**
     * NOTE
     *
     * This query uses the following tables, which needs to be populated using 'cross_host_patient_id_selector.sql'.
     * Make sure to update the year variable in the script.
     */

    // we're honest broker; send queries; we're located on vaultdb01
    size_vdb hostCount = 4;
    size_vdb instancePerHost = 1;
    registerAllMachines(0, hostCount, instancePerHost);
    size_vdb k = (size_vdb) FLAGS_k;
    time_t start_t = time(0);

    printf("[INFO] Query 10, Co-morbidity of year %d, with %d hosts each with %d instances, started.\n", FLAGS_year, hostCount, instancePerHost);



    //------------------------- postgres query -----------------------------

    TableID psqlQueryTableID = 4;
    /**
     * HB can opt to use "vdb01.unsuppressed_patient_id" to exclude certain entities from participating the query
     * Usage is as follows.
     */
    const char * psqlQueryNoYear = "SELECT icd9, cast(count(*) as integer) FROM diagnoses WHERE year = %d AND icd9 <> '008.45' AND patient_id IN (SELECT * FROM cdiff_cohort_%d) GROUP BY icd9";
    char psqlQuery[200];
    sprintf(psqlQuery, psqlQueryNoYear, FLAGS_year, FLAGS_year);

    const char * dummyPsqlQueryNoYear = "SELECT icd9, cast(count(*) as integer) FROM diagnoses WHERE year = %d AND icd9 <> '008.45' AND patient_id IN (SELECT * FROM cdiff_cohort_%d) GROUP BY icd9 LIMIT 0";
    char dummyPsqlQuery[200];
    sprintf(dummyPsqlQuery, dummyPsqlQueryNoYear, FLAGS_year);


    type::RecordSchema psqlQuerySchema = {
            psqlQueryTableID, {
                    {type::FieldDataType::Fixchar, fixchar_size},
                    {type::FieldDataType::Int, sizeof(int)},
            }
    };

    std::vector<std::thread> psqlThreads;
    psqlThreads.reserve(hostCount * instancePerHost);

    timestamp_t psqlStartTime = getTimestamp();
    for (MachineID i = 0, count = hostCount * instancePerHost; i < count; i ++) {
        if (i % instancePerHost == 0) {
            psqlThreads.emplace_back(sendPSQLQuery, i + 1, psqlQueryTableID, psqlQuerySchema, psqlQuery, STEP_QUERY);
        } else {
            // send empty tables
            psqlThreads.emplace_back(sendPSQLQuery, i + 1, psqlQueryTableID, psqlQuerySchema, dummyPsqlQuery, STEP_QUERY);
        }
    }
    for (MachineID i = 0, count = hostCount * instancePerHost; i < count; i ++) {
        psqlThreads[i].join(); // detach?
    }

    while (!OutsideController::IsDone()) {
        // busy waiting
    }
    timestamp_t psqlEndTime = getTimestamp();

    //------------------------- scan, agg -------------------------------

    TableID scanID = 7;
    TableID aggID = 10;
    TransmitterID mergeTID = 20;
    db::obdata::ObliviousTupleTable dummyTable1 (psqlQuerySchema, {});
    std::vector<std::thread> queryThreads;
    queryThreads.reserve(hostCount * instancePerHost);

    type::RecordSchema query1OutputSchema;
    std::vector<std::shared_ptr<unsigned char>> queries;
    std::vector<size_vdb> querySizes;

    timestamp_t query1StartTime = getTimestamp();
    for (MachineID machineID = 1, count = hostCount * instancePerHost; machineID <= count; machineID ++) {
        auto scan = new plan::kaoperators::KAnonymousSeqScan(scanID, k, &dummyTable1);
        plan::kaoperators::ClusterKAnonymousSortedAggregation agg(machineID, aggID,mergeTID,k,scan
                ,{plan::obexpressions::ObliviousExpressionFactory::makeColumn(0, {type::FieldDataType::Fixchar, fixchar_size}),
                  plan::obexpressions::ObliviousExpressionFactory::makeSum(1, {type::FieldDataType::Int, sizeof(int)})}
                ,{0},true,nullptr);
        size_vdb encodingSize;
        queries.emplace_back(plan::kaoperators::KAnonymousOperator::EncodeQueryToNewArray(&agg, encodingSize), [](unsigned char * p){delete[] p;});
        querySizes.push_back(encodingSize);
        queryThreads.emplace_back(sendSecureQuery, machineID, aggID, queries[machineID - 1].get(), querySizes[machineID - 1], STEP_QUERY);
        if (machineID == 1) {
            query1OutputSchema = type::RecordSchema(agg.getSchema());
        }
    }

    for (MachineID i = 0, count = hostCount * instancePerHost; i < count; i ++) {
        queryThreads[i].join(); // detach?
    }

    while (!OutsideController::IsDone()) {
        // busy wait
    }
    timestamp_t query1EndTime = getTimestamp();

    queries.clear();
    querySizes.clear();
    queryThreads.clear();

    //------------------------- sort limit -------------------------------

    scanID = 30;
    TableID sortID = 35;
    TableID limitID = 50;
    mergeTID = 60;
    db::obdata::ObliviousTupleTable dummyTable2 (query1OutputSchema, {});
    queryThreads.reserve(hostCount * instancePerHost);

    timestamp_t query2StartTime = getTimestamp();
    for (MachineID machineID = 1, count = hostCount * instancePerHost; machineID <= count; machineID ++) {
        auto scan = new plan::kaoperators::KAnonymousSeqScan(scanID, k, &dummyTable2);
        auto sort = new plan::kaoperators::KAnonymousBitonicSort(sortID,k,scan,{1},{SortOrder ::ASCEND},nullptr);
        plan::kaoperators::ClusterKAnonymousLimit limit(machineID, limitID, mergeTID, k, sort, 10, {0},{SortOrder ::ASCEND}, 1, nullptr);
        size_vdb encodingSize;
        queries.emplace_back(plan::kaoperators::KAnonymousOperator::EncodeQueryToNewArray(&limit, encodingSize), [](unsigned char * p){delete[] p;});
        querySizes.push_back(encodingSize);
        queryThreads.emplace_back(sendSecureQuery, machineID, limitID, queries[machineID - 1].get(), querySizes[machineID - 1], FINAL_QUERY);
    }

    for (MachineID i = 0, count = hostCount * instancePerHost; i < count; i ++) {
        queryThreads[i].join(); // detach?
    }

    while (!OutsideController::IsDone()) {
        // busy wait
    }
    timestamp_t query2EndTime = getTimestamp();

    //----------------------- result display -------------------

    ObliviousTupleTable * resultTable = &OutsideController::GetResultObliviousTupleTable();
#ifdef CONTROLLER_TEST_DEBUG_MSG
    printf("Table Output: \n");
#endif
    for (pos_vdb i = 0, size = resultTable->size(); i < size; i++) {
#ifdef CONTROLLER_TEST_DEBUG_MSG
        printf("%d: %s -- isDummy? %d\n", i, (*resultTable)[i].toString().c_str(), (*resultTable)[i].isDummy());
#endif
    }
    printf("[REPORT] Query 10 of year %d, %d hosts, %d instances per host: \n- psql:       %.7lf\n- query 1:    %.7lf\n- query 2:    %.7lf\n- end-to-end: %.7lf\n",
           FLAGS_year, hostCount, instancePerHost,
           double(psqlEndTime - psqlStartTime)/TS_T_PER_SEC,
           double(query1EndTime - query1StartTime)/TS_T_PER_SEC,
           double(query2EndTime - query2StartTime)/TS_T_PER_SEC,
           double(query2EndTime - psqlStartTime)/TS_T_PER_SEC);
    OutsideController::HBDataEntry(start_t, 10, k, "diagnoses", double(psqlEndTime - psqlStartTime) / TS_T_PER_SEC, "");
    OutsideController::HBDataEntry(start_t, 10, k, "agg", double(query1EndTime - query1StartTime) / TS_T_PER_SEC, "");
    OutsideController::HBDataEntry(start_t, 10, k, "sort limit", double(query2EndTime - query2StartTime) / TS_T_PER_SEC, "");
    OutsideController::HBDataEntry(start_t, 10, k, "end to end", double(query2EndTime - psqlStartTime) / TS_T_PER_SEC,
                                   std::to_string(hostCount)+std::string(" hosts ")+std::to_string(instancePerHost)+
                                   std::string(" instance each, year ") + std::to_string(FLAGS_year));
};


/**
 * Query 11
 * Full cardinality C.Diff
 */
static void query11(size_vdb fixchar_size) {

    /**
     * NOTE
     *
     * This query uses the following tables, which needs to be populated using 'cross_host_patient_id_selector.sql'.
     * Make sure to update the year variable in the script.
     */

    // we're honest broker; send queries; we're located on vaultdb01
    size_vdb hostCount = 4;
    size_vdb instancePerHost = 1;
    registerAllMachines(0, hostCount, instancePerHost);
    size_vdb k = (size_vdb) FLAGS_k;
    time_t start_t = time(0);

    printf("[INFO] Query 11. CDiff of year %d, with %d hosts each with %d instances, started.\n", FLAGS_year, hostCount, instancePerHost);


    //------------------------- postgres query -----------------------------

    TableID psqlQueryTableID = 4;
    const char * psqlQueryNoYear = "SELECT pid, timestamp_ FROM diagnoses d, pid_matcher m WHERE year = %d AND icd9 = '008.45' AND d.patient_id = m.patient_id";
    char psqlQuery[200];
    sprintf(psqlQuery, psqlQueryNoYear, FLAGS_year);

    const char * dummyPsqlQueryNoYear = "SELECT pid, timestamp_ FROM diagnoses d, pid_matcher m WHERE year = %d AND icd9 = '008.45' AND d.patient_id = m.patient_id limit 0";
    char dummyPsqlQuery[200];
    sprintf(dummyPsqlQuery, dummyPsqlQueryNoYear, FLAGS_year);

    type::RecordSchema psqlQuerySchema = {
            psqlQueryTableID, {
                    {type::FieldDataType::Int, sizeof(int)},
                    {type::FieldDataType::TimestampNoZone, sizeof(time_t)},
            }
    };

    std::vector<std::thread> psqlThreads;
    psqlThreads.reserve(hostCount * instancePerHost);

    timestamp_t psqlStartTime = getTimestamp();
    for (MachineID i = 0, count = hostCount * instancePerHost; i < count; i ++) {
        if (i % instancePerHost == 0) {
            psqlThreads.emplace_back(sendPSQLQuery, i + 1, psqlQueryTableID, psqlQuerySchema, psqlQuery, STEP_QUERY);
        } else {
            // send empty tables
            psqlThreads.emplace_back(sendPSQLQuery, i + 1, psqlQueryTableID, psqlQuerySchema, dummyPsqlQuery, STEP_QUERY);
        }
    }
    for (MachineID i = 0, count = hostCount * instancePerHost; i < count; i ++) {
        psqlThreads[i].join(); // detach?
    }

    while (!OutsideController::IsDone()) {
        // busy waiting
    }
    timestamp_t psqlEndTime = getTimestamp();

    //------------------------- scan, agg -------------------------------

    TableID scanID = 7;
    TableID aggID = 8;
    TransmitterID mergeTID = 20;
    db::obdata::ObliviousTupleTable dummyTable1 (psqlQuerySchema, {});
    std::vector<std::thread> queryThreads;
    queryThreads.reserve(hostCount * instancePerHost);

    type::RecordSchema query1OutputSchema;
    std::vector<std::shared_ptr<unsigned char>> queries;
    std::vector<size_vdb> querySizes;

    timestamp_t query1StartTime = getTimestamp();
    for (MachineID machineID = 1, count = hostCount * instancePerHost; machineID <= count; machineID ++) {
        auto scan = new plan::kaoperators::KAnonymousSeqScan(scanID, k, &dummyTable1);
        plan::kaoperators::ClusterKAnonymousWindow agg(
                machineID, aggID, mergeTID, k, scan,
               {plan::obexpressions::ObliviousExpressionFactory::makeColumn(0, {type::FieldDataType::Int, sizeof(int)}),
                plan::obexpressions::ObliviousExpressionFactory::makeColumn(1, {type::FieldDataType::TimestampNoZone, sizeof(time_t)}),
                plan::obexpressions::ObliviousExpressionFactory::makeRowNumber()},
               {0}, {1}, {SortOrder::ASCEND}, true, nullptr);
        size_vdb encodingSize;
        queries.emplace_back(plan::kaoperators::KAnonymousOperator::EncodeQueryToNewArray(&agg, encodingSize), [](unsigned char * p){delete[] p;});
        querySizes.push_back(encodingSize);
        queryThreads.emplace_back(sendSecureQuery, machineID, aggID, queries[machineID - 1].get(), querySizes[machineID - 1], STEP_QUERY);
        if (machineID == 1) {
            query1OutputSchema = type::RecordSchema(agg.getSchema());
        }
    }

    for (MachineID i = 0, count = hostCount * instancePerHost; i < count; i ++) {
        queryThreads[i].join(); // detach?
    }

    while (!OutsideController::IsDone()) {
        // busy wait
    }
    timestamp_t query1EndTime = getTimestamp();

    queries.clear();
    querySizes.clear();
    queryThreads.clear();

    //------------------------- self join -------------------------------

    scanID = 30;
    TableID scan2ID = 31;
    TableID projectID = 32;
    TableID project2ID = 33;
    TableID joinID = 35;
    mergeTID = 45;
    TransmitterID merge2TID = 55;
    TableID filterID = 65;
    db::obdata::ObliviousTupleTable dummyTable2 (query1OutputSchema, {});
    queryThreads.reserve(hostCount * instancePerHost);

    auto filterPred =  plan::obexpressions::ObliviousExpressionFactory::makeAnd(
            plan::obexpressions::ObliviousExpressionFactory::makeGTE(
                    plan::obexpressions::ObliviousExpressionFactory::makeSub(
                            plan::obexpressions::ObliviousExpressionFactory::makeColumn(4, {type::FieldDataType::TimestampNoZone, sizeof(time_t)}),
                            plan::obexpressions::ObliviousExpressionFactory::makeColumn(1, {type::FieldDataType::TimestampNoZone, sizeof(time_t)})
                    ),
                    plan::obexpressions::ObliviousExpressionFactory::makeLiteral(
                            db::obdata::obfield::ObliviousField::makeObliviousIntField(15 * 3600 * 24), sizeof(int))
            ),
            plan::obexpressions::ObliviousExpressionFactory::makeLTE(
                    plan::obexpressions::ObliviousExpressionFactory::makeSub(
                            plan::obexpressions::ObliviousExpressionFactory::makeColumn(4, {type::FieldDataType::TimestampNoZone, sizeof(time_t)}),
                            plan::obexpressions::ObliviousExpressionFactory::makeColumn(1, {type::FieldDataType::TimestampNoZone, sizeof(time_t)})
                    ),
                    plan::obexpressions::ObliviousExpressionFactory::makeLiteral(
                            db::obdata::obfield::ObliviousField::makeObliviousIntField(56 * 3600 * 24), sizeof(int))
            )
    );

    type::RecordSchema query2OutputSchema;
    timestamp_t query2StartTime = getTimestamp();
    for (MachineID machineID = 1, count = hostCount * instancePerHost; machineID <= count; machineID ++) {
        auto scan1 = new plan::kaoperators::KAnonymousSeqScan(scanID, k, &dummyTable2);
        auto scan2 = new plan::kaoperators::KAnonymousSeqScan(scan2ID, k, &dummyTable2);
        auto project = new plan::kaoperators::KAnonymousProject(projectID, k, scan2, {
                plan::obexpressions::ObliviousExpressionFactory::makeColumn(0, {type::FieldDataType::Int, sizeof(int)}),
                plan::obexpressions::ObliviousExpressionFactory::makeColumn(1, {type::FieldDataType::TimestampNoZone, sizeof(time_t)}),
                plan::obexpressions::ObliviousExpressionFactory::makeSub(
                        plan::obexpressions::ObliviousExpressionFactory::makeColumn(2, {type::FieldDataType::Int, sizeof(int)}),
                        plan::obexpressions::ObliviousExpressionFactory::makeLiteral(
                                db::obdata::obfield::ObliviousField::makeObliviousIntField(1), sizeof(int)))
        });
        auto join = new plan::kaoperators::ClusterKAnonymousGenJoin (machineID, joinID, mergeTID, merge2TID, k, scan1, project,
                                                                    {0, 2}, {0, 2}, {0}, {0}, {}, {}, false, nullptr);
        auto filter = new plan::kaoperators::KAnonymousLocalGenFilter(filterID, k , join, filterPred, filterPred, {0}, {9, 9, 9, 9, 9, 9}, nullptr);
        plan::kaoperators::KAnonymousProject project2(project2ID,k, filter, {
                plan::obexpressions::ObliviousExpressionFactory::makeColumn(0, {type::FieldDataType::Int, sizeof(int)})
        });
        size_vdb encodingSize;
        queries.emplace_back(plan::kaoperators::KAnonymousOperator::EncodeQueryToNewArray(&project2, encodingSize), [](unsigned char * p){delete[] p;});
        querySizes.push_back(encodingSize);
        queryThreads.emplace_back(sendSecureQuery, machineID, project2ID, queries[machineID - 1].get(), querySizes[machineID - 1], STEP_QUERY);
        if (machineID == 1) {
            query2OutputSchema = type::RecordSchema(project2.getSchema());
        }
    }

    for (MachineID i = 0, count = hostCount * instancePerHost; i < count; i ++) {
        queryThreads[i].join(); // detach?
    }

    while (!OutsideController::IsDone()) {
        // busy wait
    }
    timestamp_t query2EndTime = getTimestamp();

    queries.clear();
    querySizes.clear();
    queryThreads.clear();


    //------------------------- distinct -------------------------------

    scanID = 30;
    TableID distinctID = 75;
    mergeTID = 80;
    db::obdata::ObliviousTupleTable dummyTable3 (query2OutputSchema, {});
    queryThreads.reserve(hostCount * instancePerHost);

    timestamp_t query3StartTime = getTimestamp();
    for (MachineID machineID = 1, count = hostCount * instancePerHost; machineID <= count; machineID ++) {
        auto scan = new plan::kaoperators::KAnonymousSeqScan(scanID, k, &dummyTable3);
        plan::kaoperators::ClusterKAnonymousSortedAggregation distinct(machineID, distinctID, mergeTID,k, scan, {
                plan::obexpressions::ObliviousExpressionFactory::makeColumn(0, {type::FieldDataType::Int, sizeof(int)})
        }, {0}, false, nullptr);
        size_vdb encodingSize;
        queries.emplace_back(plan::kaoperators::KAnonymousOperator::EncodeQueryToNewArray(&distinct, encodingSize), [](unsigned char * p){delete[] p;});
        querySizes.push_back(encodingSize);
        queryThreads.emplace_back(sendSecureQuery, machineID, distinctID, queries[machineID - 1].get(), querySizes[machineID - 1], FINAL_QUERY);
    }

    for (MachineID i = 0, count = hostCount * instancePerHost; i < count; i ++) {
        queryThreads[i].join(); // detach?
    }

    while (!OutsideController::IsDone()) {
        // busy wait
    }
    timestamp_t query3EndTime = getTimestamp();


    //----------------------- result display -------------------

    ObliviousTupleTable * resultTable = &OutsideController::GetResultObliviousTupleTable();
#ifdef CONTROLLER_TEST_DEBUG_MSG
    printf("Table Output: \n");
#endif
    for (pos_vdb i = 0, size = resultTable->size(); i < size; i++) {
#ifdef CONTROLLER_TEST_DEBUG_MSG
        printf("%d: %s -- isDummy? %d\n", i, TuplePrinter::convert(&resultTable->getSchema(), (*resultTable)[i]).c_str(), (*resultTable)[i].isDummy());
#endif
    }
    printf("[REPORT] Query 11 of year %d, %d hosts, %d instances per host: \n- psql:       %f\n- query 1:    %f\n- query 2:    %f\n- query 3:    %f\n- end-to-end: %f\n",
           FLAGS_year, hostCount, instancePerHost,
           double(psqlEndTime - psqlStartTime)/TS_T_PER_SEC,
           double(query1EndTime - query1StartTime)/TS_T_PER_SEC,
           double(query2EndTime - query2StartTime)/TS_T_PER_SEC,
           double(query3EndTime - query3StartTime)/TS_T_PER_SEC,
           double(query3EndTime - psqlStartTime)/TS_T_PER_SEC);
    OutsideController::HBDataEntry(start_t, 11, k, "diagnoses", double(psqlEndTime - psqlStartTime) / TS_T_PER_SEC, "");
    OutsideController::HBDataEntry(start_t, 11, k, "agg", double(query1EndTime - query1StartTime) / TS_T_PER_SEC, "");
    OutsideController::HBDataEntry(start_t, 11, k, "self join", double(query2EndTime - query2StartTime) / TS_T_PER_SEC, "");
    OutsideController::HBDataEntry(start_t, 11, k, "distinct", double(query3EndTime - query3StartTime) / TS_T_PER_SEC, "");
    OutsideController::HBDataEntry(start_t, 11, k, "end to end", double(query3EndTime - psqlStartTime) / TS_T_PER_SEC,
                                                        std::to_string(hostCount)+std::string(" hosts ")+std::to_string(instancePerHost)+
                                                        std::string(" instance each, year ") + std::to_string(FLAGS_year));
};







static void queryAndIngestFromPostgres(size_vdb hostCount, size_vdb instancePerHost, type::RecordSchema schema, const char * query, const char * outputColumns) {
    std::vector<std::thread> psqlThreads;
    psqlThreads.reserve(hostCount * instancePerHost);
    char psqlBuffer[1500];
    for (MachineID i = 0, count = hostCount * instancePerHost; i < count; i ++) {
        sprintf(psqlBuffer, "WITH ___a AS (%s), ___b AS (SELECT *, row_number() over () ___r FROM ___a) SELECT %s FROM ___b WHERE ___r %% %d + 1 = %d %s",
            query, outputColumns, instancePerHost, i % instancePerHost + 1, i % instancePerHost == 0 ? "" : "LIMIT 0");
        psqlThreads.emplace_back(sendPSQLQuery, i + 1, schema.getTableID(), schema, psqlBuffer, STEP_QUERY);
    }
    for (MachineID i = 0, count = hostCount * instancePerHost; i < count; i ++) {
        psqlThreads[i].join(); // detach?
    }
    while (!OutsideController::IsDone()) {
        // busy waiting
    }
}

static TableID doGenJoin(
        size_vdb hostCount, size_vdb instancePerHost, TableID opIDBase,
        type::RecordSchema & outputSchema,
        type::RecordSchema & inputSchema1,
        type::RecordSchema & inputSchema2,
        plan::obexpressions::ObliviousExpressionList projectExprs,
        std::vector<pos_vdb> leftJoinAttr,
        std::vector<pos_vdb> rightJoinAttr,
        std::vector<pos_vdb> leftEntities,
        std::vector<pos_vdb> rightEntities,
        std::vector<size_vdb> leftImposedLevels,
        std::vector<size_vdb> rightImposedLevels,
        bool isSortingRepartition,
        size_vdb k,
        int step_or_final
) {
    TableID scanID = opIDBase;
    TableID scan2ID = opIDBase + 1;
    TableID joinID = opIDBase + 10;
    TableID projectID = opIDBase + 32;
    TransmitterID merge1TID= opIDBase + 20;
    TransmitterID merge2TID = opIDBase + 30;

    std::vector<std::shared_ptr<unsigned char>> queries;
    std::vector<size_vdb> querySizes;
    std::vector<std::thread> queryThreads;
    queryThreads.reserve(hostCount * instancePerHost);

    db::obdata::ObliviousTupleTable dummyTable1 (inputSchema1, {});
    db::obdata::ObliviousTupleTable dummyTable2 (inputSchema2, {});

    for (MachineID machineID = 1, count = hostCount * instancePerHost; machineID <= count; machineID ++) {
        auto scan1 = new plan::kaoperators::KAnonymousSeqScan(scanID, k, &dummyTable1);
        auto scan2 = new plan::kaoperators::KAnonymousSeqScan(scan2ID, k, &dummyTable2);
        auto join = new plan::kaoperators::ClusterKAnonymousGenJoin (machineID, joinID, merge1TID, merge2TID, k, scan1, scan2,
                                                                     leftJoinAttr, rightJoinAttr, leftEntities, rightEntities,
                                                                     leftImposedLevels, rightImposedLevels,
                                                                     isSortingRepartition, nullptr);

        plan::kaoperators::KAnonymousProject project(projectID,k, join, projectExprs);
        size_vdb encodingSize;
        queries.emplace_back(plan::kaoperators::KAnonymousOperator::EncodeQueryToNewArray(&project, encodingSize), [](unsigned char * p){delete[] p;});
        querySizes.push_back(encodingSize);
        queryThreads.emplace_back(sendSecureQuery, machineID, projectID, queries[machineID - 1].get(), querySizes[machineID - 1], step_or_final);
        if (machineID == 1) {
            outputSchema = type::RecordSchema(project.getSchema());
        }
    }

    for (MachineID i = 0, count = hostCount * instancePerHost; i < count; i ++) {
        queryThreads[i].join(); // detach?
    }

    while (!OutsideController::IsDone()) {
        // busy wait
    }
    return opIDBase + 40;
};


/**
 * Query 12
 * Full cardinality Aspirin Profile
 */
static void query12(size_vdb fixchar_size) {

    /**
     * NOTE
     *
     * This query uses the following tables, which needs to be populated using 'cross_host_patient_id_selector.sql'.
     * Make sure to update the year variable in the script.
     */

    // we're honest broker; send queries; we're located on vaultdb01
    size_vdb hostCount = 4;
    size_vdb instancePerHost = 1;
    size_vdb k = (size_vdb) FLAGS_k;
    time_t start_t = time(0);
    TableID baseID = 10;

    printf("[INFO] Query 12. Aspirin Profile of year %d, with %d hosts each with %d instances, started.\n", FLAGS_year, hostCount, instancePerHost);
    registerAllMachines(0, hostCount, instancePerHost);


    //------------------------- postgres query 1 -----------------------------

    TableID psqlQuery1TableID = 3;
    const char * psqlQuery1NoYear = "SELECT pid FROM diagnoses d, pid_matcher m, vdb01.patient_id_buffer_1 u WHERE d.patient_id = m.patient_id AND d.patient_id = u.patient_id AND year = %d AND icd9 ILIKE '414%%'";
    char psqlQuery1[1000];
    sprintf(psqlQuery1, psqlQuery1NoYear, FLAGS_year);
    type::RecordSchema psqlQuery1Schema = {
            psqlQuery1TableID, {
                    {type::FieldDataType::Int, INT_FIELD_SIZE},
            }
    };

    timestamp_t psql1StartTime = getTimestamp();
    queryAndIngestFromPostgres(hostCount, instancePerHost, psqlQuery1Schema, psqlQuery1, "pid");
    timestamp_t psql1EndTime = getTimestamp();

    //------------------------- postgres query 2 -----------------------------

    TableID psqlQuery2TableID = 4;
    const char * psqlQuery2NoYear = "SELECT pid, cast(pulse as integer) FROM vitals v, pid_matcher m, vdb01.patient_id_buffer_2 u WHERE year = %d AND v.patient_id = m.patient_id AND v.patient_id = u.patient_id AND pulse IS NOT NULL";
    char psqlQuery2[1000];
    sprintf(psqlQuery2, psqlQuery2NoYear, FLAGS_year);
    type::RecordSchema psqlQuery2Schema = {
            psqlQuery2TableID, {
                    {type::FieldDataType::Int, INT_FIELD_SIZE},
                    {type::FieldDataType::Int, INT_FIELD_SIZE},
            }
    };

    timestamp_t psql2StartTime = getTimestamp();
    queryAndIngestFromPostgres(hostCount, instancePerHost, psqlQuery2Schema, psqlQuery2, "pid, pulse");
    timestamp_t psql2EndTime = getTimestamp();

    //------------------------- bottom left join -------------------------------

    type::RecordSchema query1OutputSchema;
    plan::obexpressions::ObliviousExpressionList bottomLeftProjectColumns = {
            plan::obexpressions::ObliviousExpressionFactory::makeColumn(1, {type::FieldDataType::Int, sizeof(int)}),
            plan::obexpressions::ObliviousExpressionFactory::makeColumn(2, {type::FieldDataType::Int, sizeof(int)})
    };

    timestamp_t query1StartTime = getTimestamp();
    baseID = doGenJoin(hostCount, instancePerHost, baseID,
              query1OutputSchema, psqlQuery1Schema, psqlQuery2Schema, bottomLeftProjectColumns,
              {0}, {0}, {0}, {0}, {}, {0, 0}, true, k, STEP_QUERY);
    timestamp_t query1EndTime = getTimestamp();




    //------------------------- postgres query 3 -----------------------------

    TableID psqlQuery3TableID = 5;
    const char * psqlQuery3NoYear = "SELECT pid FROM medications d, pid_matcher m, vdb01.patient_id_buffer_3 u WHERE d.patient_id = m.patient_id AND d.patient_id = u.patient_id AND year = %d AND medication ILIKE '%%aspirin%%'";
    char psqlQuery3[1000];
    sprintf(psqlQuery3, psqlQuery3NoYear, FLAGS_year);
    type::RecordSchema psqlQuery3Schema = {
            psqlQuery3TableID, {
                    {type::FieldDataType::Int, INT_FIELD_SIZE},
            }
    };

    timestamp_t psql3StartTime = getTimestamp();
    queryAndIngestFromPostgres(hostCount, instancePerHost, psqlQuery3Schema, psqlQuery3, "pid");
    timestamp_t psql3EndTime = getTimestamp();

    //------------------------- postgres query 4 -----------------------------

    TableID psqlQuery4TableID = 6;
    const char * psqlQuery4 = "SELECT pid, gender, race FROM demographics d, pid_matcher m, vdb01.patient_id_buffer_3 u WHERE d.patient_id = m.patient_id AND d.patient_id = u.patient_id";
    type::RecordSchema psqlQuery4Schema = {
            psqlQuery4TableID, {
                    {type::FieldDataType::Int, INT_FIELD_SIZE},
                    {type::FieldDataType::Int, INT_FIELD_SIZE},
                    {type::FieldDataType::Int, INT_FIELD_SIZE},
            }
    };

    timestamp_t psql4StartTime = getTimestamp();
    queryAndIngestFromPostgres(hostCount, instancePerHost, psqlQuery4Schema, psqlQuery4, "pid, gender, race");
    timestamp_t psql4EndTime = getTimestamp();

    //------------------------- bottom right join -------------------------------

    type::RecordSchema query2OutputSchema;
    plan::obexpressions::ObliviousExpressionList bottomRightProjectColumns = {
            plan::obexpressions::ObliviousExpressionFactory::makeColumn(1, {type::FieldDataType::Int, sizeof(int)}),
            plan::obexpressions::ObliviousExpressionFactory::makeColumn(2, {type::FieldDataType::Int, sizeof(int)}),
            plan::obexpressions::ObliviousExpressionFactory::makeColumn(3, {type::FieldDataType::Int, sizeof(int)})
    };

    timestamp_t query2StartTime = getTimestamp();
    baseID = doGenJoin(hostCount, instancePerHost, baseID,
                       query2OutputSchema, psqlQuery3Schema, psqlQuery4Schema, bottomRightProjectColumns,
                       {0}, {0}, {0}, {0}, {}, {}, true, k, STEP_QUERY);
    timestamp_t query2EndTime = getTimestamp();

    //------------------------- top join -------------------------------

    type::RecordSchema query3OutputSchema;
    plan::obexpressions::ObliviousExpressionList topProjectColumns = {
            plan::obexpressions::ObliviousExpressionFactory::makeColumn(3, {type::FieldDataType::Int, sizeof(int)}),
            plan::obexpressions::ObliviousExpressionFactory::makeColumn(4, {type::FieldDataType::Int, sizeof(int)}),
            plan::obexpressions::ObliviousExpressionFactory::makeColumn(1, {type::FieldDataType::Int, sizeof(int)})
    };

    timestamp_t query3StartTime = getTimestamp();
    baseID = doGenJoin(hostCount, instancePerHost, baseID,
                       query3OutputSchema, query1OutputSchema, query2OutputSchema, topProjectColumns,
                       {0}, {0}, {0}, {0}, {}, {}, false, k, STEP_QUERY);
    timestamp_t query3EndTime = getTimestamp();



    //------------------------- bottom agg -------------------------------

    TableID scanID = baseID;
    TableID bottomAggID = baseID + 5;
    TransmitterID mergeTID = baseID + 15;
    db::obdata::ObliviousTupleTable dummyTable3 (query3OutputSchema, {});

    std::vector<std::shared_ptr<unsigned char>> queries;
    std::vector<size_vdb> querySizes;
    std::vector<std::thread> queryThreads;
    queryThreads.reserve(hostCount * instancePerHost);

    type::RecordSchema query4OutputSchema;

    timestamp_t query4StartTime = getTimestamp();
    for (MachineID machineID = 1, count = hostCount * instancePerHost; machineID <= count; machineID ++) {
        auto scan = new plan::kaoperators::KAnonymousSeqScan(scanID, k, &dummyTable3);
        // This is a performance killer... bitonic sort on the input
//        plan::kaoperators::ClusterKAnonymousSortedAggregation bottomAgg(machineID, bottomAggID, mergeTID,k, scan, {
//                plan::obexpressions::ObliviousExpressionFactory::makeColumn(0, {type::FieldDataType::Int, sizeof(int)}),
//                plan::obexpressions::ObliviousExpressionFactory::makeColumn(1, {type::FieldDataType::Int, sizeof(int)}),
//                plan::obexpressions::ObliviousExpressionFactory::makeSum(2, {type::FieldDataType::Int, sizeof(int)}),
//                plan::obexpressions::ObliviousExpressionFactory::makeCount(2),
//        }, {0, 1}, false, nullptr);

        plan::kaoperators::ClusterKAnonymousBinnedAggregation bottomAgg(machineID, bottomAggID, mergeTID,k, scan, {
                plan::obexpressions::ObliviousExpressionFactory::makeColumn(0, {type::FieldDataType::Int, sizeof(int)}),
                plan::obexpressions::ObliviousExpressionFactory::makeColumn(1, {type::FieldDataType::Int, sizeof(int)}),
                plan::obexpressions::ObliviousExpressionFactory::makeSum(2, {type::FieldDataType::Int, sizeof(int)}),
                plan::obexpressions::ObliviousExpressionFactory::makeCount(2),
        }, {0, 1}, false, true, nullptr);

        size_vdb encodingSize;
        queries.emplace_back(plan::kaoperators::KAnonymousOperator::EncodeQueryToNewArray(&bottomAgg, encodingSize), [](unsigned char * p){delete[] p;});
        querySizes.push_back(encodingSize);
        queryThreads.emplace_back(sendSecureQuery, machineID, bottomAggID, queries[machineID - 1].get(), querySizes[machineID - 1], STEP_QUERY);

        if (machineID == 1) {
            query4OutputSchema = type::RecordSchema(bottomAgg.getSchema());
        }
    }

    for (MachineID i = 0, count = hostCount * instancePerHost; i < count; i ++) {
        queryThreads[i].join(); // detach?
    }

    while (!OutsideController::IsDone()) {
        // busy wait
    }
    baseID += 25;
    timestamp_t query4EndTime = getTimestamp();


    queries.clear();
    querySizes.clear();
    queryThreads.clear();


    //------------------------- postgres query 5 -----------------------------

    TableID psqlQuery5TableID = 7;
    const char * psqlQuery5NoYear = "WITH a AS (SELECT pid FROM diagnoses t, pid_matcher m WHERE t.patient_id = m.patient_id AND year = %d AND icd9 ILIKE '414%%' AND t.patient_id NOT IN (SELECT * FROM vdb01.patient_id_buffer_1)), b AS (SELECT pid, pulse FROM vitals t, pid_matcher m WHERE t.patient_id = m.patient_id AND year = %d AND t.patient_id NOT IN (SELECT * FROM vdb01.patient_id_buffer_2)), c AS (SELECT pid FROM medications t, pid_matcher m WHERE t.patient_id = m.patient_id AND year = %d AND medication ILIKE '%%aspirin%%' AND t.patient_id NOT IN (SELECT * FROM vdb01.patient_id_buffer_3)), d AS (SELECT DISTINCT pid, gender, race FROM demographics t, pid_matcher m WHERE t.patient_id = m.patient_id ) SELECT gender, race, cast(sum(pulse) AS integer) sum_pulse, cast(count(pulse) as integer) count_pulse FROM a, b, c, d WHERE a.pid = b.pid AND c.pid = d.pid AND a.pid = d.pid AND a.pid = c.pid GROUP BY gender, race";
    char psqlQuery5[2000];
    sprintf(psqlQuery5, psqlQuery5NoYear, FLAGS_year, FLAGS_year, FLAGS_year);
    type::RecordSchema psqlQuery5Schema = {
            psqlQuery5TableID, {
                    {type::FieldDataType::Int, INT_FIELD_SIZE},
                    {type::FieldDataType::Int, INT_FIELD_SIZE},
                    {type::FieldDataType::Int, INT_FIELD_SIZE},
                    {type::FieldDataType::Int, INT_FIELD_SIZE},
            }
    };

    timestamp_t psql5StartTime = getTimestamp();
    queryAndIngestFromPostgres(hostCount, instancePerHost, psqlQuery5Schema, psqlQuery5, "gender, race, sum_pulse, count_pulse");
    timestamp_t psql5EndTime = getTimestamp();

    //------------------------- top agg -------------------------------

    scanID = baseID;
    TableID scanID2 = baseID + 1;
    TableID unionID = baseID + 2;
    TransmitterID merge2ID = baseID + 3;
    TableID topAggID = baseID + 6;
    mergeTID = baseID + 16;
    TableID projectID = baseID + 22;

    db::obdata::ObliviousTupleTable dummyTable4 (query4OutputSchema, {});
    db::obdata::ObliviousTupleTable dummyTable5 (psqlQuery5Schema, {});

    queryThreads.reserve(hostCount * instancePerHost);
    type::RecordSchema query5OutputSchema;

    timestamp_t query5StartTime = getTimestamp();
    for (MachineID machineID = 1, count = hostCount * instancePerHost; machineID <= count; machineID ++) {
        auto scan1 = new plan::kaoperators::KAnonymousSeqScan(scanID, k, &dummyTable4);
        auto scan2 = new plan::kaoperators::KAnonymousSeqScan(scanID2, k, &dummyTable5);
        auto repart = new plan::kaoperators::ClusterKAnonymousRepartition(machineID, 0, merge2ID, k, scan2, {0, 1}, {0, 1}, {}, true, nullptr);
        auto union_ = new plan::kaoperators::KAnonymousUnion(unionID, k, {scan1, repart});
//        plan::kaoperators::ClusterKAnonymousSortedAggregation topAgg(machineID, topAggID, mergeTID,k, union_, {
//                plan::obexpressions::ObliviousExpressionFactory::makeColumn(0, {type::FieldDataType::Int, sizeof(int)}),
//                plan::obexpressions::ObliviousExpressionFactory::makeColumn(1, {type::FieldDataType::Int, sizeof(int)}),
//                plan::obexpressions::ObliviousExpressionFactory::makeSum(2, {type::FieldDataType::Int, sizeof(int)}),
//                plan::obexpressions::ObliviousExpressionFactory::makeSum(3, {type::FieldDataType::Int, sizeof(int)}),
//        }, {0, 1}, false, nullptr);

        plan::kaoperators::ClusterKAnonymousBinnedAggregation topAgg(machineID, topAggID, mergeTID,k, union_, {
                plan::obexpressions::ObliviousExpressionFactory::makeColumn(0, {type::FieldDataType::Int, sizeof(int)}),
                plan::obexpressions::ObliviousExpressionFactory::makeColumn(1, {type::FieldDataType::Int, sizeof(int)}),
                plan::obexpressions::ObliviousExpressionFactory::makeSum(2, {type::FieldDataType::Int, sizeof(int)}),
                plan::obexpressions::ObliviousExpressionFactory::makeSum(3, {type::FieldDataType::Int, sizeof(int)}),
        }, {0, 1}, false, true, nullptr);

        // arithmetic problem dividing by zero for dummy tuple
//        plan::kaoperators::KAnonymousProject project (projectID, k, topAgg, {
//                plan::obexpressions::ObliviousExpressionFactory::makeColumn(0, {type::FieldDataType::Int, sizeof(int)}),
//                plan::obexpressions::ObliviousExpressionFactory::makeColumn(1, {type::FieldDataType::Int, sizeof(int)}),
//                plan::obexpressions::ObliviousExpressionFactory::makeDiv(
//                    plan::obexpressions::ObliviousExpressionFactory::makeColumn(2, {type::FieldDataType::Int, sizeof(int)}),
//                    plan::obexpressions::ObliviousExpressionFactory::makeColumn(3, {type::FieldDataType::Int, sizeof(int)}))
//        });

        size_vdb encodingSize;
        queries.emplace_back(plan::kaoperators::KAnonymousOperator::EncodeQueryToNewArray(&topAgg, encodingSize), [](unsigned char * p){delete[] p;});
        querySizes.push_back(encodingSize);
        queryThreads.emplace_back(sendSecureQuery, machineID, topAggID, queries[machineID - 1].get(), querySizes[machineID - 1], FINAL_QUERY);

        if (machineID == 1) {
            query5OutputSchema = type::RecordSchema(topAgg.getSchema());
        }
    }

    for (MachineID i = 0, count = hostCount * instancePerHost; i < count; i ++) {
        queryThreads[i].join(); // detach?
    }

    while (!OutsideController::IsDone()) {
        // busy wait
    }
    baseID += 25;
    timestamp_t query5EndTime = getTimestamp();


    //----------------------- result display -------------------

    ObliviousTupleTable * resultTable = &OutsideController::GetResultObliviousTupleTable();
#ifdef CONTROLLER_TEST_DEBUG_MSG
    printf("Table Output: \n");
#endif
    for (pos_vdb i = 0, size = resultTable->size(); i < size; i++) {
#ifdef CONTROLLER_TEST_DEBUG_MSG
        printf("%d: %s -- isDummy? %d\n", i, TuplePrinter::convert(&resultTable->getSchema(), (*resultTable)[i]).c_str(), (*resultTable)[i].isDummy());
#endif
    }
    printf("[REPORT] Query 12 of year %d, %d hosts, %d instances per host: \n- psql diag:  %f\n- psql vital: %f\n- left join:  %f\n- psql med:   %f\n- psql dem:   %f\n- right join: %f\n- top join:   %f\n- bottom agg: %f\n- psql local: %f\n- top agg:    %f\n- end-to-end: %f\n",
           FLAGS_year, hostCount, instancePerHost,
           double(psql1EndTime - psql1StartTime)/TS_T_PER_SEC,
           double(psql2EndTime - psql2StartTime)/TS_T_PER_SEC,
           double(query1EndTime - query1StartTime)/TS_T_PER_SEC,
           double(psql3EndTime - psql3StartTime)/TS_T_PER_SEC,
           double(psql4EndTime - psql4StartTime)/TS_T_PER_SEC,
           double(query2EndTime - query2StartTime)/TS_T_PER_SEC,
           double(query3EndTime - query3StartTime)/TS_T_PER_SEC,
           double(query4EndTime - query4StartTime)/TS_T_PER_SEC,
           double(psql5EndTime - psql5StartTime)/TS_T_PER_SEC,
           double(query5EndTime - query5StartTime)/TS_T_PER_SEC,
           double(query5EndTime - psql1StartTime)/TS_T_PER_SEC);

    OutsideController::HBDataEntry(start_t, 12, k, "diagnoses", double(psql1EndTime - psql1StartTime) / TS_T_PER_SEC, "");
    OutsideController::HBDataEntry(start_t, 12, k, "vitals", double(psql2EndTime - psql2StartTime) / TS_T_PER_SEC, "");
    OutsideController::HBDataEntry(start_t, 12, k, "left join", double(query1EndTime - query1StartTime) / TS_T_PER_SEC, "");
    OutsideController::HBDataEntry(start_t, 12, k, "medications", double(psql3EndTime - psql3StartTime) / TS_T_PER_SEC, "");
    OutsideController::HBDataEntry(start_t, 12, k, "demographics", double(psql4EndTime - psql4StartTime) / TS_T_PER_SEC, "");
    OutsideController::HBDataEntry(start_t, 12, k, "right join", double(query2EndTime - query2StartTime) / TS_T_PER_SEC, "");
    OutsideController::HBDataEntry(start_t, 12, k, "top join", double(query3EndTime - query3StartTime) / TS_T_PER_SEC, "");
    OutsideController::HBDataEntry(start_t, 12, k, "bottom agg", double(query4EndTime - query4StartTime) / TS_T_PER_SEC, "");
    OutsideController::HBDataEntry(start_t, 12, k, "local part", double(psql5EndTime - psql5StartTime) / TS_T_PER_SEC, "");
    OutsideController::HBDataEntry(start_t, 12, k, "top agg", double(query5EndTime - query5StartTime) / TS_T_PER_SEC, "");
    OutsideController::HBDataEntry(start_t, 12, k, "end to end", double(query5EndTime - psql1StartTime) / TS_T_PER_SEC,
                                   std::to_string(hostCount)+std::string(" hosts ")+std::to_string(instancePerHost)+
                                           std::string(" instance each, year ") + std::to_string(FLAGS_year));
};

/**
 * Query 13
 * Log testing
 */
static void query13() {
    OutsideController::HBDataEntry(time(0), 12, 4, "test tag", 4.2432432, "4 hosts 1 instance each");
}



TEST_F(UtilityTestFixture, SGXLaunch)
{
    OutsideController::StartServer((MachineID) FLAGS_machine_id, FLAGS_local_address_port);
    size_vdb fixchar_size = 3;
    if (FLAGS_machine_id == 0) {
        switch (FLAGS_queryID) {
            case 1:
                query1(fixchar_size);
                break;
            case 2:
                query2(fixchar_size);
                break;
            case 3:
                query3(fixchar_size);
                break;
            case 4:
                query4(fixchar_size);
                break;
            case 5:
                query5(fixchar_size);
                break;
            case 6:
                query6(fixchar_size);
                break;
            case 7:
                query7(10);
                break;
            case 8:
                query8(fixchar_size);
                break;
            case 9:
                query9(fixchar_size);
                break;
            case 10:
                query10(6);
                break;
            case 11:
                query11(6);
                break;
            case 12:
                query12(6);
                break;
            case 13:
                query13();
                break;
            default:
                printf("[FAILURE] Unknown query id: %d\n", FLAGS_queryID);
                break;
        }
    } else {
        // we're hosts
        size_vdb hostCount = 2;
        size_vdb instancePerMachine = 1;
        switch (FLAGS_queryID) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
                OutsideController::RegisterHonestBroker(0, "localhost:4000");
                break;
            case 10:
            case 11:
            case 12:
                hostCount = 4;
                instancePerMachine = 1;
                OutsideController::RegisterHonestBroker(0, "vaultdb01:4000");
                break;
            default:
                printf("[FAILURE] Unknown query id: %d\n", FLAGS_queryID);
                return;
        }
        registerAllMachines(FLAGS_machine_id, hostCount, instancePerMachine);
    }
	printf("[INFO] service deployed.\n");
	getchar();
}

