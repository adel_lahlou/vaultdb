#ifndef VAULTDB_UTILITYTESTFIXTURE_H
#define VAULTDB_UTILITYTESTFIXTURE_H

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <shared/type/RecordSchema.h>
#include <in_sgx/obdata/ObliviousTuple.h>
#include "in_sgx/obdata/ObliviousTupleTable.h"
#include "debug/DebugFakeLocalDataMover.h"

#if defined(LOCAL_DEBUG_MODE) || defined(OUTSIDE_COMPILATION)
#else
#include <gflags/gflags.h>
#include "utilities/OutsideController.h"
#endif


using namespace type;
using namespace db::obdata;
using namespace db::obdata::obfield;

#if defined(LOCAL_DEBUG_MODE)
#else
void sendPSQLQuery(MachineID dst_machine_id, TableID queryOutputTableID, type::RecordSchema psqlSchema, const char *sqlQuery, int isFinalQuery) {
        type::RecordSchema passingSchema(psqlSchema);
        OutsideController::SendPostgreSQLQuery(dst_machine_id, queryOutputTableID, passingSchema, sqlQuery, isFinalQuery);
}

void sendSecureQuery(MachineID dst_machine_id, TableID queryID, unsigned char *query, size_vdb size, int isFinalQuery) {
        OutsideController::SendQuery(dst_machine_id, queryID, query, size, isFinalQuery);
}
#endif

bool static recordschema_equal(const RecordSchema& s1, const RecordSchema& s2)
{
    if(s1.size() != s2.size())
        return false;
    for(int i = 0; i < s1.size(); ++i){
        if(s1[i].type != s2[i].type || s1[i].size != s2[i].size)
            return false;
    }
    return true;
}


bool static oblivious_tuples_equal(const ObliviousTuple& t1, const ObliviousTuple& t2)
{
    if(t1.size() != t2.size())
        return false;
    for(int i = 0; i < t1.size(); ++i){
        if(!(t1[i] == t2[i]))
            return false;
    }
    return true;
}

class UtilityTestFixture : public ::testing::Test {
protected:
    UtilityTestFixture()
    : username("test_user"),
      password("test"),
      healthlnk_site1("healthlnk_site1"),
      oinput01(s1, otuples01),
      oinput11(s1, otuples11)
    {


    };
    ~UtilityTestFixture() {};

    static db::obdata::obfield::ObliviousField makeObliviousTimestampNoZoneFieldFromString(const char * data) {
        if (strlen(data) < 19) {
            throw std::length_error("Input char array too short");
        }
        tm tm1;
        sscanf(data,"%04d-%02d-%2d %02d:%02d:%02d",&tm1.tm_year,&tm1.tm_mon,&tm1.tm_mday,
               &tm1.tm_hour,&tm1.tm_min,&tm1.tm_sec);
        tm1.tm_year -= 1900;
        tm1.tm_mon -= 1;
        tm1.tm_isdst = 0;
        return db::obdata::obfield::ObliviousField::makeObliviousTimestampNoZoneField(timegm(&tm1));
    };
    size_vdb vc_size = 10;
    size_vdb diag_vc_size = 6;
    type::RecordSchema s1 {
            (TableID) 1, {
                    {type::FieldDataType::Int, sizeof(int)},
                    {type::FieldDataType::Fixchar, vc_size},
                    {type::FieldDataType::Fixchar, vc_size},
            }
    };

    type::RecordSchema diag { 5, {
            {type::FieldDataType::Int, INT_FIELD_SIZE},
            {type::FieldDataType::Fixchar, diag_vc_size},
            {type::FieldDataType::TimestampNoZone, TIMENOZONE_FIELD_SIZE}}};

    ObliviousTupleList otuples01 {
//            1	"hello"	"world"
//            4	"foo"	"bar"
//            1	"bigger"	"better"
//            4	"secure"	"execution"
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("hello", vc_size),
                    ObliviousField::makeObliviousFixcharField("world", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousFixcharField("foo", vc_size),
                    ObliviousField::makeObliviousFixcharField("bar", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("bigger", vc_size),
                    ObliviousField::makeObliviousFixcharField("better", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(4),
                    ObliviousField::makeObliviousFixcharField("secure", vc_size),
                    ObliviousField::makeObliviousFixcharField("execution", vc_size),
            }),
    };

    ObliviousTupleList otuples11{
//            2	"bigger"	"otter"
//            3	"secure"	"world"
//            1	"hard"	"bar"
//            2	"swift"	"execution"
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("bigger", vc_size),
                    ObliviousField::makeObliviousFixcharField("otter", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(3),
                    ObliviousField::makeObliviousFixcharField("secure", vc_size),
                    ObliviousField::makeObliviousFixcharField("world", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(1),
                    ObliviousField::makeObliviousFixcharField("hard", vc_size),
                    ObliviousField::makeObliviousFixcharField("bar", vc_size),
            }),
            ObliviousTuple(false, {
                    ObliviousField::makeObliviousIntField(2),
                    ObliviousField::makeObliviousFixcharField("swift", vc_size),
                    ObliviousField::makeObliviousFixcharField("execution", vc_size),
            }),
    };

    const char * username;
    const char * password;
    const char * healthlnk_site1;
    ObliviousTupleTable oinput01;
    ObliviousTupleTable oinput11;


};

#if defined(LOCAL_DEBUG_MODE) || defined(OUTSIDE_COMPILATION)
#else
int main(int argc, char ** argv) {
	::testing::InitGoogleTest(&argc, argv);
	google::ParseCommandLineFlags(&argc, &argv, true);
	return RUN_ALL_TESTS();
}
#endif

#endif //VAULTDB_UTILITYTESTFIXTURE_H
