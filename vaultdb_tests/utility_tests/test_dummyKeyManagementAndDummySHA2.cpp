#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <in_sgx/utilities/picosha2.h>
#include <in_sgx/utilities/DummyInSGXKeyManager.h>
#include <in_sgx/utilities/DummyInSGXHashPartitioner.h>

#include "utilityTestFixture.h"

TEST_F(UtilityTestFixture, DummySha2)
{
    std::string src_str = "The quick brown fox jumps over the lazy dog";
    std::string hash_hex_str = picosha2::hash256_hex_string(src_str, utilities::DummyInSGXKeyManager::instance().getAddConstant(0), utilities::DummyInSGXKeyManager::instance().getInitialMessageDigest(0));
    ASSERT_TRUE(hash_hex_str == std::string("d7a8fbb307d7809469ca9abcb0082e4f8d5651e46d3cdb762d02d0bf37c9e592"));
}

TEST_F(UtilityTestFixture, DummyPartitioner)
{
    auto p = utilities::DummyInSGXHashPartitioner(2, 0, std::vector<pos_vdb>());
    ASSERT_EQ(p.hashObliviousField(ObliviousField::makeObliviousIntField(123)), 1);
    ASSERT_EQ(p.hashObliviousField(ObliviousField::makeObliviousIntField(125)), 1);
    ASSERT_EQ(p.hashObliviousField(ObliviousField::makeObliviousFixcharField("The quick brown fox jumps over the lazy dog", strlen("The quick brown fox jumps over the lazy dog"))), 1);
}