#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <utilities/PostgreSQLHandler.h>
#include <in_sgx/obdata/obfield/ObliviousTimestampNoZoneField.h>

#include "utilityTestFixture.h"

#ifndef POSTGRES_HANDLER_PRINT_TUPLES
#define POSTGRES_HANDLER_PRINT_TUPLES
#endif

TEST_F(UtilityTestFixture, ObliviousTableConstructionFromPostgresResult)
{
    size_vdb varcharCapacity = 6;
    ObliviousTupleList expectedTuples {
            ObliviousTuple {
                    obfield::ObliviousField::makeObliviousIntField(1),
                    obfield::ObliviousField::makeObliviousFixcharField("008.45", varcharCapacity),
                    makeObliviousTimestampNoZoneFieldFromString("2006-01-01 00:00:00"),
            },
            ObliviousTuple {
                    obfield::ObliviousField::makeObliviousIntField(2),
                    obfield::ObliviousField::makeObliviousFixcharField("008.45", varcharCapacity),
                    makeObliviousTimestampNoZoneFieldFromString("2006-02-01 00:00:00"),
            },
            ObliviousTuple {
                    obfield::ObliviousField::makeObliviousIntField(3),
                    obfield::ObliviousField::makeObliviousFixcharField("008.45", varcharCapacity),
                    makeObliviousTimestampNoZoneFieldFromString("2006-03-01 00:00:00"),
            },
            ObliviousTuple {
                    obfield::ObliviousField::makeObliviousIntField(4),
                    obfield::ObliviousField::makeObliviousFixcharField("008.45", varcharCapacity),
                    makeObliviousTimestampNoZoneFieldFromString("2006-04-01 00:00:00"),
            },
            ObliviousTuple {
                    obfield::ObliviousField::makeObliviousIntField(1),
                    obfield::ObliviousField::makeObliviousFixcharField("414.01", varcharCapacity),
                    makeObliviousTimestampNoZoneFieldFromString("2006-09-07 00:00:00"),
            },
    };

    utilities::PostgreSQLHandler pgh(username, password, healthlnk_site1);
    ObliviousTupleTable table;
    ASSERT_EQ(pgh.singleQueryWithObliviousTupleTable(table, diag, "select patient_id, icd9, timestamp_ from diagnoses limit 5"), RETURN_SUCCESS);
    std::cout << std::endl;
    for (int i = 0, size = table.size(); i < size; i++) {
#ifdef POSTGRES_HANDLER_PRINT_TUPLES
        printf("Expected / Actual: %s / %s\n", expectedTuples[i].toString().c_str(), table[i].toString().c_str());
#endif
        ASSERT_TRUE(oblivious_tuples_equal(expectedTuples[i], table[i]));
    }
}

TEST_F(UtilityTestFixture, ObliviousTableConstructionFromPostgresEmptyResult)
{
    ObliviousTupleList expectedTuples {
    };

    utilities::PostgreSQLHandler pgh(username, password, healthlnk_site1);
    ObliviousTupleTable table;
    ASSERT_NO_THROW(
            ASSERT_EQ(pgh.singleQueryWithObliviousTupleTable(table, diag, "select patient_id, icd9, timestamp_ from diagnoses limit 0"), RETURN_SUCCESS));
    ASSERT_EQ(expectedTuples.size(), table.size());
}

TEST_F(UtilityTestFixture, NonResultQuery)
{
    utilities::PostgreSQLHandler pgh(username, password, healthlnk_site1);

    ASSERT_NO_THROW(ASSERT_EQ(pgh.singleQueryWithNoResult("create table test_table (i integer)"), RETURN_SUCCESS));
    ASSERT_NO_THROW(ASSERT_EQ(pgh.singleQueryWithNoResult("drop table test_table"), RETURN_SUCCESS));
}