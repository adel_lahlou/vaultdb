#include "serializationTestFixture.h"
#include <plaintext/serialization/Serializer.h>


TEST_F(SerializationTestFixture, SerializationSizes)
{
    EXPECT_EQ(s1.serializationSize(), 33);

    EXPECT_EQ(input1[0][0].serializationSize(), 4);
    EXPECT_EQ(input1[0][1].serializationSize(), 4);
    EXPECT_EQ(input1[0][2].serializationSize(), 4);
    EXPECT_EQ(input1[0][3].serializationSize(), 6);
    EXPECT_EQ(input1[0][4].serializationSize(), 6);
    EXPECT_EQ(input1[0].serializationSize(), 25);

    EXPECT_EQ(input1[1][0].serializationSize(), 4);
    EXPECT_EQ(input1[1][1].serializationSize(), 4);
    EXPECT_EQ(input1[1][2].serializationSize(), 4);
    EXPECT_EQ(input1[1][3].serializationSize(), 4);
    EXPECT_EQ(input1[1][4].serializationSize(), 4);
    EXPECT_EQ(input1[1].serializationSize(), 21);

    EXPECT_EQ(input1[2][0].serializationSize(), 4);
    EXPECT_EQ(input1[2][1].serializationSize(), 4);
    EXPECT_EQ(input1[2][2].serializationSize(), 4);
    EXPECT_EQ(input1[2][3].serializationSize(), 7);
    EXPECT_EQ(input1[2][4].serializationSize(), 7);
    EXPECT_EQ(input1[2].serializationSize(), 27);

    EXPECT_EQ(input1[3][0].serializationSize(), 4);
    EXPECT_EQ(input1[3][1].serializationSize(), 4);
    EXPECT_EQ(input1[3][2].serializationSize(), 4);
    EXPECT_EQ(input1[3][3].serializationSize(), 7);
    EXPECT_EQ(input1[3][4].serializationSize(), 10);
    EXPECT_EQ(input1[3].serializationSize(), 30);

    EXPECT_EQ(input1[4][0].serializationSize(), 4);
    EXPECT_EQ(input1[4][1].serializationSize(), 4);
    EXPECT_EQ(input1[4][2].serializationSize(), 4);
    EXPECT_EQ(input1[4][3].serializationSize(), 7);
    EXPECT_EQ(input1[4][4].serializationSize(), 10);
    EXPECT_EQ(input1[4].serializationSize(), 30);

    EXPECT_EQ(input1.serializationSize(),
              s1.serializationSize() +
              sizeof(TableID) +
              input1[0].serializationSize() +
              input1[1].serializationSize() +
              input1[2].serializationSize() +
              input1[3].serializationSize() +
              input1[4].serializationSize() +
              sizeof(input1.size())
    );
}

TEST_F(SerializationTestFixture, SerializeAndDeserialize)
{
    size_vdb input1_serializationSize;
    char * input1_data = Serializer::serializeTupleTable(input1, input1_serializationSize);
    TupleTable deserialized1 = Serializer::deserializeTupleTable(input1_data, input1_serializationSize);

    ASSERT_TRUE(recordschema_equal(deserialized1.getSchema(), input1.getSchema()));
    ASSERT_EQ(deserialized1.size(), input1.size());

    for(size_vdb pos = 0; pos < deserialized1.size(); ++pos) {
        ASSERT_TRUE(tuples_equal(deserialized1[pos], input1[pos]));
    }

    delete[] input1_data;
}

TEST_F(SerializationTestFixture, TableDeserializationFromUnsignedCharArray)
{
    TupleTable deserialized2 = Serializer::deserializeTupleTable((char*)(tableSerial + sizeof(size_vdb) ), *(size_vdb*)tableSerial);

    ASSERT_TRUE(recordschema_equal(deserialized2.getSchema(), input2.getSchema()));
    ASSERT_EQ(deserialized2.size(), input2.size());

    for(size_vdb pos = 0; pos < deserialized2.size(); ++pos) {
        ASSERT_TRUE(tuples_equal(deserialized2[pos], input2[pos]));
    }
}