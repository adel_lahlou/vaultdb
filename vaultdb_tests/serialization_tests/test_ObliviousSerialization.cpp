#include "obliviousSerializationTestFixture.h"
#include "in_sgx/serialization/ObliviousSerializer.h"


TEST_F(ObliviousSerializationTestFixture, SerializationSizes)
{
    EXPECT_EQ(s1.serializationSize(), 33);
}

TEST_F(ObliviousSerializationTestFixture, SerializeAndDeserializeOneTuple)
{
    type::RecordSchema s = input1.getSchema();
    unsigned char * array = new unsigned char [s.tupleSerializationSize()];
    input1[0].serializeTo(array, s);
    ObliviousTuple t = ObliviousSerializer::deserializeObliviousTuple(s, array);
    ASSERT_TRUE(ObliviousTuplesEqual(t, input1[0]));

    input1[1].serializeTo(array, s);
    t = ObliviousSerializer::deserializeObliviousTuple(s, array);
    ASSERT_TRUE(ObliviousTuplesEqual(t, input1[1]));
    delete[] array;
}

TEST_F(ObliviousSerializationTestFixture, SerializeAndDeserialize)
{
    size_vdb input1_serializationSize;
    unsigned char * input1_data = ObliviousSerializer::serializeObliviousTupleTable(input1, input1_serializationSize);
    ObliviousTupleTable deserialized1 = ObliviousSerializer::deserializeObliviousTupleTable(input1_data, input1_serializationSize);

    ASSERT_TRUE(RecordSchema_equal(deserialized1.getSchema(), input1.getSchema()));
    ASSERT_EQ(deserialized1.size(), input1.size());

    for(size_vdb pos = 0; pos < deserialized1.size(); ++pos) {
        ASSERT_TRUE(ObliviousTuplesEqual(deserialized1[pos], input1[pos]));
    }

    delete[] input1_data;
}

TEST_F(ObliviousSerializationTestFixture, EmptyTableDeserialization)
{
    ObliviousTupleTable block(input2.getSchema(), ObliviousTupleList());
    size_vdb blockSize = ((block.serializationSize() >> 6) + 1) << 6;
    std::unique_ptr<unsigned char[]> buffer(new unsigned char[blockSize]);
    block.serializeTo(buffer.get());


    ObliviousTupleTable deserialized2 = ObliviousSerializer::deserializeObliviousTupleTable(
            buffer.get(),
            blockSize);

    ASSERT_TRUE(RecordSchema_equal(deserialized2.getSchema(), input2.getSchema()));
    ASSERT_EQ(deserialized2.size(), 0);
}