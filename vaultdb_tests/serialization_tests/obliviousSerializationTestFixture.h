#ifndef VAULTDB_SERIALIZATIONTESTFIXTURE_H
#define VAULTDB_SERIALIZATIONTESTFIXTURE_H


#include <gtest/gtest.h>
#include <in_sgx/obdata/ObliviousTuple.h>
#include <in_sgx/obdata/ObliviousTupleTable.h>
#include <shared/type/RecordSchema.h>

using namespace db::obdata;
using namespace db::obdata::obfield;
using namespace type;

bool static RecordSchema_equal(const RecordSchema& s1, const RecordSchema& s2)
{
    if(s1.size() != s2.size())
        return false;

    for(int i = 0; i < s1.size(); ++i){
        if(s1[i].type != s2[i].type && s1[i].size != s2[i].size)
            return false;
    }

    return true;
}

bool static ObliviousTuplesEqual(const ObliviousTuple& t1, const ObliviousTuple& t2)
{
    if(t1.size() != t2.size())
        return false;

    for(int i = 0; i < t1.size(); ++i){
        if(t1[i] != t2[i])
            return false;
    }

    return true;
}

class ObliviousSerializationTestFixture : public ::testing::Test {
protected:

    ObliviousSerializationTestFixture()
            : input1(s1, otuples), input2(s2, otuples2)
    {}

    ~ObliviousSerializationTestFixture() {}

    type::RecordSchema s1{
            (TableID)1, {
                {FieldDataType::Int, sizeof(int)},
                {FieldDataType::Int, sizeof(int)},
                {FieldDataType::Int, sizeof(int)},
                {type::FieldDataType::Fixchar, 10},
                {type::FieldDataType::Fixchar, 10},
            }
    };

    ObliviousTupleList otuples {
        ObliviousTuple{
                ObliviousField::makeObliviousIntField(1),
                ObliviousField::makeObliviousIntField(2),
                ObliviousField::makeObliviousIntField(3),
                ObliviousField::makeObliviousFixcharField("hello", 10),
                ObliviousField::makeObliviousFixcharField("world", 10),
        },
        ObliviousTuple{
                ObliviousField::makeObliviousIntField(4),
                ObliviousField::makeObliviousIntField(5),
                ObliviousField::makeObliviousIntField(6),
                ObliviousField::makeObliviousFixcharField("foo", 10),
                ObliviousField::makeObliviousFixcharField("bar", 10),
        },
        ObliviousTuple{
                ObliviousField::makeObliviousIntField(1),
                ObliviousField::makeObliviousIntField(2),
                ObliviousField::makeObliviousIntField(4),
                ObliviousField::makeObliviousFixcharField("bigger", 10),
                ObliviousField::makeObliviousFixcharField("better", 10),
        },
        ObliviousTuple{
                ObliviousField::makeObliviousIntField(4),
                ObliviousField::makeObliviousIntField(2),
                ObliviousField::makeObliviousIntField(6),
                ObliviousField::makeObliviousFixcharField("secure", 10),
                ObliviousField::makeObliviousFixcharField("execution", 10),
        },
        ObliviousTuple{
                ObliviousField::makeObliviousIntField(4),
                ObliviousField::makeObliviousIntField(2),
                ObliviousField::makeObliviousIntField(6),
                ObliviousField::makeObliviousFixcharField("secure", 10),
                ObliviousField::makeObliviousFixcharField("execution", 10),
        },
    };

    ObliviousTupleTable input1;


    unsigned char tableSerial2[0x3f] {
            0x4b, 0x00, 0x00, 0x00, 0xee, 0xee, 0xee, 0xee,
            0x02, 0x00, 0x00, 0x00, 0x03, 0x04, 0x00, 0x00,
            0x00, 0x07, 0x0A, 0x00, 0x00, 0x00, 0xee, 0xee,
            0xee, 0xee, 0x03, 0x00, 0x00, 0x00, 0x00, 0x0a,
            0x05, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x46,
            0x00, 0x00, 0x13, 0x05, 0x00, 0x00, 0x01, 0x00,
            0x00, 0x00, 0x4d, 0x00, 0x00, 0x04, 0x05, 0x00,
            0x00, 0x01, 0x00, 0x00, 0x00, 0x4d, 0x00,
    };

    type::RecordSchema s2{
            (TableID)1, {
                    {FieldDataType::Int, sizeof(int)},
                    {type::FieldDataType::Fixchar,10},
            }};

    ObliviousTupleList otuples2 {
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(1290),
                    ObliviousField::makeObliviousFixcharField("F", 1),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(1299),
                    ObliviousField::makeObliviousFixcharField("M", 1),
            },
            ObliviousTuple{
                    ObliviousField::makeObliviousIntField(1284),
                    ObliviousField::makeObliviousFixcharField("M", 1),
            },
    };

    ObliviousTupleTable input2;
};

#endif //VAULTDB_SERIALIZATIONTESTFIXTURE_H
