#ifndef VAULTDB_SERIALIZATIONTESTFIXTURE_H
#define VAULTDB_SERIALIZATIONTESTFIXTURE_H


#include <gtest/gtest.h>
#include <plaintext/data/Tuple.h>
#include <plaintext/data/TupleTable.h>
#include <shared/type/RecordSchema.h>

using namespace db::data;
using namespace db::data::field;
using namespace type;

bool static recordschema_equal(const RecordSchema& s1, const RecordSchema& s2)
{
    if(s1.size() != s2.size())
        return false;

    for(int i = 0; i < s1.size(); ++i){
        if(s1[i].type != s2[i].type || s1[i].size != s2[i].size)
            return false;
    }

    return true;
}

bool static tuples_equal(const Tuple& t1, const Tuple& t2)
{
    if(t1.size() != t2.size())
        return false;

    for(int i = 0; i < t1.size(); ++i){
        if(t1[i] != t2[i])
            return false;
    }

    return true;
}

class SerializationTestFixture : public ::testing::Test {
protected:

    SerializationTestFixture()
            : input1(s1, tuples), input2(s2, tuples2)
    {}

    ~SerializationTestFixture() {}

    type::RecordSchema s1{
            (TableID)1, {
                {FieldDataType::Int, sizeof(int)},
                {FieldDataType::Int, sizeof(int)},
                {FieldDataType::Int, sizeof(int)},
                {type::FieldDataType::Varchar, 10},
                {type::FieldDataType::Varchar, 10},
    }};

    TupleList tuples {
        Tuple{
                Field::makeIntField(1),
                Field::makeIntField(2),
                Field::makeIntField(3),
                Field::makeVarcharField("hello"),
                Field::makeVarcharField("world"),
        },
        Tuple{
                Field::makeIntField(4),
                Field::makeIntField(5),
                Field::makeIntField(6),
                Field::makeVarcharField("foo"),
                Field::makeVarcharField("bar"),
        },
        Tuple{
                Field::makeIntField(1),
                Field::makeIntField(2),
                Field::makeIntField(4),
                Field::makeVarcharField("bigger"),
                Field::makeVarcharField("better"),
        },
        Tuple{
                Field::makeIntField(4),
                Field::makeIntField(2),
                Field::makeIntField(6),
                Field::makeVarcharField("secure"),
                Field::makeVarcharField("execution"),
        },
        Tuple{
                Field::makeIntField(4),
                Field::makeIntField(2),
                Field::makeIntField(6),
                Field::makeVarcharField("secure"),
                Field::makeVarcharField("execution"),
        },
    };

    TupleTable input1;


    unsigned char tableSerial[0x33] {
            0x2f, 0x00, 0x00, 0x00, 0xee, 0xee, 0xee, 0xee,
            0x02, 0x00, 0x00, 0x00, 0x03, 0x04, 0x00, 0x00,//
            0x00, 0x08, 0x0a, 0x00, 0x00, 0x00, 0xee, 0xee,//
            0xee, 0xee, 0x03, 0x00, 0x00, 0x00, 0x00, 0x0a,
            0x05, 0x00, 0x00, 0x46, 0x00, 0x00, 0x13, 0x05,//
            0x00, 0x00, 0x4d, 0x00, 0x00, 0x04, 0x05, 0x00,
            0x00, 0x4d, 0x00
    };

    type::RecordSchema s2{
            (TableID)1, {
                    {FieldDataType::Int, sizeof(int)},
                    {type::FieldDataType::Varchar, 10},
            }};

    TupleList tuples2 {
            Tuple{
                    Field::makeIntField(1290),
                    Field::makeVarcharField("F"),
            },
            Tuple{
                    Field::makeIntField(1299),
                    Field::makeVarcharField("M"),
            },
            Tuple{
                    Field::makeIntField(1284),
                    Field::makeVarcharField("M"),
            },
    };

    TupleTable input2;
};

#endif //VAULTDB_SERIALIZATIONTESTFIXTURE_H
