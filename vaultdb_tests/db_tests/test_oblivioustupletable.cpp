#include <gtest/gtest.h>
#include "obliviousTupleTableFixtures.h"

using namespace db::obdata;

TEST_F(ObliviousTupleTableTest, ObliviousTupleTableSortStd)
{
    db::obdata::ObliviousTupleTable expected (s1, {
            db::obdata::ObliviousTuple{
                    db::obdata::obfield::ObliviousField::makeObliviousIntField(7),
                    db::obdata::obfield::ObliviousField::makeObliviousIntField(8),
                    db::obdata::obfield::ObliviousField::makeObliviousFixcharField("worldc3", vc_size)
            },
            db::obdata::ObliviousTuple{
                    db::obdata::obfield::ObliviousField::makeObliviousIntField(4),
                    db::obdata::obfield::ObliviousField::makeObliviousIntField(5),
                    db::obdata::obfield::ObliviousField::makeObliviousFixcharField("worlda2", vc_size)
            },
            db::obdata::ObliviousTuple{
                    db::obdata::obfield::ObliviousField::makeObliviousIntField(1),
                    db::obdata::obfield::ObliviousField::makeObliviousIntField(2),
                    db::obdata::obfield::ObliviousField::makeObliviousFixcharField("worldd4", vc_size)
            },
            db::obdata::ObliviousTuple{
                    db::obdata::obfield::ObliviousField::makeObliviousIntField(1),
                    db::obdata::obfield::ObliviousField::makeObliviousIntField(2),
                    db::obdata::obfield::ObliviousField::makeObliviousFixcharField("world66", vc_size)
            },
            db::obdata::ObliviousTuple{
                    db::obdata::obfield::ObliviousField::makeObliviousIntField(8),
                    db::obdata::obfield::ObliviousField::makeObliviousIntField(2),
                    db::obdata::obfield::ObliviousField::makeObliviousFixcharField("worldb1", vc_size)
            },
    });

    std::vector<pos_vdb> sortColumns {1,0};
    std::vector<SortOrder> sortOrders {SortOrder ::DESCEND, SortOrder ::ASCEND};
    ObliviousTupleTable temp1 (table1);
    std::sort(temp1.begin(), temp1.end(), [sortColumns, sortOrders](ObliviousTuple & l, ObliviousTuple & r) {
        for (pos_vdb i = 0, size = (size_vdb)sortColumns.size(); i < size; i++) {
            int comparison = l[sortColumns[i]].comparesTo(r[sortColumns[i]], 0);
            if (sortOrders[i] == SortOrder::ASCEND && comparison < 0) {
                 return true;
            } else if (sortOrders[i] == SortOrder::DESCEND && comparison > 0) {
                return true;
            } else if (comparison == 0) {
                continue;
            } else {
                return false;
            }
        }
        return false;
    });
    compareObliviousTupleTables(1, expected, temp1);
}

TEST_F(ObliviousTupleTableTest, ObliviousTupleTableReSort)
{
    db::obdata::ObliviousTupleTable expected1 (s1, {
            db::obdata::ObliviousTuple{
                    db::obdata::obfield::ObliviousField::makeObliviousIntField(7),
                    db::obdata::obfield::ObliviousField::makeObliviousIntField(8),
                    db::obdata::obfield::ObliviousField::makeObliviousFixcharField("worldc3", vc_size)
            },
            db::obdata::ObliviousTuple{
                    db::obdata::obfield::ObliviousField::makeObliviousIntField(4),
                    db::obdata::obfield::ObliviousField::makeObliviousIntField(5),
                    db::obdata::obfield::ObliviousField::makeObliviousFixcharField("worlda2", vc_size)
            },
            db::obdata::ObliviousTuple{
                    db::obdata::obfield::ObliviousField::makeObliviousIntField(1),
                    db::obdata::obfield::ObliviousField::makeObliviousIntField(2),
                    db::obdata::obfield::ObliviousField::makeObliviousFixcharField("worldd4", vc_size)
            },
    });
    db::obdata::ObliviousTupleTable expected2 (s1, {
            db::obdata::ObliviousTuple{
                    db::obdata::obfield::ObliviousField::makeObliviousIntField(1),
                    db::obdata::obfield::ObliviousField::makeObliviousIntField(2),
                    db::obdata::obfield::ObliviousField::makeObliviousFixcharField("world66", vc_size)
            },
            db::obdata::ObliviousTuple{
                    db::obdata::obfield::ObliviousField::makeObliviousIntField(8),
                    db::obdata::obfield::ObliviousField::makeObliviousIntField(2),
                    db::obdata::obfield::ObliviousField::makeObliviousFixcharField("worldb1", vc_size)
            },
    });

    ObliviousTupleTable t1(table1);
    ObliviousTupleTable t2;
    ObliviousTupleTable::half(t1, t2);

    std::vector<pos_vdb> sortColumns {1,0};
    std::vector<SortOrder> sortOrders {SortOrder ::DESCEND, SortOrder ::ASCEND};
    ObliviousTupleTable::appendNonObliviousSortHalf(t1, t2, sortColumns, sortOrders);

    compareObliviousTupleTables(1, expected1, t1);
    compareObliviousTupleTables(2, expected2, t2);
}