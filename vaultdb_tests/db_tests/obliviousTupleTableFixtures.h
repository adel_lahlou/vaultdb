#ifndef VAULTDB_PROJ_OBLIVIOUSTUPLETABLEFIXTURES_H
#define VAULTDB_PROJ_OBLIVIOUSTUPLETABLEFIXTURES_H

#include <in_sgx/obdata/obfield/ObliviousField.h>
#include <gtest/gtest.h>
#include <shared/type/RecordSchema.h>
#include <in_sgx/obdata/ObliviousTuple.h>
#include <in_sgx/obdata/ObliviousTupleTable.h>
#include <in_sgx/obdata/ObliviousTupleBlock.h>

#ifndef PRINT_TUPLE_TABLE_TEST_MSGS
//#define PRINT_TUPLE_TABLE_TEST_MSGS
#endif

#ifndef VERIFY_TUPLE_TABLE_TEST_RESULT
#define VERIFY_TUPLE_TABLE_TEST_RESULT
#endif

class ObliviousTupleTableTest : public ::testing::Test {
protected:

    static void compareObliviousTupleTables(
            TableID identifier,
            db::obdata::ObliviousTupleTable &expected,
            db::obdata::ObliviousTupleTable &actual) {
#ifdef PRINT_TUPLE_TABLE_TEST_MSGS
        fprintf(stdout, "\nTable %d verification\n", identifier);
#endif
        size_vdb expectedSize = expected.size();
        for (pos_vdb i = 0, size = (size_vdb) actual.size(); i < size; i++) {
#ifdef PRINT_TUPLE_TABLE_TEST_MSGS
            if (i < expectedSize) {
                fprintf(stdout, "Expected / Actual: %s / %s\n", expected[i].toString().c_str(), actual[i].toString().c_str());
            } else {
                fprintf(stdout, "Expected / Actual: (none) / %s\n", actual[i].toString().c_str());
            }
#endif
#ifdef VERIFY_TUPLE_TABLE_TEST_RESULT
            ASSERT_EQ(expected[i].toString(), actual[i].toString());
#endif
        }
    }

    static void compareObliviousTupleBlock(
            TableID identifier,
            db::obdata::ObliviousTupleBlock &expected,
            db::obdata::ObliviousTupleBlock &actual) {
#ifdef PRINT_TUPLE_TABLE_TEST_MSGS
        fprintf(stdout, "\nTable %d verification\n", identifier);
#endif
        size_vdb expectedSize = expected.size();
        for (pos_vdb i = 0, size = (size_vdb) actual.size(); i < size; i++) {
#ifdef PRINT_TUPLE_TABLE_TEST_MSGS
            if (i < expectedSize) {
                fprintf(stdout, "Expected / Actual: %s / %s\n", (expected.begin()+ i)->toString().c_str(), (actual.begin()+ i)->toString().c_str());
            } else {
                fprintf(stdout, "Expected / Actual: (none) / %s\n", (actual.begin()+ i)->toString().c_str());
            }
#endif
#ifdef VERIFY_TUPLE_TABLE_TEST_RESULT
            ASSERT_EQ((expected.begin()+ i)->toString(), (actual.begin()+ i)->toString());
#endif
        }
    }

    ObliviousTupleTableTest()
    : table1(s1, tuples1),
      table2(s1, tuples2)
    {}

    size_vdb vc_size = 7;

    type::RecordSchema s1 {
            (TableID) 3, {
                    {type::FieldDataType::Int, sizeof(int)},
                    {type::FieldDataType::Int, sizeof(int)},
                    {type::FieldDataType::Fixchar, 7},
            }
    };

    db::obdata::ObliviousTupleList tuples1 {
            db::obdata::ObliviousTuple{
                    db::obdata::obfield::ObliviousField::makeObliviousIntField(8),
                    db::obdata::obfield::ObliviousField::makeObliviousIntField(2),
                    db::obdata::obfield::ObliviousField::makeObliviousFixcharField("worldb1", vc_size)
            },
            db::obdata::ObliviousTuple{
                    db::obdata::obfield::ObliviousField::makeObliviousIntField(4),
                    db::obdata::obfield::ObliviousField::makeObliviousIntField(5),
                    db::obdata::obfield::ObliviousField::makeObliviousFixcharField("worlda2", vc_size)
            },
            db::obdata::ObliviousTuple{
                    db::obdata::obfield::ObliviousField::makeObliviousIntField(7),
                    db::obdata::obfield::ObliviousField::makeObliviousIntField(8),
                    db::obdata::obfield::ObliviousField::makeObliviousFixcharField("worldc3", vc_size)
            },
            db::obdata::ObliviousTuple{
                    db::obdata::obfield::ObliviousField::makeObliviousIntField(1),
                    db::obdata::obfield::ObliviousField::makeObliviousIntField(2),
                    db::obdata::obfield::ObliviousField::makeObliviousFixcharField("worldd4", vc_size)
            },
            db::obdata::ObliviousTuple{
                    db::obdata::obfield::ObliviousField::makeObliviousIntField(1),
                    db::obdata::obfield::ObliviousField::makeObliviousIntField(2),
                    db::obdata::obfield::ObliviousField::makeObliviousFixcharField("world66", vc_size)
            },
    };

    db::obdata::ObliviousTupleList tuples2 {
            db::obdata::ObliviousTuple{
                    db::obdata::obfield::ObliviousField::makeObliviousIntField(6),
                    db::obdata::obfield::ObliviousField::makeObliviousIntField(2),
                    db::obdata::obfield::ObliviousField::makeObliviousFixcharField("worldb1", vc_size)
            },
            db::obdata::ObliviousTuple{
                    db::obdata::obfield::ObliviousField::makeObliviousIntField(3),
                    db::obdata::obfield::ObliviousField::makeObliviousIntField(1),
                    db::obdata::obfield::ObliviousField::makeObliviousFixcharField("world66", vc_size)
            },
            db::obdata::ObliviousTuple{
                    db::obdata::obfield::ObliviousField::makeObliviousIntField(4),
                    db::obdata::obfield::ObliviousField::makeObliviousIntField(5),
                    db::obdata::obfield::ObliviousField::makeObliviousFixcharField("worlda2", vc_size)
            },
            db::obdata::ObliviousTuple{
                    db::obdata::obfield::ObliviousField::makeObliviousIntField(7),
                    db::obdata::obfield::ObliviousField::makeObliviousIntField(8),
                    db::obdata::obfield::ObliviousField::makeObliviousFixcharField("worldc3", vc_size)
            },
            db::obdata::ObliviousTuple{
                    db::obdata::obfield::ObliviousField::makeObliviousIntField(1),
                    db::obdata::obfield::ObliviousField::makeObliviousIntField(2),
                    db::obdata::obfield::ObliviousField::makeObliviousFixcharField("worldd4", vc_size)
            },
            db::obdata::ObliviousTuple{
                    db::obdata::obfield::ObliviousField::makeObliviousIntField(1),
                    db::obdata::obfield::ObliviousField::makeObliviousIntField(1),
                    db::obdata::obfield::ObliviousField::makeObliviousFixcharField("world66", vc_size)
            },
    };
    db::obdata::ObliviousTupleTable table1;
    db::obdata::ObliviousTupleTable table2;
};

#endif //VAULTDB_PROJ_OBLIVIOUSTUPLETABLEFIXTURES_H
