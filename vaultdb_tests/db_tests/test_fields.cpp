#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include "fieldTestFixtures.h"

using namespace db::data::field;

TEST_F(FieldTest, IntFieldAlgebraicOperator)
{
    Field shouldBeTen     = int_2 * int_5;
    Field shouldBeFive    = int_10 / int_2;
    Field shouldBeThree   = int_5 - int_2;
    Field shouldBeFifteen = int_10 + int_5;

    ASSERT_EQ(shouldBeTen, int_10);
    ASSERT_EQ(shouldBeFive, int_5);
    ASSERT_EQ(shouldBeThree, int_3);
    ASSERT_EQ(shouldBeFifteen, int_15);

    Field shouldBeFive2    =  shouldBeTen  / int_2;
    Field shouldBeTen2     =  shouldBeFive * int_2;
    Field shouldBeFive3    =  shouldBeThree + int_2;
    Field shouldBeFive4    =  shouldBeTen - int_5;

    ASSERT_EQ(shouldBeFive2, int_5);
    ASSERT_EQ(shouldBeTen2, int_10);
    ASSERT_EQ(shouldBeFive3, int_5);
    ASSERT_EQ(shouldBeFive4, int_5);

    Field shouldBeFifteen2 = shouldBeFive4 * shouldBeThree;

    ASSERT_EQ(shouldBeFifteen2, int_15);
    ASSERT_EQ(shouldBeFifteen2, shouldBeFifteen);

    IntField int_0 = IntField(0);
    Field shouldBeZero = Field::makeIntField(0);

    ASSERT_FALSE(int_0.isTruthy());
    ASSERT_FALSE(shouldBeZero.isTruthy());

    ASSERT_TRUE(int_2.isTruthy());
    ASSERT_TRUE(int_5.isTruthy());
    ASSERT_TRUE(int_10.isTruthy());
    ASSERT_TRUE(shouldBeFifteen.isTruthy());
    ASSERT_TRUE(shouldBeFive.isTruthy());
}


TEST_F(FieldTest, IntFieldComparisonOperator)
{
    ASSERT_EQ(int_5, IntField(5));
    ASSERT_NE(int_5, int_15);
    ASSERT_LT(int_5, int_10);
    ASSERT_GT(int_15, int_3);
    ASSERT_GE(int_15, IntField(15));
    ASSERT_GE(int_15, int_10);

    Field shouldBeThree   = Field::makeIntField(3);
    Field shouldBeFive    = Field::makeIntField(5);
    Field shouldBeTen     = Field::makeIntField(10);
    Field shouldBeFifteen = Field::makeIntField(15);

    ASSERT_EQ(shouldBeFive, int_5);
    ASSERT_NE(shouldBeTen, int_15);

    ASSERT_LE(shouldBeThree, int_3);
    ASSERT_LE(shouldBeThree, int_5);
    ASSERT_LT(shouldBeThree, int_10);

    ASSERT_GE(shouldBeFive, int_5);
    ASSERT_GE(shouldBeFive, int_3);
    ASSERT_GT(shouldBeFive, int_3);

    ASSERT_EQ(shouldBeFive, shouldBeFive);
    ASSERT_NE(shouldBeTen, shouldBeFifteen);

    ASSERT_LE(shouldBeThree, shouldBeThree);
    ASSERT_LE(shouldBeThree, shouldBeFive);
    ASSERT_LT(shouldBeThree, shouldBeTen);

    ASSERT_GE(shouldBeFive, shouldBeFive);
    ASSERT_GE(shouldBeFive, shouldBeThree);
    ASSERT_GT(shouldBeFive, shouldBeThree);
}


// TODO: the any throws are not actually executed
TEST_F(FieldTest, FieldCompatibility)
{
    ASSERT_NO_THROW(int_2 == str_hello);
    ASSERT_FALSE(int_2 == str_hello);
    ASSERT_NO_THROW(int_2 != str_hello);
    ASSERT_TRUE(int_2 != str_hello); // TODO: should a not equal really return not true?

    ASSERT_ANY_THROW(int_2 - str_hello);
    ASSERT_ANY_THROW(int_2 + str_hello);
    ASSERT_ANY_THROW(int_2 * str_hello);
    ASSERT_ANY_THROW(int_2 / str_hello);
    ASSERT_ANY_THROW(int_2 > str_hello);
    ASSERT_ANY_THROW(int_2 < str_hello);
    ASSERT_ANY_THROW(int_2 >= str_hello);
    ASSERT_ANY_THROW(int_2 >= str_hello);
}

TEST_F(FieldTest, VarcharFieldArithmeticOperators)
{
    Field shouldBeHelloWorld = Field::makeVarcharField("helloworld");
    Field shouldBeHelloWorld2 = str_hello + str_world;

    ASSERT_EQ(shouldBeHelloWorld, shouldBeHelloWorld2);
    ASSERT_EQ(shouldBeHelloWorld, str_helloWorld);

    ASSERT_TRUE(str_hello.isTruthy());
    ASSERT_TRUE(shouldBeHelloWorld.isTruthy());
}


TEST_F(FieldTest, VarcharFieldComparisonOperators)
{
    ASSERT_EQ(str_hello, VarcharField("hello"));
    ASSERT_NE(str_hello, VarcharField("world"));


    ASSERT_GE(str_world, VarcharField("world"));
    ASSERT_GE(str_world, str_hello);
    ASSERT_GT(str_world, str_hello);
    ASSERT_LE(str_hello, VarcharField("hello"));
    ASSERT_LE(str_hello, str_hello);
    ASSERT_LT(str_hello, str_world);
}



TEST(BoolField, Construction)
{
    BoolField bool_true(true);
    BoolField bool_false(false);

    ASSERT_TRUE(bool_true.isTruthy());
    ASSERT_FALSE(bool_false.isTruthy());

    Field shouldBeTrue = Field::makeBoolField(true);
    Field shouldBeFalse = Field::makeBoolField(false);

    ASSERT_TRUE(shouldBeTrue.isTruthy());
    ASSERT_FALSE(shouldBeFalse.isTruthy());

    Field shouldBeTrue2(shouldBeTrue);
    Field shouldBeFalse2(shouldBeFalse);

    ASSERT_TRUE(shouldBeTrue2.isTruthy());
    ASSERT_FALSE(shouldBeFalse2.isTruthy());
}