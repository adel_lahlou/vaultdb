#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <plaintext/data/Tuple.h>
#include <plaintext/data/field/IntField.h>
#include <plaintext/data/field/VarcharField.h>

using namespace db::data;
using namespace db::data::field;

TEST(TupleTest, TupleConstruction)
{
    Tuple empty;

    EXPECT_EQ(empty.size(), 0);
    EXPECT_EQ(empty.isDummy(), false);

    std::vector<Field> fs = {
            Field::makeIntField(1),
            Field::makeVarcharField("hello"),
            Field::makeIntField(2),
            Field::makeVarcharField("world")
    };

    Tuple w_vec{fs};

    EXPECT_EQ(w_vec.size(), fs.size());
    EXPECT_EQ(w_vec.isDummy(), false);

    Tuple w_list{
            Field::makeIntField(1),
            Field::makeVarcharField("hello"),
            Field::makeIntField(2),
            Field::makeVarcharField("world")
    };

    EXPECT_EQ(w_list.size(), fs.size());
    EXPECT_EQ(w_list.isDummy(), false);

    Tuple w_eq = w_vec;

    EXPECT_EQ(w_eq.size(), fs.size());
    EXPECT_EQ(w_eq.isDummy(), false);
}



TEST(TupleTest, TupleMutation)
{
    Tuple empty;

    EXPECT_EQ(empty.size(), 0);
    EXPECT_EQ(empty.isDummy(), false);

    empty.setDummyFlag(true);

    EXPECT_EQ(empty.isDummy(), true);

    Tuple notEmpty = empty;

    notEmpty.push_back(Field::makeIntField(1));
    notEmpty.push_back(Field::makeVarcharField("hello"));
    notEmpty.push_back(Field::makeIntField(2));
    notEmpty.push_back(Field::makeVarcharField("world"));

    EXPECT_EQ(empty.size(), 0);
    EXPECT_EQ(notEmpty.size(), 4);

    notEmpty.erase(3);

    EXPECT_EQ(notEmpty.size(), 3);

    notEmpty.project(ProjectList{0, 2});

    EXPECT_EQ(notEmpty.size(), 1);
    ASSERT_EQ(notEmpty[0], VarcharField("hello"));

    Tuple shouldBeNotEmpty = Tuple::join(notEmpty, empty);
    EXPECT_EQ(shouldBeNotEmpty.size(), 1);
    ASSERT_EQ(shouldBeNotEmpty[0], VarcharField("hello"));

    Tuple shouldBeNotEmptyTwice = Tuple::join(notEmpty, notEmpty);
    EXPECT_EQ(shouldBeNotEmptyTwice.size(), 2);
    ASSERT_EQ(shouldBeNotEmptyTwice[0], VarcharField("hello"));
    ASSERT_EQ(shouldBeNotEmptyTwice[1], VarcharField("hello"));
}


TEST(TupleTest, TupleAccess)
{
    Tuple w_list {
            Field::makeIntField(1),
            Field::makeVarcharField("hello"),
            Field::makeIntField(2),
            Field::makeVarcharField("world")
    };

    ASSERT_EQ(w_list[0], IntField(1));
    ASSERT_EQ(w_list[1], VarcharField("hello"));
    ASSERT_EQ(w_list[2], IntField(2));
    ASSERT_EQ(w_list[3], VarcharField("world"));
}