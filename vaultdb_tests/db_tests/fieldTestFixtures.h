#ifndef VAULTDB_FIELDTESTFIXTURES_H
#define VAULTDB_FIELDTESTFIXTURES_H

#include <gtest/gtest.h>
#include <plaintext/data/field/IntField.h>
#include <plaintext/data/field/BoolField.h>
#include <plaintext/data/field/VarcharField.h>
#include <in_sgx/obdata/obfield/ObliviousField.h>
#include <in_sgx/obdata/obfield/ObliviousTimestampNoZoneField.h>
#include <in_sgx/obdata/obfield/ObliviousIntField.h>
#include <in_sgx/obdata/obfield/ObliviousFixcharField.h>

using namespace db::obdata::obfield;

class FieldTest : public ::testing::Test {
protected:
    time_t scanForTimestampNoZone(const char * input) {
        tm tm1;
        sscanf(input,"%04d-%02d-%2d %02d:%02d:%02d",&tm1.tm_year,&tm1.tm_mon,&tm1.tm_mday,
               &tm1.tm_hour,&tm1.tm_min,&tm1.tm_sec);
        tm1.tm_year -= 1900;
        tm1.tm_mon -= 1;
        tm1.tm_isdst = 0;
        return timegm(&tm1);
    }

    FieldTest() :
    int_2(2),
    int_3(3),
    int_5(5),
    int_10(10),
    int_15(15),

    oint_200(200),
    oint_201(201),
    oint_212(212),
    oint_300(300),
    oint_0(0),
    oint_21(21),
    oint_1000(1000),

    str_hello("hello"),
    str_world("world"),
    str_helloWorld("helloworld"),

    ofc_halal("halal", 5),
    ofc_hella("hella", 5),
    ofc_hello("hello", 5),
    ofc_helloo("helloo", 6),
    ofc_holaaa("holaaa", 6),
    ofc_empty("", 0),

    ot2004_6_23_0(scanForTimestampNoZone("2004-06-23 00:00:00")),
    ot2004_6_23_1(scanForTimestampNoZone("2004-06-23 01:00:00")),
    ot2004_6_24(scanForTimestampNoZone("2004-06-24 00:00:00")),
    ot2004_7_5(scanForTimestampNoZone("2004-07-05 00:00:00")),

    day(60*60*24),
    eleven(11)
    {}

    db::data::field::IntField int_2;
    db::data::field::IntField int_3;
    db::data::field::IntField int_5;
    db::data::field::IntField int_10;
    db::data::field::IntField int_15;

    db::data::field::VarcharField str_hello;
    db::data::field::VarcharField str_world;
    db::data::field::VarcharField str_helloWorld;

    db::obdata::obfield::ObliviousIntField oint_200;
    db::obdata::obfield::ObliviousIntField oint_201;
    db::obdata::obfield::ObliviousIntField oint_212;
    db::obdata::obfield::ObliviousIntField oint_300;
    db::obdata::obfield::ObliviousIntField oint_0;
    db::obdata::obfield::ObliviousIntField oint_21;
    db::obdata::obfield::ObliviousIntField oint_1000;

    db::obdata::obfield::ObliviousFixcharField ofc_halal;
    db::obdata::obfield::ObliviousFixcharField ofc_hella;
    db::obdata::obfield::ObliviousFixcharField ofc_hello;
    db::obdata::obfield::ObliviousFixcharField ofc_helloo;
    db::obdata::obfield::ObliviousFixcharField ofc_holaaa;
    db::obdata::obfield::ObliviousFixcharField ofc_empty;

    ObliviousTimestampNoZoneField ot2004_6_23_0;
    ObliviousTimestampNoZoneField ot2004_6_23_1;
    ObliviousTimestampNoZoneField ot2004_6_24;
    ObliviousTimestampNoZoneField ot2004_7_5;

    ObliviousIntField day;
    ObliviousIntField eleven;
};

#endif //VAULTDB_FIELDTESTFIXTURES_H
