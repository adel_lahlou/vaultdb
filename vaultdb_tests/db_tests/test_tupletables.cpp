#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <plaintext/data/TupleTable.h>

#include "tupleTableTestFixtures.h"


using namespace db::data;
using namespace db::data::field;


bool tuple_equals(Tuple& t1, Tuple& t2);

TEST_F(TupleTableTest, ConstructionAndAccess)
{
    TupleTable empty(s1);

    EXPECT_EQ(empty.size(), 0);
    EXPECT_EQ(empty.getSchema().size(), 5);


    TupleTable w_list(s1, {
            Tuple{
                    Field::makeIntField(1),
                    Field::makeIntField(2),
                    Field::makeIntField(3),
                    Field::makeVarcharField("hello1"),
                    Field::makeVarcharField("world1")
            },
            Tuple{
                    Field::makeIntField(4),
                    Field::makeIntField(5),
                    Field::makeIntField(6),
                    Field::makeVarcharField("hello2"),
                    Field::makeVarcharField("world2")
            },
            Tuple{
                    Field::makeIntField(7),
                    Field::makeIntField(8),
                    Field::makeIntField(9),
                    Field::makeVarcharField("hello3"),
                    Field::makeVarcharField("world3")
            }
    });

    EXPECT_EQ(w_list.size(), 3);
    for(int i = 0; i < w_list.size(); ++i) {
        Tuple tableTuple = w_list[i];
        ASSERT_TRUE(tuple_equals(tableTuple, ts1[i]));
    }

    TupleTable w_tlist(s1, ts1);

    EXPECT_EQ(w_tlist.size(), 3);
    for(int i = 0; i < w_list.size(); ++i) {
        Tuple tableTuple = w_list[i];
        ASSERT_TRUE(tuple_equals(tableTuple, ts1[i]));
    }
}


TEST_F(TupleTableTest, Mutation)
{
    TupleTable w_vec(s1, ts1);

    Tuple t1 = Tuple{
            Field::makeIntField(10),
            Field::makeIntField(11),
            Field::makeIntField(12),
            Field::makeVarcharField("hello4"),
            Field::makeVarcharField("world4")
    };

    Tuple t2 = Tuple{
            Field::makeIntField(13),
            Field::makeIntField(14),
            Field::makeIntField(15),
            Field::makeVarcharField("hello5"),
            Field::makeVarcharField("world5")
    };

    w_vec.addTuple(t1);
    w_vec.addTuple(Tuple {
      t2[0],
      t2[1],
      t2[2],
      t2[3],
      t2[4],
    });

    for(int i = 0; i < ts1.size(); ++i) {
        Tuple tableTuple = w_vec[i];
        ASSERT_TRUE(tuple_equals(tableTuple, ts1[i]));
    }

    ASSERT_TRUE(tuple_equals(w_vec[3], t1));
    ASSERT_TRUE(tuple_equals(w_vec[4], t2));
}



bool tuple_equals(Tuple& t1, Tuple& t2)
{
    if(t1.size() != t2.size())
        return false;

    for(int f = 0; f < t1.size(); ++f) {
        if(t1[f] != t2[f])
            return false;
    }

    return true;
}
