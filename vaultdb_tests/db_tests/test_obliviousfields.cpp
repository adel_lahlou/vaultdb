#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <in_sgx/obdata/obfield/ObliviousIntField.h>
#include "fieldTestFixtures.h"

using namespace db::obdata::obfield;

TEST_F(FieldTest, ObliviousIntFieldBasic)
{
    ObliviousField shouldBeEleven = oint_212-oint_201;
    ASSERT_EQ(shouldBeEleven, eleven);
    ASSERT_TRUE(oint_200 <= oint_200);
    ASSERT_TRUE(oint_200 >= oint_200);
    ASSERT_TRUE(oint_200 == oint_200);

    ASSERT_TRUE(oint_201 != oint_200);
    ASSERT_TRUE(oint_201 >= oint_200);
    ASSERT_TRUE(oint_201 > oint_200);
    ASSERT_TRUE(oint_200 <= oint_201);
    ASSERT_TRUE(oint_200 < oint_201);

    ASSERT_FALSE(oint_201 == oint_1000);
    ASSERT_FALSE(oint_201 >= oint_1000);
    ASSERT_FALSE(oint_201 > oint_1000);
    ASSERT_FALSE(oint_201 <= oint_200);
    ASSERT_FALSE(oint_201 < oint_200);
}

TEST_F(FieldTest, ObliviousFixcharFieldBasic)
{
    ASSERT_TRUE(ofc_hella <= ofc_hella);
    ASSERT_TRUE(ofc_hella >= ofc_hella);
    ASSERT_TRUE(ofc_hella == ofc_hella);

    ASSERT_TRUE(ofc_hella != ofc_hello);
    ASSERT_TRUE(ofc_hella >= ofc_halal);
    ASSERT_TRUE(ofc_hella > ofc_halal);
    ASSERT_TRUE(ofc_halal <= ofc_hella);
    ASSERT_TRUE(ofc_halal < ofc_hella);

    ASSERT_TRUE(ofc_hella >= ofc_halal);
    ASSERT_TRUE(ofc_hella > ofc_halal);
    ASSERT_TRUE(ofc_halal <= ofc_hella);
    ASSERT_TRUE(ofc_halal < ofc_hella);

    ASSERT_TRUE(ofc_helloo >= ofc_hello);
    ASSERT_TRUE(ofc_helloo > ofc_hello);
    ASSERT_TRUE(ofc_hello <= ofc_helloo);
    ASSERT_TRUE(ofc_hello < ofc_helloo);

    ASSERT_TRUE(ofc_empty < ofc_helloo);

    ASSERT_FALSE(ofc_helloo == ofc_hello);
    ASSERT_FALSE(ofc_hella >= ofc_hello);
    ASSERT_FALSE(ofc_hella > ofc_hello);
    ASSERT_FALSE(ofc_hello <= ofc_hella);
    ASSERT_FALSE(ofc_hello < ofc_hella);

    ASSERT_FALSE(ofc_hella == ofc_hello);
}

TEST_F(FieldTest, ObliviousTimestampNoZoneBasic)
{
    ObliviousField shouldBeEleven = (ot2004_7_5-ot2004_6_24) / day;
    ASSERT_EQ(shouldBeEleven, eleven);
    ASSERT_EQ(ot2004_6_24.toString(), std::string("2004-06-24 00:00:00"));
    ASSERT_TRUE(ot2004_6_24 < ot2004_7_5);
    ASSERT_TRUE(ot2004_6_24 + (ObliviousIntField(10) * day) < ot2004_7_5);
    ASSERT_TRUE((day * ObliviousIntField(11)) + ot2004_6_24 == ot2004_7_5);
    ASSERT_TRUE(ot2004_6_24 + (ObliviousIntField(12) * day) > ot2004_7_5);

    unsigned char t1s[20];
    ot2004_6_24.serializeTo(t1s, sizeof(time_t));
    ASSERT_EQ(ObliviousTimestampNoZoneField::deserializeFrom(t1s).toString(),
              std::string("2004-06-24 00:00:00"));
    ASSERT_THROW(ot2004_6_24.serializeTo(t1s,1), std::length_error);
}

TEST_F(FieldTest, ObliviousIntFieldView)
{
    bool actualResult = false;
    ASSERT_TRUE(oint_201.isEqTo(oint_200, 1, actualResult));
    ASSERT_FALSE(actualResult);

    ASSERT_TRUE(oint_212.isGThan(oint_200, 1, false, actualResult));
    ASSERT_TRUE(actualResult);
    ASSERT_TRUE(oint_201.isGEThan(oint_200, 1, actualResult));
    ASSERT_TRUE(actualResult);
    ASSERT_TRUE(oint_201.isLEThan(oint_200, 1, actualResult));
    ASSERT_FALSE(actualResult);
    ASSERT_TRUE(oint_201.isLThan(oint_212, 1, false, actualResult));
    ASSERT_TRUE(actualResult);

    ASSERT_FALSE(oint_212.isLEThan(oint_201, 1, actualResult));
    ASSERT_TRUE(oint_212.isLEThan(oint_201, 2, actualResult));
    ASSERT_FALSE(actualResult);

    ASSERT_TRUE(oint_0.isGEThan(oint_1000, 5, actualResult));
    ASSERT_FALSE(actualResult);

    ASSERT_TRUE(oint_212.isLThan(oint_212, 1, false, actualResult));
    ASSERT_FALSE(actualResult);
    ASSERT_TRUE(oint_201.isLThan(oint_212, 1, false, actualResult));
    ASSERT_TRUE(actualResult);
    ASSERT_TRUE(oint_201.isLThan(oint_212, 2, false, actualResult));
    ASSERT_TRUE(actualResult);
    ASSERT_TRUE(oint_212.isLThan(oint_201, 2, false, actualResult));
    ASSERT_FALSE(actualResult);
}

TEST_F(FieldTest, ObliviousFixcharFieldView)
{
    bool actualResult;
    ASSERT_TRUE(ofc_hello.isEqTo(ofc_hella, 1, actualResult));
    ASSERT_FALSE(actualResult);
    ASSERT_FALSE(ofc_hello.isEqTo(ofc_halal, 1, actualResult));
    ASSERT_FALSE(actualResult);
    ASSERT_TRUE(ofc_hello.isEqTo(ofc_halal, 4, actualResult));
    ASSERT_FALSE(actualResult);

    ASSERT_TRUE(ofc_hello.isGEThan(ofc_hella, 1, actualResult));
    ASSERT_TRUE(actualResult);
    ASSERT_TRUE(ofc_hello.isGThan(ofc_hella, 1, false, actualResult));
    ASSERT_TRUE(actualResult);
    ASSERT_TRUE(ofc_hella.isGThan(ofc_hello, 1, false, actualResult));
    ASSERT_FALSE(actualResult);

    ASSERT_TRUE(ofc_helloo.isLEThan(ofc_holaaa, 1, actualResult));
    ASSERT_TRUE(actualResult);
    ASSERT_TRUE(ofc_helloo.isLThan(ofc_holaaa, 1, false, actualResult));
    ASSERT_TRUE(actualResult);
    ASSERT_FALSE(ofc_holaaa.isLThan(ofc_helloo, 1, false, actualResult));
    ASSERT_FALSE(actualResult);
    ASSERT_TRUE(ofc_helloo.isLThan(ofc_holaaa, 5, false, actualResult));
    ASSERT_TRUE(actualResult);
    ASSERT_TRUE(ofc_helloo.isLEThan(ofc_holaaa, 5, actualResult));
    ASSERT_TRUE(actualResult);
}

TEST_F(FieldTest, ObliviousTimestampNoZoneFieldView)
{
    bool actualResult;
    ASSERT_FALSE(ot2004_6_23_0.isEqTo(ot2004_6_23_1, 1, actualResult));
    ASSERT_FALSE(actualResult);
    ASSERT_TRUE(ot2004_6_23_0.isEqTo(ot2004_6_23_1, 2, actualResult));
    ASSERT_FALSE(actualResult);

    ASSERT_TRUE(ot2004_6_23_1.isLEThan(ot2004_6_23_0, 2, actualResult));
    ASSERT_FALSE(actualResult);

    ASSERT_FALSE(ot2004_6_24.isLEThan(ot2004_6_23_1, 2, actualResult));
    ASSERT_FALSE(actualResult);
    ASSERT_TRUE(ot2004_6_24.isLEThan(ot2004_6_23_1, 3, actualResult));
    ASSERT_FALSE(actualResult);
    ASSERT_FALSE(ot2004_7_5.isLEThan(ot2004_6_23_1, 3, actualResult));
    ASSERT_FALSE(actualResult);
    ASSERT_FALSE(ot2004_6_23_1.isGEThan(ot2004_7_5, 4, actualResult));
    ASSERT_FALSE(actualResult);
    ASSERT_TRUE(ot2004_7_5.isLEThan(ot2004_6_23_1, 5, actualResult));
    ASSERT_FALSE(actualResult);
    ASSERT_TRUE(ot2004_6_23_1.isGEThan(ot2004_7_5, 5, actualResult));
    ASSERT_FALSE(actualResult);

    ASSERT_TRUE(ot2004_6_24.isLThan(ot2004_6_23_1, 3, false, actualResult));
    ASSERT_FALSE(actualResult);
    ASSERT_TRUE(ot2004_6_23_1.isGThan(ot2004_6_24, 3, false, actualResult));
    ASSERT_FALSE(actualResult);
}