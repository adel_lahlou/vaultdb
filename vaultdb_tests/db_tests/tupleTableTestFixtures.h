#ifndef VAULTDB_TUPLETABLETESTFIXTURES_H_H
#define VAULTDB_TUPLETABLETESTFIXTURES_H_H

#include <gtest/gtest.h>
#include <initializer_list>


#include <plaintext/data/TupleTable.h>
#include <plaintext/data/field/IntField.h>
#include <plaintext/data/field/VarcharField.h>

class TupleTableTest : public ::testing::Test {
protected:

    TupleTableTest() {}

    type::RecordSchema s1 {
            (TableID) 3, {
                    {type::FieldDataType::Int, sizeof(int)},
                    {type::FieldDataType::Int, sizeof(int)},
                    {type::FieldDataType::Int, sizeof(int)},
                    {type::FieldDataType::Varchar, 20},
                    {type::FieldDataType::Varchar, 20},
            }
    };

    db::data::TupleList ts1 {
        db::data::Tuple{
            db::data::field::Field::makeIntField(1),
            db::data::field::Field::makeIntField(2),
            db::data::field::Field::makeIntField(3),
            db::data::field::Field::makeVarcharField("hello1"),
            db::data::field::Field::makeVarcharField("world1")
        },
        db::data::Tuple{
            db::data::field::Field::makeIntField(4),
            db::data::field::Field::makeIntField(5),
            db::data::field::Field::makeIntField(6),
            db::data::field::Field::makeVarcharField("hello2"),
            db::data::field::Field::makeVarcharField("world2")
        },
        db::data::Tuple{
                db::data::field::Field::makeIntField(7),
                db::data::field::Field::makeIntField(8),
                db::data::field::Field::makeIntField(9),
                db::data::field::Field::makeVarcharField("hello3"),
                db::data::field::Field::makeVarcharField("world3")
        }
    };
};

#endif //VAULTDB_TUPLETABLETESTFIXTURES_H_H
