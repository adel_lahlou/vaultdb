#include <gtest/gtest.h>
#include <in_sgx/obdata/ObliviousTupleBlock.h>
#include "obliviousTupleTableFixtures.h"

using namespace db::obdata;

TEST_F(ObliviousTupleTableTest, ObliviousTupleBlockSortStd)
{
    db::obdata::ObliviousTupleBlock input (0, 1, 2, 3, 8, {
            db::obdata::ObliviousTuple{
                    db::obdata::obfield::ObliviousField::makeObliviousIntField(1),
                    db::obdata::obfield::ObliviousField::makeObliviousIntField(2),
                    db::obdata::obfield::ObliviousField::makeObliviousFixcharField("worldd4", vc_size)
            },
            db::obdata::ObliviousTuple{
                    db::obdata::obfield::ObliviousField::makeObliviousIntField(1),
                    db::obdata::obfield::ObliviousField::makeObliviousIntField(2),
                    db::obdata::obfield::ObliviousField::makeObliviousFixcharField("world66", vc_size)
            },
            db::obdata::ObliviousTuple{
                    db::obdata::obfield::ObliviousField::makeObliviousIntField(4),
                    db::obdata::obfield::ObliviousField::makeObliviousIntField(5),
                    db::obdata::obfield::ObliviousField::makeObliviousFixcharField("worlda2", vc_size)
            },
            db::obdata::ObliviousTuple{
                    db::obdata::obfield::ObliviousField::makeObliviousIntField(8),
                    db::obdata::obfield::ObliviousField::makeObliviousIntField(2),
                    db::obdata::obfield::ObliviousField::makeObliviousFixcharField("worldb1", vc_size)
            },

            db::obdata::ObliviousTuple{
                    db::obdata::obfield::ObliviousField::makeObliviousIntField(7),
                    db::obdata::obfield::ObliviousField::makeObliviousIntField(8),
                    db::obdata::obfield::ObliviousField::makeObliviousFixcharField("worldc3", vc_size)
            },
    });

    db::obdata::ObliviousTupleBlock expected (0, 1, 2, 3, 8, {
            db::obdata::ObliviousTuple{
                    db::obdata::obfield::ObliviousField::makeObliviousIntField(7),
                    db::obdata::obfield::ObliviousField::makeObliviousIntField(8),
                    db::obdata::obfield::ObliviousField::makeObliviousFixcharField("worldc3", vc_size)
            },
            db::obdata::ObliviousTuple{
                    db::obdata::obfield::ObliviousField::makeObliviousIntField(4),
                    db::obdata::obfield::ObliviousField::makeObliviousIntField(5),
                    db::obdata::obfield::ObliviousField::makeObliviousFixcharField("worlda2", vc_size)
            },
            db::obdata::ObliviousTuple{
                    db::obdata::obfield::ObliviousField::makeObliviousIntField(1),
                    db::obdata::obfield::ObliviousField::makeObliviousIntField(2),
                    db::obdata::obfield::ObliviousField::makeObliviousFixcharField("worldd4", vc_size)
            },
            db::obdata::ObliviousTuple{
                    db::obdata::obfield::ObliviousField::makeObliviousIntField(1),
                    db::obdata::obfield::ObliviousField::makeObliviousIntField(2),
                    db::obdata::obfield::ObliviousField::makeObliviousFixcharField("world66", vc_size)
            },
            db::obdata::ObliviousTuple{
                    db::obdata::obfield::ObliviousField::makeObliviousIntField(8),
                    db::obdata::obfield::ObliviousField::makeObliviousIntField(2),
                    db::obdata::obfield::ObliviousField::makeObliviousFixcharField("worldb1", vc_size)
            },
    });

    std::vector<pos_vdb> sortColumns {1,0};
    std::vector<SortOrder> sortOrders {SortOrder ::DESCEND, SortOrder ::ASCEND};
    
    std::sort(input.begin(), input.end(), [sortColumns, sortOrders](ObliviousTuple & l, ObliviousTuple & r) {
        for (pos_vdb i = 0, size = (size_vdb)sortColumns.size(); i < size; i++) {
            int comparison = l[sortColumns[i]].comparesTo(r[sortColumns[i]], 0);
            if (sortOrders[i] == SortOrder::ASCEND && comparison < 0) {
                return true;
            } else if (sortOrders[i] == SortOrder::DESCEND && comparison > 0) {
                return true;
            } else if (comparison == 0) {
                continue;
            } else {
                return false;
            }
        }
        return false;
    });
    compareObliviousTupleBlock(1, expected, input);
}