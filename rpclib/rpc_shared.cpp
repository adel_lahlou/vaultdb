#include "include/rpc_shared.h"
#include <gflags/gflags.h>

DEFINE_bool(use_ssl, false, "turn on gRPC ssl");
DEFINE_string(server_cert, "server.crt", "server.crt path");
DEFINE_string(server_key, "server.key", "server.key path");
DEFINE_string(client_cert, "client.crt", "server.crt path");
DEFINE_string(client_key, "client.key", "server.key path");
DEFINE_string(ca_cert, "ca.crt", "ca.crt path");

void read(const std::string& filename, std::string& data){
    std::ifstream file ( filename.c_str (), std::ios::in );
    if ( file.is_open () )
    {
	std::stringstream ss;
	ss << file.rdbuf ();
	file.close ();
	data = ss.str ();
    }
    return;
}
