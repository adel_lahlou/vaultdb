#ifndef RPC_SHARED_H
#define RPC_SHARED_H
#include <fstream>
#include <iostream>
#include <sstream>
#include <gflags/gflags.h>

void read(const std::string& filename, std::string& data);
#endif
