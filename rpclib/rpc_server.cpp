#include "include/vaultdb.grpc.fb.h"
#include "include/vaultdb_generated.h"
#include <grpc++/grpc++.h>
#include "include/rpc_shared.h"

DECLARE_bool(use_ssl);
DECLARE_string(server_cert);
DECLARE_string(server_key);
DECLARE_string(ca_cert);

class NetworkTransmissionHandler{

    public:
	void serverProvideService(uint32_t srcMachineID, uint8_t type,
	unsigned char * buffer, unsigned int size);
};

class VaultDBServiceImpl final : public VaultDB::Service {

    public:
	void setHandler(NetworkTransmissionHandler * h) {
	    h_ = h;
	}
	virtual grpc::Status Data(
		grpc::ServerContext *context,
		const flatbuffers::grpc::Message<DataRequest> *request_msg,
		flatbuffers::grpc::Message<DataReply> *response_msg) override {

	    const DataRequest *request = request_msg->GetRoot();

	    //TODO: fix this we need to get the correct vector type out of the data!
	    uint32_t size = (unsigned int)request->data()->Length();
	    uint8_t  data[size];
	    uint32_t machineID = request->machineid();
	    uint8_t  type = request->type();
	    std::copy(request->data()->begin(), request->data()->end(), data);
	    h_->serverProvideService(machineID, type, data, size);

	    auto data_reply_offset = CreateDataReply(mb_, true);
	    mb_.Finish(data_reply_offset);
	    *response_msg = mb_.ReleaseMessage<DataReply>();
	    assert(response_msg->Verify());
	    std::cerr << "WOOP " <<  machineID << std::endl;
	    return grpc::Status::OK;
	}

    private:
	flatbuffers::grpc::MessageBuilder mb_;
	NetworkTransmissionHandler * h_;
};

void RunServer(NetworkTransmissionHandler * h, std::string server_address) {
    VaultDBServiceImpl service;
    service.setHandler(h);
    grpc::ServerBuilder builder;
    if (FLAGS_use_ssl) {
	std::string key;
	std::string cert;
	std::string root;
	read(FLAGS_server_cert, cert);
	read(FLAGS_server_key, key);
	read(FLAGS_ca_cert, root);
	grpc::SslServerCredentialsOptions::PemKeyCertPair keycert = {key, cert};
	grpc::SslServerCredentialsOptions sslOps;
	sslOps.pem_root_certs = root;
	sslOps.pem_key_cert_pairs.push_back (keycert);
	auto channel_creds = grpc::SslServerCredentials(sslOps);
	builder.AddListeningPort(server_address, channel_creds);

    } else {
	builder.AddListeningPort(server_address, grpc::InsecureServerCredentials());
    }

    builder.RegisterService(&service);
    std::unique_ptr<grpc::Server> server(builder.BuildAndStart());
    std::cerr << "Server listening on " << server_address << std::endl;
    server->Wait();
}
