#include "include/vaultdb.grpc.fb.h"
#include "include/vaultdb_generated.h"
#include <grpc++/grpc++.h>
#include <gflags/gflags.h>
#include "include/rpc_shared.h"

DECLARE_bool(use_ssl);
DECLARE_string(client_cert);
DECLARE_string(client_key);
DECLARE_string(ca_cert);

class VaultDBClient {
    public:
	VaultDBClient(std::shared_ptr<grpc::Channel> channel)
	    : stub_(VaultDB::NewStub(channel)) {}

	int Data(const uint32_t machine_ID, const uint8_t type, std::vector<uint8_t> bytes) {
	    flatbuffers::grpc::MessageBuilder mb;
	    auto data_offset = mb.CreateVector(bytes);
	    auto request_offset = CreateDataRequest(mb, machine_ID, type, data_offset);
	    mb.Finish(request_offset);
	    auto request_message = mb.ReleaseMessage<DataRequest>();


	    flatbuffers::grpc::Message<DataReply> response_msg;
	    grpc::ClientContext context;

	    auto status = stub_->Data(&context, request_message, &response_msg);
		if (status.ok()) {
		    const DataReply *response = response_msg.GetRoot();
		    return 0;
		} else {
		    std::cerr << status.error_code() << ": " << status.error_message()
			<< std::endl;
		    //TODO: fix error handling
		    return -1;
		}
	}
    private:
	std::unique_ptr<VaultDB::Stub> stub_;
};

int client_send(std::string server_addr, const uint32_t machine_ID, const uint8_t type, std::vector<uint8_t> bytes){

    std::shared_ptr<grpc::Channel> channel;
    if (FLAGS_use_ssl) {
	std::string cert;
	std::string key;
	std::string root;
	read (FLAGS_client_cert, cert );
	read (FLAGS_client_key, key );
	read (FLAGS_ca_cert, root );
	grpc::SslCredentialsOptions opts = {root,key,cert};
       channel = grpc::CreateChannel(server_addr, grpc::SslCredentials(opts));
    } else {
	channel =
	    grpc::CreateChannel(server_addr, grpc::InsecureChannelCredentials());
    }

    VaultDBClient vaultdb(channel);
    auto message = vaultdb.Data(machine_ID, type, bytes);
    std::cerr << "VaultDB sent: " << message << std::endl;
    return message;
}
