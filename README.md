## VaultDB

A secure relational database that provides a memory curtain and side-channel mitigation. 

Current implementation targets the SGX environment and links in our static library of relational operations.




### Getting started

Clone down the repo. This project uses cmake to build and gtest for the test suite. 
So, you have to create a build directory and clone down gtest.

```
git clone https://bitbucket.org/adel_lahlou/vaultdb.git 
cd vaultdb
git clone https://github.com/google/googletest.git ./vaultdb_tests/lib/googletest

mkdir build 
cd build
cmake ..
```

Change the `PG_BASE` variable in the root `CMakeLists.txt` to match the PostgreSQL root directory of your system.

You'll find the different tests in their respective folders. 




WORK IN PROGRESS:
    [] Combined Test Suite
    [] Join, Sort, Aggregation
    [] ORAM Integration
    [] Data oblivious operators
    [] SGX Execution